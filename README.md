# Unity_Project01_Battle

簡易バトルゲームです。
* 仕事用依頼メールアドレス(chiga.shou.job@gmail.com)

# Version

* VisualStudio 2017
* Unity 2020.1.12f1
* Addressables 1.8.5
* Input System 1.0.2

# Assets
* model
> [MODEL_ROBOT](https://www.cgtrader.com/free-3d-models/character/sci-fi/orange-robot-from-love-death-and-robots)



# Topic_System

* [Collision](https://gitlab.com/chigadio1/unityproject01/-/blob/main/Collision_Readme.md)
* [ExcelData](https://gitlab.com/chigadio1/unityproject01/-/blob/main/ExcelLoad_Readme.md)
* [MotionParameter（テスト段階）](https://gitlab.com/chigadio1/unityproject01/-/blob/main/MotionParameter_Readme.md)
* [Dictionary](https://gitlab.com/chigadio1/unityproject01/-/blob/main/Dictionary_Readme.md)
