﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 入力システム
/// </summary>
public class InputSystem
{
    /// <summary>
    /// 左スティック
    /// </summary>
    private Vector2 left_stick_vector_ = Vector2.zero;

    public void SetLeftStickVector(Vector2 value) 
    {
        left_stick_vector_ = value;
    }
    public Vector2 GetLeftStickVector()
    {
        return left_stick_vector_;
    }


    /// <summary>
    /// 右スティック
    /// </summary>
    private Vector2 right_stick_vector_ = Vector2.zero;

    public void SetRightStickVector(Vector2 value)
    {
        right_stick_vector_ = value;
    }
    public Vector2 GetRightStickVector()
    {
        return right_stick_vector_;
    }

}
