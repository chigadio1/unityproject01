﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// エフェクト制御
/// </summary>
public class EffectRestraint
{
    /// <summary>
    /// 停止フラグ
    /// </summary>
    private bool is_stop_ = false;

    /// <summary>
    /// オート削除
    /// </summary>
    private bool is_auto_destroy_ = false;
    public void OnAutoDestroy() 
    {
        is_auto_destroy_ = true;
    }
    public void OffAutoDestroy()
    {
        is_auto_destroy_ = false; 
    }

    public void OnStop() 
    {
        is_stop_ = true;
        if (particle_ == null) return;
        particle_.Stop();
    }
    public void OffStop()
    { 
        is_stop_ = false;
        particle_.Stop();
    }

    /// <summary>
    /// パーティクルシステム
    /// </summary>
    private ParticleSystem particle_ = null;

    /// <summary>
    /// Nullチェック
    /// </summary>
    /// <returns></returns>
    public bool NullCheck()
    {
        return particle_ == null ? true : false;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="ganme_object"></param>
    public void Init(GameObject ganme_object)
    {
        particle_ = ganme_object.GetComponent<ParticleSystem>();
    }

    public void Update()
    {
        if(is_auto_destroy_ == true)
        {
            Destroy();
            return;
        }
        if (is_stop_ == true) return;

        if (particle_.isPlaying == false) Destroy();
    }

    public void Destroy()
    {
        if (particle_ == null) return;
        if (particle_.gameObject == null) return;

        GameObject.Destroy(particle_.gameObject);
    }
}
