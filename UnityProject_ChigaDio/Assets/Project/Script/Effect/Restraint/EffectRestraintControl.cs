﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// エフェクト制御コントロール
/// </summary>
public class EffectRestraintControl
{
    /// <summary>
    /// エフェクト制御リスト
    /// </summary>
    private List<EffectRestraint> list_effect_restraint_ = new List<EffectRestraint>();
    /// <summary>
    /// Nullチェック
    /// </summary>
    /// <returns></returns>
    public bool NullCheck()
    {
        if (list_effect_restraint_.Count == 0) return true;
        return false;
    }

    /// <summary>
    /// 最大生存時間
    /// </summary>
    private float life_frame_ = 0.0f;

    /// <summary>
    /// 時間計測
    /// </summary>
    private float time_ = 0.0f;

    /// <summary>
    /// 停止フラグ
    /// </summary>
    private bool is_stop_ = false;

    public void Init(GameObject game_object,float value_life_frame)
    {
        if (game_object == null) return;

        life_frame_ = value_life_frame;

        var particles = game_object.GetComponentsInChildren<ParticleSystem>();

        foreach(var particle in particles)
        {
            var effect = new EffectRestraint();
            effect.Init(particle.gameObject);

            list_effect_restraint_.Add(effect);
        }
    }

    /// <summary>
    /// 停止On
    /// </summary>
    public void OnStop()
    {
        if (list_effect_restraint_.Count == 0) return;

        foreach (var effect in list_effect_restraint_)
        {
            effect.OnStop();
        }
        is_stop_ = true;
    }

    /// <summary>
    /// 停止OFF
    /// </summary>
    public void OffStop()
    {
        if (list_effect_restraint_.Count == 0) return;

        foreach (var effect in list_effect_restraint_)
        {
            effect.OffStop();
        }

        is_stop_ = false;
    }

    public void Update()
    {
        if (list_effect_restraint_.Count == 0) return;
        if (is_stop_ == true) return;

        UpdateLifeFrame();

        list_effect_restraint_.RemoveAll(effect => effect == null || effect.NullCheck() == true);

        foreach(var effect in list_effect_restraint_)
        {
            effect.Update();
        }
    }

    public void UpdateLifeFrame()
    {
        time_ += Time.deltaTime;

        if (time_ >= life_frame_ / 60.0f)
        {
            Destroy();
        }
    }

    public void Destroy()
    {
        foreach (var effect in list_effect_restraint_)
        {
            effect.Destroy();
        }

        list_effect_restraint_.Clear();
    }
}
