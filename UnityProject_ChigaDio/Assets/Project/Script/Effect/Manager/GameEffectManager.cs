﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲームエフェクト管理
/// </summary>
public class GameEffectManager
{
    public enum EffectRealmType
    {
        Main,
        Dungeon,
        Battle,
    }
    /// <summary>
    /// 生成されたエフェクトオブジェクト
    /// </summary>
    private EffectGenerateRepository effect_generate_repository_ = new EffectGenerateRepository();

    /// <summary>
    /// 保存されたエフェクトオブジェクト
    /// </summary>
    private EffectStorageRepository effect_storage_repository_ = new EffectStorageRepository();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    public bool is_setup_ = true;
    public bool GetIsSetUp() { return is_setup_ & effect_storage_repository_.GetIsSetUp(); }

    //エフェクトデータ
    private AddressableData<EffectScriptableData> addressable_effect_ = null;

    /// <summary>
    /// タイプ
    /// </summary>
    private EffectRealmType type_ = EffectRealmType.Main;

    /// <summary>
    /// 全ロードフラグ
    /// </summary>
    private bool all_load_flag_ = false;

    public void StartLoad(EffectRealmType type,bool all_load = true)
    {
        type_ = type;
        string type_path = GetTypeName(type);
        if (type_path == "") return;
        if (addressable_effect_ != null) addressable_effect_.Release();
        addressable_effect_ = new AddressableData<EffectScriptableData>();
        string load_path = $"Assets/Project/Assets/Data/Effect/" + type_path + $"/" + type_path + $".asset";
        addressable_effect_.LoadStart(load_path);
        all_load_flag_ = all_load;
        is_setup_ = false;
    }
    public void LoadEffect(int effect_id)
    {
      
        if (effect_storage_repository_.IsEffectObject(effect_id) == true) return;

        var effect_data = addressable_effect_.GetAddressableData().GetListData().Find(data => data.GetID() == effect_id);
        if (effect_data == null) return;

       

        var sound_data = SoundCore.Instance.GetSoundData((GameSoundPlayManager.SoundRealmType)type_, effect_data.GetSoundID());
        if (sound_data == null) return;

        effect_storage_repository_.LoadEffectObject(effect_data.GetEffectID());
        SoundCore.Instance.LoadSound((GameSoundPlayManager.SoundRealmType)type_, sound_data.GetSoundID());

        is_setup_ = false;
    }

    public void Update()
    {
        effect_generate_repository_.Update();
        if (is_setup_ == true) return;

        CheckUp();
    }

    private void CheckUp()
    {
        if (addressable_effect_.GetFlagSetUpLoading() == false) return;

        if (all_load_flag_)
        {
            foreach(var data in addressable_effect_.GetAddressableData().GetListData())
            {
                LoadEffect(data.GetID());
            }
        }

        effect_storage_repository_.Update();
        if (effect_storage_repository_.GetIsSetUp() == false) return;


        is_setup_ = true;
    }

    private string GetTypeName(EffectRealmType type)
    {
        switch (type)
        {
            case EffectRealmType.Main:
                return "Main";
            case EffectRealmType.Dungeon:
                return "Dungeon";
            case EffectRealmType.Battle:
                return "Battle";
        }

        return "";
    }

    /// <summary>
    /// 検索したIDでエフェクトオブジェクトを取得
    /// </summary>
    /// <param name="effect_id"></param>
    /// <returns></returns>
    public GameObject GetEffectObject(int effect_id)
    {
        return effect_storage_repository_.GetEffectObject(effect_id);


    }

    public EffectExcelData GetEffectData(int effect_id)
    {
        if (addressable_effect_.GetFlagSetUpLoading() == false) return null;
        return addressable_effect_.GetAddressableData().GetListData().Find(data => data.GetID() == effect_id);
    }

    public void AddEffectGenerate(EffectAction value_effect_action)
    {
        effect_generate_repository_.AddEffectAction(value_effect_action);
    }


    public void ReleaseAll()
    {
        effect_storage_repository_.ReleaseAll();
        effect_generate_repository_.ReleaseAll();
        addressable_effect_.Release();
    }
}
