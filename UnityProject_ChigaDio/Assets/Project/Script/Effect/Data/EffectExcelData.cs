﻿using ProjectSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// エフェクトExcelデータ
/// </summary>
[System.Serializable]
public class EffectExcelData : BaseComposition
{
    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }
    /// <summary>
    /// ID
    /// </summary>
    [SerializeField]
    private int id_ = 0;
    public int GetID() { return id_; }
    /// <summary>
    /// エフェクトID
    /// </summary>
    [SerializeField]
    private int effect_id_ = 0;
    public int GetEffectID() { return effect_id_; }
    /// <summary>
    /// サウンドID
    /// </summary>
    [SerializeField]
    private int sound_id_ = 0;
    public int GetSoundID() { return sound_id_; }

    /// <summary>
    /// X座標
    /// </summary>
    [SerializeField]
    private float position_x_ = 0.0f;
    public float GetPositionX() { return position_x_; }

    /// <summary>
    /// Y座標
    /// </summary>
    [SerializeField]
    private float position_y_ = 0.0f;
    public float GetPositionY() { return position_y_; }

    /// <summary>
    /// Z座標
    /// </summary>
    [SerializeField]
    private float position_z_ = 0.0f;
    public float GetPositionZ() { return position_z_; }


    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(id_), id_);
        data_group.AddData("EffectID", nameof(effect_id_), effect_id_);
        data_group.AddData("SoundID", nameof(sound_id_), sound_id_);
        data_group.AddData("座標:X", nameof(position_x_), position_x_);
        data_group.AddData("座標:Y", nameof(position_y_), position_y_);
        data_group.AddData("座標:Z", nameof(position_z_), position_z_);
    }

}
