﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(EffectScriptableData))]
public class EditorEffectDataScriptableInspector : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<EffectExcelData>(target as BaseExcelScriptableObject<EffectExcelData>);

        base.OnInspectorGUI();
    }
}
#endif


/// <summary>
/// 音データ
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreateEffectData")]
public class EffectScriptableData : BaseExcelScriptableObject<EffectExcelData>
{

}
