﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEffectRealm
{
    private Dictionary<GameEffectManager.EffectRealmType, GameEffectManager> dictionari_realm_effect_ = new Dictionary<GameEffectManager.EffectRealmType, GameEffectManager>();

    public  void Init()
    {
        dictionari_realm_effect_.Add(GameEffectManager.EffectRealmType.Main, new GameEffectManager());
        dictionari_realm_effect_.Add(GameEffectManager.EffectRealmType.Battle, new GameEffectManager());
        dictionari_realm_effect_.Add(GameEffectManager.EffectRealmType.Dungeon, new GameEffectManager());
       
    }

    public bool GetIsSetUp()
    {
        if (dictionari_realm_effect_.Count == 0) return true;
        return dictionari_realm_effect_[GameEffectManager.EffectRealmType.Main].GetIsSetUp() & dictionari_realm_effect_[GameEffectManager.EffectRealmType.Dungeon].GetIsSetUp() &
            dictionari_realm_effect_[GameEffectManager.EffectRealmType.Battle].GetIsSetUp();
    }

    public  void Update()
    {
        foreach (var data in dictionari_realm_effect_)
        {
            data.Value.Update();
        }
    }

    public void EffectInit(GameEffectManager.EffectRealmType type,bool auto_all_load = true)
    {
        dictionari_realm_effect_[type].StartLoad(type,auto_all_load);
    }

    public void MainEffectInit()
    {
        EffectInit(GameEffectManager.EffectRealmType.Main);
    }
    public void DungeonEffectInit()
    {
        EffectInit(GameEffectManager.EffectRealmType.Dungeon);
    }
    public void BattleEffectInit()
    {
        EffectInit(GameEffectManager.EffectRealmType.Battle);
    }


    public void AllDestroy()
    {
        foreach (var data in dictionari_realm_effect_)
        {
            data.Value.ReleaseAll();
        }
    }

    public void BattleEffectRelease()
    {
        dictionari_realm_effect_[GameEffectManager.EffectRealmType.Battle].ReleaseAll();
    }

    public void LoadEffect(GameEffectManager.EffectRealmType type,int effect_id)
    {
        dictionari_realm_effect_[type].LoadEffect(effect_id);
    }

    public GameObject GetEffectObject(GameEffectManager.EffectRealmType type,int effect_id)
    {
        return dictionari_realm_effect_[type].GetEffectObject(effect_id);

        
    }

    public EffectExcelData GetEffectData(GameEffectManager.EffectRealmType type,int effect_id)
    {

        return dictionari_realm_effect_[type].GetEffectData(effect_id);
    }

    public void AddEffectGenerate(GameEffectManager.EffectRealmType type, EffectAction value_effect_action)
    {
        dictionari_realm_effect_[type].AddEffectGenerate(value_effect_action);
    }
}
