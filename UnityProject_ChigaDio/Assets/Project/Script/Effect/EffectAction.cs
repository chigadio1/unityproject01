﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// エフセクト動作
/// </summary>
public class EffectAction : MonoBehaviour
{
    /// <summary>
    /// エフェクト制御管理
    /// </summary>
    private EffectRestraintControl effect_restraint_control_ = new EffectRestraintControl();

    /// <summary>
    /// 生存フレーム
    /// </summary>
    private float life_frame_ = 0.0f;
    public void SetLifeFrame(float value_life_frame) { life_frame_ = value_life_frame; }

    /// <summary>
    /// 停止On
    /// </summary>
    public void OnStop()
    {
        effect_restraint_control_.OnStop();
    }

    /// <summary>
    /// 停止OFF
    /// </summary>
    public void OffStop()
    {
        effect_restraint_control_.OffStop();
    }

    // Start is called before the first frame update
    void Start()
    {
        effect_restraint_control_.Init(gameObject, life_frame_);
    }

    // Update is called once per frame
    void Update()
    {
        effect_restraint_control_.Update();
        if(effect_restraint_control_.NullCheck())
        {
            EffectDestroy();
            return;
        }
    }

    public void EffectDestroy()
    {
        if (gameObject != null) GameObject.Destroy(gameObject);
        effect_restraint_control_.Destroy();
    }
}
