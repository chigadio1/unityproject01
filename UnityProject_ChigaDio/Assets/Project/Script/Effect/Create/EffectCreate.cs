﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// エフェクト生成
/// </summary>
public static class EffectCreate
{
    /// <summary>
    /// 生成
    /// </summary>
    /// <param name="effect_id"></param>
    /// <param name="position"></param>
    /// <param name="scale"></param>
    /// <param name="life_frame"></param>
    /// <returns></returns>
    public static EffectAction Appearance(int effect_id, Vector3 position, Quaternion rotation,Vector3 scale, float life_frame,GameEffectManager.EffectRealmType type)
    {
        var effect_data = EffectCore.Instance.GetEffectData(type, effect_id);
        if(effect_data == null)
        {
            return null;
        }
        var effect_obj = EffectCore.Instance.GetEffectObject(type,effect_data.GetEffectID());
        if (effect_obj == null)
        {
            EffectCore.Instance.LoadEffect(type,effect_id);
            return null;
        }

        var effect_generate = GameObject.Instantiate(effect_obj, position, rotation);
        effect_generate.transform.localScale = scale;
        effect_generate.transform.SetParent(EffectCore.Instance.transform);

        effect_generate.name = DictionaryAccessor.GetName(DictionaryAccessor.GetEffectDictionaryData(effect_data.GetEffectID()));

        var effect_action = effect_generate.GetComponent<EffectAction>();
        if (effect_action == null) effect_action = effect_generate.AddComponent<EffectAction>();
        effect_action.SetLifeFrame(life_frame);

        EffectCore.Instance.AddEffectGenerate(type,effect_action);
        SoundCore.Instance.BattleSESoundPlay(effect_data.GetSoundID());
        return effect_action;
    }
}
