﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectStorageRepository
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// 保管オブジェクト
    /// </summary>
    private Dictionary<int, AddressableData<GameObject>> storage_effect_object_ = new Dictionary<int, AddressableData<GameObject>>();

    /// <summary>
    /// ロード
    /// </summary>
    public void LoadEffectObject(int load_effect_id)
    {
        if (storage_effect_object_.ContainsKey(load_effect_id) == true) return;

        var effect_obj = new AddressableData<GameObject>();
        effect_obj.LoadStart($"Assets/Project/Assets/Effect/" + $"{load_effect_id:0000}" + "/Prefab/" + $"{load_effect_id:0000}" + ".prefab");
        storage_effect_object_.Add(load_effect_id, effect_obj);
        is_setup_ = false;
    }

    public void Update()
    {
        if (is_setup_ == true) return;

        is_setup_ = true;

        foreach(var effect in storage_effect_object_)
        {
            if(effect.Value.GetFlagSetUpLoading() == false)
            {
                is_setup_ = false;
            }
        }
    }

    public bool IsEffectObject(int search_id)
    {
        return storage_effect_object_.ContainsKey(search_id);
    }

    public GameObject GetEffectObject(int search_id)
    {
        if (storage_effect_object_.ContainsKey(search_id) == false) return null;

        if (storage_effect_object_[search_id].GetFlagSetUpLoading() == false) return null;

        return storage_effect_object_[search_id].GetAddressableData();
    }

    public void ReleaseAll()
    {
        foreach(var effect in storage_effect_object_)
        {
            if(effect.Value != null)effect.Value.Release();
        }
        storage_effect_object_.Clear();
    }

   
}
