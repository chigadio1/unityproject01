﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 生成エフェクト補間
/// </summary>
public class EffectGenerateRepository
{
    /// <summary>
    /// 生成されたエフェクトのリスト
    /// </summary>
    private List<EffectAction> list_effect_action_ = new List<EffectAction>();

    /// <summary>
    /// 追加エフェクト
    /// </summary>
    /// <param name="value_effect"></param>
    public void AddEffectAction(EffectAction value_effect)
    {
        list_effect_action_.Add(value_effect);
    }

    public void Update()
    {
        list_effect_action_.RemoveAll(data => data == null);
    }

    public void ReleaseAll()
    {
        list_effect_action_.RemoveAll(data => data == null);
        foreach(var effect in list_effect_action_)
        {
            effect.EffectDestroy();
        }

        list_effect_action_.Clear();
    }

    public void OnStopAll()
    {
        foreach (var effect in list_effect_action_)
        {
            if (effect == null) continue;
            effect.OnStop();
        }
    }

    public void OffStopAll()
    {
        foreach (var effect in list_effect_action_)
        {
            if (effect == null) continue;
            effect.OffStop();
        }
    }
}
