﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// エフェクトコア
/// </summary>
public class EffectCore : Singleton<EffectCore>
{

    /// <summary>
    /// ゲームエフェクト管理
    /// </summary>
    private GameEffectRealm game_effect_realm_ = new GameEffectRealm();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }
    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "EffectCore";
        game_effect_realm_.Init();
        //仮
        game_effect_realm_.MainEffectInit();
        game_effect_realm_.DungeonEffectInit();
        game_effect_realm_.BattleEffectInit();
    }

    public void BattleStart()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        is_setup_ = game_effect_realm_.GetIsSetUp();

        game_effect_realm_.Update();
    }

    /// <summary>
    /// ロードエフェクト
    /// </summary>
    /// <param name="effect_id"></param>
    public void LoadEffect(GameEffectManager.EffectRealmType type,int effect_id)
    {
        game_effect_realm_.LoadEffect(type, effect_id);
    }

    /// <summary>
    /// 検索したIDでエフェクトオブジェクトを取得
    /// </summary>
    /// <param name="effect_id"></param>
    /// <returns></returns>
    public GameObject GetEffectObject(GameEffectManager.EffectRealmType type,int effect_id)
    {
        return game_effect_realm_.GetEffectObject(type,effect_id);

        
    }

    public EffectExcelData GetEffectData(GameEffectManager.EffectRealmType type, int effect_id)
    {

        return game_effect_realm_.GetEffectData(type, effect_id);
    }

    public void AddEffectGenerate(GameEffectManager.EffectRealmType type,EffectAction value_effect_action)
    {
        game_effect_realm_.AddEffectGenerate(type,value_effect_action);
    }

    public void BattleEffectRelease()
    {
        game_effect_realm_.BattleEffectRelease();
    }

    public void AllReleaseObject()
    {
        game_effect_realm_.AllDestroy();
    }


    private void OnDestroy()
    {
        AllReleaseObject();
    }
}
