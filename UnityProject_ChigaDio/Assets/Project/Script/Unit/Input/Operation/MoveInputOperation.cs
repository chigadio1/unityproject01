﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// 移動入力
/// </summary>
public class MoveInputOperation : BaseInputOperation<BaseUnit,BaseFlagActionManager>
{
    /// <summary>
    /// 移動方向
    /// </summary>
    private Vector3 move_forward_ = Vector3.zero;

    private float move_speed_ = 10.0f;

    public override void InputAction(BaseUnit unit, BaseFlagActionManager flag_manager)
    {
        MoveInput(unit, flag_manager);
    }

    protected void MoveInput(BaseUnit unit, BaseFlagActionManager flag_manager)
    {
        if (Keyboard.current.wKey.isPressed | Keyboard.current.sKey.isPressed | Keyboard.current.aKey.isPressed | Keyboard.current.dKey.isPressed)
        {
            flag_manager.OnMoveFlag();
            move_forward_ = Vector3.zero;

            if (Keyboard.current.wKey.isPressed)
            {
                move_forward_.z = 1.0f;
            }
            else if (Keyboard.current.sKey.isPressed)
            {
                move_forward_.z = -1.0f;
            }
            if (Keyboard.current.aKey.isPressed)
            {
                move_forward_.x = -1.0f;
            }
            else if (Keyboard.current.dKey.isPressed)
            {
                move_forward_.x = 1.0f;
            }

        }
        else
        {
            flag_manager.OffMoveFlag();
        }
    }

    

    public override void GameAction(BaseUnit unit, BaseFlagActionManager flag_manager)
    {
        MoveAction(unit, flag_manager);



    }

    protected void MoveAction(BaseUnit unit, BaseFlagActionManager flag_manager)
    {
        if (flag_manager.GetMoveFlag() == false)
        {
            unit.ChangeAnimation(AnimationConvert.AnimationState.Anim_Idle);
            return;
        }

        Vector3 camera_forward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 camera_right = Camera.main.transform.right;


        camera_forward.y = 0.0f;

        Vector3 move = camera_forward * move_forward_.z + camera_right * move_forward_.x;

        Vector3 move_pos = unit.transform.position + (move * (move_speed_ * Time.deltaTime));

        Vector3 diff = (move_pos - unit.transform.position);
        if (diff.magnitude >= 0.01f)
        {
            unit.transform.rotation = Quaternion.LookRotation(diff);
            unit.transform.position = move_pos;
            unit.ChangeAnimation(AnimationConvert.AnimationState.Anim_Run);
        }
    }
}

/// <summary>
/// 移動入力(バトル中)
/// </summary>
public class BattleMoveInputOperation : BaseInputOperation<BattleUnit, FlagBattlePlayerUnitManager>
{
    /// <summary>
    /// 移動方向
    /// </summary>
    private Vector3 move_forward_ = Vector3.zero;

    private float move_speed_ = 10.0f;

    public override void InputAction(BattleUnit unit, FlagBattlePlayerUnitManager flag_manager)
    {
        if (flag_manager.GetCharaAction() == true) return;
        if (flag_manager.GetDeathOrKnockBackFlag() == true) return;
        MoveInput(unit, flag_manager);
    }

    protected void MoveInput(BattleUnit unit, FlagBattlePlayerUnitManager flag_manager)
    {
        if (Keyboard.current.wKey.isPressed | Keyboard.current.sKey.isPressed | Keyboard.current.aKey.isPressed | Keyboard.current.dKey.isPressed)
        {
            flag_manager.OnMoveFlag();
            move_forward_ = Vector3.zero;

            if (Keyboard.current.wKey.isPressed)
            {
                move_forward_.z = 1.0f;
            }
            else if (Keyboard.current.sKey.isPressed)
            {
                move_forward_.z = -1.0f;
            }
            if (Keyboard.current.aKey.isPressed)
            {
                move_forward_.x = -1.0f;
            }
            else if (Keyboard.current.dKey.isPressed)
            {
                move_forward_.x = 1.0f;
            }

        }
        else
        {
            flag_manager.OffMoveFlag();
        }
    }



    public override void GameAction(BattleUnit unit, FlagBattlePlayerUnitManager flag_manager)
    {
        if (flag_manager.GetCharaAction() == true) return;
        if (flag_manager.GetDeathOrKnockBackFlag() == true) return;
        MoveAction(unit, flag_manager);



    }

    protected void MoveAction(BattleUnit unit, FlagBattlePlayerUnitManager flag_manager)
    {
        if (flag_manager.GetMoveFlag() == false)
        {
            unit.ChangeAnimation(AnimationConvert.AnimationState.Anim_Idle);
            return;
        }

        Vector3 camera_forward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 camera_right = Camera.main.transform.right;


        camera_forward.y = 0.0f;

        Vector3 move = camera_forward * move_forward_.z + camera_right * move_forward_.x;

        Vector3 move_pos = unit.transform.position + (move * (move_speed_ * Time.deltaTime));

        Vector3 diff = (move_pos - unit.transform.position);
        if (diff.magnitude >= 0.01f)
        {
            unit.transform.rotation = Quaternion.LookRotation(diff);
            unit.transform.position = move_pos;
            unit.ChangeAnimation(AnimationConvert.AnimationState.Anim_Run);
        }
    }
}