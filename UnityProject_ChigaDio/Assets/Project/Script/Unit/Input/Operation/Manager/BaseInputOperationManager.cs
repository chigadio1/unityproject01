﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎入力操作管理
/// </summary>
public class BaseInputOperationManager<T,F> where T : BaseUnit where F : BaseFlagActionManager
{
    /// <summary>
    /// 移動
    /// </summary>
    protected MoveInputOperation move_input_operation_ = new MoveInputOperation();


    /// <summary>
    /// 入力
    /// </summary>
    /// <param name="unit"></param>
    /// <param name="flag_manager"></param>
    public virtual  void InputAction(T unit, F flag_manager)
    {
        move_input_operation_.InputAction(unit, flag_manager);
    }

    /// <summary>
    /// 動作
    /// </summary>
    /// <param name="unit"></param>
    /// <param name="flag_manager"></param>
    public virtual void GameAction(T unit, F flag_manager)
    {
        move_input_operation_.GameAction(unit, flag_manager);
    }
}
