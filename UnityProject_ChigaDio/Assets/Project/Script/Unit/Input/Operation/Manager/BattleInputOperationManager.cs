﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleInputOperationManager : BaseInputOperationManager<BattleUnit, FlagBattlePlayerUnitManager>
{

    /// <summary>
    /// 移動
    /// </summary>
    protected BattleMoveInputOperation battle_move_input_operation_ = new BattleMoveInputOperation();

    /// <summary>
    /// キャラアクション
    /// </summary>
    private CharaActionInputOperation chara_action_input_operation_ = new CharaActionInputOperation();

    public override void InputAction(BattleUnit unit, FlagBattlePlayerUnitManager flag_manager)
    {
        battle_move_input_operation_.InputAction(unit, flag_manager);
        chara_action_input_operation_.InputAction(unit, flag_manager);
    }

    public override void GameAction(BattleUnit unit, FlagBattlePlayerUnitManager flag_manager)
    {
        battle_move_input_operation_.GameAction(unit, flag_manager);
        chara_action_input_operation_.GameAction(unit, flag_manager);
    }
}