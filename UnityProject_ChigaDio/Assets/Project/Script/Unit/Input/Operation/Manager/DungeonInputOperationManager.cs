﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョン中の入力
/// </summary>
public class DungeonInputOperationManager : BaseInputOperationManager<DungeonPlayerUnit,FlagDungeonPlayerUnitManager>
{
    public override void InputAction(DungeonPlayerUnit unit, FlagDungeonPlayerUnitManager flag_manager)
    {
        base.InputAction(unit, flag_manager);
    }

    public override void GameAction(DungeonPlayerUnit unit, FlagDungeonPlayerUnitManager flag_manager)
    {
        base.GameAction(unit, flag_manager);
    }
}
