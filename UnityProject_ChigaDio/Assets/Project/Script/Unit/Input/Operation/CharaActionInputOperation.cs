﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// キャラアクション操作
/// </summary>
public class CharaActionInputOperation : BaseInputOperation<BattleUnit, FlagBattlePlayerUnitManager>
{

    /// <summary>
    /// モーションアクション
    /// </summary>
    private MotionParameterConvert.MotionActionName motion_action_ = MotionParameterConvert.MotionActionName.MAX_Action;

    public override void InputAction(BattleUnit unit, FlagBattlePlayerUnitManager flag_manager)
    {
        if (flag_manager.GetDeathOrKnockBackFlag() == true) return;
        if(Keyboard.current.jKey.isPressed)
        {
            flag_manager.OnCharaActionSquareTriggerFlag();
        }
        else if(Keyboard.current.iKey.isPressed)
        {
            flag_manager.OnCharaActionCrossTriggerFlag();
        }
        else if(Keyboard.current.kKey.isPressed)
        {
            flag_manager.OnCharaActionTriangleTriggerFlag();
        }
    }



    public override void GameAction(BattleUnit unit, FlagBattlePlayerUnitManager flag_manager)
    {
        if (flag_manager.GetDeathOrKnockBackFlag() == true) return;

        //モーションパラメーター作動中
        if (flag_manager.GetCharaAction())
        {
            if(unit.GetIsMotionParameterPlay() == false)
            {
                flag_manager.OffCharaActionTriangleFlag();
                flag_manager.OffCharaActionSquareFlag();
                flag_manager.OffCharaActionCrossFlag();
                motion_action_ = MotionParameterConvert.MotionActionName.MAX_Action;
            }
        }
        else
        {
            if (flag_manager.GetCharaActionSquareTriggerFlag())
            {
                motion_action_ = MotionParameterConvert.MotionActionName.Attack_Combo_A;
                flag_manager.OnCharaActionSquareFlag();
            }
            else if (flag_manager.GetCharaActionCrossTriggerFlag())
            {
                motion_action_ = MotionParameterConvert.MotionActionName.Attack_Combo_B;
                flag_manager.OnCharaActionCrossFlag();
            }
            else if (flag_manager.GetCharaActionTriangleTriggerFlag())
            {
                motion_action_ = MotionParameterConvert.MotionActionName.Attack_Combo_A;
                flag_manager.OnCharaActionTriangleFlag();
            }

            if (motion_action_ == MotionParameterConvert.MotionActionName.MAX_Action) return;

            unit.PlayMotionAction(motion_action_);
        }



    }
}
