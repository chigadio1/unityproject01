﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 基礎操作
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="F"></typeparam>
public class BaseInputOperation<T,F> where T : BaseUnit where F : BaseFlagActionManager
{
    public virtual  void InputAction(T unit,F flag_manager) { }

    public virtual void GameAction(T unit,F flag_manager) { }
}
