﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ルート
/// </summary>
public class RootDetails 
{
    /// <summary>
    /// ルートデータ
    /// </summary>
    private DungeonRootData root_data_ = null;

    /// <summary>
    /// ルートインデックス
    /// </summary>
    private int root_index_ = 0;

    /// <summary>
    /// ルートTransform
    /// </summary>
    private DungeonUnit dungeon_unit_ = null;
    

    public void InitRoot(DungeonRootData value_data,DungeonUnit value_unit) 
    {
        root_data_ = value_data;
        dungeon_unit_ = value_unit;
    }

    public void UpdateRoot()
    {
        if (dungeon_unit_ == null) return;
        MoveUpdate();
    }

    public void MoveUpdate()
    {
        Vector3 this_position = dungeon_unit_.transform.position;
        Vector3 root_position = root_data_.GetListRootPosition()[root_index_].GetRootPosition();

        ///仮
        if((root_position - this_position).magnitude <= 1.0f)
        {
            root_index_++;
            if (root_data_.GetListRootPosition().Count <= root_index_) root_index_ = 0;
        }
        else
        {
            dungeon_unit_.ChangeAnimation(AnimationConvert.AnimationState.Anim_Walk);
        }

        dungeon_unit_.transform.position += (root_position - this_position).normalized * root_data_.GetMoveSpeed();

        if (root_position == this_position) return;
        Vector3 look_angle =  Quaternion.LookRotation((root_position - this_position), Vector3.up).eulerAngles;
        look_angle.x = look_angle.z = 0.0f;

        dungeon_unit_.transform.rotation = Quaternion.Lerp(dungeon_unit_.transform.rotation, Quaternion.Euler(look_angle), root_data_.GetLookSpeed());
    }
}
