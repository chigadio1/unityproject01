﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonUnit : BaseUnit
{
    /// <summary>
    /// コリジョン
    /// </summary>
    [SerializeField]
    protected BaseCollisionConstruction collision_;

    protected void CollisionSetUp()
    {
        collision_ = new BaseCollisionConstruction();
        collision_.Init(gameObject, CollisionLayer.kPlayer, CollisionPushType.kThisPush, LayerMask.NameToLayer("Player"));
    }
}
