﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonPlayerUnit : DungeonUnit
{

    /// <summary>
    /// フラグ管理
    /// </summary>
    private FlagDungeonPlayerUnitManager flag_dungeon_manager_ = new FlagDungeonPlayerUnitManager();

    /// <summary>
    /// 入力アクション
    /// </summary>
    private DungeonInputOperationManager input_operation_manager_ = new DungeonInputOperationManager();

    // Start is called before the first frame update
    void Start()
    {
        
        unit_animation_.Init(gameObject);
        flag_dungeon_manager_.Init();

        

        ///仮
        is_setup_ = true;
        CollisionSetUp();
        collision_.RpgCollisionDetailsControl.HitSecondFunc = HitEnter;
    }

    // Update is called once per frame
    void Update()
    {
        if (is_setup_ == false) return;
        if (is_stop_ == true) return;
        UpdateInput();
        StartDungeonTalk();


    }

    /// <summary>
    /// 座標が近い会話ポジションを割り出し、開始させる
    /// </summary>
    /// <param name="position"></param>
    private void StartDungeonTalk()
    {
        DungeonCore.Instance.StartDungeonTalk(transform.position);
    }

    void UpdateInput()
    {
        input_operation_manager_.InputAction(this, flag_dungeon_manager_);
        input_operation_manager_.GameAction(this, flag_dungeon_manager_);
        flag_dungeon_manager_.Update();
    }

    void HitEnter(Collider collider, Collider this_collider)
    {
        var unit = collider.gameObject.AllComponent<DungeonEnemyUnit>();
        if (unit == null) return;

        BattleCore.Instance.BattleStart(unit);
    }
}
