﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonEnemyUnit : DungeonUnit
{
    /// <summary>
    /// ダンジョンUniEnemyID
    /// </summary>
    private int dungeon_unit_enemy_id_ = 0;
    public int GetDungeonUnitEnemyID() { return dungeon_unit_enemy_id_; }

    /// <summary>
    /// ルート詳細
    /// </summary>
    private RootDetails root_details_ = new RootDetails();

    public void InitDungeonEnemy(int value_character_id, int value_dungeon_unit_enemy_id, DungeonRootData value_data ,int value_character_details_id = 1)
    {
        dungeon_unit_enemy_id_ = value_dungeon_unit_enemy_id;
        InitUnit(value_character_id, value_character_details_id);

        root_details_.InitRoot(value_data, this);

     

        unit_animation_.Init(gameObject);
    }

    public void Start()
    {
        CollisionSetUp();
    }

    public void Update()
    {
        root_details_.UpdateRoot();
    }

}
