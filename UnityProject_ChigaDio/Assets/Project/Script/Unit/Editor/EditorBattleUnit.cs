﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

/// <summary>
/// バトルUnitエディタ
/// </summary>
[CustomEditor(typeof(BattleUnit))]
public class EditorBattleUnit : Editor
{


    /// <summary>
    /// モーションパラメータのアクションインデックス
    /// </summary>
    private int motion_action_index_ = 0;
    public override void OnInspectorGUI()
    {
        UnitInspector();
    }

    protected void UnitInspector()
    {
        var unit = target as BattleUnit;
        MotionParameter(unit);
        CharaStatus(unit);

    }

    protected void MotionParameter(BattleUnit unit)
    {
        if (UnityEditor.EditorApplication.isPlaying == false) return;
        if (DataCore.Instance.GetIsSetUp() == false) return;

        if (EditorApplication.isPlaying == false) return;

        EditorGUILayout.BeginHorizontal();


        var list_data = DictionaryAccessor.GetMotionParameterActionDictionaryName();
        if (list_data == null || list_data.Count == 0)
        {
            EditorGUILayout.EndHorizontal();
            return;
        }


        EditorGUI.BeginChangeCheck();
        var select_index = motion_action_index_;

        var pul_index = list_data.Count > 0 ? EditorGUILayout.Popup("Action", select_index, list_data.ToArray()) : -1;

        if (EditorGUI.EndChangeCheck())
        {
            motion_action_index_ = pul_index;
        }

        if (GUILayout.Button("PlayAction"))
        {
            var data = DictionaryAccessor.GetMotionParameterActionDictionaryData(list_data[motion_action_index_]);
            unit.PlayMotionAction(DictionaryAccessor.GetID(data));
        }


        EditorGUILayout.EndHorizontal();
    }

    protected void CharaStatus(BattleUnit unit)
    {
        EditorGUILayout.BeginVertical(GUI.skin.box);

        EditorGUILayout.LabelField("CharaStatus---");

        EditorGUILayout.BeginHorizontal(GUI.skin.box);

        GUILayout.Label("HP");

        GUILayout.Label("MAX" + unit.GetMaxHP().ToString());

        GUILayout.Label("NOW" + unit.GetNowHP().ToString());

        EditorGUILayout.EndHorizontal();


        EditorGUILayout.BeginHorizontal(GUI.skin.box);

        GUILayout.Label("MP");

        GUILayout.Label("MAX" + unit.GetMaxMP().ToString());

        GUILayout.Label("NOW" + unit.GetNowMP().ToString());

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal(GUI.skin.box);

        GUILayout.Label("ATTACK");

        GUILayout.Label("MAX" + unit.GetBaseAttack().ToString());

        GUILayout.Label("NOW" + unit.GetNowAttack().ToString());

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal(GUI.skin.box);

        GUILayout.Label("SPEED");

        GUILayout.Label("MAX" + unit.GetBaseSpeed().ToString());

        GUILayout.Label("NOW" + unit.GetNowSpeed().ToString());

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal(GUI.skin.box);

        GUILayout.Label("DEFENCE");

        GUILayout.Label("MAX" + unit.GetBaseDefence().ToString());

        GUILayout.Label("NOW" + unit.GetNowDefence().ToString());

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();
    }
}
#endif