﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Unitコア
/// </summary>
public class UnitCore : Singleton<UnitCore>
{
    /// <summary>
    /// Unitマネージャー
    /// </summary>
    private UnitManager unit_manager_ = new UnitManager();

    /// <summary>
    /// ロードしらUnitのID
    /// </summary>
    /// <returns></returns>
    public List<int> GetLoadUnitIDList()
    {
        return unit_manager_.GetLoadUnitIDList();
    }


    /// <summary>
    /// セットアップフラグ
    /// </summary>
    /// <returns></returns>
    public bool GetIsSetUp() { return unit_manager_.GetIsSetUp(); }
    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "UnitCore";
    }

    // Update is called once per frame
    void Update()
    {
        unit_manager_.Update();
    }

    /// <summary>
    /// バトルキャラ作成
    /// </summary>
    /// <param name="create_id"></param>
    public void CreateUnit(int create_id)
    {
        unit_manager_.CreateUnit(create_id);
    }

    /// <summary>
    /// UnitObject
    /// </summary>
    /// <param name="serach_id"></param>
    /// <returns></returns>
    public GameObject GetUnitGameObject(int serach_id)
    {
        return unit_manager_.GetUnitGameObject(serach_id);
    }

    public void ReleaseUnit(int id)
    {
        unit_manager_.ReleaseUnit(id);
    }

    public void OnDestroy()
    {
        unit_manager_.ReleaseUnitAll();
    }


}
