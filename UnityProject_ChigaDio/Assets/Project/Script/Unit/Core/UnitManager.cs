﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ユニットマネージャー
/// </summary>
public class UnitManager
{
    /// <summary>
    /// ユニットリスト
    /// </summary>
    private Dictionary<int,AddressableData<GameObject>> dictionary_unit_ = new Dictionary<int, AddressableData<GameObject>>();

    /// <summary>
    /// UnitObject
    /// </summary>
    /// <param name="serach_id"></param>
    /// <returns></returns>
    public GameObject GetUnitGameObject(int serach_id)
    {
        if ((is_setup_load_ & is_setup_unit_) == false) return null;
        if (dictionary_unit_.ContainsKey(serach_id) == false) return null;
        return dictionary_unit_[serach_id].GetAddressableData();
    }

    /// <summary>
    /// ロードしらUnitのID
    /// </summary>
    /// <returns></returns>
    public List<int> GetLoadUnitIDList()
    {
        if ((is_setup_load_ & is_setup_unit_) == false) return null;
        List<int> load_units = new List<int>();

        foreach(var data in dictionary_unit_)
        {
            load_units.Add(data.Key);
        }

        return load_units;
    }


    /// <summary>
    /// セットアップロードフラグ
    /// </summary>
    private bool is_setup_load_ = false;
    public bool GetIsSetUpLoad() { return is_setup_load_; }

    /// <summary>
    /// セットアップUnitフラグ
    /// </summary>
    private bool is_setup_unit_ = false;
    public bool GetIsSetUpUnit() { return is_setup_unit_; }

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    /// <returns></returns>
    public bool GetIsSetUp() { return is_setup_load_ & is_setup_unit_; }

    /// <summary>
    /// バトルキャラ作成
    /// </summary>
    /// <param name="create_id"></param>
    public void CreateUnit(int create_id)
    {
        if (dictionary_unit_.ContainsKey(create_id) == true) return;

        var data = new AddressableData<GameObject>();
        data.LoadStart($"Assets/Project/Assets/Model/Chara/{create_id:0000}/Prefab/{create_id:0000}_BattlUnit.prefab");
        dictionary_unit_.Add(create_id,data);
        is_setup_load_ = false;
        is_setup_unit_ = false;
    }

    /// <summary>
    /// 更新処理
    /// </summary>
    public void Update()
    {
        LoadUpdate();
        SetUpBattleUnit();
    }

    /// <summary>
    /// 更新ロード
    /// </summary>
    public void LoadUpdate()
    {
        if (is_setup_load_ == true) return;
        is_setup_load_ = true;
        foreach(var unit in dictionary_unit_)
        {
            if (unit.Value.GetFlagSetUpLoading() == false) is_setup_load_ = false;
        }
    }

    /// <summary>
    /// バトルユニットのセットアップ
    /// </summary>
    public void SetUpBattleUnit()
    {
        if (is_setup_unit_ == true) return;
        if (is_setup_load_ == false) return;



        is_setup_unit_ = true;

        foreach(var data in dictionary_unit_)
        {
            if(data.Value.GetFlagSetUpLoading() == false)
            {
                is_setup_unit_ = false;
                continue;
            }
        }

        is_setup_unit_ = true;
    }


    public void ReleaseUnit(int id)
    {
        if (dictionary_unit_.Count == 0) return;

        if (dictionary_unit_.ContainsKey(id) == false) return;

        dictionary_unit_[id].Release();
        dictionary_unit_.Remove(id);
    }


    public void ReleaseUnitAll()
    {
        if (dictionary_unit_.Count == 0) return;


        foreach (var unit in dictionary_unit_)
        {
            unit.Value.Release();
        }

        dictionary_unit_.Clear();
    }
}
