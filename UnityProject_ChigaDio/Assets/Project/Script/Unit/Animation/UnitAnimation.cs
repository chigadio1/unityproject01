﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ユニットアニメーション
/// </summary>
public class UnitAnimation
{
    /// <summary>
    /// アニメーター
    /// </summary>
    private Animator unit_animator_ = null;

    /// <summary>
    /// 現在のアニメーション状態
    /// </summary>
    private AnimationConvert.AnimationState now_animation_state_ = AnimationConvert.AnimationState.Anim_Idle;

    /// <summary>
    /// アニメーションスピード
    /// </summary>
    private float animation_speed_ = 1.0f;

    public void Init(GameObject this_object)
    {
        if (this_object == null) return;

        unit_animator_ =  this_object.GetComponentInChildren<Animator>();
        animation_speed_ = unit_animator_.speed;
    }

    public void OnStop()
    {
        if (unit_animator_ == null) return;
        unit_animator_.speed = 0.0f;
    }

    public void OnPlay()
    {
        if (unit_animator_ == null) return;
        unit_animator_.speed = animation_speed_;
    }

    /// <summary>
    /// アニメーション切り替え
    /// </summary>
    /// <param name="next_state"></param>
    /// <param name="forced"></param>
    public void ChangeAnimation(AnimationConvert.AnimationState next_state, bool forced = false)
    {
        if (unit_animator_ == null) return;
        if(forced == false)
        {
            if (next_state == now_animation_state_) return;
        }

        now_animation_state_ = next_state;


        unit_animator_.CrossFadeInFixedTime(AnimationConvert.GetAnimationName(now_animation_state_), 0.1f);
    }

    public bool GetIsPlaying()
    {
        return unit_animator_.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f ? true : false;
    }
}
