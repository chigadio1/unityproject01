﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトルユニット
/// </summary>
public class BattleUnit : BaseUnit
{
    /// <summary>
    /// UnitType
    /// </summary>
    public enum UnitType
    {
        None,
        Player,
        Enemy
    }

    /// <summary>
    /// ユニットタイプ
    /// </summary>
    protected UnitType unit_type_ = UnitType.None;
    public UnitType GetUnitType() { return unit_type_; }
    public void SetUnitType(UnitType type) { unit_type_ = type; }
    /// <summary>
    /// モーションパラメータ
    /// </summary>
    protected MotionParameter motion_parameter_ = new MotionParameter();
    /// <summary>
    /// モーションパラメーターにあるエフェクトIDリスト
    /// </summary>
    /// <returns></returns>
    public List<int> GetEffectIDList()
    {
        return motion_parameter_.GetEffectIDList();
    }

    /// <summary>
    /// モーションパラメータ作動中か
    /// </summary>
    /// <returns></returns>
    public bool GetIsMotionParameterPlay()
    {
        return motion_parameter_.GetIsPlay();
    }

    /// <summary>
    /// モーションパラメーターにあるBulletIDリスト
    /// </summary>
    /// <returns></returns>
    public List<int> GetBulletIDList()
    {
        return motion_parameter_.GetBulletIDList();
    }

    /// <summary>
    /// コリジョン
    /// </summary>
    [SerializeField]
    protected BaseCollisionConstruction collision_;

    /// <summary>
    /// コリジョンタイプ
    /// </summary>
    [SerializeField]
    protected CollisionLayer collision_type_ = CollisionLayer.kPlayer;
  
    /// <summary>
    /// コリジョン押し出しタイプ
    /// </summary>
    [SerializeField]
    protected CollisionPushType collision_push_type_ = CollisionPushType.kThisPush;

    /// <summary>
    /// 攻撃コリジョン
    /// </summary>
    protected BaseBattleCollisionGameSubstance attack_collision_ = null;

    /// <summary>
    /// フラグ管理
    /// </summary>
    protected FlagBattlePlayerUnitManager flag_battle_manager_ = new FlagBattlePlayerUnitManager();
    public FlagBattlePlayerUnitManager GetFlagBattleManager() { return flag_battle_manager_; }
    /// <summary>
    /// 入力アクション
    /// </summary>
    private BattleInputOperationManager input_operation_manager_ = new BattleInputOperationManager();

    /// <summary>
    /// 死亡フラグ
    /// </summary>
    public bool GetIsDeath() { return flag_battle_manager_.GetDeathFlag(); }

    /// <summary>
    /// Unitターゲット検索
    /// </summary>
    protected UnitTarget unit_target_search_ = new UnitTarget();
    /// <summary>
    /// ターゲットにしている敵
    /// </summary>
    protected BattleUnit target_unit_ = null;
    public BattleUnit GetTarget()
    {
        return target_unit_;
    }


    #region Data

    /// <summary>
    /// キャラクターステータスデータ
    /// </summary>
    protected BattleCharacterStatusData character_status_data_ = new BattleCharacterStatusData();



    public int GetMaxHP()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetMaxHP();
    }

    public int GetMaxMP()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetMaxMP();
    }

    public int GetBaseAttack()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetBaseAttack();
    }

    public int GetBaseSpeed()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetBaseSpeed();
    }
    public int GetBaseDefence()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetBaseDefence();
    }

    /// <summary>
    /// キャラクターHP
    /// </summary>
    /// <returns></returns>
    public int GetNowHP() { return character_status_data_.GetNowHP(); }
    public void AddHP(int value_add_hp)
    {
        character_status_data_.AddHP(value_add_hp);
    }

    /// <summary>
    /// キャラクターMP
    /// </summary>
    /// <returns></returns>
    public int GetNowMP() { return character_status_data_.GetNowMP(); }
    public void AddMP(int value_add_mp)
    {
        character_status_data_.AddMP(value_add_mp);
    }

    /// <summary>
    /// 攻撃力 
    /// </summary>
    /// <returns></returns>
    public int GetNowAttack() { return character_status_data_.GetNowAttack(); }
    public void StartAttackBuff(int value_buff_point, float value_life_frame)
    {
        character_status_data_.StartAttackBuff(value_buff_point, value_life_frame);
    }

    public void StartAttackDeBuff(int value_buff_point, float value_life_frame)
    {
        character_status_data_.StartAttackDeBuff(value_buff_point, value_life_frame);
    }

    /// <summary>
    /// 素早さ
    /// </summary>
    /// <returns></returns>
    public int GetNowSpeed() { return character_status_data_.GetNowSpeed(); }
    public void StartSpeedBuff(int value_buff_point, float value_life_frame)
    {
        character_status_data_.StartSpeedBuff(value_buff_point, value_life_frame);
    }

    public void StartSpeedDeBuff(int value_buff_point, float value_life_frame)
    {
        character_status_data_.StartSpeedDeBuff(value_buff_point, value_life_frame);
    }

    /// <summary>
    /// 防御力
    /// </summary>
    /// <returns></returns>
    public int GetNowDefence() { return character_status_data_.GetNowDefence(); }
    public void StartDefenceBuff(int value_buff_point, float value_life_frame)
    {
        character_status_data_.StartDefenceBuff(value_buff_point, value_life_frame);
    }

    public void StartDefenceDeBuff(int value_buff_point, float value_life_frame)
    {
        character_status_data_.StartDefenceDeBuff(value_buff_point, value_life_frame);
    }

    /// <summary>
    /// キャラデータセットアップ
    /// </summary>
    private bool is_chara_data_setup_ = false;

    /// <summary>
    /// プレイヤー操作キャラフラグ
    /// </summary>
    private bool is_player_ = false;
    #endregion

    #region AI

    /// <summary>
    /// AI
    /// </summary>
    private AI.BattleAIBrain ai_brain_ = new AI.BattleAIBrain();

    #endregion

    /// <summary>
    /// ノックバックセットアップ
    /// </summary>
    /// <param name="target_unit"></param>
    /// <param name="poitn"></param>
    public void KnockBackSetUp(BattleUnit target_unit, float point)
    {
        if (flag_battle_manager_.GetCharaAction()) return;

        
        if (point <= BattleDefinition.KnockBackSPoint()) return;
        if (GetIsDeath()) return;
        flag_battle_manager_.OnKnockBackFlag();

        motion_parameter_.SetTargetGameObject(target_unit.gameObject);
        motion_parameter_.PlayAction(MotionParameterConvert.MotionActionName.Damage_KnockBack_S);

    }

    // Start is called before the first frame update
    void Start()
    {
        motion_parameter_.Init(gameObject, $"Assets/Project/Assets/Data/MotionData/{GetUnitID():0000}/{GetUnitID():0000}_Combo.asset", this);
        ai_brain_.Init(this);

        unit_animation_ = new UnitAnimation();
        unit_animation_.Init(gameObject);

        flag_battle_manager_.Init();

        collision_ = new BaseCollisionConstruction();
        collision_.Init(gameObject, collision_type_, collision_push_type_, LayerMask.NameToLayer("Player"));


        motion_parameter_.SetTargetGameObject(target_unit_?.gameObject);
        unit_target_search_.Init(this);

        ChangeAnimation(AnimationConvert.AnimationState.Anim_Idle);

        if (unit_type_ == UnitType.Player) is_player_ = true;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public override void InitUnit(int value_character_id,int value_character_details_id = 1)
    {
        base.InitUnit(value_character_id, value_character_details_id);
    }

    // Update is called once per frame
    void Update()
    {
        if (KnockBackCheckUpdate() == true) return;
        if (GetIsDeath() == true) return;
        if (SetUp() == false) return;

        UpdateTarget();      
        motion_parameter_.Update(this);
        if (unit_type_ == UnitType.Enemy) ai_brain_.Update();
        if (unit_type_ == UnitType.Enemy) is_setup_ = motion_parameter_.GetIsSetUp() & ai_brain_.GetIsSetUp();
        else is_setup_ = motion_parameter_.GetIsSetUp();
        UpdateInput();

        BattleCore.Instance.BattlAreaCollisionCorrection(gameObject,is_player_);
        DeathCheckUpdate();
    }

    /// <summary>
    /// 敵検索
    /// </summary>
    protected void UpdateTarget()
    {
        if (KnockBackCheckUpdate() == true) return;
        if (GetIsDeath() == true) return;

        unit_target_search_.TargetUpdate(this);
        target_unit_ = unit_target_search_.GetTarget();
        motion_parameter_.SetTargetGameObject(target_unit_?.gameObject);
    }

    private void UpdateInput()
    {
        if (unit_type_ == UnitType.Enemy) return;
        input_operation_manager_.InputAction(this, flag_battle_manager_);
        input_operation_manager_.GameAction(this, flag_battle_manager_);
        flag_battle_manager_.Update();
    }

    /// <summary>
    /// 死亡☑
    /// </summary>
    protected void DeathCheckUpdate()
    {
        if (character_status_data_.GetNowHP() <= 0)
        {
            motion_parameter_.ActionEnd();
            unit_animation_.ChangeAnimation(AnimationConvert.AnimationState.Anim_Death);
            flag_battle_manager_.OnDeathFlag();
        }

    }

    /// <summary>
    /// ノックバック☑
    /// </summary>
    /// <returns></returns>
    protected bool KnockBackCheckUpdate()
    {
        if(flag_battle_manager_.GetKnockBackFlag())
        {
            if (motion_parameter_.GetIsPlay() == false)
            {
                flag_battle_manager_.OffKnockBackFlag();
                return true;
            }
        }

        return false;
    }

    protected virtual bool SetUp()
    {
        if (is_chara_data_setup_ == true) return true;

        var data = DataCore.Instance.GetCharacterStatusData(unit_id_, unit_details_id_);
        if (data == null) return false;
        is_chara_data_setup_ = true;
        character_status_data_.InitSetUp(data);
        return is_chara_data_setup_ & BattleCore.Instance.GetIsSetUp();
    }


    /// <summary>
    /// モーションパラメータ再生
    /// </summary>
    /// <param name="action_id"></param>
    public void PlayMotionAction(int action_id)
    {
        if (BattleCore.Instance.GetIsSetUp() == false) return;
        if (motion_parameter_.GetIsSetUp() == false) return;
        motion_parameter_.PlayAction(action_id - 1, true);
#if UNITY_EDITOR
        flag_battle_manager_.OnCharaActionSquareFlag();
        flag_battle_manager_.OnCharaActionTriangleFlag();
        flag_battle_manager_.OnCharaActionCrossFlag();
#endif
    }

    /// <summary>
    /// モーションパラメータ再生
    /// </summary>
    /// <param name="action_id"></param>
    public void PlayMotionAction(MotionParameterConvert.MotionActionName action_name,bool foreced = false)
    {
        if (BattleCore.Instance.GetIsSetUp() == false) return;
        if (motion_parameter_.GetIsSetUp() == false) return;
        motion_parameter_.PlayAction(action_name);
#if UNITY_EDITOR
        flag_battle_manager_.OnCharaActionSquareFlag();
        flag_battle_manager_.OnCharaActionTriangleFlag();
        flag_battle_manager_.OnCharaActionCrossFlag();
#endif
    }

    private void OnDestroy()
    {
        ai_brain_.Release();
        motion_parameter_.Release();
        var col = RpgCollisionControl.InstanceCheck;
        if (col != null) col.ErasureCollisionConstruction(collision_);
    }

}
