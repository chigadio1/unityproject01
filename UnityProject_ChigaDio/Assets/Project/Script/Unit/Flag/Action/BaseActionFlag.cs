﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ベースフラグアクション
/// </summary>
public class BaseActionFlag
{
    /// <summary>
    /// フラグ
    /// </summary>
    private bool is_flag_ = false;
    public void OnFlag() { is_flag_ = true; }
    public void OffFlag() { is_flag_ = false; }
    public bool GetFlag() { return is_flag_; }
    /// <summary>
    /// トリガーフラグ
    /// </summary>
    private bool is_trigger_flag_ = false;
    public void OnTriggerFlag() { is_trigger_flag_ = true; }
    public void OnTriggerFlag(float value_trigger_time) 
    {
        OnTriggerFlag();
        trigger_time_ = 0.0f;
        trigger_false_time_ = value_trigger_time;
    }
    public void OffTriggerFlag() { is_trigger_flag_ = false; }
    public bool GetTriggerFlag() { return is_trigger_flag_; }
    /// <summary>
    /// リピートフラグ
    /// </summary>
    private bool is_repeat_flag_ = false;
    public void OnRepeatFlag(float value_repeat_time = 0.0f)
    {
        is_repear_time_flag_ = true;
        repeat_true_time_ = value_repeat_time;
    }
    public bool GetRepeatFlag()
    {
        if (repeat_time_ >= repeat_true_time_)
        {
            is_repeat_flag_ = true;
            repeat_true_time_ = 0.0f;
        }
        else
        {
            is_repeat_flag_ = false;
        }
        return is_repeat_flag_;
    }
    /// <summary>
    /// リピートタイム計算フラグ
    /// </summary>
    private bool is_repear_time_flag_ = false;
    /// <summary>
    /// 計算時間
    /// </summary>
    private float trigger_time_ = 0.0f;


    /// <summary>
    /// 計算時間
    /// </summary>
    private float repeat_time_ = 0.0f;

    /// <summary>
    /// トリガーをFalseになる、時間
    /// </summary>
    private float trigger_false_time_ = 0.0f;

    /// <summary>
    /// Repeatになる、時間
    /// </summary>
    private float repeat_true_time_ = 0.0f;

    public void Init()
    {
        is_flag_ = is_repear_time_flag_ = is_repeat_flag_ = is_trigger_flag_ = false;
        repeat_time_ = repeat_true_time_ = trigger_false_time_ = trigger_time_ = 0.0f;
    }

    public void Update()
    {
        TriggerUpdate();
        RepeatUpdate();
    }

    void TriggerUpdate()
    {
        if (is_trigger_flag_ == false) return;

        trigger_time_ += Time.deltaTime;
        if (trigger_time_ >= trigger_false_time_)
        {
            trigger_time_ = 0.0f;
            is_trigger_flag_ = false;
        }
    }

    void RepeatUpdate()
    {
        if (is_repear_time_flag_ == false)
        {
            repeat_time_ = 0.0f;
            repeat_true_time_ = 0.0f;
            return;
        }
        is_repear_time_flag_ = false;

        repeat_time_ += Time.deltaTime;
    }

}
