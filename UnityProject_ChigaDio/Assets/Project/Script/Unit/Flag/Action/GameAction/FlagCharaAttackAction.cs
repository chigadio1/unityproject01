﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// キャラ攻撃フラグ
/// </summary>
public class FlagCharaAttackAction : BaseGameActionFlag
{
    /// <summary>
    /// 四角ボタン
    /// </summary>
    public BaseActionFlag GetActionSquareFlag() { return action_flag_; }

    /// <summary>
    /// 三角ボタン
    /// </summary>
    protected BaseActionFlag action_triangle_flag_ = new BaseActionFlag();
    public BaseActionFlag GetActionTriangleFlag() { return action_triangle_flag_; }

    /// <summary>
    /// バツボタン
    /// </summary>
    protected BaseActionFlag action_cross_flag_ = new BaseActionFlag();
    public BaseActionFlag GetActionCrossFlag() { return action_cross_flag_; }


    public override void Init()
    {
        action_flag_ = new BaseActionFlag();
        action_triangle_flag_ = new BaseActionFlag();
        action_cross_flag_ = new BaseActionFlag();
    }

    public override void Update()
    {
        action_flag_.Update();
        action_triangle_flag_.Update();
        action_cross_flag_.Update();
    }
}
