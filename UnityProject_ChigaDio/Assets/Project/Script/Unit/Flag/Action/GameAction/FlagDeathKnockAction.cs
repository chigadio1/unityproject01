﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 死亡とノックバックフラグ
/// </summary>
public class FlagDeathKnockAction : BaseGameActionFlag
{
    /// <summary>
    /// 死亡フラグ
    /// </summary>
    public BaseActionFlag GetDeathFlag() { return action_flag_; }

    /// <summary>
    /// ノックバック
    /// </summary>
    protected BaseActionFlag action_knock_back_flag_ = new BaseActionFlag();
    public BaseActionFlag GetKnockBackFlag() { return action_knock_back_flag_; }

    public override void Init()
    {
        action_flag_ = new BaseActionFlag();
        action_knock_back_flag_ = new BaseActionFlag();
    }

    public override void Update()
    {
        action_flag_.Update();
        action_knock_back_flag_.Update();
    }
}
