﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲーム中のアクション
/// </summary>
public class BaseGameActionFlag
{

    /// <summary>
    /// アクションフラグ
    /// </summary>
    protected BaseActionFlag action_flag_ = new BaseActionFlag();
    public BaseActionFlag GetActionFlag() { return action_flag_; }


    public virtual void Init()
    {
        action_flag_ = new BaseActionFlag();
    }

    public virtual  void Update()
    {
        action_flag_.Update();
    }

}
