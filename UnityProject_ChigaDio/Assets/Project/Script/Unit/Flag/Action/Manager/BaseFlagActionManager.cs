﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎フラグアクション管理
/// </summary>
public class BaseFlagActionManager
{
    /// <summary>
    /// ゲームアクションフラグリスト
    /// </summary>
    protected List<BaseGameActionFlag> list_game_action_flag_ = new List<BaseGameActionFlag>();

    #region　移動
    /// <summary>
    /// 移動フラグ
    /// </summary>
    protected FlagMoveAction flag_move_action_ = new FlagMoveAction();

    public void OnMoveFlag() { flag_move_action_?.GetActionFlag()?.OnFlag(); }
    public void OffMoveFlag() { flag_move_action_?.GetActionFlag()?.OffFlag(); }
    public bool GetMoveFlag() { return flag_move_action_.GetActionFlag().GetFlag(); }

    public void OnMoveTriggerFlag() { flag_move_action_?.GetActionFlag()?.OnTriggerFlag(); }
    public void OnMoveTriggerFlag(float value_trigger_false_time) { flag_move_action_?.GetActionFlag()?.OnTriggerFlag(value_trigger_false_time); }
    public void OffMoveTriggerFlag() { flag_move_action_?.GetActionFlag()?.OffTriggerFlag(); }
    public bool GetMoveTriggerFlag() { return flag_move_action_.GetActionFlag().GetTriggerFlag(); }

    public void OnMoveRepeatFlag() { flag_move_action_?.GetActionFlag()?.OnRepeatFlag(); }
    public void OnMoveRepeatFlag(float value_repeat_true_time) { flag_move_action_?.GetActionFlag()?.OnRepeatFlag(value_repeat_true_time); }
    public bool GetMoveRepeatFlag() { return flag_move_action_.GetActionFlag().GetRepeatFlag(); }

    #endregion

    #region キャラアクション

    /// <summary>
    /// キャラアクションフラグ
    /// </summary>
    protected FlagCharaAttackAction flag_chara_action_ = new FlagCharaAttackAction();

    public bool GetCharaAction() { return flag_chara_action_.GetActionCrossFlag().GetFlag() | flag_chara_action_.GetActionSquareFlag().GetFlag() | flag_chara_action_.GetActionTriangleFlag().GetFlag(); }
    public void OnCharaAction() { flag_chara_action_.GetActionCrossFlag().OnFlag(); flag_chara_action_.GetActionSquareFlag().OnFlag(); flag_chara_action_.GetActionTriangleFlag().OnFlag(); }
    public void OffCharaAction() { flag_chara_action_.GetActionCrossFlag().OffFlag(); flag_chara_action_.GetActionSquareFlag().OffFlag(); flag_chara_action_.GetActionTriangleFlag().OffFlag(); }

    public void OnCharaActionSquareFlag() { flag_chara_action_?.GetActionFlag()?.OnFlag(); }
    public void OffCharaActionSquareFlag() { flag_chara_action_?.GetActionFlag()?.OffFlag(); }
    public bool GetCharaActionSquareFlag() { return flag_chara_action_.GetActionFlag().GetFlag(); }

    public void OnCharaActionSquareTriggerFlag() { flag_chara_action_?.GetActionFlag()?.OnTriggerFlag(); }
    public void OnCharaActionSquareTriggerFlag(float value_trigger_false_time) { flag_chara_action_?.GetActionFlag()?.OnTriggerFlag(value_trigger_false_time); }
    public void OffCharaActionSquareTriggerFlag() { flag_chara_action_?.GetActionFlag()?.OffTriggerFlag(); }
    public bool GetCharaActionSquareTriggerFlag() { return flag_chara_action_.GetActionFlag().GetTriggerFlag(); }

    public void OnCharaActionSquareRepeatFlag() { flag_chara_action_?.GetActionFlag()?.OnRepeatFlag(); }
    public void OnCharaActionSquareRepeatFlag(float value_repeat_true_time) { flag_chara_action_?.GetActionFlag()?.OnRepeatFlag(value_repeat_true_time); }
    public bool GetCharaActionSquareRepeatFlag() { return flag_chara_action_.GetActionFlag().GetRepeatFlag(); }

    public void OnCharaActionTriangleFlag() { flag_chara_action_?.GetActionTriangleFlag()?.OnFlag(); }
    public void OffCharaActionTriangleFlag() { flag_chara_action_?.GetActionTriangleFlag()?.OffFlag(); }
    public bool GetCharaActionTriangleFlag() { return flag_chara_action_.GetActionTriangleFlag().GetFlag(); }

    public void OnCharaActionTriangleTriggerFlag() { flag_chara_action_?.GetActionTriangleFlag()?.OnTriggerFlag(); }
    public void OnCharaActionTriangleTriggerFlag(float value_trigger_false_time) { flag_chara_action_?.GetActionTriangleFlag()?.OnTriggerFlag(value_trigger_false_time); }
    public void OffCharaActionTriangleTriggerFlag() { flag_chara_action_?.GetActionTriangleFlag()?.OffTriggerFlag(); }
    public bool GetCharaActionTriangleTriggerFlag() { return flag_chara_action_.GetActionTriangleFlag().GetTriggerFlag(); }

    public void OnCharaActionTriangleRepeatFlag() { flag_chara_action_?.GetActionTriangleFlag()?.OnRepeatFlag(); }
    public void OnCharaActionTriangleRepeatFlag(float value_repeat_true_time) { flag_chara_action_?.GetActionTriangleFlag()?.OnRepeatFlag(value_repeat_true_time); }
    public bool GetCharaActionTriangleRepeatFlag() { return flag_chara_action_.GetActionTriangleFlag().GetRepeatFlag(); }

    public void OnCharaActionCrossFlag() { flag_chara_action_?.GetActionCrossFlag()?.OnFlag(); }
    public void OffCharaActionCrossFlag() { flag_chara_action_?.GetActionCrossFlag()?.OffFlag(); }
    public bool GetCharaActionCrossFlag() { return flag_chara_action_.GetActionCrossFlag().GetFlag(); }

    public void OnCharaActionCrossTriggerFlag() { flag_chara_action_?.GetActionCrossFlag()?.OnTriggerFlag(); }
    public void OnCharaActionCrossTriggerFlag(float value_trigger_false_time) { flag_chara_action_?.GetActionCrossFlag()?.OnTriggerFlag(value_trigger_false_time); }
    public void OffCharaActionCrossTriggerFlag() { flag_chara_action_?.GetActionCrossFlag()?.OffTriggerFlag(); }
    public bool GetCharaActionCrossTriggerFlag() { return flag_chara_action_.GetActionCrossFlag().GetTriggerFlag(); }

    public void OnCharaActionCrossRepeatFlag() { flag_chara_action_?.GetActionCrossFlag()?.OnRepeatFlag(); }
    public void OnCharaActionCrossRepeatFlag(float value_repeat_true_time) { flag_chara_action_?.GetActionCrossFlag()?.OnRepeatFlag(value_repeat_true_time); }
    public bool GetCharaActionCrossRepeatFlag() { return flag_chara_action_.GetActionCrossFlag().GetRepeatFlag(); }

    #endregion

    #region 死亡、ノックバック
    protected FlagDeathKnockAction flag_death_knock_action_ = new FlagDeathKnockAction();

    public bool GetDeathOrKnockBackFlag() { return flag_death_knock_action_.GetDeathFlag().GetFlag() | flag_death_knock_action_.GetKnockBackFlag().GetFlag(); }

    public void OnDeathFlag() { flag_death_knock_action_?.GetDeathFlag()?.OnFlag(); }
    public void OffDeathFlag() { flag_death_knock_action_?.GetDeathFlag()?.OffFlag(); }
    public bool GetDeathFlag() { return flag_death_knock_action_.GetDeathFlag().GetFlag(); }


    public void OnKnockBackFlag() { flag_death_knock_action_?.GetKnockBackFlag()?.OnFlag(); }
    public void OffKnockBackFlag() { flag_death_knock_action_?.GetKnockBackFlag()?.OffFlag(); }
    public bool GetKnockBackFlag() { return flag_death_knock_action_.GetKnockBackFlag().GetFlag(); }

    #endregion

    public virtual  void Init()
    {
        list_game_action_flag_.Add(flag_move_action_);
        list_game_action_flag_.Add(flag_chara_action_);
        list_game_action_flag_.Add(flag_death_knock_action_);
    }

    /// <summary>
    /// 更新処理
    /// </summary>
    public void Update()
    {
       foreach(var flag in list_game_action_flag_)
        {
            flag.Update();
        }
    }

    /// <summary>
    /// フラグ初期化
    /// </summary>
    public void AllFlagFalse()
    {
        foreach (var flag in list_game_action_flag_)
        {
            flag.GetActionFlag()?.Init();
        }
    }

}
