﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトル中のキャラステータス
/// </summary>
public class BattleCharacterStatusData
{
    /// <summary>
    /// キャラクターステータス
    /// </summary>
    private CharacterStatusGameData character_status_data_ = null;
    public int GetMaxHP()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetCharacterHP();
    }

    public int GetMaxMP()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetCharacterMP();
    }

    public int GetBaseAttack()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetCharacterAttack();
    }

    public int GetBaseSpeed()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetCharacterSpeed();
    }
    public int GetBaseDefence()
    {
        if (character_status_data_ == null) return 0;
        return character_status_data_.GetCharacterDefence();
    }


    /// <summary>
    /// 現在のHP
    /// </summary>
    private int now_character_hp_ = 0;
    public int GetNowHP() { return now_character_hp_; }
    public void AddHP(int value_add_hp)
    {
        now_character_hp_ += value_add_hp;
        now_character_hp_ = Mathf.Clamp(now_character_hp_, 0, character_status_data_.GetCharacterHP());
    }

    /// <summary>
    /// 現在のMP
    /// </summary>
    private int now_character_mp_ = 0;
    public int GetNowMP() { return now_character_mp_; }
    public void AddMP(int value_add_mp)
    {
        now_character_mp_ += value_add_mp;
        now_character_mp_ = Mathf.Clamp(now_character_mp_, 0, character_status_data_.GetCharacterMP());
    }

    /// <summary>
    /// 現在の攻撃力
    /// </summary>
    private int now_character_attack_ = 0;
    public int GetNowAttack() { return now_character_attack_ + attack_buff_control_.GetAddBuffPoint() + attack_buff_control_.GetAddDeBuffPoint(); }

    /// <summary>
    /// 現在の素早さ
    /// </summary>
    private int now_character_speed_ = 0;
    public int GetNowSpeed() { return now_character_speed_ + speed_buff_control_.GetAddBuffPoint() + speed_buff_control_.GetAddDeBuffPoint(); }

    /// <summary>
    /// 現在の防御力
    /// </summary>
    private int now_character_defence_ = 0;
    public int GetNowDefence() { return now_character_defence_ + defence_buff_control_.GetAddBuffPoint() + defence_buff_control_.GetAddDeBuffPoint(); }

    /// <summary>
    /// 攻撃バフ
    /// </summary>
    private BattleCharasterBuffControlData attack_buff_control_ = new BattleCharasterBuffControlData();

    public void StartAttackBuff(int value_buff_point, float value_life_frame)
    {
        attack_buff_control_.StartBuff(value_buff_point, value_life_frame);
    }

    public void StartAttackDeBuff(int value_buff_point, float value_life_frame)
    {
        attack_buff_control_.StartDeBuff(value_buff_point, value_life_frame);
    }


    /// <summary>
    /// 素早さバフ
    /// </summary>
    private BattleCharasterBuffControlData speed_buff_control_ = new BattleCharasterBuffControlData();
    public void StartSpeedBuff(int value_buff_point, float value_life_frame)
    {
        speed_buff_control_.StartBuff(value_buff_point, value_life_frame);
    }

    public void StartSpeedDeBuff(int value_buff_point, float value_life_frame)
    {
        speed_buff_control_.StartDeBuff(value_buff_point, value_life_frame);
    }

    /// <summary>
    /// 防御バフ
    /// </summary>
    private BattleCharasterBuffControlData defence_buff_control_ = new BattleCharasterBuffControlData();
    public void StartDefenceBuff(int value_buff_point, float value_life_frame)
    {
        defence_buff_control_.StartBuff(value_buff_point, value_life_frame);
    }

    public void StartDefenceDeBuff(int value_buff_point, float value_life_frame)
    {
        defence_buff_control_.StartDeBuff(value_buff_point, value_life_frame);
    }

    public void InitSetUp(CharacterStatusGameData value_data)
    {
        character_status_data_ = value_data;

        now_character_hp_ = character_status_data_.GetCharacterHP();
        now_character_mp_ = character_status_data_.GetCharacterMP();

        now_character_attack_ = character_status_data_.GetCharacterAttack();
        now_character_speed_ = character_status_data_.GetCharacterSpeed();
        now_character_defence_ = character_status_data_.GetCharacterDefence();
    }

    public void Update()
    {
        BuffUpdate();
    }

    private void BuffUpdate()
    {
        attack_buff_control_.Update();
        speed_buff_control_.Update();
        defence_buff_control_.Update();
    }
}
