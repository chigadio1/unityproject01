﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトル中のバフステータス
/// </summary>
public class BaseBattleCharacterStatusBuffData
{
    /// <summary>
    /// 生存フレーム
    /// </summary>
    protected float life_frame_ = 0.0f;

    /// <summary>
    /// 計算時間
    /// </summary>
    protected float time_ = 0.0f;

    /// <summary>
    /// 追加するバフポイント
    /// </summary>
    protected int add_buff_point_ = 0;
    public int GetAddBuffPoint() { return add_buff_point_; }

    /// <summary>
    /// タイムオーバーフラグ
    /// </summary>
    /// <returns></returns>
    public bool GetBuffTimeOver() { return time_ >= life_frame_ ? true : false; }

    public void Init()
    {
        add_buff_point_ = 0;
        life_frame_ = time_ = 0.0f;
    }

    public void StartBuff(int value_buff_point,float value_life_frame)
    {
        Init();
        life_frame_ = value_life_frame * (1.0f / 60.0f);
        add_buff_point_ = value_buff_point;
    }

    public void Update()
    {
        time_ += Time.deltaTime;

        if (GetBuffTimeOver()) Init();
    }
}
