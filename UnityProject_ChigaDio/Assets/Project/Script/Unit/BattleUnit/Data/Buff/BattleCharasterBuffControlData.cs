﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バグコントロールデータ
/// </summary>
public class BattleCharasterBuffControlData
{

    /// <summary>
    /// バフデータ
    /// </summary>
    private BaseBattleCharacterStatusBuffData buff_data_ = new BaseBattleCharacterStatusBuffData();
    public int GetAddBuffPoint() { return buff_data_.GetAddBuffPoint(); }
    public bool GetBuffTimeOver() { return buff_data_.GetBuffTimeOver(); }

    /// <summary>
    /// デバフデータ
    /// </summary>
    private BaseBattleCharacterStatusBuffData de_buff_data_ = new BaseBattleCharacterStatusBuffData();
    public int GetAddDeBuffPoint() { return de_buff_data_.GetAddBuffPoint(); }
    public bool GetDeBuffTimeOver() { return de_buff_data_.GetBuffTimeOver(); }

    public void BuffInit()
    {
        buff_data_.Init();
    }

    public void DeBuffInit()
    {
        de_buff_data_.Init();
    }

    public void AllBuffInit()
    {
        BuffInit();
        DeBuffInit();
    }

    public void StartBuff(int value_buff_point, float value_life_frame)
    {
        buff_data_.StartBuff(value_buff_point, value_life_frame);
    }

    public void StartDeBuff(int value_buff_point, float value_life_frame)
    {
        de_buff_data_.StartBuff(value_buff_point, value_life_frame);
    }

    public void Update()
    {
        buff_data_.Update();
        de_buff_data_.Update();
    }
}
