﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎Unit
/// </summary>
public class BaseUnit : MonoBehaviour
{
    /// <summary>
    /// アニメーションユニット
    /// </summary>
    protected UnitAnimation unit_animation_ = new UnitAnimation();



    /// <summary>
    /// ユニットID
    /// </summary>
    [SerializeField]
    protected int unit_id_ = 1;
    public  int GetUnitID() { return unit_id_; }

    /// <summary>
    /// ユニット詳細ID
    /// </summary>
    [SerializeField]
    protected int unit_details_id_ = 1;
    public int GetUnitDetailsID() { return unit_details_id_; }

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    protected bool is_setup_ = false;
    public virtual bool GetIsSetUp() { return is_setup_; }

    protected bool is_stop_ = false;
    public virtual void OnStop() 
    {
        is_stop_ = true;
        unit_animation_.OnStop();
    }
    public virtual void OffStop()
    {
        is_stop_ = false;
        unit_animation_.OnPlay();
    }

    /// <summary>
    /// アニメーション切り替え
    /// </summary>
    /// <param name="next_state"></param>
    /// <param name="forced"></param>
    public void ChangeAnimation(AnimationConvert.AnimationState next_state, bool forced = false)
    {
        if (unit_animation_ == null) return;
        unit_animation_.ChangeAnimation(next_state,forced);
    }

    public bool GetIsAnimationPlay()
    {
        return unit_animation_.GetIsPlaying();
    }

    /// <summary>
    /// 初期化
    /// </summary>
    public virtual void InitUnit(int value_character_id,int value_character_details_id = 1)
    {
        unit_id_ = value_character_id;
        unit_details_id_ = value_character_details_id;
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }


}
