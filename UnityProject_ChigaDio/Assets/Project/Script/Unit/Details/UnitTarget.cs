﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
/// <summary>
/// バトル中のUnit検索(距離)
/// </summary>
public class UnitTarget
{
    /// <summary>
    /// ターゲットリスト(距離が近い順)
    /// </summary>
    private List<BattleUnit> list_target_unit_ = new List<BattleUnit>();
    private BattleUnit[] array_target_unit_ = null;
    public BattleUnit GetTarget()
    {
        if (array_target_unit_ == null) return null;
        if (array_target_unit_.Length == 0) return null;
        return array_target_unit_.First();
    }
    public List<BattleUnit> GetTargetAll()
    {
        return array_target_unit_.ToList();
    }

    /// <summary>
    /// 時間測定
    /// </summary>
    private float time_ = 0.0f;
    public bool GetIsTargetUpdate() { return time_ >= 1.0f/BattleDefinition.GetMaxTargetSetUpTime(); }

    public void Init(BattleUnit self_unit)
    {
        if (self_unit.GetUnitType() == BattleUnit.UnitType.Enemy) list_target_unit_ = BattleCore.Instance.GetBattleMemberControl().GetPlayerMemberUnit();
        else list_target_unit_ = BattleCore.Instance.GetBattleMemberControl().GetEnemyMemberUnit();
    }

    public void TargetUpdate(BattleUnit self_unit)
    {

        TargetSetUp(self_unit);
    }

    /// <summary>
    /// ターゲットのセットアップ
    /// </summary>
    /// <param name="self_unit"></param>
    /// <param name="list_unit"></param>
    private void TargetSetUp(BattleUnit self_unit)
    {
        if (self_unit.GetFlagBattleManager().GetCharaAction()) return;
        if (self_unit.GetIsDeath()) return;
        if (self_unit.GetFlagBattleManager().GetKnockBackFlag()) return;

        time_ += Time.deltaTime;

        if (GetIsTargetUpdate() == false) return;

        Vector3 position = self_unit.transform.position;
        array_target_unit_ =  list_target_unit_.ToArray().OrderBy(target => Vector3.Distance(target.transform.position, position)).ToArray();

    }
}
