﻿using ProjectSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベル会話データ
/// </summary>
[System.Serializable]
public class NovelTalkData : BaseComposition
{
    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// ID
    /// </summary>d
    [SerializeField]
    private int id_ = 0;
    public int GetID() { return id_; }

    /// <summary>
    /// コマンドテキスト
    /// </summary>
    [SerializeField]
    private string command_text_ = "";
    public string GetCommandText() { return command_text_; }

    /// <summary>
    /// 自動ページ
    /// </summary>
    [SerializeField]
    private bool is_auto_page_ = false;
    public bool GetIsAutoPage() { return is_auto_page_; }

    /// <summary>
    /// クリック操作
    /// </summary>
    [SerializeField]
    private bool is_click_ = true;
    public bool GetIsClick() { return is_click_; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(id_), id_);
        data_group.AddData("TextCommand", nameof(command_text_), command_text_);
        data_group.AddData("AutoPage", nameof(is_auto_page_), is_auto_page_);
        data_group.AddData("Click", nameof(is_click_), is_click_);
    }
}
