﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(NovelTalkDataScriptable))]
public class EditorNovelTalkDataScriptable : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<NovelTalkData>(target as BaseExcelScriptableObject<NovelTalkData>);
        base.OnInspectorGUI();
    }
}


#endif

/// <summary>
/// ノベル会話データ
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/NovelTalkData")]
public class NovelTalkDataScriptable : BaseExcelScriptableObject<NovelTalkData>
{

}
