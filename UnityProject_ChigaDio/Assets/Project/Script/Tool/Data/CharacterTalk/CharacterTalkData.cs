﻿using System.Collections;
using System.Collections.Generic;
using ProjectSystem;
using UnityEngine;

/// <summary>
/// 会話データ
/// </summary>
[System.Serializable]
public class CharacterTalkData : BaseComposition
{
    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// キャラクターID
    /// </summary>d
    [SerializeField]
    private int character_id_ = 0;
    public int GetCharacterID() { return character_id_; }

    /// <summary>
    /// キャラクター表情Type
    /// </summary>
    [SerializeField]
    private GameUIDefinition.CharacterFaceType character_face_type_ = GameUIDefinition.CharacterFaceType.Amimia;
    public GameUIDefinition.CharacterFaceType GetCharacterFaceType() { return character_face_type_; }

    /// <summary>
    /// UIエフェクトID
    /// </summary>
    [SerializeField]
    private int ui_effect_id_ = 0;
    public  int GetUIEffectID() { return ui_effect_id_; }

    /// <summary>
    /// キャラクター名
    /// </summary>
    [SerializeField]
    private string character_name_ = "";
    public string GetCharacterName() { return character_name_; }

    /// <summary>
    /// キャラクターセリフ
    /// </summary>
    [SerializeField]
    private string character_dialogue_ = "";
    public string GetCharacterDialogue() { return character_dialogue_; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("キャラクターID", nameof(character_id_), character_id_);
        data_group.AddData("表情Type", nameof(character_face_type_), character_face_type_);
        data_group.AddData("UIEffectID", nameof(ui_effect_id_), ui_effect_id_);
        data_group.AddData("名前", nameof(character_name_), character_name_);
        data_group.AddData("セリフ", nameof(character_dialogue_), character_dialogue_);
    }


}
