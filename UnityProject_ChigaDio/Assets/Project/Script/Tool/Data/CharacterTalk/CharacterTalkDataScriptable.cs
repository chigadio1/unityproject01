﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(CharacterTalkDataScriptable))]
public class EditorCharaterTalkDataScriptable : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<CharacterTalkData>(target as BaseExcelScriptableObject<CharacterTalkData>);
        base.OnInspectorGUI();
    }
}


#endif


/// <summary>
/// キャラクター会話
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreateCharacterTalkData")]
public class CharacterTalkDataScriptable : BaseExcelScriptableObject<CharacterTalkData>
{

}
