﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// キャラクターステータスの全保管クラス
/// </summary>
public class CharacterStatusDataStorageManager
{
    /// <summary>
    /// 保管辞書
    /// </summary>
    private Dictionary<int, CharacterStatusStorage> dictionary_character_status_storage_ = new Dictionary<int, CharacterStatusStorage>();

    /// <summary>
    /// アドレサブル
    /// </summary>
    private Dictionary<int, AddressableData<CharacterStatusDataScriptable>> dictionary_addressable_character_status_data_ = new Dictionary<int, AddressableData<CharacterStatusDataScriptable>>();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = true;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// キャラクターデータ取得
    /// </summary>
    /// <param name="search_details_id"></param>
    /// <returns></returns>
    public CharacterStatusGameData GetCharacterStatusData(int search_unit_id,int search_details_id)
    {
        if (dictionary_character_status_storage_.ContainsKey(search_unit_id) == false) return null;

        return dictionary_character_status_storage_[search_unit_id].GetCharacterStatusData(search_details_id);
    }

    /// <summary>
    /// ロード
    /// </summary>
    /// <param name="unit_id"></param>
    public void LoadCharacterStatusData(int unit_id)
    {
        if (dictionary_addressable_character_status_data_.ContainsKey(unit_id) == true) return;
        if (dictionary_character_status_storage_.ContainsKey(unit_id) == true) return;
        is_setup_ = false;
        AddressableData<CharacterStatusDataScriptable> addressable_data = new AddressableData<CharacterStatusDataScriptable>();
        addressable_data.LoadStart($"Assets/Project/Assets/CharacterStatus/{unit_id:0000}/{unit_id:0000}.asset");

        dictionary_addressable_character_status_data_.Add(unit_id, addressable_data);
    }

    public void Update()
    {
        SetUp();
    }

    private void SetUp()
    {
        if (is_setup_ == true) return;

        is_setup_ = true;

        foreach (var data in dictionary_addressable_character_status_data_)
        {
            if (data.Value.GetFlagSetUpLoading() == false)
            {
                is_setup_ = false;
                return;
            }
        }

        foreach (var data in dictionary_addressable_character_status_data_)
        {
            CharacterStatusStorage storage = new CharacterStatusStorage();
            storage.AddCharacterStatusGameData(data.Value.GetAddressableData().GetListData());

            dictionary_character_status_storage_.Add(data.Key, storage);

            data.Value.Release();
        }

        dictionary_addressable_character_status_data_.Clear();
    }

}
