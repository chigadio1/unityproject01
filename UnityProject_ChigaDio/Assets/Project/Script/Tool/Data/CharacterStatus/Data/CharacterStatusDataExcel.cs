﻿using System.Collections;
using System.Collections.Generic;
using ProjectSystem;
using UnityEngine;

/// <summary>
/// キャラステータスExcelデータ
/// </summary>
[System.Serializable]
public class CharacterStatusDataExcel : BaseComposition
{
    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// キャラ詳細ID
    /// </summary>
    [SerializeField]
    private int character_details_id_ = 0;
    public int GetCharacterDetailsID() { return character_details_id_;}

    /// <summary>
    /// キャラネーム
    /// </summary>
    [SerializeField]
    private string character_name_ = "";
    public string GetCharacterName() { return character_name_; }

    /// <summary>
    /// HP
    /// </summary>
    [SerializeField]
    private int character_hp_ = 0;
    public int GetCharacterHP() { return character_hp_; }

    /// <summary>
    /// MP
    /// </summary>
    [SerializeField]
    private int character_mp_ = 0;
    public int GetCharacterMP() { return character_mp_; }

    /// <summary>
    /// 攻撃力
    /// </summary>
    [SerializeField]
    private int character_attack_ = 0;
    public int GetCharacterAttack() { return character_attack_; }

    /// <summary>
    /// 速さ
    /// </summary>
    [SerializeField]
    private int character_speed_ = 0;
    public int GetCharacterSpeed() { return character_speed_; }

    /// <summary>
    /// 防御
    /// </summary>
    [SerializeField]
    private int character_defence_ = 0;
    public int GetCharacterDefence() { return character_defence_; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(character_details_id_), character_details_id_);
        data_group.AddData("Name", nameof(character_name_), character_name_);
        data_group.AddData("HP", nameof(character_hp_), character_hp_);
        data_group.AddData("MP", nameof(character_mp_), character_mp_);
        data_group.AddData("Attack", nameof(character_attack_), character_attack_);
        data_group.AddData("Speed", nameof(character_speed_), character_speed_);
        data_group.AddData("Defence", nameof(character_defence_), character_defence_);
    }
}
