﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(CharacterStatusDataScriptable))]
public class EditorCharaterStatusDataScriptable : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<CharacterStatusDataExcel>(target as BaseExcelScriptableObject<CharacterStatusDataExcel>);
        base.OnInspectorGUI();
    }
}


#endif

/// <summary>
/// キャラクターデータ
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreateCharacterStatusData")]
public class CharacterStatusDataScriptable : BaseExcelScriptableObject<CharacterStatusDataExcel>
{
}
