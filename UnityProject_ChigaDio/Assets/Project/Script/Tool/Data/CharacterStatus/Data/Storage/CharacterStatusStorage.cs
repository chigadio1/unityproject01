﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// キャラステータスストレージ
/// </summary>
public class CharacterStatusStorage
{
    /// <summary>
    /// 辞書型キャラクターステータスデータ
    /// </summary>
    private Dictionary<int, CharacterStatusGameData> dictionary_character_status_data_ = new Dictionary<int, CharacterStatusGameData>();

    /// <summary>
    /// 追加
    /// </summary>
    /// <param name="list_data"></param>
    public void AddCharacterStatusGameData(List<CharacterStatusDataExcel> list_data)
    {
        foreach(var data in list_data)
        {
            if (dictionary_character_status_data_.ContainsKey(data.GetCharacterDetailsID()) == true) continue;

            CharacterStatusGameData game_data = new CharacterStatusGameData();
            game_data.SetValue(data);
            dictionary_character_status_data_.Add(data.GetCharacterDetailsID(),game_data);
        }
    }

    /// <summary>
    /// キャラクターデータ取得
    /// </summary>
    /// <param name="search_details_id"></param>
    /// <returns></returns>
    public CharacterStatusGameData GetCharacterStatusData(int search_details_id)
    {
        if (dictionary_character_status_data_.ContainsKey(search_details_id) == false) return null;

        return dictionary_character_status_data_[search_details_id];
    }
}
