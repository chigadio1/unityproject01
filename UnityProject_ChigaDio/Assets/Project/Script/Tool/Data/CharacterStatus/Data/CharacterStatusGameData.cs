﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲーム中のキャラステータス
/// </summary>
public class CharacterStatusGameData
{

    /// <summary>
    /// インデックス
    /// </summary>
    
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// キャラ詳細ID
    /// </summary>
    
    private int character_details_id_ = 0;
    public int GetCharacterDetailsID() { return character_details_id_; }

    /// <summary>
    /// キャラネーム
    /// </summary>
    
    private string character_name_ = "";
    public string GetCharacterName() { return character_name_; }

    /// <summary>
    /// HP
    /// </summary>
    
    private int character_hp_ = 0;
    public int GetCharacterHP() { return character_hp_; }

    /// <summary>
    /// MP
    /// </summary>
    
    private int character_mp_ = 0;
    public int GetCharacterMP() { return character_mp_; }

    /// <summary>
    /// 攻撃力
    /// </summary>
    
    private int character_attack_ = 0;
    public int GetCharacterAttack() { return character_attack_; }

    /// <summary>
    /// 速さ
    /// </summary>
    
    private int character_speed_ = 0;
    public int GetCharacterSpeed() { return character_speed_; }

    /// <summary>
    /// 防御
    /// </summary>
    
    private int character_defence_ = 0;
    public int GetCharacterDefence() { return character_defence_; }

    public void SetValue(CharacterStatusDataExcel data)
    {
        index_ = data.GetIndex();
        character_details_id_ = data.GetCharacterDetailsID();
        character_name_ = data.GetCharacterName();
        character_hp_ = data.GetCharacterHP();
        character_mp_ = data.GetCharacterMP();
        character_attack_ = data.GetCharacterAttack();
        character_speed_ = data.GetCharacterSpeed();
        character_defence_ = data.GetCharacterDefence();
    }
}
