﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// データコア
/// </summary>
public class DataCore : Singleton<DataCore>
{
    /// <summary>
    /// 辞書集約
    /// </summary>
    private DictionaryIntensive dictionary_intensive_ = new DictionaryIntensive();

    /// <summary>
    /// キャラクターステータス
    /// </summary>
    private CharacterStatusDataStorageManager character_status_storage_ = new CharacterStatusDataStorageManager();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_ & character_status_storage_.GetIsSetUp(); }

    /// <summary>
    /// 辞書セットアップフラグ
    /// </summary>
    private bool is_dictionary_setup_ = false;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "DataCore";

        dictionary_intensive_.Init();
    }

    // Update is called once per frame
    void Update()
    {

        character_status_storage_.Update();
        if (is_setup_ == true) return;

        SetUpDictionary();

        is_setup_ = is_dictionary_setup_  & character_status_storage_.GetIsSetUp();
    }

    public void ReleaseAllDictionary()
    {
        if (is_setup_ == false) return;

        dictionary_intensive_.ReleaseAll();
    }

    public void OnDestroy()
    {
        ReleaseAllDictionary();
    }

    /// <summary>
    /// セットアップ辞書
    /// </summary>
    void SetUpDictionary()
    {
        if (is_dictionary_setup_ == true) return;

        dictionary_intensive_.Update();

        is_dictionary_setup_ = dictionary_intensive_.GetIsSetUp();
    }

    /// <summary>
    /// キャラクターデータ取得
    /// </summary>
    /// <param name="search_details_id"></param>
    /// <returns></returns>
    public CharacterStatusGameData GetCharacterStatusData(int search_unit_id, int search_details_id)
    {
        var data = character_status_storage_.GetCharacterStatusData(search_unit_id, search_details_id);
        if (data == null) LoadCharacterStatusData(search_unit_id);
        return data;
    }

    /// <summary>
    /// ロード
    /// </summary>
    /// <param name="unit_id"></param>
    public void LoadCharacterStatusData(int unit_id)
    {
        character_status_storage_.LoadCharacterStatusData(unit_id);
    }

    /// <summary>
    /// データリスト Get
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public List<DictionaryData> GetListDictionaryData(DictionaryType.Type type)
    {
        return dictionary_intensive_.GetListDictionaryData(type);
    }

    /// <summary>
    /// キャラクタ-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetCharacterDictionaryData(int search_id)
    {
        return dictionary_intensive_.GetCharacterDictionaryData(search_id);
    }

    /// <summary>
    /// キャラクタ-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetCharacterDictionaryData(string search_name)
    {
        return dictionary_intensive_.GetCharacterDictionaryData(search_name);
    }

    /// <summary>
    /// Effect-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetEffectDictionaryData(int search_id)
    {
        return dictionary_intensive_.GetEffectDictionaryData(search_id);
    }

    /// <summary>
    /// Effect-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetEffectDictionaryData(string search_name)
    {
        return dictionary_intensive_.GetEffectDictionaryData(search_name);
    }

    /// <summary>
    /// Animation-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetAnimationDictionaryData(int search_id)
    {
        return dictionary_intensive_.GetAnimationDictionaryData(search_id);
    }

    /// <summary>
    /// Animation-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetAnimationDictionaryData(string search_name)
    {
        return dictionary_intensive_.GetAnimationDictionaryData(search_name);
    }

    /// <summary>
    /// MotionParameterAction-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetMotionParameterActionDictionaryData(int search_id)
    {
        return dictionary_intensive_.GetMotionParameterActionDictionaryData(search_id);
    }

    /// <summary>
    /// MotionParameterAction-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetMotionParameterActionDictionaryData(string search_name)
    {
        return dictionary_intensive_.GetMotionParameterActionDictionaryData(search_name);
    }

    /// <summary>
    /// MotionParameterAction 全名前取得
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public List<string> GetMotionParameterActionDictionnaryName()
    {
        return dictionary_intensive_.GetMotionParameterActionDictionnaryName(DictionaryType.Type.MotionParameterAction);
    }

    /// <summary>
    /// Sound-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetSoundDictionaryData(int search_id)
    {
        return dictionary_intensive_.GetSoundDictionaryData(search_id);
    }

    /// <summary>
    /// Sound-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetSoundDictionaryData(string search_name)
    {
        return dictionary_intensive_.GetSoundDictionaryData(search_name);
    }

    /// <summary>
    /// Sound 全名前取得
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public List<string> GetSoundDictionnaryName()
    {
        return dictionary_intensive_.GetSoundDictionnaryName(DictionaryType.Type.Sound);
    }
}
