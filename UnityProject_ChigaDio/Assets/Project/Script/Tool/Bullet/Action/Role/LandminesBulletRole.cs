﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 地雷弾丸
/// </summary>
public class LandminesBulletRole : BaseBulletRole
{
    public LandminesBulletRole()
    {
        bullet_type_ = BulletDataExcel.BulletType.Landmines;
    }


}
