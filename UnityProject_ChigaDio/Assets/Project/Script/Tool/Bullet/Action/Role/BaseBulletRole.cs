﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎弾丸動作クラス
/// </summary>
public class BaseBulletRole
{
    /// <summary>
    /// 弾丸タイプ
    /// </summary>
    protected BulletDataExcel.BulletType bullet_type_ = BulletDataExcel.BulletType.None;

    /// <summary>
    /// 生存フレーム
    /// </summary>
    protected float life_frmae_ = 0.0f;

    /// <summary>
    /// フレーム経過
    /// </summary>
    protected float time_ = 0.0f;

    /// <summary>
    /// 弾丸Transform
    /// </summary>
    protected Transform bullet_transform_ = null;

    /// <summary>
    /// 攻撃コリジョン役割
    /// </summary>
    protected BulletDataExcel.BulletCollisionRole attack_collision_role_ = BulletDataExcel.BulletCollisionRole.None;

    /// <summary>
    /// 攻撃コリジョンがあたったとき、呼び出す弾丸ID
    /// </summary>
    protected int attack_hit_collision_next_bullet_id_ = 0;

    /// <summary>
    /// 背景コリジョン役割
    /// </summary>
    protected BulletDataExcel.BulletCollisionRole background_collision_role_ = BulletDataExcel.BulletCollisionRole.None;

    /// <summary>
    /// 背景コリジョンがあたったとき、呼び出す弾丸ID
    /// </summary>
    protected int background_hit_collision_next_bullet_id_ = 0;

    public BaseBulletRole()
    {

    }

    /// <summary>
    /// 初期化
    /// </summary>
    public virtual void Init(BulletGameData data,BattleUnit unit,Transform bullet_transform)
    { 
        bullet_transform_ = bullet_transform;
        bullet_type_ = data.GetBulletType();
        life_frmae_ = data.GetLifeFrame();

        attack_collision_role_ = data.GetBulletAttackCollisionRole();
        attack_hit_collision_next_bullet_id_ = data.GetBulletHitAttackCollisionNextID();

        background_collision_role_ = data.GetBulletBackGroundCollisionRole();
        background_hit_collision_next_bullet_id_ = data.GetBulletHitBackGroundCollisionNextID();

        Start(unit,data);
    }

    protected virtual void Start(BattleUnit unit, BulletGameData data)
    {

    }

    /// <summary>
    /// 更新処理
    /// </summary>
    public virtual void Update(BattleUnit unit)
    {
        KeyFrameUpdate();
        if (GetIsDestroy()) return;
        RoleUpdate(unit);
    }

    protected virtual void KeyFrameUpdate()
    {
        time_ += (1.0f / 60.0f);
    }

    public bool GetIsDestroy()
    {
        return time_ >= life_frmae_ ? true : false;
    }

    /// <summary>
    /// 更新処理（役割）
    /// </summary>
    protected virtual void RoleUpdate(BattleUnit unit)
    {

    }
}
