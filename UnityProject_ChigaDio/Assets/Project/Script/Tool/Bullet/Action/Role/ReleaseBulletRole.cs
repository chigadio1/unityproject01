﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 放出弾丸
/// </summary>
public class ReleaseBulletRole : BaseBulletRole
{
    /// <summary>
    /// ゴールポジションVector
    /// </summary>
    private Vector3 goal_position_vector_ = Vector3.one;
    /// <summary>
    /// 弾丸の速さ
    /// </summary>
    private float bullet_speed_ = 0.0f;
    protected override void Start(BattleUnit unit,BulletGameData data)
    {
        if (unit.GetTarget() == null) goal_position_vector_ = ((unit.transform.position + unit.transform.forward * 10.0f) - unit.transform.position).normalized;
        else goal_position_vector_ = (unit.GetTarget().transform.position - unit.transform.position).normalized;
        bullet_speed_ = data.GetFloat01();

        bullet_transform_.rotation = Quaternion.LookRotation(((bullet_transform_.position + goal_position_vector_) - bullet_transform_.transform.position));
    }

    protected override void RoleUpdate(BattleUnit unit)
    {
        bullet_transform_.position += goal_position_vector_ * bullet_speed_;
    }

    public ReleaseBulletRole()
    {
        bullet_type_ = BulletDataExcel.BulletType.Release;
    }
}
