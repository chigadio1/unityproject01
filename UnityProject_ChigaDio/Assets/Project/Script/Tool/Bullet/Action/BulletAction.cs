﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAction : MonoBehaviour
{
    /// <summary>
    /// コリジョンデータ
    /// </summary>
    private List<BaseBulletCollisionDetails> list_battle_collision_details_ = new List<BaseBulletCollisionDetails>();

    /// <summary>
    /// 親オブジェクト
    /// </summary>
    private GameObject parent_obj_ = null;

    /// <summary>
    /// 弾丸動作
    /// </summary>
    private BaseBulletRole bullet_role_ = null;

    /// <summary>
    /// 所有者
    /// </summary>
    private BattleUnit self_battle_unit_ = null;

    /// <summary>
    /// スキルデータ
    /// </summary>
    private SkillGameData skill_data_ = null;

    /// <summary>
    /// 攻撃ヒット
    /// </summary>
    private bool is_attack_hit_ = false;
    public void SetUp(BattleUnit unit,GameObject parent_object, BulletGameData bullet_data ,List<BattleCollisionGameData> list_collision_game_data)
    {
        parent_obj_ = parent_object;
        var collision_object = new GameObject();
        collision_object.name = "Collision";
        collision_object.layer = LayerMask.NameToLayer("Player");
        self_battle_unit_ = unit;
        foreach (var collision_data in list_collision_game_data)
        {
            var collider =  collision_object.AddComponent<SphereCollider>();

            var bullet_data_details_ = new BaseBulletCollisionDetails();
            bullet_data_details_.InitSetUp(collider, bullet_data,collision_data);
            list_battle_collision_details_.Add(bullet_data_details_);
        }
        collision_object.transform.SetParent(gameObject.transform);
        collision_object.transform.localPosition = Vector3.zero;
        BaseCollisionConstruction collision_game = new BaseCollisionConstruction();
        collision_game.Init(gameObject,CollisionLayer.kBullet,CollisionPushType.kNone,LayerMask.NameToLayer("Player"));
        collision_game.RpgCollisionDetailsControl.HitSecondFunc = HitSecond;
        gameObject.transform.SetParent(parent_object.transform);

        bullet_role_ = BulletCreate.Appearance(bullet_data.GetBulletType());
        bullet_role_.Init(bullet_data,unit,transform);

        skill_data_ = SkillCore.Instance.GetSkillGameData(bullet_data.GetBulletSkillID());
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bullet_role_.Update(self_battle_unit_);
        bool flag_destroy = true;
        flag_destroy = flag_destroy = bullet_role_.GetIsDestroy();

        if (flag_destroy == true || is_attack_hit_ == true)
        {
            GameObject.Destroy(gameObject);
            GameObject.Destroy(parent_obj_);    
        }
    }

    void HitSecond(Collider collider,Collider this_collider)
    {
        var unit = collider.gameObject.GetComponentInParent<BattleUnit>();
        if (unit == null) return;
        if (unit == self_battle_unit_) return;
        if (unit.GetUnitType() == self_battle_unit_.GetUnitType()) return;

        SkillResult result = new SkillResult();
        result.SetUp(self_battle_unit_, skill_data_);
        result.AddResultOpponentUnit(unit);
        result.Calculation();

        foreach (var data in result.GetSkillPoint())
        {
            GameUICore.Instance.BattleNumber(data, unit.transform.position);
        }

        unit.KnockBackSetUp(self_battle_unit_, result.GetSkillPoint()[0]);

        is_attack_hit_ = true;
    }
}
