﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 全弾丸データを管理するクラス
/// </summary>
public class BulletDataStorageManager
{
    /// <summary>
    /// 各弾丸データ集
    /// </summary>
    private Dictionary<int, BulletDataStorage> dictionary_bullet_data_storage_ = new Dictionary<int, BulletDataStorage>();

    /// <summary>
    /// ロードする弾丸データ
    /// </summary>
    private Dictionary<int, AddressableData<BulletDataScriptable>> dictionary_addresable_bullet_data_ = new Dictionary<int, AddressableData<BulletDataScriptable>>();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// ロード
    /// </summary>
    /// <param name="bullet_id"></param>
    public void LoadBulletData(int bullet_id)
    {
        is_setup_ = false;
        if (dictionary_bullet_data_storage_.ContainsKey(bullet_id) == true) return;
        if(dictionary_addresable_bullet_data_.ContainsKey(bullet_id) == true)return;

        var data = new AddressableData<BulletDataScriptable>();
        data.LoadStart($"Assets/Project/Assets/Data/Bullet/{bullet_id:0000}/{bullet_id:0000}.asset");
        dictionary_addresable_bullet_data_.Add(bullet_id,data);
    }

    public void Update()
    {
        SetUp();
    }

    public void AllRelease()
    {
        dictionary_bullet_data_storage_.Clear();
        dictionary_addresable_bullet_data_.Clear();
    }

    /// <summary>
    /// 弾丸データ取得
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public List<BulletGameData> GetBulletData(int search_id,int bullet_id)
    {
        if (dictionary_bullet_data_storage_.ContainsKey(search_id) == false) return null;

        return dictionary_bullet_data_storage_[search_id].GetBulletData(bullet_id);
    }


    /// <summary>
    /// 弾丸データCollisionID取得
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public List<int> GetBulletCollisionID(int search_id, int bullet_id)
    {
        if (dictionary_bullet_data_storage_.ContainsKey(search_id) == false) return null;

        return dictionary_bullet_data_storage_[search_id].GetBulletCollisionID(bullet_id);
    }

    /// <summary>
    /// 弾丸データEffectID取得
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public List<int> GetBulletEffectID(int search_id, int bullet_id)
    {
        if (dictionary_bullet_data_storage_.ContainsKey(search_id) == false) return null;

        return dictionary_bullet_data_storage_[search_id].GetBulletEffectID(bullet_id);
    }

    public void SetUp()
    {
        if (is_setup_ == true) return;

        is_setup_ = true;

        foreach(var data in dictionary_addresable_bullet_data_)
        {
            if(data.Value.GetFlagSetUpLoading() == false)
            {
                is_setup_ = false;
                return;
            }
        }

        foreach(var data in dictionary_addresable_bullet_data_)
        {
            dictionary_bullet_data_storage_.Add(data.Key, new BulletDataStorage());
            dictionary_bullet_data_storage_[data.Key].AddBulletData(data.Value.GetAddressableData().GetListData());
            
            foreach(var details_data in dictionary_bullet_data_storage_[data.Key].GetAllBulletGameData())
            {
                EffectCore.Instance.LoadEffect(GameEffectManager.EffectRealmType.Battle,details_data.GetBulletEffectID());
            }

            data.Value.Release();
        }

        dictionary_addresable_bullet_data_.Clear();
        
    }
}
