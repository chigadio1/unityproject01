﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 弾丸データ倉庫
/// </summary>
public class BulletDataStorage
{
    /// <summary>
    /// 弾丸データ
    /// </summary>
    private Dictionary<int, List<BulletGameData>> dictionary_bullet_game_data_ = new Dictionary<int, List<BulletGameData>>();

    public List<BulletGameData> GetAllBulletGameData()
    {
        List<BulletGameData> list_bullet_game_data = new List<BulletGameData>();

        foreach(var data in dictionary_bullet_game_data_)
        {
            foreach(var details in data.Value)
            {
                list_bullet_game_data.Add(details);
            }
        }

        return list_bullet_game_data;
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    public BulletDataStorage()
    {
        dictionary_bullet_game_data_ = new Dictionary<int, List<BulletGameData>>();
    }

    /// <summary>
    /// 追加弾丸データ
    /// </summary>
    /// <param name="list_data"></param>
    public void AddBulletData(List<BulletDataExcel> list_data)
    {
        foreach(var data in list_data)
        {
            if(dictionary_bullet_game_data_.ContainsKey(data.GetBulletID()) == false)
            {
                dictionary_bullet_game_data_.Add(data.GetBulletID(),new List<BulletGameData>());
            }
            var bullet_data = new BulletGameData();
            bullet_data.SetValue(data);
            dictionary_bullet_game_data_[data.GetBulletID()].Add(bullet_data);
        }
    }

    /// <summary>
    /// 弾丸データ取得
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public List<BulletGameData> GetBulletData(int search_id)
    {
        if (dictionary_bullet_game_data_.ContainsKey(search_id) == false) return null;

        return dictionary_bullet_game_data_[search_id];
    }

    /// <summary>
    /// 弾丸データCollisionID取得
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public List<int> GetBulletCollisionID(int search_id)
    {
        if (dictionary_bullet_game_data_.ContainsKey(search_id) == false) return null;

        List<int> bullet_collision_ids_ = new List<int>();

        foreach(var data in dictionary_bullet_game_data_[search_id])
        {
            bullet_collision_ids_.Add(data.GetBulletCollisionID());
        }

        return bullet_collision_ids_;
    }

    /// <summary>
    /// 弾丸データEffectID取得
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public List<int> GetBulletEffectID(int search_id)
    {
        if (dictionary_bullet_game_data_.ContainsKey(search_id) == false) return null;

        List<int> bullet_collision_ids_ = new List<int>();

        foreach (var data in dictionary_bullet_game_data_[search_id])
        {
            bullet_collision_ids_.Add(data.GetBulletEffectID());
        }

        return bullet_collision_ids_;
    }

    public void ReleaseAll()
    {
        dictionary_bullet_game_data_.Clear();
    }
}
