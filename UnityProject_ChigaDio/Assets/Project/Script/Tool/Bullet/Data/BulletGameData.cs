﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 弾丸ゲームデータ
/// </summary>
public class BulletGameData
{
    /// <summary>
    /// インデックス
    /// </summary>
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// 弾丸ID
    /// </summary>
    private int bullet_id_ = 0;
    public int GetBulletID() { return bullet_id_; }

    /// <summary>
    /// 弾丸タイプ
    /// </summary>
    private BulletDataExcel.BulletType bullet_type_ = BulletDataExcel.BulletType.None;
    public BulletDataExcel.BulletType GetBulletType() { return bullet_type_; }

    /// <summary>
    /// 最大生存フレーム
    /// </summary>
    private float life_frame_ = 0.0f;
    public float GetLifeFrame() { return life_frame_; }

    /// <summary>
    /// FLOAT01
    /// </summary>
    private float float_01_ = 0.0f;
    public float GetFloat01() { return float_01_; }

    /// <summary>
    /// FLOAT02
    /// </summary>
    private float float_02_ = 0.0f;
    public float GetFloat02() { return float_02_; }

    /// <summary>
    /// X座標
    /// </summary>
    private float position_x_ = 0.0f;
    public float GetPositionX() { return position_x_; }

    /// <summary>
    /// Y座標
    /// </summary>
    private float position_y_ = 0.0f;
    public float GetPositionY() { return position_y_; }

    /// <summary>
    /// Z座標
    /// </summary>
    private float position_z_ = 0.0f;
    public float GetPositionZ() { return position_z_; }

    /// <summary>
    /// 弾丸エフェクトID
    /// </summary>
    private int bullet_effect_id_ = 0;
    public int GetBulletEffectID() { return bullet_effect_id_; }


    /// <summary>
    /// 弾丸コリジョンID
    /// </summary>
    private int bullet_collision_id_ = 0;
    public int GetBulletCollisionID() { return bullet_collision_id_; }


    /// <summary>
    /// 弾丸スキルID
    /// </summary>
    [SerializeField]
    private int bullet_skill_id_ = 0;
    public int GetBulletSkillID() { return bullet_skill_id_; }

    /// <summary>
    /// 弾丸攻撃コリジョン役割
    /// </summary>
    private BulletDataExcel.BulletCollisionRole bullet_attack_collision_role_ = BulletDataExcel.BulletCollisionRole.None;
    public BulletDataExcel.BulletCollisionRole GetBulletAttackCollisionRole() { return bullet_attack_collision_role_; }

    /// <summary>
    /// 弾丸攻撃コリジョンが当たったとき、生成される次の弾丸ID
    /// </summary>
    [SerializeField]
    private int bullet_hit_attack_collision_next_id_ = 0;
    public int GetBulletHitAttackCollisionNextID() { return bullet_hit_attack_collision_next_id_; }

    /// <summary>
    /// 弾丸背景コリジョン役割
    /// </summary>
    private BulletDataExcel.BulletCollisionRole bullet_background_collision_role_ = BulletDataExcel.BulletCollisionRole.None;
    public BulletDataExcel.BulletCollisionRole GetBulletBackGroundCollisionRole() { return bullet_background_collision_role_; }

    /// <summary>
    /// 弾丸背景コリジョンが当たったとき、生成される次の弾丸ID
    /// </summary>
    private int bullet_hit_background_collision_next_id_ = 0;
    public int GetBulletHitBackGroundCollisionNextID() { return bullet_hit_background_collision_next_id_; }

    public void SetValue(BulletDataExcel data)
    {
        index_ = data.GetIndex();
        bullet_id_ = data.GetBulletID();
        bullet_type_ = data.GetBulletType();
        life_frame_ = data.GetLifeFrame();
        float_01_ = data.GetFloat01();
        float_02_ = data.GetFloat02();
        position_x_ = data.GetPositionX();
        position_y_ = data.GetPositionY();
        position_z_ = data.GetPositionZ();
        bullet_effect_id_ = data.GetBulletEffectID();
        bullet_collision_id_ = data.GetBulletCollisionID();
        bullet_skill_id_ = data.GetBulletSkillID();
        bullet_attack_collision_role_ = data.GetBulletAttackCollisionRole();
        bullet_hit_attack_collision_next_id_ = data.GetBulletHitAttackCollisionNextID();
        bullet_background_collision_role_ = data.GetBulletBackGroundCollisionRole();
        bullet_hit_background_collision_next_id_ = data.GetBulletHitBackGroundCollisionNextID();
    }
}
