﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 弾丸Excelデータ
/// </summary>
[System.Serializable]
public class BulletDataExcel : BaseComposition
{
    /// <summary>
    /// 弾丸タイプ
    /// </summary>
    public enum BulletType
    {
        Landmines, //地雷型
        Release, //放出
        None
    }

    /// <summary>
    /// 弾丸コリジョン実行
    /// </summary>
    public enum BulletCollisionRole
    {
        Hit,
        Destroy,
        None
    }

    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// 弾丸ID
    /// </summary>
    [SerializeField]
    private int bullet_id_ = 0;
    public int GetBulletID() { return bullet_id_; }
    
    /// <summary>
    /// 弾丸タイプ
    /// </summary>
    [SerializeField]
    private BulletType bullet_type_ = BulletType.None;
    public BulletType GetBulletType() { return bullet_type_; }

    /// <summary>
    /// 最大生存フレーム
    /// </summary>
    [SerializeField]
    private float life_frame_ = 0.0f;
    public float GetLifeFrame() { return life_frame_; }

    /// <summary>
    /// FLOAT01
    /// </summary>
    [SerializeField]
    private float float_01_ = 0.0f;
    public float GetFloat01() { return float_01_; }

    /// <summary>
    /// FLOAT02
    /// </summary>
    [SerializeField]
    private float float_02_ = 0.0f;
    public float GetFloat02() { return float_02_; }

    /// <summary>
    /// X座標
    /// </summary>
    [SerializeField]
    private float position_x_ = 0.0f;
    public float GetPositionX() { return position_x_; }

    /// <summary>
    /// Y座標
    /// </summary>
    [SerializeField]
    private float position_y_ = 0.0f;
    public float GetPositionY() { return position_y_; }

    /// <summary>
    /// Z座標
    /// </summary>
    [SerializeField]
    private float position_z_ = 0.0f;
    public float GetPositionZ() { return position_z_; }

    /// <summary>
    /// 弾丸エフェクトID
    /// </summary>
    [SerializeField]
    private int bullet_effect_id_ = 0;
    public int GetBulletEffectID() { return bullet_effect_id_; }


    /// <summary>
    /// 弾丸コリジョンID
    /// </summary>
    [SerializeField]
    private int bullet_collision_id_ = 0;
    public int GetBulletCollisionID() { return bullet_collision_id_; }

    /// <summary>
    /// 弾丸スキルID
    /// </summary>
    [SerializeField]
    private int bullet_skill_id_ = 0;
    public int GetBulletSkillID() { return bullet_skill_id_; }

    /// <summary>
    /// 弾丸攻撃コリジョン役割
    /// </summary>
    [SerializeField]
    private BulletCollisionRole bullet_attack_collision_role_ = BulletCollisionRole.None;
    public BulletCollisionRole GetBulletAttackCollisionRole() { return bullet_attack_collision_role_; }

    /// <summary>
    /// 弾丸攻撃コリジョンが当たったとき、生成される次の弾丸ID
    /// </summary>
    [SerializeField]
    private int bullet_hit_attack_collision_next_id_ = 0;
    public int GetBulletHitAttackCollisionNextID() { return bullet_hit_attack_collision_next_id_; }

    /// <summary>
    /// 弾丸背景コリジョン役割
    /// </summary>
    [SerializeField]
    private BulletCollisionRole bullet_background_collision_role_ = BulletCollisionRole.None;
    public BulletCollisionRole GetBulletBackGroundCollisionRole() { return bullet_background_collision_role_; }

    /// <summary>
    /// 弾丸背景コリジョンが当たったとき、生成される次の弾丸ID
    /// </summary>
    [SerializeField]
    private int bullet_hit_background_collision_next_id_ = 0;
    public int GetBulletHitBackGroundCollisionNextID() { return bullet_hit_background_collision_next_id_; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ProjectSystem.ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(bullet_id_), bullet_id_);
        data_group.AddData("BulletType", nameof(bullet_type_), bullet_type_);
        data_group.AddData("LifeFrame", nameof(life_frame_), life_frame_);
        data_group.AddData("Float01", nameof(float_01_), float_01_);
        data_group.AddData("Float02", nameof(float_02_), float_02_);
        data_group.AddData("X座標", nameof(position_x_), position_x_);
        data_group.AddData("Y座標", nameof(position_y_), position_y_);
        data_group.AddData("Z座標", nameof(position_z_), position_z_);
        data_group.AddData("EffectID", nameof(bullet_effect_id_), bullet_effect_id_);
        data_group.AddData("CollisionID", nameof(bullet_collision_id_), bullet_collision_id_);
        data_group.AddData("SkillID", nameof(bullet_skill_id_), bullet_skill_id_);
        data_group.AddData("AttackCollisionRole", nameof(bullet_attack_collision_role_), bullet_attack_collision_role_);
        data_group.AddData("HitAttackCollisionNextBulletID", nameof(bullet_hit_attack_collision_next_id_), bullet_hit_attack_collision_next_id_);
        data_group.AddData("BackGroundCollisionRole", nameof(bullet_background_collision_role_), bullet_background_collision_role_);
        data_group.AddData("HitBackGroundCollisionNextBulletID", nameof(bullet_hit_background_collision_next_id_), bullet_hit_background_collision_next_id_);    
    }

}
