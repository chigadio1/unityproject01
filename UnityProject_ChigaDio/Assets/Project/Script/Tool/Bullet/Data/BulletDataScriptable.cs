﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(BulletDataScriptable))]
public class EditorBulletDataScriptable : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<BulletDataExcel>(target as BaseExcelScriptableObject<BulletDataExcel>);
        base.OnInspectorGUI();
    }
}


#endif

/// <summary>
/// Bulletスクリプタブルデータ
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreateBulletData")]
public class BulletDataScriptable : BaseExcelScriptableObject<BulletDataExcel>
{
}
