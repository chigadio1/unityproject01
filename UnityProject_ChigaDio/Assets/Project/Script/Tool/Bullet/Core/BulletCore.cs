﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バレットコア
/// </summary>
public class BulletCore : Singleton<BulletCore>
{
    /// <summary>
    /// 弾丸保存管理
    /// </summary>
    private BulletDataStorageManager bullet_data_storage_manager_ = new BulletDataStorageManager();

    public bool GetIsSetUP() { return bullet_data_storage_manager_.GetIsSetUp(); }

    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "BulletCore";
    }

    // Update is called once per frame
    void Update()
    {
        bullet_data_storage_manager_.Update();
    }

    public void AllRelease()
    {
        bullet_data_storage_manager_.AllRelease();
    }

    /// <summary>
    /// ロード
    /// </summary>
    /// <param name="bullet_id"></param>
    public void LoadBulletData(int bullet_id)
    {
        bullet_data_storage_manager_.LoadBulletData(bullet_id);
    }

    /// <summary>
    /// 弾丸データ取得
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public List<BulletGameData> GetBulletData(int search_id, int bullet_id)
    {
        return bullet_data_storage_manager_.GetBulletData(search_id, bullet_id);
    }

    /// <summary>
    /// 弾丸データCollisionID取得
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public List<int> GetBulletCollisionID(int search_id, int bullet_id)
    {
        return bullet_data_storage_manager_.GetBulletCollisionID(search_id, bullet_id);
    }

    /// <summary>
    /// 弾丸データEffectID取得
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public List<int> GetBulletEffectID(int search_id, int bullet_id)
    {
        return bullet_data_storage_manager_.GetBulletEffectID(search_id, bullet_id);
    }
}
