﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 弾丸生成
/// </summary>
public static class BulletCreate
{

    /// <summary>
    /// 生成
    /// </summary>
    /// <param name="bullet_id"></param>
    /// <param name="bullet_details_id"></param>
    /// <param name="position"></param>
    public static void Appearance(BattleUnit unit,int bullet_id, int bullet_details_id, Vector3 position,Quaternion quaternion)
    {
        var bullet_data = BulletCore.Instance.GetBulletData(bullet_id,bullet_details_id);
        if (bullet_data == null) return;

        var bullet_object = new GameObject();
        bullet_object.transform.SetParent(BulletCore.Instance.transform);
        bullet_object.name = $"{bullet_id:0000}_{bullet_details_id:0000}";

        int count = 1;
        foreach(var bullet in bullet_data)
        {

            var collision_data = BattleCollisionCore.Instance.GetBattleCollisionGameData(bullet_id,bullet.GetBulletCollisionID());

            
            var bullet_details_object = new GameObject();
            bullet_details_object.transform.position = (quaternion * new Vector3(bullet.GetPositionX(), bullet.GetPositionY(), bullet.GetPositionZ())) + position;
            var bullet_action = bullet_details_object.AddComponent<BulletAction>();

            bullet_details_object.name = $"{bullet_id:0000}_{bullet_details_id:0000}_{bullet.GetBulletID():0000}_{count:0000}";

            bullet_action.SetUp(unit,bullet_object, bullet, collision_data);
            bullet_action.enabled = false;
            bullet_action.enabled = true;
            var effect_data = EffectCreate.Appearance(bullet.GetBulletEffectID(), Vector3.zero, quaternion, Vector3.one, bullet.GetLifeFrame(),GameEffectManager.EffectRealmType.Battle); ;
            if(effect_data != null)
            {
                effect_data.transform.SetParent(bullet_details_object.transform);
                effect_data.transform.localPosition = Vector3.zero;
            }

            count++;
        }
    }

    public static BaseBulletRole Appearance(BulletDataExcel.BulletType type)
    {
        switch (type)
        {
            case BulletDataExcel.BulletType.Landmines:
                return new LandminesBulletRole();
            case BulletDataExcel.BulletType.Release:
                return new  ReleaseBulletRole();
            case BulletDataExcel.BulletType.None:
                break;
        }

        return null;

    }
}
