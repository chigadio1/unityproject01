﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// アクセサーエラー
/// </summary>
public  class AccessorError
{
    /// <summary>
    /// エラーint
    /// </summary>
    private static int error_int_ = 99999;
    public static int GetErrorInt() { return error_int_; }

    /// <summary>
    /// エラーString
    /// </summary>
    private static string error_string_ = "Error";
    public static string GetErrorString() { return error_string_; }
}
