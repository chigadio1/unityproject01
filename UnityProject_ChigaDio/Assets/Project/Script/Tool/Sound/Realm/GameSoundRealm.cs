﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSoundRealm : BaseRealm<GameSoundPlayManager,GameSoundPlayManager.SoundRealmType>
{
    public bool GetIsSetUp()
    {
        if (dictionary_realm_datas_.Count == 0) return false;

        return dictionary_realm_datas_[GameSoundPlayManager.SoundRealmType.Dungeon].GetIsSetUp() &
            dictionary_realm_datas_[GameSoundPlayManager.SoundRealmType.Main].GetIsSetUp() &
            dictionary_realm_datas_[GameSoundPlayManager.SoundRealmType.Battle].GetIsSetUp() &
            dictionary_realm_datas_[GameSoundPlayManager.SoundRealmType.UI].GetIsSetUp();
    }

    public override void Update()
    {
        foreach(var data in dictionary_realm_datas_)
        {
            data.Value.Update();
        }
    }

    public void SoundInit(GameSoundPlayManager.SoundRealmType type)
    {
        dictionary_realm_datas_[type].StartLoad(type);
    }

    public void MainSoundInit()
    {
        SoundInit(GameSoundPlayManager.SoundRealmType.Main);
    }
    public void DungeonSoundInit()
    {
        SoundInit(GameSoundPlayManager.SoundRealmType.Dungeon);
    }
    public void BattleSoundInit()
    {
        SoundInit(GameSoundPlayManager.SoundRealmType.Battle);
    }
    public void UISoundInit()
    {
        SoundInit(GameSoundPlayManager.SoundRealmType.UI);
    }


    public void LoadSound(GameSoundPlayManager.SoundRealmType realm_type, int sound_id)
    {
        dictionary_realm_datas_[realm_type].LoadAudioClip(sound_id);
    }
    private GameSoundPlay SESoundPlay(GameSoundPlayManager.SoundRealmType realm_type,GameObject sound_object,int sound_id)
    {
        return dictionary_realm_datas_[realm_type].SESoundPlay(sound_object, sound_id);
    }

    private GameSoundPlay BGMSoundPlay(GameSoundPlayManager.SoundRealmType realm_type, GameObject sound_object, int sound_id)
    {
        return dictionary_realm_datas_[realm_type].BGMSoundPlay(sound_object, sound_id);
    }

    public GameSoundPlay BattleSESoundPlay(GameObject sound_object, int sound_id)
    {
        return SESoundPlay(GameSoundPlayManager.SoundRealmType.Battle,sound_object, sound_id);
    }

    public GameSoundPlay BattleBGMSoundPlay(GameObject sound_object, int sound_id)
    {
        return BGMSoundPlay (GameSoundPlayManager.SoundRealmType.Battle, sound_object, sound_id);
    }

    public GameSoundPlay UISESoundPlay(GameObject sound_object, int sound_id)
    {
        return SESoundPlay(GameSoundPlayManager.SoundRealmType.Battle, sound_object, sound_id);
    }

    public GameSoundPlay UIBGMSoundPlay(GameObject sound_object, int sound_id)
    {
        return BGMSoundPlay(GameSoundPlayManager.SoundRealmType.Battle, sound_object, sound_id);
    }

    public SoundExcelData GetSoundData(GameSoundPlayManager.SoundRealmType realm_type,int sound_id)
    {

        return dictionary_realm_datas_[realm_type].GetSoundData(sound_id);
    }

    public void AllDestroy()
    {
        foreach(var data in dictionary_realm_datas_)
        {
            data.Value.AllDestroy();
        }
    }
}
