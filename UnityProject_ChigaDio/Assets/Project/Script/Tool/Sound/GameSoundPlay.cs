﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲームサウンド
/// </summary>
public class GameSoundPlay
{
    public enum SoundType
    {
        SE,
        BGM
    }


    /// <summary>
    /// 音再生機能
    /// </summary>
    private AudioSource sound_audio_source_ = null;

    /// <summary>
    /// 音タイプ
    /// </summary>
    private SoundType sound_type_ = SoundType.SE;

    /// <summary>
    /// 再生中か
    /// </summary>
    private bool is_play_ = false;
    public bool GetIsPlay() { return is_play_; }

    /// <summary>
    /// 終了判定
    /// </summary>
    private bool is_end_ = true;
    public bool GetIsEnd() { return is_end_; }

    public void StartSound(AudioClip value_audio,SoundType value_type,GameObject sound_object,SoundExcelData data)
    {
        if (value_audio == null) return;
        if (sound_object == null) return;
        sound_type_ = value_type;

        sound_audio_source_ =  sound_object.AddComponent<AudioSource>();
        sound_audio_source_.clip = value_audio;
     
        if(sound_type_ == SoundType.BGM)
        {
            sound_audio_source_.loop = true;
        }

        sound_audio_source_.volume = data.GetSoundVolume();
        sound_audio_source_.Play();
        is_play_ = true;
        is_end_ = false;
    }

    public void Update()
    {
        if (is_end_ == true) return;
        if (is_play_ == false) return;
        if (sound_audio_source_ == null)
        {
            is_play_ = false;
            return;
        }

        TypeUpdate();
    }

    private void TypeUpdate()
    {
        if(sound_type_ == SoundType.SE)
        {
            SEUpdate();
        }
        else
        {
            BGMUpdate();
        }
    }

    private void SEUpdate()
    {
       if(sound_audio_source_.isPlaying == false)
        {
            is_play_ = false;
            is_end_ = true;
            Destroy();
        }
    }

    private void BGMUpdate()
    {

    }

    public void Destroy()
    {
        if (sound_audio_source_ == null) return;

        GameObject.Destroy(sound_audio_source_);
    }
}
