﻿using System.Collections;
using System.Collections.Generic;
using ProjectSystem;
using UnityEngine;


/// <summary>
/// 音Excelデータ
/// </summary>
[System.Serializable]
public class SoundExcelData : BaseComposition
{
    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }
    /// <summary>
    /// ID
    /// </summary>
    [SerializeField]
    private int id_ = 0;
    public int GetID() { return id_; }
    /// <summary>
    /// サウンドID
    /// </summary>
    [SerializeField]
    private int sound_id_ = 0;
    public int GetSoundID() { return sound_id_; }

    /// <summary>
    ///音の種類
    /// </summary>
    [SerializeField]
    private GameSoundPlay.SoundType sound_type_ = GameSoundPlay.SoundType.SE;
    public GameSoundPlay.SoundType GetSoundType() { return sound_type_; }


    /// <summary>
    /// 音の大きさ
    /// </summary>
    [SerializeField]
    private float sound_volume = 1.0f;
    public float GetSoundVolume() { return sound_volume; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(id_), id_);
        data_group.AddData("SoundID", nameof(sound_id_), sound_id_);
        data_group.AddData("SoundType", nameof(sound_type_), sound_type_);
        data_group.AddData("Volume", nameof(sound_volume), sound_volume);
    }
}
