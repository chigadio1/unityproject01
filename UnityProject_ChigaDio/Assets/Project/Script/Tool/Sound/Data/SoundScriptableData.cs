﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(SoundScriptableData))]
public class EditorSoundDataScriptableInspector : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<SoundExcelData>(target as BaseExcelScriptableObject<SoundExcelData>);

        base.OnInspectorGUI();
    }
}
#endif


/// <summary>
/// 音データ
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreateSoundData")]
public class SoundScriptableData : BaseExcelScriptableObject<SoundExcelData>
{
}
