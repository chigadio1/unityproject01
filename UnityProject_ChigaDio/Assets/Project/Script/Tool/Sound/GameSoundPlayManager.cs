﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲーム音管理
/// </summary>
public class GameSoundPlayManager
{
    public enum SoundRealmType
    {
        Main,
        Dungeon,
        Battle,
        UI,
    }


    /// <summary>
    /// 動的に集約した音オブジェ
    /// </summary>
    private Dictionary<int, AddressableData<AudioClip>> dictionary_audio_clips_ = new Dictionary<int, AddressableData<AudioClip>>();

    /// <summary>
    /// アドレササウンド
    /// </summary>
    private AddressableData<SoundScriptableData> addressable_sound_scriotable_ = null;

    /// <summary>
    /// リスト：SE
    /// </summary>
    private List<GameSoundPlay> list_se_game_sound_play_ = new List<GameSoundPlay>();

    /// <summary>
    /// BGM
    /// </summary>
    private GameSoundPlay bgm_game_sound_play_ = new GameSoundPlay();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = true;
    public bool GetIsSetUp() { return is_setup_; }

    public void StartLoad(SoundRealmType type)
    {
        string type_path = GetTypeName(type);
        if (type_path == "") return;
        if (addressable_sound_scriotable_ != null) addressable_sound_scriotable_.Release();
        addressable_sound_scriotable_ = new AddressableData<SoundScriptableData>();
        string load_path = $"Assets/Project/Assets/Data/Sound/" + type_path + $"/" + type_path + $".asset";
        addressable_sound_scriotable_.LoadStart(load_path);
        is_setup_ = false;
    }

    public void LoadAudioClip(int sound_id)
    {
        if (addressable_sound_scriotable_.GetFlagSetUpLoading() == false) return;
        if (dictionary_audio_clips_.ContainsKey(sound_id) == true) return;

        var addressable = new AddressableData<AudioClip>();
        addressable.LoadStart($"Assets/Project/Assets/Data/Sound/Prefab/{sound_id:0000}.mp3");
        dictionary_audio_clips_.Add(sound_id, addressable);
        is_setup_ = false;
    }

    private string GetTypeName(SoundRealmType type)
    {
        switch (type)
        {
            case SoundRealmType.Main:
                return "Main";
            case SoundRealmType.Dungeon:
                return "Dungeon";
            case SoundRealmType.Battle:
                return "Battle";
            case SoundRealmType.UI:
                return "UI";
        }

        return "";
    }


    public void Update()
    {
        UpdateCheck();
        if (is_setup_ == false) return;
        foreach(var data in list_se_game_sound_play_)
        {
            data.Update();
        }
        bgm_game_sound_play_.Update();
        UpdateAutoDestroy();
    }

    private void UpdateCheck()
    {
        if (is_setup_ == true) return;
        if (DataCore.Instance.GetIsSetUp() == false) return;
        if (addressable_sound_scriotable_.GetFlagSetUpLoading() == false) return;

        foreach(var data in addressable_sound_scriotable_.GetAddressableData().GetListData())
        {
            LoadAudioClip(data.GetSoundID());
        }

        foreach(var data in dictionary_audio_clips_)
        {
            if (data.Value.GetFlagSetUpLoading() == false) return;
        }

        is_setup_ = true;
    }

    private void UpdateAutoDestroy()
    {
        list_se_game_sound_play_.RemoveAll(data => data.GetIsEnd() == true);
    }

    public GameSoundPlay SESoundPlay(GameObject sound_object, int sound_id)
    {
        if (is_setup_ == false) return null;
        var data =  addressable_sound_scriotable_.GetAddressableData().GetListData();
        var sound_data =  data.Find(sound_id_data => sound_id_data.GetID() == sound_id);
        if (sound_data == null) return null;
        if (dictionary_audio_clips_.ContainsKey(sound_data.GetSoundID()) == false) return null;
        var audio = dictionary_audio_clips_[sound_data.GetSoundID()].GetAddressableData();
        if (audio == null) return null;

        var sound = new GameSoundPlay();
        sound.StartSound(audio, GameSoundPlay.SoundType.SE, sound_object, sound_data);
        list_se_game_sound_play_.Add(sound);

        return sound;
    }


    public GameSoundPlay BGMSoundPlay(GameObject sound_object, int sound_id)
    {

        if (is_setup_ == false) return null;
        var data = addressable_sound_scriotable_.GetAddressableData().GetListData();
        var sound_data = data.Find(sound_id_data => sound_id_data.GetID() == sound_id);
        if (sound_data == null) return null;
        if (dictionary_audio_clips_.ContainsKey(sound_data.GetSoundID()) == false) return null;
        var audio = dictionary_audio_clips_[sound_data.GetSoundID()].GetAddressableData();
        if (audio == null) return null;

        if (bgm_game_sound_play_ != null) bgm_game_sound_play_.Destroy();
        var sound = new GameSoundPlay();
        sound.StartSound(audio, GameSoundPlay.SoundType.BGM, sound_object,sound_data);
        bgm_game_sound_play_ = sound;

        return sound;
    }

    public void AllDestroy()
    {
        AllSEDestroy();
        BGMDestroy();

        foreach(var data in dictionary_audio_clips_)
        {
            data.Value.Release();
        }
        dictionary_audio_clips_.Clear();
        addressable_sound_scriotable_.Release();
        is_setup_ = true;
    }

    public void AllSEDestroy()
    {

        foreach (var data in list_se_game_sound_play_)
        {
            data.Destroy();
        }
        list_se_game_sound_play_.Clear();
    }

    public void BGMDestroy()
    {
        bgm_game_sound_play_.Destroy();
        bgm_game_sound_play_ = null;
    }

    public SoundExcelData GetSoundData(int sound_id)
    {
        if (addressable_sound_scriotable_.GetFlagSetUpLoading() == false) return null;

        return addressable_sound_scriotable_.GetAddressableData().GetListData().Find(data => data.GetID() == sound_id);
    }
    
}