﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundCore : Singleton<SoundCore>
{

    /// <summary>
    /// サウンド情報
    /// </summary>
    private GameSoundRealm game_sound_realm_ = new GameSoundRealm();

    public bool GetIsSetUp() { return game_sound_realm_.GetIsSetUp(); }

    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "SoundCore";

        game_sound_realm_.Start();
        //仮
        game_sound_realm_.MainSoundInit();
        game_sound_realm_.BattleSoundInit();
        game_sound_realm_.DungeonSoundInit();
        game_sound_realm_.UISoundInit();
    }

    private void Update()
    {
        game_sound_realm_.Update();
    }

    public void BattleSESoundPlay(int sound_id)
    {
        if (game_sound_realm_.GetIsSetUp() == false) return;
        game_sound_realm_.BattleSESoundPlay(gameObject, sound_id);
    }
    public void BattleBGMSoundPlay(int sound_id)
    {
        if (game_sound_realm_.GetIsSetUp() == false) return;
        game_sound_realm_.BattleBGMSoundPlay(gameObject, sound_id);
    }

    public GameSoundPlay UISESoundPlay(int sound_id)
    {
        if (game_sound_realm_.GetIsSetUp() == false) return null;
        return game_sound_realm_.UISESoundPlay(gameObject, sound_id);
    }
    public GameSoundPlay UIBGMSoundPlay(int sound_id)
    {
        if (game_sound_realm_.GetIsSetUp() == false) return null;
        return game_sound_realm_.UIBGMSoundPlay(gameObject, sound_id);
    }

    private void OnDestroy()
    {
        game_sound_realm_.AllDestroy();
    }

    public SoundExcelData GetSoundData(GameSoundPlayManager.SoundRealmType realm_type, int sound_id)
    {
        return game_sound_realm_.GetSoundData(realm_type, sound_id);
    }

    public void LoadSound(GameSoundPlayManager.SoundRealmType realm_type, int sound_id)
    {
        game_sound_realm_.LoadSound(realm_type, sound_id);
    }
}
