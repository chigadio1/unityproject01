﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// アニメーション変換
/// </summary>
public class AnimationConvert
{
    /// <summary>
    /// アニメーションステート
    /// </summary>
    public enum AnimationState
    {
        Anim_Idle = 5001, //待機
        Anim_Walk,        //歩き
        Anim_Run,         //走る
        Anim_KnockBack_S, //ノックバック
        Anim_Attack_01,   //攻撃
        Anim_Attack_02,   //攻撃2
        Anim_Death,       //死亡
        Anim_Fall,        //落下
        Max_Anim
    }



    public static string GetAnimationName(AnimationState id)
    {
       
        return $"{(int)id:0000}";
    }
}
