﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// コリジョンレイヤー
/// </summary>
public enum CollisionLayer : int
{
    kNone,                         //指定なし
    kPlayer,                       //プレイヤー
    kEnemy,                        //エネミー
    kNoneUnit,                     //指定しないUnit
    kBullet,                       //弾丸
    kAttack,                       //攻撃
    kSensing,                      //感知
    kBackGround,                   //背景
}