﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
/// <summary>
/// コリジョン計算
/// </summary>
public static class RpgCollisionCalculation
{

    /// <summary>
    /// 強制終了するMaxカウント
    /// </summary>
    private  static int exit_max_count = 100;

    public enum CapsuleDirectionType : int
    {
        kAxisY,
        kAxisX,
        kAxisZ
    }

    /// <summary>
    /// カプセル内にコリジョンがあるかどうか
    /// </summary>
    /// <param name="collider"></param>
    /// <returns></returns>
    public static Collider[] CapsuleCalculation(CapsuleCollider collider)
    {
        float scale = Mathf.Max(collider.transform.localScale.x, collider.transform.localScale.z);
        float radius = collider.radius * scale + 0.01f;
        float _height = (((collider.height * collider.transform.localScale.y) / 2.0f) - radius);

        Vector3 up = collider.transform.up;
        if(collider.direction == (int)CapsuleDirectionType.kAxisX)
        {
            up = collider.transform.right;
        } 
        else if(collider.direction == (int)CapsuleDirectionType.kAxisZ)
        {
            up = collider.transform.forward;
        }

        Vector3 start_pos = Vector3.zero;
        start_pos = collider.center + collider.transform.position + ((_height * up));
        Vector3 end_pos = Vector3.zero;
        end_pos = collider.center + collider.transform.position + ((_height * (up * -1.0f)));

        Collider[] colliders = OverlapCapsule(start_pos, end_pos, radius);
        return colliders.Where(col => col != collider).ToArray();


    }
    /// <summary>
    /// カプセルOverlap
    /// </summary>
    /// <param name="start_pos">カプセル：スタート</param>
    /// <param name="end_pos">カプセル : エンド</param>
    /// <param name="radius">カプセル : 範囲</param>
    /// <returns></returns>
    public static Collider[] OverlapCapsule(Vector3 start_pos,Vector3 end_pos,float radius)
    {
        return Physics.OverlapCapsule(start_pos, end_pos, radius);
    }

    /// <summary>
    ///球内にコリジョンがあるかどうか
    /// </summary>
    /// <param name="collider"></param>
    /// <returns></returns>
    public static Collider[] SphereCalculation(SphereCollider collider)
    {
        float simulationRadius = collider.radius * collider.transform.localScale.x;
        Collider[] colliders = OverlapSphere(collider.transform.position + collider.center, simulationRadius);
        return colliders.Where(col => col != collider).ToArray();
    }
    /// <summary>
    /// 球体Overlap
    /// </summary>
    /// <param name="pos">球　: 座標</param>
    /// <param name="radius">球　: 範囲</param>
    /// <returns></returns>
    public static Collider[] OverlapSphere(Vector3 pos, float radius)
    {
        return Physics.OverlapSphere(pos, radius);
    }

    /// <summary>
    /// ボックス内にコリジョンがあるかどうか
    /// </summary>
    /// <param name="collider"></param>
    /// <returns></returns>
    public static Collider[] BoxCalculation(BoxCollider collider)
    {

        Vector3 size = collider.transform.localScale;
        size.x *= collider.size.x;
        size.y *= collider.size.y;
        size.z *= collider.size.z;
        Vector3 pos = collider.transform.position + collider.center;
   
        Collider[] colliders = OverlapBox(pos, size, collider.transform.rotation);
        return colliders.Where(col => col != collider).ToArray();

    }
    /// <summary>
    /// ボックスOverlap
    /// </summary>
    /// <param name="pos">ボックス : 座標</param>
    /// <param name="size">ボックス　: サイズ</param>
    /// <param name="rotation">ボックス　: 回転</param>
    /// <returns></returns>
    public static Collider[] OverlapBox(Vector3 pos, Vector3 size, Quaternion rotation)
    {
        return Physics.OverlapBox(pos, size,rotation );
    }

    /// <summary>
    /// 押し出し計算
    /// </summary>
    /// <param name="type"></param>
    /// <param name="this_collider"></param>
    /// <param name="opponent_collider"></param>
    /// <param name="pushBackVector"></param>
    /// <param name="pushBackDistance"></param>
    /// <returns></returns>
    public static bool ComputePenetration(CollisionPushType type, Collider this_collider, Vector3 position, Collider opponent_collider, Vector3 opponent_position, ref Vector3 pushBackVector, ref float pushBackDistance)
    {
        if (type == CollisionPushType.kThisPush || type == CollisionPushType.kNone)
        {
            return ComputePenetrationThisPush(this_collider, position, opponent_collider, opponent_position ,ref pushBackVector, ref pushBackDistance);
        }
        else
        {
            return ComputePenetrationOpponentPush(this_collider, position, opponent_collider, opponent_position, ref pushBackVector, ref pushBackDistance);
        }
    }

    /// <summary>
    /// 押し出し計算(自分)
    /// </summary>
    /// <param name="collider"></param>
    /// <param name="pushBackVector"></param>
    /// <param name="pushBackDistance"></param>
    /// <returns></returns>
    private static bool ComputePenetrationThisPush(Collider this_collider,Vector3 position ,Collider opponent_collider, Vector3 opponent_position, ref Vector3 pushBackVector, ref float pushBackDistance)
    {
        return Physics.ComputePenetration(
                        this_collider, //自分自身のコライダー
                        position, //自分自身の現在の座標
                        this_collider.transform.rotation, //自分自身の回転
                        opponent_collider, //相手のコライダー
                        opponent_position, //相手の前フレームの座標
                        opponent_collider.transform.rotation, //相手の回転
                        out pushBackVector, //押し込みベクトル
                        out pushBackDistance //押し込みの長さ
                    );
    }

    /// <summary>
    /// 押し出し計算(相手)
    /// </summary>
    /// <param name="collider"></param>
    /// <param name="pushBackVector"></param>
    /// <param name="pushBackDistance"></param>
    /// <returns></returns>
    private static bool ComputePenetrationOpponentPush(Collider this_collider, Vector3 position, Collider opponent_collider, Vector3 opponent_position, ref Vector3 pushBackVector, ref float pushBackDistance)
    {
        return Physics.ComputePenetration(
                        opponent_collider,
                        opponent_position,
                        opponent_collider.transform.rotation,
                        this_collider,
                        position,
                        this_collider.transform.rotation,
                        out pushBackVector,
                        out pushBackDistance
                    );

    }

    /// <summary>
    /// バトルエリアの計算
    /// </summary>
    /// <param name="game_object"></param>
    public static bool BattlAreaCollisionCorrection(GameObject game_object)
    {
        if (BattleCore.Instance.GetIsPlayBattle() == false) return false;

        Vector3 battle_area_position = BattleCore.Instance.GetBattleAreaPosition();
        float battle_area_length = BattleCore.Instance.GetBattleAreaLength();

        Vector3 unit_to_battle_vector = game_object.transform.position - battle_area_position;

        if (unit_to_battle_vector.magnitude >= battle_area_length)
        {
            game_object.transform.position = battle_area_position + (unit_to_battle_vector.normalized * battle_area_length);
            return true;
        }

        return false;


    }

    /// <summary>
    /// 当たり判定の計算
    /// </summary>
    /// <param name="this_collison"></param>
    /// <param name="collision_details"></param>
    /// <param name="collider"></param>
    public static void CollisionCorrection(BaseCollisionConstruction this_collison, RpgCollisionDetailsControl this_collision_control, RpgCollisionDetails collision_details, List<BaseCollisionConstruction> opponent_collider, Collider[] colliders)
    {
        if (this_collison == null || collision_details == null) return;
        if (opponent_collider.Count == 0) return;
        if (colliders.Length == 0) return;
        if (this_collison.CollisionLayer == CollisionLayer.kBackGround) return;
        for (int i = 0; i < opponent_collider.Count; i++)
        {

            Vector3 pushBackVector = Vector3.zero;
            float pushBackDistance = 0.0f;
            if (this_collision_control == opponent_collider[i].RpgCollisionDetailsControl) continue;
            List<RpgCollisionDetails> detaile =  opponent_collider[i].RpgCollisionDetailsControl.ListRpgCollisionDetaile;

            if (CollisionLayerIsHit(this_collison.CollisionLayer, opponent_collider[i].CollisionLayer) == false) continue;

            foreach (var opponent_col in detaile)
            {
                if (opponent_col == collision_details) continue;
                if (opponent_col.ThisObject == collision_details.ThisObject) continue;
                ///めり込んでいた場合
                if (ComputePenetration(this_collison.CollisionPushType, collision_details.RpgCollision, RpgCollisionDetailsAccessor.CalculationPosition(collision_details)
                                       , opponent_col.RpgCollision, RpgCollisionDetailsAccessor.SaveCollisionPosition(opponent_col)
                                       , ref pushBackVector, ref pushBackDistance))
                {
                    if (pushBackDistance >= float.Epsilon)
                    {
                        Vector3 updatedPostion = (pushBackVector) * (pushBackDistance);
                        if ((this_collison.CollisionPushType != CollisionPushType.kNone && opponent_collider[i].CollisionPushType != CollisionPushType.kNone))
                        {
                            this_collison.ThisObject.transform.position += new Vector3(updatedPostion.x, updatedPostion.y, updatedPostion.z);
                            RpgCollisionDetailsAccessor.SaveCollisionPosition(collision_details, this_collison.ThisObject.transform.position);
                            RpgCollisionDetailsAccessor.CalculationPosition(collision_details, this_collison.ThisObject.transform.position);
                        }


                        this_collision_control.CollisionHitEnter(opponent_col.RpgCollision, collision_details.RpgCollision, opponent_col);
                        opponent_collider[i].RpgCollisionDetailsControl.CollisionHitEnter(collision_details.RpgCollision, opponent_col.RpgCollision, collision_details);
                    }
                }

               
                
            }
        }

    }

    public static bool CollisionLayerIsHit(CollisionLayer this_layer, CollisionLayer opponent_layer)
    {
        if(this_layer == CollisionLayer.kBullet)
        {
            if (opponent_layer == CollisionLayer.kBullet) return false;
        }
        else if(this_layer == CollisionLayer.kAttack || this_layer == CollisionLayer.kSensing)
        {
            if (opponent_layer == CollisionLayer.kAttack || opponent_layer == CollisionLayer.kSensing) return false;
        }
        else if(this_layer == CollisionLayer.kBackGround)
        {
            if (opponent_layer == CollisionLayer.kBackGround) return false;
        }

        return true;
    }

    /// <summary>
    /// 強制終了
    /// </summary>
    /// <param name="cnt"></param>
    private static void Exit(int cnt)
    {
        if (cnt >= exit_max_count)
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
            return;
#else
        Application.Quit ();
#endif
        }
    }

#if UNITY_EDITOR
    /// <summary>
    /// デバッグ描画
    /// </summary>
    public static void DebugDraw(RpgCollisionDetails debug_draw)
    {
        if (debug_draw.RpgCollision.GetType() == typeof(BoxCollider))
        {
           RpgCollisionCalculation.DrawBox((BoxCollider)debug_draw.RpgCollision);
        }
        else if (debug_draw.RpgCollision.GetType() == typeof(SphereCollider))
        {
           RpgCollisionCalculation.DrawSphere((SphereCollider)debug_draw.RpgCollision);
        }
        else if (debug_draw.RpgCollision.GetType() == typeof(CapsuleCollider))
        {
           RpgCollisionCalculation.DrawCapsule((CapsuleCollider)debug_draw.RpgCollision);
        }
    }

    /// <summary>
    /// カプセル描画
    /// </summary>
    /// <param name="collider"></param>
    /// <returns></returns>
    public static void DrawCapsule(CapsuleCollider collider)
    {


        float scale = Mathf.Max(collider.transform.localScale.x, collider.transform.localScale.z);
        float radius = collider.radius * scale + 0.01f;
        float _height = (((collider.height * collider.transform.localScale.y) / 2.0f) - radius);

        Vector3 up = collider.transform.up;
        if (collider.direction == 0)
        {
            up = collider.transform.right;
        }
        else if (collider.direction == (int)CapsuleDirectionType.kAxisZ)
        {
            up = collider.transform.forward;
        }

        Vector3 start_pos = Vector3.zero;
        start_pos = collider.center + collider.transform.position + ((_height * up));
        Vector3 end_pos = Vector3.zero;
        end_pos = collider.center + collider.transform.position + ((_height * (up * -1.0f)));

        Gizmos.DrawWireSphere(start_pos, radius);
        Gizmos.DrawWireSphere(end_pos, radius);

        var offsets = new Vector3[] { new Vector3(-1.0f, 0.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f), new Vector3(0.0f, -1.0f, 0.0f) };
        for (int i = 0; i < offsets.Length; i++)
        {
            Gizmos.DrawLine(start_pos + offsets[i] * radius, end_pos + offsets[i] * radius);
        }

    }

    /// <summary>
    ///球内描画
    /// </summary>
    /// <param name="collider"></param>
    /// <returns></returns>
    public static void DrawSphere(SphereCollider collider)
    {
        float simulationRadius = collider.radius * collider.transform.localScale.x;
        Gizmos.DrawWireSphere(collider.center + collider.gameObject.transform.position, simulationRadius);
    }

    /// <summary>
    /// ボックス描画
    /// </summary>
    /// <param name="collider"></param>
    /// <returns></returns>
    public static void DrawBox(BoxCollider collider)
    {

        var preMatrix = Gizmos.matrix;

        // カプセル空間（(0, 0)からZ軸方向にカプセルが伸びる空間）からワールド座標系への変換行列
        Gizmos.matrix = Matrix4x4.TRS(collider.transform.position, collider.transform.rotation, collider.transform.lossyScale);

        Vector3 size = collider.transform.localScale;
        size.x *= collider.size.x;
        size.y *= collider.size.y;
        size.z *= collider.size.z;
        Vector3 pos = collider.transform.position + collider.center;

        Gizmos.DrawWireCube(Vector3.zero, size);

        Gizmos.matrix = preMatrix;

    }

#endif
}
