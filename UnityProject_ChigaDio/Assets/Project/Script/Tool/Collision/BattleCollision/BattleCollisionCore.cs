﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトルコリジョンコア
/// </summary>
public class BattleCollisionCore : Singleton<BattleCollisionCore>
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;

    /// <summary>
    /// セットアップ済みかどうか
    /// </summary>
    /// <returns></returns>
    public bool GetIsSetUP() { return is_setup_ & battle_collision_game_data_management_.GetIsSetUp(); }

    /// <summary>
    /// コリジョンデータ集
    /// </summary>
    private BattleCollisionGameDataManagement battle_collision_game_data_management_ = new BattleCollisionGameDataManagement();

    /// <summary>
    /// アタックオブジェクトのネーム
    /// </summary>
    private const string attack_object_name = "ATTACK";
    /// <summary>
    /// アタックオブジェクト
    /// </summary>
    public GameObject attack_object_;

    public void Start()
    {
        gameObject.name = "BattleCollisionCore";
        battle_collision_game_data_management_.Init();

        attack_object_ = new GameObject();
        attack_object_.transform.SetParent(transform);
        attack_object_.transform.localPosition = Vector3.zero;
        attack_object_.name = attack_object_name;
    }

    public void Update()
    {
        if (is_setup_ == false)
        {
            battle_collision_game_data_management_.Update();
            is_setup_ = battle_collision_game_data_management_.GetIsSetUp();
        }
    }

    public void LoadUnitCollision(int collision_id)
    {
        is_setup_ = false;

        battle_collision_game_data_management_.LoadUnitCollision(collision_id);
    }

    public void LoadBulletCollision(int collision_id)
    {
        is_setup_ = false;

        battle_collision_game_data_management_.LoadBulletCollision(collision_id);
    }

    /// <summary>
    /// アタックコリジョンデータを獲得
    /// </summary>
    /// <param name="id">UnitのID</param>
    /// <param name="collision_id">コリジョンID</param>
    /// <returns></returns>
    public List<BattleCollisionGameData> GetBattleCollisionGameData(int id, int collision_id)
    {
        if (is_setup_ == false) return null;
        if (battle_collision_game_data_management_ == null) return null;

        return battle_collision_game_data_management_.GetBattleCollisionGameData(id, collision_id);
    }

    /// <summary>
    /// アタックコリジョン作成
    /// </summary>
    /// <param name="id"></param>
    /// <param name="collision_id"></param>
    /// <param name="original_position"></param>
    /// <returns></returns>
    public BaseBattleCollisionGameSubstance CreateAttackCollision(int id, int collision_id,int skill_id,Vector3 original_position,BattleUnit owner_unit)
    {
        var list_data = GetBattleCollisionGameData(id, collision_id);
        if (list_data == null) return null;

        var collision =  new GameObject();
        collision.transform.SetParent(attack_object_.transform);

        collision.name = $"{id:0000}_Collision_{collision_id:0000}";

        var attack_collision = collision.AddComponent<BaseBattleCollisionGameSubstance>();

        attack_collision.transform.position = owner_unit.transform.position + owner_unit.transform.rotation * original_position;

        attack_collision.Init(list_data, SkillCore.Instance.GetSkillGameData(skill_id),owner_unit, owner_unit.transform.rotation);

        return attack_collision;
    }

    private void OnDestroy()
    {
        battle_collision_game_data_management_.ReleaseAll();
    }
}
