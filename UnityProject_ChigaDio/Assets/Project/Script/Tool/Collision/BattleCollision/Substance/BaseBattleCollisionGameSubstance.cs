﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// コリジョン(ゲーム)
/// </summary>
public class BaseBattleCollisionGameSubstance : MonoBehaviour
{

    /// <summary>
    /// コリジョン
    /// </summary>
    [SerializeField]
    protected BaseCollisionConstruction collision_;

    /// <summary>
    /// リスト：バトル詳細
    /// </summary>
    protected List<BattleCollisionDetails> list_battle_collision_details_ = new List<BattleCollisionDetails>();

    /// <summary>
    /// 所有者
    /// </summary>
    protected BattleUnit owner_unit_ = null;

    /// <summary>
    /// スキルデータ
    /// </summary>
    protected SkillGameData skill_game_data_ = null;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="data"></param>
    public void Init(List<BattleCollisionGameData> data,SkillGameData skill_data,BattleUnit value_owner_unit,Quaternion start_rotation)
    {
        owner_unit_ = value_owner_unit;
        skill_game_data_ = skill_data;
        foreach (var collision_data in data)
        {
            SphereCollider collider = gameObject.AddComponent<SphereCollider>();

            collider.radius = collision_data.GetStartRadius();
            collider.center = new Vector3(collision_data.GetStartPositionX(), collision_data.GetStartPositionY(), collision_data.GetStartPositionZ());

            var details = new BattleCollisionDetails();
            details.SetUp(collider, collision_data);
            list_battle_collision_details_.Add(details);
        }
        transform.rotation = start_rotation;
        collision_ = new BaseCollisionConstruction();
        collision_.Init(gameObject, CollisionLayer.kAttack, CollisionPushType.kNone,gameObject.layer);
        collision_.RpgCollisionDetailsControl.HitSecondFunc = SecondEnter;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDetails();
    }

    /// <summary>
    /// 詳細更新
    /// </summary>
    protected void UpdateDetails()
    {
        bool destroy_flag = true;
        foreach(var data in list_battle_collision_details_)
        {
            if (destroy_flag == true)
            {
                destroy_flag = data.Update();
            }
            else
            {
                data.Update();
            }
        }

        if(destroy_flag == true)
        {
            Object.Destroy(gameObject);
        }
    }

    void SecondEnter(Collider collider,Collider this_collider)
    {
        var unit = collider.gameObject.GetComponentInParent<BattleUnit>();
        if (unit == null) return;
        if (unit == owner_unit_) return;
        if (unit.GetUnitType() == owner_unit_.GetUnitType()) return;

        SkillResult result = new SkillResult();
        result.SetUp(owner_unit_, skill_game_data_);
        result.AddResultOpponentUnit(unit);
        result.Calculation();

        foreach(var data in result.GetSkillPoint())
        {
            GameUICore.Instance.BattleNumber(data,unit.transform.position);
        }

        unit.KnockBackSetUp(owner_unit_,result.GetSkillPoint()[0]);
    }
}
