﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトルコリジョンデータ（フレーム計算など）
/// </summary>
public class BattleCollisionDetails
{
    /// <summary>
    /// コリジョンデータ
    /// </summary>
    private BattleCollisionGameData collision_data_ = null;

    /// <summary>
    /// チェンジ切り替えフラグ
    /// </summary>
    private bool is_change_start_ = false;

    /// <summary>
    /// 計算フレーム数(切り替え)
    /// </summary>
    private float add_frame_change_time_ = 0.0f;

    /// <summary>
    /// 計算フレーム数(生存)
    /// </summary>
    private float add_frame_life_time_ = 0.0f;

    /// <summary>
    /// 切り替え後のラジウス
    /// </summary>
    private float change_radisu_ = 0.0f;

    /// <summary>
    /// 切り替え後の座標
    /// </summary>
    private Vector3 change_position_ = Vector3.zero;

    /// <summary>
    /// コライダー
    /// </summary>
    private SphereCollider collider_ = null;


    public void SetUp(SphereCollider value_collider,BattleCollisionGameData value_data)
    {
        collision_data_ = value_data;
        collider_ = value_collider;

        add_frame_change_time_ = 0.0f;

        change_radisu_ = collision_data_.GetStartRadius();
        change_position_ = new Vector3(collision_data_.GetStartPositionX(), collision_data_.GetStartPositionY(), collision_data_.GetStartPositionZ());
    }

    public bool Update()
    {
        if (collider_ == null) return false;

        if (is_change_start_ == false)
        {
            add_frame_change_time_ += Time.deltaTime;
            if (add_frame_change_time_ >= collision_data_.GetStartFrameChange() * (1.0f / 60.0f))
            {
                is_change_start_ = true;
                add_frame_change_time_ = 0.0f;
            }
        }
        else

        {
            add_frame_change_time_ += Time.deltaTime;
            add_frame_change_time_ = Mathf.Min(add_frame_change_time_, collision_data_.GetEndFrameChange() * (1.0f / 60.0f));

            float lerp_clamp = add_frame_change_time_ / (collision_data_.GetEndFrameChange() * (1.0f / 60.0f));

            change_radisu_ = Mathf.Lerp(collision_data_.GetStartRadius(), collision_data_.GetEndRadius(), lerp_clamp);

            Vector3 start_pos = new Vector3(collision_data_.GetStartPositionX(), collision_data_.GetStartPositionY(), collision_data_.GetStartPositionZ());
            Vector3 end_pos = new Vector3(collision_data_.GetEndPositionX(), collision_data_.GetEndPositionY(), collision_data_.GetEndPositionZ());

            change_position_ = Vector3.Lerp(start_pos, end_pos, lerp_clamp);

            collider_.radius = change_radisu_;
            collider_.center = change_position_;
        }

        add_frame_life_time_ += Time.deltaTime;
        if(add_frame_life_time_ >= collision_data_.GetFrameLife() * (1.0 / 60.0f))
        {
            return true;
        }

        return false;
    }
}
