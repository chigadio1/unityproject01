﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BattleCollisionGameDataをコントロールする
/// </summary>
public class BattleCollisionGameDataControl
{
    /// <summary>
    /// リスト
    /// </summary>
    Dictionary<int, List<BattleCollisionGameData>> dictionary_battle_collision_game_data_ = new Dictionary<int, List<BattleCollisionGameData>>();

    public void Init()
    {
        dictionary_battle_collision_game_data_ = new Dictionary<int, List<BattleCollisionGameData>>();
    }

    /// <summary>
    /// 追加
    /// </summary>
    /// <param name="list_motion_data"></param>
    public void AddListMotionData(List<BattleCollisionData> list_collision_data)
    {
        if (list_collision_data == null) return;
        if (list_collision_data.Count == 0) return;

        foreach (var data in list_collision_data)
        {
            BattleCollisionGameData collision_data = new BattleCollisionGameData();
            collision_data.DataValue(data);
            int id = collision_data.GetCollisionID();
            if (dictionary_battle_collision_game_data_.ContainsKey(id) == false)
            {
                dictionary_battle_collision_game_data_.Add(id, new List<BattleCollisionGameData>());
            }
            dictionary_battle_collision_game_data_[id].Add(collision_data);
        }
    }

    public List<BattleCollisionGameData> GetBattleCollisionGameData(int collision_id)
    {
        if (dictionary_battle_collision_game_data_ == null) return null;
        if (dictionary_battle_collision_game_data_.Count == 0) return null;
        if (dictionary_battle_collision_game_data_.ContainsKey(collision_id) == false) return null;

        if (dictionary_battle_collision_game_data_[collision_id].Count == 0) return null;

        return dictionary_battle_collision_game_data_[collision_id];
    }

    public BattleCollisionGameData GetMotionActionData(int collision_id, int index)
    {
        if (dictionary_battle_collision_game_data_ == null) return null;
        if (dictionary_battle_collision_game_data_.Count == 0) return null;
        if (dictionary_battle_collision_game_data_.ContainsKey(collision_id) == false) return null;

        if (dictionary_battle_collision_game_data_[collision_id].Count == 0) return null;

        if (dictionary_battle_collision_game_data_[collision_id].Count <= index || index < 0) return null;

        return dictionary_battle_collision_game_data_[collision_id][index];
    }


    public uint GetMotionActionLength(int collision_id)
    {
        if (dictionary_battle_collision_game_data_ == null) return 0;
        if (dictionary_battle_collision_game_data_.Count == 0) return 0;
        if (dictionary_battle_collision_game_data_.ContainsKey(collision_id) == false) return 0;

        if (dictionary_battle_collision_game_data_[collision_id].Count == 0) return 0;


        return (uint)dictionary_battle_collision_game_data_[collision_id].Count;
    }


    public void Release()
    {
        if (dictionary_battle_collision_game_data_ == null) return;

        dictionary_battle_collision_game_data_.Clear();
        dictionary_battle_collision_game_data_ = null;
    }
}
