﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(BattleCollisionScriptableObject))]
public class EditorBattleCollisionScriptableObject : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<BattleCollisionData>(target as BaseExcelScriptableObject<BattleCollisionData>);
       
        base.OnInspectorGUI();
    }
}


#endif

/// <summary>
/// バトルコリジョンデータスクリプタブルオブジェクト
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreareCollisionData")]
public class BattleCollisionScriptableObject : BaseExcelScriptableObject<BattleCollisionData>
{

}
