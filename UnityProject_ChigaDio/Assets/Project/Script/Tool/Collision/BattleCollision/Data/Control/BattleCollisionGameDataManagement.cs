﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// コリジョンデータをすべて所持するClass
/// </summary>
public class BattleCollisionGameDataManagement 
{
    /// <summary>
    /// リスト(バトルコリジョン)
    /// </summary>
    Dictionary<int, BattleCollisionGameDataControl> dictionary_battle_collision_game_data_control_ = new Dictionary<int, BattleCollisionGameDataControl>();

    /// <summary>
    /// スクリプタブルオブジェクトリスト
    /// </summary>
    Dictionary<int,AddressableData<BattleCollisionScriptableObject>> dictionary_battle_collision_scriptable_object_ = new Dictionary<int, AddressableData<BattleCollisionScriptableObject>>();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        is_setup_ = false;
    }

    /// <summary>
    /// ロード
    /// </summary>
    /// <param name="collision_id"></param>
    public void LoadUnitCollision(int collision_id)
    {
        is_setup_ = false;

        if (dictionary_battle_collision_scriptable_object_.ContainsKey(collision_id) == true) return;
        if (dictionary_battle_collision_game_data_control_.ContainsKey(collision_id) == true) return;

        var data = new AddressableData<BattleCollisionScriptableObject>();
        data.LoadStart($"Assets/Project/Assets/Data/Collision/Unit/{collision_id:0000}/{collision_id:0000}.asset");

        dictionary_battle_collision_scriptable_object_.Add(collision_id, data);
    }

    /// <summary>
    /// ロード
    /// </summary>
    /// <param name="collision_id"></param>
    public void LoadBulletCollision(int collision_id)
    {
        is_setup_ = false;

        if (dictionary_battle_collision_scriptable_object_.ContainsKey(collision_id) == true) return;
        if (dictionary_battle_collision_game_data_control_.ContainsKey(collision_id) == true) return;

        var data = new AddressableData<BattleCollisionScriptableObject>();
        data.LoadStart($"Assets/Project/Assets/Data/Collision/Bullet/{collision_id:0000}/{collision_id:0000}.asset");

        dictionary_battle_collision_scriptable_object_.Add(collision_id, data);
    }

    public void Update()
    {
        SetUp();
    }

    private void SetUp()
    {
        if (is_setup_ == true) return;

        is_setup_ = true;

        foreach(var data in dictionary_battle_collision_scriptable_object_)
        {
            if (data.Value.GetFlagSetUpLoading() == false)
            {
                is_setup_ = false;
                return;
            }
        }

        foreach(var data in dictionary_battle_collision_scriptable_object_)
        {
            var collision_data = data.Value.GetAddressableData();
            dictionary_battle_collision_game_data_control_.Add(data.Key, new BattleCollisionGameDataControl());
            dictionary_battle_collision_game_data_control_[data.Key].AddListMotionData(collision_data.GetListData());
        }

        foreach(var data in dictionary_battle_collision_scriptable_object_)
        {
            data.Value.Release();
        }

        dictionary_battle_collision_scriptable_object_.Clear();


    }

    public List<BattleCollisionGameData> GetBattleCollisionGameData(int id,int collision_id)
    {
        if (dictionary_battle_collision_game_data_control_ == null) return null;

        if (dictionary_battle_collision_game_data_control_.Count == 0) return null;
        if (dictionary_battle_collision_game_data_control_.ContainsKey(id) == false) return null;

        if (dictionary_battle_collision_game_data_control_[id] == null) return null;

        return dictionary_battle_collision_game_data_control_[id].GetBattleCollisionGameData(collision_id);
    }

    public void ReleaseAll()
    {
        foreach(var col in dictionary_battle_collision_scriptable_object_)
        {
            col.Value.Release();
        }

        dictionary_battle_collision_scriptable_object_.Clear();
        dictionary_battle_collision_game_data_control_.Clear();
    }
}
