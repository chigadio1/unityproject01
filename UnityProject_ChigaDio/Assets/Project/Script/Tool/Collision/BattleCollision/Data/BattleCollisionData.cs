﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトルコリジョンデータ
/// </summary>
[System.Serializable]
public class BattleCollisionData : BaseComposition
{
    /// <summary>
    /// コリジョンタイプ
    /// </summary>
    public enum CollisionType
    {
        Attack
    }


    /// <summary>
    /// ID
    /// </summary>
    [SerializeField]
    private int collision_id_ = 0;
    public int GetCollisionID() { return collision_id_; }

    /// <summary>
    /// 何フレーム生存するか
    /// </summary
    [SerializeField]
    private float frame_life_ = 0.0f;
    public  float GetFrameLife() { return frame_life_; }

    /// <summary>
    /// 何フレームで切り替わりを始めるか()
    /// </summary>
    [SerializeField]
    private float frame_change_start_ = 0.0f;
    public float GetStartFrameChange() { return frame_change_start_; }

    /// <summary>
    /// 切り替わり開始してから何フレームで終了するか
    /// </summary>
    [SerializeField]
    private float frame_change_end_ = 0.0f;
    public float GetEndFrameChange() { return frame_change_end_; }

    /// <summary>
    /// コリジョンタイプ
    /// </summary>
    [SerializeField]
    private CollisionType battle_collision_type_ = CollisionType.Attack;
    public CollisionType GetBattleCollisionType() { return battle_collision_type_; }

    /// <summary>
    /// 初期座標(X)
    [SerializeField]
    private float start_position_x_ = 0.0f;
    public float GetStartPositionX() { return start_position_x_; }

    /// <summary>
    /// 初期座標(Y)
    [SerializeField]
    private float start_position_y_ = 0.0f;
    public float GetStartPositionY() { return start_position_y_; }

    /// <summary>
    /// 初期座標(Z)
    /// </summary>
    [SerializeField]
    private float start_position_z_ = 0.0f;
    public float GetStartPositionZ() { return start_position_z_; }

    /// <summary>
    /// 終了座標(X)
    /// </summary>
    [SerializeField]
    private float end_position_x_ = 0.0f;
    public float GetEndPositionX() { return end_position_x_; }

    /// <summary>
    /// 終了座標(Y)
    /// </summary>
    [SerializeField]
    private float end_position_y_ = 0.0f;
    public float GetEndPositionY() { return end_position_y_; }

    /// <summary>
    /// 終了座標(Z)
    /// </summary>
    [SerializeField]
    private float end_position_z_ = 0.0f;
    public float GetEndPositionZ() { return end_position_z_; }

    /// <summary>
    /// 初期サイズ
    /// </summary>
    [SerializeField]
    private float start_radius_ = 0.0f;
    public float GetStartRadius() { return start_radius_; }

    /// <summary>
    /// 終了サイズ
    /// </summary>
    [SerializeField]
    private float end_radius_ = 0.0f;
    public float GetEndRadius() { return end_radius_; }

    public override void ManualSetUp(ref DataFrameGroup data_grop, ref ProjectSystem.ExcelSystem.DataGroup excel_data_group)
    {
        data_grop.AddData("ID", nameof(collision_id_), collision_id_);
        data_grop.AddData("FrameLife", nameof(frame_life_), frame_life_);
        data_grop.AddData("FrameChange_Start", nameof(frame_change_start_), frame_change_start_);
        data_grop.AddData("FrameChange_End", nameof(frame_change_end_), frame_change_end_);
        data_grop.AddData("CollisionType", nameof(battle_collision_type_), battle_collision_type_);
        data_grop.AddData("StartPosition_X", nameof(start_position_x_), start_position_x_);
        data_grop.AddData("StartPosition_Y", nameof(start_position_y_), start_position_y_);
        data_grop.AddData("StartPosition_Z", nameof(start_position_z_), start_position_z_);
        data_grop.AddData("EndPosition_X", nameof(end_position_x_), end_position_x_);
        data_grop.AddData("EndPosition_Y", nameof(end_position_y_), end_position_y_);
        data_grop.AddData("EndPosition_Z", nameof(end_position_z_), end_position_z_);
        data_grop.AddData("Start_Radius", nameof(start_radius_), start_radius_);
        data_grop.AddData("End_Radius", nameof(end_radius_), end_radius_);
    }

}
