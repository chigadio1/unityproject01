﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BattleCollisionDataからデータを受け取り保持するクラス
/// </summary>
public class BattleCollisionGameData
{
    /// <summary>
    /// ID
    /// </summary>
    private int collision_id_ = 0;
    public int GetCollisionID() { return collision_id_; }

    /// <summary>
    /// 何フレーム生存するか
    /// </summary
    private float frame_life_ = 0.0f;
    public float GetFrameLife() { return frame_life_; }

    /// <summary>
    /// 何フレームで切り替わりを始めるか()
    /// </summary>
    private float frame_change_start_ = 0.0f;
    public float GetStartFrameChange() { return frame_change_start_; }

    /// <summary>
    /// 切り替わり開始してから何フレームで終了するか
    /// </summary>
    private float frame_change_end_ = 0.0f;
    public float GetEndFrameChange() { return frame_change_end_; }


    /// <summary>
    /// コリジョンタイプ
    /// </summary>
    private BattleCollisionData.CollisionType battle_collision_type_ = BattleCollisionData.CollisionType.Attack;
    public BattleCollisionData.CollisionType GetBattleCollisionType() { return battle_collision_type_; }

    /// <summary>
    /// 初期座標(X)
    private float start_position_x_ = 0.0f;
    public float GetStartPositionX() { return start_position_x_; }

    /// <summary>
    /// 初期座標(Y)
    private float start_position_y_ = 0.0f;
    public float GetStartPositionY() { return start_position_y_; }

    /// <summary>
    /// 初期座標(Z)
    /// </summary>
    private float start_position_z_ = 0.0f;
    public float GetStartPositionZ() { return start_position_z_; }

    /// <summary>
    /// 終了座標(X)
    /// </summary>
    private float end_position_x_ = 0.0f;
    public float GetEndPositionX() { return end_position_x_; }

    /// <summary>
    /// 終了座標(Y)
    /// </summary>
    private float end_position_y_ = 0.0f;
    public float GetEndPositionY() { return end_position_y_; }

    /// <summary>
    /// 終了座標(Z)
    /// </summary>
    private float end_position_z_ = 0.0f;
    public float GetEndPositionZ() { return end_position_z_; }

    /// <summary>
    /// 初期サイズ
    /// </summary>
    private float start_radius_ = 0.0f;
    public float GetStartRadius() { return start_radius_; }

    /// <summary>
    /// 終了サイズ
    /// </summary>
    private float end_radius_ = 0.0f;
    public float GetEndRadius() { return end_radius_; }

    /// <summary>
    /// データを入れる
    /// </summary>
    /// <param name="value_data"></param>
    public void DataValue(BattleCollisionData value_data)
    {
        collision_id_ = value_data.GetCollisionID();
        frame_life_ = value_data.GetFrameLife();
        frame_change_start_ = value_data.GetStartFrameChange();
        frame_change_end_ = value_data.GetEndFrameChange();
        battle_collision_type_ = value_data.GetBattleCollisionType();
        start_position_x_ = value_data.GetStartPositionX();
        start_position_y_ = value_data.GetStartPositionY();
        start_position_z_ = value_data.GetStartPositionZ();
        end_position_x_ = value_data.GetEndPositionX();
        end_position_y_ = value_data.GetEndPositionY();
        end_position_z_ = value_data.GetEndPositionZ();
        start_radius_ = value_data.GetStartRadius();
        end_radius_ = value_data.GetEndRadius();
    }
}
