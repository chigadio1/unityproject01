﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// デバックコリジョン管理
/// </summary>
public class DebugCollisionControl
{
    /// <summary>
    /// リスト
    /// </summary>
    private List<DebugCollision> list_debug_collision_ = new List<DebugCollision>();

    /// <summary>
    /// デバッグ描画
    /// </summary>
    private bool is_debug_draw_ = false;
    public void OnDebugDraw()
    {
        if (is_debug_draw_ == true) return;
        foreach(var debug in list_debug_collision_)
        {
            debug.OnDraw();
        }
        is_debug_draw_ = true;
    }
    public void OffDebugDraw()
    {
        if (is_debug_draw_ == false) return;
        foreach (var debug in list_debug_collision_)
        {
            debug.OffDraw();
        }
        is_debug_draw_ = false;
    }
    public bool GetFlagDraw() { return is_debug_draw_; }


    public void Init()
    {
        if (list_debug_collision_ == null) list_debug_collision_ = new List<DebugCollision>();
    }

    public void Update()
    {
        ReleaseUpdate();
        AutoAddCollision();
        ManualUpdate();
    }

    private void ManualUpdate()
    {
        foreach (var debug in list_debug_collision_)
        {
            debug.Update();
        }
    }

    private void AutoAddCollision()
    {
        var collision =  RpgCollisionControl.Instance.ListAllCollisionConstruction;
        if (collision == null) return;

        foreach(var collision_obj in collision)
        {
            int count = 0;
            foreach(var collision_details in collision_obj.RpgCollisionDetailsControl.ListRpgCollisionDetaile)
            {
                if (SearchFlag(collision_details.ThisObject) == true) continue;
              
                DebugCollision debug = new DebugCollision();
                debug.Init(collision_obj,collision_details, RpgCollisionControl.Instance.gameObject, collision_details.ThisObject.name+ "_ID_" + $"{count:0000}");
                count++;
                list_debug_collision_.Add(debug);
            }
        }
    }

    private  void ReleaseUpdate()
    {
        list_debug_collision_.RemoveAll(debug => debug.GetRelease() == true);
    }

    private bool SearchFlag(GameObject object_search)
    {
        if (SearchCollision(object_search) != null) return true;
        return false;
    }

    private DebugCollision SearchCollision(GameObject object_search)
    {
        if (list_debug_collision_ == null) return null;
        if (list_debug_collision_.Count == 0) return null;

        return list_debug_collision_.Find(collision => collision.GetInstanceID() == object_search.GetInstanceID());
    }
}
