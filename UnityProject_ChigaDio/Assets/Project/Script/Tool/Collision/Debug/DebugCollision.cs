﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
/// <summary>
/// デバッグコリジョン
/// </summary>
public class DebugCollision
{
    /// <summary>
    /// コリジョン詳細
    /// </summary>
    private RpgCollisionDetails collision_details_ = null;

    /// <summary>
    /// コリジョンの詳細を管理
    /// </summary>
    private BaseCollisionConstruction collision_construction_ = null;

    /// <summary>
    /// 描画するかどうか
    /// </summary>
    private bool is_draw_ = false;

    public void OnDraw() {  DrawMesh(); is_draw_ = true; }
    public void OffDraw() { DrawDisable(); is_draw_ = false; }

    /// <summary>
    /// リリースフラグ
    /// </summary>
    private bool is_release_ = false;
    public bool GetRelease() { return is_release_; }

    /// <summary>
    /// ゲームオブジェクト
    /// </summary>
    private GameObject game_object_ = null;
    public GameObject GetGameObject() { return game_object_; }

    /// <summary>
    /// インスタンスID
    /// </summary>
    private int instance_id_ = 0;
    public int GetInstanceID() { return instance_id_; }

    /// <summary>
    /// カラー
    /// </summary>
    private Color dbg_collision_color_ = Color.red;

    /// <summary>
    /// メッシュ
    /// </summary>
    private MeshRenderer mesh_renderer_ = null;

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="value"></param>
    public void Init(BaseCollisionConstruction value_collision_construction,RpgCollisionDetails value, GameObject value_parent,string value_name)
    {
        collision_construction_ = value_collision_construction;
        collision_details_ = value;

        var obj = new GameObject();
        obj.transform.SetParent(value_parent.transform);
        obj.gameObject.name = value_name;
        game_object_ = obj;

        instance_id_ = value.ThisObject.GetInstanceID();
        

        UpdateTransform();
    }

    public void Update()
    {

        if (Release() == true || is_release_ == true) return;
        UpdateTransform();
        UpdateColor();
    }

    public void UpdateColor()
    {
        if (mesh_renderer_ == null) return;

        switch (collision_construction_.CollisionLayer)
        {
            case CollisionLayer.kNone:
                dbg_collision_color_ = Color.white;
                break;
            case CollisionLayer.kPlayer:
                dbg_collision_color_ = Color.green;
                break;
            case CollisionLayer.kEnemy:
                dbg_collision_color_ = Color.red;
                break;
            case CollisionLayer.kNoneUnit:
                dbg_collision_color_ = Color.cyan;
                break;
            case CollisionLayer.kBackGround:
                dbg_collision_color_ = Color.blue;
                break;
        }

        ///透過設定
        dbg_collision_color_.a = 0.3f;
        mesh_renderer_.sharedMaterial.SetColor("_BaseColor", dbg_collision_color_);
    }

    /// <summary>
    /// 座標設定
    /// </summary>
    private void UpdateTransform()
    {
        if (collision_construction_ == null) return;
        if (collision_details_ == null) return;
        if (game_object_ == null) return;

        Vector3 center_position = Vector3.zero;
        Collider collider = collision_details_.RpgCollision;

        if (collider == null) return;

        if(collider.GetType() == typeof(BoxCollider))
        {
            BoxCollider box = collider as BoxCollider;
            center_position = box.center;

            Vector3 size = collider.transform.localScale;
            size.x *= box.size.x;
            size.y *= box.size.y;
            size.z *= box.size.z;
            game_object_.transform.localScale = size;
        }
        else if(collider.GetType() == typeof(SphereCollider))
        {
            SphereCollider sphere = collider as SphereCollider;
            center_position = sphere.center;
            float size = sphere.radius * collider.transform.localScale.x;
            game_object_.transform.localScale = new Vector3(size,size,size);
        }
        else if(collider.GetType() == typeof(CapsuleCollider))

        {
            CapsuleCollider capsule = collider as CapsuleCollider;
            center_position = capsule.center;

            Vector3 euler_angle = collision_details_.ThisObject.transform.eulerAngles;
            Vector3 add_euler = Vector3.zero;
            if (capsule.direction == 0)
            {
                add_euler = new Vector3(0.0f, 0.0f, 90.0f);
            }
            else if (capsule.direction == 1)
            {
                add_euler = new Vector3(0.0f, 0.0f, 0.0f);
            }
            else if(capsule.direction == 2)
            {
                add_euler = new Vector3(90.0f, 0.0f, 0.0f);
            }


            game_object_.transform.rotation = Quaternion.Euler(euler_angle.x + add_euler.x, euler_angle.y + add_euler.y, euler_angle.z + add_euler.z);


            Vector3 capsuleLocalScale = collision_details_.ThisObject.transform.localScale;
            float radius = capsule.radius;

            float max_scale_xz = Mathf.Max(capsuleLocalScale.x, capsuleLocalScale.z);
            float max_scale_y = capsuleLocalScale.y;




            float newCapsuleLocalScaleX = max_scale_xz * radius * 2f;
            float newCapsuleLocalScaleY = max_scale_y * capsule.height * 0.5f;
            float newCapsuleLocalScaleZ = max_scale_xz * radius * 2f;
            game_object_.transform.localScale = new Vector3(newCapsuleLocalScaleX, newCapsuleLocalScaleY, newCapsuleLocalScaleZ);
        }

        game_object_.transform.position = collision_details_.ThisObject.transform.position + center_position ;


    }

    private void DrawMesh()
    {
        if (is_draw_ == true) return;
        if (game_object_.GetComponent<MeshRenderer>() != null)
        {
            game_object_.GetComponent<MeshRenderer>().enabled = true;
            return;
        }
        if (game_object_.GetComponent<MeshFilter>() != null) return;

        PrimitiveType type = PrimitiveType.Cube;
        Collider collider = collision_details_.RpgCollision;
        if (collider.GetType() == typeof(BoxCollider))
        {
            type = PrimitiveType.Cube;
        }
        else if (collider.GetType() == typeof(SphereCollider))
        {
            type = PrimitiveType.Sphere;
        }
        else if (collider.GetType() == typeof(CapsuleCollider))

        {
            type = PrimitiveType.Capsule;
        }


        var obj = GameObject.CreatePrimitive(type);

        var mesh = game_object_.AddComponent<MeshFilter>();
        mesh.mesh = obj.GetComponent<MeshFilter>().mesh;
        
        Object.Destroy(obj);
        mesh_renderer_ =  game_object_.AddComponent<MeshRenderer>();

        mesh_renderer_.material = new Material(Shader.Find("Unlit/S_DbgUnlitCollision"));

        //色
        mesh_renderer_.sharedMaterial.SetColor("_BaseColor", dbg_collision_color_);
        mesh_renderer_.sharedMaterial.renderQueue = 3000 + (-10);
        mesh_renderer_.sharedMaterial.SetFloat("_TransparentSortPriority", (-10));
        mesh_renderer_.material.color = dbg_collision_color_;
    }

    private void DrawDisable()
    {
        if (is_draw_ == false) return;
        if (is_release_) return;
        if (game_object_ == null) return;
        if (game_object_.GetComponent<MeshFilter>() == null) return;
        if (game_object_.GetComponent<MeshRenderer>() == null) return;

       
        game_object_.GetComponent<MeshRenderer>().enabled = false;
    }

    /// <summary>
    /// 解放
    /// </summary>
    /// <returns></returns>
    private bool Release()
    {
        if (collision_details_ != null)
        {
            if (collision_details_.ThisObject != null) return false;
        }
        if (game_object_ == null) return true;
        
        Object.Destroy(game_object_);

        is_release_ = true;

        return true;
    }


}
