﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(RpgCollisionControl))]
public class EditorCollisionControlInspector : Editor
{
    private bool is_dbg_draw_ = false;

    public override void OnInspectorGUI()
    {
        var target_control = target as RpgCollisionControl;



        is_dbg_draw_ = target_control.GetDbgControl().GetFlagDraw();
        is_dbg_draw_ = GUILayout.Toggle(is_dbg_draw_, "Draw");

        if (is_dbg_draw_) target_control.GetDbgControl().OnDebugDraw();
        else target_control.GetDbgControl().OffDebugDraw();

    }
}

#endif
