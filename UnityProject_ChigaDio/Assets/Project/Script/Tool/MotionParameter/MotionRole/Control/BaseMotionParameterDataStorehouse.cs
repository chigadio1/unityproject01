﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// モーションパラメータデータ保管庫(基礎)
/// </summary>
public class BaseMotionParameterDataStorehouse<T> where T : BaseMotionParameterRoleDataControl,new()
{
    /// <summary>
    /// モーションデータ辞書
    /// </summary>
    private Dictionary<int, T> dictionary_motion_data_ = new Dictionary<int, T>(); 

    /// <summary>
    /// モーションデータ
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public T GetMotionParameterRoleData(int search_id)
    {
        if (dictionary_motion_data_.ContainsKey(search_id) == false) return default(T);

        return dictionary_motion_data_[search_id];
    }

    /// <summary>
    /// アクション実行
    /// </summary>
    /// <param name="detailes"></param>
    /// <param name="details_data"></param>
    public void PlayInit(MotionParameterDetails detailes, MotionParameterDetailsData details_data)
    {
        if (dictionary_motion_data_.ContainsKey(details_data.GetActionID()) == true) return;
        T data = new T();
        data.Init(detailes, details_data);

        dictionary_motion_data_.Add(details_data.GetActionID(), data);

    }

    public void Update()
    {
        List<int> list_remove = new List<int>();
        foreach(var data in dictionary_motion_data_)
        {
            data.Value.Updata();
            data.Value.AnotherUpdate();
            if (data.Value.GetLifeOver() == true) list_remove.Add(data.Key);
        }

    }

    public void AutoRelease()
    {
        List<int> list_remove = new List<int>();

        foreach (var data in dictionary_motion_data_)
        {
            if (data.Value.GetLifeOver() == true) list_remove.Add(data.Key);
        }

        foreach (var key in list_remove)
        {
            dictionary_motion_data_.Remove(key);
        }
    }

    public void ReleaseAll()
    {
        dictionary_motion_data_.Clear();
    }
}
