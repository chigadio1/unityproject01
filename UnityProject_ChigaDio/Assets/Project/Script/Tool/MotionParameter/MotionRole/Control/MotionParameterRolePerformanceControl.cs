﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// モーション役割実行コントロール
/// </summary>
public class MotionParameterRolePerformanceControl
{
    /// <summary>
    /// 実行リスト
    /// </summary>
    private List<BaseMotionParameterRolePerformance> list_motion_parameter_role_ = new List<BaseMotionParameterRolePerformance>();


    public void Init(MotionParameterRoleDataControlManagement details, BattleUnit unit)
    {


        AllInit();

    }

    /// <summary>
    /// モーション役割実行
    /// </summary>
    public void PlayInit(MotionParameterDetailsData data_details, MotionParameterRoleDataControlManagement role_control , BattleUnit unit)
    {
        var motion_performance = CreateMotionParameterPerformance(MotionParameterConvert.ConvertMotionActionRoleNameID(data_details.GetActionRoleName()));
        if (motion_performance == null) return;

        motion_performance.Init(data_details.GetActionID(),role_control.GetMotionParameterRoleDataControl(),unit);
        list_motion_parameter_role_.Add(motion_performance);
    }





    private BaseMotionParameterRolePerformance CreateMotionParameterPerformance(MotionParameterConvert.MotionActionRoleName type)
    {
        switch (type)
        {
            case MotionParameterConvert.MotionActionRoleName.Transform_Position:
                return new MotionActionRole.TransformPositionPerformance();
            case MotionParameterConvert.MotionActionRoleName.Transform_Target_TeleportPosition:
                return new MotionActionRole.TransformTargetTeleportPositionPerformance();
            case MotionParameterConvert.MotionActionRoleName.Transform_Target_Homing:
                return new MotionActionRole.TransformTargetHomingPerformance();
            case MotionParameterConvert.MotionActionRoleName.Transform_Target_LookRotation:
                return new MotionActionRole.TransformTargetLookRotationPerformance();
            case MotionParameterConvert.MotionActionRoleName.Animation_Change:
                return new MotionActionRole.AnimationChangePerformance();
            case MotionParameterConvert.MotionActionRoleName.Collision_Attack:
                return new MotionActionRole.CollisionAttackPerformance();
            case MotionParameterConvert.MotionActionRoleName.Collision_Sensing:
                return new MotionActionRole.CollisionSensingPerformance();
            case MotionParameterConvert.MotionActionRoleName.Effect_Generate:
                return new MotionActionRole.EffectGeneratePerformance();
            case MotionParameterConvert.MotionActionRoleName.Action_End:
                break;
            case MotionParameterConvert.MotionActionRoleName.Max_Action:
                break;
            case MotionParameterConvert.MotionActionRoleName.Bullet_Generate:
                return new MotionActionRole.BulletGeneratePerformance();
        }

        return null;

    }

    public void AllInit()
    {
        list_motion_parameter_role_.Clear();
    }

    public void Update(MotionParameterRoleDataControlManagement details, BattleUnit unit)
    {
        var control = details.GetMotionParameterRoleDataControl();
        foreach (var role in list_motion_parameter_role_)
        {
            role.Updata(control, unit);
        }


        list_motion_parameter_role_.RemoveAll(data => control.GetMotionParameterDataActive(data.GetRoleActionName(),data.GetRoleDataID()) == false);
    }

}
