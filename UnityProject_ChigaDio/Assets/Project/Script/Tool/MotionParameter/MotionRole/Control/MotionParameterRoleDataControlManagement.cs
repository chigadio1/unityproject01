﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// モーションパラメータ
/// </summary>
public class MotionParameterRoleDataControlManagement
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp()
    {
        return is_setup_ && motion_parameter_details_.GetIsSetUp();
    }

    /// <summary>
    /// モーションパラメータ実行管理
    /// </summary>
    private MotionParameterRolePerformanceControl motion_parameter_role_performance_control_ = new MotionParameterRolePerformanceControl();

    /// <summary>
    /// バトルユニット
    /// </summary>
    private BattleUnit unit_ = null;


    /// <summary>
    /// 再生中
    /// </summary>
    private bool is_play_ = false;
    public bool GetIsPlay() { return is_play_; }

    /// <summary>
    /// キーフレーム
    /// </summary>
    private float key_fraem_ = 0.0f;

    /// <summary>
    /// アクションID
    /// </summary>
    private uint action_id_ = (999999);

    /// <summary>
    /// アクションIndex
    /// </summary>
    private uint action_index_ = 0;

    /// <summary>
    /// 役割管理
    /// </summary>
    private MotionParameterRoleDataControl motion_parameter_role_control_ = new MotionParameterRoleDataControl();
    public MotionParameterRoleDataControl GetMotionParameterRoleDataControl() { return motion_parameter_role_control_; }

   



    /// <summary>
    /// ストップフラグ
    /// </summary>
    private bool is_stop_ = false;
    public void OnStop() { is_setup_ = true; }
    public void OffStop() { is_setup_ = false; }

    /// <summary>
    /// アクション終了フラグ
    /// </summary>
    private bool is_action_end_ = true;
    public bool GetIsActionEnd() { return is_action_end_; }

    /// <summary>
    /// 目標設定
    /// </summary>
    public void SetTargetObject(GameObject value_target)
    {
        if (motion_parameter_details_ == null) return;
        motion_parameter_details_.SetTargetObject(value_target);
    }

    /// <summary>
    /// データ集
    /// </summary>
    private MotionParameterDetails motion_parameter_details_ = new MotionParameterDetails();

    public void Init(GameObject value_this_object,string value_data_path,BattleUnit unit)
    {
        motion_parameter_details_ = new MotionParameterDetails();
        motion_parameter_details_.Init(value_data_path, value_this_object);
        motion_parameter_role_control_ = new MotionParameterRoleDataControl();
        motion_parameter_role_control_.Init();
        motion_parameter_role_performance_control_.Init(this, unit);
        unit_ = unit;

    }

    /// <summary>
    /// モーションパラメーターにあるエフェクトIDリスト
    /// </summary>
    /// <returns></returns>
    public List<int> GetEffectIDList()
    {
        if (is_setup_ == false) return null;
        return motion_parameter_details_.GetEffectIDList();
    }

    /// <summary>
    /// モーションパラメーターにあるBulletIDリスト
    /// </summary>
    /// <returns></returns>
    public List<int> GetBulletIDList()
    {
        if (is_setup_ == false) return null;
        return motion_parameter_details_.GetBulletIDList();
    }

    /// <summary>
    /// アクションInit
    /// </summary>
    public void ActionInit()
    {
        motion_parameter_role_control_.Init();
        key_fraem_ = 0.0f;
        is_action_end_ = false;
        is_play_ = true;
        action_id_ = action_index_ = 0;


    }

    /// <summary>
    /// アクション終了 
    /// </summary>
    public void ActionEnd()
    {
        motion_parameter_role_control_.Init();
        key_fraem_ = 0.0f;
        is_action_end_ = true;
        is_play_ = false;
        action_id_ = action_index_ = 0;
    }

    /// <summary>
    /// アクション再生
    /// </summary>
    public void PlayAction(int value_action_id,bool forced = false)
    {
        
        if (motion_parameter_details_ == null) return;
        if(forced == false)
        {
            if (value_action_id == action_id_ && is_play_ == true) return;
        }
        ActionInit();
        action_id_ = (uint)value_action_id;

        var data = motion_parameter_details_.GetMotionActionData((int)action_id_, (int)action_index_);
        if(data == null)
        {
            action_id_ = (99999);
            return;
        }
        int role_id = MotionParameterConvert.ConvertMotionActionRoleID(data.GetActionRoleName());
        motion_parameter_role_control_.PlayInit(motion_parameter_details_, data);
        motion_parameter_role_performance_control_.PlayInit(data, this, unit_);
    }

    public void Update(BattleUnit unit)
    {
        if (is_stop_ == true) return;
        motion_parameter_details_.Update();
        is_setup_ = motion_parameter_details_.GetIsSetUp();
        if (is_setup_ == false) return;

        if (BattleCollisionCore.Instance.GetIsSetUP() == false) return;
        if (BattleCore.Instance.GetIsSetUp() == false) return;
        UpdateMotionParameter(unit);
    }

    private void UpdateMotionParameter(BattleUnit unit)
    {
        if(is_action_end_ == true && is_play_ == true)
        {
            is_play_ = false;
            motion_parameter_role_control_.Init();
            return;

        }
        UpdateMotionParameterRole(unit);
        UpdateListMotionParameterRole(unit);
        UpdateKeyFrame();
    }

    private void UpdateMotionParameterRole(BattleUnit unit)
    {
        ActionInitListMotionParameterRole();
        if (is_action_end_ == true) return;
        uint action_length = motion_parameter_details_.GetMotionActionLength((int)action_id_);
        if (action_length == 0)
        {
            is_action_end_ = true;
            return;
        }



        for (uint count = action_index_; count < action_length; count++)
        {
            var data = motion_parameter_details_.GetMotionActionData((int)action_id_, (int)count);
            if(data == null)
            {
                is_action_end_ = true;
                return;
            }
            int role_id = MotionParameterConvert.ConvertMotionActionRoleID(data.GetActionRoleName());
            if (role_id == (int)MotionParameterConvert.MotionActionRoleName.Action_End)
            {
                is_action_end_ = true;
                return;
            }


            var next_data = motion_parameter_details_.GetMotionActionData((int)action_id_,(int) count + 1);
      
            float action_frame = next_data.GetActionFrame() * (1/60.0f);
            if (action_frame  <= key_fraem_)
            {
 
                action_index_++;

                data = motion_parameter_details_.GetMotionActionData((int)action_id_, (int)action_index_);
                if (data == null)
                {
                    is_action_end_ = true;
                    return;
                }
                role_id = MotionParameterConvert.ConvertMotionActionRoleID(data.GetActionRoleName());
                if (role_id == (int)MotionParameterConvert.MotionActionRoleName.Action_End)
                {
                    is_action_end_ = true;
                    return;
                }
                motion_parameter_role_control_.PlayInit(motion_parameter_details_,data);
                motion_parameter_role_performance_control_.PlayInit(data, this, unit);
                count = action_index_ - 1;

            }
            else
            {
                return;
            }


        }
        is_action_end_ = true;

    }

    private void ActionInitListMotionParameterRole()
    {

    }

    private void UpdateListMotionParameterRole(BattleUnit unit)
    {
        motion_parameter_role_control_.PlayUpdate();
        motion_parameter_role_performance_control_.Update(this,unit);
        motion_parameter_role_control_.PlayAutoReleaseUpdate();
    }



    /// <summary>
    /// キーフレーム更新
    /// </summary>
    private void UpdateKeyFrame()
    {
        key_fraem_ += 1/60.0f;
    }

    public void Release()
    {
        motion_parameter_details_.Release();
    }
}
