﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// モーションパラメータの各アクション出0田管理
/// </summary>
public class MotionParameterRoleDataControl
{


    /// <summary>
    /// モーションロール実行データ
    /// </summary>
    private MotionParameterDataStorehouseControl motion_role_parameter_data_ = new MotionParameterDataStorehouseControl();

    /// <summary>
    /// データがまだ存在するか
    /// </summary>
    /// <returns></returns>
    public bool GetMotionParameterDataActive(MotionParameterConvert.MotionActionRoleName type, int role_id)
    {
        if (motion_role_parameter_data_ == null) return false;
        return motion_role_parameter_data_.MotionParameterDataActive(type, role_id);
    }




    /// <summary>
    /// 進む移動量と方向
    /// </summary>
    public Vector3 GetAmoutMoveVector(int id) { return motion_role_parameter_data_.GetAmoutMoveVector( id); }

    /// <summary>
    /// 瞬間移動座標
    /// </summary>
    /// <returns></returns>
    public Vector3 GetTeleportPosition(int id) { return motion_role_parameter_data_.GetTeleportPosition( id); }
    /// <summary>
    /// テレポートフラグ
    /// </summary>
    /// <returns></returns>
    public bool GetIsTeleport(int id) { return motion_role_parameter_data_.GetIsTeleport(id); }
    /// <summary>
    /// テレポートフラグをOFF
    /// </summary>
    public void OffIsTeleport(int id) { motion_role_parameter_data_.OffIsTeleport( id); }

    /// <summary>
    /// アニメーション切り替えできるかどうか
    /// </summary>
    /// <returns></returns>
    public bool GetIsAnimationChange(int id) { return motion_role_parameter_data_.GetIsAnimationChange( id) ; }
    /// <summary>
    /// アニメーション切り替えフラグOFF
    /// </summary>
    public void OffIsAnimation(int id) { motion_role_parameter_data_.OffIsAnimation( id); }

    /// <summary>
    /// アニメーションID
    /// </summary>
    /// <returns></returns>
    public AnimationConvert.AnimationState GetAnimationState(int id) { return motion_role_parameter_data_.GetAnimationState( id); }

    /// <summary>
    /// ホーミングフラグのget
    /// </summary>
    /// <returns></returns>
    public bool GetIsHoming(int id) { return motion_role_parameter_data_.GetIsHoming( id); }
    /// <summary>
    /// ホーミングフラグをきる
    /// </summary>
    public void OffIsHoming(int id) { motion_role_parameter_data_.OffIsHoming( id); }
    /// <summary>
    /// ホーミングの回転計算結果
    /// </summary>
    /// <returns></returns>
    public Quaternion GetRotationHoming(int id) { return motion_role_parameter_data_.GetRotationHoming( id); }


    /// <summary>
    /// 瞬間回転の計算結果 Get
    /// </summary>
    /// <returns></returns>
    public Quaternion GetLookRotation(int id) { return motion_role_parameter_data_.GetLookRotation( id); }
    /// <summary>
    /// 瞬間回転フラグ　Get
    /// </summary>
    /// <returns></returns>
    public bool GetIsLookRotation(int id) { return motion_role_parameter_data_.GetIsLookRotation( id); }


    /// <summary>
    /// コリジョン生成フラグ(攻撃)
    /// </summary>
    /// <returns></returns>
    public bool GetIsCreateCollisionAttack(int id) { return motion_role_parameter_data_.GetIsCreateCollisionAttack( id); }

    /// <summary>
    /// コリジョン生成フラグをOFF(攻撃)
    /// </summary>
    public void OffIsCreateCollisionAttack(int id) { motion_role_parameter_data_.OffIsCreateCollisionAttack( id); }

    /// <summary>
    /// コリジョンID:Get(攻撃)
    /// </summary>
    /// <returns></returns>
    public int GetCollisionAttackID(int id) { return motion_role_parameter_data_.GetCollisionAttackID( id); }

    /// <summary>
    /// コリジョンSkllID
    /// </summary>
    /// <returns></returns>
    public int GetCollisionAttackSkillID(int id) { return motion_role_parameter_data_.GetCollisionAttackSkillID(id); }


    /// <summary>
    /// 座標設定(攻撃)
    /// </summary>
    /// <returns></returns>
    public Vector3 GetUnitAddPositionAttack(int id) { return motion_role_parameter_data_.GetUnitAddPositionAttack( id) ; }

    /// <summary>
    /// コリジョン生成フラグ(感知)
    /// </summary>
    /// <returns></returns>
    public bool GetIsCreateCollisionSensing(int id) { return motion_role_parameter_data_.GetIsCreateCollisionSensing( id); }

    /// <summary>
    /// コリジョン生成フラグをOFF(感知)
    /// </summary>
    public void OffIsCreateCollisionSensing(int id) { motion_role_parameter_data_.OffIsCreateCollisionSensing( id); }

    /// <summary>
    /// コリジョンID:Get(感知)
    /// </summary>
    /// <returns></returns>
    public int GetCollisionSensingID(int id) { return motion_role_parameter_data_.GetCollisionSensingID( id); }


    /// <summary>
    /// 座標設定(感知)
    /// </summary>
    /// <returns></returns>
    public Vector3 GetUnitAddPositionSensing(int id) { return motion_role_parameter_data_.GetUnitAddPositionSensing(id); }

    /// <summary>
    /// エフェクトID
    /// </summary>
    /// <returns></returns>
    public int GetEffectID(int id) { return motion_role_parameter_data_.GetEffectID(id); }

    /// <summary>
    /// エフェクト生存時間
    /// </summary>
    public float GetEffectLifeFrame(int id) { return motion_role_parameter_data_.GetEffectLifeFrame(id); }

    /// <summary>
    /// エフェクト座標
    /// </summary>
    public Vector3 GetEffectPosition(int id) { return motion_role_parameter_data_.GetEffectPosition(id); }

    /// <summary>
    /// BulletID
    /// </summary>
    /// <returns></returns>
    public int GetBulletID(int id) { return motion_role_parameter_data_.GetBulletID(id); }

    /// <summary>
    /// BulletDetailsID
    /// </summary>
    /// <returns></returns>
    public int GetBulletDetailsID(int id) { return motion_role_parameter_data_.GetBulletDetailsID(id); }

    /// <summary>
    /// Bullet座標
    /// </summary>
    public Vector3 GetBulletPosition(int id) { return motion_role_parameter_data_.GetBulletPosition(id); }

    public void Init()
    {
        ReleaseAll();
    }

    public void ReleaseAll()
    {
        motion_role_parameter_data_.ReleaseAll();
    }


    /// <summary>
    /// 再生初期
    /// </summary>
    /// <param name="detailes"></param>
    /// <param name="data"></param>
    /// <param name="role_id"></param>
    public bool PlayInit(MotionParameterDetails detailes, MotionParameterDetailsData data)
    {
        return motion_role_parameter_data_.PlayInit(MotionParameterConvert.ConvertMotionActionRoleNameID(data.GetActionRoleName()), detailes, data);
    }

    /// <summary>
    /// 再生
    /// </summary>
    public void PlayUpdate()
    {
        motion_role_parameter_data_.Update();

    }


    /// <summary>
    /// 再生(アクション自動解除)
    /// </summary>
    public void PlayAutoReleaseUpdate()
    {
        motion_role_parameter_data_.AutoReleaseUpdate();

    }


}
