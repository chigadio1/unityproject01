﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 管理Class(MotionParameterDataStorehouse)
/// </summary>
public class MotionParameterDataStorehouseControl
{
    /// <summary>
    /// 座標データ
    /// </summary>
    private BaseMotionParameterDataStorehouse<MotionActionRole.TransformPositionData> motion_role_data_transform_position_ = new BaseMotionParameterDataStorehouse<MotionActionRole.TransformPositionData>();

    /// <summary>
    /// 瞬間移動データ
    /// </summary>
    private BaseMotionParameterDataStorehouse<MotionActionRole.TransformTargetTeleportPositionData> motion_role_data_target_teleport_position_ = new BaseMotionParameterDataStorehouse<MotionActionRole.TransformTargetTeleportPositionData>();


    /// <summary>
    /// ホーミングデータ
    /// </summary>
    private BaseMotionParameterDataStorehouse<MotionActionRole.TransformTargetHomingData> motion_role_data_target_homing_ = new BaseMotionParameterDataStorehouse<MotionActionRole.TransformTargetHomingData>();


    /// <summary>
    /// 瞬間回転データ
    /// </summary>
    private BaseMotionParameterDataStorehouse<MotionActionRole.TransformTargetLookRotationData> motion_role_data_target_look_rotation_ = new BaseMotionParameterDataStorehouse<MotionActionRole.TransformTargetLookRotationData>();

    /// <summary>
    /// アニメーション切り替えデータ
    /// </summary>
    private BaseMotionParameterDataStorehouse<MotionActionRole.AnimationChangeData> motion_role_data_animation_change_ = new BaseMotionParameterDataStorehouse<MotionActionRole.AnimationChangeData>();

    /// <summary>
    /// 攻撃コリジョンデータ
    /// </summary>
    private BaseMotionParameterDataStorehouse<MotionActionRole.CollisionAttackData> motion_role_data_collision_attack_ = new BaseMotionParameterDataStorehouse<MotionActionRole.CollisionAttackData>();

    /// <summary>
    /// 感知コリジョンデータ
    /// </summary>
    private BaseMotionParameterDataStorehouse<MotionActionRole.CollisionSensingData> motion_role_data_collision_sensing_ = new BaseMotionParameterDataStorehouse<MotionActionRole.CollisionSensingData>();

    /// <summary>
    /// エフェクト生成データ
    /// </summary>
    private BaseMotionParameterDataStorehouse<MotionActionRole.EffectGenerateData> motion_role_data_effect_generate_ = new BaseMotionParameterDataStorehouse<MotionActionRole.EffectGenerateData>();

    /// <summary>
    /// エフェクト生成データ
    /// </summary>
    private BaseMotionParameterDataStorehouse<MotionActionRole.BulletGenerateData> motion_role_data_bullet_generate_ = new BaseMotionParameterDataStorehouse<MotionActionRole.BulletGenerateData>();


    /// <summary>
    /// 進む移動量と方向
    /// </summary>
    public Vector3 GetAmoutMoveVector(int id)
    {
        var data = motion_role_data_transform_position_.GetMotionParameterRoleData(id);
        if (data == null) return Vector3.zero;
        return data.GetAmoutoMoveVector();
    }

    /// <summary>
    /// 瞬間移動座標
    /// </summary>
    /// <returns></returns>
    public Vector3 GetTeleportPosition(int id)
    {
        var data = motion_role_data_target_teleport_position_.GetMotionParameterRoleData(id);
        if (data == null) return Vector3.zero;
        return data.GetTeleportPosition(); 
    }
    /// <summary>
    /// テレポートフラグ
    /// </summary>
    /// <returns></returns>
    public bool GetIsTeleport(int id)
    {
        var data = motion_role_data_target_teleport_position_.GetMotionParameterRoleData(id);
        if (data == null) return false;
        return data.GetIsTeleport();
    }
    /// <summary>
    /// テレポートフラグをOFF
    /// </summary>
    public void OffIsTeleport(int id)
    {
        var data = motion_role_data_target_teleport_position_.GetMotionParameterRoleData(id);
        if (data == null) return;

        data.OffIsTeleport();
    }

    /// <summary>
    /// アニメーション切り替えできるかどうか
    /// </summary>
    /// <returns></returns>
    public bool GetIsAnimationChange(int id)
    {
        var data = motion_role_data_animation_change_.GetMotionParameterRoleData(id);
        if (data == null) return false;
        return data.GetIsAnimationChange(); 
    }
    /// <summary>
    /// アニメーション切り替えフラグOFF
    /// </summary>
    public void OffIsAnimation(int id) 
    {
        var data = motion_role_data_animation_change_.GetMotionParameterRoleData(id);
        if (data == null) return;

        data.OffIsAnimation();
    }

    /// <summary>
    /// アニメーションID
    /// </summary>
    /// <returns></returns>
    public AnimationConvert.AnimationState GetAnimationState(int id)
    {
        var data = motion_role_data_animation_change_.GetMotionParameterRoleData(id);
        if (data == null) return AnimationConvert.AnimationState.Anim_Idle;
        return data.GetAnimationState(); 
    }

    /// <summary>
    /// ホーミングフラグのget
    /// </summary>
    /// <returns></returns>
    public bool GetIsHoming(int id)
    {
        var data = motion_role_data_target_homing_.GetMotionParameterRoleData(id);
        if (data == null) return false;
        return data.GetIsHoming();
    }
    /// <summary>
    /// ホーミングフラグをきる
    /// </summary>
    public void OffIsHoming(int id)
    {
        var data = motion_role_data_target_homing_.GetMotionParameterRoleData(id);
        if (data == null) return;
        data.OffIsHoming(); 
    }
    /// <summary>
    /// ホーミングの回転計算結果
    /// </summary>
    /// <returns></returns>
    public Quaternion GetRotationHoming(int id)
    {
        var data = motion_role_data_target_homing_.GetMotionParameterRoleData(id);
        if (data == null) return Quaternion.identity;
        return data.GetRotationHoming(); 
    }


    /// <summary>
    /// 瞬間回転の計算結果 Get
    /// </summary>
    /// <returns></returns>
    public Quaternion GetLookRotation(int id)
    {
        var data = motion_role_data_target_look_rotation_.GetMotionParameterRoleData(id);
        if (data == null) return Quaternion.identity;
        return data.GetLookRotation(); 
    }
    /// <summary>
    /// 瞬間回転フラグ　Get
    /// </summary>
    /// <returns></returns>
    public bool GetIsLookRotation(int id)
    {
        var data = motion_role_data_target_look_rotation_.GetMotionParameterRoleData(id);
        if (data == null) return false;
        return data.GetIsLookRotation();
    }


    /// <summary>
    /// コリジョン生成フラグ(攻撃)
    /// </summary>
    /// <returns></returns>
    public bool GetIsCreateCollisionAttack(int id)
    {
        var data = motion_role_data_collision_attack_.GetMotionParameterRoleData(id);
        if (data == null) return false;
        return data.GetIsCreateCollision();
    }

    /// <summary>
    /// コリジョン生成フラグをOFF(攻撃)
    /// </summary>
    public void OffIsCreateCollisionAttack(int id)
    {
        var data = motion_role_data_collision_attack_.GetMotionParameterRoleData(id);
        if (data == null) return ;
        data.OffIsCreateCollision(); 
    }

    /// <summary>
    /// コリジョンID:Get(攻撃)
    /// </summary>
    /// <returns></returns>
    public int GetCollisionAttackID(int id)
    {
        var data = motion_role_data_collision_attack_.GetMotionParameterRoleData(id);
        if (data == null) return 0;
        return data.GetCollisionID(); 
    }

    /// <summary>
    /// 攻撃コリジョンでのSkillID返却
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public int GetCollisionAttackSkillID(int id)
    {
        var data = motion_role_data_collision_attack_.GetMotionParameterRoleData(id);
        if (data == null) return 0;
        return data.GetAttackCollisionSkillID();
    }


    /// <summary>
    /// 座標設定(攻撃)
    /// </summary>
    /// <returns></returns>
    public Vector3 GetUnitAddPositionAttack(int id) 
    {
        var data = motion_role_data_collision_attack_.GetMotionParameterRoleData(id);
        if (data == null) return Vector3.one;
        return data.GetUnitAddPosition(); 
    }

    /// <summary>
    /// 座標設定(感知)
    /// </summary>
    /// <returns></returns>
    public Vector3 GetUnitAddPositionSensing(int id)
    {
        var data = motion_role_data_collision_sensing_.GetMotionParameterRoleData(id);
        if (data == null) return Vector3.one;
        return data.GetUnitAddPosition();
    }



    /// <summary>
    /// コリジョン生成フラグ(感知)
    /// </summary>
    /// <returns></returns>
    public bool GetIsCreateCollisionSensing(int id) 
    {
        var data = motion_role_data_collision_sensing_.GetMotionParameterRoleData(id);
        if (data == null) return false;
        return data.GetIsCreateCollision(); 
    }

    /// <summary>
    /// コリジョン生成フラグをOFF(感知)
    /// </summary>
    public void OffIsCreateCollisionSensing(int id)
    {
        var data = motion_role_data_collision_attack_.GetMotionParameterRoleData(id);
        if (data == null) return;
        data.OffIsCreateCollision();
    }

    /// <summary>
    /// コリジョンID:Get(感知)
    /// </summary>
    /// <returns></returns>
    public int GetCollisionSensingID(int id)
    {
        var data = motion_role_data_collision_attack_.GetMotionParameterRoleData(id);
        if (data == null) return 0;
        return data.GetCollisionID();
    }

    /// <summary>
    /// エフェクトID
    /// </summary>
    /// <returns></returns>
    public int GetEffectID(int id)
    {
        var data = motion_role_data_effect_generate_.GetMotionParameterRoleData(id);
        if (data == null) return 0;
        return data.GetEffectID();
    }

    /// <summary>
    /// エフェクト生存時間
    /// </summary>
    public float GetEffectLifeFrame(int id)
    {
        var data = motion_role_data_effect_generate_.GetMotionParameterRoleData(id);
        if (data == null) return 0.0f;
        return data.GetEffectLifeFrame(); 
    }

    /// <summary>
    /// エフェクト座標
    /// </summary>
    public Vector3 GetEffectPosition(int id)
    {
        var data = motion_role_data_effect_generate_.GetMotionParameterRoleData(id);
        if (data == null) return Vector3.zero;
        return data.GetEffectPosition(); 
    }


    /// <summary>
    /// BulletID
    /// </summary>
    /// <returns></returns>
    public int GetBulletID(int id)
    {
        var data = motion_role_data_bullet_generate_.GetMotionParameterRoleData(id);
        if (data == null) return 0;
        return data.GetBulletID();
    }

    /// <summary>
    /// BulletDetailsID
    /// </summary>
    /// <returns></returns>
    public int GetBulletDetailsID(int id)
    {
        var data = motion_role_data_bullet_generate_.GetMotionParameterRoleData(id);
        if (data == null) return 0;
        return data.GetBulletDetailsID();
    }

    /// <summary>
    /// Bullet座標
    /// </summary>
    public Vector3 GetBulletPosition(int id)
    {
        var data = motion_role_data_bullet_generate_.GetMotionParameterRoleData(id);
        if (data == null) return Vector3.zero;
        return data.GetBulletPosition();
    }

    /// <summary>
    /// 実行
    /// </summary>
    /// <param name="role_id"></param>
    /// <param name="detailes"></param>
    /// <param name="details_data"></param>
    public bool PlayInit(MotionParameterConvert.MotionActionRoleName role_id,MotionParameterDetails detailes, MotionParameterDetailsData details_data)
    {
        switch (role_id)
        {
            case MotionParameterConvert.MotionActionRoleName.Transform_Position:
                motion_role_data_transform_position_.PlayInit(detailes, details_data);
                break;
            case MotionParameterConvert.MotionActionRoleName.Transform_Target_TeleportPosition:
                motion_role_data_target_teleport_position_.PlayInit(detailes, details_data);
                break;
            case MotionParameterConvert.MotionActionRoleName.Transform_Target_Homing:
                motion_role_data_target_homing_.PlayInit(detailes, details_data);
                break;
            case MotionParameterConvert.MotionActionRoleName.Transform_Target_LookRotation:
                motion_role_data_target_look_rotation_.PlayInit(detailes, details_data);
                break;
            case MotionParameterConvert.MotionActionRoleName.Animation_Change:
                motion_role_data_animation_change_.PlayInit(detailes, details_data);
                break;
            case MotionParameterConvert.MotionActionRoleName.Collision_Attack:
                motion_role_data_collision_attack_.PlayInit(detailes, details_data);
                break;
            case MotionParameterConvert.MotionActionRoleName.Collision_Sensing:
                motion_role_data_collision_sensing_.PlayInit(detailes, details_data);
                break;
            case MotionParameterConvert.MotionActionRoleName.Effect_Generate:
                motion_role_data_effect_generate_.PlayInit(detailes, details_data);
                break;
            case MotionParameterConvert.MotionActionRoleName.Bullet_Generate:
                motion_role_data_bullet_generate_.PlayInit(detailes, details_data);
                break;
            case MotionParameterConvert.MotionActionRoleName.Action_End:
                return true;
            case MotionParameterConvert.MotionActionRoleName.Max_Action:
                return true;

        }

        return false;
    }

    /// <summary>
    /// データがまだ存在するか
    /// </summary>
    /// <returns></returns>
    public bool MotionParameterDataActive(MotionParameterConvert.MotionActionRoleName type, int role_id)
    {
        return GetMotionParameterData(type, role_id) != null ? true : false;
    }

    private BaseMotionParameterRoleDataControl GetMotionParameterData(MotionParameterConvert.MotionActionRoleName type, int role_id)
    {
        switch (type)
        {
            case MotionParameterConvert.MotionActionRoleName.Transform_Position:
                return motion_role_data_transform_position_.GetMotionParameterRoleData(role_id);
            case MotionParameterConvert.MotionActionRoleName.Transform_Target_TeleportPosition:
                return motion_role_data_target_teleport_position_.GetMotionParameterRoleData(role_id);
            case MotionParameterConvert.MotionActionRoleName.Transform_Target_Homing:
                return motion_role_data_target_homing_.GetMotionParameterRoleData(role_id);
            case MotionParameterConvert.MotionActionRoleName.Transform_Target_LookRotation:
                return motion_role_data_target_look_rotation_.GetMotionParameterRoleData(role_id);
            case MotionParameterConvert.MotionActionRoleName.Animation_Change:
                return motion_role_data_animation_change_.GetMotionParameterRoleData(role_id);
            case MotionParameterConvert.MotionActionRoleName.Collision_Attack:
                return motion_role_data_collision_attack_.GetMotionParameterRoleData(role_id);
            case MotionParameterConvert.MotionActionRoleName.Collision_Sensing:
                return motion_role_data_collision_sensing_.GetMotionParameterRoleData(role_id);
            case MotionParameterConvert.MotionActionRoleName.Effect_Generate:
                return motion_role_data_effect_generate_.GetMotionParameterRoleData(role_id);
            case MotionParameterConvert.MotionActionRoleName.Action_End:
                break;
            case MotionParameterConvert.MotionActionRoleName.Max_Action:
                break;
            case MotionParameterConvert.MotionActionRoleName.Bullet_Generate:
                return motion_role_data_bullet_generate_.GetMotionParameterRoleData(role_id);
        }

        return null;
    }

    /// <summary>
    /// 更新処理
    /// </summary>
    public void Update()
    {
        motion_role_data_transform_position_.Update();
        motion_role_data_target_teleport_position_.Update();
        motion_role_data_target_homing_.Update();
        motion_role_data_target_look_rotation_.Update();
        motion_role_data_animation_change_.Update();
        motion_role_data_collision_attack_.Update();
        motion_role_data_collision_sensing_.Update();
        motion_role_data_effect_generate_.Update();
        motion_role_data_bullet_generate_.Update();
    }

    /// <summary>
    /// 更新処理(自動解除)
    /// </summary>
    public void AutoReleaseUpdate()
    {
        motion_role_data_transform_position_.AutoRelease();
        motion_role_data_target_teleport_position_.AutoRelease();
        motion_role_data_target_homing_.AutoRelease();
        motion_role_data_target_look_rotation_.AutoRelease();
        motion_role_data_animation_change_.AutoRelease();
        motion_role_data_collision_attack_.AutoRelease();
        motion_role_data_collision_sensing_.AutoRelease();
        motion_role_data_effect_generate_.AutoRelease();
        motion_role_data_bullet_generate_.AutoRelease();
    }


    public void ReleaseAll()
    {
        motion_role_data_transform_position_.ReleaseAll();
        motion_role_data_target_teleport_position_.ReleaseAll();
        motion_role_data_target_homing_.ReleaseAll();
        motion_role_data_target_look_rotation_.ReleaseAll();
        motion_role_data_animation_change_.ReleaseAll();
        motion_role_data_collision_attack_.ReleaseAll();
        motion_role_data_collision_sensing_.ReleaseAll();
        motion_role_data_effect_generate_.ReleaseAll();
        motion_role_data_bullet_generate_.ReleaseAll();
    }


}
