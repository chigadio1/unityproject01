﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MotionActionRole
{
    /// <summary>
    /// 弾丸生成役割データ
    /// </summary>
    public class BulletGenerateData : BaseMotionParameterRoleDataControl
    {
        /// <summary>
        /// 弾丸ID
        /// </summary>
        private int bullet_id_ = 0;
        public int GetBulletID() { return bullet_id_; }

        /// <summary>
        /// 弾丸詳細ID
        /// </summary>
        private int bullet_detaols_id_ = 0;
        public int GetBulletDetailsID() { return bullet_detaols_id_; }

        /// <summary>
        /// 弾丸座標
        /// </summary>
        private Vector3 bullet_position_ = Vector3.zero;
        public Vector3 GetBulletPosition() { return bullet_position_; }

        public override void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
        {
            base.Init(detailes, data);
            ActionInit();

            bullet_id_ = data.GetActionInt01();
            bullet_detaols_id_ = data.GetActionInt02();

            bullet_position_ = new Vector3(data.GetActionFloat01(),data.GetActionFloat02(),data.GetActionFloat03());

        }

        public override void ActionInit()
        {
            base.ActionInit();
            bullet_id_ = bullet_detaols_id_ = 0;
            bullet_position_ = Vector3.zero;

        }

        public override void Updata()
        {
            base.Updata();
        }
    }

    /// <summary>
    /// 弾丸生成実行
    /// </summary>
    public class BulletGeneratePerformance : BaseCollisionPerformance
    {

        public override void Init(int value_role_data_id, MotionParameterRoleDataControl data, BattleUnit unit)
        {
            role_data_id_ = value_role_data_id;
            role_action_name_ = MotionParameterConvert.MotionActionRoleName.Bullet_Generate;
            Vector3 position = (unit.transform.rotation * data.GetBulletPosition(value_role_data_id)) + unit.transform.position;

            BulletCreate.Appearance(unit,data.GetBulletID(value_role_data_id), data.GetBulletDetailsID(value_role_data_id), position,unit.transform.rotation);
        }
    }
}
