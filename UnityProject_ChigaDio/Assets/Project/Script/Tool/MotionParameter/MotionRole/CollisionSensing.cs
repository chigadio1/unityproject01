﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MotionActionRole
{
    /// <summary>
    /// 感知コリジョンデータ
    /// </summary>
    public class CollisionSensingData : BaseCollisionData
    {
        public override void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
        {
            base.Init(detailes, data);
        }

        public override void ActionInit()
        {
            base.ActionInit();
        }

        public override void Updata()
        {
            base.Updata();
        }
    }

    /// <summary>
    /// 感知コリジョン実行
    /// </summary>
    public class CollisionSensingPerformance : BaseCollisionPerformance
    {
        public override void Init(int value_role_data_id, MotionParameterRoleDataControl data, BattleUnit unit)
        {
            role_data_id_ = value_role_data_id;
            role_action_name_ = MotionParameterConvert.MotionActionRoleName.Collision_Sensing;
        }
    }
}
