﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MotionActionRole
{
    /// <summary>
    /// 攻撃コリジョンデータ
    /// </summary>
    public class CollisionAttackData : BaseCollisionData
    {
        /// <summary>
        /// スキルID
        /// </summary>
        private int skill_id_ = 0;
        public int GetAttackCollisionSkillID() { return skill_id_; }
        public override void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
        {
            base.Init(detailes, data);
            skill_id_ = data.GetActionInt02();
        }

        public override void ActionInit()
        {
            base.ActionInit();
        }

        public override void Updata()
        {
            base.Updata();
        }
    }

    /// <summary>
    /// 攻撃コリジョン実行
    /// </summary>
    public class CollisionAttackPerformance : BaseCollisionPerformance
    {
        public override void Init(int value_role_data_id,MotionParameterRoleDataControl data, BattleUnit unit)
        {
            role_data_id_ = value_role_data_id;
            role_action_name_ = MotionParameterConvert.MotionActionRoleName.Collision_Attack;
        }

        public override void Updata(MotionParameterRoleDataControl data, BattleUnit unit)
        {
           if(data.GetIsCreateCollisionAttack(role_data_id_))
            {
                BattleCollisionCore.Instance.CreateAttackCollision(data.GetCollisionAttackID(role_data_id_), unit.GetUnitID(), data.GetCollisionAttackSkillID(role_data_id_),data.GetUnitAddPositionAttack(role_data_id_), unit);
                data.OffIsCreateCollisionAttack(role_data_id_);
            }
        }
    }
}