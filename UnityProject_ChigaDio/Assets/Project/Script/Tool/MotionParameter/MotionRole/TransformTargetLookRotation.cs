﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MotionActionRole
{
    /// <summary>
    /// 瞬間回転
    /// </summary>
    public class TransformTargetLookRotationData : BaseMotionParameterRoleDataControl
    {
        /// <summary>
        /// 回転するオブジェクト
        /// </summary>
        private GameObject this_object_ = null;

        /// <summary>
        /// 目標オブジェクト
        /// </summary>
        private GameObject target_object_ = null;

        /// <summary>
        /// 回転
        /// </summary>
        private Quaternion look_rotation_ = Quaternion.identity;

        /// <summary>
        /// 瞬間回転の計算結果 Get
        /// </summary>
        /// <returns></returns>
        public Quaternion GetLookRotation() { return look_rotation_; }

        /// <summary>
        /// 瞬間回転フラグ
        /// </summary>
        private bool is_look_rotation_ = false;

        /// <summary>
        /// 瞬間回転フラグ　Get
        /// </summary>
        /// <returns></returns>
        public bool GetIsLookRotation() { return is_look_rotation_; }

        public override void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
        {
            base.Init(detailes, data);
            ActionInit();

            target_object_ = detailes.GetTargetObject();
            if (target_object_ == null) return;

            this_object_ = detailes.GetThisObject();
           

            Vector3 this_position = this_object_.transform.position;
            Vector3 target_position = target_object_.transform.position;

            this_position.y = target_position.y = 0.0f;

            look_rotation_ = Quaternion.LookRotation(target_position - this_position);

            is_look_rotation_ = true;
        }

        public override void ActionInit()
        {
            is_look_rotation_ = false;
            look_rotation_ = Quaternion.identity;
        }

        public override void Updata()
        {
            base.Updata();
        }
    }

    /// <summary>
    /// 瞬間回転実行
    /// </summary>
    public class TransformTargetLookRotationPerformance : BaseMotionParameterRolePerformance
    {




        public override void Init(int value_role_data_id, MotionParameterRoleDataControl data, BattleUnit unit)
        {
            role_data_id_ = value_role_data_id;
            role_action_name_ = MotionParameterConvert.MotionActionRoleName.Transform_Target_LookRotation;
        }

        public override void ActionInit()
        {

        }

        public override void Updata(MotionParameterRoleDataControl data, BattleUnit unit)
        {
            if(data.GetIsLookRotation(role_data_id_))
            {
                unit.transform.rotation = data.GetLookRotation(role_data_id_);
            }
        }

        public override void AnotherUpdate(MotionParameterRoleDataControl data, BattleUnit unit)
        {

        }

    }
}