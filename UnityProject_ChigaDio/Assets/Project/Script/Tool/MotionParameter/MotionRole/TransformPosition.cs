﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MotionActionRole
{
    /// <summary>
    /// 移動データ
    /// </summary>
    public class TransformPositionData : BaseMotionParameterRoleDataControl
    {
        /// <summary>
        /// 方向
        /// </summary>
        public enum VectorType
        {
            Vector_X,
            Vector_Y,
            Vector_Z
        }

        /// <summary>
        /// 方向移動量
        /// </summary>
        private Vector3 amouto_move_vector_ = Vector3.zero;
        public Vector3 GetAmoutoMoveVector() { return amouto_move_vector_; }

        /// <summary>
        /// 動く方向
        /// </summary>
        private Vector3 move_vector_ = Vector3.zero;

        /// <summary>
        /// 移動量
        /// </summary>
        private float amount_move_ = 0.0f;

        public override void Init(MotionParameterDetails detailes,MotionParameterDetailsData data)
        {
            base.Init(detailes, data);
            ActionInit();
            VectorType type =  (VectorType)data.GetActionInt01();
            switch (type)
            {
                case VectorType.Vector_X:
                    move_vector_ = new Vector3(1.0f, 0.0f, 0.0f);
                    break;
                case VectorType.Vector_Y:
                    move_vector_ = new Vector3(0.0f, 1.0f, 0.0f);
                    break;
                case VectorType.Vector_Z:
                    move_vector_ = new Vector3(0.0f, 0.0f, 1.0f);
                    break;
            }
            amount_move_ = data.GetActionFloat01();

        }

        public override void ActionInit()
        {
            move_vector_ = amouto_move_vector_ = Vector3.zero;
            amount_move_ = 0.0f;
        }

        public override void Updata()
        {
            base.Updata();
            amouto_move_vector_ =  (move_vector_ * amount_move_);
        }
    }

    /// <summary>
    /// 移動実行
    /// </summary>
    public class TransformPositionPerformance : BaseMotionParameterRolePerformance
    {




        public override void Init(int value_role_data_id, MotionParameterRoleDataControl data, BattleUnit unit)
        {
            role_data_id_ = value_role_data_id;
            role_action_name_ = MotionParameterConvert.MotionActionRoleName.Transform_Position;
        }

        public override void ActionInit()
        {
          
        }

        public override void Updata(MotionParameterRoleDataControl data, BattleUnit unit)
        {
            if (unit == null) return;

            unit.transform.position += unit.transform.rotation * data.GetAmoutMoveVector(role_data_id_);
        }

        public override void AnotherUpdate(MotionParameterRoleDataControl data, BattleUnit unit)
        {
          
        }

    }
}
