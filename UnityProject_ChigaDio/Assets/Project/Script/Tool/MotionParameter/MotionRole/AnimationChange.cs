﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MotionActionRole
{
    /// <summary>
    /// アニメーション切り替え
    /// </summary>
    public class AnimationChangeData : BaseMotionParameterRoleDataControl
    {

        private AnimationConvert.AnimationState animation_state_ = AnimationConvert.AnimationState.Anim_Idle;

        /// <summary>
        /// アニメーションID
        /// </summary>
        /// <returns></returns>
        public AnimationConvert.AnimationState GetAnimationState() { return animation_state_; }

        /// <summary>
        /// 切り替えフラグ
        /// </summary>
        private bool is_animation_change_ = false;
        /// <summary>
        /// アニメーション切り替えできるかどうか
        /// </summary>
        /// <returns></returns>
        public bool GetIsAnimationChange() { return is_animation_change_; }
        /// <summary>
        /// アニメーション切り替えフラグOFF
        /// </summary>
        public void OffIsAnimation() { is_animation_change_ = false; }


        public override void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
        {
            base.Init(detailes, data);
            ActionInit();
            animation_state_ = (AnimationConvert.AnimationState)data.GetActionInt01();
            is_animation_change_ = true;
        }

        public override void ActionInit()
        {
            animation_state_ = AnimationConvert.AnimationState.Anim_Idle;
            is_animation_change_ = false;
        }

        public override void Updata()
        {
            base.Updata();
        }
    }

    /// <summary>
    /// アニメーション切り替え実行
    /// </summary>
    public class AnimationChangePerformance : BaseMotionParameterRolePerformance
    {




        public override void Init(int value_role_data_id,MotionParameterRoleDataControl data, BattleUnit unit)
        {
            role_data_id_ = value_role_data_id;
            role_action_name_ = MotionParameterConvert.MotionActionRoleName.Animation_Change;

            unit.ChangeAnimation(data.GetAnimationState(role_data_id_), true);
            data.OffIsAnimation(role_data_id_);
        }

        public override void ActionInit()
        {

        }

        public override void Updata(MotionParameterRoleDataControl data, BattleUnit unit)
        {


        }

        public override void AnotherUpdate(MotionParameterRoleDataControl data, BattleUnit unit)
        {

        }

    }
}
