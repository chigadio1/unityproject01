﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MotionActionRole
{
    /// <summary>
    /// ホーミング
    /// </summary>
    public class TransformTargetHomingData : BaseMotionParameterRoleDataControl
    {
        /// <summary>
        /// Y軸で足す角度
        /// </summary>
        private float add_angel_y_ = 0.0f;

        /// <summary>
        /// 回転するオブジェクト
        /// </summary>
        private GameObject this_object_ = null;

        /// <summary>
        /// 目標オブジェクト
        /// </summary>
        private GameObject target_object_ = null;

        /// <summary>
        /// ホーミング計算結果の角度
        /// </summary>
        private Quaternion rotation_homing_ = Quaternion.identity;
        public Quaternion GetRotationHoming() { return rotation_homing_; }

        /// <summary>
        /// ホーミング中のフラグ
        /// </summary>
        private bool is_homing_ = false;
        /// <summary>
        /// ホーミングフラグのget
        /// </summary>
        /// <returns></returns>
        public bool GetIsHoming() { return is_homing_; }
        /// <summary>
        /// ホーミングフラグをきる
        /// </summary>
        public void OffIsHoming() { is_homing_ = false; }

        
        public override void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
        {
            base.Init(detailes, data);
            ActionInit();

            this_object_ =  detailes.GetThisObject();
            target_object_ = detailes.GetTargetObject();
            is_homing_ = true;
            add_angel_y_ = data.GetActionFloat01();
            rotation_homing_ = this_object_.transform.rotation;

        }

        public override void ActionInit()
        {
            add_angel_y_ = 0.0f;
            rotation_homing_ = Quaternion.identity;
            is_homing_ = false;
        }

        public override void Updata()
        {
            base.Updata();
            if (target_object_ == null) return;
            ///自分と対象の座標で、角度を求める
            Vector3 this_position_ = this_object_.transform.position;
            Vector3 target_position_ = target_object_.transform.position;

            target_position_.y =  this_position_.y = 0.0f;

            ///角度差をだす
            float angle_difference =  Vector3.SignedAngle((target_position_ - this_position_).normalized, this_object_.transform.forward,Vector3.up);

            float cross_y = Vector3.Cross((target_position_ - this_position_), this_object_.transform.forward).y < 0.0f ? 1.0f : -1.0f;


            if (add_angel_y_ >= Mathf.Abs(angle_difference))
            {
                rotation_homing_ = Quaternion.LookRotation(target_position_ - this_position_);
            }
            else
            {
                Vector3 this_now_angle = this_object_.transform.eulerAngles;
                this_now_angle.y += add_angel_y_ * cross_y;
                rotation_homing_ = Quaternion.Euler(this_now_angle.x, this_now_angle.y, this_now_angle.z);
            }
        }
    }

    /// <summary>
    /// ホーミング実行
    /// </summary>
    public class TransformTargetHomingPerformance : BaseMotionParameterRolePerformance
    {




        public override void Init(int value_role_data_id, MotionParameterRoleDataControl data, BattleUnit unit)
        {
            role_data_id_ = value_role_data_id;
            role_action_name_ = MotionParameterConvert.MotionActionRoleName.Transform_Target_Homing;
        }

        public override void ActionInit()
        {

        }

        public override void Updata(MotionParameterRoleDataControl data, BattleUnit unit)
        {
            if(data.GetIsHoming(role_data_id_))
            {
                unit.transform.rotation = data.GetRotationHoming(role_data_id_);
            }
        }

        public override void AnotherUpdate(MotionParameterRoleDataControl data, BattleUnit unit)
        {

        }

    }
}
