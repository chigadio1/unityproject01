﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MotionActionRole
{
    /// <summary>
    /// エフェクト生成データ
    /// </summary>
    public class EffectGenerateData : BaseMotionParameterRoleDataControl
    {

        /// <summary>
        /// エフェクトID
        /// </summary>
        private int effect_id_ = 0;
        public int GetEffectID() { return effect_id_; }

        /// <summary>
        /// エフェクト生存時間
        /// </summary>
        private float effect_life_frame_ = 0.0f;
        public float GetEffectLifeFrame() { return effect_life_frame_; }

        /// <summary>
        /// エフェクト座標
        /// </summary>
        private Vector3 effect_position_ = Vector3.zero;
        public Vector3 GetEffectPosition() { return effect_position_; }


        public override void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
        {
            base.Init(detailes, data);

            ActionInit();

            effect_id_ = data.GetActionInt01();
            effect_life_frame_ = data.GetActionFloat01();
            effect_position_ = new Vector3(data.GetActionFloat02(), data.GetActionFloat03(), data.GetActionFloat04());
        }

        public override void ActionInit()
        {
            base.ActionInit();

            effect_id_ = 0;
            effect_life_frame_ = 0.0f;
            effect_position_ = Vector3.zero; 
        }

        public override void Updata()
        {
            base.Updata();
        }
    }

    /// <summary>
    /// エフェクト生成実行
    /// </summary>
    public class EffectGeneratePerformance : BaseCollisionPerformance
    {
            
        public override void Init(int value_role_data_id, MotionParameterRoleDataControl data, BattleUnit unit)
        {
            role_data_id_ = value_role_data_id;
            role_action_name_ = MotionParameterConvert.MotionActionRoleName.Effect_Generate;

            Vector3 effect_pos = (unit.transform.rotation * data.GetEffectPosition(role_data_id_)) + unit.transform.position;

            EffectCreate.Appearance(data.GetEffectID(role_data_id_), effect_pos, unit.transform.rotation, Vector3.one, data.GetEffectLifeFrame(role_data_id_),GameEffectManager.EffectRealmType.Battle);
        }
    }
}
