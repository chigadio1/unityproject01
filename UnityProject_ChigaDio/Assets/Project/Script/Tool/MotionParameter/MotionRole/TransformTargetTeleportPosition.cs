﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MotionActionRole
{
    /// <summary>
    /// ターゲットに向かい瞬間移動
    /// </summary>
    public class TransformTargetTeleportPositionData : BaseMotionParameterRoleDataControl
    {
        /// <summary>
        /// テレポート先ポジション
        /// </summary>
        private Vector3 teleport_position = Vector3.zero;
        public Vector3 GetTeleportPosition()
        {
            return teleport_position;
        }

        /// <summary>
        /// テレポートするかどうか
        /// </summary>
        private bool is_teleport_ = false;
        public bool GetIsTeleport()
        {
            return is_teleport_;
        }
        public void OffIsTeleport()
        {
            is_teleport_ = false;
        }

        public override void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
        {
            base.Init(detailes, data);
            ActionInit();
            if (detailes.GetThisObject() == null || detailes.GetTargetObject() == null) return;

            Vector3 this_position = detailes.GetThisObject().transform.position;
            Vector3 target_position = detailes.GetTargetObject().transform.position;

            ///長さ
            float length = data.GetActionFloat01();

            teleport_position =   target_position  + (this_position - target_position).normalized * length;

            is_teleport_ = true;
        }

        public override void ActionInit()
        {
            is_teleport_ = false;
            teleport_position = Vector3.zero;
        }

        public override void Updata()
        {
            base.Updata();
        }
    }

    /// <summary>
    /// 瞬間移動実行
    /// </summary>
    public class TransformTargetTeleportPositionPerformance : BaseMotionParameterRolePerformance
    {




        public override void Init(int value_role_data_id, MotionParameterRoleDataControl data, BattleUnit unit)
        {
            role_data_id_ = value_role_data_id;
            role_action_name_ = MotionParameterConvert.MotionActionRoleName.Transform_Target_TeleportPosition;
        }

        public override void ActionInit()
        {

        }

        public override void Updata(MotionParameterRoleDataControl data, BattleUnit unit)
        {
            if (unit == null) return;

            if(data.GetIsTeleport(role_data_id_))
            {
                unit.transform.position = data.GetTeleportPosition(role_data_id_);
                data.OffIsTeleport(role_data_id_);
            }
        }

        public override void AnotherUpdate(MotionParameterRoleDataControl data, BattleUnit unit)
        {

        }

    }
}
