﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// モーション役割データ
/// </summary>
public class BaseMotionParameterRoleDataControl
{
    /// <summary>
    /// 生存フレーム
    /// </summary>
    protected float life_frame_ = 0.0f;

    /// <summary>
    /// 計算時間
    /// </summary>
    protected float time_ = 0.0f;

    /// <summary>
    /// 生存時間を超えていたら
    /// </summary>
    /// <returns></returns>
    public bool GetLifeOver() { return time_ >= life_frame_ * (1.0f / 60.0f) ? true : false; }

    /// <summary>
    /// 初期化
    /// </summary>
    public virtual void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
    {
        life_frame_ = data.GetActionLifeFrame();
    }

    public virtual void ActionInit() { }

    public void TimeUpdate()
    {
        time_ += 1.0f / 60.0f;
    }

    /// <summary>
    /// 更新処理
    /// </summary>
    public virtual void Updata()
    {
        TimeUpdate();
    }

    /// <summary>
    /// 別更新
    /// </summary>
    public virtual void AnotherUpdate()
    {

    }
}

/// <summary>
/// モーション役割実行
/// </summary>
public class BaseMotionParameterRolePerformance
{
    /// <summary>
    /// モーションパラメーター役割ID
    /// </summary>
    protected int role_data_id_ = 0;
    public int GetRoleDataID() { return role_data_id_; }

    /// <summary>
    /// アクションタイプ
    /// </summary>
    protected MotionParameterConvert.MotionActionRoleName role_action_name_ = MotionParameterConvert.MotionActionRoleName.Action_End;
    public MotionParameterConvert.MotionActionRoleName GetRoleActionName() { return role_action_name_; }

    /// <summary>
    /// 初期化
    /// </summary>
    public virtual void Init(int value_role_data_id,MotionParameterRoleDataControl data, BattleUnit unit)
    {
        role_data_id_ = value_role_data_id;
        
    }

    public virtual void ActionInit() 
    { }

    /// <summary>
    /// 更新処理
    /// </summary>
    public virtual void Updata(MotionParameterRoleDataControl data, BattleUnit unit)
    {

    }

    /// <summary>
    /// 別更新
    /// </summary>
    public virtual void AnotherUpdate(MotionParameterRoleDataControl data, BattleUnit unit)
    {

    }
}
