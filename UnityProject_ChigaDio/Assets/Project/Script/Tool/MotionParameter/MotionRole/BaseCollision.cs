﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MotionActionRole
{
    public class BaseCollisionData : BaseMotionParameterRoleDataControl
    {
        /// <summary>
        /// コリジョンクリエイトフラグ
        /// </summary>
        protected bool is_create_collision_ = false;
        /// <summary>
        /// コリジョン生成フラグ
        /// </summary>
        /// <returns></returns>
        public bool GetIsCreateCollision() { return is_create_collision_; }

        /// <summary>
        /// コリジョン生成フラグをOFF
        /// </summary>
        public void OffIsCreateCollision() { is_create_collision_ = false; }

        /// <summary>
        /// コリジョンID
        /// </summary>
        protected int collision_id_ = 0;
        /// <summary>
        /// コリジョンID:Get
        /// </summary>
        /// <returns></returns>
        public int GetCollisionID() { return collision_id_; }

        /// <summary>
        /// 座標設定
        /// </summary>
        protected Vector3 unit_add_position_ = Vector3.zero;

        /// <summary>
        /// 座標設定
        /// </summary>
        /// <returns></returns>
        public Vector3 GetUnitAddPosition() { return unit_add_position_; }

        public override void Init(MotionParameterDetails detailes, MotionParameterDetailsData data)
        {
            base.Init(detailes, data);
            ActionInit();
            collision_id_ = data.GetActionInt01();

            is_create_collision_ = true;

            unit_add_position_ = new Vector3(data.GetActionFloat01(), data.GetActionFloat02(), data.GetActionFloat03());
        }

        public override void ActionInit()
        {
            collision_id_ = 0;
            is_create_collision_ = false;
            unit_add_position_ = Vector3.zero;
        }

    }

    /// <summary>
    /// コリジョン実装
    /// </summary>
    public class BaseCollisionPerformance : BaseMotionParameterRolePerformance
    {

    }
}