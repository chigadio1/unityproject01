﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// モーションパラメータ
/// </summary>
public class MotionParameter
{

    /// <summary>
    /// モーションパラメータの役割データ
    /// </summary>
    private MotionParameterRoleDataControlManagement motion_parameter_role_data_management_ = new MotionParameterRoleDataControlManagement();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    public bool GetIsSetUp()
    {
        return motion_parameter_role_data_management_.GetIsSetUp();
    }

    /// <summary>
    /// 再生中フラグ
    /// </summary>
    /// <returns></returns>
    public bool GetIsPlay()
    {
        return motion_parameter_role_data_management_.GetIsPlay();
    }


    /// <summary>
    /// ターゲットセット
    /// </summary>
    public void SetTargetGameObject(GameObject value_target)
    {
        if (motion_parameter_role_data_management_ == null) return;
        motion_parameter_role_data_management_.SetTargetObject(value_target);
    }

    /// <summary>
    /// モーションパラメーターにあるエフェクトIDリスト
    /// </summary>
    /// <returns></returns>
    public List<int> GetEffectIDList()
    {
        return motion_parameter_role_data_management_.GetEffectIDList();
    }

    /// <summary>
    /// モーションパラメーターにあるBulletIDリスト
    /// </summary>
    /// <returns></returns>
    public List<int> GetBulletIDList()
    {
        return motion_parameter_role_data_management_.GetBulletIDList();
    }


    public void Init(GameObject value_this_object, string path,BattleUnit unit)
    {
        motion_parameter_role_data_management_.Init(value_this_object,path,unit);

    }

    public void Update(BattleUnit unit)
    {
        motion_parameter_role_data_management_.Update(unit);
        if (motion_parameter_role_data_management_.GetIsSetUp() == false) return;

    }

    /// <summary>
    /// アクション再生
    /// </summary>
    /// <param name="value_action_id"></param>
    /// <param name="forced"></param>
    public void PlayAction(int value_action_id, bool forced = false)
    {
        if (motion_parameter_role_data_management_ == null) return;
        motion_parameter_role_data_management_.PlayAction(value_action_id, forced);
    
    
    }

    /// <summary>
    /// アクション再生
    /// </summary>
    /// <param name="value_action_id"></param>
    /// <param name="forced"></param>
    public void PlayAction(MotionParameterConvert.MotionActionName value_action_id, bool forced = false)
    {
        if (motion_parameter_role_data_management_ == null) return;
        motion_parameter_role_data_management_.PlayAction((int)value_action_id, forced);
    }

    public void ActionInit()
    {
        motion_parameter_role_data_management_.ActionInit();
    }
    public void ActionEnd()
    {
        motion_parameter_role_data_management_.ActionEnd();
    }


    public void Release()
    {
        motion_parameter_role_data_management_.Release();
    }
}
