﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// MotionDataからデータを貰い保持する
/// </summary>
public class MotionParameterDetailsData 
{
    /// <summary>
    /// アクションインデックス
    /// </summary>
    [SerializeField]
    private int action_index_ = 0;
    public int GetActionIndex() { return action_index_; }

    /// <summary>
    /// アクションネーム
    /// </summary>
    [SerializeField]
    private string action_name_ = "";
    public string GetActionName() { return action_name_; }

    /// <summary>
    /// アクション役割ネーム
    /// </summary>
    [SerializeField]
    private string action_role_name_ = "";
    public string GetActionRoleName() { return action_role_name_; }

    /// <summary>
    /// アクションID
    /// </summary>
    [SerializeField]
    private int action_id_ = 0;
    public int GetActionID() { return action_id_; }

    /// <summary>
    /// アクション実行フレーム開始
    /// </summary>
    [SerializeField]
    private float action_frame_ = 0;
    public float GetActionFrame() { return action_frame_; }

    /// <summary>
    /// アクション実行フレーム生存
    /// </summary>
    [SerializeField]
    private float action_life_frame_ = 0;
    public float GetActionLifeFrame() { return action_life_frame_; }

    /// <summary>
    /// アクションInt01
    /// </summary>
    [SerializeField]
    private int action_int_01_ = 0;
    public int GetActionInt01() { return action_int_01_; }

    /// <summary>
    /// アクションInt02
    /// </summary>
    [SerializeField]
    private int action_int_02_ = 0;
    public int GetActionInt02() { return action_int_02_; }

    /// <summary>
    /// アクションInt01
    /// </summary>
    [SerializeField]
    private int action_int_03_ = 0;
    public int GetActionInt03() { return action_int_03_; }

    /// <summary>
    /// アクションInt01
    /// </summary>
    [SerializeField]
    private int action_int_04_ = 0;
    public int GetActionInt04() { return action_int_04_; }

    /// <summary>
    /// アクションFloat01
    /// </summary>
    [SerializeField]
    private float action_float_01_ = 0;
    public float GetActionFloat01() { return action_float_01_; }

    /// <summary>
    /// アクションFloat02
    /// </summary>
    [SerializeField]
    private float action_float_02_ = 0;
    public float GetActionFloat02() { return action_float_02_; }

    /// <summary>
    /// アクションFloat03
    /// </summary>
    [SerializeField]
    private float action_float_03_ = 0;
    public float GetActionFloat03() { return action_float_03_; }

    /// <summary>
    /// アクションFloat04
    /// </summary>
    [SerializeField]
    private float action_float_04_ = 0;
    public float GetActionFloat04() { return action_float_04_; }

    /// <summary>
    /// アクションstring01
    /// </summary>
    [SerializeField]
    private string action_string_01_ = "";
    public string GetActionString01() { return action_string_01_; }

    /// <summary>
    /// アクションstring02
    /// </summary>
    [SerializeField]
    private string action_string_02_ = "";
    public string GetActionString02() { return action_string_02_; }

    /// <summary>
    /// セットアップ
    /// </summary>
    /// <param name="value_data"></param>
    public void SetUp(MotionData value_data)
    {
        action_index_      = value_data.GetActionIndex();
        action_name_       = value_data.GetActionName();
        action_role_name_  = value_data.GetActionRoleName();
        action_id_         = value_data.GetActionID();
        action_frame_      = value_data.GetActionFrame();
        action_life_frame_ = value_data.GetActionLifeFrame();
        action_int_01_     = value_data.GetActionInt01();
        action_int_02_     = value_data.GetActionInt02();
        action_int_03_     = value_data.GetActionInt03();
        action_int_04_     = value_data.GetActionInt04();
        action_float_01_   = value_data.GetActionFloat01();
        action_float_02_   = value_data.GetActionFloat02();
        action_float_03_   = value_data.GetActionFloat03();
        action_float_04_   = value_data.GetActionFloat04();
        action_string_01_  = value_data.GetActionString01();
        action_string_01_  = value_data.GetActionString02();
    }
}
