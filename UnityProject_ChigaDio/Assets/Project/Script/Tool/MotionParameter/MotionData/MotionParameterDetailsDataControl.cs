﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// MotionParameterDetailsDataの管理クラス
/// </summary>
public class MotionParameterDetailsDataControl
{
    /// <summary>
    /// リスト
    /// </summary>
    Dictionary<int,List<MotionParameterDetailsData>> dictionary_motion_parameter_detailes_data_ = new Dictionary<int, List<MotionParameterDetailsData>>();

    public void Init()
    {
        dictionary_motion_parameter_detailes_data_ = new Dictionary<int, List<MotionParameterDetailsData>>();
    }

    /// <summary>
    /// エフェクトIDを返す
    /// </summary>
    /// <returns></returns>
    public List<int> GetEffectIDList()
    {
        if (dictionary_motion_parameter_detailes_data_.Count == 0) return null;

        List<int> get_effects_ = new List<int>();

        foreach(var dictionary_data in dictionary_motion_parameter_detailes_data_)
        {
            foreach(var data in dictionary_data.Value)
            {
                if(MotionParameterConvert.ConvertMotionActionRoleNameID(data.GetActionRoleName()) == MotionParameterConvert.MotionActionRoleName.Effect_Generate)
                {
                    get_effects_.Add(data.GetActionInt01());
                }
            }
        }

        return get_effects_;
    }


    /// <summary>
    /// バレットIDを返す
    /// </summary>
    /// <returns></returns>
    public List<int> GetBulletIDList()
    {
        if (dictionary_motion_parameter_detailes_data_.Count == 0) return null;

        List<int> get_effects_ = new List<int>();

        foreach (var dictionary_data in dictionary_motion_parameter_detailes_data_)
        {
            foreach (var data in dictionary_data.Value)
            {
                if (MotionParameterConvert.ConvertMotionActionRoleNameID(data.GetActionRoleName()) == MotionParameterConvert.MotionActionRoleName.Bullet_Generate)
                {
                    get_effects_.Add(data.GetActionInt01());
                }
            }
        }

        return get_effects_;
    }


    /// <summary>
    /// 追加
    /// </summary>
    /// <param name="list_motion_data"></param>
    public void AddListMotionData(List<MotionData> list_motion_data)
    {
        if (list_motion_data == null) return;
        if (list_motion_data.Count == 0) return;

        foreach(var data in list_motion_data)
        {
            MotionParameterDetailsData motion = new MotionParameterDetailsData();
            motion.SetUp(data);
            int id = MotionParameterConvert.ConvertMotionActionID(motion.GetActionName());
            if (dictionary_motion_parameter_detailes_data_.ContainsKey(id) == false)
            {
                dictionary_motion_parameter_detailes_data_.Add(id, new List<MotionParameterDetailsData>());
            }
            dictionary_motion_parameter_detailes_data_[id].Add(motion);
        }
    }

    public MotionParameterDetailsData GetMotionActionData(int motion_action_id, int index)
    {
        if (dictionary_motion_parameter_detailes_data_ == null) return null;
        if (dictionary_motion_parameter_detailes_data_.Count == 0) return null;
        if (dictionary_motion_parameter_detailes_data_.ContainsKey(motion_action_id) == false) return null;

        if (dictionary_motion_parameter_detailes_data_[motion_action_id].Count == 0) return null;

        if (dictionary_motion_parameter_detailes_data_[motion_action_id].Count <= index || index < 0) return null;

        return dictionary_motion_parameter_detailes_data_[motion_action_id][index];
    }


    public uint GetMotionActionLength(int motion_action_id)
    {
        if (dictionary_motion_parameter_detailes_data_ == null) return 0;
        if (dictionary_motion_parameter_detailes_data_.Count == 0) return 0;
        if (dictionary_motion_parameter_detailes_data_.ContainsKey(motion_action_id) == false) return 0;

        if (dictionary_motion_parameter_detailes_data_[motion_action_id].Count == 0) return 0;


        return (uint)dictionary_motion_parameter_detailes_data_[motion_action_id].Count;
    }


    public void Release()
    {
        if (dictionary_motion_parameter_detailes_data_ == null) return;

        dictionary_motion_parameter_detailes_data_.Clear();
        dictionary_motion_parameter_detailes_data_ = null;
    }
}
