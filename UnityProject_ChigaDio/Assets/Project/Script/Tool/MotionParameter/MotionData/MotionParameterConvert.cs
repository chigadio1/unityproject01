﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// コンバートクラス
/// </summary>
public static class MotionParameterConvert
{
    /// <summary>
    /// モーションパラメータの役割ID
    /// </summary>
    public enum MotionActionRoleName : int
    {
        Transform_Position = 0,            // 座標移動
        Transform_Target_TeleportPosition, // 瞬間移動
        Transform_Target_Homing,           // ホーミング回転
        Transform_Target_LookRotation,     // 瞬間回転
        Animation_Change,        // アニメーション切り替え
        Collision_Attack,        // 攻撃コリジョン
        Collision_Sensing,        // 感知コリジョン
        Effect_Generate,        // エフェクト生成
        Bullet_Generate,        // 弾丸生成
        Action_End,                         // アクションエンド
        Max_Action
    }

    /// <summary>
    /// アクションString
    /// </summary>
    private static string[][] motion_action_role_name_ = new string[(int)MotionActionRoleName.Max_Action][]
    {
        new string[] { "Transform_Position", "TransformPositionData", "TRANSFORM_POSITION" },
        new string[] { "Transform_Target_TeleportPosition", "TransformTargetTeleportPositionData", "TRANSFORM_TARGET_TELEPORTPOSITION" },
        new string[] { "Transform_Target_Homing", "TransformTargetHoming", "TRANSFORM_TARGET_HOMING" },
        new string[] { "Transform_Target_LookRotation", "TransformTargetLookRotation", "TRANSFORM_TARGET_LOOKROTATION" },
        new string[] { "Animation_Change", "AnimationChangeData", "ANIMATION_CHANGE"},
        new string[] { "Collision_Attack", "CollisionAttack", "COLLIISON_ATTACK"},
        new string[] { "Collision_Sensing", "CollisionSensing", "COLLISION_SENSING"},
        new string[] { "Effect_Generate", "EffectGenerate", "EFFECT_GENARATE"},
        new string[] { "Bullet_Generate", "BulletGenerate", "BULLET_GENARATE"},
        new string[] { "Action_End", "ActionEnd", "ACTION_END" }
    };

    /// <summary>
    /// アクションID
    /// </summary>
    public enum MotionActionName : int
    {
        Attack_Combo_A,      // 攻撃A
        Attack_Combo_B,      // 攻撃B
        Attack_Combo_C,      // 攻撃C
        Damage_KnockBack_S,  // ダメージリアクション(小)
        MAX_Action           // 最大
    }

    /// <summary>
    /// アクションString
    /// </summary>
    private static string[][] motion_action_name_ = new string[(int)MotionActionName.MAX_Action][]
    {
        new string[] {"Attack_Combo_A","ATTACK_COMBO_A","AttackComboA" },
        new string[] {"Attack_Combo_B","ATTACK_COMBO_B","AttackComboB" },
        new string[] {"Attack_Combo_C","ATTACK_COMBO_C", "AttackComboC" },
        new string[] {"Damage_KnockBack_S", "DAMAGE_KNOCKBACK_S", "DamageKnockBackS"},
    };

    public static int ConvertMotionActionID(string name)
    {
        MotionActionName id = MotionActionName.Attack_Combo_A;
        foreach (var list_action_name in motion_action_name_)
        {
            foreach(var action_name in list_action_name)
            {
                if (action_name == name) return (int)id;
            }
            id = (MotionActionName)(id + 1);
        }
        return (int)id;
    }

    public static MotionActionName ConvertMotionActionNameID(string name)
    {
        return (MotionActionName)ConvertMotionActionID(name);
    }

    public static int ConvertMotionActionRoleID(string name)
    {
        MotionActionRoleName id = MotionActionRoleName.Transform_Position;
        foreach (var list_action_name in motion_action_role_name_)
        {
            foreach (var action_name in list_action_name)
            {
                if (action_name == name) return (int)id;
            }
            id = (MotionActionRoleName)(id + 1);
        }
        return (int)id;
    }

    public static MotionActionRoleName ConvertMotionActionRoleNameID(string name)
    {
        return (MotionActionRoleName)ConvertMotionActionRoleID(name);
    }

}
