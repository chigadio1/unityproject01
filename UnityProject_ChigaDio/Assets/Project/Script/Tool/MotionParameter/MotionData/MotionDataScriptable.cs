﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/CreateMotionParameter")]
public class MotionDataScriptable : BaseExcelScriptableObject<MotionData>
{
}
