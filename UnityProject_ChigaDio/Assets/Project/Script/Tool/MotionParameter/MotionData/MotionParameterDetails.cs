﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 詳細データ
/// </summary>
public class MotionParameterDetails 
{
    /// <summary>
    /// モーションパラメーターを使うオブジェクト
    /// </summary>
    private GameObject this_object_ = null;
    public GameObject GetThisObject() { return this_object_; }

    /// <summary>
    /// ターゲットオブジェクト
    /// </summary>
    private GameObject target_object_ = null;
    public void SetTargetObject(GameObject value_target) { target_object_ = value_target; }
    public GameObject GetTargetObject() { return target_object_; }

    /// <summary>
    /// モーションデータ
    /// </summary>
    private MotionParameterDetailsDataControl motion_parameter_details_data_control_ = new MotionParameterDetailsDataControl();

    /// <summary>
    /// モーションデータ集
    /// </summary>
    private AddressableData<MotionDataScriptable> motion_data_ = null;

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }



    public void Init(string motion_path,GameObject value_this_obj)
    {
        motion_data_ = new AddressableData<MotionDataScriptable>();
        motion_data_.LoadStart(motion_path);
        this_object_ = value_this_obj;
        is_setup_ = false;
    }

    public void Update()
    {
        DataInput();
    }

    /// <summary>
    /// アクションデータ取り出し
    /// </summary>
    /// <param name="motion_action_id"></param>
    /// <param name="index"></param>
    /// <returns></returns>
    public MotionParameterDetailsData GetMotionActionData(int motion_action_id, int index)
    {
        if (motion_parameter_details_data_control_ == null) return null;
        if (is_setup_ == false) return null;

        return motion_parameter_details_data_control_.GetMotionActionData(motion_action_id, index);
    }

    /// <summary>
    /// モーションパラメーターにあるエフェクトIDリスト
    /// </summary>
    /// <returns></returns>
    public List<int> GetEffectIDList()
    {
        if (is_setup_ == false) return null;
        return motion_parameter_details_data_control_.GetEffectIDList();
    }

    /// <summary>
    /// モーションパラメーターにあるエフェクトIDリスト
    /// </summary>
    /// <returns></returns>
    public List<int> GetBulletIDList()
    {
        if (is_setup_ == false) return null;
        return motion_parameter_details_data_control_.GetBulletIDList();
    }


    /// <summary>
    /// アクションの長さを取得
    /// </summary>
    /// <param name="motion_action_id"></param>
    /// <returns></returns>
    public uint GetMotionActionLength(int motion_action_id)
    {
        if (motion_parameter_details_data_control_ == null) return 0;
        if (is_setup_ == false) return 0;
        return motion_parameter_details_data_control_.GetMotionActionLength(motion_action_id);
    }

    /// <summary>
    /// ロードしたデータを入れる
    /// </summary>
    private void DataInput()
    {
        if (is_setup_ == false)
        {
            if (motion_data_.GetFlagSetUpLoading())
            {
                motion_parameter_details_data_control_.AddListMotionData(motion_data_.GetAddressableData()?.GetListData());
                motion_data_.Release();
                is_setup_ = true;
            }
        }
    }

    public void Release()
    {
        motion_data_.Release();
    }



}
