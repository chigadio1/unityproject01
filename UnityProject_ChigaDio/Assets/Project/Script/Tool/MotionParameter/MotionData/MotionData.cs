﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// モーションデータクラス
/// </summary>
[System.Serializable]
public class MotionData : BaseComposition
{
    /// <summary>
    /// アクションインデックス
    /// </summary>
    [SerializeField]
    private int action_index_ = 0;
    public int GetActionIndex() { return action_index_; }

    /// <summary>
    /// アクションネーム
    /// </summary>
    [SerializeField]
    private string action_name_ = "";
    public string GetActionName() { return action_name_; }

    /// <summary>
    /// アクション役割ネーム
    /// </summary>
    [SerializeField]
    private string action_role_name_ = "";
    public string GetActionRoleName() { return action_role_name_; }

    /// <summary>
    /// アクションID
    /// </summary>
    [SerializeField]
    private int action_id_ = 0;
    public int GetActionID() { return action_id_; }

    /// <summary>
    /// アクション実行フレーム開始
    /// </summary>
    [SerializeField]
    private float action_frame_ = 0;
    public float GetActionFrame() { return action_frame_; }

    /// <summary>
    /// アクション実行フレーム生存
    /// </summary>
    [SerializeField]
    private float action_life__frame_ = 0;
    public float GetActionLifeFrame() { return action_life__frame_; }

    /// <summary>
    /// アクションInt01
    /// </summary>
    [SerializeField]
    private int action_int_01_ = 0;
    public int GetActionInt01() { return action_int_01_; }

    /// <summary>
    /// アクションInt02
    /// </summary>
    [SerializeField]
    private int action_int_02_ = 0;
    public int GetActionInt02() { return action_int_02_; }

    /// <summary>
    /// アクションInt01
    /// </summary>
    [SerializeField]
    private int action_int_03_ = 0;
    public int GetActionInt03() { return action_int_03_; }

    /// <summary>
    /// アクションInt01
    /// </summary>
    [SerializeField]
    private int action_int_04_ = 0;
    public int GetActionInt04() { return action_int_04_; }

    /// <summary>
    /// アクションFloat01
    /// </summary>
    [SerializeField]
    private float action_float_01_ = 0;
    public float GetActionFloat01() { return action_float_01_; }

    /// <summary>
    /// アクションFloat02
    /// </summary>
    [SerializeField]
    private float action_float_02_ = 0;
    public float GetActionFloat02() { return action_float_02_; }

    /// <summary>
    /// アクションFloat03
    /// </summary>
    [SerializeField]
    private float action_float_03_ = 0;
    public float GetActionFloat03() { return action_float_03_; }

    /// <summary>
    /// アクションFloat04
    /// </summary>
    [SerializeField]
    private float action_float_04_ = 0;
    public float GetActionFloat04() { return action_float_04_; }

    /// <summary>
    /// アクションstring01
    /// </summary>
    [SerializeField]
    private string action_string_01_ = "";
    public string GetActionString01() { return action_string_01_; }

    /// <summary>
    /// アクションstring02
    /// </summary>
    [SerializeField]
    private string action_string_02_ = "";
    public string GetActionString02() { return action_string_02_; }



    public override void ManualSetUp(ref DataFrameGroup data_group, ref ProjectSystem.ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("ActionIndex", nameof(action_index_), action_index_);
        data_group.AddData("ActionName", nameof(action_name_), action_name_);
        data_group.AddData("ActionRoleName", nameof(action_role_name_), action_role_name_);
        data_group.AddData("ActionID", nameof(action_id_), action_id_);
        data_group.AddData("ActionFrame", nameof(action_frame_), action_frame_);
        data_group.AddData("ActionLifeFrame", nameof(action_life__frame_), action_life__frame_);
        data_group.AddData("Int01", nameof(action_int_01_), action_int_01_);
        data_group.AddData("Int02", nameof(action_int_02_), action_int_02_);
        data_group.AddData("Int03", nameof(action_int_03_), action_int_03_);
        data_group.AddData("Int04", nameof(action_int_04_), action_int_04_);
        data_group.AddData("Float01", nameof(action_float_01_), action_float_01_);
        data_group.AddData("Float02", nameof(action_float_02_), action_float_02_);
        data_group.AddData("Float03", nameof(action_float_03_), action_float_03_);
        data_group.AddData("Float04", nameof(action_float_04_), action_float_04_);
        data_group.AddData("String01", nameof(action_string_01_), action_string_01_);
        data_group.AddData("String02", nameof(action_string_02_), action_string_02_);
    }



}
