﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

/// <summary>
/// MotionDataのデータインスペクター
/// </summary>
[CustomEditor(typeof(MotionDataScriptable))]
public class EditorMotionDataScriptableInspector : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<MotionData>(target as BaseExcelScriptableObject<MotionData>);

        base.OnInspectorGUI();
    }


}
#endif