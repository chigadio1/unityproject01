﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
/// <summary>
/// シナリオテキスト
/// </summary>
[System.Serializable]
public class ScenarioText : BaseComposition
{
    public enum type
    {
        bulter,
        yu,
        driver
    }

    [SerializeField]
    private int id_ = 0;
    public  int GeID()
    {
        return  id_;
    }

    [SerializeField]
    private type name_ = type.yu;
    public  type GetName()
    {
       return  name_; 
    }

    [SerializeField]
    private string text_ = "";
    public  string GetText()
    {
        return  text_;
    }


    public ScenarioText()
    {
        name_ = type.yu;
        text_ = "";
    }


    public override void ManualSetUp(ref DataFrameGroup data_group, ref ProjectSystem.ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("id", nameof(id_),id_);
        data_group.AddData("発言者", nameof(name_), name_);
        data_group.AddData("sentence", nameof(text_), text_);
    }

}
