﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
public class EditorExcelScriptableObjectInspector
{
    /// <summary>
    /// インスペクタ―に表示
    /// </summary>
    /// <param name="excel_scriptable_object"></param>
    public static void OnGUI<T>(BaseExcelScriptableObject<T> excel_scriptable_object) where T : BaseComposition,new()
    {
        if (excel_scriptable_object == null) return;



        EditorGUI.BeginChangeCheck();

        GUILayout.BeginHorizontal();

        excel_scriptable_object.DataID = (uint)EditorGUILayout.IntField("ID",(int)excel_scriptable_object.DataID);

        GUILayout.EndHorizontal();

        excel_scriptable_object.IsOpenExcel = EditorGUILayout.Foldout(excel_scriptable_object.IsOpenExcel, "Excel");

        if (excel_scriptable_object.IsOpenExcel)
        {
            EditorGUI.indentLevel++;
            GUILayout.BeginVertical();
            GUILayout.Label("PATH");
            excel_scriptable_object.ExcelLoadPath = GUILayout.TextField(excel_scriptable_object.ExcelLoadPath);
            if (GUILayout.Button("..."))
            {
                if (excel_scriptable_object.data_id_ == 0)
                {
                    excel_scriptable_object.ExcelLoadPath = FileDialog(excel_scriptable_object.ExcelLoadPath,"xlsx");
                }
                else
                {
                    excel_scriptable_object.ExcelLoadPath = FolderDialog(excel_scriptable_object.ExcelLoadPath);
                }
                GUIUtility.ExitGUI();
            }
            GUILayout.EndVertical();
          
            GUILayout.BeginVertical();
            GUILayout.Label("SHEET");
            excel_scriptable_object.ExcelLoadSheetName = GUILayout.TextField(excel_scriptable_object.ExcelLoadSheetName);
            GUILayout.EndVertical();
            EditorGUI.indentLevel--;
        }

        excel_scriptable_object.IsOpenJson = EditorGUILayout.Foldout(excel_scriptable_object.IsOpenJson, "Json");
        if (excel_scriptable_object.IsOpenJson)
        {
            EditorGUI.indentLevel++;
            GUILayout.BeginVertical();
            GUILayout.Label("JSON_PATH");
            excel_scriptable_object.JsonDataPath = GUILayout.TextField(excel_scriptable_object.JsonDataPath);
            if (GUILayout.Button("..."))
            {
                excel_scriptable_object.JsonDataPath = FolderDialog(excel_scriptable_object.JsonDataPath);
            }
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            GUILayout.Label("JSON_NAME");
            excel_scriptable_object.JsonDataName = GUILayout.TextField(excel_scriptable_object.JsonDataName);
            GUILayout.EndVertical();
            EditorGUI.indentLevel--;
        }

        excel_scriptable_object.IsOpenBinary = EditorGUILayout.Foldout(excel_scriptable_object.IsOpenBinary, "Binary");
        if (excel_scriptable_object.IsOpenBinary)
        {
            EditorGUI.indentLevel++;
            GUILayout.BeginVertical();
            GUILayout.Label("BINARY_PATH");
            excel_scriptable_object.BinaryDataPath = GUILayout.TextField(excel_scriptable_object.BinaryDataPath);
            if (GUILayout.Button("..."))
            {
                excel_scriptable_object.BinaryDataPath = FolderDialog(excel_scriptable_object.BinaryDataPath);
                GUIUtility.ExitGUI();
            }
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            GUILayout.Label("BINARY_NAME");
            excel_scriptable_object.BinaryDataName = GUILayout.TextField(excel_scriptable_object.BinaryDataName);
            GUILayout.EndVertical();
            EditorGUI.indentLevel--;
        }

        if (GUILayout.Button("CREATE"))
        {
            excel_scriptable_object.CreateManual();
            EditorUtility.SetDirty(excel_scriptable_object);
            AssetDatabase.SaveAssets();
        }

        if (GUILayout.Button("EXPORT"))
        {
            excel_scriptable_object.CreateExport();
            EditorUtility.SetDirty(excel_scriptable_object);
            AssetDatabase.SaveAssets();
        }

        if (GUILayout.Button("SAVE_JSON"))
        {
            ProjectSystem.ExcelJsonSystem<T>.SaveJson(excel_scriptable_object.GetListData(),
            excel_scriptable_object.JsonDataPath,
            excel_scriptable_object.JsonDataName);
            AssetDatabase.Refresh();
        }
        if (GUILayout.Button("SAVE_BINARY"))
        {
            ProjectSystem.ExcelBinarySystem<T>.SaveBinary(excel_scriptable_object.GetListData(),
            excel_scriptable_object.BinaryDataPath,
            excel_scriptable_object.BinaryDataName);
            AssetDatabase.Refresh();
        }
        if (GUILayout.Button("LOAD_JSON"))
        {
            
            var list = ProjectSystem.ExcelJsonSystem<T>.LoadJsonEditor(excel_scriptable_object.GetListData(),
            excel_scriptable_object.JsonDataPath + "/" + excel_scriptable_object.JsonDataName + ".json");
            excel_scriptable_object.SetListData(list);
        }
        if (GUILayout.Button("LOAD_BINARY"))
        {
            var list = ProjectSystem.ExcelBinarySystem<T>.LoadBinary(
            excel_scriptable_object.BinaryDataPath,
            excel_scriptable_object.BinaryDataName);
            excel_scriptable_object.SetListData(list);
        }
        if (GUILayout.Button("CLEAR"))
        {
            excel_scriptable_object.GetListData().Clear();
        }

        

        EditorUtility.SetDirty(excel_scriptable_object); // Dirtyフラグを立てることで、Unity終了時に勝手に.assetに書き出す
    }

    public static string FolderDialog(string default_path)
    {
        string path = @"C:\";
        if (default_path != null) path = default_path;
        //パスの取得
        var return_path = EditorUtility.OpenFolderPanel("Open Folder", path, "");

        return return_path;

    }


    public static string FileDialog(string default_path,string file_type)
    {
        string path = @"C:\";
        if (default_path != null) path = default_path;
        //パスの取得
        var return_path = EditorUtility.OpenFilePanelWithFilters("Open " + file_type, path, new[] { "Dataファイル", file_type });

        return return_path;

    }
}
#endif