﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 基礎Excelスクリプタブルオブジェクト
/// </summary>
/// <typeparam name="T"></typeparam>
[System.Serializable]
public class BaseExcelScriptableObject<T> : ScriptableObject where T : BaseComposition,new()
{
    [SerializeField]
    protected ListDataObject<T> list_data_object_ = new ListDataObject<T>();
    public List<T> GetListData()
    {
        return list_data_object_.GetListDataObject();
    }
    public T GetDataObject(int index)
    {
        if (list_data_object_ == null) return null;
        var list_data = list_data_object_.GetListDataObject();
        if (list_data == null) return null;
        if (list_data.Count == 0) return null;
        if (index >= list_data.Count || index < 0) return null;

        return list_data[index];
    }
    public void SetListData(List<T> value_list_data)
    {
        list_data_object_.SetListDataObject(value_list_data);
    }

    /// <summary>
    /// データID
    /// </summary>
    public uint data_id_ = 0;
    public uint DataID
    {
        set { data_id_ = value; }
        get { return data_id_; }
    }

    public BaseExcelScriptableObject()
    {

    }

    /// <summary>
    /// 初期化
    /// </summary>
    protected void Init()
    {
        list_data_object_ = new ListDataObject<T>();
        list_data_object_.SetUp();
    }
#if UNITY_EDITOR
    [SerializeField]
    protected string excel_load_path_ = "";
    public string ExcelLoadPath
    {
        set { excel_load_path_ = value; }
        get { return excel_load_path_; }
    }
    [SerializeField]
    protected string excel_load_sheet_name_ = "";
    public string ExcelLoadSheetName
    {
        set { excel_load_sheet_name_ = value; }
        get { return excel_load_sheet_name_; }
    }
    [SerializeField]
    protected string json_data_name = "";
    public string JsonDataName
    {
        set { json_data_name = value; }
        get { return json_data_name; }
    }
    [SerializeField]
    protected string json_data_save_path = "";
    public string JsonDataPath
    {
        set { json_data_save_path = value; }
        get { return json_data_save_path; }
    }
    [SerializeField]
    protected string binary_data_name = "";
    public string BinaryDataName
    {
        set { binary_data_name = value; }
        get { return binary_data_name; }
    }
    [SerializeField]
    protected string binary_data_save_path = "";
    public string BinaryDataPath
    {
        set { binary_data_save_path = value; }
        get { return binary_data_save_path; }
    }

    [SerializeField]
    protected bool is_open_excel = false;
    public bool IsOpenExcel
    {
        set { is_open_excel = value; }
        get { return is_open_excel; }
    }
    [SerializeField]
    protected bool is_open_json = false;
    public bool IsOpenJson
    {
        set { is_open_json = value; }
        get { return is_open_json; }
    }
    [SerializeField]
    protected bool is_open_binary = false;
    public bool IsOpenBinary
    {
        set { is_open_binary = value; }
        get { return is_open_binary; }
    }

#endif


#if UNITY_EDITOR
    public void Create()
    {
        Init();
        if (data_id_ == 0)
        {
            list_data_object_.SetListDataObject(CreateExcelData<T>.CreateAuto(excel_load_path_, excel_load_sheet_name_));
        }
        else
        {
            list_data_object_.SetListDataObject(CreateExcelData<T>.CreateAuto(excel_load_path_ + @"\" + $"{data_id_:0000}" + @"\" + $"{data_id_:0000}.xlsx", excel_load_sheet_name_));
        }
    }

    public void CreateManual()
    {
        Init();
        if (data_id_ == 0)
        {
            list_data_object_.SetListDataObject(CreateExcelData<T>.CreateManual(excel_load_path_, excel_load_sheet_name_));
        }
        else
        {
            list_data_object_.SetListDataObject(CreateExcelData<T>.CreateManual(excel_load_path_ + @"\" + $"{data_id_:0000}" + @"\" + $"{data_id_:0000}.xlsx", excel_load_sheet_name_));
        }
    }

    public void CreateExport()
    {
        if (data_id_ == 0)
        {
            CreateExcelData<T>.ExportManual(excel_load_path_, excel_load_sheet_name_,this);
        }
        else
        {
            CreateExcelData<T>.ExportManual(excel_load_path_ + @"\" + $"{data_id_:0000}" + @"\" + $"{data_id_:0000}.xlsx", excel_load_sheet_name_,this);
        }
    }
#endif
}
