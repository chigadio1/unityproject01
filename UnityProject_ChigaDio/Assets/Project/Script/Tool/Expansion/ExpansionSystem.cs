﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
/// <summary>
/// 拡張システム
/// </summary>
public　static class ExpansionSystem
{
    public static T AllComponent<T>(this GameObject self) where T : Component
    {
        T mono = self.GetComponent<T>();
        if (mono == null) mono = self.GetComponentInParent<T>();
        if (mono == null) mono = self.GetComponentInChildren<T>();

        return mono;
    }

    public static T[] AllComponents<T>(this GameObject self) where T : Component
    {
        T[] mono = self.GetComponents<T>();
        T[] mono1 = self.GetComponentsInParent<T>();
        T[] mono2 = self.GetComponentsInChildren<T>();

        mono = mono.Concat(mono1).ToArray();
        mono = mono.Concat(mono2).ToArray();

        return mono;
    }

    public static T GetSearchObjectName<T>(this GameObject self,string search_name) where T : Component
    {
        T[] mono = self.GetComponentsInChildren<T>();

        foreach(var data in mono)
        {
            if (data.gameObject.name == search_name) return data;
        }
        return null;
    }

    public static void HomingRotation(this GameObject self,GameObject target,float add_angle)
    {
        self.HomingRotation(target.transform.position, add_angle);
    }

    public static void HomingRotation(this GameObject self, Transform target, float add_angle)
    {
        self.HomingRotation(target.position, add_angle);
    }

    public static void HomingRotation(this GameObject self, Vector3 target, float add_angle)
    {
        Vector3 self_position = self.transform.position;

        Vector3 target_to_self_vector = (target - self_position);

        float target_to_self_angle = Quaternion.LookRotation(target_to_self_vector).eulerAngles.y;
        target_to_self_angle = target_to_self_angle.ClampAngleY180();

        float self_angle_y_ = self.ClampAngleY180();

        float cross_y = Vector3.Cross(self.transform.forward, target_to_self_vector.normalized).y;
        cross_y = cross_y >= 0.0f ? 1.0f : -1.0f;

        if(cross_y == 1)
        {
            if(add_angle + self_angle_y_ < target_to_self_angle)
            {
                self.transform.Rotate(Vector3.up, add_angle);
            }
            else
            {
                self.transform.rotation = Quaternion.LookRotation(target_to_self_vector);
            }
        }
        else
        {
            if (self_angle_y_ - add_angle > target_to_self_angle)
            {
                self.transform.Rotate(Vector3.up, -add_angle);
            }
            else
            {
                self.transform.rotation = Quaternion.LookRotation(target_to_self_vector);
            }
        }
    }

    public static float ClampAngleY180(this GameObject self)
    {
        float angle_y = self.transform.rotation.eulerAngles.y;

        return angle_y >= 180.0f ? 180.0f - angle_y : angle_y;
    }

    public static float ClampAngleY180(this Transform self)
    {
        float angle_y = self.rotation.eulerAngles.y;

        return angle_y >= 180.0f ? 180.0f - angle_y : angle_y;
    }

    public static float ClampAngleY180(this float self)
    {
        float angle_y = self;

        return angle_y >= 180.0f ? 180.0f - angle_y : angle_y;
    }
}
