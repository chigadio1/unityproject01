﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 数字メッシュUI
/// </summary>
public class NumbersMesh
{
    /// <summary>
    /// メッシュリスト
    /// </summary>
    private List<UIMesh> list_mesh_ = new List<UIMesh>();

    /// <summary>
    /// 数字
    /// </summary>
    private int number_ = 0;

    /// <summary>
    /// 保存数字
    /// </summary>
    private int[] save_numbers_ = null;

    /// <summary>
    /// 幅
    /// </summary>
    private float width_ = 0.0f;
    /// <summary>
    /// 高さ
    /// </summary>
    private float height_ = 0.0f;

    /// <summary>
    /// 計測時間
    /// </summary>
    private float time_ = 0.0f;

    /// <summary>
    /// 桁数
    /// </summary>
    private int digit_ = 0;

    /// <summary>
    /// 座標
    /// </summary>
    private Vector3 position_ = Vector3.zero;

    /// <summary>
    /// 終了フラグ
    /// </summary>
    private bool is_end_ = false;
    public bool GetIsEnd() { return is_end_; }

    /// <summary>
    /// 色
    /// </summary>
    private Color color_ = Color.white;


    public void SetUp(Vector3 world_position,float value_width,float value_height,int value_number,Color color)
    {
        number_ = value_number;
        width_ = value_width;
        height_ = value_height;
        position_ = world_position;
       
        color_ = color;


        digit_ = (number_ == 0) ? 1 : ((int)Mathf.Log10(number_) + 1);
        save_numbers_ = new int[digit_];
        

        int local_number = number_;
        int count = 0;

        if (local_number == 0)
        {
            save_numbers_[0] = number_;
        }

        else
        {
            while (local_number > 0)
            {
                save_numbers_[count] = local_number % 10;
                local_number = local_number / 10;
                count++;
            }
        }

        foreach(var num in save_numbers_)
        {
            list_mesh_.Add(new UIMesh());
        }

        time_ = 0.0f;
        is_end_ = false;
    }

    /// <summary>
    /// 描画
    /// </summary>
    /// <param name="vh"></param>
    public void Draw(VertexHelper vh,RectTransform transform)
    {
        if (digit_ == 0) return;
        Vector3 z_pos = Camera.main.WorldToScreenPoint(position_);
        if (z_pos.z <= 0.0f)
        {
            TimeUpdate();
            return;
        }

        Vector3 pos = RectTransformUtility.WorldToScreenPoint(Camera.main, position_);
        float screen_width = Screen.width;
        float screen_height = Screen.height;



        pos.x -= screen_width / 2.0f;
        pos.y -= screen_height / 2.0f;

        Vector2[] uv = new Vector2[4];
        for (int count = 0; count < save_numbers_.Length; count++)
        {

            float u = (save_numbers_[count]/ 10.0f);
            uv[0] = new Vector2(u, 1.0f);
            uv[1] = new Vector2(u, 0.0f);
            uv[2] = new Vector2(u + 0.1f, 1.0f);
            uv[3] = new Vector2(u + 0.1f, 0.0f);
            list_mesh_[count].Draw(vh,
                                   pos,
                                   width_,
                                   height_,
                                   uv,
                                   color_);
            pos.x -= width_;
        }

        TimeUpdate();

    }

    private void TimeUpdate()
    {
        time_ += Time.deltaTime;
        if (time_ >= GameUIDefinition.GetNumberLifeMaxTime())
        {
            is_end_ = true;
        }
    }

}


