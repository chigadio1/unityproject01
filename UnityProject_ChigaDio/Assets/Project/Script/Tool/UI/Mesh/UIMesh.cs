﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI用のメッシュ
/// </summary>
public class UIMesh
{
    /// <summary>
    /// 頂点情報
    /// </summary>
    private List<Vector2> list_vertex_ = new List<Vector2>();

 

    /// <summary>
    /// 描画
    /// </summary>
    /// <param name="vh"></param>
    public void Draw(VertexHelper vh, Vector3 draw_start_position, float size_width, float size_height, Color color)
    {
        DrawMesh(vh,draw_start_position,size_width,size_height,color);
    }


    /// <summary>
    /// 描画
    /// </summary>
    /// <param name="vh"></param>
    public void Draw(VertexHelper vh, Vector3 draw_start_position, float size_width, float size_height, Vector2[] uv,Color color)
    {
        DrawMesh(vh, draw_start_position, size_width, size_height,uv, color);
    }

    protected virtual void DrawMesh(VertexHelper vh,Vector3 draw_start_position, float size_width,float size_height,Color color)
    {
        if (vh == null) return;
        list_vertex_.Clear();

        float size_width_half = size_width / 2.0f;
        float size_height_half = size_height / 2.0f;
        //左上
        list_vertex_.Add(new Vector3(draw_start_position.x - size_width_half, draw_start_position.y + size_width_half, 0.0f));
        //左下
        list_vertex_.Add(new Vector3(draw_start_position.x - size_width_half, draw_start_position.y - size_width_half, 0.0f));
        //右上
        list_vertex_.Add(new Vector3(draw_start_position.x + size_width_half, draw_start_position.y + size_width_half, 0.0f));
        //右下
        list_vertex_.Add(new Vector3(draw_start_position.x + size_width_half, draw_start_position.y - size_width_half, 0.0f));

        UIVertex[] ui_vertex = new UIVertex[list_vertex_.Count];
       
        for(int count = 0;  count < ui_vertex.Length; count++)
        {
            ui_vertex[count].color = color;
            ui_vertex[count].position = list_vertex_[count];
        }

        ui_vertex[0].uv0 = new Vector2(0.0f,1.0f);
        ui_vertex[1].uv0 = new Vector2(0.0f, 0.0f);
        ui_vertex[2].uv0 = new Vector2(1.0f, 1.0f);
        ui_vertex[3].uv0 = new Vector2(1.0f, 0.0f);

        vh.AddUIVertexQuad(new UIVertex[] {
            ui_vertex[1], ui_vertex[3], ui_vertex[2], ui_vertex[0]
        });

       

    }


    protected virtual void DrawMesh(VertexHelper vh, Vector3 draw_start_position, float size_width, float size_height, Vector2[] uv,Color color)
    {
        if (vh == null) return;
        list_vertex_.Clear();

        float size_width_half = size_width / 2.0f;
        float size_height_half = size_height / 2.0f;
        //左上
        list_vertex_.Add(new Vector3(draw_start_position.x - size_width_half, draw_start_position.y + size_width_half, 0.0f));
        //左下
        list_vertex_.Add(new Vector3(draw_start_position.x - size_width_half, draw_start_position.y - size_width_half, 0.0f));
        //右上
        list_vertex_.Add(new Vector3(draw_start_position.x + size_width_half, draw_start_position.y + size_width_half, 0.0f));
        //右下
        list_vertex_.Add(new Vector3(draw_start_position.x + size_width_half, draw_start_position.y - size_width_half, 0.0f));

        UIVertex[] ui_vertex = new UIVertex[list_vertex_.Count];

        for (int count = 0; count < ui_vertex.Length; count++)
        {
            ui_vertex[count].color = color;
            ui_vertex[count].position = list_vertex_[count];
        }

        ui_vertex[0].uv0 = uv[0];
        ui_vertex[1].uv0 = uv[1];
        ui_vertex[2].uv0 = uv[2];
        ui_vertex[3].uv0 = uv[3];

        vh.AddUIVertexQuad(new UIVertex[] {
            ui_vertex[1], ui_vertex[3], ui_vertex[2], ui_vertex[0]
        });



    }
}
