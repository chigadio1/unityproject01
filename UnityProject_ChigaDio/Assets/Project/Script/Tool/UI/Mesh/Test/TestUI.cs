﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TestUI : Graphic
{
    private NumbersMesh mesh_ = new NumbersMesh();

    public GameObject obj = null;

    void Start()
    {


    }

    void Update()
    {
        mesh_.SetUp(obj.transform.position, 100.0f, 100.0f, 10000, Color.white);

        SetVerticesDirty();
    }

    protected override void UpdateMaterial()
    {
        base.UpdateMaterial();
        canvasRenderer.SetTexture(material.mainTexture);

    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();



        mesh_.Draw(vh,gameObject.GetComponent<RectTransform>());

        
    }
}
