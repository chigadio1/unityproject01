﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseDataStorage<T,F,K> where T : BaseComposition,new() where F : BaseExcelScriptableObject<T>,new()
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    protected bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// アドレサブルデータ
    /// </summary>
    protected AddressableData<F> addressable_data_ = null;

    /// <summary>
    /// データリスト
    /// </summary>
    protected Dictionary<int,K> dictionary_data_ = new Dictionary<int, K>();

    public K GetData(int search_id)
    {
        if (dictionary_data_.ContainsKey(search_id) == false) return default(K);

        return dictionary_data_[search_id];
    }


    public Dictionary<int, K> GetAllData()
    {
        return dictionary_data_;
    }

    /// <summary>
    /// パス
    /// </summary>
    /// <returns></returns>
    protected virtual string GetPath(int id) { return ""; }

    /// <summary>
    /// ロード開始
    /// </summary>
    public void LoadStart(int id)
    {
        addressable_data_ = new AddressableData<F>();
        addressable_data_.LoadStart(GetPath(id));
        is_setup_ = false;
    }

    public void Update()
    {
        if (is_setup_ == true) return;
        is_setup_ = true;
        UpdateLoad();
    }

    protected  void UpdateLoad()
    {
        if (addressable_data_ == null)
        {
            is_setup_ = false;
            return;
        }
        if(addressable_data_.GetFlagSetUpLoading() == false)
        {
            is_setup_ = false;
            return;
        }

        SetForValue(addressable_data_.GetAddressableData().GetListData());
        addressable_data_.Release();
    }

    protected  void SetForValue(List<T> list_data)
    {
        foreach(var data in list_data)
        {
            SetValue(data);
        }
    }

    protected virtual void SetValue(T data)
    {

    }

}


public class BaseDataStorage<T, F> where T : BaseComposition, new() where F : BaseExcelScriptableObject<T>, new()
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    protected bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// アドレサブルデータ
    /// </summary>
    protected AddressableData<F> addressable_data_ = null;

    /// <summary>
    /// データリスト
    /// </summary>
    protected Dictionary<int, T> dictionary_data_ = new Dictionary<int, T>();

    public T GetData(int search_id)
    {
        if (dictionary_data_.ContainsKey(search_id) == false) return default(T);

        return dictionary_data_[search_id];
    }

    public Dictionary<int, T> GetAllData()
    {
        return dictionary_data_;
    }

    /// <summary>
    /// パス
    /// </summary>
    /// <returns></returns>
    protected virtual string GetPath(int id) { return ""; }

    /// <summary>
    /// ロード開始
    /// </summary>
    public void LoadStart(int id)
    {
        addressable_data_ = new AddressableData<F>();
        addressable_data_.LoadStart(GetPath(id));
        is_setup_ = false;
    }

    public void Update()
    {
        if (is_setup_ == true) return;
        is_setup_ = true;
        UpdateLoad();
    }

    protected void UpdateLoad()
    {
        if (addressable_data_ == null)
        {
            is_setup_ = false;
            return;
        }
        if (addressable_data_.GetFlagSetUpLoading() == false)
        {
            is_setup_ = false;
            return;
        }

        SetForValue(addressable_data_.GetAddressableData().GetListData());
        
    }

    protected void SetForValue(List<T> list_data)
    {
        foreach (var data in list_data)
        {
            SetValue(data);
        }
    }

    public void Release()
    {
        if (is_setup_ == false) return;
        if (addressable_data_.GetFlagSetUpLoading() == false) return;
        addressable_data_.Release();
    }

    protected virtual void SetValue(T data)
    {

    }

}