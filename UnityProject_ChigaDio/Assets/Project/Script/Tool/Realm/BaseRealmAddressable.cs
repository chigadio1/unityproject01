﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseRealmAddressable<T,E> where T : Object, new() where E : struct
{
    private Dictionary<E, Dictionary<int, AddressableData<T>>> dictionary_addressable_data_ = new Dictionary<E, Dictionary<int, AddressableData<T>>>();

    private bool is_setup_ = true;
    public bool GetIsSetUp() { return is_setup_; }

    public void Start()
    {
        foreach (E ee in System.Enum.GetValues(typeof(E)))
        {
            dictionary_addressable_data_.Add(ee, new Dictionary<int, AddressableData<T>>());
        }
    }

    protected virtual string GetPath(int id)
    {
        return "";
    }

    protected virtual string GetPath(E type)
    {
        return "";
    }

    public void LoadStartID(E type,int id)
    {
        if (dictionary_addressable_data_[type].ContainsKey(id) == true) return;

        AddressableData<T> data = new AddressableData<T>();
        data.LoadStart(GetPath(id));
        dictionary_addressable_data_[type].Add(id, data);
        is_setup_ = false;
    }

    public void LoadStartType(E type,int id)
    {
        if (dictionary_addressable_data_[type].ContainsKey(id) == true) return;

        AddressableData<T> data = new AddressableData<T>();
        data.LoadStart(GetPath(type));
        dictionary_addressable_data_[type].Add(id, data);
        is_setup_ = false;
    }

    public void Update()
    {
        if (is_setup_ == true) return;

        foreach(var data in dictionary_addressable_data_)
        {
            foreach(var chiled_data in data.Value)
            {
                if (chiled_data.Value.GetFlagSetUpLoading() == false) return;
            }
        }

        is_setup_ = true;
    }

    public void AllRelease()
    {

        foreach (var data in dictionary_addressable_data_)
        {
            foreach (var chiled_data in data.Value)
            {
                chiled_data.Value.Release();
            }
            data.Value.Clear();
        }

        dictionary_addressable_data_.Clear();
    }

    public void TypeRelease(E type)
    {
        foreach(var data in dictionary_addressable_data_[type])
        {
            data.Value.Release();
        }

        dictionary_addressable_data_[type].Clear();
    }

    public T GetAddressableData(E type, int id)
    {
        if (is_setup_ == false) return null;
        if (dictionary_addressable_data_.ContainsKey(type) == false) return null;
        if (dictionary_addressable_data_[type].ContainsKey(id) == false) return null;

        return dictionary_addressable_data_[type][id].GetAddressableData();
    }
}
