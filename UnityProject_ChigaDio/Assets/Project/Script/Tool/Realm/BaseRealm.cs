﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
/// <summary>
/// 基礎分野
/// </summary>
public class BaseRealm<T,E> where T : new() where E : struct
{
    protected Dictionary<E, T> dictionary_realm_datas_ = new Dictionary<E, T>();

    public void Start()
    {
        foreach(E ee in Enum.GetValues(typeof(E)))
        {
            dictionary_realm_datas_.Add(ee, new T());
        }
        Init();
    }

    public virtual void Init() { }

    public virtual void Update() { }
}
