﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
public class EditorPlacement : MonoBehaviour
{
    //=======================================
    //関数ポインタ配列
    public delegate int iFunction();
    public struct iArray
    {
        public iFunction iFun;
    }
    //======================================


    /// <summary>
    /// 配置データ
    /// </summary>
    private DungeonBattlePlacementScriptable placement_data_ = null;

    /// <summary>
    /// バトルメンバー
    /// </summary>
    private DungeonBattleMemberScriptable battle_member_data_ = null;

    /// <summary>
    /// ダンジョンUnit
    /// </summary>
    private DungeonUnitScriptable unit_data_ = null;

    private GameObject[] unit_object_ = null;

    /// <summary>
    /// MapID
    /// </summary>
    public int map_id_ = 0;

    /// <summary>
    /// 配置ID
    /// </summary>
    public int map_placement_id_ = 0;

    /// <summary>
    /// バトルメンバーID
    /// </summary>
    public int map_battle_member_id_ = 0;

    /// <summary>
    /// バトルエリア
    /// </summary>
    public float battle_area_length_ = 0.0f;

    public void LoadInit(int map_id)
    {
        unit_data_ = UnityEditor.AssetDatabase.LoadAssetAtPath<DungeonUnitScriptable>($"Assets/Project/Assets/Data/Map/{map_id_:0000}/DungeonUnit.asset");
        battle_member_data_ = UnityEditor.AssetDatabase.LoadAssetAtPath<DungeonBattleMemberScriptable>($"Assets/Project/Assets/Data/Map/{map_id_:0000}/BattleMember/DungeonBattleMember.asset");
        placement_data_ = UnityEditor.AssetDatabase.LoadAssetAtPath<DungeonBattlePlacementScriptable>($"Assets/Project/Assets/Data/Map/{map_id_:0000}/Placement/DungeonBattlePlacement.asset");

        var data = placement_data_.GetListData().Find(data_pla => data_pla.GetID() == map_placement_id_);
        if (data == null) return;

        transform.position = new Vector3(data.GetBattleAreaStartPositionX(), data.GetBattleAreaStartPositionY(), data.GetBattleAreaStartPositionZ());

        if (unit_object_ == null)
        {
            unit_object_ = new GameObject[BattleDefinition.GetMaxBattleUnitEnemy()];
        }
        for (int count = 0; count < BattleDefinition.GetMaxBattleUnitEnemy(); count++)
        {
            if (unit_object_[count] != null)
            {
                GameObject.DestroyImmediate(unit_object_[count]);
            }
        }

        map_id_ = map_id;
    }

    /// <summary>
    /// 初期
    /// </summary>
    /// <param name="map_id"></param>
    public void StartInit(int map_id)
    {
        if (placement_data_ == null) return;
        if (battle_member_data_ == null) return;
        if (unit_data_ == null) return;

        var search_battle_member = battle_member_data_.GetListData().Find(data => data.GetID() == map_battle_member_id_);
        if (search_battle_member == null) return;

        for (int count = 0; count < BattleDefinition.GetMaxBattleUnitEnemy(); count++)
        {
            if(unit_object_[count] != null)
            {
                GameObject.DestroyImmediate(unit_object_[count]);
            }
          
            var obj = new GameObject();
            obj.transform.position = gameObject.transform.position;
            obj.transform.rotation = Quaternion.identity;

            obj.name = $"Pin{count + 1:0000}_Unit";
            obj.transform.SetParent(gameObject.transform);
            obj.transform.localRotation = Quaternion.Euler(0.0f,180.0f, 0.0f);

            unit_object_[count] = obj;
        }

        StartTransform();
    }

    public void SetValue(int map_id, int placement_id = 1)
    {
        if (placement_data_ == null) return;
        SetTransform(placement_id);
        SetBattleArea(placement_id);
        placement_data_.CreateExport();
        EditorUtility.SetDirty(placement_data_);
        AssetDatabase.SaveAssets();
    }

    private DungeonBattlePlacementExcelData GetSearchID(int placement_id = 1)
    {
        if (placement_data_ == null) return null;

        return placement_data_.GetListData().Find(data => data.GetID() == placement_id);
    }

    public void StartTransform(int placement_id = 1)
    {
        var placement = GetSearchID(placement_id);
        if (placement == null) return;

        unit_object_[0].transform.localPosition = new Vector3(placement.GetPin01StartPositionX(), placement.GetPin01StartPositionY(), placement.GetPin01StartPositionZ());
        unit_object_[1].transform.localPosition = new Vector3(placement.GetPin02StartPositionX(), placement.GetPin02StartPositionY(), placement.GetPin02StartPositionZ());
        unit_object_[2].transform.localPosition = new Vector3(placement.GetPin03StartPositionX(), placement.GetPin03StartPositionY(), placement.GetPin03StartPositionZ());
        unit_object_[3].transform.localPosition = new Vector3(placement.GetPin04StartPositionX(), placement.GetPin04StartPositionY(), placement.GetPin04StartPositionZ());
        unit_object_[4].transform.localPosition = new Vector3(placement.GetPin05StartPositionX(), placement.GetPin05StartPositionY(), placement.GetPin05StartPositionZ());
        unit_object_[5].transform.localPosition = new Vector3(placement.GetPin06StartPositionX(), placement.GetPin06StartPositionY(), placement.GetPin06StartPositionZ());

        unit_object_[0].transform.localScale = new Vector3(placement.GetPin01StartScale(), placement.GetPin01StartScale(), placement.GetPin01StartScale());
        unit_object_[1].transform.localScale = new Vector3(placement.GetPin02StartScale(), placement.GetPin02StartScale(), placement.GetPin02StartScale());
        unit_object_[2].transform.localScale = new Vector3(placement.GetPin03StartScale(), placement.GetPin03StartScale(), placement.GetPin03StartScale());
        unit_object_[3].transform.localScale = new Vector3(placement.GetPin04StartScale(), placement.GetPin04StartScale(), placement.GetPin04StartScale());
        unit_object_[4].transform.localScale = new Vector3(placement.GetPin05StartScale(), placement.GetPin05StartScale(), placement.GetPin05StartScale());
        unit_object_[5].transform.localScale = new Vector3(placement.GetPin06StartScale(), placement.GetPin06StartScale(), placement.GetPin06StartScale());

        unit_object_[0].transform.localRotation = Quaternion.Euler(0.0f, unit_object_[0].transform.eulerAngles.y + placement.GetPin01StartRotation(), 0.0f);
        unit_object_[1].transform.localRotation = Quaternion.Euler(0.0f, unit_object_[1].transform.eulerAngles.y + placement.GetPin02StartRotation(), 0.0f);
        unit_object_[2].transform.localRotation = Quaternion.Euler(0.0f, unit_object_[2].transform.eulerAngles.y + placement.GetPin03StartRotation(), 0.0f);
        unit_object_[3].transform.localRotation = Quaternion.Euler(0.0f, unit_object_[3].transform.eulerAngles.y + placement.GetPin04StartRotation(), 0.0f);
        unit_object_[4].transform.localRotation = Quaternion.Euler(0.0f, unit_object_[4].transform.eulerAngles.y + placement.GetPin05StartRotation(), 0.0f);
        unit_object_[5].transform.localRotation = Quaternion.Euler(0.0f, unit_object_[5].transform.eulerAngles.y + placement.GetPin06StartRotation(), 0.0f);
    }

    public void SetTransform(int placement_id = 1)
    {
        var placement = GetSearchID(placement_id);
        if (placement == null) return;

        placement.SetPin01StartPositionX(unit_object_[0].transform.localPosition.x);
        placement.SetPin01StartPositionY(unit_object_[0].transform.localPosition.y);
        placement.SetPin01StartPositionZ(unit_object_[0].transform.localPosition.z);
        placement.SetPin02StartPositionX(unit_object_[1].transform.localPosition.x);
        placement.SetPin02StartPositionY(unit_object_[1].transform.localPosition.y);
        placement.SetPin02StartPositionZ(unit_object_[1].transform.localPosition.z);
        placement.SetPin03StartPositionX(unit_object_[2].transform.localPosition.x);
        placement.SetPin03StartPositionY(unit_object_[2].transform.localPosition.y);
        placement.SetPin03StartPositionZ(unit_object_[2].transform.localPosition.z);
        placement.SetPin04StartPositionX(unit_object_[3].transform.localPosition.x);
        placement.SetPin04StartPositionY(unit_object_[3].transform.localPosition.y);
        placement.SetPin04StartPositionZ(unit_object_[3].transform.localPosition.z);
        placement.SetPin05StartPositionX(unit_object_[4].transform.localPosition.x);
        placement.SetPin05StartPositionY(unit_object_[4].transform.localPosition.y);
        placement.SetPin05StartPositionZ(unit_object_[4].transform.localPosition.z);
        placement.SetPin06StartPositionX(unit_object_[5].transform.localPosition.x);
        placement.SetPin06StartPositionY(unit_object_[5].transform.localPosition.y);
        placement.SetPin06StartPositionZ(unit_object_[5].transform.localPosition.z);

        placement.SetPin01StartScale(unit_object_[0].transform.localScale.x);
        placement.SetPin02StartScale(unit_object_[1].transform.localScale.x);
        placement.SetPin03StartScale(unit_object_[2].transform.localScale.x);
        placement.SetPin04StartScale(unit_object_[3].transform.localScale.x);
        placement.SetPin05StartScale(unit_object_[4].transform.localScale.x);
        placement.SetPin06StartScale(unit_object_[5].transform.localScale.x);

        placement.SetPin01StartRotation(180.0f - unit_object_[0].transform.eulerAngles.y);
        placement.SetPin02StartRotation(180.0f - unit_object_[1].transform.eulerAngles.y);
        placement.SetPin03StartRotation(180.0f - unit_object_[2].transform.eulerAngles.y);
        placement.SetPin04StartRotation(180.0f - unit_object_[3].transform.eulerAngles.y);
        placement.SetPin05StartRotation(180.0f - unit_object_[4].transform.eulerAngles.y);
        placement.SetPin06StartRotation(180.0f - unit_object_[5].transform.eulerAngles.y);


    }

    void OnDrawGizmos()
    {
        Gizmo();
    }
    public void Gizmo()
    {
        if (unit_object_ == null) return;
        for(int count = 0; count < BattleDefinition.GetMaxBattleUnitEnemy(); count++)
        {
            if (unit_object_[count] == null) continue;
            Gizmos.DrawSphere(unit_object_[count].transform.position, 1.0f);
            Gizmos.DrawLine(unit_object_[count].transform.position, unit_object_[count].transform.position + (unit_object_[count].transform.forward * 2.0f));
        }
    }

    public void SetBattleArea(int placement_id = 1)
    {
        var placement = GetSearchID(placement_id);
        if (placement == null) return;

        placement.SetBattleAreaLength(battle_area_length_);
        placement.SetBattleAreaStartPositionX(transform.position.x);
        placement.SetBattleAreaStartPositionY(transform.position.y);
        placement.SetBattleAreaStartPositionZ(transform.position.z);

    }
}

[CustomEditor(typeof(EditorPlacement))]
public class EditorInspectorPlacement : Editor
{

    public override void OnInspectorGUI()
    {
        EditorPlacement editor = target as EditorPlacement;

        GUILayout.BeginHorizontal();

        EditorGUILayout.LabelField("MAP_ID");
        editor.map_id_ = EditorGUILayout.IntField(editor.map_id_);

        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("PLACEMENT_ID");
        editor.map_placement_id_ = EditorGUILayout.IntField(editor.map_placement_id_);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("BATTLEMEMBER_ID");
        editor.map_battle_member_id_ = EditorGUILayout.IntField(editor.map_battle_member_id_);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("BattleArea_Length");
        editor.battle_area_length_ = EditorGUILayout.FloatField(editor.battle_area_length_);
        GUILayout.EndHorizontal();

        GUILayout.BeginVertical();
        if (GUILayout.Button("Load"))
        {
            editor.LoadInit(editor.map_id_);
        }
        if (GUILayout.Button("PinSetting"))
        {
            editor.StartInit(editor.map_id_);
        }
        if (GUILayout.Button("Save"))
        {
            editor.SetValue(editor.map_id_, editor.map_placement_id_);
        }
        GUILayout.EndVertical();
    }

   
  


}


#endif