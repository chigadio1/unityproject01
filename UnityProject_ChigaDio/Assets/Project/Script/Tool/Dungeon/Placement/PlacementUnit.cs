﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

/// <summary>
/// 配置用Unit
/// </summary>
public class PlacementUnit : MonoBehaviour
{

    /// <summary>
    /// ボスフラグ
    /// </summary>
    private bool is_boss_ = false;
    public void OnIsBoss() { is_boss_ = true; }
    public void OffIsBoss() { is_boss_ = false; }

    /// <summary>
    /// リーダーフラグ
    /// </summary>
    private bool is_leader_ = false;
    public bool GetIsLeader() { return is_leader_; }

    /// <summary>
    ///UnitID
    /// </summary>
    private int unit_id_ = 0;
    public int GetUnitID() { return unit_id_; }

    /// <summary>
    /// UnitObj
    /// </summary>
    private GameObject unit_obj_ = null;

    public Vector3 GetUnitPosition() { return unit_obj_ != null ? unit_obj_.transform.position : Vector3.zero; }
    public Vector3 GetUnitScale() { return unit_obj_ != null ? unit_obj_.transform.localScale : Vector3.one; }
    public Quaternion GetUnitRotation() { return unit_obj_ != null ? unit_obj_.transform.rotation : Quaternion.identity; }

    public Vector3 GetUnitLocalPosition() { return unit_obj_ != null ? unit_obj_.transform.localPosition : Vector3.zero; }
    public Vector3 GetUnitLocalScale() { return unit_obj_ != null ? unit_obj_.transform.localScale : Vector3.one; }
    public Quaternion GetUnitLocalRotation() { return unit_obj_ != null ? unit_obj_.transform.localRotation : Quaternion.identity; }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="value_unit_id"></param>
    /// <param name="value_is_leaser"></param>
    /// <param name="value_is_boss"></param>
    public void StartInit(int value_unit_id,Transform parent,bool value_is_leader = false,bool value_is_boss = false)
    {
        unit_id_ = value_unit_id;
        is_boss_ = value_is_boss;
        is_leader_ = value_is_leader;

        unit_obj_ = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>($"Assets/Project/Assets/Model/Chara/{unit_id_:0000}/Prefab/{unit_id_:0000}_BattlUnit.prefab");
        unit_obj_.transform.localPosition = Vector3.zero;
        unit_obj_.transform.SetParent(parent);
    }
}

#endif
