﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(EditorDungeonRoot))]
public class EditorInspectorDungeonRoot : Editor
{
    public override void OnInspectorGUI()
    {
        var editor = target as EditorDungeonRoot;


        GUILayout.BeginHorizontal();
        GUILayout.Label("MAP_ID");
        editor.map_id_ = EditorGUILayout.IntField(editor.map_id_);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("ROOT_ID");
        editor.root_id_ = EditorGUILayout.IntField(editor.root_id_);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if(GUILayout.Button("LOAD"))
        {
            editor.Load();
        }
        if (editor.dungeon_root_data_ != null)
        {
            if (GUILayout.Button("LOAD_ROOT"))
            {
                editor.LoadRoot();
            }
        }
        GUILayout.EndHorizontal();

        if(editor.dungeon_root_data_ != null)
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("ADD"))
            {
                editor.AddRoot();
            }
            if (editor.dungeon_root_data_ != null)
            {
                if (GUILayout.Button("Save"))
                {
                    editor.Save();
                }
            }
            GUILayout.EndHorizontal();
        }

    }


}


public class EditorDungeonRoot : MonoBehaviour
{
    //ルートオブジェクト
    public List<GameObject> list_root_test_object_ = new List<GameObject>();

    /// <summary>
    /// マップID
    /// </summary>
    public int map_id_ = 9001;

    /// <summary>
    /// ルートID
    /// </summary>
    public int root_id_ = 1;

    public DungeonRootScriptable dungeon_root_data_ = null;

    public void Load()
    {
        dungeon_root_data_ = AssetDatabase.LoadAssetAtPath<DungeonRootScriptable>($"Assets/Project/Assets/Data/Map/{map_id_:0000}/Root/DungeonRoot.asset");
    }

    public void LoadRoot()
    {
        if (dungeon_root_data_ == null) return;

        var data = dungeon_root_data_.GetDungeonRootData(root_id_);
        if (data == null) return;

        foreach(var game_obj in list_root_test_object_)
        {
            if (game_obj == null) continue;
            GameObject.DestroyImmediate(game_obj);
        }
        list_root_test_object_.Clear();

        foreach(var root_data in data.GetListRootPosition())
        {
            var game_obj = new GameObject();
            game_obj.transform.position = Vector3.zero;

            game_obj.name = $"Root_{list_root_test_object_.Count:0000}";

            game_obj.transform.SetParent(gameObject.transform);

            game_obj.transform.position = root_data.GetRootPosition();

            list_root_test_object_.Add(game_obj);
        }
    }

    public void Save()
    {
        if (dungeon_root_data_ == null) return;
        if (list_root_test_object_ == null) return;
        if (list_root_test_object_.Count == 0) return;


        DungeonRootData root_data = new DungeonRootData();
        foreach(var root_obj in list_root_test_object_)
        {
            DungeonRootPosition root_position = new DungeonRootPosition();
            root_position.SetRootPosition(root_obj.transform.position);
            root_data.AddRootPosition(root_position);
        }
        root_data.SetID(root_id_);

        dungeon_root_data_.SetData(root_id_, root_data);

        EditorUtility.SetDirty(dungeon_root_data_);
        AssetDatabase.SaveAssets();
    }

    public void AddRoot()
    {
        if (dungeon_root_data_ == null) return;
        var game_obj = new GameObject();
        game_obj.transform.position = Vector3.zero;

        game_obj.name = $"Root_{list_root_test_object_.Count:0000}";

        game_obj.transform.SetParent(gameObject.transform);

        if (list_root_test_object_.Count == 0) game_obj.transform.position = gameObject.transform.position;
        else game_obj.transform.position = list_root_test_object_[list_root_test_object_.Count - 1].transform.position;

        list_root_test_object_.Add(game_obj);
    }

    public void Update()
    {
        if (dungeon_root_data_ == null) return;
        list_root_test_object_.RemoveAll(data => data == null);
    }
}
#endif