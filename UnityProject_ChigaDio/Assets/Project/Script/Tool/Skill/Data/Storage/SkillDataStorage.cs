﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// スキル―データ保管
/// </summary>
public class SkillDataStorage 
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// 辞書スキルデータ
    /// </summary>
    private Dictionary<int, SkillGameData> dictionary_skill_data_ = new Dictionary<int, SkillGameData>();

    /// <summary>
    /// 動的スキルデータ
    /// </summary>
    private AddressableData<SkillDataScriptable> addressable_skill_data_ = new AddressableData<SkillDataScriptable>();

    public SkillGameData GetSkillGameData(int serach_id)
    {
        if (dictionary_skill_data_.ContainsKey(serach_id) == false) return null;

        return dictionary_skill_data_[serach_id];
    }

    public void Init()
    {
        addressable_skill_data_.LoadStart($"Assets/Project/Assets/Skill/Skill.asset");

        is_setup_ = false;

    }

    public void Update()
    {
        SetUp();
    }

    private void SetUp()
    {
        if (is_setup_ == true) return;

        if (addressable_skill_data_.GetFlagSetUpLoading() == false) return;

        foreach(var data in addressable_skill_data_.GetAddressableData().GetListData())
        {
            SkillGameData game_data = new SkillGameData();
            game_data.SetValue(data);
            dictionary_skill_data_.Add(data.GetSkillID(), game_data);
            
        }

        addressable_skill_data_.Release();

        is_setup_ = true;
    }
}
