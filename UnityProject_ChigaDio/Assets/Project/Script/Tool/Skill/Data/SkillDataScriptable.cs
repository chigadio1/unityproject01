﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(SkillDataScriptable))]
public class EditorSkillDataScriptable : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<SkillExcelData>(target as BaseExcelScriptableObject<SkillExcelData>);
        base.OnInspectorGUI();
    }
}

#endif

/// <summary>
/// SkillExcelDataScriptable
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreateSkillData")]
public class SkillDataScriptable : BaseExcelScriptableObject<SkillExcelData>
{

}
