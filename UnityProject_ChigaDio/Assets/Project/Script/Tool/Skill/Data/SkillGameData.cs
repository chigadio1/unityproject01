﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// SkillGameData
/// </summary>
public class SkillGameData
{
    /// <summary>
    /// インデックス
    /// </summary>
    
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// スキルID
    /// </summary>
    
    private int skill_id_ = 0;
    public int GetSkillID() { return skill_id_; }

    /// <summary>
    /// スキルネーム
    /// </summary>
    
    private string skill_name_ = "";
    public string GetSkillName() { return skill_name_; }

    /// <summary>
    /// スキルタイプ
    /// </summary>
    
    private SkillExcelData.SkillType skill_type_ = SkillExcelData.SkillType.None;
    public SkillExcelData.SkillType GetSkillType() { return skill_type_; }

    /// <summary>
    /// スキルバフ目標タイプ  
    /// </summary>
    
    private SkillExcelData.SkillBuffTargetType skill_buff_target_type_ = SkillExcelData.SkillBuffTargetType.None;
    public SkillExcelData.SkillBuffTargetType GetSkillBuffTargetType() { return skill_buff_target_type_; }

    /// <summary>
    /// スキル範囲タイプ
    /// </summary>
    private SkillExcelData.SkillRangeType skill_range_type_ = SkillExcelData.SkillRangeType.ForRange;
    public SkillExcelData.SkillRangeType GetSkillRangeType() { return skill_range_type_; }

    /// <summary>
    /// Skillポイント
    /// </summary>
    private int skill_point_ = 0;
    public int GetSkillPoint() { return skill_point_; }

    /// <summary>
    /// Float01
    /// </summary>
    
    private float float_01_ = 0.0f;
    public float GetFloat01() { return float_01_; }

    /// <summary>
    /// スキル説明
    /// </summary>
    
    private string skill_explanation_ = "";
    public string GetSkillExplanation() { return skill_explanation_; }

    public void SetValue(SkillExcelData data)
    {
        index_ = data.GetIndex();
        skill_id_ = data.GetSkillID();
        skill_name_ = data.GetSkillName();
        skill_type_ = data.GetSkillType();
        skill_buff_target_type_ = data.GetSkillBuffTargetType();
        skill_range_type_ = data.GetSkillRangeType();
        skill_point_ = data.GetSkillPoint();
        float_01_ = data.GetFloat01();
        skill_explanation_ = data.GetSkillExplanation();
    }
}
