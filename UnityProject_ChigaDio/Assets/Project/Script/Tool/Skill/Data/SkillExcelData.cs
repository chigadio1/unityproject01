﻿using System.Collections;
using System.Collections.Generic;
using ProjectSystem;
using UnityEngine;

/// <summary>
/// スキルExcelデータ
/// </summary>
[System.Serializable]
public class SkillExcelData : BaseComposition
{
    /// <summary>
    /// スキルタイプ
    /// </summary>
    public enum SkillType
    {
        Attack,
        Heal,
        None,
    }

    /// <summary>
    /// バフ対象
    /// </summary>
    public enum SkillBuffTargetType
    {
        Alies,
        Opponent,
        None,
    }

    /// <summary>
    /// スキル発動範囲タイプ
    /// </summary>
    public enum SkillRangeType
    {
        ForRange,
        LengthRange,
        AllRange
    }


    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// スキルID
    /// </summary>
    [SerializeField]
    private int skill_id_ = 0;
    public int GetSkillID() { return skill_id_; }

    /// <summary>
    /// スキルネーム
    /// </summary>
    [SerializeField]
    private string skill_name_ = "";
    public string GetSkillName() { return skill_name_; }

    /// <summary>
    /// スキルタイプ
    /// </summary>
    [SerializeField]
    private SkillType skill_type_ = SkillType.None;
    public SkillType GetSkillType() { return skill_type_; }

    /// <summary>
    /// スキルバフ目標タイプ  
    /// </summary>
    [SerializeField]
    private SkillBuffTargetType skill_buff_target_type_ = SkillBuffTargetType.None;
    public SkillBuffTargetType GetSkillBuffTargetType() { return skill_buff_target_type_; }

    /// <summary>
    /// スキル範囲タイプ
    /// </summary>
    [SerializeField]
    private SkillRangeType skill_range_type_ = SkillRangeType.ForRange;
    public SkillRangeType GetSkillRangeType() { return skill_range_type_; }

    /// <summary>
    /// Skillポイント
    /// </summary>
    [SerializeField]
    private int skill_point_ = 0;
    public int GetSkillPoint() { return skill_point_; }

    /// <summary>
    /// Float01
    /// </summary>
    [SerializeField]
    private float float_01_ = 0.0f;
    public float GetFloat01() { return float_01_; }

    /// <summary>
    /// スキル説明
    /// </summary>
    [SerializeField]
    private string skill_explanation_ = "";
    public string GetSkillExplanation() { return skill_explanation_; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(skill_id_), skill_id_);
        data_group.AddData("Name", nameof(skill_name_), skill_name_);
        data_group.AddData("SkillType", nameof(skill_type_), skill_type_);
        data_group.AddData("BuffType", nameof(skill_buff_target_type_), skill_buff_target_type_);
        data_group.AddData("SkillRange", nameof(skill_range_type_), skill_range_type_);
        data_group.AddData("Point", nameof(skill_point_), skill_point_);
        data_group.AddData("Float01", nameof(float_01_), float_01_);
        data_group.AddData("説明", nameof(skill_explanation_), skill_explanation_);
    }

}
