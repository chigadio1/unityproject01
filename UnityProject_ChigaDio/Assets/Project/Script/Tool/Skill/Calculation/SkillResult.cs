﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// SkillResult
/// </summary>
public class SkillResult
{
    /// <summary>
    /// スキル発動者
    /// </summary>
    private BattleUnit skill_invoker_unit_ = null;

    /// <summary>
    /// スキル発動相手
    /// </summary>
    private List<BattleUnit> list_opponent_unit_ = new List<BattleUnit>();

    /// <summary>
    /// スキルデータ
    /// </summary>
    private SkillGameData skill_data_ = null;

    /// <summary>
    /// スキルポイント集
    /// </summary>
    private List<int> list_skill_point_ = new List<int>();
    public List<int> GetSkillPoint() { return list_skill_point_; }

    public void SetUp(BattleUnit value_skill_invoker_unit,SkillGameData value_skill_data)
    {
        skill_invoker_unit_ = value_skill_invoker_unit;
        skill_data_ = value_skill_data;
    }

    public void AddResultOpponentUnit(BattleUnit value_opponent_unit)
    {
        list_opponent_unit_.Add(value_opponent_unit);
    }

    public void Calculation()
    {
        if (skill_data_ == null) return;
        ///スキルが攻撃なら
        if(skill_data_.GetSkillType() == SkillExcelData.SkillType.Attack)
        {
            DamageCalulation();
        }
        //バフ、デバフなら
        else
        {

        }
    }

    /// <summary>
    /// ダメージ計算
    /// </summary>
    public void DamageCalulation()
    {
        foreach(var unit in list_opponent_unit_)
        {
#if UNITY_EDITOR
            Debug.Log($"Damege ->{skill_data_.GetSkillPoint():00000}");
            unit.AddHP(-skill_data_.GetSkillPoint());
#endif
            list_skill_point_.Add(skill_data_.GetSkillPoint());
        }
    }
}
