﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// スキルコア
/// </summary>
public class SkillCore : Singleton<SkillCore>
{

    /// <summary>
    /// スキルストレージ
    /// </summary>
    private SkillDataStorage skill_data_storage_ = new SkillDataStorage();

    public bool GetIsSetUp() { return skill_data_storage_.GetIsSetUp(); }
    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "SkillCore";
        skill_data_storage_.Init();
    }

    // Update is called once per frame
    void Update()
    {
        skill_data_storage_.Update();
    }

    public SkillGameData GetSkillGameData(int serach_id)
    {


        return skill_data_storage_.GetSkillGameData(serach_id);
    }
}
