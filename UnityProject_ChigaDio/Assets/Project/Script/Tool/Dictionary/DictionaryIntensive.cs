﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 辞書集約
/// </summary>
public class DictionaryIntensive
{

    /// <summary>
    /// リスト辞書
    /// </summary>
    private List<DictionaryType> list_dictionary_type_ = new List<DictionaryType>();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {

        for(int count = 0; count < (int)DictionaryType.Type.Max; count++)
        {
            var dictionary = new DictionaryType();
            dictionary.Load((DictionaryType.Type)count);
            list_dictionary_type_.Add(dictionary);
        }
    }

    public void Update()
    {
        if (is_setup_ == true) return;

        is_setup_ = true;

        foreach(var dictionary in list_dictionary_type_)
        {
            dictionary.Update();
            if(dictionary.GetIsSetUp() == false) { is_setup_ = false; }
        }
    }

    public void ReleaseAll()
    {
        foreach(var data in list_dictionary_type_)
        {
            data.Release();
        }

        list_dictionary_type_.Clear();
    }

    /// <summary>
    /// データリスト Get
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public List<DictionaryData> GetListDictionaryData(DictionaryType.Type type)
    {
        return list_dictionary_type_[(int)type].GetDictionaryData();
    }

    /// <summary>
    /// キャラクタ-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetCharacterDictionaryData(int search_id)
    {
        return GetDictionaryData(DictionaryType.Type.Character, search_id);
    }

    /// <summary>
    /// キャラクタ-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetCharacterDictionaryData(string search_name)
    {
        return GetDictionaryData(DictionaryType.Type.Character, search_name);
    }

    /// <summary>
    /// Effect-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetEffectDictionaryData(int search_id)
    {
        return GetDictionaryData(DictionaryType.Type.Effect, search_id);
    }

    /// <summary>
    /// Effect-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetEffectDictionaryData(string search_name)
    {
        return GetDictionaryData(DictionaryType.Type.Effect, search_name);
    }

    /// <summary>
    /// Animation-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetAnimationDictionaryData(int search_id)
    {
        return GetDictionaryData(DictionaryType.Type.Animation, search_id);
    }

    /// <summary>
    /// Animation-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetAnimationDictionaryData(string search_name)
    {
        return GetDictionaryData(DictionaryType.Type.Animation, search_name);
    }

    /// <summary>
    /// MotionParameterAction-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetMotionParameterActionDictionaryData(int search_id)
    {
        return GetDictionaryData(DictionaryType.Type.MotionParameterAction, search_id);
    }

    /// <summary>
    /// MotionParameterAction 全名前取得
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public List<string> GetMotionParameterActionDictionnaryName(DictionaryType.Type type)
    {
        return GetDictionnaryName(type);
    }

    /// <summary>
    /// MotionParameterAction-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetMotionParameterActionDictionaryData(string search_name)
    {
        return GetDictionaryData(DictionaryType.Type.MotionParameterAction, search_name);
    }


    /// <summary>
    /// Sound-ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData GetSoundDictionaryData(int search_id)
    {
        return GetDictionaryData(DictionaryType.Type.Sound, search_id);
    }

    /// <summary>
    /// Sound 全名前取得
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public List<string> GetSoundDictionnaryName(DictionaryType.Type type)
    {
        return GetDictionnaryName(type);
    }

    /// <summary>
    /// Sound-Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData GetSoundDictionaryData(string search_name)
    {
        return GetDictionaryData(DictionaryType.Type.Sound, search_name);
    }

    private DictionaryData GetDictionaryData(DictionaryType.Type type,int search_id)
    {
        return list_dictionary_type_[(int)type].SerachID(search_id);
    }
    private DictionaryData GetDictionaryData(DictionaryType.Type type, string search_name)
    {
        return list_dictionary_type_[(int)type].SerachName(search_name);
    }

    private List<string> GetDictionnaryName(DictionaryType.Type type)
    {
        return list_dictionary_type_[(int)type].GetDictionnaryName();
    }


}
