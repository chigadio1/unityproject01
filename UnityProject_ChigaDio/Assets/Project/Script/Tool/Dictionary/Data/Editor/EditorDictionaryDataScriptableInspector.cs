﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

/// <summary>
/// 辞書スクリプタブルインスペクター
/// </summary>
[CustomEditor(typeof(DictionaryScriptable))]
public class EditorDictionaryDataScriptableInspector : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<DictionaryData>(target as BaseExcelScriptableObject<DictionaryData>);

        base.OnInspectorGUI();
    }
}
#endif