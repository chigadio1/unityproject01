﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 辞書データ
/// </summary>
[System.Serializable]
public class DictionaryData : BaseComposition
{
    /// <summary>
    /// ID
    /// </summary>
    [SerializeField]
    private int id_ = 0;
    public int GetID() { return id_; }

    /// <summary>
    /// 名前
    /// </summary>
    [SerializeField]
    private string name_ = "";
    public string GetName() { return name_; }

    /// <summary>
    /// 詳細
    /// </summary>
    [SerializeField]
    private string details_ = "";
    public string GetDetails() { return details_; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ProjectSystem.ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("ID", nameof(id_), id_);
        data_group.AddData("Name", nameof(name_), name_);
        data_group.AddData("Details", nameof(details_), details_);
    }
}
