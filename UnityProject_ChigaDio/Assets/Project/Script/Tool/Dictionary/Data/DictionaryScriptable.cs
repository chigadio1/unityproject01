﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 辞書スクリプタブル
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreateDictionary")]
public class DictionaryScriptable : BaseExcelScriptableObject<DictionaryData>
{
}
