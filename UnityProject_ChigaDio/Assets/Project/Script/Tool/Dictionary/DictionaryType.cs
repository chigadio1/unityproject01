﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 辞書種類
/// </summary>
public class DictionaryType
{
    /// <summary>
    /// 種類
    /// </summary>
    public enum Type
    {
        Character,
        Effect,
        Animation,
        MotionParameterAction,
        Sound,
        Max
    }


    /// <summary>
    /// 辞書deta
    /// </summary>
    private AddressableData<DictionaryScriptable> dictionary_data_ = new AddressableData<DictionaryScriptable>();

    /// <summary>
    /// 辞書スクリプタブル
    /// </summary>
    private DictionaryScriptable dictionary_scriptable_ = null;
    public List<DictionaryData> GetDictionaryData()
    {
        if (dictionary_scriptable_ == null) return null;
        return dictionary_scriptable_.GetListData();
    }

    /// <summary>
    /// セットアップ
    /// </summary>
    /// <returns></returns>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// ロード
    /// </summary>
    /// <param name="dictionary_type"></param>
    public void Load(Type dictionary_type)
    {
        string type_name = GetTypeName(dictionary_type);

        dictionary_data_.LoadStart("Assets/Project/Assets/Data/Dictionary/" + type_name + "Dictionary.asset");
    }

    public void Update()
    {
        ValueData();
    }

    /// <summary>
    /// 指定したTypeを識別し、名前を返す
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public string GetTypeName(Type type)
    {
        switch (type)
        {
            case Type.Character:
                return "Character";
            case Type.Max:
                return "";
            case Type.Effect:
                return "Effect";
            case Type.Animation:
                return "Animation";
            case Type.MotionParameterAction:
                return "MotionParameterAction";
            case Type.Sound:
                return "Sound";
        }

        return "";
    }

    private void ValueData()
    {
        if (dictionary_scriptable_ != null) return;
        if (dictionary_data_.GetFlagSetUpLoading() == false) return;
        dictionary_scriptable_ = dictionary_data_.GetAddressableData();
        is_setup_ = true;
    }

    /// <summary>
    /// 検索ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public DictionaryData SerachID(int search_id)
    {
        if (dictionary_data_ == null) return null;
        if (GetIsSetUp() == false) return null;
        ValueData();

        var list =  dictionary_scriptable_.GetListData();

        foreach(var data in list)
        {
            if (data.GetID() == search_id) return data;
        }

        return null;
    }

    /// <summary>
    /// 検索Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public DictionaryData SerachName(string search_name)
    {
        if (dictionary_data_ == null) return null;
        if (GetIsSetUp() == false) return null;
        ValueData();

        var list = dictionary_scriptable_.GetListData();

        foreach (var data in list)
        {
            if (data.GetName() == search_name) return data;
        }

        return null;
    }

    public List<string> GetDictionnaryName()
    {
        List<string> list_name = new List<string>();

        foreach(var data in dictionary_scriptable_.GetListData())
        {
            list_name.Add(data.GetName());
        }

        return list_name;
    }

    public void Release()
    {
        if (dictionary_data_ == null) return;
        dictionary_data_.Release();
    }
}
