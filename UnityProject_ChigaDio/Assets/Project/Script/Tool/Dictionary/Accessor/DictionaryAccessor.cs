﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 辞書アクセサー
/// </summary>
public static class DictionaryAccessor
{
    /// <summary>
    /// ID Get
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static int GetID(DictionaryData data)
    {
        if (data == null) return AccessorError.GetErrorInt();

        return data.GetID();
    }

    /// <summary>
    /// Name Get
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string GetName(DictionaryData data)
    {
        if (data == null) return AccessorError.GetErrorString();

        return data.GetName();
    }

    /// <summary>
    /// Details Get
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string GetDetails(DictionaryData data)
    {
        if (data == null) return AccessorError.GetErrorString();

        return data.GetDetails();
    }

    /// <summary>
    /// 検索ID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public static DictionaryData GetCharacterDictionaryData(int search_id)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetCharacterDictionaryData(search_id);
    }

    /// <summary>
    /// 検索Name
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public static DictionaryData GetCharacterDictionaryData(string search_name)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetCharacterDictionaryData(search_name);
    }

    /// <summary>
    /// EffectID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public static DictionaryData GetEffectDictionaryData(int search_id)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetEffectDictionaryData(search_id);
    }

    /// <summary>
    /// EffectName
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public static DictionaryData GetEffectDictionaryData(string search_name)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetEffectDictionaryData(search_name);
    }

    /// <summary>
    /// AnimationID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public static DictionaryData GetAnimationDictionaryData(int search_id)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetAnimationDictionaryData(search_id);
    }

    /// <summary>
    /// AnimationName
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public static DictionaryData GetAnimationDictionaryData(string search_name)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetAnimationDictionaryData(search_name);
    }

    /// <summary>
    /// MotionParameterActionID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public static DictionaryData GetMotionParameterActionDictionaryData(int search_id)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetMotionParameterActionDictionaryData(search_id);
    }

    /// <summary>
    /// MotionParameterActionName
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public static DictionaryData GetMotionParameterActionDictionaryData(string search_name)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetMotionParameterActionDictionaryData(search_name);
    }
    /// <summary>
    /// MotionParameterActionNameを全取得
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public static List<string> GetMotionParameterActionDictionaryName()
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetMotionParameterActionDictionnaryName();
    }

    /// <summary>
    /// SoundID
    /// </summary>
    /// <param name="search_id"></param>
    /// <returns></returns>
    public static DictionaryData GetSoundDictionaryData(int search_id)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetSoundDictionaryData(search_id);
    }

    /// <summary>
    /// SoundName
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public static DictionaryData GetSoundDictionaryData(string search_name)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetSoundDictionaryData(search_name);
    }
    /// <summary>
    /// SoundNameを全取得
    /// </summary>
    /// <param name="search_name"></param>
    /// <returns></returns>
    public static List<string> GetSoundDictionaryName()
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetSoundDictionnaryName();
    }

    /// <summary>
    /// データリスト Get
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static List<DictionaryData> GetListDictionaryData(DictionaryType.Type type)
    {
        var data = DataCore.Instance;
        if (data.GetIsSetUp() == false) return null;

        return data.GetListDictionaryData(type);
    }
}
