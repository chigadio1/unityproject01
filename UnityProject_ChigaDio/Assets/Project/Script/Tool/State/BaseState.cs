﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ベースステート
/// </summary>
public class BaseState
{
    /// <summary>
    /// 終了フラグ
    /// </summary>
    protected bool is_end_ = false;
    public bool GetIsEnd() { return is_end_; }

    protected bool is_end_update_ = false;
    public bool GetIsEndUpdate() { return is_end_update_; }

    public virtual void Start() { }

    public void Update() 
    {
        if (is_end_ == true) return;
        if (is_end_update_ == true) return;
        UpdateDetails();
    }

    protected virtual void UpdateDetails()
    {
       
    }

    public void LateUpdate()
    {
        if (is_end_ == true) return;
        if (is_end_update_ == true) return;
        LateUpdateDetails();
    }

    protected virtual void LateUpdateDetails()
    {

    }

    public void End() { }
}
