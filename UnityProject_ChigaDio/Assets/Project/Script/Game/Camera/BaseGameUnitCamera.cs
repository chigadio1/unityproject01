﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ユニットカメラ
/// </summary>
public class BaseGameUnitCamera : BaseGameCamera
{
    private GameUnitCameraDetails camera_details_ = new GameUnitCameraDetails();


    #region Targetとの詳細
    /// <summary>
    /// 距離
    /// </summary>
    [SerializeField]
    protected float target_to_length_ = 8.26f;
    /// <summary>
    /// 高さ
    /// </summary
    [SerializeField]
    protected float target_to_height_ = 1.94f;
    /// <summary>
    /// 横
    /// </summary>
    [SerializeField]
    protected float target_to_width_ = 2.12f;

    /// <summary>
    /// 回転速度
    /// </summary>
    [SerializeField]
    protected float angle_speed_ = 1.0f;
    /// <summary>
    /// 振り向き速度
    /// </summary>
    [SerializeField]
    protected float look_speed_ = 50.0f;
    /// <summary>
    /// 移動速度
    /// </summary>
    [SerializeField]
    protected float move_speed_ = 50.0f;
    #endregion

    [SerializeField]
    private BaseUnit unit_ = null;
    public void SetTargetUnit(BaseUnit value)
    {
        unit_ = value;
    }
    public void Start()
    {

        camera_details_.SetUp(unit_,target_to_length_, target_to_height_, target_to_width_,angle_speed_,move_speed_, look_speed_);
    }

 

    public virtual void InitSetUp()
    {

    }

    public void LateUpdate()
    {
        
        //if(is_update_)
        //{
        //    is_update_ = false;
        //    return;
        //}
        UpdateCamera();
    }

    public void UpdateCamera()
    {
        if (is_stop_) return;
#if UNITY_EDITOR
        camera_details_.SetUp(unit_, target_to_length_, target_to_height_, target_to_width_, angle_speed_, move_speed_, look_speed_);
#endif
        camera_details_.Update(this);
        is_update_ = true;
    }
}
