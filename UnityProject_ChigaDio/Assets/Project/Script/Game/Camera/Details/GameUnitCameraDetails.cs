﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
/// <summary>
/// ユニットカメラ詳細
/// </summary>
public class GameUnitCameraDetails
{
    /// <summary>
    /// 目標ユニット
    /// </summary>
    private BaseUnit target_unit_ = null;

    /// <summary>
    /// ユニットとの距離
    /// </summary>
    private float target_to_length_ = 0.0f;

    /// <summary>
    /// ユニットとの上下
    /// </summary>
    private float tatget_to_height_ = 0.0f;

    /// <summary>
    /// ユニットとの左右
    /// </summary>
    private float target_to_width_ = 0.0f;

    /// <summary>
    /// 移動
    /// </summary>
    private float move_speed_ = 0.0f;
    /// <summary>
    /// 振り向き
    /// </summary>
    private float look_speed_ = 0.0f;

    /// <summary>
    /// 角度(XZ)
    /// </summary>
    private float angel_xz_ = 0.0f;
    /// <summary>
    /// 角度(Y)
    /// </summary>
    private float angel_y_ = 0.0f;

    private float angle_speed_ = 1.0f;

    public void SetUp(BaseUnit value_unit, float value_target_to_length, float value_target_to_height, float value_target_to_width, float value_angle_speed = 1.0f,float value_move_speed = 1.0f,float value_look_speed = 1.0f)
    {
        target_unit_ = value_unit;
        target_to_length_ = value_target_to_length;
        tatget_to_height_ = value_target_to_height;
        target_to_width_ = value_target_to_width;
        angle_speed_ = value_angle_speed;
        move_speed_ = value_move_speed;
        look_speed_ = value_look_speed;
    }

    public void Update(BaseGameUnitCamera camera)
    {
        if (target_unit_ == null) return;
        if (camera.GetStop() == true) return;
        InputUodate(camera);

        
    }

    private void InputUodate(BaseGameUnitCamera camera)
    {
        if(Keyboard.current.leftArrowKey.isPressed)
        {
            angel_xz_ -= angle_speed_;
        }
        else if (Keyboard.current.rightArrowKey.isPressed)
        {
            angel_xz_ += angle_speed_;
        }
        if (Keyboard.current.upArrowKey.isPressed)
        {
            angel_y_ -= angle_speed_;
        }
        else if (Keyboard.current.downArrowKey.isPressed)
        {
            angel_y_ += angle_speed_;
        }

        var camra_to_target = Quaternion.Euler(0.0f, angel_xz_, 0.0f) * Vector3.forward;
        camra_to_target = Quaternion.AngleAxis(angel_y_, camera.transform.right) * camra_to_target;
        Vector3 unit_pos = target_unit_.transform.position;

        Vector3 pos = unit_pos + (camra_to_target.normalized * target_to_length_);
        pos += camera.transform.right * target_to_width_;

        Vector3 move_pos_vector = (pos - camera.transform.position);

        if (pos == move_pos_vector) return;

        if (move_pos_vector.magnitude > move_speed_  * Time.deltaTime)
        {
            camera.transform.position += move_pos_vector.normalized * (move_speed_ * Time.deltaTime);
        }
        else
        {
            camera.transform.position = pos;

        }
        unit_pos.y += tatget_to_height_;



        Vector3 look_pos = unit_pos - camera.transform.position;
        look_pos += camera.transform.right * target_to_width_;
        
        
        camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, Quaternion.LookRotation(look_pos), look_speed_ * Time.deltaTime);
    }
}
