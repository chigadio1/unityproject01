﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGameCamera : MonoBehaviour
{
    /// <summary>
    /// 停止フラグ
    /// </summary>
    protected bool is_stop_ = false;
    public void OnStop() { is_stop_ = true; }
    public void OffStop() { is_stop_ = false; }
    public bool GetStop() { return is_stop_; }
    /// <summary>
    /// 更新処理したかどうか
    /// </summary>
    protected bool is_update_ = false;

    /// <summary>
    /// カメラ更新
    /// </summary>
    public virtual void CameraUpdate() { }
}
