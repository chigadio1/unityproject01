﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// マップ詳細
/// </summary>
public class MapDetails
{

    /// <summary>
    /// マップオブジェクト
    /// </summary>
    private AddressableData<GameObject> addressable_map_object_ = null;

    /// <summary>
    /// マップインスタンスオブジェクト
    /// </summary>
    private GameObject map_instance_object_ = null;
    public GameObject GetMapInstanceObject() { return map_instance_object_; }

    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    public void LoadStart(int map_id)
    {
        if (addressable_map_object_ != null) return;
        if (map_instance_object_ != null) return;
        addressable_map_object_ = new AddressableData<GameObject>();
        addressable_map_object_.LoadStart($"Assets/Project/Assets/Map/{map_id:0000}/Prefab/{map_id:0000}.prefab");
        is_setup_ = false;
    }

    public void Update()
    {
        if (is_setup_ == true) return;
        if (addressable_map_object_ == null) return;
        if (addressable_map_object_.GetFlagSetUpLoading() == false) return;

        is_setup_ = true;
    }

    public GameObject CreateMapObject(bool auto_addressable_release = true)
    {
        if (is_setup_ == false) return null;
        if (map_instance_object_ != null) return null;

        map_instance_object_ =  GameObject.Instantiate(addressable_map_object_.GetAddressableData(), Vector3.zero, Quaternion.identity);
        if(auto_addressable_release == true)
        {
            addressable_map_object_.Release();
            addressable_map_object_ = null;
        }

        BaseCollisionConstruction collision_ = new BaseCollisionConstruction();
        collision_.Init(map_instance_object_, CollisionLayer.kBackGround, CollisionPushType.kOpponentPush, LayerMask.NameToLayer("Collision"));

        return map_instance_object_;
    }

    public void Release()
    {
        if (addressable_map_object_ != null)
        {
            if (addressable_map_object_.GetFlagSetUpLoading() == false) return;
            addressable_map_object_.Release();
            addressable_map_object_ = null;

        }
        if (map_instance_object_ != null)
        {
            GameObject.Destroy(map_instance_object_);
            map_instance_object_ = null;
        }
    }
}
