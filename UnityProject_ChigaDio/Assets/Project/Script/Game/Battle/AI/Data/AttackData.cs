﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{ 
    /// <summary>
    /// AIの攻撃データ
    /// </summary>
    [System.Serializable]
    public class AttackData
    {
        /// <summary>
        /// AIの攻撃詳細
        /// </summary>
        [System.Serializable]
        public class AttackDetails
        {
            /// <summary>
            /// 攻撃割合
            /// </summary>
            [SerializeField]
            private float attack_raito_ = 0.0f;
            public float GetAttackRaito() { return attack_raito_; }

            /// <summary>
            /// 攻撃モーション
            /// </summary>
            [SerializeField]
            private MotionParameterConvert.MotionActionName attack_action_ = MotionParameterConvert.MotionActionName.Attack_Combo_A;
            public MotionParameterConvert.MotionActionName GetAttackAction() { return attack_action_; }

            /// <summary>
            /// 目標までの距離
            /// </summary>
            [SerializeField]
            private float target_length_ = 0.0f;
            public float GetTargetLength() { return target_length_; }
        }

        /// <summary>
        /// アタック詳細リスト
        /// </summary>
        [SerializeField]
        private List<AttackDetails> list_attack_details_ = new List<AttackDetails>();
        public List<AttackDetails> GetListAttackDetails() { return list_attack_details_; }

    }
}