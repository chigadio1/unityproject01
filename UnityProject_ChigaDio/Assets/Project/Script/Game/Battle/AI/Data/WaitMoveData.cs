﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    /// <summary>
    /// AIの待ち時間移動
    /// </summary>
    [System.Serializable]
    public class WaitMoveData
    {
        /// <summary>
        /// 移動タイプ
        /// </summary>
        public enum MoveType
        {
            StraightLine, //直線
            Circle, //円
        }

        /// <summary>
        /// 移動タイプ
        /// </summary>
        [SerializeField]
        private MoveType move_type_ = MoveType.StraightLine;
        public MoveType GetMoveType() { return move_type_; }

        [System.Serializable]
        public class MaxMinxFloat
        {
            [SerializeField]
            private float max_ = 0.0f;
            public float GetMax() { return max_; }

            [SerializeField]
            private float min_ = 0.0f;
            public float GetMin() { return min_; }

            public float MinMaxRandom() { return Random.Range(min_, max_); }
        }

        /// <summary>
        /// ターゲットとの距離(Min.Max)
        /// </summary>
        [SerializeField]
        private MaxMinxFloat target_length_range_ = new MaxMinxFloat();
        public MaxMinxFloat GetTargetLengthRange() { return target_length_range_; }

        /// <summary>
        /// 移動終了したときの待ち時間
        /// </summary>
        [SerializeField]
        private MaxMinxFloat move_stop_time_range_ = new MaxMinxFloat();
        public MaxMinxFloat GetMoveStopTimeRange() { return move_stop_time_range_; }

        /// <summary>
        /// 攻撃のうつる時間
        /// </summary>
        [SerializeField]
        private MaxMinxFloat attack_state_time_range_ = new MaxMinxFloat();
        public MaxMinxFloat GetAttackStateTimeRange() { return attack_state_time_range_; }

    }
}