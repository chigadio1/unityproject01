﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    /// <summary>
    /// AIのデータ
    /// </summary>
    [CreateAssetMenu(menuName = "ScriptableObjects/CreateAI")]
    public class AIDataScriptable : ScriptableObject
    {

        /// <summary>
        /// 攻撃データ
        /// </summary>
        [SerializeField]
        private AttackData attack_data_ = new AttackData();
        public AttackData GetAttackData() { return attack_data_; }

        /// <summary>
        /// 移動データ
        /// </summary>
        [SerializeField]
        private WaitMoveData move_data_ = new WaitMoveData();
        public WaitMoveData GetWaitMoveData() { return move_data_; }

    }
}