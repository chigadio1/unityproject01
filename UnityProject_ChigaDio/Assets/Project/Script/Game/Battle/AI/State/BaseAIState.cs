﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AI
{
    /// <summary>
    /// 基礎AIState
    /// </summary>
    public class BaseAIState
    {
        public enum State
        {
            Wait,
            Attack,
            Max
        }

        /// <summary>
        /// 終了フラグ
        /// </summary>
        protected bool is_end_ = false;
        public bool GetIsEnd() { return is_end_; }

        /// <summary>
        /// 初期
        /// </summary>
        public virtual void Init(BattleAIBrain ai) { }

        /// <summary>
        /// 更新
        /// </summary>
        public virtual void Update(BattleAIBrain ai) { }

        /// <summary>
        /// 終了
        /// </summary>
        public virtual void Uninit(BattleAIBrain ai) { }
    }
}