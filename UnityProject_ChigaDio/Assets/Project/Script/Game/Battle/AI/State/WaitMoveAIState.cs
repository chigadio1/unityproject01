﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    /// <summary>
    /// 待ち時間中のAI
    /// </summary>
    public class WaitMoveAIState : BaseAIState
    {
        /// <summary>
        /// 移動後の待機時間
        /// </summary>
        private float wait_time_ = 0.0f;
        private float wait_add_time_ = 0.0f;
        /// <summary>
        /// 攻撃に移行する待機時間
        /// </summary>
        private float wait_next_attack_time_ = 0.0f;
        private float wait_add_next_attack_time_ = 0.0f;
        /// <summary>
        /// 目標との距離
        /// </summary>
        private float target_length_ = 0.0f;

        /// <summary>
        /// 移動方法
        /// </summary>
        private WaitMoveData.MoveType move_type_ = WaitMoveData.MoveType.StraightLine;

        /// <summary>
        /// 目標地点
        /// </summary>
        private Vector3 goal_position_ = Vector3.zero;

        /// <summary>
        /// 移動停止フラグ
        /// </summary>
        private bool is_move_stop_ = false;

        /// <summary>
        /// 初期
        /// </summary>
        public override void Init(BattleAIBrain ai)
        {
            var data = ai.GetAIData();
            if (ai == null)
            {
                is_end_ = true;
                return;
            }
            wait_next_attack_time_ = data.GetWaitMoveData().GetAttackStateTimeRange().MinMaxRandom();
            MoveSetUp(ai);
        }

        private void MoveSetUp(BattleAIBrain ai)
        {
            is_move_stop_ = false;
            wait_add_time_ = 0.0f;
            var unit = ai.GetBattleUnit();
            unit.GetFlagBattleManager().OnMoveFlag();

            var data = ai.GetAIData();
            if (ai == null)
            {
                is_end_ = true;
                return;
            }

            wait_time_ = data.GetWaitMoveData().GetMoveStopTimeRange().MinMaxRandom();
          
            target_length_ = data.GetWaitMoveData().GetTargetLengthRange().MinMaxRandom();
            move_type_ = data.GetWaitMoveData().GetMoveType();

            goal_position_ = GetGoalPosition(ai, unit);
        }

        private Vector3 GetGoalPosition(BattleAIBrain ai,BattleUnit unit)
        {
            Vector3 move_goal_position = unit.gameObject.transform.position;

            var target_unit = unit.GetTarget();
            if (target_unit == null) return move_goal_position; 
            switch (move_type_)
            {
                case WaitMoveData.MoveType.StraightLine:

                    var vector = (target_unit.transform.position - move_goal_position).normalized;
                    vector.y = 0.0f;

                    move_goal_position = move_goal_position + (vector * target_length_);

                    break;
                case WaitMoveData.MoveType.Circle:

                    Vector3 vector_random = Vector3.zero;
                    vector_random.x = Random.Range(-1.0f, 1.0f);
                    vector_random.z = Random.Range(-1.0f, 1.0f);

                    move_goal_position = target_unit.transform.position + (vector_random * target_length_);

                    break;
            }

            return move_goal_position;


        }

        /// <summary>
        /// 更新
        /// </summary>
        public override void Update(BattleAIBrain ai)
        {
            wait_add_next_attack_time_ += Time.deltaTime;

            if(wait_add_next_attack_time_ >= wait_next_attack_time_)
            {
                is_end_ = true;
                return;
            }

            MoveUpdate(ai);

        }

        private void MoveUpdate(BattleAIBrain ai)
        {
            var unit = ai.GetBattleUnit();
            if(unit == null)
            {
                is_end_ = true;
                return;
            }

            if(unit.GetFlagBattleManager().GetKnockBackFlag())
            {
                is_move_stop_ = true;
            }


            if (is_move_stop_)
            {
                wait_add_time_ = Time.deltaTime;
                if (wait_add_time_ >= wait_time_)
                {
                    MoveSetUp(ai);
                    return;
                }
            }
            else if ((unit.transform.position - goal_position_).magnitude <= 0.1)
            {
                unit.ChangeAnimation(AnimationConvert.AnimationState.Anim_Idle);
                is_move_stop_ = true;
            }


            MoveAction(unit);

        }

        private void MoveAction(BattleUnit unit)
        {
            if (is_move_stop_) return;
            Vector3 vector = Vector3.zero;
            vector = (goal_position_ - unit.transform.position).normalized;
        
            unit.transform.position = unit.transform.position + (vector * BattleDefinition.GetAIMoveSpeed());

            if(unit.GetTarget() != null)unit.gameObject.HomingRotation(unit.GetTarget()?.gameObject,BattleDefinition.GetAIRotationSpeed());
            if (BattleCore.Instance.BattlAreaCollisionCorrection(unit.gameObject))
            {
                unit.ChangeAnimation(AnimationConvert.AnimationState.Anim_Idle);
                is_move_stop_ = true;
                return;
            }

            unit.ChangeAnimation(AnimationConvert.AnimationState.Anim_Run);
        }

        /// <summary>
        /// 終了
        /// </summary>
        public override void Uninit(BattleAIBrain ai)
        {
            var unit = ai.GetBattleUnit();
            unit.GetFlagBattleManager().OffMoveFlag();
        }
    }
}