﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI
{
    /// <summary>
    /// 攻撃中のAI
    /// </summary>
    public class AttackAIState : BaseAIState
    {
        /// <summary>
        /// 実行アクション
        /// </summary>
        private MotionParameterConvert.MotionActionName action_name_ = MotionParameterConvert.MotionActionName.MAX_Action;

        /// <summary>
        /// ターゲットとの距離
        /// </summary>
        private float target_length_ = 0.0f;

        /// <summary>
        /// 強制時間
        /// </summary>
        private float forced_add_time_ = 0.0f;


        /// <summary>
        /// 初期
        /// </summary>
        public override void Init(BattleAIBrain ai)
        {
            var data = ai.GetAIData();
            var unit = ai.GetBattleUnit();

            if (data == null || unit == null)
            {
                is_end_ = true;
                return;
            }

           

            var action = GetActionName(data);
            if(action.Item1 == MotionParameterConvert.MotionActionName.MAX_Action)
            {
                is_end_ = true;
                return;
            }

            action_name_ = action.Item1;
            target_length_ = action.Item2;

        


        }

        private (MotionParameterConvert.MotionActionName , float) GetActionName(AIDataScriptable data)
        {
            var attack_data = data.GetAttackData();
            var list_data   = attack_data.GetListAttackDetails();

            var array = list_data.ToArray().OrderBy(target => -target.GetAttackRaito()).ToArray();


            for(int count = 0; count < array.Length; count++)
            { 

                if(Random.Range(0.0f, 100.0f) <= array[count].GetAttackRaito()) return (array[count].GetAttackAction(), array[count].GetTargetLength()); 
                else
                {
                    if (count + 1 == array.Length) return (array[0].GetAttackAction(), array[0].GetTargetLength());
                }
            }

            return (MotionParameterConvert.MotionActionName.MAX_Action, 0.0f);
        }

        /// <summary>
        /// 更新
        /// </summary>
        public override void Update(BattleAIBrain ai)
        {
            if(ai.GetBattleUnit().GetFlagBattleManager().GetCharaAction())
            {
                if(ai.GetBattleUnit().GetIsMotionParameterPlay() == false)
                {
                    ai.GetBattleUnit().GetFlagBattleManager().OffCharaAction();
                    is_end_ = true;
                    return;
                }
            }
            MoveUpdate(ai.GetBattleUnit());
        }

        private void MoveUpdate(BattleUnit unit)
        {
            if (unit.GetFlagBattleManager().GetCharaAction()) return;
            Vector3 vector = Vector3.zero;
            vector = (unit.GetTarget().transform.position - unit.transform.position).normalized;

            unit.transform.position = unit.transform.position + (vector * BattleDefinition.GetAIMoveSpeed());

            if (unit.GetTarget() != null) unit.gameObject.HomingRotation(unit.GetTarget()?.gameObject, BattleDefinition.GetAIRotationSpeed());
            if (BattleCore.Instance.BattlAreaCollisionCorrection(unit.gameObject) || (unit.GetTarget().transform.position - unit.transform.position).magnitude <= target_length_)
            {
                unit.GetFlagBattleManager().OnCharaAction();
                unit.ChangeAnimation(AnimationConvert.AnimationState.Anim_Idle);
                unit.PlayMotionAction(action_name_);
                return;
            }


            forced_add_time_ += Time.deltaTime;
            if (forced_add_time_ >= 9999)
            {
                unit.GetFlagBattleManager().OnCharaAction();
                unit.PlayMotionAction(action_name_);
                return;
            }

            unit.ChangeAnimation(AnimationConvert.AnimationState.Anim_Run);
        }



        /// <summary>
        /// 終了
        /// </summary>
        public override void Uninit(BattleAIBrain ai)
        {
            var unit = ai.GetBattleUnit();

            if (unit == null)
            {
                is_end_ = true;
                return;
            }
            unit.GetFlagBattleManager().OffCharaAction();
        }
    }
}