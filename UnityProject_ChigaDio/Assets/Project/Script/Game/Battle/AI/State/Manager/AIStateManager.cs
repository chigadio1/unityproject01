﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    /// <summary>
    /// AIステートの制御
    /// </summary>
    public class AIStateManager
    {
        /// <summary>
        /// AIステート
        /// </summary>
        private BaseAIState ai_state_ = null;

        /// <summary>
        /// AIステートタイプ
        /// </summary>
        private BaseAIState.State ai_state_type_ = BaseAIState.State.Wait;

        /// <summary>
        /// 初期
        /// </summary>
        public void Init(BattleAIBrain brain)
        {
            ai_state_ = new WaitMoveAIState();
            ai_state_.Init(brain);
            ai_state_type_ = BaseAIState.State.Wait;
        }

        public void Update(BattleAIBrain brain)
        {
            if (ai_state_ == null) return;
            if (ai_state_.GetIsEnd()) NextState(brain);
            ai_state_.Update(brain);
        }

        private void NextState(BattleAIBrain brain)
        {
            ai_state_type_++;

            if (ai_state_type_ == BaseAIState.State.Max) ai_state_type_ = BaseAIState.State.Wait;
            if (ai_state_ == null) return;
            ai_state_.Uninit(brain);
            ai_state_ = CreateAIState();
            ai_state_.Init(brain);
        }

        private BaseAIState CreateAIState()
        {
            switch (ai_state_type_)
            {
                case BaseAIState.State.Wait:
                    return new WaitMoveAIState();
                case BaseAIState.State.Attack:
                    return new AttackAIState();
                case BaseAIState.State.Max:
                    break;
            }
            return null;
        }
    }
}