﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    /// <summary>
    /// バトルのAI行動を決めるクラス
    /// </summary>
    public class BattleAIBrain
    {
        /// <summary>
        /// AIステート管理
        /// </summary>
        private AIStateManager ai_state_manager_ = new AIStateManager();

        /// <summary>
        /// バトルユニット
        /// </summary>
        private BattleUnit unit_ = null;
        public BattleUnit GetBattleUnit() { return unit_; }

        /// <summary>
        /// AIデータ
        /// </summary>
        private AddressableData<AIDataScriptable> addressable_ai_ = null;
        public AIDataScriptable GetAIData() { return addressable_ai_.GetAddressableData(); }

        /// <summary>
        /// セットアップフラグ
        /// </summary>
        private bool is_setup_ = true;
        public bool GetIsSetUp() { return is_setup_; }

        public void Init(BattleUnit　unit)
        {
            unit_ = unit;
           

            addressable_ai_ = new AddressableData<AIDataScriptable>();
            addressable_ai_.LoadStart($"Assets/Project/Assets/CharacterStatus/{unit_.GetUnitID():0000}/AI/{unit_.GetUnitID():0000}.asset");
            is_setup_ = false;
        }

        public void Update()
        {
            if (unit_ == null) return;
            if (SetUp() == false) return;
                 
            ai_state_manager_.Update(this);
        }

        private bool SetUp()
        {
            if (is_setup_ == true) return true;

            if(addressable_ai_.GetFlagSetUpLoading())
            {
                is_setup_ = true;
                ai_state_manager_.Init(this);
                return true;
            }

            return false;


        }

        public void Release()
        {
            if (is_setup_ == false) return;
            addressable_ai_.Release();
            addressable_ai_ = null;
        }


    }

}