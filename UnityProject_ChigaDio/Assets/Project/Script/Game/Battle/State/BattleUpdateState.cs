﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUpdateState : BaseBattleState
{
    protected override void UpdateDetails()
    {
        BattleCheck();
    }

    /// <summary>
    /// バトル終了判定
    /// </summary>
    private void BattleCheck()
    {
        var member_control = BattleCore.Instance.GetBattleMemberControl();
        if (member_control == null) is_end_ = true;

        var list_player = member_control.GetPlayerMemberUnit();
        var list_enemy =  member_control.GetEnemyMemberUnit();

        if (list_player == null) is_end_ = true;
        if (list_enemy == null) is_end_ = true;

        bool enemy_loser = true;
        foreach(var data in list_enemy)
        {
            if (data.GetIsDeath())
            {
                //モーションが続いていたら生きている
                if (data.GetIsAnimationPlay() == false || data.GetIsMotionParameterPlay() == true) enemy_loser = false;
            }
            else enemy_loser = false;
        }

        if(enemy_loser == true)
        {
            is_end_ = true;
            BattleCore.Instance.SetMathResult(BattleSystem.MatchResult.Player);
            return;
        }

        bool player_loser = true;
        foreach (var data in list_player)
        {
            if (data.GetIsDeath())
            {
                //モーションが続いていたら生きている
                if (data.GetIsAnimationPlay() == false || data.GetIsMotionParameterPlay() == true) player_loser = false;
            }
            else player_loser = false;
        }

        if (player_loser == true)
        {
            is_end_ = true;
            BattleCore.Instance.SetMathResult(BattleSystem.MatchResult.Enemy);
            return;
        }
    }
}
