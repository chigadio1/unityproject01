﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUninitState : BaseBattleState
{
    /// <summary>
    /// エフェクト
    /// </summary>
    EffectAction[] effect_actions_ = null;

    public override void Start()
    {
        List<BattleUnit> list_game_object_ = new List<BattleUnit>();
        int effect_count = 0;
        if(BattleCore.Instance.GetMathResult() == BattleSystem.MatchResult.Player)
        {
            effect_count =  BattleCore.Instance.GetBattleMemberControl().GetEnemyMemberUnit().Count;
            list_game_object_ = BattleCore.Instance.GetBattleMemberControl().GetEnemyMemberUnit();
        }
        else
        {
            effect_count = BattleCore.Instance.GetBattleMemberControl().GetPlayerMemberUnit().Count;
            list_game_object_ = BattleCore.Instance.GetBattleMemberControl().GetPlayerMemberUnit();
        }

        effect_actions_ = new EffectAction[effect_count];


        for (int count = 0; count < effect_actions_.Length; count++)
        {
            BattleUnit unit = list_game_object_[count];
            if (unit == null) continue;

            effect_actions_[count] = EffectCreate.Appearance(BattleDefinition.GetEffectSmokeID(),unit.gameObject.transform.position, Quaternion.identity, Vector3.one, 180.0f, GameEffectManager.EffectRealmType.Dungeon);
        }


        
    }
    protected override void UpdateDetails()
    {
        bool end_effect = true;
        foreach(var data in effect_actions_)
        {
            if(data == null)
            {
                end_effect = false;
                return;
            }
        }

        if (end_effect == false) return;
        is_end_ = true;

        BattleCore.Instance.BattleEnd();
    }
}
