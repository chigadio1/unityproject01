﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトルステート管理
/// </summary>
public class BattleStateManager
{
    /// <summary>
    /// バトルステート
    /// </summary>
    private BaseBattleState battle_state_ = new BaseBattleState();

    /// <summary>
    /// 現在のバトルステート
    /// </summary>
    private BaseBattleState.BattleStateType battle_state_type_ = BaseBattleState.BattleStateType.Uninit;

    /// <summary>
    /// 再生中か
    /// </summary>
    private bool is_play_state_ = false;
    public bool GetIsPlayState() { return is_play_state_; }


    public void StartBattle()
    {
        battle_state_type_ = BaseBattleState.BattleStateType.Begin;
        battle_state_ = GetCreateBattleState();
        battle_state_.Start();
        is_play_state_ = true;
    }

    public void EndBattle()
    {
        battle_state_type_ = BaseBattleState.BattleStateType.Uninit;
        battle_state_.End();
        battle_state_ = GetCreateBattleState();
        battle_state_.Start();
        is_play_state_ = false;
    }

    public void Update()
    {
        if (is_play_state_ == false) return;
        StateUpdate();
    }



    public void LateUpdate()
    {
        if (is_play_state_ == false) return;
        StateLateUpdate();
    }

    private void StateUpdate()
    {
        battle_state_.Update();
    }

    private void StateLateUpdate()
    {
        battle_state_.LateUpdate();

        if (battle_state_.GetIsEnd() == false) return;
        if (battle_state_type_ == BaseBattleState.BattleStateType.Uninit)
        {
            battle_state_.End();
            is_play_state_ = false;
            return;
        }

        battle_state_type_++;

        battle_state_.End();
        battle_state_ = GetCreateBattleState();
        battle_state_.Start();
    }

    private BaseBattleState GetCreateBattleState()
    {
        switch (battle_state_type_)
        {
            case BaseBattleState.BattleStateType.Begin:
                return new BattleBeginState();
            case BaseBattleState.BattleStateType.Init:
                return new BattleInitState();
            case BaseBattleState.BattleStateType.Update:
                return new BattleUpdateState();
            case BaseBattleState.BattleStateType.Uninit:
                return new BattleUninitState();
        }

        return null;

    }
}
