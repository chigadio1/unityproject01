﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleBeginState : BaseBattleState
{
    protected override void UpdateDetails()
    {
        is_end_ = true;
    }
}
