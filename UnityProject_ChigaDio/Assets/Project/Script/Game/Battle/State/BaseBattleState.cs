﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎バトルステート
/// </summary>
public class BaseBattleState : BaseState
{
    public enum BattleStateType
    {
        Begin,
        Init,
        Update,
        Uninit
    }
}
