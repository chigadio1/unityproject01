﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 定義
/// </summary>
public class BattleDefinition
{
    /// <summary>
    /// プレイヤー側最大人数(Battle)
    /// </summary>
    private static int max_battle_unit_player_ = 4;
    public static int GetMaxBattleUnitPlayer() { return max_battle_unit_player_; }

    /// <summary>
    /// エネミー側最大人数(Battle)
    /// </summary>
    private static int max_battle_unit_enemy_ = 6;
    public static int GetMaxBattleUnitEnemy() { return max_battle_unit_enemy_; }

    /// <summary>
    /// バトルエスケープ脱出時間
    /// </summary>
    private static float max_battle_area_escape_time_ = 10.0f;
    public static float GetMaxBattleAreaEscapeTime() { return max_battle_area_escape_time_; }

    /// <summary>
    /// ノックバックサイズS
    /// </summary>
    private static float knock_back_s_point_ = 10.0f;
    public static float KnockBackSPoint() {return knock_back_s_point_;}

    /// <summary>
    /// 煙エフェクトID
    /// </summary>
    private static int effect_smoke_id_ = 3002;
    public static int GetEffectSmokeID() { return effect_smoke_id_; }

    /// <summary>
    /// ターゲット測定時間
    /// </summary>
    private static float max_target_setup_time_ = 30.0f;
    public static float GetMaxTargetSetUpTime() { return max_target_setup_time_; }

    /// <summary>
    /// 移動係数(AI)
    /// </summary>
    private static float ai_move_speed_ = 0.1f;
    public static float GetAIMoveSpeed() { return ai_move_speed_; }

    /// <summary>
    /// 回転係数(AI)
    /// </summary>
    private static float ai_rotation_speed_ = 10.1f;
    public static float GetAIRotationSpeed() { return ai_rotation_speed_; }
}
