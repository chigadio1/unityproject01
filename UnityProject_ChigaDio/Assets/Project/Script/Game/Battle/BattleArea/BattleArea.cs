﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleArea
{
    /// <summary>
    /// バトルオブジェクト
    /// </summary>
    private GameObject battle_area_object_ = null;
    public Vector3 GetBattleAreaPosition() 
    {
        if (battle_area_object_ == null) return Vector3.zero;
        return battle_area_object_.transform.position;
    }

    /// <summary>
    /// バトルエリアの長さ
    /// </summary>
    private float battle_area_length_ = 0.0f;
    public float GetBattleAreaLength() { return battle_area_length_; }

    public void StartBattleArea(float value_battle_area_length,Vector3 value_battle_area_position)
    {
        if(battle_area_object_ != null)
        {
            GameObject.Destroy(battle_area_object_);
        }
        battle_area_length_ = value_battle_area_length;

        battle_area_object_ = new GameObject();
        battle_area_object_.name = "BattleArea";
        battle_area_object_.transform.SetParent(BattleCore.Instance.transform);
        battle_area_object_.transform.position = value_battle_area_position;
    }

    public void EndBattleArea()
    {
        battle_area_length_ = 0.0f;
        GameObject.Destroy(battle_area_object_);
    }
}
