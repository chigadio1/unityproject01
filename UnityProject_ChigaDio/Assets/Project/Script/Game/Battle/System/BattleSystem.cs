﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトルシステム
/// </summary>
public class BattleSystem
{
    /// <summary>
    /// バトル結果
    /// </summary>
    public enum MatchResult
    {
        None,
        Player,
        Enemy,
    }

    /// <summary>
    /// バトル結果
    /// </summary>
    public MatchResult math_result_ = MatchResult.None;
    public void SetMathResult(MatchResult value) { math_result_ = value; }
    public MatchResult GetMathResult() { return math_result_; }

    #region BattleArea
    /// <summary>
    /// バトルエリア
    /// </summary>
    private BattleArea battle_area_ = new BattleArea();

    public float GetBattleAreaLength() { return battle_area_.GetBattleAreaLength(); }
    public Vector3 GetBattleAreaPosition() { return battle_area_.GetBattleAreaPosition(); }
    

    /// <summary>
    /// バトルエスケープ時間
    /// </summary>
    public float battle_area_escape_time_ = 0.0f;
    public float GetBattleAreaEscapeTime() { return battle_area_escape_time_; }

    #endregion

    /// <summary>
    /// バトル中
    /// </summary>
    public bool GetIsPlayBattle() { return battle_state_manager_.GetIsPlayState(); }

    /// <summary>
    /// バトルステートマネージャー
    /// </summary>
    private BattleStateManager battle_state_manager_ = new BattleStateManager();

    public void BattleStart(float value_battle_area_length, Vector3 value_battle_area_position)
    {
        math_result_ = MatchResult.None;
        battle_area_.StartBattleArea(value_battle_area_length, value_battle_area_position);
        battle_state_manager_.StartBattle();
        battle_area_escape_time_ = 0.0f;

        
    }

    public void Update()
    {
        battle_state_manager_.Update();
    }

    public void LateUpdate()
    {
        battle_state_manager_.LateUpdate();
    }



    public void BattleEnd()
    {
        battle_area_.EndBattleArea();
        battle_state_manager_.EndBattle();
    }

    public void AddBattleEscapeTime()
    {
        battle_area_escape_time_ += Time.deltaTime;
        battle_area_escape_time_ = Mathf.Min(battle_area_escape_time_, BattleDefinition.GetMaxBattleAreaEscapeTime());
    }

    public void SubtractionBattleEscapeTime()
    {
        battle_area_escape_time_ -= Time.deltaTime;
        battle_area_escape_time_ = Mathf.Max(battle_area_escape_time_, 0.0f);
    }

    public bool GetBattleAreaEscapeFlag()
    {
        return battle_area_escape_time_ >= BattleDefinition.GetMaxBattleAreaEscapeTime();
    }
}
