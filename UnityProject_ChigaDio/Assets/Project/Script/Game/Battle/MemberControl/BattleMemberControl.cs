﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleMemberControl
{
    /// <summary>
    /// プレイヤー側メンバー
    /// </summary>
    private List<BattleUnit> player_member_unit_ = new List<BattleUnit>();

    public List<BattleUnit> GetPlayerMemberUnit() { return player_member_unit_; }

    /// <summary>
    /// エネミー側メンバー
    /// </summary>
    private List<BattleUnit> enemy_member_unit_ = new List<BattleUnit>();

    public List<BattleUnit> GetEnemyMemberUnit() { return enemy_member_unit_; }

    /// <summary>
    /// 全バトルメンバー
    /// </summary>
    private List<BattleUnit> all_battle_menmer_unit_ = new List<BattleUnit>();

    public List<BattleUnit> GetAllBattleMemberUnit()
    {
        return all_battle_menmer_unit_;
    }

    /// <summary>
    /// 更新処理
    /// </summary>
    public void Update()
    {
        AutoRelease();
    }

    private void AutoRelease()
    {
        player_member_unit_.RemoveAll(unit => unit == null);
        enemy_member_unit_.RemoveAll(unit => unit == null);
        all_battle_menmer_unit_.RemoveAll(unit => unit == null);

    }

   
    /// <summary>
    /// プレイヤー側追加
    /// </summary>
    public void AddPlayerUnit(BattleUnit unit)
    {
        player_member_unit_.Add(unit);
        all_battle_menmer_unit_.Add(unit);
    }

    /// <summary>
    /// エネミー側追加
    /// </summary>
    /// <param name="unit"></param>
    public void AddEnemyUnit(BattleUnit unit)
    {
        enemy_member_unit_.Add(unit);
        all_battle_menmer_unit_.Add(unit);
    }

    public void AllRelease()
    {
        player_member_unit_.Clear();
        enemy_member_unit_.Clear();
    }
}
