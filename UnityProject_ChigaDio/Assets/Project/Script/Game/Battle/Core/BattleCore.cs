﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトルコア
/// </summary>
public class BattleCore : Singleton<BattleCore>
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_ & UnitCore.Instance.GetIsSetUp() & BattleCollisionCore.Instance.GetIsSetUP() & BulletCore.Instance.GetIsSetUP() & SkillCore.Instance.GetIsSetUp() & EffectCore.Instance.GetIsSetUp() & SoundCore.Instance.GetIsSetUp(); }

    /// <summary>
    /// バトルメンバー管理
    /// </summary>
    private BattleMemberControl battle_member_control_ = new BattleMemberControl();
    public BattleMemberControl GetBattleMemberControl() { return battle_member_control_; }

    #region BattleSystem
    /// <summary>
    /// バトルシステム
    /// </summary>
    private BattleSystem battle_system_ = new BattleSystem();

    public bool GetIsPlayBattle() { return battle_system_.GetIsPlayBattle() & is_setup_; }
    public float GetBattleAreaLength() { return battle_system_.GetBattleAreaLength(); }
    public Vector3 GetBattleAreaPosition() { return battle_system_.GetBattleAreaPosition(); }


    public void SetMathResult(BattleSystem.MatchResult value) { battle_system_.SetMathResult(value); }
    public BattleSystem.MatchResult GetMathResult() { return battle_system_.GetMathResult(); }

    /// <summary>
    /// 戦うエネミーのリーダー情報
    /// </summary>
    private DungeonEnemyUnit dungeon_enemy_ = null;



    /// <summary>
    /// バトル開始
    /// </summary>
    /// <param name="value_battle_area_length"></param>
    /// <param name="value_battle_area_position"></param>
    public void BattleStart(DungeonEnemyUnit dungeon_enemy)
    {
        if (battle_system_.GetIsPlayBattle()) return;
        if (dungeon_enemy == null) return;

        var battle_area = DungeonCore.Instance.GetDungeonBattlePlacementData(dungeon_enemy.GetDungeonUnitEnemyID());
        
        if (battle_area == null) return;

        battle_system_.BattleStart(battle_area.GetBattleAreaLength(), new Vector3(battle_area.GetBattleAreaStartPositionX(), battle_area.GetBattleAreaStartPositionY(), battle_area.GetBattleAreaStartPositionZ()));
        BattleStartUnit(dungeon_enemy);

        EffectCore.Instance.BattleStart();
    }

    private void BattleStartUnit(DungeonEnemyUnit dungeon_enemy)
    {
        var battle_member = DungeonCore.Instance.GetDungeonBattleMemberData(dungeon_enemy.GetDungeonUnitEnemyID());
        if (battle_member == null) return;

        for(int count = 0; count < BattleDefinition.GetMaxBattleUnitEnemy(); count++)
        {
            int id = battle_member.GetPinUnitID(count);
            if (id == 0) continue;
            UnitCore.Instance.CreateUnit(id);
        }
        dungeon_enemy_ = dungeon_enemy;
        is_setup_ = false;
    }

    /// <summary>
    ///  バトル終了
    /// </summary>
    public void BattleEnd() 
    {
        battle_system_.BattleEnd();


        var battle_member = DungeonCore.Instance.GetDungeonBattleMemberData(dungeon_enemy_.GetDungeonUnitEnemyID());
        if (battle_member == null) return;


        var all_battle_member = DungeonCore.Instance.GetDungeonAllBattleMemberData();
        if (all_battle_member == null) return;

        bool[] release_flag = new bool[BattleDefinition.GetMaxBattleUnitEnemy() - 1];

        foreach(var parent_data in all_battle_member)
        {
            for(int count = 1; count < BattleDefinition.GetMaxBattleUnitEnemy(); count++)
            {
                if (release_flag[count - 1] == false) continue;
                if (battle_member.GetPinUnitID(count) == parent_data.Value.GetPin01UnitID()) release_flag[count - 1] = false;
            }
        }

        for (int count = 1; count < BattleDefinition.GetMaxBattleUnitEnemy(); count++)
        {
            if (release_flag[count - 1] == true) UnitCore.Instance.ReleaseUnit(battle_member.GetPinUnitID(count));
        }

        var player_list = battle_member_control_.GetPlayerMemberUnit();
        for (int count = 0; count < battle_member_control_.GetPlayerMemberUnit().Count; count++)
        {

            var unit = player_list[count].gameObject.GetComponent<DungeonUnit>();
            if (unit != null) unit.enabled = true;
            Destroy(player_list[count]);
        }

        var enemy_list = battle_member_control_.GetEnemyMemberUnit();
        int enemy_count = 0;
        for (int count = 0; count < battle_member_control_.GetEnemyMemberUnit().Count; count++)
        {
            GameObject.Destroy(enemy_list[count]);
            var unit = enemy_list[count].gameObject.GetComponent<DungeonEnemyUnit>();
            if (unit != null) unit.enabled = true; 
            if (enemy_count > 0) Destroy(enemy_list[count].gameObject);
            enemy_count++;
            
        }



        BulletCore.Instance.AllRelease();
        EffectCore.Instance.BattleEffectRelease();


        dungeon_enemy_ = null;

    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "BattleCore";
    }

    // Update is called once per frame
    void Update()
    {

        SetUp();

        if (is_setup_ == false) return;

        battle_member_control_.Update();
        battle_system_.Update();
    }

    private void LateUpdate()
    {
        if (is_setup_ == false) return;
        battle_system_.LateUpdate();
    }

    /// <summary>
    /// プレイヤー側追加
    /// </summary>
    public void AddPlayerUnit(BattleUnit unit)
    {
        battle_member_control_.AddPlayerUnit(unit);
    }

    /// <summary>
    /// エネミー側追加
    /// </summary>
    /// <param name="unit"></param>
    public void AddEnemyUnit(BattleUnit unit)
    {
        battle_member_control_.AddEnemyUnit(unit);
    }

    /// <summary>
    /// クリエイトUnit
    /// </summary>
    /// <param name="create_unit_id"></param>
    /// <returns></returns>
    private BattleUnit CreateBattleUnit(int create_unit_id)
    {
        var unit_object = UnitCore.Instance.GetUnitGameObject(create_unit_id);
        if (unit_object == null) return null;

        var instance_object = GameObject.Instantiate(unit_object, Vector3.zero, Quaternion.identity);

        var unit_parent_object = new GameObject();
        unit_parent_object.name = $"Unit_" + $"{create_unit_id:0000}";

        instance_object.transform.SetParent(unit_parent_object.transform);
        var battle_unit = instance_object.AddComponent<BattleUnit>();
        battle_unit.InitUnit(create_unit_id);

        unit_parent_object.transform.SetParent(transform);

        BattleCollisionCore.Instance.LoadUnitCollision(create_unit_id);
        DataCore.Instance.LoadCharacterStatusData(create_unit_id);

        return battle_unit;
    }

    /// <summary>
    /// クリエイトUnit(Player)
    /// </summary>
    /// <param name="create_unit_id"></param>
    /// <returns></returns>
    public void CreatePlayerBattleUnit(int create_unit_id)
    {
        var unit = CreateBattleUnit(create_unit_id);
        if (unit == null) return;



        AddPlayerUnit(unit);
    }

    /// <summary>
    /// クリエイトUnit(Enemy)
    /// </summary>
    /// <param name="create_unit_id"></param>
    /// <returns></returns>
    public void CreateEnemyBattleUnit(int create_unit_id)
    {
        var unit = CreateBattleUnit(create_unit_id);
        if (unit == null) return;

        AddEnemyUnit(unit);
    }

    void SetUp()
    {
        is_setup_ = UnitCore.Instance.GetIsSetUp() & is_setup_;
        if (is_setup_ == true) return;
        if (SetUpBattle() == false) return;
        foreach (var unit in battle_member_control_.GetAllBattleMemberUnit())
        {
            if(unit.GetIsSetUp() == false)
            {

                is_setup_ = false;
                return;
            }
            foreach(var effect_id in unit.GetEffectIDList())
            {
                EffectCore.Instance.LoadEffect(GameEffectManager.EffectRealmType.Battle,effect_id);
            }
            foreach(var bullet_id in unit.GetBulletIDList())
            {
                BulletCore.Instance.LoadBulletData(bullet_id);

                BattleCollisionCore.Instance.LoadBulletCollision(bullet_id);
         
            }


        }

        is_setup_ = true;
    }

    private bool SetUpBattle()
    {
        return SetUpBattleEnemy() & SetUpBattlePlayer();
    }

    private bool SetUpBattleEnemy()
    {
        if (battle_member_control_.GetEnemyMemberUnit().Count > 0) return true;
        if (dungeon_enemy_ == null) return true;

        var battle_member = DungeonCore.Instance.GetDungeonBattleMemberData(dungeon_enemy_.GetDungeonUnitEnemyID());
        if (battle_member == null) return true;

        dungeon_enemy_.enabled = false;
        var enemy_battle = dungeon_enemy_.gameObject.AddComponent<BattleUnit>();
        if (enemy_battle == null) return false;

        var battle_area = DungeonCore.Instance.GetDungeonBattlePlacementData(dungeon_enemy_.GetDungeonUnitEnemyID());
        if (battle_area == null) return false;

        enemy_battle.transform.position = GetBattleAreaPosition() + battle_area.GetPinNoPosition(0);
        enemy_battle.transform.rotation = Quaternion.Euler(0.0f, battle_area.GetPinNoRotation(0), 0.0f);
        enemy_battle.transform.localScale = battle_area.GetPinNoScale(0);

        enemy_battle.InitUnit(battle_member.GetPin01UnitID());
        BattleCollisionCore.Instance.LoadUnitCollision(battle_member.GetPin01UnitID());
        DataCore.Instance.LoadCharacterStatusData(battle_member.GetPin01UnitID());
        enemy_battle.SetUnitType(BattleUnit.UnitType.Enemy);
        battle_member_control_.AddEnemyUnit(enemy_battle);

        for(int count = 1; count < BattleDefinition.GetMaxBattleUnitEnemy(); count++)
        {
            if (battle_member.GetPinUnitID(count) == 0) continue;
            var unit = CreateBattleUnit(battle_member.GetPinUnitID(count));
            if (unit == null) continue;
            battle_member_control_.AddEnemyUnit(unit);
            unit.InitUnit(battle_member.GetPinUnitID(count));
            BattleCollisionCore.Instance.LoadUnitCollision(battle_member.GetPinUnitID(count));
            DataCore.Instance.LoadCharacterStatusData(battle_member.GetPinUnitID(count));
            unit.transform.position = GetBattleAreaPosition() + battle_area.GetPinNoPosition(count);
            unit.transform.localScale = battle_area.GetPinNoScale(count);
            unit.transform.rotation = Quaternion.Euler(0.0f, battle_area.GetPinNoRotation(count), 0.0f);
            unit.SetUnitType(BattleUnit.UnitType.Enemy);
        }

        return true;
    }
    private bool SetUpBattlePlayer()
    {
        if (battle_member_control_.GetPlayerMemberUnit().Count > 0) return true;
        if (battle_member_control_.GetEnemyMemberUnit().Count == 0) return true;
        if (dungeon_enemy_ == null) return true;

        var main_player = GameCore.Instance.GetMainPlayerUnit();
        if (main_player == null) return false;

        main_player.enabled = false;
        var main_player_battle =  main_player.gameObject.AddComponent<BattleUnit>();
        BattleCollisionCore.Instance.LoadUnitCollision(main_player.GetUnitID());
        DataCore.Instance.LoadCharacterStatusData(main_player.GetUnitID());
        main_player_battle.SetUnitType(BattleUnit.UnitType.Player);
        battle_member_control_.AddPlayerUnit(main_player_battle);

        return true;
    }

    /// <summary>
    /// バトルエリア内にUnitを収める関数
    /// </summary>
    /// <param name="gameObject"></param>
    public bool BattlAreaCollisionCorrection(GameObject gameObject,bool is_player = false)
    {
        bool flag = RpgCollisionCalculation.BattlAreaCollisionCorrection(gameObject);

        if (is_player)
        {
            if (battle_system_.GetBattleAreaEscapeFlag())
            {
                BattleEnd();
                return flag;
            }
            else
            {
                if (flag)
                {
                    battle_system_.AddBattleEscapeTime();
                }
                else
                {
                    battle_system_.SubtractionBattleEscapeTime();
                }
            }

#if UNITY_EDITOR
            Debug.Log($"Battle_Escape_Time->{battle_system_.GetBattleAreaEscapeTime()}");
#endif

        }
        return flag;
    }
}
