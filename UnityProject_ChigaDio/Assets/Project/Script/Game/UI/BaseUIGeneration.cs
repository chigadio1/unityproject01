﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// UI生成物
/// </summary>
/// <typeparam name="T"></typeparam>
public class BaseUIGeneration<T> where T : BaseUIAuxiliary
{

    /// <summary>
    /// アドレサブルオブジェクト
    /// </summary>
    private AddressableData<GameObject> addressable_object_ = null;

    /// <summary>
    /// UI補助スクリプト
    /// </summary>
    private T ui_auxiliary_ = null;

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;
    private bool GetIsSetUp() { return is_setup_; }

    public void LoadStart(string path)
    {
        addressable_object_ = new AddressableData<GameObject>();
        addressable_object_.LoadStart(path);
        is_setup_ = false;

    }

    public void Update()
    {
        if (is_setup_ == true) return;

        is_setup_ = addressable_object_.GetFlagSetUpLoading();
    }

    public T GetUIAuxiliary()
    {
        if (ui_auxiliary_ != null) return ui_auxiliary_;
        if (is_setup_ == false) return null;
        if (addressable_object_ == null) return null;

        var instance = GameObject.Instantiate(addressable_object_.GetAddressableData(),Vector3.zero,Quaternion.identity);
       

        addressable_object_.Release();
        addressable_object_ = null;
        ui_auxiliary_ = instance.GetComponent<T>();

        return ui_auxiliary_;
    }

}
