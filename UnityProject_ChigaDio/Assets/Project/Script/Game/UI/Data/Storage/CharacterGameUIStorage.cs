﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGameUIStorage
{
    /// <summary>
    /// 辞書キャラUI
    /// </summary>
    private Dictionary<int, CharacterGameUIData> dictionary_character_game_ui_data_ = new Dictionary<int, CharacterGameUIData>();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = true;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// 読み込み
    /// </summary>
    /// <param name="character_id"></param>
    public bool CharacterUILoad(int character_id)
    {
        if (dictionary_character_game_ui_data_.ContainsKey(character_id) == true) return true;

        CharacterGameUIData data = new CharacterGameUIData();
        data.LoadStart($"Assets/Project/Assets/UI/Texture/Character/{character_id:0000}/{character_id:0000}.png");
        dictionary_character_game_ui_data_.Add(character_id, data);
        is_setup_ = false;
        return false;
    }

    public void Update()
    {
        if (is_setup_ == true) return;

        foreach(var data in dictionary_character_game_ui_data_)
        {
            data.Value.Update();
            if (data.Value.GetIsSetUp() == false) return;
        }

        is_setup_ = true;
    }

    public Sprite GetSprite(int character_id,GameUIDefinition.CharacterFaceType type)
    {
        if (dictionary_character_game_ui_data_.ContainsKey(character_id) == false) return null;
        return dictionary_character_game_ui_data_[character_id].GetSprite((int)type);
    }

    public void Release(int character_id)
    {
        if (dictionary_character_game_ui_data_.ContainsKey(character_id) == false) return;

        dictionary_character_game_ui_data_[character_id].Release();
        dictionary_character_game_ui_data_.Remove(character_id);
    }

    public void ReleaseAll()
    {
        foreach(var data in dictionary_character_game_ui_data_)
        {
            data.Value.Release();
        }

        dictionary_character_game_ui_data_.Clear();
    }
}
