﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 背景画像UI保存
/// </summary>
public class BackGroundGameUIStorage
{
    /// <summary>
    /// 辞書キャラUI
    /// </summary>
    private Dictionary<int, BackGroundGameUIData> dictionary_background_game_ui_data_ = new Dictionary<int, BackGroundGameUIData>();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = true;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// 読み込み
    /// </summary>
    /// <param name="character_id"></param>
    public bool BackGroundUILoad(int backgounrd_id)
    {
        if (dictionary_background_game_ui_data_.ContainsKey(backgounrd_id) == true) return true;

        BackGroundGameUIData data = new BackGroundGameUIData();
        data.LoadStart($"Assets/Project/Assets/UI/Texture/BackGround/{backgounrd_id:0000}.png");
        dictionary_background_game_ui_data_.Add(backgounrd_id, data);
        is_setup_ = false;
        return false;
    }

    public void Update()
    {
        if (is_setup_ == true) return;

        foreach (var data in dictionary_background_game_ui_data_)
        {
            data.Value.Update();
            if (data.Value.GetIsSetUp() == false) return;
        }

        is_setup_ = true;
    }

    public Sprite GetSprite(int backgounrd_id)
    {
        if (dictionary_background_game_ui_data_.ContainsKey(backgounrd_id) == false) return null;
        return dictionary_background_game_ui_data_[backgounrd_id].GetSprite();
    }

    public void Release(int character_id)
    {
        if (dictionary_background_game_ui_data_.ContainsKey(character_id) == false) return;

        dictionary_background_game_ui_data_[character_id].Release();
        dictionary_background_game_ui_data_.Remove(character_id);
    }

    public void ReleaseAll()
    {
        foreach (var data in dictionary_background_game_ui_data_)
        {
            data.Value.Release();
        }

        dictionary_background_game_ui_data_.Clear();
    }
}
