﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIManagerData
{
    public bool GetIsSetUp() { return character_game_ui_storage_.GetIsSetUp() & background_game_ui_storage_.GetIsSetUp(); }

    #region Character
    /// <summary>
    /// キャラクターUIデータ
    /// </summary>
    private CharacterGameUIStorage character_game_ui_storage_ = new CharacterGameUIStorage();


    public bool LoadCharacter(int character_id)
    {
        return character_game_ui_storage_.CharacterUILoad(character_id);
    }

    public Sprite GetCharacterSprite(int character_id, GameUIDefinition.CharacterFaceType face_type)
    {
        return character_game_ui_storage_.GetSprite(character_id, face_type);
    }

    public void ReleaseCharacter(int character_id)
    {
        character_game_ui_storage_.Release(character_id);
    }

    public void ReleaseCharacterAll()
    {
        character_game_ui_storage_.ReleaseAll();
    }

    #endregion

    #region BackGround
    /// <summary>
    /// 背景UIデータ
    /// </summary>
    private BackGroundGameUIStorage background_game_ui_storage_ = new BackGroundGameUIStorage();


    public bool LoadBackGround(int character_id)
    {
        return background_game_ui_storage_.BackGroundUILoad(character_id);
    }

    public Sprite GetBackGroundSprite(int character_id)
    {
        return background_game_ui_storage_.GetSprite(character_id);
    }

    public void ReleaseBackGround(int character_id)
    {
        background_game_ui_storage_.Release(character_id);
    }

    public void ReleaseBackGroundAll()
    {
        background_game_ui_storage_.ReleaseAll();
    }

    #endregion


    public void Init()
    {

    }

    public void Update()
    {
        character_game_ui_storage_.Update();
        background_game_ui_storage_.Update();
    }

    public void ReleaseAll()
    {
        ReleaseCharacterAll();
        ReleaseBackGroundAll();
    }
}
