﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎ゲームUIデータ
/// </summary>
public class BaseGameUIData
{
    /// <summary>
    /// 画像
    /// </summary>
    protected AddressableData<Sprite> addressable_sprite_ = new AddressableData<Sprite>();

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    protected bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// 解放
    /// </summary>
    public void Release() { addressable_sprite_.Release(); }

    /// <summary>
    /// 読み込み開始
    /// </summary>
    /// <param name="load_path"></param>
    public void LoadStart(string load_path)
    {
        addressable_sprite_.LoadArrayStart(load_path) ;
        is_setup_ = false;
    }

    /// <summary>
    /// 更新
    /// </summary>
    public void Update() 
    {
        if (is_setup_ == true) return;
        if (addressable_sprite_.GetFlagSetUpLoading() == false) return;
        is_setup_ = true;
    }

    public Sprite GetSprite(int index)
    {
        if (is_setup_ == false) return null;
        return addressable_sprite_.GetArrayAddressableData(index);
    }

    public Sprite GetSprite()
    {
        if (is_setup_ == false) return null;
        return addressable_sprite_.GetAddressableData();
    }
}
