﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 基礎ノベル情報
/// </summary>
public class BaseNovelInformation<T> where T : MaskableGraphic
{
    /// <summary>
    /// 情報
    /// </summary>
    protected T information_graphic_ = null;

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    protected bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    public void SetUp(GameObject gameObject, string search_chiled_name)
    {
        if (gameObject == null) return;

        var chileds = gameObject.GetComponentsInChildren<RectTransform>();
     
        for(int count = 0; count < chileds.Length; count++)
        {
            if (chileds[count].gameObject.name == search_chiled_name)
            {
                var information = chileds[count].GetComponent<T>();
                if (information != null)
                {
                    information_graphic_ = information;
                    is_setup_ = true;
                    break;
                }
            }
        }

        Init();
    }

    public void Update()
    {
        DetailsUpdate();
    }

    protected void Init()
    {
    }

    protected virtual void DetailsUpdate()
    {
    }
}
