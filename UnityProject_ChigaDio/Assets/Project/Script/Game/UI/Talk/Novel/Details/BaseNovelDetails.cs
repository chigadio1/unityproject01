﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 基礎ノベル：詳細
/// </summary>
public class BaseNovelDetails
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    protected bool is_setup_ = false;
    public virtual bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// ノベル詳細情報を保持するオブジェクト
    /// </summary>
    protected　GameObject novel_game_object_ = null;

    public void SetUp(GameObject gameObject,string search_chiled_name,int index = 0)
    {
        if (gameObject == null) return;

        var chiled = gameObject.GetComponentsInChildren<RectTransform>();

        foreach(var chiled_obj in chiled)
        {
            if(chiled_obj.name == search_chiled_name)
            {
                novel_game_object_ = chiled_obj.gameObject;
                is_setup_ = true;
                break;
            }
        }

        DetailsSetUp(index);
    }

    public void Update()
    {
        DetailsUpdate();
    }

    protected void DetailsSetUp(int index = 0)
    {
        if (is_setup_ == false) return;
        DetailsSetUpTrue(index);
    }

    protected virtual void DetailsSetUpTrue(int index = 0)
    {

    }

    protected virtual void DetailsUpdate()
    { 
    }
}
