﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベルトーク詳細
/// </summary>
public class NovelTalkDetails : BaseNovelDetails
{
    /// <summary>
    /// セリフ
    /// </summary>
    private TextNovelInformation talk_text_ = null;

    /// <summary>
    /// セリフ発言者名
    /// </summary>
    private TextNovelInformation actor_text_ = null;

    /// <summary>
    /// トーク画像
    /// </summary>
    private ImageNovelInformation talk_image_ = null;

    protected override void DetailsSetUpTrue(int index = 0)
    {
        talk_image_ = new ImageNovelInformation();
        talk_image_.SetUp(novel_game_object_, $"TalkImage");

        actor_text_ = new TextNovelInformation();
        actor_text_.SetUp(novel_game_object_, $"NameText");

        talk_text_ = new TextNovelInformation();
        talk_text_.SetUp(novel_game_object_, $"TalkText");
    }

    protected override void DetailsUpdate()
    {
        if (talk_image_ == null) return;
        if (actor_text_ == null) return;
        if (talk_text_ == null) return;
    }

    public void SetTalkText(string value_text)
    {
        if (is_setup_ == false) return;
        if (talk_image_ == null) return;
        if (actor_text_ == null) return;
        if (talk_text_ == null) return;

        talk_text_.SetText(value_text);
    }


    public void SetActorText(string value_text)
    {
        if (is_setup_ == false) return;
        if (talk_image_ == null) return;
        if (actor_text_ == null) return;
        if (talk_text_ == null) return;

        actor_text_.SetText(value_text);
    }

}
