﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// ノベルテキスト情報
/// </summary>
public class TextNovelInformation : BaseNovelInformation<Text>
{
    public void SetText(string value_text)
    {
        if (is_setup_ == false) return;

        information_graphic_.text = value_text;
    }

    public void SetFontSize(int value_size)
    {
        if (is_setup_ == false) return;
        information_graphic_.fontSize = value_size;
    }
}
