﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NovelDetailsManager
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;

    /// <summary>
    /// ロード終了フラグ
    /// </summary>
    private bool is_loading_end_ = false;

    /// <summary>
    /// 再生フラグ
    /// </summary>
    private bool is_play_ = false;
    public bool GetIsPlay() { return is_play_; }


    /// <summary>
    /// ノベルに登場する最大キャラ
    /// </summary>
    private NovelCharaDetails[] chara_details_ = new NovelCharaDetails[GameUIDefinition.GetMaxNovelCharaCount()];
    public NovelCharaDetails GetCharaDetails(int index)
    {
        if (index < 0 || index >= GameUIDefinition.GetMaxNovelCharaCount()) return chara_details_[GameUIDefinition.GetMaxNovelCharaCount() - 1];

        return chara_details_[index];
    }

    /// <summary>
    /// 会話詳細
    /// </summary>
    private NovelTalkDetails talk_details_ = new NovelTalkDetails();
    public NovelTalkDetails GetTalkDetails() { return talk_details_; }

    /// <summary>
    /// 背景画像
    /// </summary>
    private NovelBackGroundDetails background_details_ = new NovelBackGroundDetails();
    public NovelBackGroundDetails GetBackGroundDetails() { return background_details_; }

    /// <summary>
    /// フェード画像
    /// </summary>
    private NovelFadeDetails fade_details_ = new NovelFadeDetails();
    public NovelFadeDetails GetFadeDetails() { return fade_details_; }

    /// <summary>
    /// 管理しているオブジェクト
    /// </summary>
    private GameObject parent_obj_ = null;


    /// <summary>
    /// ノベル会話データ
    /// </summary>
    private AddressableData<NovelTalkDataScriptable> novel_talk_scriptable_ = null;

    /// <summary>
    /// コマンド計算
    /// </summary>
    private CommandCalculation command_calculation_ = new CommandCalculation();

    public void SetUp(GameObject value_obj)
    {
        if (value_obj == null) return;
        parent_obj_ = value_obj;
        ObjectSetUp();

      

    }

    public void Play(int talk_id)
    {
        if (is_play_ == true) return;

        if (novel_talk_scriptable_ == null) novel_talk_scriptable_ = new AddressableData<NovelTalkDataScriptable>();
        if (novel_talk_scriptable_.GetFlagSetUpLoading() == false) novel_talk_scriptable_.Release();
        else novel_talk_scriptable_.Release();


        novel_talk_scriptable_.LoadStart($"Assets/Project/Assets/Data/Story/{talk_id:0000}/{talk_id:0000}.asset");
        is_setup_ = is_play_ = is_loading_end_ =  false;
    }


    private void ObjectSetUp()
    {

        for (int index = 0; index < chara_details_.Length; index++)
        {
            chara_details_[index] = new NovelCharaDetails();
            chara_details_[index].SetUp(parent_obj_, "CharacterScreen", index + 1);
            chara_details_[index].OffImage();
            
        }

        talk_details_.SetUp(parent_obj_, "TalkScreen");
        background_details_.SetUp(parent_obj_, "NovelScreen");
        fade_details_.SetUp(parent_obj_, "FadeScreen");

        is_setup_ = true;
       
    }

    public void Update()
    {

        LoadSetUp();
        PlaySetUp();
        PlayUpdate();
    }

    private void LoadSetUp()
    {
        if (novel_talk_scriptable_ == null) return;
        if (novel_talk_scriptable_.GetFlagSetUpLoading() == false) return;
        if (is_loading_end_ == true) return;

        if (GameUICore.Instance.GetIsSetUI() == false) return;
        if (SoundCore.Instance.GetIsSetUp() == false) return;

        command_calculation_.Play(novel_talk_scriptable_.GetAddressableData(),this);
        is_loading_end_ = true;
    }

    private void PlaySetUp()
    {
        if (is_play_ == true) return;
        if (is_loading_end_ == false) return;
        if (GameUICore.Instance.GetIsSetUI() == false) return;
        if (SoundCore.Instance.GetIsSetUp() == false) return;
        is_play_ = true;
    }

    private void PlayUpdate()
    {
        if (is_loading_end_ == false) return;
        if (is_play_ == false) return;

        command_calculation_.Update(this);
    }

    public void Release()
    {
        novel_talk_scriptable_.Release();
    }
}
