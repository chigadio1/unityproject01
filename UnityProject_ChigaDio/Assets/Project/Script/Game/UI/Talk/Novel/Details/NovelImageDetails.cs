﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベル画像詳細
/// </summary>
public class NovelImageDetails : BaseNovelDetails
{
    /// <summary>
    /// キャライメージ
    /// </summary>
    protected ImageNovelInformation novel_image_ = null;

    /// <summary>
    /// キャラ画像
    /// </summary>
    /// <returns></returns>
    public int GetImageID() { return novel_image_ != null ? novel_image_.GetImageID() : 0; }

    public void DetailsSetUp(int value_image_id)
    {
        if (novel_image_ == null) return;
        novel_image_.SetUp(value_image_id);
    }

    protected override void DetailsUpdate()
    {
        if (novel_image_ == null) return;
    }

    public void SetImage(Sprite value_image)
    {
        if (is_setup_ == false) return;
        if (novel_image_.GetIsSetUp() == false) return;
        novel_image_.SetCharaImage(value_image);
    }

    public void SetImage(Texture2D value_image)
    {
        if (is_setup_ == false) return;
        if (novel_image_.GetIsSetUp() == false) return;
        novel_image_.SetCharaImage(value_image);
    }

    public void SetAlpha(float value_alpha)
    {
        if (is_setup_ == false) return;
        if (novel_image_ == null) return;
        if (novel_image_.GetIsSetUp() == false) return;
        novel_image_.SetAlpha(value_alpha);
    }

    public void OnImage()
    {
        if (is_setup_ == false) return;
        if (novel_image_.GetIsSetUp() == false) return;
        novel_image_.OnCharacter();
    }

    public void OffImage()
    {
        if (is_setup_ == false) return;
        if (novel_image_.GetIsSetUp() == false) return;
        novel_image_.OffCharacter();
    }
}
