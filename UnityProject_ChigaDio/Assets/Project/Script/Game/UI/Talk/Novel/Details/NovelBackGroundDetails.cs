﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベル背景画像詳細
/// </summary>
public class NovelBackGroundDetails : NovelCharaDetails
{
    protected override void DetailsSetUpTrue(int index = 0)
    {
        novel_image_ = new ImageNovelInformation();
        novel_image_.SetUp(novel_game_object_, $"NovelBackGround");
    }
}
