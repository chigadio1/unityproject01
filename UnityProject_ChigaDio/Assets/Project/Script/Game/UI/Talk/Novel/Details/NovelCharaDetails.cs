﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// ノベルキャラ詳細
/// </summary>
public class NovelCharaDetails : NovelImageDetails
{
    protected override void DetailsSetUpTrue(int index = 0)
    {
        novel_image_ = new ImageNovelInformation();
        novel_image_.SetUp(novel_game_object_, $"Value{index:00}");
    }
}
