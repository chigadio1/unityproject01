﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ノベル画像情報
/// </summary>
public class ImageNovelInformation : BaseNovelInformation<Image>
{

    /// <summary>
    /// 画像イメージ
    /// </summary>
    protected int image_id_ = 0;
    public int GetImageID() { return image_id_; }

    public void SetUp(int value_id) { image_id_ = value_id; }
    
    public void SetCharaImage(Sprite value_image)
    {
        if (is_setup_ == false) return;
        if (value_image == null) return;
        if (information_graphic_.name == value_image.name) return;
        information_graphic_.sprite = value_image;
    }

    public void SetCharaImage(Texture2D value_image)
    {
        if (is_setup_ == false) return;
        if (value_image == null) return;
        if (information_graphic_.name == value_image.name) return;
        Texture2D tex = new Texture2D(value_image.width, value_image.height);
        information_graphic_.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
    }

    public void SetAlpha(float value_alpha)
    {
        if (is_setup_ == false) return;
        if (information_graphic_ == null) return;

        Color color = information_graphic_.color;

        color.a = value_alpha;

        information_graphic_.color = color;
    }

    public void OnCharacter()
    {
        if (is_setup_ == false) return;
        information_graphic_.gameObject.SetActive(true);
    }

    public void OffCharacter()
    {
        if (is_setup_ == false) return;
        information_graphic_.gameObject.SetActive(false);
    }
}
