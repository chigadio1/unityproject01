﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベル：アクションコマンドの管理クラス
/// </summary>
public class NovelCommandActionManager
{
    /// <summary>
    /// コマンドアクション
    /// </summary>
    private List<List<BaseOrijinCommandAction>> list_command_action_ = new List<List<BaseOrijinCommandAction>>();

    /// <summary>
    /// メインコマンドカウント
    /// </summary>
    private int main_command_count_ = 0;


    /// <summary>
    /// 終了フラグ
    /// </summary>
    private bool is_end_ = false;
    public bool GetIsEnd() { return is_end_; }

    private bool is_start_ = false;

    /// <summary>
    /// サブコマンドカウント
    /// </summary>
    private int sub_command_count_ = 0;
    public void CommandActionSetUp(List<CommandCalculation.CommandSplit> list_command_split,NovelDetailsManager details_manager)
    {

        int now_command_id_ = list_command_split[0].GetID();
        list_command_action_.Add(new List<BaseOrijinCommandAction>());
        int count = 1;
        foreach(var data in list_command_split)
        {
            
            if (count < now_command_id_)
            {
                list_command_action_.Add(new List<BaseOrijinCommandAction>());
                count = now_command_id_;
            }
            now_command_id_ = data.GetID();
            var action = BranchActionSetUp(data.GetCommandType(),data,details_manager);
            if (action == null) continue;
            list_command_action_[count - 1].Add(action);
        }
    }

    public void Update()
    {
        if(is_start_ == false)
        {
            list_command_action_[main_command_count_][sub_command_count_].Start();
            is_start_ = true;
        }
        if (is_end_ == true) return;
        list_command_action_[main_command_count_][sub_command_count_].ActionUpdate();

        if(list_command_action_[main_command_count_][sub_command_count_].GetIsEnd())
        {
            sub_command_count_++;
            if (sub_command_count_ >= list_command_action_[main_command_count_].Count)
            {
                sub_command_count_ = 0;
                main_command_count_++;
                if (main_command_count_ >= list_command_action_.Count)
                {
                    is_end_ = true;
                    return;
                }
               
            }
            list_command_action_[main_command_count_][sub_command_count_].Start();

        }
    }

    private BaseOrijinCommandAction BranchActionSetUp(GameUIDefinition.CommandMainNovel type, CommandCalculation.CommandSplit command, NovelDetailsManager details_manager)
    {
        if (command == null) return null;
        switch (type)
        {
            case GameUIDefinition.CommandMainNovel.BackGround:
                return BackGroundSetUp(command, details_manager);
            case GameUIDefinition.CommandMainNovel.Character:
                return CharacterSetUp(command, details_manager);
            case GameUIDefinition.CommandMainNovel.Text:
                return TextSetUp(command, details_manager);
            case GameUIDefinition.CommandMainNovel.WaitTime:
                return WaitTimeSetUp(command, details_manager);
            case GameUIDefinition.CommandMainNovel.None:
                break;
            case GameUIDefinition.CommandMainNovel.Fade:
                return FadeSetUp(command, details_manager);
        }

        return null;

    }

    private BaseOrijinCommandAction BackGroundSetUp(CommandCalculation.CommandSplit command, NovelDetailsManager details_manager)
    {
        BackGroundCommandAction action = new BackGroundCommandAction();

        action.SetUp(details_manager.GetBackGroundDetails(), command);
        return action;
        
    }

    private BaseOrijinCommandAction CharacterSetUp(CommandCalculation.CommandSplit command, NovelDetailsManager details_manager)
    {
        CharacterCommandAction action = new CharacterCommandAction();

        string character_value_name = "Value01";

        foreach(var data in command.GetListCommandDetails())
        {
            ///文字数一致、文字部分一致
            if(data.GetCommandName().IndexOf("Value") >= 0)
            {
                character_value_name = data.GetCommandName();
            }
        }

        if(character_value_name.Length >= 7)
        {
            character_value_name = character_value_name.Substring(0, 7);
        }

        string parese = character_value_name[character_value_name.Length - 1].ToString();
        var character_details = details_manager.GetCharaDetails(int.Parse(parese) - 1);
        if (character_details == null) return null;

        action.SetUp(character_details,command);
        
        return action;

    }

    private BaseOrijinCommandAction TextSetUp(CommandCalculation.CommandSplit command, NovelDetailsManager details_manager)
    {
        TextCommandAction action = new TextCommandAction();

        action.SetUp(details_manager.GetTalkDetails(), command);
       
        return action;
    }

    private BaseOrijinCommandAction WaitTimeSetUp(CommandCalculation.CommandSplit command, NovelDetailsManager details_manager)
    {
        WaitTimeCommandAction action = new WaitTimeCommandAction();

        action.SetUpOrijin(command);

        return action;
    }

    private BaseOrijinCommandAction FadeSetUp(CommandCalculation.CommandSplit command, NovelDetailsManager details_manager)
    {
        FadeCommandAction action = new FadeCommandAction();

        action.SetUp(details_manager.GetFadeDetails(),command);

        return action;
    }

}
