﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// フェードコマンド
/// </summary>
public class FadeCommandRole : BaseCommandRole
{
    /// <summary>
    /// フェードのタイプ
    /// </summary>
    private FadeType fade_type_ = FadeType.Out;

    /// <summary>
    /// フェードにかかる時間
    /// </summary>
    private float fade_time_ = 0.0f;

    /// <summary>
    /// 計算用
    /// </summary>
    private float time_ = 0.0f;

    /// <summary>
    /// 値を初期化
    /// </summary>
    public override void Init()
    {
        time_ = fade_time_ = 0.0f;
        fade_type_ = FadeType.Out;
    }

    /// <summary>
    /// 0から1におさまるフェード単位
    /// </summary>
    /// <returns></returns>
    public float FadeLerpTime()
    {
        time_ = Mathf.Min(time_, fade_time_);
        if (fade_time_ == 0.0f) return 1.0f;



        if(fade_type_ == FadeType.Out) return Mathf.Lerp(0.0f, 1.0f, time_ / fade_time_);
        else return Mathf.Lerp(1.0f, 0.0f, time_ / fade_time_);
    }

    /// <summary>
    /// 終了判定
    /// </summary>
    /// <returns></returns>
    public override bool GetIsEnd()
    {
        return time_ >= fade_time_ ? true : false;
    }

    /// <summary>
    /// フェードタイプ
    /// </summary>
    public enum FadeType
    {
        Out,
        In
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="value_type"></param>
    /// <param name="value_time">フェードにかかる時間</param>
    public void Start(FadeType value_type = FadeType.Out, float value_time = 0.0f)
    {
        fade_type_ = value_type;
        fade_time_ = value_time;
    }

    public override void CommandUpdate()
    {
        time_ += Time.deltaTime * GameUIDefinition.GetEventTalkSpeed();

        

      
    }
}
