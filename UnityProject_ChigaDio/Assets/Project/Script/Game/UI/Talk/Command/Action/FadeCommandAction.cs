﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベルコマンド：フェードアクション
/// </summary>
public class FadeCommandAction : BaseCommandAction<NovelFadeDetails>
{
    /// <summary>
    /// フェード
    /// </summary>
    private FadeCommandRole fade_role_ = new FadeCommandRole();

    /// <summary>
    /// フェードタイプ
    /// </summary>
    FadeCommandRole.FadeType fade_type_ = FadeCommandRole.FadeType.Out;

    /// <summary>
    /// フェード時間
    /// </summary>
    private float fade_time_ = 0.0f;

    public override bool GetIsEnd()
    {
        return fade_role_.GetIsEnd();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value_image"></param>
    /// <param name="value_type"></param>
    /// <param name="value_time">フェードにかかる時間</param>
    public override void Start()
    {
        if (details_ == null) return;
        fade_role_.Start(fade_type_, fade_time_);

    }

    protected override void CommnadSplitSetUp()
    {
        if (split_ == null) return;

        foreach (var data in split_.GetListCommandDetails())
        {
            string name = data.GetCommandName();
            string property = data.GetCommandProperty();
            if (name.IndexOf(GameUIDefinition.GetNovelListFadeCommand(GameUIDefinition.FadeCommandNovel.Fade_Time)) >= 0)
            {
                fade_time_ = float.Parse(property); ;
            }
            else
            {
                if (property.IndexOf("In") >= 0 || property.IndexOf("IN") >= 0) fade_type_ = FadeCommandRole.FadeType.In;
            }
        }

    }


    public override void ActionInit()
    {
        if (details_ == null) return;
        fade_role_.Init();
    }

    public override void ActionUpdate()
    {
        if (details_ == null) return;
        fade_role_.CommandUpdate();
        details_.SetAlpha(1.0f -  fade_role_.FadeLerpTime());
        details_.OnImage();
    }
}
