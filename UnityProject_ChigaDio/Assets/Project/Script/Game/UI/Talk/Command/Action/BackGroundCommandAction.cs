﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベルコマンド：背景アクション
/// </summary>
public class BackGroundCommandAction : BaseCommandAction<NovelBackGroundDetails>
{
     /// <summary>
    /// イメージ
    /// </summary>
    private ImageCommandRole image_role_ = new ImageCommandRole();

    /// <summary>
    /// 画像ID
    /// </summary>
    private int image_id_ = 0;

    /// <summary>
    /// フェードタイプ
    /// </summary>
    private FadeCommandRole.FadeType fade_type_ = FadeCommandRole.FadeType.Out;

    /// <summary>
    /// フェード時間
    /// </summary>
    private float fade_time_ = 0.0f;


    public override bool GetIsEnd()
    {
        return image_role_.GetIsEnd();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value_image"></param>
    /// <param name="value_type"></param>
    /// <param name="value_time">フェードにかかる時間</param>
    public override void Start()
    {
        if (details_ == null) return;
        image_role_.Start(GameUICore.Instance.GetBackGroundSprite(image_id_), fade_type_, fade_time_);
    }

    protected override void CommnadSplitSetUp()
    {
        if (split_ == null) return;

        foreach (var data in split_.GetListCommandDetails())
        {
            string name = data.GetCommandName();
            string property = data.GetCommandProperty();

            if (name.IndexOf(GameUIDefinition.GetNovelListBackGroundCommand(GameUIDefinition.BackGroundCommandNovel.Fade_Time)) >= 0)
            {
                fade_time_ = float.Parse(property); ;
            }
            else if (name.IndexOf(GameUIDefinition.GetNovelListBackGroundCommand(GameUIDefinition.BackGroundCommandNovel.Fade)) >= 0)
            {
                if (property.IndexOf("In") >= 0 || property.IndexOf("IN") >= 0) fade_type_ = FadeCommandRole.FadeType.In;
            }
            else
            {
                image_id_ = int.Parse(property);
            }

        }
    }

    public override void ActionInit()
    {
        if (details_ == null) return;
        image_role_.Init();
    }

    public override void ActionUpdate()
    {
        if (details_ == null) return;
        image_role_.CommandUpdate();
        details_.SetAlpha(image_role_.GetFadeLerpTime());
        details_.SetImage(image_role_.GetImage());
    }
}
