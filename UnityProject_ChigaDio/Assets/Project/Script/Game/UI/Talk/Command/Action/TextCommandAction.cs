﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベルコマンド：テキストアクション
/// </summary>
public class TextCommandAction : BaseCommandAction<NovelTalkDetails>
{
    /// <summary>
    /// テキスト
    /// </summary>
    private TextCommandRole text_role_ = new TextCommandRole();

    /// <summary>
    /// セリフ
    /// </summary>
    private string dialog_ = "";


    /// <summary>
    /// 発言者
    /// </summary>
    private string actor_name_ = "";
    public string GetText()
    {
        return text_role_.GetText();
    }
    

    public override bool GetIsEnd()
    {
        return text_role_.GetIsEnd();
    }

    /// <summary>
    /// 
    /// </summary>
    public override void Start()
    {
        if (details_ == null) return;
        text_role_.Start(dialog_);
    }

    protected override void CommnadSplitSetUp()
    {
        if (split_ == null) return;

        foreach (var data in split_.GetListCommandDetails())
        {
            string name = data.GetCommandName();
            string property = data.GetCommandProperty();

            if (name.IndexOf(GameUIDefinition.GetNovelListTextCommand(GameUIDefinition.TextCommandNovel.Actor)) >= 0)
            {
                actor_name_ = property;
            }
            else if (name.IndexOf(GameUIDefinition.GetNovelListTextCommand(GameUIDefinition.TextCommandNovel.Dialog)) >= 0)
            {
                dialog_ = property;
            }
        }
    }

    public override void ActionInit()
    {
        if (details_ == null) return;
        text_role_.Init();
    }

    public override void ActionUpdate()
    {
        if (details_ == null) return;
        text_role_.CommandUpdate();

        details_.SetActorText(actor_name_);
        details_.SetTalkText(text_role_.GetText());

    }
}
