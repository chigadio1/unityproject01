﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 基礎コマンドアクションの原点
/// </summary>
public class BaseOrijinCommandAction
{
    /// <summary>
    /// コマンド分割
    /// </summary>
    protected CommandCalculation.CommandSplit split_ = null;

    public virtual void SetUpOrijin(CommandCalculation.CommandSplit value_split)
    {
        split_ = value_split;
        CommnadSplitSetUp();
    }

    public virtual void Start() { }

    protected virtual void CommnadSplitSetUp() { }

    public virtual bool GetIsEnd()
    {
        return true;
    }
    /// <summary>
    /// コマンド反映更新
    /// </summary>
    /// <param name="details"></param>
    public virtual void ActionUpdate()
    {

    }

    /// <summary>
    /// アクションの数値を初期化
    /// </summary>
    public virtual void ActionInit()
    {

    }
}
