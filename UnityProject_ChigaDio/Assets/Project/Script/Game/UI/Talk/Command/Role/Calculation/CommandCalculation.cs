﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベルコマンド計算(実行もこちらでする)
/// </summary>
public class CommandCalculation
{
    /// <summary>
    /// コマンド分割
    /// </summary>
    public class CommandSplit
    {
        /// <summary>
        /// コマンドテキスト
        /// </summary>
        private string command_text_ = "";

        /// <summary>
        /// コマンド詳細
        /// </summary>
        public class CommandDetails
        {
            /// <summary>
            /// コマンド名
            /// </summary>
            private string command_name_ = "";
            public string GetCommandName() { return command_name_; }

            /// <summary>
            /// コマンド変数
            /// </summary>
            private string command_property_ = "";
            public string GetCommandProperty() { return command_property_; }

            public void SetUp(string value_command_name,string value_command_property)
            {
                command_name_ = value_command_name;
                command_property_ = value_command_property;
            }
        }

        /// <summary>
        /// コマンドタイプ
        /// </summary>
        private GameUIDefinition.CommandMainNovel command_main_type_ = GameUIDefinition.CommandMainNovel.None;
        public GameUIDefinition.CommandMainNovel GetCommandType() { return command_main_type_; }

        /// <summary>
        /// コマンドリスト
        /// </summary>
        private List<CommandDetails> list_command_details_ = new List<CommandDetails>();
        public List<CommandDetails> GetListCommandDetails() { return list_command_details_; }

        /// <summary>
        /// コマンドID
        /// </summary>
        private int id_ = 1;
        public int GetID() { return id_; }
        /// <summary>
        /// コマンド名を検索し取得
        /// </summary>
        /// <param name="find_name"></param>
        /// <returns></returns>
        public CommandDetails GetCommandFindName(string find_name)
        {
;
            foreach(var data in list_command_details_)
            {
                if(data.GetCommandName() == find_name)
                {
                    return data;
                }
            }

            return null;
        }

        /// <summary>
        /// コマンド名を検索し取得
        /// </summary>
        /// <param name="find_name"></param>
        /// <returns></returns>
        public CommandDetails GetCommandFindName(GameUIDefinition.CommandMainNovel find_name)
        {
            return GetCommandFindName(Enum.GetName(typeof(GameUIDefinition.CommandMainNovel), find_name));
        }

        /// <summary>
        /// 終了判定
        /// </summary>
        private bool is_end_ = false;
        public bool GetIsEnd() { return is_end_; }

        public void Start(string command_text, int value_id)
        {
            /// < が存在すればTrue
            bool is_command = false;
            for (int count = 0; count < command_text.Length; count++)
            {
                if(is_command == true)
                {
                    if(">" == command_text[count].ToString())
                    {
                        Split();
                        id_ = value_id;
                        return;
                    }
                    command_text_ += command_text[count].ToString();
                }
                if("<" == command_text[count].ToString())
                {
                    is_command = true;
                    
                }

            }
        }

        private void Split()
        {
            ///半角スペースで区切る
            List<string> split = new List<string>(command_text_.Split(' '));
            split.RemoveAll(data => data == "");
            
            for (int count = 0; count < split.Count; count+=3)
            {
                CommandDetails command = new CommandDetails();
                command.SetUp(split[count],split[count + 2]);
                list_command_details_.Add(command);
            }

            for(int count = 0; count < (int)GameUIDefinition.CommandMainNovel.None; count++)
            {
                if(list_command_details_[0].GetCommandName().IndexOf(Enum.GetName(typeof(GameUIDefinition.CommandMainNovel), count)) >= 0)
                {
                    command_main_type_ = (GameUIDefinition.CommandMainNovel)count;
                    return;
                }
                
            }

            if (list_command_details_[0].GetCommandName().IndexOf("Value") >= 0) command_main_type_ = GameUIDefinition.CommandMainNovel.Character;
        }
    }

    /// <summary>
    /// 読み込みする画像のIDと読み込み状態
    /// </summary>
    public class LoadSpriteIDData
    {
        /// <summary>
        /// 画像番号
        /// </summary>
        private int load_id_ = 0;
        public int GetLoadID() { return load_id_; }

        /// <summary>
        /// ロード終了フラグ
        /// </summary>
        public bool is_load_end_ = false;
        public bool GetIsLoadEnd() { return is_load_end_; }

        /// <summary>
        /// 画像タイプ
        /// </summary>
        public GameUIDefinition.SpriteType type_ = GameUIDefinition.SpriteType.Character;

        public void Load(GameUIDefinition.SpriteType type,int load_id)
        {
            load_id_ = load_id;
            type_ = type;
            switch (type)
            {
                case GameUIDefinition.SpriteType.Character:
                    is_load_end_ = GameUICore.Instance.LoadCharacter(load_id_);
                    break;
                case GameUIDefinition.SpriteType.BackGround:
                    is_load_end_ = GameUICore.Instance.LoadBackGround(load_id_);
                    break;
            }



        }

        public bool LoadUpdate()
        {
            switch (type_)
            {
                case GameUIDefinition.SpriteType.Character:
                    is_load_end_ = GameUICore.Instance.LoadCharacter(load_id_);
                    break;
                case GameUIDefinition.SpriteType.BackGround:
                    is_load_end_ = GameUICore.Instance.LoadBackGround(load_id_);
                    break;
            }

            return is_load_end_;
        }
    }

    /// <summary>
    /// コマンドが分割されている状態
    /// </summary>
    private List<CommandSplit> list_command_split_ = new List<CommandSplit>();

    /// <summary>
    /// 読み込みしている画像IDリスト
    /// </summary>
    private List<LoadSpriteIDData> list_load_sprite_id_ = new List<LoadSpriteIDData>();

    /// <summary>
    /// コマンドアクション管理
    /// </summary>
    private NovelCommandActionManager command_action_manager_ = new NovelCommandActionManager();

    public void Play(NovelTalkDataScriptable talk_data, NovelDetailsManager novel_manager)
    {
        if (talk_data == null) return;

        foreach (var data in talk_data.GetListData())
        {
            string text = data.GetCommandText();
            CommandSplitCalculation(text,data.GetID());
        }

        CommandLoadSprite();
        CommandSetUp(novel_manager);
    }

    /// <summary>
    /// テキストを分割
    /// </summary>
    private void CommandSplitCalculation(string text,int id)
    {
        CommandSplit split = new CommandSplit();
        split.Start(text,id);
        list_command_split_.Add(split);
    }

    /// <summary>
    /// 画像ロード
    /// </summary>
    private void CommandLoadSprite()
    {
        foreach(var command in list_command_split_)
        {
            ///画像ならロード
            if(command.GetCommandType() == GameUIDefinition.CommandMainNovel.BackGround || command.GetCommandType() == GameUIDefinition.CommandMainNovel.Character)
            {
                GameUIDefinition.SpriteType type = GameUIDefinition.SpriteType.BackGround;
                if (command.GetCommandType() == GameUIDefinition.CommandMainNovel.Character) type = GameUIDefinition.SpriteType.Character;

                var data = command.GetCommandFindName(command.GetCommandType());
                if (data == null) continue;

                LoadSpriteIDData load = new LoadSpriteIDData();
                load.Load(type, int.Parse(data.GetCommandProperty()));

                list_load_sprite_id_.Add(load);
            }
        }
    }

    private void CommandSetUp(NovelDetailsManager novel_manager)
    {
        command_action_manager_.CommandActionSetUp(list_command_split_, novel_manager);
    }

    public void Update(NovelDetailsManager novel_manager)
    {
        command_action_manager_.Update();
    }


}
