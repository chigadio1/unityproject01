﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベルコマンド：キャラアクション
/// </summary>
public class CharacterCommandAction : BaseCommandAction<NovelImageDetails>
{
    /// <summary>
    /// イメージ
    /// </summary>
    private ImageCommandRole image_role_ = new ImageCommandRole();

    /// <summary>
    /// 画像ID
    /// </summary>
    private int image_id_ = 0;

    /// <summary>
    /// 表情
    /// </summary>
    private GameUIDefinition.CharacterFaceType face_type_ = GameUIDefinition.CharacterFaceType.Amimia;

    /// <summary>
    /// フェードタイプ
    /// </summary>
    private FadeCommandRole.FadeType fade_type_ = FadeCommandRole.FadeType.Out;

    /// <summary>
    /// フェード時間
    /// </summary>
    private float fade_time_ = 0.0f;

    public override bool GetIsEnd()
    {
        return image_role_.GetIsEnd();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value_image"></param>
    /// <param name="value_type"></param>
    /// <param name="value_time">フェードにかかる時間</param>
    public override void Start()
    {
        if (details_ == null) return;
        image_role_.Start(GameUICore.Instance.GetCharacterSprite(image_id_,face_type_),fade_type_,fade_time_);
       

    }

    protected override void CommnadSplitSetUp()
    {
        if (split_ == null) return;

        foreach(var data in split_.GetListCommandDetails())
        {
            string name = data.GetCommandName();
            string property = data.GetCommandProperty();
            if (name.IndexOf(GameUIDefinition.GetNovelListCharacterCommand(GameUIDefinition.CharacterCommandNovel.Fade_Time)) >= 0)
            {
                fade_time_ = float.Parse(property); ;
            }
            else if(name.IndexOf(GameUIDefinition.GetNovelListCharacterCommand(GameUIDefinition.CharacterCommandNovel.Fade)) >= 0)
            {
                if (property.IndexOf("In") >= 0 || property.IndexOf("IN") >= 0) fade_type_ = FadeCommandRole.FadeType.In;
            }
            else if(name.IndexOf(GameUIDefinition.GetNovelListCharacterCommand(GameUIDefinition.CharacterCommandNovel.Facial)) >= 0)
            {
                int type = int.Parse(property);
                face_type_ = (GameUIDefinition.CharacterFaceType)type;
            }
            else if(name.IndexOf(GameUIDefinition.GetNovelListCharacterCommand(GameUIDefinition.CharacterCommandNovel.Location)) >= 0)
            {
                //
            }
            else if(name.IndexOf("Value") >= 0)
            {

            }
            else
            {
                image_id_ = int.Parse(property);
                details_.DetailsSetUp(image_id_);
            }
        }

        if(image_id_ == 0)
        {
            image_id_ = details_.GetImageID();
        }
    }

    public override void ActionInit()
    {
        if (details_ == null) return;
        image_role_.Init();
    }

    public override void ActionUpdate()
    {
        if (details_ == null) return;
        image_role_.CommandUpdate();
        details_.SetAlpha(image_role_.GetFadeLerpTime());
        details_.SetImage(image_role_.GetImage());
        details_.OnImage();
    }

}
