﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎コマンドアクション
/// </summary>
public class BaseCommandAction<T> : BaseOrijinCommandAction where T : BaseNovelDetails
{

    /// <summary>
    /// 詳細
    /// </summary>
    public T details_ = null;

    public override bool GetIsEnd()
    {
        return true;
    }

    public virtual void SetUp(T value, CommandCalculation.CommandSplit value_split)
    {
        details_ = value;
        split_ = value_split;
        CommnadSplitSetUp();
    }



    /// <summary>
    /// コマンド反映更新
    /// </summary>
    /// <param name="details"></param>
    public override void ActionUpdate()
    {

    }

    /// <summary>
    /// アクションの数値を初期化
    /// </summary>
    public override void ActionInit()
    {

    }
}
