﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
///     テキストコマンド
/// </summary>
public class TextCommandRole : BaseCommandRole
{

    /// <summary>
    /// 計算時間
    /// </summary>
    private float time_ = 0.0f;

    /// <summary>
    /// 終了フラグ
    /// </summary>
    private bool is_end_ = false;

    /// <summary>
    /// テキスト
    /// </summary>
    private string text_ = "";

    /// <summary>
    /// 保存されているテキスト
    /// </summary>
    private string save_text_ = "";
    public string GetText() { return save_text_; }

    /// <summary>
    /// テキスト表示範囲
    /// </summary>
    private int text_range_ = 1;


    /// <summary>
    /// 終了判定
    /// </summary>
    /// <returns></returns>
    public override bool GetIsEnd()
    {
        return is_end_;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="value_wait_time">待ち時間</param>
    public void Start(string text)
    {
        text_ = text;
    }

    public override void Init()
    {
        time_  = 0.0f;
        is_end_ = false;
    }

    public override void CommandUpdate()
    {
        time_ += Time.deltaTime * GameUIDefinition.GetEventTalkSpeed();

        if(time_ >= GameUIDefinition.GetMaxNovelTalkTimeNext())
        {
            text_range_++;
            time_ = 0.0f;
            text_range_ = Mathf.Min(text_range_, text_.Length);
            save_text_ = text_.Substring(0, text_range_);

            if(text_range_ >= text_.Length)
            {
                is_end_ = true;
            }

        }
    }
}
