﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノベルコマンド：待ち時間アクション
/// </summary>
public class WaitTimeCommandAction : BaseOrijinCommandAction
{
    /// <summary>
    /// 待ち時間
    /// </summary>
    private WaitTimeCommandRole wait_tme_role_ = new WaitTimeCommandRole();

    /// <summary>
    /// 待ち時間測定
    /// </summary>
    private float wait_time_ = 0.0f;


    public override bool GetIsEnd()
    {
        return wait_tme_role_.GetIsEnd();
    }

    /// <summary>
    /// 
    /// </summary>
    public override void Start()
    {

        wait_tme_role_.Start(wait_time_);
    }

    protected override void CommnadSplitSetUp()
    {
        if (split_ == null) return;

        var data = split_.GetListCommandDetails();
        if (data == null) return;

        wait_time_ = float.Parse(data[0].GetCommandProperty());
    }

    public override void ActionInit()
    {
        wait_tme_role_.Init();
    }

    public override void ActionUpdate()
    {
        
        wait_tme_role_.CommandUpdate();
    }
}
