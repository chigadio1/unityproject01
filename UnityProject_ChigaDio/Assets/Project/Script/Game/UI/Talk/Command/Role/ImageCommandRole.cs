﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 画像コマンド
/// </summary>
public class ImageCommandRole : BaseCommandRole
{
    /// <summary>
    /// フェードコマンド
    /// </summary>
    protected FadeCommandRole fade_command_ = new FadeCommandRole();

    /// <summary>
    /// 画像
    /// </summary>
    protected Sprite image_ = null;
    public Sprite GetImage() { return image_; }

    /// <summary>
    /// 0から1におさまるフェード単位
    /// </summary>
    /// <returns></returns>
    public float GetFadeLerpTime()
    {
        return fade_command_.FadeLerpTime();
    }

    /// <summary>
    /// 終了判定
    /// </summary>
    /// <returns></returns>
    public override bool GetIsEnd()
    {
        return fade_command_.GetIsEnd();
    }

    public override void Init()
    {
        fade_command_.Init();
    }

    /// <summary>
    /// 更新処理
    /// </summary>
    public override void CommandUpdate()
    {
        fade_command_.CommandUpdate();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="value_image"></param>
    /// <param name="value_type"></param>
    /// <param name="value_time">フェードにかかる時間</param>
    public void Start(Sprite value_image, FadeCommandRole.FadeType value_type = FadeCommandRole.FadeType.Out, float value_time = 0.0f)
    {
        image_ = value_image;
        fade_command_.Start(value_type, value_time);
    }
}
