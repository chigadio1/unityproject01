﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 待ち時間コマンド
/// </summary>
public class WaitTimeCommandRole : BaseCommandRole
{
    /// <summary>
    /// 待ち時間
    /// </summary>
    private float wait_time_ = 0.0f;
    /// <summary>
    /// 計算時間
    /// </summary>
    private float time_ = 0.0f;

    /// <summary>
    /// 終了判定
    /// </summary>
    /// <returns></returns>
    public override bool GetIsEnd()
    {
        return time_ >= wait_time_ ? true : false;
    }

    /// <summary>
    /// 初期化
    /// </summary>
    /// <param name="value_wait_time">待ち時間</param>
    public void Start(float value_wait_time = 0.0f)
    {
        wait_time_ = value_wait_time;
    }

    public override void Init()
    {
        wait_time_ = time_ = 0.0f;
    }

    public override void CommandUpdate()
    {
        time_ += Time.deltaTime * GameUIDefinition.GetEventTalkSpeed();

        time_ = Mathf.Min(wait_time_, time_);
    }
}
