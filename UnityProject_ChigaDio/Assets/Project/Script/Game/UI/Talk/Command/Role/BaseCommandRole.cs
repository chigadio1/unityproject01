﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎会話コマンド
/// </summary>
public class BaseCommandRole
{
    public virtual bool GetIsEnd()
    {
        return true;
    }
    /// <summary>
    /// 更新処理
    /// </summary>
    public virtual void CommandUpdate()
    {

    }

    /// <summary>
    /// 初期化
    /// </summary>
    public virtual void Init()
    {

    }
}
