﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIDungeonTalkAuxiliary : BaseGameUITalkAuxiliary
{


    private TalkProductManager talk_product_manager_ = new TalkProductManager();

    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "GameUIDungeonTalkCore";

    }

    public void StartDungeonTalk(int map_id , int talk_id)
    {
        if (talk_product_manager_.GetIsPlay()) return;

        talk_product_manager_.SetUp(map_id, talk_id);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameUICore.Instance.GetIsSetUp() == false) return;
        talk_product_manager_.Update();
    }
}
