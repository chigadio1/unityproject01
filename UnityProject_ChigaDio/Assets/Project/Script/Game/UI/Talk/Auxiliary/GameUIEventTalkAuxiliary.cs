﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUIEventTalkAuxiliary : BaseGameUITalkAuxiliary
{



    /// <summary>
    /// 会話動作
    /// </summary>
    private TalkProduct talk_product_ = new TalkProduct();

    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "GameUIEventTalkCore";
       
    }

    public override  void CreateTalkUI(GameObject instance) 
    {
       
        talk_product_.SetUp(instance, "悲しいよな……\n目の前はパラダイスなのに、手がとどかないこのもどかしさよ", "花村　陽介");
    }

    // Update is called once per frame
    void Update()
    {
        talk_product_.Update(null);
    }
}
