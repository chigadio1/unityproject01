﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎会話UIコア
/// </summary>
public class BaseGameUITalkAuxiliary : BaseUIAuxiliary
{
    [SerializeField]
    /// <summary>
    /// 会話オブジェクト
    /// </summary>
    protected GameObject instance_talk_object_ = null;

    public GameObject CreateTalkObject(Vector3 position)
    {

        var parent_obj = gameObject.GetSearchObjectName<RectTransform>("TalkParent");
        if (parent_obj == null) return null;
        var instance_obj = GameObject.Instantiate(instance_talk_object_, Vector3.zero, Quaternion.identity);
        instance_obj.transform.SetParent(parent_obj.transform);
        instance_obj.GetComponent<RectTransform>().localPosition = position;
        return instance_obj;
    }

    public virtual void CreateTalkUI(GameObject instance) { }
}
