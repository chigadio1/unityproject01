﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// UI補助マネージャー
/// </summary>
public class UIAuxiliaryManager
{
    /// <summary>
    /// ダンジョン会話
    /// </summary>
    private BaseUIGeneration<GameUIDungeonTalkAuxiliary> dungeon_talk_auxiliary_generation_ = new BaseUIGeneration<GameUIDungeonTalkAuxiliary>();

    public GameUIDungeonTalkAuxiliary GetGameUIDungeonTalkAuxiliary()
    {
        if (is_setup_ == false) return null;
        return dungeon_talk_auxiliary_generation_.GetUIAuxiliary();
    }

    public void StartDungeonTalk(int map_id, int talk_id)
    {
        if (is_setup_ == false) return;
        GetGameUIDungeonTalkAuxiliary()?.StartDungeonTalk(map_id, talk_id);
    }

    /// <summary>
    /// イベント会話
    /// </summary>
    private BaseUIGeneration<GameUIEventTalkAuxiliary> event_talk_auxiliary_generation_ = new BaseUIGeneration<GameUIEventTalkAuxiliary>();
    public GameUIEventTalkAuxiliary GetGameUIEventTalkAuxiliary()
    {
        if (is_setup_ == false) return null;
        return event_talk_auxiliary_generation_.GetUIAuxiliary();
    }

    /// <summary>
    /// 数字
    /// </summary>
    private BaseUIGeneration<GameUINumberAuxiliary> number_auxiliary_generation_ = new BaseUIGeneration<GameUINumberAuxiliary>();
    public GameUINumberAuxiliary GetGameUINumberAuxiliary()
    {
        if (is_setup_ == false) return null;
        return number_auxiliary_generation_.GetUIAuxiliary();
    }

    private BaseUIAuxiliary[] ui_auxiliaries_ = new BaseUIAuxiliary[(int)GameUIDefinition.AuxiliaryType.MAX];

    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }


    public void SetUp()
    {
        
        dungeon_talk_auxiliary_generation_.LoadStart(GameUIDefinition.GetGameUIPath(GameUIDefinition.AuxiliaryType.UIDungeonTalk));
        event_talk_auxiliary_generation_.LoadStart(GameUIDefinition.GetGameUIPath(GameUIDefinition.AuxiliaryType.UIEventTalk));
        number_auxiliary_generation_.LoadStart(GameUIDefinition.GetGameUIPath(GameUIDefinition.AuxiliaryType.UINumber));
    }

    public void DungeonSetUp()
    {
        SetActive(GameUIDefinition.AuxiliaryType.UIDungeonTalk, true);
        SetActive(GameUIDefinition.AuxiliaryType.UIEventTalk, true);
        SetActive(GameUIDefinition.AuxiliaryType.UINumber, true);
    }

    public void BattleSetUp()
    {
        SetActive(GameUIDefinition.AuxiliaryType.UIDungeonTalk, false);
        SetActive(GameUIDefinition.AuxiliaryType.UIEventTalk, false);
        SetActive(GameUIDefinition.AuxiliaryType.UINumber, true);
    }

    public void Update()
    {

        dungeon_talk_auxiliary_generation_.Update();
        event_talk_auxiliary_generation_.Update();
        number_auxiliary_generation_.Update();
        SetuUpUpdate();
    }

    private void SetuUpUpdate()
    {
        if (is_setup_ == true) return;
        ui_auxiliaries_[(int)GameUIDefinition.AuxiliaryType.UIDungeonTalk] = dungeon_talk_auxiliary_generation_.GetUIAuxiliary();
        ui_auxiliaries_[(int)GameUIDefinition.AuxiliaryType.UIEventTalk] = event_talk_auxiliary_generation_.GetUIAuxiliary() ;
        ui_auxiliaries_[(int)GameUIDefinition.AuxiliaryType.UINumber] = number_auxiliary_generation_.GetUIAuxiliary();

        foreach(var ui in ui_auxiliaries_)
        {
            if(ui == null)
            {
                is_setup_ = false;
                return;
            }
            ui.gameObject.SetActive(false);
        }

        foreach (var ui in ui_auxiliaries_)
        {
            ui.gameObject.transform.SetParent(GameUICore.Instance.transform);
        }



        is_setup_ = true;
    }

    private void SetActive(GameUIDefinition.AuxiliaryType type,bool flag)
    {
        if (is_setup_ == false) return;
        if (ui_auxiliaries_[(int)type] == null) return;

        ui_auxiliaries_[(int)type].gameObject.SetActive(flag);
    }


}
