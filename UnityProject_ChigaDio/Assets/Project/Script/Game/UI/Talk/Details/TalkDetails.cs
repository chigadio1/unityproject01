﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 会話詳細
/// </summary>
public class TalkDetails
{
    /// <summary>
    /// UIテキストTalk
    /// </summary>
    private Text ui_text_talk_ = null;

    /// <summary>
    /// UIテキストName
    /// </summary>
    private Text ui_text_name_ = null;

    /// <summary>
    /// キャラImage
    /// </summary>
    private Image ui_chara_image_ = null;
    public void SetCharaImage(Sprite value_chara_sprite)
    {
        if (ui_chara_image_ == null) return;
        ui_chara_image_.sprite = value_chara_sprite;
    }

    /// <summary>
    /// 時間
    /// </summary>
    private float time_ = 0.0f;

    /// <summary>
    /// テキスト速度
    /// </summary>
    private float text_speed_ = 3.0f;

    /// <summary>
    /// テキストカウント
    /// </summary>
    private int text_count_ = 1;

    /// <summary>
    /// 終了判定
    /// </summary>
    private bool is_end_ = true;
    public bool GetIsEnd() { return is_end_; }
    public void OnIsEnd() { is_end_ = true; }

    public void SetUp(GameObject parent_object)
    {
        var text_chiled = parent_object.GetComponentsInChildren<Text>();
        foreach(var text in text_chiled)
        {
            if (text.gameObject.name == GameUIDefinition.GetTalkObjectName())
            {
                ui_text_talk_ = text;
            }
            else if (text.gameObject.name == GameUIDefinition.GetCharaNameObjectName())
            {
                ui_text_name_ = text;
            }
        }

        var image_chiled = parent_object.GetComponentsInChildren<Image>();

        foreach(var image in image_chiled)
        {
            if(image.gameObject.name == GameUIDefinition.GetCharaImageObjectName())
            {
                ui_chara_image_ = image;
            }
        }

        is_end_ = false;
    }

    public void Init()
    {
        time_ = 0.0f;
        text_count_ = 1;
    }

    public void Update(string value_name,string value_talk,Sprite value_chara_image)
    {
        if(is_end_ == true)
        {
            text_count_ = Mathf.Clamp(text_count_, 0, value_talk.Length);
            return;
        }

        if (ui_text_name_ == null) return;
        if (ui_text_talk_ == null) return;
        if (ui_chara_image_ == null) return;

        ui_text_name_.text = value_name;
        UpdateTalk(value_talk);

        if (value_chara_image == null) return;
        ui_chara_image_.sprite = value_chara_image;
    }

    private void UpdateTalk(string value_talk)
    {

        if (value_talk.Length == 0)
        {
            is_end_ = true;
            return;
        }
        time_ += Time.deltaTime * text_speed_;



        ui_text_talk_.text = value_talk.Substring(0, text_count_);
        if(time_ >= GameUIDefinition.GetMaxTalkTimeNext())
        {

            if (text_count_ >= value_talk.Length)
            {
                is_end_ = true;
                return;
            }

            time_ = 0.0f;
            
            string sytem_talk =  value_talk.Substring(text_count_ - 1, 1);
            if(sytem_talk == "/" || sytem_talk == @"\" || sytem_talk == @"\")
            {
                text_count_++;
            }

            text_count_++;

            text_count_ = Mathf.Clamp(text_count_, 0, value_talk.Length);
        }
    }
}
