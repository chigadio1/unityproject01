﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#if UNITY_EDITOR

using UnityEditor;
[CustomEditor(typeof(TalkPlaceScriptableData))]
public class EditorTalkPlaceScriptableData : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<TalkPlaceData>(target as BaseExcelScriptableObject<TalkPlaceData>);
        base.OnInspectorGUI();
    }
}
#endif

/// <summary>
/// 会話場所
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreateTalkPlace")]
public class TalkPlaceScriptableData : BaseExcelScriptableObject<TalkPlaceData>
{
}

public class TalkPlaceStorage : BaseDataStorage<TalkPlaceData, TalkPlaceScriptableData>
{
    protected override string GetPath(int id)
    {
        return $"Assets/Project/Assets/Data/Map/{id:0000}/Talk/DungeonTalk/TalkPoint/DungeonTalkPlace.asset";
    }

    protected override void SetValue(TalkPlaceData data)
    {
        dictionary_data_.Add(data.GetID(), data);
    }
}

