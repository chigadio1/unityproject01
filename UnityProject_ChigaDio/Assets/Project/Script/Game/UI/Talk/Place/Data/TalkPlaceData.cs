﻿using System.Collections;
using System.Collections.Generic;
using ProjectSystem;
using UnityEngine;


/// <summary>
/// 会話場所データ
/// </summary>
[System.Serializable]
public class TalkPlaceData : BaseComposition
{

    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }
    /// <summary>
    /// ID
    /// </summary>
    [SerializeField]
    private int id_ = 0;
    public int GetID() { return id_; }
    /// <summary>
    /// 会話ID
    /// </summary>
    [SerializeField]
    private int talk_id_ = 0;
    public int GetTalkID() { return talk_id_; }
    [SerializeField]
    private float talk_position_x_ = 0.0f;
    public float GetTalkPositionX() { return talk_position_x_; }
    [SerializeField]
    private float talk_position_y_ = 0.0f;
    public float GetTalkPositionY() { return talk_position_y_; }
    [SerializeField]
    private float talk_position_z_ = 0.0f;
    public float GetTalkPositionZ() { return talk_position_z_; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(id_), id_);
        data_group.AddData("TalkID", nameof(talk_id_), talk_id_);
        data_group.AddData("X座標", nameof(talk_position_x_), talk_position_x_);
        data_group.AddData("Y座標", nameof(talk_position_y_), talk_position_y_);
        data_group.AddData("Z座標", nameof(talk_position_z_), talk_position_z_);
    }
}
