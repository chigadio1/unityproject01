﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 会話データ管理
/// </summary>
public class TalkDataStorage
{
    /// <summary>
    /// 会話データ
    /// </summary>
    private AddressableData<CharacterTalkDataScriptable> addressable_character_talk_data_ = null;

    /// <summary>
    /// セットアップフラグ
    /// </summary>  
    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }

    public bool GetIsEnd(int index)
    {
        if (is_setup_ == false) return false;

        return index >= addressable_character_talk_data_.GetAddressableData().GetListData().Count - 1 ? true : false;
    }

    public void LoadStart(int map_id, int talk_id)
    {
        addressable_character_talk_data_ = new AddressableData<CharacterTalkDataScriptable>();
        addressable_character_talk_data_.LoadStart($"Assets/Project/Assets/Data/Map/{map_id:0000}/Talk/DungeonTalk/TalkList/{talk_id:0000}.asset");
        is_setup_ = false;
    }

    public void Release()
    {
        addressable_character_talk_data_.Release();
    }

    public void Update()
    {
        if (is_setup_ == true) return;

        if (addressable_character_talk_data_.GetFlagSetUpLoading() == false) return;

        foreach(var data in addressable_character_talk_data_.GetAddressableData().GetListData())
        {
            GameUICore.Instance.LoadCharacter(data.GetCharacterID());
        }

        is_setup_ = true;
        
    }

    public CharacterTalkData GetIndexCharacterTalkData(int index)
    {
        if (is_setup_ == false) return null;
        if (addressable_character_talk_data_.GetAddressableData() == null) return null;
        if (index >= addressable_character_talk_data_.GetAddressableData().GetListData().Count) return null;
        return addressable_character_talk_data_.GetAddressableData().GetListData()[index];
    }
}
