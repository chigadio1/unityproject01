﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 会話生成物
/// </summary>
public class TalkProduct
{
    /// <summary>
    /// 会話詳細
    /// </summary>
    private TalkDetails talk_details_ = new TalkDetails();
    public void SetCharaImage(Sprite value_chara_sprite)
    {
        talk_details_.SetCharaImage(value_chara_sprite);
    }
    /// <summary>
    /// UITransform
    /// </summary>
    private RectTransform rect_transform_ = null;
    public RectTransform GetRectTransform() { return rect_transform_; }

    /// <summary>
    /// 最初の座標
    /// </summary>
    private Vector3 start_position_ = Vector3.zero;

    /// <summary>
    /// 終了座標
    /// </summary>
    private Vector3 end_position_ = Vector3.zero;

    /// <summary>
    /// 会話
    /// </summary>
    private string talk_text_ = "";
    /// <summary>
    /// 名前
    /// </summary>
    private string name_text_ = "";

    public bool GetIsEnd() { return talk_details_.GetIsEnd(); }
    public bool GetIsEndPosition()
    {
        if (rect_transform_ == null) return true;
        return (rect_transform_.position - end_position_).magnitude <= GameUIDefinition.GetTalkDungeonCheckPositionLength() ? true : false;
    }
    public void OnIsEnd() { talk_details_.OnIsEnd(); }

    /// <summary>
    /// 初期化
    /// </summary>
    public void SetUp(GameObject instance,string value_talk,string value_name)
    {
        talk_details_.SetUp(instance);
        rect_transform_ = instance.GetComponent<RectTransform>();
        end_position_ = rect_transform_.position;
        talk_text_ = value_talk;
        name_text_ = value_name;
    }

    public void Init()
    {
        talk_details_.Init();
    }

    public void Update(Sprite value_chara)
    {
        talk_details_.Update(name_text_, talk_text_, value_chara);

        if (rect_transform_ == null) return;
        rect_transform_.position = Vector3.Lerp(rect_transform_.position, end_position_, Time.deltaTime * GameUIDefinition.GetTalkDungeonAddPositionSpeed());
    }

    /// <summary>
    /// 上
    /// </summary>
    /// <param name="add_y"></param>
    public void AddY(float add_y)
    {
        end_position_.y += add_y;
    }

    public void Destroy()
    {
        if (rect_transform_ == null) return;
        GameObject.Destroy(rect_transform_.gameObject);
    }
}
