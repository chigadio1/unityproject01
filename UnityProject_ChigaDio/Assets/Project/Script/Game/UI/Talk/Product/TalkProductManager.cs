﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 会話役割マネージャー
/// </summary>
public class TalkProductManager
{
    /// <summary>
    /// 会話役割リスト
    /// </summary>
    private List<TalkProduct> list_talk_product_ = new List<TalkProduct>();

    /// <summary>
    ///会話データ集
    /// </summary>
    private TalkDataStorage talk_data_storage_ = new TalkDataStorage();

    /// <summary>
    /// カウント
    /// </summary>
    private int index_count_ = -1;

    /// <summary>
    /// 時間計測
    /// </summary>
    private float time_ = 0.0f;

    /// <summary>
    /// 再生フラグ
    /// </summary>
    private bool is_play_ = false;
    public bool GetIsPlay() { return is_play_; }


    public void SetUp(int map_id,int talk_id)
    {
        if (is_play_ == true) return;
        talk_data_storage_.LoadStart(map_id,talk_id);
        is_play_ = true;
    }

    private void TimeUpdate()
    {
        time_ += Time.deltaTime;
    }

    public void Update()
    {
        
        if (is_play_ == false) return;
        talk_data_storage_.Update();
        if (talk_data_storage_.GetIsSetUp() == false) return;
        if (GameUICore.Instance.GetIsSetUp() == false) return;

        AddTalkUpdate();


        foreach (var data in list_talk_product_)
        {
            data.Update(GameUICore.Instance.GetCharacterSprite(1,GameUIDefinition.CharacterFaceType.Amimia));
        }



    }

    public void AddTalkUpdate()
    {

        if (list_talk_product_.Count > 0)
        {
           if (list_talk_product_[list_talk_product_.Count - 1].GetIsEnd() == true && list_talk_product_[list_talk_product_.Count - 1].GetIsEndPosition() == true)
           {
                TimeUpdate();
                if (time_ >= GameUIDefinition.GetTalkDungeonNextKeepMaxTime())
                {
                    if (talk_data_storage_.GetIsEnd(index_count_) == true)
                    {
                        time_ = 0.0f;
                        AllRelease();

                    }
                    else
                    {
                        AddTalk(GameUICore.Instance.GetGameUIDungeonTalkInstanceObject(Vector3.zero));
                        time_ = 0.0f;
                    }
                }
           }
        }
        else
        {
           AddTalk(GameUICore.Instance.GetGameUIDungeonTalkInstanceObject(Vector3.zero));
        }
    }

    public void AddTalk(GameObject instance)
    {
        if (talk_data_storage_.GetIsSetUp() == false) return;
        if (instance == null) return;

        index_count_++;
        var talk_data = talk_data_storage_.GetIndexCharacterTalkData(index_count_);
        if (talk_data == null) return;

        TalkProduct add_talk = new TalkProduct();
        add_talk.SetUp(instance, talk_data.GetCharacterDialogue(), talk_data.GetCharacterName());
        add_talk.SetCharaImage(GameUICore.Instance.GetCharacterSprite(talk_data.GetCharacterID(), talk_data.GetCharacterFaceType()));
        var rect = add_talk.GetRectTransform();
        if (rect == null) return;
        rect.localPosition = GameUIDefinition.GetTalkDungeonStartPosition();

        list_talk_product_.Add(add_talk);

        foreach(var data in list_talk_product_)
        {
            data.AddY(GameUIDefinition.GetTalkDungeonAddPositionY());
        }
    }

    public void AllRelease()
    {
        talk_data_storage_.Release();
        foreach(var data in list_talk_product_)
        {
            data.Destroy();
        }
        index_count_ = -1;
        list_talk_product_.Clear();
        is_play_ = false;
    }
}
