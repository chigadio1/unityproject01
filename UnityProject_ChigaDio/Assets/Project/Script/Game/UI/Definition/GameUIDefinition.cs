﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///　ゲームUI定義
/// </summary>
public static class GameUIDefinition
{
    /// <summary>
    /// 表情
    /// </summary>
    public enum CharacterFaceType
    {
        Amimia, //無表情
        Angry, //怒り顔
        inform_01, //告げ
        BeAmazed, //あきれ顔
        inform_02, //告げ２
        GiveUp, //諦め
        Denial, //否定
        BeShy //照れる
    }

    /// <summary>
    /// 補助タイプ
    /// </summary>
    public enum AuxiliaryType
    {
        UIDungeonTalk,
        UIEventTalk,
        UINumber,
        
        MAX
    }

    /// <summary>
    /// 画像タイプ
    /// </summary>
    public enum SpriteType
    {
        Character,
        BackGround,
        None
    }

    /// <summary>
    /// ノベルのキャラ立ち位置
    /// </summary>
    public enum NovelCharacterLocation
    {
        Center,
        Left,
        Right
    }

    /// <summary>
    /// コマンドメイン
    /// </summary>
    public enum CommandMainNovel
    {
        BackGround,
        Character,
        Text,
        WaitTime,
        Fade,
        None
    }

    /// <summary>
    /// 背景コマンド
    /// </summary>
    public enum BackGroundCommandNovel
    {
        Fade,
        Fade_Time
    }

    /// <summary>
    /// フェードコマンド
    /// </summary>
    public enum FadeCommandNovel
    {
        Fade_Time
    }


    /// <summary>
    /// キャラコマンド
    /// </summary>
    public enum CharacterCommandNovel
    {
        Fade,
        Fade_Time,
        Facial,
        Location
    }

    /// <summary>
    /// テキストコマンド
    /// </summary>
    public enum TextCommandNovel
    {
        Actor,
        Dialog,
        
    }

    /// <summary>
    /// ノベル：背景コマンドリスト
    /// </summary>
    private static string[] novel_list_background_command_ =
    {
        "Fade","Fade.Time"
    };
    public static string[] GetNovelListBackGroundCommand() { return novel_list_background_command_; }
    public static string GetNovelListBackGroundCommand(BackGroundCommandNovel type) { return novel_list_background_command_[(int)type]; }

    /// <summary>
    /// ノベル：フェードコマンドリスト
    /// </summary>
    private static string[] novel_list_fade_command_ =
    {
        "Fade.Time"
    };
    public static string[] GetNovelListFadeCommand() { return novel_list_fade_command_; }
    public static string GetNovelListFadeCommand(FadeCommandNovel type) { return novel_list_fade_command_[(int)type]; }

    /// <summary>
    /// ノベル：キャラコマンドリスト
    /// </summary>
    private static string[] novel_list_character_command_ =
    {
        "Fade","Fade.Time","Facial","Location"
    };
    public static string[] GetNovelListCharacterCommand() { return novel_list_character_command_; }
    public static string GetNovelListCharacterCommand(CharacterCommandNovel type) { return novel_list_character_command_[(int)type]; }

    /// <summary>
    /// ノベル：テキストコマンドリスト
    /// </summary>
    private static string[] novel_list_text_command_ =
    {
        "Actor","Dialog"
    };
    public static string[] GetNovelListTextCommand() { return novel_list_text_command_; }
    public static string GetNovelListTextCommand(TextCommandNovel type) { return novel_list_text_command_[(int)type]; }

    /// <summary>
    /// ダンジョン会話パス
    /// </summary>
    private static string game_ui_dungeon_talk_path_ = "Assets/Project/Assets/UI/Prefab/Talk/UIDungeonTalkAuxiliary.prefab";
    public　static string GetGameUIDungeonTalkPath() { return game_ui_dungeon_talk_path_; }

    /// <summary>
    /// イベント会話パス
    /// </summary>
    private static string game_ui_event_talk_path_ = "Assets/Project/Assets/UI/Prefab/Talk/UIEventTalkAuxiliary.prefab";
    public static string GetGameUIEventTalkPath() { return game_ui_event_talk_path_; }

    /// <summary>
    /// 数字パス
    /// </summary>
    private static string game_ui_number_path_ = "Assets/Project/Assets/UI/Prefab/Number/UINumber.prefab";
    public static string GetGameUINumberPath() { return game_ui_number_path_; }

    /// <summary>
    ///  ノベル会話パス
    /// </summary>
    private static string game_ui_novel_talk_path_ = "Assets/Project/Assets/UI/Prefab/Talk/UINovelTalkAuxiliary.prefab";
    public static string GetGameUINovelTalkPath() { return game_ui_number_path_; }

    /// <summary>
    /// パス
    /// </summary>
    private static string[] game_ui_path_ = new string[(int)AuxiliaryType.MAX]
    {
        game_ui_dungeon_talk_path_,game_ui_event_talk_path_,game_ui_number_path_
    };
    public static string GetGameUIPath(AuxiliaryType type)
    {
        return game_ui_path_[(int)type];
    }

    /// <summary>
    /// 名前オブジェクトUI
    /// </summary>
    private static string chara_name_object_name_ = "NameText";
    public static string GetCharaNameObjectName() { return chara_name_object_name_; }

    /// <summary>
    /// 会話オブジェクトUI
    /// </summary>
    private static string talk_object_name_ = "TalkText";
    public static string GetTalkObjectName() { return talk_object_name_; }

    /// <summary>
    /// キャラ画像オブジェクトUI
    /// </summary>
    private static string chara_image_object_name_ = "CharaImage";
    public static string GetCharaImageObjectName() { return chara_image_object_name_; }

    /// <summary>
    /// 会話の一文字ずつ表示するmax時間
    /// </summary>
    private static float max_talk_time_next_ = 0.25f;
    public static float GetMaxTalkTimeNext() { return max_talk_time_next_; }


    /// <summary>
    /// 会話の一文字ずつ表示するmax時間
    /// </summary>
    private static float max_novel_talk_time_next_ = 0.1f;
    public static float GetMaxNovelTalkTimeNext() { return max_novel_talk_time_next_; }

    /// <summary>
    /// イベント会話座標
    /// </summary>
    private static Vector3 talk_event_position_ = new Vector3(0.0f, -300.0f, 0.0f);
    public static Vector3 GetTalkEventUIPosition() { return talk_event_position_; }

    /// <summary>
    /// ダンジョン会話の座標スピード
    /// </summary>
    private static float talk_dungeon_add_position_speed_ = 3.0f;
    public static float GetTalkDungeonAddPositionSpeed() { return talk_dungeon_add_position_speed_; }

    /// <summary>
    /// ダンジョン会話の吹き出しの最初の座標
    /// </summary>
    private static Vector3 talk_dungeon_start_position = new Vector3(0.0f, -120.0f, 0.0f);
    public static Vector3 GetTalkDungeonStartPosition() { return talk_dungeon_start_position; }


    /// <summary>
    /// ダンジョン会話の吹き出しY加算
    /// </summary>
    private static float talk_dungeon_add_position_y_ = 120.0f;
    public static float GetTalkDungeonAddPositionY() { return talk_dungeon_add_position_y_; }

    /// <summary>
    /// 最終座標と現在の座標との比較に使う数値
    /// </summary>
    private static float talk_dungeon_check_position_length_ = 1.0f;
    public static float GetTalkDungeonCheckPositionLength() { return talk_dungeon_check_position_length_; }

    /// <summary>
    /// ダンジョン会話で、一つのセリフを言い終えたら次のセリフを言うまで待つ時間
    /// </summary>
    private static float talk_dungeon_next_keep_max_time_ = 0.24f;
    public static float GetTalkDungeonNextKeepMaxTime() { return talk_dungeon_next_keep_max_time_; }

    /// <summary>
    /// UIの数字生存時間
    /// </summary>
    private static float number_life_max_time_ = 5.0f;
    public static float GetNumberLifeMaxTime() { return number_life_max_time_; }

    /// <summary>
    /// バトル数字:幅
    /// </summary>
    private static float number_battle_width_ = 100.0f;
    public static float GetNumberBattleWidth() { return number_battle_width_; }

    /// <summary>
    /// バトル数字:高さ
    /// </summary>
    private static float number_battle_height_ = 100.0f;
    public static float GetNumberBattleHeight() { return number_battle_height_; }

    /// <summary>
    /// イベントトークスピード
    /// </summary>
    private static float event_talk_speed_ = 1.0f;
    public static float GetEventTalkSpeed() { return event_talk_speed_; }

    /// <summary>
    /// ノベルキャラ最大人数
    /// </summary>
    private static int max_novel_chara_count_ = 7;
    public static int GetMaxNovelCharaCount() { return max_novel_chara_count_; }

}
