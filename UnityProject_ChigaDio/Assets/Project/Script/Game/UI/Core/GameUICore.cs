﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲームUICore
/// </summary>
public class GameUICore : Singleton<GameUICore>
{
    #region GameUIManager
    /// <summary>
    /// Gameuiの管理
    /// </summary>
    private GameUIManagerData game_ui_manager = new GameUIManagerData();

    public bool LoadCharacter(int character_id)
    {
        is_setup_ = game_ui_manager.LoadCharacter(character_id);
        return is_setup_;
    }

    public Sprite GetCharacterSprite(int character_id, GameUIDefinition.CharacterFaceType face_type)
    {
        return game_ui_manager.GetCharacterSprite(character_id, face_type);
    }

    public void ReleaseCharacter(int character_id)
    {
        game_ui_manager.ReleaseCharacter(character_id);
    }

    public void ReleaseCharacterAll()
    {
        game_ui_manager.ReleaseCharacterAll();
    }

    public bool LoadBackGround(int background_id)
    {
        is_setup_ = game_ui_manager.LoadBackGround(background_id);
        return is_setup_;
    }

    public Sprite GetBackGroundSprite(int background_id)
    {
        return game_ui_manager.GetBackGroundSprite(background_id);
    }

    public void ReleaseBackGround(int character_id)
    {
        game_ui_manager.ReleaseBackGround(character_id);
    }

    public void ReleaseBackGroundAll()
    {
        game_ui_manager.ReleaseBackGroundAll();
    }

    #endregion

    #region Auxiliary

    /// <summary>
    /// 補助UI管理
    /// </summary>
    private UIAuxiliaryManager ui_auxiliary_manager_ = new UIAuxiliaryManager();

    public GameUIDungeonTalkAuxiliary GetGameUIDungeonTalkAuxiliary()
    {
        return ui_auxiliary_manager_.GetGameUIDungeonTalkAuxiliary();
    }
    public void StartDungeonTalk(int map_id, int talk_id)
    {
        if (is_setup_ == false) return;
        GetGameUIDungeonTalkAuxiliary()?.StartDungeonTalk(map_id, talk_id);
    }

    public GameObject GetGameUIDungeonTalkInstanceObject(Vector3 pos)
    {
        return GetGameUIDungeonTalkAuxiliary()?.CreateTalkObject(pos);
    }

    /// <summary>
    /// バトル数字
    /// </summary>
    public void BattleNumber(int number,Vector3 position)
    {
        ui_auxiliary_manager_.GetGameUINumberAuxiliary().Add(position, GameUIDefinition.GetNumberBattleWidth(), GameUIDefinition.GetNumberBattleHeight(), number, Color.white);
    }

    public void DungeonSetUp()
    {
        ui_auxiliary_manager_.DungeonSetUp();
    }
    #endregion

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = true;
    public bool GetIsSetUp() { return is_setup_ = game_ui_manager.GetIsSetUp() & ui_auxiliary_manager_.GetIsSetUp(); }

    /// <summary>
    /// UIのセットアップフラグ
    /// </summary>
    /// <returns></returns>
    public bool GetIsSetUI() { return game_ui_manager.GetIsSetUp(); }
    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "GameUICore";

        ui_auxiliary_manager_.SetUp();

        ///仮
        game_ui_manager.LoadCharacter(1);
    }

    // Update is called once per frame
    void Update()
    {
        SetUpCheck();
        ui_auxiliary_manager_.Update();
        game_ui_manager.Update();
    }

    

    private void SetUpCheck()
    {
        is_setup_ = game_ui_manager.GetIsSetUp() & ui_auxiliary_manager_.GetIsSetUp();
    }

    public void OnDestroy()
    {
        game_ui_manager.ReleaseAll();
        
    }
}
