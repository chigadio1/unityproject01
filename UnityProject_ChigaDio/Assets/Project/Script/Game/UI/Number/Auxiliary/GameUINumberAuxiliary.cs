﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUINumberAuxiliary : BaseUIAuxiliary
{
    /// <summary>
    /// 数字UI管理
    /// </summary>
    private NumberUIManager number_ui_manager_ = new NumberUIManager();

    void Start()
    {
        gameObject.name = "GameUINumber";
    }

    void Update()
    {
        SetVerticesDirty();
    }

    public void Add(Vector3 world_position, float value_width, float value_height, int value_number, Color color)
    {
        number_ui_manager_.Add(world_position, value_width, value_height, value_number, color);
    }

    public void Draw(VertexHelper vh)
    {
        number_ui_manager_.Draw(vh,gameObject.GetComponent<RectTransform>());
    }

    protected override void UpdateMaterial()
    {
        base.UpdateMaterial();
        canvasRenderer.SetTexture(material.mainTexture);

    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();



        Draw(vh);


    }
}
