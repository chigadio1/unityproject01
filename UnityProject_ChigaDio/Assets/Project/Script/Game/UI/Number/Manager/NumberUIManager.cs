﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ナンバーUI管理
/// </summary>
public class NumberUIManager
{
    /// <summary>
    /// 数字メッシュ
    /// </summary>
    private List<NumbersMesh> list_number_mesh_ = new List<NumbersMesh>();

    /// <summary>
    /// 追加
    /// </summary>
    /// <param name="world_position"></param>
    /// <param name="value_width"></param>
    /// <param name="value_height"></param>
    /// <param name="value_number"></param>
    /// <param name="color"></param>
    public void Add(Vector3 world_position, float value_width, float value_height, int value_number, Color color)
    {
        NumbersMesh mesh = new NumbersMesh();
        mesh.SetUp(world_position, value_width, value_height, value_number, color);
        list_number_mesh_.Add(mesh);
    }

    public void Draw(VertexHelper vh,RectTransform transform)
    {
        if (vh == null) return;

    

        foreach (var data in list_number_mesh_)
        {
            data.Draw(vh,transform);
        }

        list_number_mesh_.RemoveAll(data => data.GetIsEnd() == true);

       
    }

    public void Destroy()
    {
        list_number_mesh_.Clear();
    }
}
