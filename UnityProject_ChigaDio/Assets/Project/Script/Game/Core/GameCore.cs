﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲームコア
/// </summary>
public class GameCore : Singleton<GameCore>
{

    /// <summary>
    /// ゲームメインメンバ-
    /// </summary>
    private MainGameUnitMember game_unit_member_ = new MainGameUnitMember();
    public BaseUnit GetMainPlayerUnit() { return game_unit_member_.GetMainPlayerUnit(); }
    public BaseUnit GetPlayerUnitIndex(int index) { return game_unit_member_.GetPlayerUnitIndex(index); }

    /// <summary>
    /// バトルコリジョンコア
    /// </summary>
    private BattleCollisionCore collision_core_ = null;

    /// <summary>
    /// Unitコア
    /// </summary>
    private UnitCore unit_core_ = null;

    /// <summary>
    /// エフェクトコア
    /// </summary>
    private EffectCore effect_core_ = null;

    /// <summary>
    /// ダンジョンコア
    /// </summary>
    private DungeonCore dungeon_core_ = null;

    /// <summary>
    /// UIコア
    /// </summary>
    private GameUICore ui_core_ = null;

    /// <summary>
    /// サウンドコア
    /// </summary>
    private SoundCore sound_core_ = null;


    public bool GetIsSetUp() { return collision_core_.GetIsSetUP() & unit_core_.GetIsSetUp() & effect_core_.GetIsSetUp() & ui_core_.GetIsSetUp() & sound_core_.GetIsSetUp(); }

    // Start is called before the first frame update
    void Start()
    {
        gameObject.name = "GameCore";

        collision_core_ = BattleCollisionCore.Instance;
        unit_core_ = UnitCore.Instance;
        effect_core_ = EffectCore.Instance;
        dungeon_core_ = DungeonCore.Instance;
        ui_core_ = GameUICore.Instance;
        sound_core_ = SoundCore.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

#if UNITY_EDITOR
    public void CreateMain(int[] member_unit_id)
    {
        game_unit_member_.CreatePlayerMain(member_unit_id[0]);
        game_unit_member_.CreatePlayerNPC(member_unit_id[1],1);
        game_unit_member_.CreatePlayerNPC(member_unit_id[2], 2);
        game_unit_member_.CreatePlayerNPC(member_unit_id[3], 3);
    }

    public void SE(int sound_id = 2001)
    {
        sound_core_.BattleSESoundPlay(sound_id);
    }

    public void BGM(int sound_id = 2501)
    {
        sound_core_.BattleBGMSoundPlay(sound_id);
    }
#endif
}
