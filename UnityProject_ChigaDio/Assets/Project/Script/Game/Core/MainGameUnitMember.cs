﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// メインゲームメンバー
/// </summary>
public class MainGameUnitMember
{
    /// <summary>
    /// プレイヤー側
    /// </summary>
    private BaseUnit[] player_units_ = new BaseUnit[BattleDefinition.GetMaxBattleUnitPlayer()];
    public BaseUnit GetPlayerUnitIndex(int index) { return player_units_[index]; }

    /// <summary>
    /// 操作プレイヤー
    /// </summary>
    private BaseUnit main_player_unit_ = null;
    public BaseUnit GetMainPlayerUnit() { return main_player_unit_; }

    public bool CreatePlayerMain(int unit_id)
    {
        if (DataCore.Instance.GetCharacterDictionaryData(unit_id) == null) return false;
        var unit_object = UnitCore.Instance.GetUnitGameObject(unit_id);
        if (unit_object == null) return false;
        if (main_player_unit_ != null) return false;

        var instance_obj = GameObject.Instantiate(unit_object, Vector3.zero, Quaternion.identity);
        instance_obj.transform.SetParent(DungeonCore.Instance.transform);

        instance_obj.gameObject.name = $"PlayerUnit_{unit_id:000}";
        var unit = instance_obj.AddComponent<DungeonPlayerUnit>();
        unit.InitUnit(unit_id);

        main_player_unit_ = unit;
        player_units_[0] = unit;

        var game_camera = Camera.main.gameObject.AddComponent<BaseGameUnitCamera>();
        game_camera.SetTargetUnit(main_player_unit_);

        return true;
    }

    public bool CreatePlayerNPC(int unit_id,int member_number)
    {
        if (DataCore.Instance.GetCharacterDictionaryData(unit_id) == null) return false;
        var unit_object = UnitCore.Instance.GetUnitGameObject(unit_id);
        if (unit_object == null) return false;
        
        if (player_units_[member_number] != null) return false; 
        var instance_obj = GameObject.Instantiate(unit_object, Vector3.zero, Quaternion.identity);
        instance_obj.transform.SetParent(DungeonCore.Instance.transform);

        instance_obj.gameObject.name = $"PlayerUnit_{unit_id:000}";
        var unit = instance_obj.AddComponent<DungeonUnit>();
        unit.InitUnit(unit_id);
        unit.transform.position = new Vector3(0.0f, 0.0f, 1.0f * member_number);

        player_units_[member_number] = unit;

        return true;
    }
}
