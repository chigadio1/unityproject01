﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(GameCore))]
public class GameCoreEditor : Editor
{


    /// <summary>
    /// Characterインデックス
    /// </summary>
    private int character_index_ = 0;


    /// <summary>
    /// Effectインデックス
    /// </summary>
    private int effect_index_ = 0;
    /// <summary>
    /// 生存フレーム
    /// </summary>
    private float effect_life_frame_ = 0.0f;


    /// <summary>
    /// バトルインデックス
    /// </summary>
    private int battle_unit_index_ = 0;

    /// <summary>
    /// メンバーID
    /// </summary>
    private int[] member_unit_id_ = new int[BattleDefinition.GetMaxBattleUnitPlayer()];


    public void OnEnable()
    {
        
    }

    public override void OnInspectorGUI()
    {
        var game = target as GameCore;
        if (game == null) return;

        CreateDungeon();
        CretaeChara();
        SoundSE();
        SoundBGM();
        Number();
        CretaeDungeonUnit();
        CretaeBattleUnit();
    }

    private void CreateDungeon()
    {
        if (EditorApplication.isPlaying == false) return;
        EditorGUILayout.BeginHorizontal();


        if (GUILayout.Button("CreateDungeon"))
        {
            DungeonCore.Instance.InisStart();
        }

        EditorGUILayout.EndHorizontal();
    }

    private void CretaeChara()
    {
        if (EditorApplication.isPlaying == false) return;

        EditorGUILayout.BeginHorizontal();


        var list_data = DictionaryAccessor.GetListDictionaryData(DictionaryType.Type.Character);
        if (list_data == null || list_data.Count == 0)
        {
            EditorGUILayout.EndHorizontal();
            return;
        }

        List<string> chara_name_ = new List<string>();
        foreach (var data in list_data)
        {
            chara_name_.Add(DictionaryAccessor.GetName(data));
        }

        EditorGUI.BeginChangeCheck();
        var select_index = character_index_;

        var pul_index = chara_name_.Count > 0 ? EditorGUILayout.Popup("Chara", select_index, chara_name_.ToArray()) : -1;

        if (EditorGUI.EndChangeCheck())
        {
            character_index_ = pul_index;
        }


        if (GUILayout.Button("CreateUnitObject"))
        {
            var data = DictionaryAccessor.GetCharacterDictionaryData(chara_name_[character_index_]);
            UnitCore.Instance.CreateUnit(DictionaryAccessor.GetID(data));
        }

        EditorGUILayout.EndHorizontal();


    }

    private void CretaeDungeonUnit()
    {
        if (UnityEditor.EditorApplication.isPlaying == false) return;
        for (int count = 0; count < BattleDefinition.GetMaxBattleUnitPlayer(); count++)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField($"Player_Number{count + 1:0000}");
            member_unit_id_[count] = EditorGUILayout.IntField(member_unit_id_[count]);

            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("MainMember"))
        {
            GameCore.Instance.CreateMain(member_unit_id_);
        }
    }

    private void SoundSE()
    {

        if (GUILayout.Button("SE"))
        {
            GameCore.Instance.SE();
        }
    }

    private void SoundBGM()
    {

        if (GUILayout.Button("BGM"))
        {
            GameCore.Instance.BGM();
        }
    }

    private void Number()
    {

        if (GUILayout.Button("Number"))
        {
            GameUICore.Instance.BattleNumber(9999, Camera.main.transform.forward * 3.0f + Camera.main.transform.position);
        }
    }

    private void CretaeBattleUnit()
    {
        if (EditorApplication.isPlaying == false) return;

        EditorGUILayout.BeginHorizontal();


        var list_data = UnitCore.Instance.GetLoadUnitIDList();
        if (list_data == null || list_data.Count == 0)
        {
            EditorGUILayout.EndHorizontal();
            return;
        }

        List<string> chara_name_ = new List<string>();
        foreach (var data in list_data)
        {
            chara_name_.Add(DictionaryAccessor.GetName(DictionaryAccessor.GetCharacterDictionaryData(data)));
        }

        EditorGUI.BeginChangeCheck();
        var select_index = battle_unit_index_;

        var pul_index = chara_name_.Count > 0 ? EditorGUILayout.Popup("Chara", select_index, chara_name_.ToArray()) : -1;

        if (EditorGUI.EndChangeCheck())
        {
            battle_unit_index_ = pul_index;
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("CreateBattleUnitObject(Player)"))
        {
            var data = DictionaryAccessor.GetCharacterDictionaryData(chara_name_[battle_unit_index_]);
            BattleCore.Instance.CreatePlayerBattleUnit(DictionaryAccessor.GetID(data));
            EffectCore.Instance.BattleStart();
        }


        if (GUILayout.Button("CreateBattleUnitObject(Enemy)"))
        {
            var data = DictionaryAccessor.GetCharacterDictionaryData(chara_name_[battle_unit_index_]);
            BattleCore.Instance.CreateEnemyBattleUnit(DictionaryAccessor.GetID(data));
        }


        EditorGUILayout.EndHorizontal();


    }
}
#endif