﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonCore : Singleton<DungeonCore>
{
    private DungeonDataManager dungeon_data_manager_ = new DungeonDataManager();
    /// <summary>
    /// ダンジョン会話処理
    /// </summary>
    private DungeonTalkPlaceProcess dungeon_talk_place_process_ = new DungeonTalkPlaceProcess();
    /// <summary>
    /// マップ情報
    /// </summary>
    private MapDetails dungeon_map_details_ = new MapDetails();
    /// <summary>
    /// マップID
    /// </summary>
    private int map_id_ = 0;
    public int GetMapID() { return map_id_; }

    /// <summary>
    /// ユニットセットアップ  
    /// </summary>
    private bool is_setup_unit_ = false;
    /// <summary>
    /// ユニットセットアップ Create 
    /// </summary>
    private bool is_setup_create_unit_ = false;

    public bool GetIsSetUp()
    {
        return dungeon_data_manager_.GetIsSetUp() & dungeon_map_details_.GetIsSetUp();
    }

    public void Start()
    {
        gameObject.name = "DungeonCore";
    }

    public void InisStart(int map_id = 9001)
    {
        map_id_ = map_id;
        dungeon_data_manager_ = new DungeonDataManager();
        dungeon_data_manager_.InitLoadStart(map_id);
        dungeon_map_details_.LoadStart(map_id_);
        dungeon_talk_place_process_.LoadStart(map_id);
        GameUICore.Instance.DungeonSetUp();
    }

    public void SetUpUpdate()
    {
        if (dungeon_data_manager_.GetIsSetUp() == false) return;
        if (dungeon_map_details_.GetIsSetUp() == false) return;
        if (dungeon_talk_place_process_.GetIsSetUp() == false) return;
        SetUpUnit();
    }

    private void SetUpUnit()
    {
        if (is_setup_unit_ == true)
        {
            SetUpUnitCreate();
            return;
        }
        is_setup_unit_ = true;

        var all_data = dungeon_data_manager_.GetAllDungeonUnitGameData();
        if(all_data == null)
        {
            is_setup_unit_ = false;
            return;
        }

        foreach (var data in all_data)
        {
            UnitCore.Instance.CreateUnit(data.Value.GetUnitID());
        }
    }

    private void SetUpUnitCreate()
    {
        if (is_setup_create_unit_ == true) return;
        is_setup_create_unit_ = true;

        if(UnitCore.Instance.GetIsSetUp() == false)
        {
            is_setup_create_unit_ = false;
            return;
        }

        var all_data = dungeon_data_manager_.GetAllDungeonUnitGameData();
        if (all_data == null)
        {
            is_setup_create_unit_ = false;
            return;
        }

        foreach(var data in all_data)
        {
            var root_data = dungeon_data_manager_.GetDungeonRootData(data.Value.GetRootID());
            if (root_data == null) continue;

            var unit_obj = GameObject.Instantiate(UnitCore.Instance.GetUnitGameObject(data.Value.GetUnitID()),Vector3.zero,Quaternion.identity);
            if (unit_obj == null) continue;

            unit_obj.transform.position = new Vector3(data.Value.GetUnitStartPositionX(), data.Value.GetUnitStartPositionY(), data.Value.GetUnitStartPositionZ());
            var unit = unit_obj.AddComponent<DungeonEnemyUnit>();



            unit.InitDungeonEnemy(data.Value.GetUnitID(),data.Value.GetID(),root_data);
            unit_obj.transform.SetParent(gameObject.transform);
        }
    }

    private void CreateMapUpdate()
    {
        dungeon_map_details_?.CreateMapObject();
    }

    public void Update()
    {
        dungeon_data_manager_?.Update();
        CreateMapUpdate();
        dungeon_map_details_?.Update();
        dungeon_talk_place_process_?.Update();
        SetUpUpdate();
    }

    /// <summary>
    /// 座標が近い会話ポジションを割り出し、開始させる
    /// </summary>
    /// <param name="position"></param>
    public void StartDungeonTalk(Vector3 position)
    {
        if (is_setup_create_unit_ == false || is_setup_unit_ == false) return;
        dungeon_talk_place_process_?.StartDungeonTalk(position);
    }

    public void LoadStartDungeonBattleMemberData(int id)
    {
        dungeon_data_manager_.LoadStartDungeonBattleMemberData(id);
    }
    public DungeonBattleMemberGameData GetDungeonBattleMemberData(int search_id)
    {
        return dungeon_data_manager_?.GetDungeonBattleMemberData(search_id);
    }
    public Dictionary<int, DungeonBattleMemberGameData> GetDungeonAllBattleMemberData()
    {
        return dungeon_data_manager_?.GetDungeonAllBattleMemberData();
    }

    public void LoadStartDungeonBattlePlacementData(int id)
    {
        dungeon_data_manager_.LoadStartDungeonBattlePlacementData(id);
    }
    public DungeonBattlePlacementGamelData GetDungeonBattlePlacementData(int search_id)
    {
        return dungeon_data_manager_?.GetDungeonBattlePlacementData(search_id);
    }

    public void LoadStartDungeonUnitData(int id)
    {
        dungeon_data_manager_.LoadStartDungeonUnitData(id);
    }
    public DungeonUnitGameData GetDungeonUnitData(int search_id)
    {
        return dungeon_data_manager_?.GetDungeonUnitData(search_id);
    }

    private void OnDestroy()
    {
        dungeon_data_manager_.Release();
    }
}
