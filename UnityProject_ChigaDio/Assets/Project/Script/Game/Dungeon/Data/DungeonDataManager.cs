﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョンデータマネージャー
/// </summary>
public class DungeonDataManager
{
    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = true;
    public bool GetIsSetUp()
    {
        return is_setup_ & dungeon_battle_placement_storage_.GetIsSetUp() & dungeon_battle_member_storage_.GetIsSetUp() & dungeon_unit_storage_.GetIsSetUp() & dungeon_root_storage_.GetIsSetUp();
    }


    /// <summary>
    /// ダンジョンバトルメンバー保管
    /// </summary>
    private DungeonBattleMemberDataStorage dungeon_battle_member_storage_ = new DungeonBattleMemberDataStorage();

    public void LoadStartDungeonBattleMemberData(int id)
    {
        dungeon_battle_member_storage_.LoadStart(id);
    }
    public DungeonBattleMemberGameData GetDungeonBattleMemberData(int search_id)
    {
        return dungeon_battle_member_storage_?.GetData(search_id);
    }

    public Dictionary<int,DungeonBattleMemberGameData> GetDungeonAllBattleMemberData()
    {
        return dungeon_battle_member_storage_?.GetAllData();
    }

    /// <summary>
    /// ダンジョンバトル配置保管
    /// </summary>
    private DungeonBattlePlacementDataStorage dungeon_battle_placement_storage_ = new DungeonBattlePlacementDataStorage();
    public void LoadStartDungeonBattlePlacementData(int id)
    {
        dungeon_battle_placement_storage_.LoadStart(id);
    }
    public DungeonBattlePlacementGamelData GetDungeonBattlePlacementData(int search_id)
    {
        return dungeon_battle_placement_storage_?.GetData(search_id);
    }

    /// <summary>
    /// ダンジョンに存在するUnit情報保管
    /// </summary>
    private DungeonUnitDataStorage dungeon_unit_storage_ = new DungeonUnitDataStorage();

    public void LoadStartDungeonUnitData(int id)
    {
        dungeon_unit_storage_.LoadStart(id);
    }
    public DungeonUnitGameData GetDungeonUnitData(int search_id)
    {
        return dungeon_unit_storage_?.GetData(search_id);
    }
    public Dictionary<int,DungeonUnitGameData> GetAllDungeonUnitGameData()
    {
        return dungeon_unit_storage_.GetAllData();
    }

    /// <summary>
    /// ダンジョンルート
    /// </summary>
    private DungeonRootDataStorage dungeon_root_storage_ = new DungeonRootDataStorage();
    public DungeonRootData GetDungeonRootData(int search_id)
    {
        return dungeon_root_storage_.GetData(search_id);
    }

    public void Init() { }

    public void InitLoadStart(int id)
    {
        LoadStartDungeonBattleMemberData(id);
        LoadStartDungeonBattlePlacementData(id);
        LoadStartDungeonUnitData(id);
        dungeon_root_storage_.LoadStart(id);
    }

    public void Update()
    {
        dungeon_battle_member_storage_.Update();
        dungeon_battle_placement_storage_.Update();
        dungeon_unit_storage_.Update();
        dungeon_root_storage_.Update();
    }

    public void Release()
    {
        dungeon_root_storage_.Release();
    }
}
