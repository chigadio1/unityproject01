﻿using System.Collections;
using System.Collections.Generic;
using ProjectSystem;
using UnityEngine;

/// <summary>
/// ダンジョンUnitデータExcel
/// </summary>
[System.Serializable]
public class DungeonUnitExcelData : BaseComposition
{
    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// ID
    /// </summary>
    [SerializeField]
    private int id_ = 0;
    public int GetID() { return id_; }


    /// <summary>
    /// UnitID
    /// </summary>
    [SerializeField]
    private int unit_id_ = 0;
    public int GetUnitID() { return unit_id_; }

    /// <summary>
    /// 初期座標X
    /// </summary>
    [SerializeField]
    private float unit_start_position_x_ = 0.0f;
    public float GetUnitStartPositionX() { return unit_start_position_x_; }

    /// <summary>
    /// 初期座標Y
    /// </summary>
    [SerializeField]
    private float unit_start_position_y_ = 0.0f;
    public float GetUnitStartPositionY() { return unit_start_position_y_; }

    /// <summary>
    /// 初期座標Z
    /// </summary>
    [SerializeField]
    private float unit_start_position_z_ = 0.0f;
    public float GetUnitStartPositionZ() { return unit_start_position_z_; }

    /// <summary>
    /// ボスフラグ
    /// </summary>
    [SerializeField]
    private bool is_boss_ = false;
    public bool GetIsBoss() { return is_boss_; }

    /// <summary>
    /// RootID
    /// </summary>
    [SerializeField]
    private int root_id_ = 0;
    public int GetRootID() { return root_id_; }

    /// <summary>
    /// MemberID
    /// </summary>
    [SerializeField]
    private int member_id_ = 0;
    public int GetMemberID() { return member_id_; }

    /// <summary>
    /// PlacementID
    /// </summary>
    [SerializeField]
    private int placement_id_ = 0;
    public int GetPlacementID() { return placement_id_; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(id_), id_);
        data_group.AddData("UnitID", nameof(unit_id_), unit_id_);
        data_group.AddData("初期座標:X", nameof(unit_start_position_x_), unit_start_position_x_);
        data_group.AddData("初期座標:Y", nameof(unit_start_position_y_), unit_start_position_y_);
        data_group.AddData("初期座標:Z", nameof(unit_start_position_z_), unit_start_position_z_);
        data_group.AddData("BossFrag", nameof(is_boss_), is_boss_);
        data_group.AddData("RootID", nameof(root_id_), root_id_);
        data_group.AddData("MemberID", nameof(member_id_), member_id_);
        data_group.AddData("PlacementID", nameof(placement_id_), placement_id_);
    }
}
