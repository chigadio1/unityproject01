﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// DungeonUnitDataの保管庫
/// </summary>
public class DungeonUnitDataStorage : BaseDataStorage<DungeonUnitExcelData,DungeonUnitScriptable,DungeonUnitGameData>
{

    

    protected override string GetPath(int id)
    {
        return $"Assets/Project/Assets/Data/Map/{id:0000}/DungeonUnit.asset";
    }

    protected override void SetValue(DungeonUnitExcelData data)
    {
        if (dictionary_data_.ContainsKey(data.GetID()) == true) return;
        DungeonUnitGameData game_data = new DungeonUnitGameData();
        game_data.SetValue(data);
        dictionary_data_.Add(game_data.GetID(),game_data);
   
    }
}
