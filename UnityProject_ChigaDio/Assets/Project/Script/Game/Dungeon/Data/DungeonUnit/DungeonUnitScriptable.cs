﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(DungeonUnitScriptable))]
public class EditorDungeonUnitScriptableObject : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<DungeonUnitExcelData>(target as BaseExcelScriptableObject<DungeonUnitExcelData>);
        base.OnInspectorGUI();
    }
}
#endif


[CreateAssetMenu(menuName = "ScriptableObjects/CreareDungeonUnit")]
public class DungeonUnitScriptable : BaseExcelScriptableObject<DungeonUnitExcelData>
{
}
