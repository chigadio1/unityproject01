﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョンUnitデータ
/// </summary>
public class DungeonUnitGameData
{
    /// <summary>
    /// インデックス
    /// </summary>
    
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// ID
    /// </summary>
    
    private int id_ = 0;
    public int GetID() { return id_; }


    /// <summary>
    /// UnitID
    /// </summary>
    
    private int unit_id_ = 0;
    public int GetUnitID() { return unit_id_; }

    /// <summary>
    /// 初期座標X
    /// </summary>
    
    private float unit_start_position_x_ = 0.0f;
    public float GetUnitStartPositionX() { return unit_start_position_x_; }

    /// <summary>
    /// 初期座標Y
    /// </summary>
    
    private float unit_start_position_y_ = 0.0f;
    public float GetUnitStartPositionY() { return unit_start_position_y_; }

    /// <summary>
    /// 初期座標Z
    /// </summary>
    
    private float unit_start_position_z_ = 0.0f;
    public float GetUnitStartPositionZ() { return unit_start_position_z_; }

    /// <summary>
    /// ボスフラグ
    /// </summary>
    
    private bool is_boss_ = false;
    public bool GetIsBoss() { return is_boss_; }

    /// <summary>
    /// RootID
    /// </summary>
    
    private int root_id_ = 0;
    public int GetRootID() { return root_id_; }

    /// <summary>
    /// MemberID
    /// </summary>
    
    private int member_id_ = 0;
    public int GetMemberID() { return member_id_; }

    /// <summary>
    /// PlacementID
    /// </summary>
    
    private int placement_id_ = 0;
    public int GetPlacementID() { return placement_id_; }

    public  void SetValue(DungeonUnitExcelData data)
    {
        index_ = data.GetIndex();
        id_ = data.GetID();
        unit_id_ = data.GetUnitID();
        unit_start_position_x_ = data.GetUnitStartPositionX();
        unit_start_position_y_ = data.GetUnitStartPositionY();
        unit_start_position_z_ = data.GetUnitStartPositionZ();
        is_boss_ = data.GetIsBoss();
        root_id_ = data.GetRootID();
        member_id_ = data.GetMemberID();
        placement_id_ = data.GetPlacementID();
    }
}
