﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(DungeonBattlePlacementScriptable))]
public class EditorDungeonBattlePlacementScriptable : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<DungeonBattlePlacementExcelData>(target as BaseExcelScriptableObject<DungeonBattlePlacementExcelData>);

        base.OnInspectorGUI();
    }
}


#endif


/// <summary>
/// ダンジョンでのバトル配置データ
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreareBattlePlacement")]
public class DungeonBattlePlacementScriptable : BaseExcelScriptableObject<DungeonBattlePlacementExcelData>
{
}
