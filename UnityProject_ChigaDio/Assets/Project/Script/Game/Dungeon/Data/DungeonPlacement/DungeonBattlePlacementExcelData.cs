﻿using System.Collections;
using System.Collections.Generic;
using ProjectSystem;
using UnityEngine;

/// <summary>
/// ダンジョンバトル配置データExcel
/// </summary>
[System.Serializable]
public class DungeonBattlePlacementExcelData : BaseComposition
{
    /// <summary>
    /// Index
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// ID
    /// </summary>
    [SerializeField]
    private int id_ = 0;
    public int GetID() { return id_; }

    /// <summary>
    /// バトルエリア半径
    /// </summary>
    [SerializeField]
    private float battle_area_length_ = 0.0f;
    public float GetBattleAreaLength() { return battle_area_length_; }

    /// <summary>
    /// バトルエリア初期座標:X
    /// </summary>
    [SerializeField]
    private float battle_area_start_position_x_ = 0.0f;
    public float GetBattleAreaStartPositionX() { return battle_area_start_position_x_; }

    /// <summary>
    /// バトルエリア初期座標:Y
    /// </summary>
    [SerializeField]
    private float battle_area_start_position_y_ = 0.0f;
    public float GetBattleAreaStartPositionY() { return battle_area_start_position_y_; }

    /// <summary>
    /// バトルエリア初期座標:Z
    /// </summary>
    [SerializeField]
    private float battle_area_start_position_z_ = 0.0f;
    public float GetBattleAreaStartPositionZ() { return battle_area_start_position_z_; }

    /// <summary>
    /// Pin01初期座標:X
    /// </summary>
    [SerializeField]
    private float pin01_start_position_x_ = 0.0f;
    public float GetPin01StartPositionX() { return pin01_start_position_x_; }

    /// <summary>
    /// Pin01初期座標:Y
    /// </summary>
    [SerializeField]
    private float pin01_start_position_y_ = 0.0f;
    public float GetPin01StartPositionY() { return pin01_start_position_y_; }

    /// <summary>
    /// Pin01初期座標:Z
    /// </summary>
    [SerializeField]
    private float pin01_start_position_z_ = 0.0f;
    public float GetPin01StartPositionZ() { return pin01_start_position_y_; }

    /// <summary>
    /// Pin01拡縮度
    /// </summary>
    [SerializeField]
    private float pin01_start_scale_ = 0.0f;
    public float GetPin01StartScale() { return pin01_start_scale_; }

    /// <summary>
    /// Pin01初期回転
    /// </summary>
    [SerializeField]
    private float pin01_start_rotation_ = 0.0f;
    public float GetPin01StartRotation() { return pin01_start_rotation_; }

    /// <summary>
    /// Pin02初期座標:X
    /// </summary>
    [SerializeField]
    private float pin02_start_position_x_ = 0.0f;
    public float GetPin02StartPositionX() { return pin02_start_position_x_; }

    /// <summary>
    /// Pin02初期座標:Y
    /// </summary>
    [SerializeField]
    private float pin02_start_position_y_ = 0.0f;
    public float GetPin02StartPositionY() { return pin02_start_position_y_; }

    /// <summary>
    /// Pin02初期座標:Z
    /// </summary>
    [SerializeField]
    private float pin02_start_position_z_ = 0.0f;
    public float GetPin02StartPositionZ() { return pin02_start_position_y_; }

    /// <summary>
    /// Pin02拡縮度
    /// </summary>
    [SerializeField]
    private float pin02_start_scale_ = 0.0f;
    public float GetPin02StartScale() { return pin02_start_scale_; }

    /// <summary>
    /// Pin02初期回転
    /// </summary>
    [SerializeField]
    private float pin02_start_rotation_ = 0.0f;
    public float GetPin02StartRotation() { return pin02_start_rotation_; }

    /// <summary>
    /// Pin03初期座標:X
    /// </summary>
    [SerializeField]
    private float pin03_start_position_x_ = 0.0f;
    public float GetPin03StartPositionX() { return pin03_start_position_x_; }

    /// <summary>
    /// Pin03初期座標:Y
    /// </summary>
    [SerializeField]
    private float pin03_start_position_y_ = 0.0f;
    public float GetPin03StartPositionY() { return pin03_start_position_y_; }

    /// <summary>
    /// Pin03初期座標:Z
    /// </summary>
    [SerializeField]
    private float pin03_start_position_z_ = 0.0f;
    public float GetPin03StartPositionZ() { return pin03_start_position_y_; }

    /// <summary>
    /// Pin03拡縮度
    /// </summary>
    [SerializeField]
    private float pin03_start_scale_ = 0.0f;
    public float GetPin03StartScale() { return pin03_start_scale_; }

    /// <summary>
    /// Pin03初期回転
    /// </summary>
    [SerializeField]
    private float pin03_start_rotation_ = 0.0f;
    public float GetPin03StartRotation() { return pin03_start_rotation_; }

    /// <summary>
    /// Pin04初期座標:X
    /// </summary>
    [SerializeField]
    private float pin04_start_position_x_ = 0.0f;
    public float GetPin04StartPositionX() { return pin04_start_position_x_; }

    /// <summary>
    /// Pin04初期座標:Y
    /// </summary>
    [SerializeField]
    private float pin04_start_position_y_ = 0.0f;
    public float GetPin04StartPositionY() { return pin04_start_position_y_; }

    /// <summary>
    /// Pin04初期座標:Z
    /// </summary>
    [SerializeField]
    private float pin04_start_position_z_ = 0.0f;
    public float GetPin04StartPositionZ() { return pin04_start_position_y_; }

    /// <summary>
    /// Pin04拡縮度
    /// </summary>
    [SerializeField]
    private float pin04_start_scale_ = 0.0f;
    public float GetPin04StartScale() { return pin04_start_scale_; }

    /// <summary>
    /// Pin04初期回転
    /// </summary>
    [SerializeField]
    private float pin04_start_rotation_ = 0.0f;
    public float GetPin04StartRotation() { return pin04_start_rotation_; }

    /// <summary>
    /// Pin05初期座標:X
    /// </summary>
    [SerializeField]
    private float pin05_start_position_x_ = 0.0f;
    public float GetPin05StartPositionX() { return pin05_start_position_x_; }

    /// <summary>
    /// Pin05初期座標:Y
    /// </summary>
    [SerializeField]
    private float pin05_start_position_y_ = 0.0f;
    public float GetPin05StartPositionY() { return pin05_start_position_y_; }

    /// <summary>
    /// Pin05初期座標:Z
    /// </summary>
    [SerializeField]
    private float pin05_start_position_z_ = 0.0f;
    public float GetPin05StartPositionZ() { return pin05_start_position_y_; }

    /// <summary>
    /// Pin05拡縮度
    /// </summary>
    [SerializeField]
    private float pin05_start_scale_ = 0.0f;
    public float GetPin05StartScale() { return pin05_start_scale_; }

    /// <summary>
    /// Pin05初期回転
    /// </summary>
    [SerializeField]
    private float pin05_start_rotation_ = 0.0f;
    public float GetPin05StartRotation() { return pin05_start_rotation_; }

    /// <summary>
    /// Pin06初期座標:X
    /// </summary>
    [SerializeField]
    private float pin06_start_position_x_ = 0.0f;
    public float GetPin06StartPositionX() { return pin06_start_position_x_; }

    /// <summary>
    /// Pin06初期座標:Y
    /// </summary>
    [SerializeField]
    private float pin06_start_position_y_ = 0.0f;
    public float GetPin06StartPositionY() { return pin06_start_position_y_; }

    /// <summary>
    /// Pin06初期座標:Z
    /// </summary>
    [SerializeField]
    private float pin06_start_position_z_ = 0.0f;
    public float GetPin06StartPositionZ() { return pin06_start_position_y_; }

    /// <summary>
    /// Pin06拡縮度
    /// </summary>
    [SerializeField]
    private float pin06_start_scale_ = 0.0f;
    public float GetPin06StartScale() { return pin06_start_scale_; }

    /// <summary>
    /// Pin06初期回転
    /// </summary>
    [SerializeField]
    private float pin06_start_rotation_ = 0.0f;
    public float GetPin06StartRotation() { return pin06_start_rotation_; }

#if UNITY_EDITOR
    public void SetIndex(int index) { index_ = index; }
    public void SetID(int id) { id_ = id; }
    public void SetBattleAreaLength(float length) { battle_area_length_ = length; }
    public void SetBattleAreaStartPositionX(float position) { battle_area_start_position_x_ = position; }
    public void SetBattleAreaStartPositionY(float position) { battle_area_start_position_y_ = position; }
    public void SetBattleAreaStartPositionZ(float position) { battle_area_start_position_z_ = position; }

    public void SetPin01StartPositionX(float position) { pin01_start_position_x_ = position; }
    public void SetPin01StartPositionY(float position) { pin01_start_position_y_ = position; }
    public void SetPin01StartPositionZ(float position) { pin01_start_position_z_ = position; }
    public void SetPin01StartScale(float scale) { pin01_start_scale_ = scale; }
    public void SetPin01StartRotation(float rotation) { pin01_start_rotation_ = rotation; }

    public void SetPin02StartPositionX(float position) { pin02_start_position_x_ = position; }
    public void SetPin02StartPositionY(float position) { pin02_start_position_y_ = position; }
    public void SetPin02StartPositionZ(float position) { pin02_start_position_z_ = position; }
    public void SetPin02StartScale(float scale) { pin02_start_scale_ = scale; }
    public void SetPin02StartRotation(float rotation) { pin02_start_rotation_ = rotation; }

    public void SetPin03StartPositionX(float position) { pin03_start_position_x_ = position; }
    public void SetPin03StartPositionY(float position) { pin03_start_position_y_ = position; }
    public void SetPin03StartPositionZ(float position) { pin03_start_position_z_ = position; }
    public void SetPin03StartScale(float scale) { pin03_start_scale_ = scale; }
    public void SetPin03StartRotation(float rotation) { pin03_start_rotation_ = rotation; }

    public void SetPin04StartPositionX(float position) { pin04_start_position_x_ = position; }
    public void SetPin04StartPositionY(float position) { pin04_start_position_y_ = position; }
    public void SetPin04StartPositionZ(float position) { pin04_start_position_z_ = position; }
    public void SetPin04StartScale(float scale) { pin04_start_scale_ = scale; }
    public void SetPin04StartRotation(float rotation) { pin04_start_rotation_ = rotation; }

    public void SetPin05StartPositionX(float position) { pin05_start_position_x_ = position; }
    public void SetPin05StartPositionY(float position) { pin05_start_position_y_ = position; }
    public void SetPin05StartPositionZ(float position) { pin05_start_position_z_ = position; }
    public void SetPin05StartScale(float scale) { pin05_start_scale_ = scale; }
    public void SetPin05StartRotation(float rotation) { pin05_start_rotation_ = rotation; }

    public void SetPin06StartPositionX(float position) { pin06_start_position_x_ = position; }
    public void SetPin06StartPositionY(float position) { pin06_start_position_y_ = position; }
    public void SetPin06StartPositionZ(float position) { pin06_start_position_z_ = position; }
    public void SetPin06StartScale(float scale) { pin06_start_scale_ = scale; }
    public void SetPin06StartRotation(float rotation) { pin06_start_rotation_ = rotation; }
#endif

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(id_), id_);
        data_group.AddData("バトルエリア:半径", nameof(battle_area_length_), battle_area_length_);
        data_group.AddData("バトルエリア:初期座標:X", nameof(battle_area_start_position_x_), battle_area_start_position_x_);
        data_group.AddData("バトルエリア:初期座標:Y", nameof(battle_area_start_position_y_), battle_area_start_position_y_);
        data_group.AddData("バトルエリア:初期座標:Z", nameof(battle_area_start_position_z_), battle_area_start_position_z_);

        data_group.AddData("Pin1:初期座標:X", nameof(pin01_start_position_x_), pin01_start_position_x_);
        data_group.AddData("Pin1:初期座標:Y", nameof(pin01_start_position_y_), pin01_start_position_y_);
        data_group.AddData("Pin1:初期座標:Z", nameof(pin01_start_position_z_), pin01_start_position_z_);
        data_group.AddData("Pin1:拡縮度", nameof(pin01_start_scale_), pin01_start_scale_);
        data_group.AddData("Pin1:初期回転", nameof(pin01_start_rotation_), pin01_start_rotation_);

        data_group.AddData("Pin2:初期座標:X", nameof(pin02_start_position_x_), pin02_start_position_x_);
        data_group.AddData("Pin2:初期座標:Y", nameof(pin02_start_position_y_), pin02_start_position_y_);
        data_group.AddData("Pin2:初期座標:Z", nameof(pin02_start_position_z_), pin02_start_position_z_);
        data_group.AddData("Pin2:拡縮度", nameof(pin02_start_scale_), pin02_start_scale_);
        data_group.AddData("Pin2:初期回転", nameof(pin02_start_rotation_), pin02_start_rotation_);

        data_group.AddData("Pin3:初期座標:X", nameof(pin03_start_position_x_), pin03_start_position_x_);
        data_group.AddData("Pin3:初期座標:Y", nameof(pin03_start_position_y_), pin03_start_position_y_);
        data_group.AddData("Pin3:初期座標:Z", nameof(pin03_start_position_z_), pin03_start_position_z_);
        data_group.AddData("Pin3:拡縮度", nameof(pin03_start_scale_), pin03_start_scale_);
        data_group.AddData("Pin3:初期回転", nameof(pin03_start_rotation_), pin03_start_rotation_);

        data_group.AddData("Pin4:初期座標:X", nameof(pin04_start_position_x_), pin04_start_position_x_);
        data_group.AddData("Pin4:初期座標:Y", nameof(pin04_start_position_y_), pin04_start_position_y_);
        data_group.AddData("Pin4:初期座標:Z", nameof(pin04_start_position_z_), pin04_start_position_z_);
        data_group.AddData("Pin4:拡縮度", nameof(pin04_start_scale_), pin04_start_scale_);
        data_group.AddData("Pin4:初期回転", nameof(pin04_start_rotation_), pin04_start_rotation_);

        data_group.AddData("Pin5:初期座標:X", nameof(pin05_start_position_x_), pin05_start_position_x_);
        data_group.AddData("Pin5:初期座標:Y", nameof(pin05_start_position_y_), pin05_start_position_y_);
        data_group.AddData("Pin5:初期座標:Z", nameof(pin05_start_position_z_), pin05_start_position_z_);
        data_group.AddData("Pin5:拡縮度", nameof(pin05_start_scale_), pin05_start_scale_);
        data_group.AddData("Pin5:初期回転", nameof(pin05_start_rotation_), pin05_start_rotation_);

        data_group.AddData("Pin6:初期座標:X", nameof(pin06_start_position_x_), pin06_start_position_x_);
        data_group.AddData("Pin6:初期座標:Y", nameof(pin06_start_position_y_), pin06_start_position_y_);
        data_group.AddData("Pin6:初期座標:Z", nameof(pin06_start_position_z_), pin06_start_position_z_);
        data_group.AddData("Pin6:拡縮度", nameof(pin06_start_scale_), pin06_start_scale_);
        data_group.AddData("Pin6:初期回転", nameof(pin06_start_rotation_), pin06_start_rotation_);
    }
}
