﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョンバトル配置ゲームデータ
/// </summary>
public class DungeonBattlePlacementGamelData
{
    /// <summary>
    /// Index
    /// </summary>
    
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// ID
    /// </summary>
    
    private int id_ = 0;
    public int GetID() { return id_; }

    /// <summary>
    /// バトルエリア半径
    /// </summary>
    
    private float battle_area_length_ = 0.0f;
    public float GetBattleAreaLength() { return battle_area_length_; }

    /// <summary>
    /// バトルエリア初期座標:X
    /// </summary>
    
    private float battle_area_start_position_x_ = 0.0f;
    public float GetBattleAreaStartPositionX() { return battle_area_start_position_x_; }

    /// <summary>
    /// バトルエリア初期座標:Y
    /// </summary>
    
    private float battle_area_start_position_y_ = 0.0f;
    public float GetBattleAreaStartPositionY() { return battle_area_start_position_y_; }

    /// <summary>
    /// バトルエリア初期座標:Z
    /// </summary>
    
    private float battle_area_start_position_z_ = 0.0f;
    public float GetBattleAreaStartPositionZ() { return battle_area_start_position_z_; }

    /// <summary>
    /// Pin01初期座標:X
    /// </summary>
    
    private float pin01_start_position_x_ = 0.0f;
    public float GetPin01StartPositionX() { return pin01_start_position_x_; }

    /// <summary>
    /// Pin01初期座標:Y
    /// </summary>
    
    private float pin01_start_position_y_ = 0.0f;
    public float GetPin01StartPositionY() { return pin01_start_position_y_; }

    /// <summary>
    /// Pin01初期座標:Z
    /// </summary>
    
    private float pin01_start_position_z_ = 0.0f;
    public float GetPin01StartPositionZ() { return pin01_start_position_y_; }

    /// <summary>
    /// Pin01拡縮度
    /// </summary>
    
    private float pin01_start_scale_ = 0.0f;
    public float GetPin01StartScale() { return pin01_start_scale_; }

    /// <summary>
    /// Pin01初期回転
    /// </summary>
    
    private float pin01_start_rotation_ = 0.0f;
    public float GetPin01StartRotation() { return pin01_start_rotation_; }

    /// <summary>
    /// Pin02初期座標:X
    /// </summary>
    
    private float pin02_start_position_x_ = 0.0f;
    public float GetPin02StartPositionX() { return pin02_start_position_x_; }

    /// <summary>
    /// Pin02初期座標:Y
    /// </summary>
    
    private float pin02_start_position_y_ = 0.0f;
    public float GetPin02StartPositionY() { return pin02_start_position_y_; }

    /// <summary>
    /// Pin02初期座標:Z
    /// </summary>
    
    private float pin02_start_position_z_ = 0.0f;
    public float GetPin02StartPositionZ() { return pin02_start_position_y_; }

    /// <summary>
    /// Pin02拡縮度
    /// </summary>
    
    private float pin02_start_scale_ = 0.0f;
    public float GetPin02StartScale() { return pin02_start_scale_; }

    /// <summary>
    /// Pin02初期回転
    /// </summary>
    
    private float pin02_start_rotation_ = 0.0f;
    public float GetPin02StartRotation() { return pin02_start_rotation_; }

    /// <summary>
    /// Pin03初期座標:X
    /// </summary>
    
    private float pin03_start_position_x_ = 0.0f;
    public float GetPin03StartPositionX() { return pin03_start_position_x_; }

    /// <summary>
    /// Pin03初期座標:Y
    /// </summary>
    
    private float pin03_start_position_y_ = 0.0f;
    public float GetPin03StartPositionY() { return pin03_start_position_y_; }

    /// <summary>
    /// Pin03初期座標:Z
    /// </summary>
    
    private float pin03_start_position_z_ = 0.0f;
    public float GetPin03StartPositionZ() { return pin03_start_position_y_; }

    /// <summary>
    /// Pin03拡縮度
    /// </summary>
    
    private float pin03_start_scale_ = 0.0f;
    public float GetPin03StartScale() { return pin03_start_scale_; }

    /// <summary>
    /// Pin03初期回転
    /// </summary>
    
    private float pin03_start_rotation_ = 0.0f;
    public float GetPin03StartRotation() { return pin03_start_rotation_; }

    /// <summary>
    /// Pin04初期座標:X
    /// </summary>
    
    private float pin04_start_position_x_ = 0.0f;
    public float GetPin04StartPositionX() { return pin04_start_position_x_; }

    /// <summary>
    /// Pin04初期座標:Y
    /// </summary>
    
    private float pin04_start_position_y_ = 0.0f;
    public float GetPin04StartPositionY() { return pin04_start_position_y_; }

    /// <summary>
    /// Pin04初期座標:Z
    /// </summary>
    
    private float pin04_start_position_z_ = 0.0f;
    public float GetPin04StartPositionZ() { return pin04_start_position_y_; }

    /// <summary>
    /// Pin04拡縮度
    /// </summary>
    
    private float pin04_start_scale_ = 0.0f;
    public float GetPin04StartScale() { return pin04_start_scale_; }

    /// <summary>
    /// Pin04初期回転
    /// </summary>
    
    private float pin04_start_rotation_ = 0.0f;
    public float GetPin04StartRotation() { return pin04_start_rotation_; }

    /// <summary>
    /// Pin05初期座標:X
    /// </summary>
    
    private float pin05_start_position_x_ = 0.0f;
    public float GetPin05StartPositionX() { return pin05_start_position_x_; }

    /// <summary>
    /// Pin05初期座標:Y
    /// </summary>
    
    private float pin05_start_position_y_ = 0.0f;
    public float GetPin05StartPositionY() { return pin05_start_position_y_; }

    /// <summary>
    /// Pin05初期座標:Z
    /// </summary>
    
    private float pin05_start_position_z_ = 0.0f;
    public float GetPin05StartPositionZ() { return pin05_start_position_y_; }

    /// <summary>
    /// Pin05拡縮度
    /// </summary>
    
    private float pin05_start_scale_ = 0.0f;
    public float GetPin05StartScale() { return pin05_start_scale_; }

    /// <summary>
    /// Pin05初期回転
    /// </summary>
    
    private float pin05_start_rotation_ = 0.0f;
    public float GetPin05StartRotation() { return pin05_start_rotation_; }

    /// <summary>
    /// Pin06初期座標:X
    /// </summary>
    
    private float pin06_start_position_x_ = 0.0f;
    public float GetPin06StartPositionX() { return pin06_start_position_x_; }

    /// <summary>
    /// Pin06初期座標:Y
    /// </summary>
    
    private float pin06_start_position_y_ = 0.0f;
    public float GetPin06StartPositionY() { return pin06_start_position_y_; }

    /// <summary>
    /// Pin06初期座標:Z
    /// </summary>
    
    private float pin06_start_position_z_ = 0.0f;
    public float GetPin06StartPositionZ() { return pin06_start_position_y_; }

    /// <summary>
    /// Pin06拡縮度
    /// </summary>
    
    private float pin06_start_scale_ = 0.0f;
    public float GetPin06StartScale() { return pin06_start_scale_; }

    /// <summary>
    /// Pin06初期回転
    /// </summary>
    
    private float pin06_start_rotation_ = 0.0f;
    public float GetPin06StartRotation() { return pin06_start_rotation_; }

    public void SetValue(DungeonBattlePlacementExcelData value_data)
    {
        index_ = value_data.GetIndex();
        id_ = value_data.GetID();
        battle_area_length_ = value_data.GetBattleAreaLength();
        battle_area_start_position_x_ = value_data.GetBattleAreaStartPositionX();
        battle_area_start_position_y_ = value_data.GetBattleAreaStartPositionY();
        battle_area_start_position_z_ = value_data.GetBattleAreaStartPositionZ();

        pin01_start_position_x_ = value_data.GetPin01StartPositionX();
        pin01_start_position_y_ = value_data.GetPin01StartPositionY();
        pin01_start_position_z_ = value_data.GetPin01StartPositionZ();
        pin01_start_scale_ = value_data.GetPin01StartScale();
        pin01_start_rotation_ = value_data.GetPin01StartRotation();

        pin02_start_position_x_ = value_data.GetPin02StartPositionX();
        pin02_start_position_y_ = value_data.GetPin02StartPositionY();
        pin02_start_position_z_ = value_data.GetPin02StartPositionZ();
        pin02_start_scale_ = value_data.GetPin02StartScale();
        pin02_start_rotation_ = value_data.GetPin02StartRotation();

        pin03_start_position_x_ = value_data.GetPin03StartPositionX();
        pin03_start_position_y_ = value_data.GetPin03StartPositionY();
        pin03_start_position_z_ = value_data.GetPin03StartPositionZ();
        pin03_start_scale_ = value_data.GetPin03StartScale();
        pin03_start_rotation_ = value_data.GetPin03StartRotation();

        pin04_start_position_x_ = value_data.GetPin04StartPositionX();
        pin04_start_position_y_ = value_data.GetPin04StartPositionY();
        pin04_start_position_z_ = value_data.GetPin04StartPositionZ();
        pin04_start_scale_ = value_data.GetPin04StartScale();
        pin04_start_rotation_ = value_data.GetPin04StartRotation();

        pin05_start_position_x_ = value_data.GetPin05StartPositionX();
        pin05_start_position_y_ = value_data.GetPin05StartPositionY();
        pin05_start_position_z_ = value_data.GetPin05StartPositionZ();
        pin05_start_scale_ = value_data.GetPin05StartScale();
        pin05_start_rotation_ = value_data.GetPin05StartRotation();

        pin06_start_position_x_ = value_data.GetPin06StartPositionX();
        pin06_start_position_y_ = value_data.GetPin06StartPositionY();
        pin06_start_position_z_ = value_data.GetPin06StartPositionZ();
        pin06_start_scale_ = value_data.GetPin06StartScale();
        pin06_start_rotation_ = value_data.GetPin06StartRotation();
    }

    public Vector3 GetPinNoPosition(int pin_no)
    {
        switch (pin_no)
        {
            case 0:
                return new Vector3(GetPin01StartPositionX(), GetPin01StartPositionY(), GetPin01StartPositionZ());
            case 1:
                return new Vector3(GetPin02StartPositionX(), GetPin02StartPositionY(), GetPin02StartPositionZ());
            case 2:
                return new Vector3(GetPin03StartPositionX(), GetPin03StartPositionY(), GetPin03StartPositionZ());
            case 3:
                return new Vector3(GetPin04StartPositionX(), GetPin04StartPositionY(), GetPin04StartPositionZ());
            case 4:
                return new Vector3(GetPin05StartPositionX(), GetPin05StartPositionY(), GetPin05StartPositionZ());
            case 5:
                return new Vector3(GetPin06StartPositionX(), GetPin06StartPositionY(), GetPin06StartPositionZ());
        }

        return Vector3.zero;
    }

    public Vector3 GetPinNoScale(int pin_no)
    {
        switch (pin_no)
        {
            case 0:
                return new Vector3(GetPin01StartScale(), GetPin01StartScale(), GetPin01StartScale());
            case 1:
                return new Vector3(GetPin02StartScale(), GetPin02StartScale(), GetPin02StartScale());
            case 2:
                return new Vector3(GetPin03StartScale(), GetPin03StartScale(), GetPin03StartScale());
            case 3:
                return new Vector3(GetPin04StartScale(), GetPin04StartScale(), GetPin04StartScale());
            case 4:
                return new Vector3(GetPin05StartScale(), GetPin05StartScale(), GetPin05StartScale());
            case 5:
                return new Vector3(GetPin06StartScale(), GetPin06StartScale(), GetPin06StartScale());
        }

        return Vector3.one;
    }

    public float GetPinNoRotation(int pin_no)
    {
        switch (pin_no)
        {
            case 0:
                return GetPin01StartRotation();
            case 1:
                return GetPin02StartRotation();
            case 2:
                return GetPin03StartRotation();
            case 3:
                return GetPin04StartRotation();
            case 4:
                return GetPin05StartRotation();
            case 5:
                return GetPin06StartRotation();
        }

        return 0.0f;
    }


}
