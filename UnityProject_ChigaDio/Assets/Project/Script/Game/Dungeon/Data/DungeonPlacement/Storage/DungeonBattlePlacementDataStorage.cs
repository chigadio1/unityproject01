﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョンバトル配置データ保管庫
/// </summary>
public class DungeonBattlePlacementDataStorage  : BaseDataStorage<DungeonBattlePlacementExcelData,DungeonBattlePlacementScriptable,DungeonBattlePlacementGamelData>
{
    protected override string GetPath(int id)
    {
        return $"Assets/Project/Assets/Data/Map/{id:0000}/Placement/DungeonBattlePlacement.asset";
    }

    protected override void SetValue(DungeonBattlePlacementExcelData data)
    {
        if (dictionary_data_.ContainsKey(data.GetID()) == true) return;
        DungeonBattlePlacementGamelData game_data = new DungeonBattlePlacementGamelData();
        game_data.SetValue(data);
        dictionary_data_.Add(game_data.GetID(), game_data);
    }
}
