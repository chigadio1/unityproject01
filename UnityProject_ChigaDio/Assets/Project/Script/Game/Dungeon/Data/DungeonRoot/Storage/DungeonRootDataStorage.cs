﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョンデータストレージ
/// </summary>
public class DungeonRootDataStorage
{

    /// <summary>
    /// セットアップフラグ
    /// </summary>
    private bool is_setup_ = false;

    public bool GetIsSetUp() { return is_setup_; }

    /// <summary>
    /// アドレサブルデータ
    /// </summary>
    private AddressableData<DungeonRootScriptable> addressable_data_ = null;

    public void LoadStart(int map_id)
    {
        addressable_data_ = new AddressableData<DungeonRootScriptable>();
        addressable_data_.LoadStart($"Assets/Project/Assets/Data/Map/{map_id:0000}/Root/DungeonRoot.asset");
        is_setup_ = false;
    }

    public void Update()
    {
        if (is_setup_ == true) return;
        if (addressable_data_ == null) return;
        is_setup_ = true;

        if(addressable_data_.GetFlagSetUpLoading() == false)
        {
            is_setup_ = false;
            return;
        }
    }

    public DungeonRootData GetData(int search_id)
    {
        if (is_setup_ == false) return null;

        var dungeon = addressable_data_.GetAddressableData();
        return dungeon.GetDungeonRootData(search_id);
    }

    public void Release()
    {
        if (addressable_data_ == null) return;
        addressable_data_.Release();
    }
}
