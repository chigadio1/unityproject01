﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョンルートスクリプタブル
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreareDungeonRoot")]
public class DungeonRootScriptable : ScriptableObject
{
    /// <summary>
    /// ダンジョンルートリスト
    /// </summary>
    [SerializeField]
    private List<DungeonRootData> list_dungeon_root_data_;

#if UNITY_EDITOR
    public void AddData(DungeonRootData value_data)
    {
        if (list_dungeon_root_data_ == null) list_dungeon_root_data_ = new List<DungeonRootData>();

        if (list_dungeon_root_data_.Find(data => data.GetID() == value_data.GetID()) != null) return;

        list_dungeon_root_data_.Add(value_data);
    }

    public void SetData(int search_id, DungeonRootData value_data)
    {
        if (list_dungeon_root_data_ == null) list_dungeon_root_data_ = new List<DungeonRootData>();
        var search_data = GetDungeonRootData(search_id);
        if (search_data == null)
        {
            AddData(value_data);
        }
        else
        {
            list_dungeon_root_data_.RemoveAll(data => data.GetID() == search_id);

            list_dungeon_root_data_.Add(value_data);
        }
    }
#endif
    public DungeonRootData GetDungeonRootData(int search_id)
    {
        if (list_dungeon_root_data_ == null) list_dungeon_root_data_ = new List<DungeonRootData>();

        return list_dungeon_root_data_.Find(data => data.GetID() == search_id);
    }
}
