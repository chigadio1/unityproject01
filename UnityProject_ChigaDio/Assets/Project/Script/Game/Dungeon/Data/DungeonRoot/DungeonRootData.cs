﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョンルート
/// </summary>
[System.Serializable]
public class DungeonRootData
{
    /// <summary>
    /// ID
    /// </summary>
    [SerializeField]
    private int id_ = 0;
    public int GetID() { return id_; }

#if UNITY_EDITOR
    public void SetID(int value) { id_ = value; }
#endif

    /// <summary>
    /// ルートポジションリスト
    /// </summary>
    [SerializeField]
    private List<DungeonRootPosition> list_root_position_ = new List<DungeonRootPosition>();
    public List<DungeonRootPosition> GetListRootPosition() { return list_root_position_; }


#if UNITY_EDITOR
    public void AddRootPosition(DungeonRootPosition value) { list_root_position_.Add (value); }
#endif

    /// <summary>
    /// 動く速さ
    /// </summary>
    [SerializeField]
    private float move_speed_ = 0.1f;
    public float GetMoveSpeed() { return move_speed_; }

#if UNITY_EDITOR
    public void SetMoveSpeed(float value) { move_speed_ = value; }
#endif

    /// <summary>
    /// 振り向く速度
    /// </summary>
    [SerializeField]
    private float look_speed_ = 0.1f;
    public float GetLookSpeed() { return look_speed_; }
#if UNITY_EDITOR
    public void SetLookSpeed(float value) { look_speed_= value; }
#endif
}

/// <summary>
/// ダンジョンルートポジション
/// </summary>
[System.Serializable]
public class DungeonRootPosition
{
    /// <summary>
    /// ルートポジション
    /// </summary>
    [SerializeField]
    private Vector3 root_position_ = Vector3.zero;
    public Vector3 GetRootPosition() { return root_position_; }

#if UNITY_EDITOR
    public void SetRootPosition(Vector3 value) { root_position_ = value; }
#endif
    /// <summary>
    /// ストップフレーム
    /// </summary>
    [SerializeField]
    private float frame_stop_ = 0.0f;
    public float GetFrameStop() { return frame_stop_; }

#if UNITY_EDITOR
    public void SetFrameStop(float value)
    {
        frame_stop_ = value;
    }
#endif
}

