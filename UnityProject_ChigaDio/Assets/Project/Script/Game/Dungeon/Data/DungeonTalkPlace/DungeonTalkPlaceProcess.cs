﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョン会話処理
/// </summary>
public class DungeonTalkPlaceProcess 
{
    /// <summary>
    /// 会話ストレージ
    /// </summary>
    TalkPlaceStorage talk_place_storage_ = new TalkPlaceStorage();

    private bool is_setup_ = false;
    public bool GetIsSetUp() { return is_setup_; }



    public void LoadStart(int map_id)
    {
        if (talk_place_storage_.GetIsSetUp() == true) return;

        talk_place_storage_.LoadStart(map_id);

        is_setup_ = false;
    }

    public void Update()
    {
        if (is_setup_ == true) return;
        talk_place_storage_.Update();
        is_setup_ = talk_place_storage_.GetIsSetUp();
    }

    /// <summary>
    /// 座標が近い会話ポジションを割り出し、開始させる
    /// </summary>
    /// <param name="position"></param>
    public void StartDungeonTalk(Vector3 position)
    {
        if (is_setup_ == false) return;
        int talk_id = 0;
        Vector3 data_save_position = Vector3.zero;
        foreach (var data in talk_place_storage_.GetAllData())
        {
            Vector3 data_position = new Vector3(data.Value.GetTalkPositionX(), data.Value.GetTalkPositionY(), data.Value.GetTalkPositionZ());

            if ((position - data_position).magnitude >= 3.0f) continue;
            if((position - data_position).magnitude <= (position - data_save_position).magnitude)
            {
                data_save_position = data_position;
                talk_id = data.Value.GetTalkID();
            }
        }

        if (talk_id == (0)) return;

        GameUICore.Instance.StartDungeonTalk(DungeonCore.Instance.GetMapID(), talk_id);
    }
}
