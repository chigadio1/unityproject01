﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(DungeonBattleMemberScriptable))]
public class EditorDungeonBattleMembertScriptable : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<DungeonBattleMemberExcelData>(target as BaseExcelScriptableObject<DungeonBattleMemberExcelData>);

        base.OnInspectorGUI();
    }
}


#endif


/// <summary>
/// ダンジョンバトルメンバースクリプタブル
/// </summary>
[CreateAssetMenu(menuName = "ScriptableObjects/CreareBattleMember")]
public class DungeonBattleMemberScriptable : BaseExcelScriptableObject<DungeonBattleMemberExcelData>
{
}
