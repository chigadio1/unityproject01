﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョンバトルメンバー保管庫
/// </summary>
public class DungeonBattleMemberDataStorage : BaseDataStorage<DungeonBattleMemberExcelData, DungeonBattleMemberScriptable, DungeonBattleMemberGameData>
{
    protected override string GetPath(int id)
    {
        return $"Assets/Project/Assets/Data/Map/{id:0000}/BattleMember/DungeonBattleMember.asset";
    }

    protected override void SetValue(DungeonBattleMemberExcelData data)
    {
        if (dictionary_data_.ContainsKey(data.GetID()) == true) return;
        DungeonBattleMemberGameData game_data = new DungeonBattleMemberGameData();
        game_data.SetValue(data);
        dictionary_data_.Add(game_data.GetID(), game_data);
    }
}
