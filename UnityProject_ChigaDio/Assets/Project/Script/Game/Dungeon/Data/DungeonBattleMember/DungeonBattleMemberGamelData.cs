﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ダンジョンバトルPinUnitデータ
/// </summary>
public class DungeonBattleMemberGameData
{
    /// <summary>
    /// インデックス
    /// </summary>
    
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// ID
    /// </summary>
    
    private int id_ = 0;
    public int GetID() { return id_; }

    /// <summary>
    /// Pin01のUnitID
    /// </summary>
    
    private int pin01_unit_id_ = 0;
    public int GetPin01UnitID() { return pin01_unit_id_; }

    /// <summary>
    /// Pin02のUnitID
    /// </summary>
    
    private int pin02_unit_id_ = 0;
    public int GetPin02UnitID() { return pin02_unit_id_; }

    /// <summary>
    /// Pin03のUnitID
    /// </summary>
    
    private int pin03_unit_id_ = 0;
    public int GetPin03UnitID() { return pin03_unit_id_; }

    /// <summary>
    /// Pin04のUnitID
    /// </summary>
    
    private int pin04_unit_id_ = 0;
    public int GetPin04UnitID() { return pin04_unit_id_; }

    /// <summary>
    /// Pin05のUnitID
    /// </summary>
    
    private int pin05_unit_id_ = 0;
    public int GetPin05UnitID() { return pin05_unit_id_; }

    /// <summary>
    /// Pin06のUnitID
    /// </summary>
    
    private int pin06_unit_id_ = 0;
    public int GetPin06UnitID() { return pin06_unit_id_; }

    public int GetPinUnitID(int pin_no)
    {
        switch (pin_no)
        {
            case 0:
                return GetPin01UnitID();
            case 1:
                return GetPin02UnitID();
            case 2:
                return GetPin03UnitID();
            case 3:
                return GetPin04UnitID();
            case 4:
                return GetPin05UnitID();
            case 5:
                return GetPin06UnitID();
        }

        return 0;

    }

    public void SetValue(DungeonBattleMemberExcelData data)
    {
        index_ = data.GetIndex();
        id_ = data.GetID();
        pin01_unit_id_ = data.GetPin01UnitID();
        pin02_unit_id_ = data.GetPin02UnitID();
        pin03_unit_id_ = data.GetPin03UnitID();
        pin04_unit_id_ = data.GetPin04UnitID();
        pin05_unit_id_ = data.GetPin05UnitID();
        pin06_unit_id_ = data.GetPin06UnitID();
    }
}
