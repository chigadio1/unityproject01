﻿using System.Collections;
using System.Collections.Generic;
using ProjectSystem;
using UnityEngine;

/// <summary>
/// ダンジョンバトルPinUnitデータExcel
/// </summary>
[System.Serializable]
public class DungeonBattleMemberExcelData : BaseComposition
{

    /// <summary>
    /// インデックス
    /// </summary>
    [SerializeField]
    private int index_ = 0;
    public int GetIndex() { return index_; }

    /// <summary>
    /// ID
    /// </summary>
    [SerializeField]
    private int id_ = 0;
    public int GetID() { return id_; }

    /// <summary>
    /// Pin01のUnitID
    /// </summary>
    [SerializeField]
    private int pin01_unit_id_ = 0;
    public int GetPin01UnitID() { return pin01_unit_id_; }

    /// <summary>
    /// Pin02のUnitID
    /// </summary>
    [SerializeField]
    private int pin02_unit_id_ = 0;
    public int GetPin02UnitID() { return pin02_unit_id_; }

    /// <summary>
    /// Pin03のUnitID
    /// </summary>
    [SerializeField]
    private int pin03_unit_id_ = 0;
    public int GetPin03UnitID() { return pin03_unit_id_; }

    /// <summary>
    /// Pin04のUnitID
    /// </summary>
    [SerializeField]
    private int pin04_unit_id_ = 0;
    public int GetPin04UnitID() { return pin04_unit_id_; }

    /// <summary>
    /// Pin05のUnitID
    /// </summary>
    [SerializeField]
    private int pin05_unit_id_ = 0;
    public int GetPin05UnitID() { return pin05_unit_id_; }

    /// <summary>
    /// Pin06のUnitID
    /// </summary>
    [SerializeField]
    private int pin06_unit_id_ = 0;
    public int GetPin06UnitID() { return pin06_unit_id_; }

    public override void ManualSetUp(ref DataFrameGroup data_group, ref ExcelSystem.DataGroup excel_data_group)
    {
        data_group.AddData("Index", nameof(index_), index_);
        data_group.AddData("ID", nameof(id_), id_);
        data_group.AddData("Pin1UnitID", nameof(pin01_unit_id_), pin01_unit_id_);
        data_group.AddData("Pin2UnitID", nameof(pin02_unit_id_), pin02_unit_id_);
        data_group.AddData("Pin3UnitID", nameof(pin03_unit_id_), pin03_unit_id_);
        data_group.AddData("Pin4UnitID", nameof(pin04_unit_id_), pin04_unit_id_);
        data_group.AddData("Pin5UnitID", nameof(pin05_unit_id_), pin05_unit_id_);
        data_group.AddData("Pin6UnitID", nameof(pin06_unit_id_), pin06_unit_id_);

    }
}
