﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GaneTestScript : MonoBehaviour
{
    /// <summary>
    /// コリジョン
    /// </summary>
    [SerializeField]
    protected BaseCollisionConstruction collision_;
    // Start is called before the first frame update
    void Start()
    {
        collision_ = new BaseCollisionConstruction();
        collision_.Init(gameObject, CollisionLayer.kBackGround, CollisionPushType.kThisPush, LayerMask.NameToLayer("Collision"));

    }

    // Update is called once per frame
    void Update()
    {
    }
}
