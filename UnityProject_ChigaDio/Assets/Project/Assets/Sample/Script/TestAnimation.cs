﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAnimation : MonoBehaviour
{
    private UnitAnimation unit_animatiom_ = null;
    // Start is called before the first frame update
    void Start()
    {
        unit_animatiom_ = new UnitAnimation();
        unit_animatiom_.Init(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        unit_animatiom_.ChangeAnimation(AnimationConvert.AnimationState.Anim_Run);
    }
}
