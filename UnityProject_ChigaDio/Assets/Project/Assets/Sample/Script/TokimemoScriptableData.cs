﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Sample.TokimemoScriptableData))]
public class EditorTokimekiScriptableData : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<Sample.TokimemoData>(target as BaseExcelScriptableObject<Sample.TokimemoData>);
        base.OnInspectorGUI();
    }
}


#endif

namespace Sample
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Sample/CreateTokimekiData")]
    public class TokimemoScriptableData : BaseExcelScriptableObject<TokimemoData>
    {
    }
}
