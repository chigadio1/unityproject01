﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sample
{

    public class TestMono : MonoBehaviour
    {

        [SerializeField]
        private BaseCollisionConstruction collision_;

        [SerializeField]
        protected CollisionLayer collision_type_;


        [SerializeField]
        protected CollisionPushType collision_push_type_;


        [SerializeField]
        Vector3 move;
        [SerializeField]
        float speed = 0.1f;
        [SerializeField]
        private bool is_input_ = false;


        private AddressableObject<GameObject> model = null;
        // Start is called before the first frame update
        void Start()
        {
            model = new AddressableObject<GameObject>("Assets/Project/Assets/Model/Chara/0002/Prefab/0002_Model.prefab");
            model.LoadStart();
            collision_ = new BaseCollisionConstruction();
            collision_.Init(gameObject, collision_type_, collision_push_type_, LayerMask.NameToLayer("Player"));


        }

        // Update is called once per frame
        void Update()
        {

            model.Instance();
         
            if (is_input_ == true)
            {
                move = Vector3.zero;
                if(Input.GetKey(KeyCode.LeftArrow))
                {
                    move.x = 1;
                }

                transform.position += (move * speed) * Time.deltaTime;
            }
            else
            {


                transform.position += (move * speed) * Time.deltaTime;
            }


        }

        void Enter(Collider col)
        {
            Debug.Log(col.gameObject);
        }

        void Second(Collider col)
        {
            Debug.LogWarning(col.gameObject);
        }

        void Release(Collider col)
        {
            Debug.LogError(col.gameObject);
        }

    }
}
