﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sample
{
    /// <summary>
    /// サンプルデータ
    /// </summary>
    [System.Serializable]
    public class TokimemoData : BaseComposition
    {
        /// <summary>
        /// 名前
        /// </summary>
        [SerializeField]
        private string girl_name_ = "";
        public string GetGirlName() { return girl_name_; }

        /// <summary>
        /// 誕生月
        /// </summary>
        [SerializeField]
        private int birth_month_ = 0;
        public int GetBirthMonth() { return birth_month_; }

        /// <summary>
        /// 誕生日
        /// </summary>
        [SerializeField]
        private int birth_day_ = 0;
        public int GetBirthDay() { return birth_day_; }

        /// <summary>
        /// 身長
        /// </summary>
        [SerializeField]
        private float height_ = 0;
        public float GetHeight() { return height_; }

        /// <summary>
        /// 体重
        /// </summary>
        [SerializeField]
        private float weight_ = 0;
        public float GetWeight() { return weight_; }

        /// <summary>
        /// 胸
        /// </summary>
        [SerializeField]
        private float breast_ = 0;
        public float GetBreast() { return breast_; }

        /// <summary>
        /// 腰
        /// </summary>
        [SerializeField]
        private float　waist_ = 0;
        public float GetWaist() { return waist_; }

        /// <summary>
        /// 尻
        /// </summary>
        [SerializeField]
        private float hip_ = 0;
        public float GetHip() { return hip_; }


        public override void ManualSetUp(ref DataFrameGroup data_group, ref ProjectSystem.ExcelSystem.DataGroup excel_data_group)
        {
            data_group.AddData("名前", nameof(girl_name_), girl_name_);
            data_group.AddData("誕生月", nameof(birth_month_), birth_month_);
            data_group.AddData("誕生日", nameof(birth_day_), birth_day_);
            data_group.AddData("身長", nameof(height_), height_);
            data_group.AddData("体重", nameof(weight_), weight_);
            data_group.AddData("B", nameof(breast_), breast_);
            data_group.AddData("W", nameof(waist_), waist_);
            data_group.AddData("H", nameof(hip_), hip_);
        }
    }
}
