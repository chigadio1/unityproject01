﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextCollision : MonoBehaviour
{

    /// <summary>
    /// コリジョン
    /// </summary>
    [SerializeField]
    protected BaseCollisionConstruction collision_;

    public Vector3 add_position_ = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        collision_ = new BaseCollisionConstruction();
        collision_.Init(gameObject, CollisionLayer.kPlayer, CollisionPushType.kThisPush, LayerMask.NameToLayer("Player"));
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += add_position_;
    }
}
