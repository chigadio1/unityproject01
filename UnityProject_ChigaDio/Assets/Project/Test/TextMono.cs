﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMono : MonoBehaviour
{
    private NovelDetailsManager manager_ = new NovelDetailsManager();
    // Start is called before the first frame update
    void Start()
    {
        manager_.SetUp(gameObject);
        manager_.Play(1);
    }

    // Update is called once per frame
    void Update()
    {
        manager_.Update();
    }
}
