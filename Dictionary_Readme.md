# Dictionary
* [Readme](https://gitlab.com/chigadio1/unityproject01/-/blob/main/README.md)

## 概要
キャラクターのIDや、エフェクトのIDなどをすべて纏め保存し、いつでもID参照できる設計。<br>
Dictionaryから参照する利点は、必ず存在しているIDを取得できることや、Excelでデータを纏められるため見やすさもあります。<br>

## Script
* DictionaryType-> 辞書種類クラス(キャラクター、エフェクトのIDを纏められる)
* DictionaryIntensive -> DictionaryTypeを纏め管理するクラス(ここから、キャラクター、エフェクトのIDが取得できる)
* DictionaryAccessor -> IDやNameを検索できるStaticクラス
* DataCore -> DictionaryIntensiveがあり、シングルトンクラスでどこからでもアクセルできる(DictionaryAccessorはここから情報が取得できる)

