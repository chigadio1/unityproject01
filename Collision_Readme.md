# Collision
* [Readme](https://gitlab.com/chigadio1/unityproject01/-/blob/main/README.md)

## 概要
物理を使わない当たり判定を使用したいため、Unityで使う当たり判定の押し出し計算を利用して、独自でコリジョン制御できるようにした。<br>
物理に依存しないアクションゲームなどで使いたいため作成。

## Script
* RpgCollisionCalculation -> 押し出しの計算をするStaticClass
* BaseCollisionConstruction -> 独自のコリジョン情報を持つクラス
* RpgCollisionDetails -> コリジョンの詳細情報を保持し、更新するクラス(座標やコリジョンを所有しているオブジェクト)
* RpgCollisionDetailsAccessor -> RpgCollisionDetailsのアクセサー
* RpgCollisionHit -> ヒットフラグの管理
* RpgCollisionHitFlag -> ヒットフラグ(当たり続ける、当たった瞬間、離れた瞬間)
* RpgCollisionPosition -> コリジョン座標(前回と今の座標を計算や保持など)
* RpgCollisionDetailsControl -> RpgCollisionDetailsを管理するClass(複数コリジョンがある想定でも使えるように)
* RpgCollisionControl -> 全コリジョンを保持し管理するClass(ここの更新で、ヒット判定や押し出しをする)
* RpgCollisionConnection -> RpgCollisionControlに接続し情報が取れるStaticClass
* CollisionLayer,CollisionPushType -> コリジョンタイプ、押し出しタイプClass

## Tutorial

### Step1 コリジョン作成
以下の画像のように、Sample_001という空オブジェクトの子として、コリジョンのオブジェクトを作り、従来通りColliderを設定してください<br>(*RigidBodyはいりません)<br>
![SampleCollision](/uploads/deeafa5abedfb2f2a62da2d0983179c4/SampleCollision.png)

### Step2 スクリプトでコリジョン指定
仮でいいので、MonoBehaviourを継承したClassを作り、以下のようなのを作ってください

```c#:TestMono.cs
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sample
{

    public class TestMono : MonoBehaviour
    {

        [SerializeField]
        private BaseCollisionConstruction collision_;

        [SerializeField]
        protected CollisionLayer collision_type_;


        [SerializeField]
        protected CollisionPushType collision_push_type_;


        [SerializeField]
        Vector3 move;
        [SerializeField]
        float speed = 0.1f;
        [SerializeField]
        private bool is_input_ = false;
        // Start is called before the first frame update
        void Start()
        {
            collision_ = new BaseCollisionConstruction();
            collision_.Init(gameObject, collision_type_, collision_push_type_, LayerMask.NameToLayer("Player"));
            collision_.RpgCollisionDetailsControl.HitEnterFunc = Enter;
        }

        // Update is called once per frame
        void Update()
        {
            if (is_input_ == true)
            {
                move = Vector3.zero;
                if(Input.GetKey(KeyCode.LeftArrow))
                {
                    move.x = 1;
                }

                transform.position += (move * speed) * Time.deltaTime;
            }
            else
            {


                transform.position += (move * speed) * Time.deltaTime;
            }
        }

        void Enter(Collider col)
        {
            Debug.Log(col.gameObject);
        }

    }
}
```
<br>
一番大事なのは、BaseCollisionConstruction CollisionLayer CollisionPushTypeの変数をもっていることです。<br>
StartでBaseCollisionConstructionをnewし、Init関数を呼び出しておりますが、引数は<br>
コリジョンを保持している大本(今回はSample_001)のGameObject,押し出しタイプ、コリジョンのレイヤータイプ、コリジョンが設定されているレイヤー名指定(画像だとCubeのレイヤー名を指定ください。今回はPlayerレイヤーを作ったため、その名前で検索するようにしています)<br>

### Step3 Inspectorでコリジョンタイプを選ぶ
Sample_001のインスペクターで、以下の画像のように設定。<br>
![CollisionInspector](/uploads/7138a29c0756f3618610ec5a3ef38974/CollisionInspector.png)
<br><br>
CollisionLayerは、コリジョンの計算順番でPlayerかEnemyかなど選びます。<br>
CollisionPushTypeは押し出しタイプ指定なので、今回は当たったら自分自身を押し出しするようにします<br>

![CollisionTest](/uploads/ade4b4645f781dbc1094cd9c116e8cfa/CollisionTest.gif)

### Step4 デリゲートを使用しよう!
<br>また、以下のように先ほどのスクリプトを修正すれば、従来のコリジョンの判定をスクリプトできるようになります。

```c#:TestMono.cs
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sample
{

    public class TestMono : MonoBehaviour
    {

        [SerializeField]
        private BaseCollisionConstruction collision_;

        [SerializeField]
        protected CollisionLayer collision_type_;


        [SerializeField]
        protected CollisionPushType collision_push_type_;


        [SerializeField]
        Vector3 move;
        [SerializeField]
        float speed = 0.1f;
        [SerializeField]
        private bool is_input_ = false;
        // Start is called before the first frame update
        void Start()
        {
            collision_ = new BaseCollisionConstruction();
            collision_.Init(gameObject, collision_type_, collision_push_type_, LayerMask.NameToLayer("Player"));
            collision_.RpgCollisionDetailsControl.HitEnterFunc = Enter;
            collision_.RpgCollisionDetailsControl.HitSecondFunc = Enter;
            collision_.RpgCollisionDetailsControl.HitReleaseFunc= Enter;
        }

        // Update is called once per frame
        void Update()
        {
            if (is_input_ == true)
            {
                move = Vector3.zero;
                if(Input.GetKey(KeyCode.LeftArrow))
                {
                    move.x = 1;
                }

                transform.position += (move * speed) * Time.deltaTime;
            }
            else
            {


                transform.position += (move * speed) * Time.deltaTime;
            }
        }

        void Enter(Collider col)
        {
            Debug.Log(col.gameObject);
        }

        void Second(Collider col)
        {
            Debug.LogWarning(col.gameObject);
        }

        void Release(Collider col)
        {
            Debug.LogError(col.gameObject);
        }

    }
}

```
<br>デリゲートを使ってますので、簡単にできるようになっております。
