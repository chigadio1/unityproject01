# MotionParameter(Ver_Alpha)
* [Readme](https://gitlab.com/chigadio1/unityproject01/-/blob/main/README.md)
## 使用したもの
* AddressableSystem
* Excel_Load

## 概要
簡単にアクション動作をスクリプトで複雑に組むのではなく、動きやコリジョン作成などの部分をデータ化し動作できるように。<br>
無双系、NiaRなどのシステムを参考に作成。


## Script

## ActionName
* Attack_Combo_A      // 攻撃A
* Attack_Combo_B      // 攻撃B
* Attack_Combo_C      // 攻撃C
* Damage_Reaction_S   // ダメージリアクション(小)

## ActionRole
* MotionData,MotionDataScriptable -> Excelで使用したスクリプト(データ集)
* MotionParameterConvert -> 変換用StaticClass。名前からActionIDを取れるように。
* MotionParameterDetails -> データを保存するClas。ターゲットオブジェクトなどの設定ができる。
* MotionParameterDetailsData ->　MotionDataから情報を受け取る(中身は一緒)
* MotionParameterDetailsDataControl -> 各ActionNameのデータを保存しているClass
* BaseMotionParameterRoleDataControl -> モーションパラメータのデータを保持し計算するClass
* BaseMotionParameterRolePerformance -> BaseMotionParameterRoleDataControlからデータを取得し、ゲーム上に反映するClass
* MotionParameterRoleDataControl -> BaseMotionParameterRoleDataControlをListにし管理するClass
* MotionParameterRoleDataControlManagement -> MotionParameterRoleDataControlを保持し、キーフレームで回してアクションデータを保持するClass
* MotionParameterRolePerformanceControl -> BaseMotionParameterRolePerformanceをListにし、ゲーム上に反映するClass
* MotionParameter -> MotionParameterRoleDataControlManagement,MotionParameterRolePerformanceControlを保持する大本Class

### Transform_Position
移動できるアクション。オブジェクトが向いている方向に合わせて、x,y,zを移動できる。
#### 引数List
* Int01 -> 座標移動タイプ(0:X 1:Y 2:Z)
* Int02 ->  なし
* Int03 ->  なし
* Int04 ->　なし
* Float01 -> 移動量(1Fで動く移動量)
* Float02 -> なし
* Float03 -> なし
* Float04 -> なし
* String01 -> なし
* String02 -> なし

### Transform_TeleportPosition
ターゲットがいた場合、そのターゲットに瞬間移動できるアクション。
#### 引数List
* Int01 ->  なし
* Int02 ->  なし
* Int03 ->  なし
* Int04 ->　なし
* Float01 -> ターゲットの瞬間移動距離
* Float02 -> なし
* Float03 -> なし
* Float04 -> なし
* String01 -> なし
* String02 -> なし

### Animation_Change
アニメーションの切り替えアクション
#### 引数List
* Int01 ->  アニメーションID
* Int02 ->  なし
* Int03 ->  なし
* Int04 ->　なし
* Float01 -> なし
* Float02 -> なし
* Float03 -> なし
* Float04 -> なし
* String01 -> なし
* String02 -> なし

### Transform_Target_LookRotation
目標に向かい、瞬間的に回転する
#### 引数List
* Int01 ->  なし
* Int02 ->  なし
* Int03 ->  なし
* Int04 ->　なし
* Float01 -> なし
* Float02 -> なし
* Float03 -> なし
* Float04 -> なし
* String01 -> なし
* String02 -> なし

### Transform_Target_Homing
目標に向かい、指定した角度ずつ回転する
#### 引数List
* Int01 ->  なし
* Int02 ->  なし
* Int03 ->  なし
* Int04 ->　なし
* Float01 -> 1フレームに回転する角度
* Float02 -> なし
* Float03 -> なし
* Float04 -> なし
* String01 -> なし
* String02 -> なし

### Collision_Attack
攻撃コリジョンを生成
#### 引数List
* Int01 ->  コリジョンID
* Int02 ->  なし
* Int03 ->  なし
* Int04 ->　なし
* Float01 -> 生成の座標からXにおく値
* Float02 ->　生成の座標からyにおく値
* Float03 -> 生成の座標からzにおく値
* Float04 -> なし
* String01 -> なし
* String02 -> なし

### Effect_Generate
エフェクト生成
#### 引数List
* Int01 ->  エフェクトID
* Int02 ->  なし
* Int03 ->  なし
* Int04 ->　なし
* Float01 -> 生存フレーム
* Float02 ->　生成の座標からxにおく値
* Float03 -> 生成の座標からyにおく値
* Float04 -> 生成の座標からzにおく値
* String01 -> なし
* String02 -> なし

## 実装予定
* ~~Transform_Target_LookRotation //瞬間回転(ターゲットに向く)~~(実装済み)
* ~~Transform_Target_Homing       //ターゲットに向かって徐々に回転~~(実装済み)
* ~~Collision_Attack              //攻撃コリジョン作成~~(実装済み)
* ~~Collision_Sensing             //感知コリジョン作成~~(仮実装済み(中身はまだ決めておりません))

## Update
* 2021/07/19 ->　各モーションパラメータのアクションで、指定したフレーム分稼働するよう修正
* 2021/07/19 ->  effectの生成アクションを追加



## Tutorial(テスト版)

### Step1 データ作成
以下の画像のように、Excelでデータを作成しExcel_LoadのReadmeを参照しデータを作成してください。<br>
![Excel_ActionData](/uploads/b07ebeee7e029bd0d5fbd5ac0d9ef833/Excel_ActionData.png)

<br>データを作ったら、必ずAddressableの名前をどこかに保存し覚えてください。この名前は使ってロードできます。
<br>画像の一番右上の部分
![AddressableName](/uploads/822260d23ccb4b92702ffd29e2a855c2/AddressableName.png)

### Step2 BattleUnitを使って再生
ScriptでBattleUnitとというのがありますので、そちらを使ってMotionParameterの再生を行います。<br>
```c#:BattleUnit.cs
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// バトルユニット
/// </summary>
public class BattleUnit : BaseUnit
{
    MotionParameter motion_parameter_ = new MotionParameter();

    [SerializeField]
    private GameObject test_target_ = null;
    // Start is called before the first frame update
    void Start()
    {
        motion_parameter_.Init(gameObject, "Assets/Project/Assets/Data/MotionData/0001/0001_Combo.asset", this);
        unit_animation_ = new UnitAnimation();
        unit_animation_.Init(gameObject);
        motion_parameter_.SetTargetGameObject(test_target_);
    }

    // Update is called once per frame
    void Update()
    {
        motion_parameter_.PlayAction((int)MotionParameterConvert.MotionActionName.Attack_Combo_A);
        motion_parameter_.Update(this);
    }
}

```

<br>Attack_Combo_Aを再生するように指示していますので、先ほどExcelだとアニメーションを再生し、6フレーム後に前に前進し600フレームで終了するようになってます。<br>
![MotionParameter_Test](/uploads/52be4347bcebbf37496864147db92155/MotionParameter_Test.gif)

## Alpha_Sample

<br>
以下の画像のようにExcelでデータを作り、一連の流れを作ったものです。<br>
![Alpha_MotionParameter](/uploads/aaca0d9b05bce48edace48763bee6a3e/Alpha_MotionParameter.png)
<br>

![Sample_Video](https://gitlab.com/chigadio1/unityproject01/-/blob/main/ReadmeImage/Alpah_MotionParameter.mp4)
<br>Sampel_Videoをクリックすれば、動画があるページに飛びます。
