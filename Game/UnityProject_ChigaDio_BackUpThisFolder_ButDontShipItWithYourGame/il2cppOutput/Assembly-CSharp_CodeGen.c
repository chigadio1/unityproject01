﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void AnimatorMoveEvent::.ctor()
extern void AnimatorMoveEvent__ctor_m4CF537CC75E3B901B2E42BB16F13D56FE93A6D23 (void);
// 0x00000002 System.Void Gravity::Update()
extern void Gravity_Update_m3F26C341A7C867384B6F6F1F79917BA88A9352F8 (void);
// 0x00000003 System.Void Gravity::.ctor()
extern void Gravity__ctor_mCE9306F61EE4AA3FD6BCB49955BD241D70FFFDC0 (void);
// 0x00000004 System.Void PlayerCamera::Start()
extern void PlayerCamera_Start_m82AB289AD7CDCCE13772830C711CCD7C7FC835F6 (void);
// 0x00000005 System.Void PlayerCamera::LateUpdate()
extern void PlayerCamera_LateUpdate_m63AE5C85B32C3BE1C76CA69E89B1F383E2D9C159 (void);
// 0x00000006 System.Void PlayerCamera::.ctor()
extern void PlayerCamera__ctor_mEC7D02708FA0EE1799559A768E8CA74A42E3C26F (void);
// 0x00000007 System.Void PlayerInputController::Start()
extern void PlayerInputController_Start_m7E1EA7300A0ABAB5BFBE64B03C6C308E4842CC14 (void);
// 0x00000008 System.Void PlayerInputController::Update()
extern void PlayerInputController_Update_m87BB74A84FB5ACED4D74DE2C4870402DA3B53D44 (void);
// 0x00000009 System.Void PlayerInputController::.ctor()
extern void PlayerInputController__ctor_m4D6570072769159A1458FB4FB7CE42C3B82F4B81 (void);
// 0x0000000A UnityEngine.Vector3 PlayerMachine::get_lookDirection()
extern void PlayerMachine_get_lookDirection_m22611B931E75B30FD267FA7D46C94E71E4A1D0AB (void);
// 0x0000000B System.Void PlayerMachine::set_lookDirection(UnityEngine.Vector3)
extern void PlayerMachine_set_lookDirection_m982B1A55B493E1702E5569D6698D532406491E51 (void);
// 0x0000000C System.Void PlayerMachine::Start()
extern void PlayerMachine_Start_m29E18119D1199DBEFACE854873FBA5CDB4571A44 (void);
// 0x0000000D System.Void PlayerMachine::EarlyGlobalSuperUpdate()
extern void PlayerMachine_EarlyGlobalSuperUpdate_mF99AD5B0952F1B2E1E01C6C9A8AF4FAF9F6FE036 (void);
// 0x0000000E System.Void PlayerMachine::LateGlobalSuperUpdate()
extern void PlayerMachine_LateGlobalSuperUpdate_m19A2A81CC8FE1CA798BB45F96940B3693CDE5218 (void);
// 0x0000000F System.Boolean PlayerMachine::AcquiringGround()
extern void PlayerMachine_AcquiringGround_mA904C0126801D9CB3A70C82EE49D5F9F71BA7D70 (void);
// 0x00000010 System.Boolean PlayerMachine::MaintainingGround()
extern void PlayerMachine_MaintainingGround_m4CFE4EC092FFDEB8EEE74ECC0231392725C7638A (void);
// 0x00000011 System.Void PlayerMachine::RotateGravity(UnityEngine.Vector3)
extern void PlayerMachine_RotateGravity_m1BF3C223D1EC4E278AE663F32F1EA021A12B6CD5 (void);
// 0x00000012 UnityEngine.Vector3 PlayerMachine::LocalMovement()
extern void PlayerMachine_LocalMovement_m3676E72DF892292E85836E0BE0BB7B96CCA73CA9 (void);
// 0x00000013 System.Single PlayerMachine::CalculateJumpSpeed(System.Single,System.Single)
extern void PlayerMachine_CalculateJumpSpeed_m63F7B8B35F37A474CB177D6C2C087605468BB64A (void);
// 0x00000014 System.Void PlayerMachine::Idle_EnterState()
extern void PlayerMachine_Idle_EnterState_m4FB783C114BF66FA8C15B3B7C6E48ACC86C50A21 (void);
// 0x00000015 System.Void PlayerMachine::Idle_SuperUpdate()
extern void PlayerMachine_Idle_SuperUpdate_m276FACF0FB8B33E6F3DFCFE8DFC8AAB7737BC921 (void);
// 0x00000016 System.Void PlayerMachine::Idle_ExitState()
extern void PlayerMachine_Idle_ExitState_m0CD95C239192AB0036C31338C71B181A2D7CD31A (void);
// 0x00000017 System.Void PlayerMachine::Walk_SuperUpdate()
extern void PlayerMachine_Walk_SuperUpdate_mED9705562ADAF6E83F6AF0B7DDBA83D929865184 (void);
// 0x00000018 System.Void PlayerMachine::Jump_EnterState()
extern void PlayerMachine_Jump_EnterState_mE058669471ACF2F519FD076E43A6EE29F0A1B265 (void);
// 0x00000019 System.Void PlayerMachine::Jump_SuperUpdate()
extern void PlayerMachine_Jump_SuperUpdate_mA59CE36B7156F91E60575B92AFBEFF5FEE81BA4D (void);
// 0x0000001A System.Void PlayerMachine::Fall_EnterState()
extern void PlayerMachine_Fall_EnterState_m88BD4E64D1102B4AF23DFAF8BE5C1620C4B01311 (void);
// 0x0000001B System.Void PlayerMachine::Fall_SuperUpdate()
extern void PlayerMachine_Fall_SuperUpdate_mA9A8968D79B743E8B03D90E08BE5AD323B470413 (void);
// 0x0000001C System.Void PlayerMachine::.ctor()
extern void PlayerMachine__ctor_mFB76217EE0E65F0C5D8F02DE88E8B939969B3611 (void);
// 0x0000001D System.Void PlayerMachineDebug::Awake()
extern void PlayerMachineDebug_Awake_mBFB9CD13E55C3CD2905524BAB4AB1D6744A6D921 (void);
// 0x0000001E System.Void PlayerMachineDebug::OnGUI()
extern void PlayerMachineDebug_OnGUI_m39D8235D6159982339822EB3B0946B3AE62EB011 (void);
// 0x0000001F System.Void PlayerMachineDebug::.ctor()
extern void PlayerMachineDebug__ctor_mE58BDBCEADC357269199913D6CC4368394C85F96 (void);
// 0x00000020 System.Void SimpleStateMachine::OnGUI()
extern void SimpleStateMachine_OnGUI_m73F3DD6B06CA64A695E4DE96BD8969FFD26F2A96 (void);
// 0x00000021 System.Enum SimpleStateMachine::get_currentState()
extern void SimpleStateMachine_get_currentState_mF623DA6531D792B45BE168DE37F8FF79CD8CE868 (void);
// 0x00000022 System.Void SimpleStateMachine::set_currentState(System.Enum)
extern void SimpleStateMachine_set_currentState_mAB779E4E78092DAF4E8937194C736F2D82FAF771 (void);
// 0x00000023 System.Void SimpleStateMachine::ChangingState()
extern void SimpleStateMachine_ChangingState_mB80E5504E17DD09F593279E82A8960785E0100AB (void);
// 0x00000024 System.Void SimpleStateMachine::ConfigureCurrentState()
extern void SimpleStateMachine_ConfigureCurrentState_mF2D435CC444FF954CFCD0CB41164AD254A8B0A70 (void);
// 0x00000025 T SimpleStateMachine::ConfigureDelegate(System.String,T)
// 0x00000026 System.Void SimpleStateMachine::Update()
extern void SimpleStateMachine_Update_m4433D56185EB32990DB65BE9ECFECABF0A3803F7 (void);
// 0x00000027 System.Void SimpleStateMachine::FixedUpdate()
extern void SimpleStateMachine_FixedUpdate_m47BCDA01586AA33827ED9CDFA026E9ABD8952D20 (void);
// 0x00000028 System.Void SimpleStateMachine::LateUpdate()
extern void SimpleStateMachine_LateUpdate_mC79C69BF865F66D3941B7C0593C74BEB89DE1596 (void);
// 0x00000029 System.Void SimpleStateMachine::EarlyGlobalSuperUpdate()
extern void SimpleStateMachine_EarlyGlobalSuperUpdate_m804C277EFD52FDB4AB492F419158CD8C1C6315BF (void);
// 0x0000002A System.Void SimpleStateMachine::LateGlobalSuperUpdate()
extern void SimpleStateMachine_LateGlobalSuperUpdate_m8F004C99B826FED6BE52DD1F1DF9A5E922D570E3 (void);
// 0x0000002B System.Void SimpleStateMachine::DoNothing()
extern void SimpleStateMachine_DoNothing_mCE49620E3865FA5F9048CADD7EEB326066DC60C0 (void);
// 0x0000002C System.Void SimpleStateMachine::.ctor()
extern void SimpleStateMachine__ctor_m557202ABAB66066393F33FF4DF68783A080D0AC7 (void);
// 0x0000002D System.Void SimpleStateMachine_State::.ctor()
extern void State__ctor_m64C836AE0CF303AA0ADDBA20966318C57965B433 (void);
// 0x0000002E System.Void BSPTree::Awake()
extern void BSPTree_Awake_mB3085EC0835714A3446013B8E9DF5DF269ACC6C3 (void);
// 0x0000002F System.Void BSPTree::Start()
extern void BSPTree_Start_m896AD5B5EDF0398EE210F08E6644C71670123E05 (void);
// 0x00000030 UnityEngine.Vector3 BSPTree::ClosestPointOn(UnityEngine.Vector3,System.Single)
extern void BSPTree_ClosestPointOn_mC73316D12D5FE601325F643F068BCC3B04E62C05 (void);
// 0x00000031 System.Void BSPTree::FindClosestTriangles(BSPTree_Node,UnityEngine.Vector3,System.Single,System.Collections.Generic.List`1<System.Int32>)
extern void BSPTree_FindClosestTriangles_m1851BEA73EA689AC46A4BF6FCB6BDA8540CFBA23 (void);
// 0x00000032 UnityEngine.Vector3 BSPTree::ClosestPointOnTriangle(System.Int32[],UnityEngine.Vector3)
extern void BSPTree_ClosestPointOnTriangle_m4ECE85FD2F27FEA7DC6E8483684219A92AFA85B8 (void);
// 0x00000033 System.Void BSPTree::BuildTriangleTree()
extern void BSPTree_BuildTriangleTree_m101B54A435C1B45C35E7588865225985F20A33D9 (void);
// 0x00000034 System.Void BSPTree::RecursivePartition(System.Collections.Generic.List`1<System.Int32>,System.Int32,BSPTree_Node)
extern void BSPTree_RecursivePartition_m5C2380883C695E20CC26A38384E5D2532AB6CA5B (void);
// 0x00000035 System.Void BSPTree::Split(System.Collections.Generic.List`1<System.Int32>,UnityEngine.Vector3,UnityEngine.Vector3,System.Collections.Generic.List`1<System.Int32>&,System.Collections.Generic.List`1<System.Int32>&)
extern void BSPTree_Split_mB6E29CE71073EFD8163FC5F90DF26F00A9F4205A (void);
// 0x00000036 System.Boolean BSPTree::PointAbovePlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void BSPTree_PointAbovePlane_mF46597D4E01D45B229AB8E8A1F54E47FB487C80D (void);
// 0x00000037 System.Single BSPTree::PointDistanceFromPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void BSPTree_PointDistanceFromPlane_m265353EC3CA9DE5215C3B5661AC63A4FA9207CA9 (void);
// 0x00000038 System.Void BSPTree::ClosestPointOnTriangleToPoint(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void BSPTree_ClosestPointOnTriangleToPoint_mC28E42DBE5F1E451ECD16D23CC9D665E5DEE759C (void);
// 0x00000039 System.Void BSPTree::DrawTriangleSet(System.Int32[],UnityEngine.Color)
extern void BSPTree_DrawTriangleSet_mB5F38185503EDF064DDFDBAAA59EE396AEA3B9C7 (void);
// 0x0000003A System.Void BSPTree::.ctor()
extern void BSPTree__ctor_mE69746BDDD4B533075DFDCAA760384FFD334D4CE (void);
// 0x0000003B System.Void BSPTree_Node::.ctor()
extern void Node__ctor_m82E5A39F45DC3AF3494A23AA12AF0575BA81CFFA (void);
// 0x0000003C System.Void BruteForceMesh::Awake()
extern void BruteForceMesh_Awake_m933DA912CE7BE2E6537B19F032049A4B13510CEB (void);
// 0x0000003D UnityEngine.Vector3 BruteForceMesh::ClosestPointOn(UnityEngine.Vector3)
extern void BruteForceMesh_ClosestPointOn_mFCE8DF9442EA847DC21170D3E140A3995B339C0D (void);
// 0x0000003E UnityEngine.Vector3 BruteForceMesh::ClosestPointOnTriangle(System.Int32[],UnityEngine.Vector3)
extern void BruteForceMesh_ClosestPointOnTriangle_m9EC0A1E4DE935758750059589F5B68B12D16EF57 (void);
// 0x0000003F System.Void BruteForceMesh::ClosestPointOnTriangleToPoint(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void BruteForceMesh_ClosestPointOnTriangleToPoint_mE877F486C3B2317A017C76FFFE86B8C4133F1176 (void);
// 0x00000040 System.Void BruteForceMesh::.ctor()
extern void BruteForceMesh__ctor_m1F09509780DAA44B7D6CBE01806DF989AF905578 (void);
// 0x00000041 System.Single SuperCharacterController::get_deltaTime()
extern void SuperCharacterController_get_deltaTime_m4B1717B6D963AC19D88C7C44D6D40C79D90693D2 (void);
// 0x00000042 System.Void SuperCharacterController::set_deltaTime(System.Single)
extern void SuperCharacterController_set_deltaTime_mDEE5BF0E7A708A32B2DA5999637273D310096E4F (void);
// 0x00000043 SuperCharacterController_SuperGround SuperCharacterController::get_currentGround()
extern void SuperCharacterController_get_currentGround_mB2132C21D00976C32CF8C7EEE5BC9F71728A53D9 (void);
// 0x00000044 System.Void SuperCharacterController::set_currentGround(SuperCharacterController_SuperGround)
extern void SuperCharacterController_set_currentGround_m0510FA181E9366A157C24F9BF6A3A21D6DF2DBBD (void);
// 0x00000045 CollisionSphere SuperCharacterController::get_feet()
extern void SuperCharacterController_get_feet_mAE07373444C1B61902863383345B8E402779059E (void);
// 0x00000046 System.Void SuperCharacterController::set_feet(CollisionSphere)
extern void SuperCharacterController_set_feet_m50C50603EF4C8C5320F5A1AF0BBDA99A641649CC (void);
// 0x00000047 CollisionSphere SuperCharacterController::get_head()
extern void SuperCharacterController_get_head_m13C74CB339AE3B2370EBF90AAA600C2726B6A6D2 (void);
// 0x00000048 System.Void SuperCharacterController::set_head(CollisionSphere)
extern void SuperCharacterController_set_head_mEB5C9D0B8F8A30EB3247CB26D9C478E9A6DA948C (void);
// 0x00000049 System.Single SuperCharacterController::get_height()
extern void SuperCharacterController_get_height_mC02590B4097E19329BA5755139A9E89E4F241DCD (void);
// 0x0000004A UnityEngine.Vector3 SuperCharacterController::get_up()
extern void SuperCharacterController_get_up_mBB7061D99C5B6BF348BDC84B4B28E5E21615E93E (void);
// 0x0000004B UnityEngine.Vector3 SuperCharacterController::get_down()
extern void SuperCharacterController_get_down_m237E6665B7F7B91275CE8B4BC1643F5114CD428D (void);
// 0x0000004C System.Collections.Generic.List`1<SuperCollision> SuperCharacterController::get_collisionData()
extern void SuperCharacterController_get_collisionData_mA67D64ACEF134851999B3258C1543D10105C404D (void);
// 0x0000004D System.Void SuperCharacterController::set_collisionData(System.Collections.Generic.List`1<SuperCollision>)
extern void SuperCharacterController_set_collisionData_m91786FAE60DB3345EE983622EBD8162F8EE39E78 (void);
// 0x0000004E UnityEngine.Transform SuperCharacterController::get_currentlyClampedTo()
extern void SuperCharacterController_get_currentlyClampedTo_mD50E9D486A93FBD6524704AC146518B892D91E6D (void);
// 0x0000004F System.Void SuperCharacterController::set_currentlyClampedTo(UnityEngine.Transform)
extern void SuperCharacterController_set_currentlyClampedTo_m8730779A4BA87DCDDA87B908B5D76A8A7BDBEFC7 (void);
// 0x00000050 System.Single SuperCharacterController::get_heightScale()
extern void SuperCharacterController_get_heightScale_mE7962B085838953382B8D570D39E6A0ACB793E53 (void);
// 0x00000051 System.Void SuperCharacterController::set_heightScale(System.Single)
extern void SuperCharacterController_set_heightScale_m9B9FA91DA93838E42C33DF84C176FF586A806F78 (void);
// 0x00000052 System.Single SuperCharacterController::get_radiusScale()
extern void SuperCharacterController_get_radiusScale_m3DEB0666E2CACB226AD8A61899BC7575292FAE26 (void);
// 0x00000053 System.Void SuperCharacterController::set_radiusScale(System.Single)
extern void SuperCharacterController_set_radiusScale_mDF7CD58D84A40F55F816EA23FAA6A0CA3BBE1FD6 (void);
// 0x00000054 System.Boolean SuperCharacterController::get_manualUpdateOnly()
extern void SuperCharacterController_get_manualUpdateOnly_mDA89864515B45AA8BCB2FA0808CC78EDD3B0E91E (void);
// 0x00000055 System.Void SuperCharacterController::set_manualUpdateOnly(System.Boolean)
extern void SuperCharacterController_set_manualUpdateOnly_mD4CA74FFEBDF33B8CCF4231BC299DEF1AA5C97E5 (void);
// 0x00000056 System.Void SuperCharacterController::add_AfterSingleUpdate(SuperCharacterController_UpdateDelegate)
extern void SuperCharacterController_add_AfterSingleUpdate_m64828D285C315ED77004B726B727099EE426C31D (void);
// 0x00000057 System.Void SuperCharacterController::remove_AfterSingleUpdate(SuperCharacterController_UpdateDelegate)
extern void SuperCharacterController_remove_AfterSingleUpdate_m367F0FB219BC87378E09AC5031526767A93D6A89 (void);
// 0x00000058 System.Void SuperCharacterController::Awake()
extern void SuperCharacterController_Awake_m8EE1F24EED8FF3907BFECD036AADF864A2C53558 (void);
// 0x00000059 System.Void SuperCharacterController::Update()
extern void SuperCharacterController_Update_m2C1E011366D2682253D29D1FEDD533EFB802E059 (void);
// 0x0000005A System.Void SuperCharacterController::ManualUpdate(System.Single)
extern void SuperCharacterController_ManualUpdate_mB58C0CB410ED0D4F7675238EE0D94FDA420157BC (void);
// 0x0000005B System.Void SuperCharacterController::SingleUpdate()
extern void SuperCharacterController_SingleUpdate_mBA8DBAA0C232987C95B4A999FCF232C0FCFB7625 (void);
// 0x0000005C System.Void SuperCharacterController::ProbeGround(System.Int32)
extern void SuperCharacterController_ProbeGround_mB8B7994CA2781090FF01E555F122013B9F1EB04A (void);
// 0x0000005D System.Boolean SuperCharacterController::SlopeLimit()
extern void SuperCharacterController_SlopeLimit_m80D890535CF1499C95012CBE2BB7728490BA37BA (void);
// 0x0000005E System.Void SuperCharacterController::ClampToGround()
extern void SuperCharacterController_ClampToGround_mA18B2AC04A4A1D062FA28E76DF414C0CF34E0C08 (void);
// 0x0000005F System.Void SuperCharacterController::EnableClamping()
extern void SuperCharacterController_EnableClamping_m0366ADA4BB9D7A2AA4214E5C508C31AD1F7B9CF2 (void);
// 0x00000060 System.Void SuperCharacterController::DisableClamping()
extern void SuperCharacterController_DisableClamping_m7D34CDAB1A2AEF9C8E61C380204766F64E2A5328 (void);
// 0x00000061 System.Void SuperCharacterController::EnableSlopeLimit()
extern void SuperCharacterController_EnableSlopeLimit_m243D5CC6FABC7B4C0DE7B8C7035C23F761CA48BD (void);
// 0x00000062 System.Void SuperCharacterController::DisableSlopeLimit()
extern void SuperCharacterController_DisableSlopeLimit_m79847B0600319D58623FE3A057863F641EF1D41E (void);
// 0x00000063 System.Boolean SuperCharacterController::IsClamping()
extern void SuperCharacterController_IsClamping_m6758DB8AD0B3299DC996D18DA35ED45DC96A5A26 (void);
// 0x00000064 System.Void SuperCharacterController::RecursivePushback(System.Int32,System.Int32)
extern void SuperCharacterController_RecursivePushback_mACF618F9C4A105A170A3B66DD89CA0B002E01220 (void);
// 0x00000065 System.Void SuperCharacterController::PushIgnoredColliders()
extern void SuperCharacterController_PushIgnoredColliders_m645C54EFEC0ADA624ACE4989B00C87FA8483EFDE (void);
// 0x00000066 System.Void SuperCharacterController::PopIgnoredColliders()
extern void SuperCharacterController_PopIgnoredColliders_m0E9F1294732E9EFC1AD931BAE6ACEEF4DE6BE194 (void);
// 0x00000067 System.Void SuperCharacterController::OnDrawGizmos()
extern void SuperCharacterController_OnDrawGizmos_mD1F90083B67DB1A260C3AC7F3EB2041C00BE5B77 (void);
// 0x00000068 UnityEngine.Vector3 SuperCharacterController::SpherePosition(CollisionSphere)
extern void SuperCharacterController_SpherePosition_m70EB65363266470FA633544AE301113A39CEAF16 (void);
// 0x00000069 System.Boolean SuperCharacterController::PointBelowHead(UnityEngine.Vector3)
extern void SuperCharacterController_PointBelowHead_mD6410CFC7909693DD11E3C599DCE45AEB8A9CC6D (void);
// 0x0000006A System.Boolean SuperCharacterController::PointAboveFeet(UnityEngine.Vector3)
extern void SuperCharacterController_PointAboveFeet_mC7C358C7AF52C7FF6365EC70E675A449AA20A8B0 (void);
// 0x0000006B System.Void SuperCharacterController::IgnoreCollider(UnityEngine.Collider)
extern void SuperCharacterController_IgnoreCollider_mFFF3DF2CA22C6C75E7FA93A729F67AD4EB84B08C (void);
// 0x0000006C System.Void SuperCharacterController::RemoveIgnoredCollider(UnityEngine.Collider)
extern void SuperCharacterController_RemoveIgnoredCollider_mBE71B551F84F4C62EF4CB483076C1A667758DECD (void);
// 0x0000006D System.Void SuperCharacterController::ClearIgnoredColliders()
extern void SuperCharacterController_ClearIgnoredColliders_mC74409A2F8912286B433551A724342F601884D97 (void);
// 0x0000006E System.Void SuperCharacterController::.ctor()
extern void SuperCharacterController__ctor_m63390420E07A777BA78E97A2E62D99D07477B1A6 (void);
// 0x0000006F UnityEngine.RaycastHit SuperCharacterController_Ground::get_hit()
extern void Ground_get_hit_mCDBAC5B9F772B2E150B99D546B52AB6C22F05CB9_AdjustorThunk (void);
// 0x00000070 System.Void SuperCharacterController_Ground::set_hit(UnityEngine.RaycastHit)
extern void Ground_set_hit_mA6C1C6158A183FF3877531BDA67E1086AEF842F1_AdjustorThunk (void);
// 0x00000071 UnityEngine.RaycastHit SuperCharacterController_Ground::get_nearHit()
extern void Ground_get_nearHit_m598B18C65FE5795A3D247E1635379C7540C8FE8F_AdjustorThunk (void);
// 0x00000072 System.Void SuperCharacterController_Ground::set_nearHit(UnityEngine.RaycastHit)
extern void Ground_set_nearHit_mE42C9DA3AF9F83A00E690111990F9D392D420162_AdjustorThunk (void);
// 0x00000073 UnityEngine.RaycastHit SuperCharacterController_Ground::get_farHit()
extern void Ground_get_farHit_m8D2E3316676770CE4C8C630636A402A9573DAC06_AdjustorThunk (void);
// 0x00000074 System.Void SuperCharacterController_Ground::set_farHit(UnityEngine.RaycastHit)
extern void Ground_set_farHit_m77928112BF9AEC65022C84A1787201A32D43C0E6_AdjustorThunk (void);
// 0x00000075 UnityEngine.RaycastHit SuperCharacterController_Ground::get_secondaryHit()
extern void Ground_get_secondaryHit_m330ACA2CD1D0E381A78C82DB59BE01F45AFD2214_AdjustorThunk (void);
// 0x00000076 System.Void SuperCharacterController_Ground::set_secondaryHit(UnityEngine.RaycastHit)
extern void Ground_set_secondaryHit_m1B485989BFB235D4A70CD88A8A2702A8420FC333_AdjustorThunk (void);
// 0x00000077 SuperCollisionType SuperCharacterController_Ground::get_collisionType()
extern void Ground_get_collisionType_m681F70A85A656A8EA0043ECA6AE8051D8B7C10A5_AdjustorThunk (void);
// 0x00000078 System.Void SuperCharacterController_Ground::set_collisionType(SuperCollisionType)
extern void Ground_set_collisionType_mFDE77082DCA8DFBE5B76692957ACA9821BB85C66_AdjustorThunk (void);
// 0x00000079 UnityEngine.Transform SuperCharacterController_Ground::get_transform()
extern void Ground_get_transform_m85C700E28EBB85313CBDDA9B7784EE1CBA88974D_AdjustorThunk (void);
// 0x0000007A System.Void SuperCharacterController_Ground::set_transform(UnityEngine.Transform)
extern void Ground_set_transform_mAA8A16FE791804601DBDCD66D43FA1CDC696CE2C_AdjustorThunk (void);
// 0x0000007B System.Void SuperCharacterController_Ground::.ctor(UnityEngine.RaycastHit,UnityEngine.RaycastHit,UnityEngine.RaycastHit,UnityEngine.RaycastHit,SuperCollisionType,UnityEngine.Transform)
extern void Ground__ctor_mBAF5C5D866DFBDE3651139DB4BC5EC7DDECA79CD_AdjustorThunk (void);
// 0x0000007C System.Void SuperCharacterController_UpdateDelegate::.ctor(System.Object,System.IntPtr)
extern void UpdateDelegate__ctor_m69C25AB9EA029AC2BDCEF5282E45ADF3AB3418AF (void);
// 0x0000007D System.Void SuperCharacterController_UpdateDelegate::Invoke()
extern void UpdateDelegate_Invoke_m3B1A8722CE871383E5D9D4916F5F1EFDACDDE01B (void);
// 0x0000007E System.IAsyncResult SuperCharacterController_UpdateDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void UpdateDelegate_BeginInvoke_m58E72D637AF7049B8EBB4BF5A59F3A6A0ED4AE89 (void);
// 0x0000007F System.Void SuperCharacterController_UpdateDelegate::EndInvoke(System.IAsyncResult)
extern void UpdateDelegate_EndInvoke_m17688328E5916F4C6A7513664F32904DBE519BCC (void);
// 0x00000080 System.Void SuperCharacterController_IgnoredCollider::.ctor(UnityEngine.Collider,System.Int32)
extern void IgnoredCollider__ctor_m5913C6929A7A48164018EF1DEAC0DE6E5FDD5ACC_AdjustorThunk (void);
// 0x00000081 System.Void SuperCharacterController_SuperGround::.ctor(UnityEngine.LayerMask,SuperCharacterController,UnityEngine.QueryTriggerInteraction)
extern void SuperGround__ctor_m16B316409835B253D60853B2D3E459C58FA9B5E5 (void);
// 0x00000082 SuperCollisionType SuperCharacterController_SuperGround::get_superCollisionType()
extern void SuperGround_get_superCollisionType_mA15833F8BA17B592C7EC905A3C9361AFB7A5D45D (void);
// 0x00000083 System.Void SuperCharacterController_SuperGround::set_superCollisionType(SuperCollisionType)
extern void SuperGround_set_superCollisionType_m800172A31CA12CE74A1D1A104ECE6C4AC32A2F79 (void);
// 0x00000084 UnityEngine.Transform SuperCharacterController_SuperGround::get_transform()
extern void SuperGround_get_transform_m24A16453BDEFB23A57C0209653BEAF3C62B428EC (void);
// 0x00000085 System.Void SuperCharacterController_SuperGround::set_transform(UnityEngine.Transform)
extern void SuperGround_set_transform_m2E890EE165C2918FD25663FF4B485062BA4AED17 (void);
// 0x00000086 System.Void SuperCharacterController_SuperGround::ProbeGround(UnityEngine.Vector3,System.Int32)
extern void SuperGround_ProbeGround_mEEA7A129E1C50F49D520C075176FB4E525F3AE44 (void);
// 0x00000087 System.Void SuperCharacterController_SuperGround::ResetGrounds()
extern void SuperGround_ResetGrounds_mB49F3AF4D561509AC053F36BD18883D71FC763AA (void);
// 0x00000088 System.Boolean SuperCharacterController_SuperGround::IsGrounded(System.Boolean,System.Single)
extern void SuperGround_IsGrounded_mA2EE3770FF6EA2CD2EEEEC55AD0C103F45A2CAA4 (void);
// 0x00000089 System.Boolean SuperCharacterController_SuperGround::IsGrounded(System.Boolean,System.Single,UnityEngine.Vector3&)
extern void SuperGround_IsGrounded_mBB93BBD3FC1AE29566A904B2B4F7089EA03D6138 (void);
// 0x0000008A System.Boolean SuperCharacterController_SuperGround::OnSteadyGround(UnityEngine.Vector3,UnityEngine.Vector3)
extern void SuperGround_OnSteadyGround_m89403ADF08A4E0CC17F027774D2F1A80B4E95D12 (void);
// 0x0000008B UnityEngine.Vector3 SuperCharacterController_SuperGround::PrimaryNormal()
extern void SuperGround_PrimaryNormal_mDA40AC4E872D05973E9CD6F26E83C472AC8E6BC2 (void);
// 0x0000008C System.Single SuperCharacterController_SuperGround::Distance()
extern void SuperGround_Distance_mF5D6B3771181AFD9BE48997DF208AFE444665A83 (void);
// 0x0000008D System.Void SuperCharacterController_SuperGround::DebugGround(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void SuperGround_DebugGround_m3996448158025B7A3A433AA5E43F692FED45EAF4 (void);
// 0x0000008E System.Boolean SuperCharacterController_SuperGround::SimulateSphereCast(UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern void SuperGround_SimulateSphereCast_m15F85ADCB950D1B1E2C33E484D58026D5A804D79 (void);
// 0x0000008F UnityEngine.Vector3 SuperCharacterController_SuperGround_GroundHit::get_point()
extern void GroundHit_get_point_m23F49DFD517AC9F37753A1AC3FDEB9788AB5C9E6 (void);
// 0x00000090 System.Void SuperCharacterController_SuperGround_GroundHit::set_point(UnityEngine.Vector3)
extern void GroundHit_set_point_mD2DFF334DC1B71D33E0EE19616E9B25D56FF47DE (void);
// 0x00000091 UnityEngine.Vector3 SuperCharacterController_SuperGround_GroundHit::get_normal()
extern void GroundHit_get_normal_mBA93744A7C9AFA06E3F00BBB06A5343EB53665C3 (void);
// 0x00000092 System.Void SuperCharacterController_SuperGround_GroundHit::set_normal(UnityEngine.Vector3)
extern void GroundHit_set_normal_mD2ED0F20A538F5409BA8C06F6A8056B1C4641B4B (void);
// 0x00000093 System.Single SuperCharacterController_SuperGround_GroundHit::get_distance()
extern void GroundHit_get_distance_m21A6DFE8F4BC011B5F57CC79DEF3D6BB7DAC6963 (void);
// 0x00000094 System.Void SuperCharacterController_SuperGround_GroundHit::set_distance(System.Single)
extern void GroundHit_set_distance_m49F38D09A08B448BCCBE35AD454C8F614616D3BE (void);
// 0x00000095 System.Void SuperCharacterController_SuperGround_GroundHit::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void GroundHit__ctor_m9C5BA2C1D9B27270E1CCD6137CD62BCDF919A58E (void);
// 0x00000096 System.Void CollisionSphere::.ctor(System.Single,System.Boolean,System.Boolean)
extern void CollisionSphere__ctor_m8B6896929AC46724C7176DE7D0769E52C6E89051 (void);
// 0x00000097 System.Boolean SuperCollider::ClosestPointOnSurface(UnityEngine.Collider,UnityEngine.Vector3,System.Single,UnityEngine.Vector3&)
extern void SuperCollider_ClosestPointOnSurface_m0E241D3340730B3924E93CF40498884A61CCAD78 (void);
// 0x00000098 UnityEngine.Vector3 SuperCollider::ClosestPointOnSurface(UnityEngine.SphereCollider,UnityEngine.Vector3)
extern void SuperCollider_ClosestPointOnSurface_m8A3388260168C5F43200F3DBFF0C3015EC065B50 (void);
// 0x00000099 UnityEngine.Vector3 SuperCollider::ClosestPointOnSurface(UnityEngine.BoxCollider,UnityEngine.Vector3)
extern void SuperCollider_ClosestPointOnSurface_m867A36263673F54F2E801ECD80C57D06E3F3EE0A (void);
// 0x0000009A UnityEngine.Vector3 SuperCollider::ClosestPointOnSurface(UnityEngine.CapsuleCollider,UnityEngine.Vector3)
extern void SuperCollider_ClosestPointOnSurface_m6215B50B2406B7CB3412AD293E60F0BDA807525A (void);
// 0x0000009B UnityEngine.Vector3 SuperCollider::ClosestPointOnSurface(UnityEngine.TerrainCollider,UnityEngine.Vector3,System.Single,System.Boolean)
extern void SuperCollider_ClosestPointOnSurface_m60F4A006038B267AE4DC2959A6545FBC0B6EEFD7 (void);
// 0x0000009C System.Void SuperCollisionType::.ctor()
extern void SuperCollisionType__ctor_m58AD25094EB498E254E0EF2292BEDFEFA8A20345 (void);
// 0x0000009D UnityEngine.Vector3 SuperMath::ClampAngleOnPlane(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3)
extern void SuperMath_ClampAngleOnPlane_m5BB0A3A83ED516466A7B7C6FB98B8BB6597DAC0C (void);
// 0x0000009E System.Single SuperMath::BoundedInterpolation(System.Single[],System.Single[],System.Single)
extern void SuperMath_BoundedInterpolation_m834D66312D42D645887BD3FD4E30C74BD03BD31E (void);
// 0x0000009F System.Boolean SuperMath::PointAbovePlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void SuperMath_PointAbovePlane_m8CB95E8E19A589A854B40A80D8CEE23673FAABB0 (void);
// 0x000000A0 System.Boolean SuperMath::Timer(System.Single,System.Single)
extern void SuperMath_Timer_mADABCD91D257BE558916109355BD2BD6936525BF (void);
// 0x000000A1 System.Single SuperMath::ClampAngle(System.Single)
extern void SuperMath_ClampAngle_m9830852BD369E529AB4A9E80B8F8B0895F6ADE91 (void);
// 0x000000A2 System.Single SuperMath::CalculateJumpSpeed(System.Single,System.Single)
extern void SuperMath_CalculateJumpSpeed_m60F2AFEA8C036ED0216AE49A12CE6A8AC7EBDAF7 (void);
// 0x000000A3 System.Enum SuperStateMachine::get_currentState()
extern void SuperStateMachine_get_currentState_mB32DC68BCD3EC5879ED163868C3909D70C0FD22F (void);
// 0x000000A4 System.Void SuperStateMachine::set_currentState(System.Enum)
extern void SuperStateMachine_set_currentState_mF61AB7F790DDB8F384C2F985B0E85FA499C5E70F (void);
// 0x000000A5 System.Void SuperStateMachine::ChangingState()
extern void SuperStateMachine_ChangingState_m0E4A6EA42581C579A9D4AA2E169C67E70395AAB3 (void);
// 0x000000A6 System.Void SuperStateMachine::ConfigureCurrentState()
extern void SuperStateMachine_ConfigureCurrentState_mAF64784AB940093D532E8F1666B354E88CCD7D68 (void);
// 0x000000A7 T SuperStateMachine::ConfigureDelegate(System.String,T)
// 0x000000A8 System.Void SuperStateMachine::SuperUpdate()
extern void SuperStateMachine_SuperUpdate_m550110B2775A0F1C4B1952F198104239F44B682F (void);
// 0x000000A9 System.Void SuperStateMachine::EarlyGlobalSuperUpdate()
extern void SuperStateMachine_EarlyGlobalSuperUpdate_m755E4B3BFBC2F3657F63A94B1DE63A02754DA42B (void);
// 0x000000AA System.Void SuperStateMachine::LateGlobalSuperUpdate()
extern void SuperStateMachine_LateGlobalSuperUpdate_m1FBDC9014B829263350CB8E3C1F56ED568FBBF0F (void);
// 0x000000AB System.Void SuperStateMachine::DoNothing()
extern void SuperStateMachine_DoNothing_m203B2026577A03CF9EA025EAE48DD692639629FA (void);
// 0x000000AC System.Void SuperStateMachine::.ctor()
extern void SuperStateMachine__ctor_m75032D44E6CE0D427E15E28A60D051C3329334CE (void);
// 0x000000AD System.Void SuperStateMachine_State::.ctor()
extern void State__ctor_m2EF844EB75FFA4BFA7E7878CED14B95DAA9E9073 (void);
// 0x000000AE System.Void DebugDraw::DrawMarker(UnityEngine.Vector3,System.Single,UnityEngine.Color,System.Single,System.Boolean)
extern void DebugDraw_DrawMarker_m7A0BE38CF30CCB1AFF13F5C56CCF725232529DE7 (void);
// 0x000000AF System.Void DebugDraw::DrawPlane(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Color,System.Single,System.Boolean)
extern void DebugDraw_DrawPlane_m6FE87BA9E0D89D72E9630D07D49C9655888F2907 (void);
// 0x000000B0 System.Void DebugDraw::DrawVector(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color,System.Single,System.Boolean)
extern void DebugDraw_DrawVector_mD11E4A557147A5A5946DFC0A9CF171F3D82C34CF (void);
// 0x000000B1 System.Void DebugDraw::DrawTriangle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern void DebugDraw_DrawTriangle_m70B96CEDF7796717F31E4134F58033F6E7B72723 (void);
// 0x000000B2 System.Void DebugDraw::DrawTriangle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,UnityEngine.Transform)
extern void DebugDraw_DrawTriangle_mB64BF1DF3EFC758DCAF112ED6B96627EC8798A65 (void);
// 0x000000B3 System.Void DebugDraw::DrawMesh(UnityEngine.Mesh,UnityEngine.Color,UnityEngine.Transform)
extern void DebugDraw_DrawMesh_mC058ABA78E2AA7D5F21881185FB09D9F44862C0D (void);
// 0x000000B4 UnityEngine.Color DebugDraw::RandomColor()
extern void DebugDraw_RandomColor_m6AF9C52FCF7C86BA392749753BFAB182206C4962 (void);
// 0x000000B5 System.Void Math3d::Init()
extern void Math3d_Init_m34E98B06D3C5330AF1B0AA54D38B6D118B4B7B09 (void);
// 0x000000B6 UnityEngine.Vector3 Math3d::AddVectorLength(UnityEngine.Vector3,System.Single)
extern void Math3d_AddVectorLength_mBBF07717EDE6A156B5EF654E86E656611B7574B0 (void);
// 0x000000B7 UnityEngine.Vector3 Math3d::SetVectorLength(UnityEngine.Vector3,System.Single)
extern void Math3d_SetVectorLength_mCF6F873ED18FFEDDA5D2CEC48BB4D27595B46D29 (void);
// 0x000000B8 UnityEngine.Quaternion Math3d::SubtractRotation(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Math3d_SubtractRotation_m577BFC25ED2D262CC545BB5426CD5EAE5B746E38 (void);
// 0x000000B9 System.Boolean Math3d::PlanePlaneIntersection(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_PlanePlaneIntersection_m82CFA8805252E35FAE28C48F3D999B81232143E9 (void);
// 0x000000BA System.Boolean Math3d::LinePlaneIntersection(UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_LinePlaneIntersection_m20A69BA0741B5FC28BE4570339B52632DA3C3152 (void);
// 0x000000BB System.Boolean Math3d::LineLineIntersection(UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_LineLineIntersection_m5BF9048638CD6AE0079E1691E8465FBF6E52CAFE (void);
// 0x000000BC System.Boolean Math3d::ClosestPointsOnTwoLines(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_ClosestPointsOnTwoLines_mB8A1F4EEBD17E40FD2A5E6CF0A2AC6B3D194D397 (void);
// 0x000000BD UnityEngine.Vector3 Math3d::ProjectPointOnLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_ProjectPointOnLine_m1AA7F9F87D468123DC274702E2D15003721A2123 (void);
// 0x000000BE UnityEngine.Vector3 Math3d::ProjectPointOnLineSegment(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_ProjectPointOnLineSegment_m49A3362FC58074CAE5F06A4108B217817CD5737B (void);
// 0x000000BF UnityEngine.Vector3 Math3d::ProjectPointOnPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_ProjectPointOnPlane_mCC932441F87E842ED16EAE8A350E46E0B4B504CF (void);
// 0x000000C0 UnityEngine.Vector3 Math3d::ProjectVectorOnPlane(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_ProjectVectorOnPlane_m7B24068B9C069286A580B2E0170F8AADC9FD2734 (void);
// 0x000000C1 System.Single Math3d::SignedDistancePlanePoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_SignedDistancePlanePoint_mDA8C61C5FB25CCA1F5369D9C8499294C4F616FDB (void);
// 0x000000C2 System.Single Math3d::SignedDotProduct(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_SignedDotProduct_mFAB0824BDCF7FBEBC7D1DEF1F6F599B708F4F98C (void);
// 0x000000C3 System.Single Math3d::SignedVectorAngle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_SignedVectorAngle_m7775F79314F3319122A6AA338517EA1EABA42518 (void);
// 0x000000C4 System.Single Math3d::AngleVectorPlane(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_AngleVectorPlane_mFB19C119BBD07236B0A5A6AAF26242C370BD7EA5 (void);
// 0x000000C5 System.Single Math3d::DotProductAngle(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_DotProductAngle_m1244D537C89756D878F39DBA444A9745B7346A4E (void);
// 0x000000C6 System.Void Math3d::PlaneFrom3Points(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_PlaneFrom3Points_mBDC00FA958FB6FF946D707E5745777A38855FE2C (void);
// 0x000000C7 UnityEngine.Vector3 Math3d::GetForwardVector(UnityEngine.Quaternion)
extern void Math3d_GetForwardVector_m91D1149165764C0BBD02ADB3890BC8C7D6C0B900 (void);
// 0x000000C8 UnityEngine.Vector3 Math3d::GetUpVector(UnityEngine.Quaternion)
extern void Math3d_GetUpVector_m827E4EA310C248A5CBEC0166F87A211E5D3177F0 (void);
// 0x000000C9 UnityEngine.Vector3 Math3d::GetRightVector(UnityEngine.Quaternion)
extern void Math3d_GetRightVector_m822F4E11B4E2E98539A527C66754A2EEF707C94F (void);
// 0x000000CA UnityEngine.Quaternion Math3d::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern void Math3d_QuaternionFromMatrix_m3CD776834DF4ACF1A372DD0B70033EB8BE65C521 (void);
// 0x000000CB UnityEngine.Vector3 Math3d::PositionFromMatrix(UnityEngine.Matrix4x4)
extern void Math3d_PositionFromMatrix_m3081C8A5C48D696AC36E13C803F1AB308D67FE54 (void);
// 0x000000CC System.Void Math3d::LookRotationExtended(UnityEngine.GameObject&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_LookRotationExtended_m9043427D32E9829190BCE6C9ED63B6AB34F9053B (void);
// 0x000000CD System.Void Math3d::TransformWithParent(UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Quaternion,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void Math3d_TransformWithParent_m44EF02AF2D7440EFC55EE2F1DB7C947880571454 (void);
// 0x000000CE System.Void Math3d::PreciseAlign(UnityEngine.GameObject&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_PreciseAlign_m2888D1FFBB41827EDA60084850C672609D41CE68 (void);
// 0x000000CF System.Void Math3d::VectorsToTransform(UnityEngine.GameObject&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_VectorsToTransform_mF2C6B0D307BF868B713E23C306FC559C88C0625A (void);
// 0x000000D0 System.Int32 Math3d::PointOnWhichSideOfLineSegment(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_PointOnWhichSideOfLineSegment_m129C3CBD061CE335131BF3352957C2B81255EE6A (void);
// 0x000000D1 System.Single Math3d::MouseDistanceToLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_MouseDistanceToLine_m2C1605490D4F149BAA6D98DEDDBF7FAB83994BDF (void);
// 0x000000D2 System.Single Math3d::MouseDistanceToCircle(UnityEngine.Vector3,System.Single)
extern void Math3d_MouseDistanceToCircle_mE32D5174D0B2819451F52E547A8C9DB74C6E1A21 (void);
// 0x000000D3 System.Boolean Math3d::IsLineInRectangle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_IsLineInRectangle_m40CC653F190F0645118B5A8E25181A847B62F56B (void);
// 0x000000D4 System.Boolean Math3d::IsPointInRectangle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_IsPointInRectangle_m78445C7CE181733DAFC35106646C1E9A670259AC (void);
// 0x000000D5 System.Boolean Math3d::AreLineSegmentsCrossing(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Math3d_AreLineSegmentsCrossing_m7B9495FA3B4B058B357E2BDDBD4974EB42692AF4 (void);
// 0x000000D6 System.Void Math3d::.ctor()
extern void Math3d__ctor_mF697AAF66443A2CB51A167502795B0F6DAB9B84F (void);
// 0x000000D7 System.Void Math3d::.cctor()
extern void Math3d__cctor_m02D33B3F572D37724D0A8D065083C503BBB7AA3D (void);
// 0x000000D8 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x000000D9 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x000000DA System.Void PowerUp::OnTriggerEnter(UnityEngine.Collider)
extern void PowerUp_OnTriggerEnter_m6C53E6F682A799FFFB99B5C8CFDADF969AACB3A2 (void);
// 0x000000DB System.Void PowerUp::Pickup()
extern void PowerUp_Pickup_mD3966CDAC8FB2810C6B16FF76E4554EA12BE411A (void);
// 0x000000DC System.Void PowerUp::.ctor()
extern void PowerUp__ctor_mDAF4AED1581D2E6AFD14782F86F935DFDE5E1A87 (void);
// 0x000000DD System.Void Quit::Start()
extern void Quit_Start_m4E42BFD36424A5BE5406B3BEC3BB8348E532CD13 (void);
// 0x000000DE System.Void Quit::Update()
extern void Quit_Update_m50901908B023B77D852F6F63B3C5D392AD313C33 (void);
// 0x000000DF System.Void Quit::.ctor()
extern void Quit__ctor_mEB6E7FE6F53362692AD96E4DEE1D3906A592A7C4 (void);
// 0x000000E0 UnityEngine.InputSystem.InputActionAsset InHouseGameInput::get_asset()
extern void InHouseGameInput_get_asset_mA2689FA21A8D9EC2DCE5318E6DE11A11ADBD3C92 (void);
// 0x000000E1 System.Void InHouseGameInput::.ctor()
extern void InHouseGameInput__ctor_m8E835E3FE78639D99D80BC46D69FCA30AD89656B (void);
// 0x000000E2 System.Void InHouseGameInput::Dispose()
extern void InHouseGameInput_Dispose_mE9D906CB0A3555B42B232B56F51B819B5C2F0E35 (void);
// 0x000000E3 System.Nullable`1<UnityEngine.InputSystem.InputBinding> InHouseGameInput::get_bindingMask()
extern void InHouseGameInput_get_bindingMask_mC1981059D9205E4EEC52F23DD2436DB7A7C86D00 (void);
// 0x000000E4 System.Void InHouseGameInput::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void InHouseGameInput_set_bindingMask_m09A42F8D9878FEF7DBD1C5680D3AADB9333994E2 (void);
// 0x000000E5 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> InHouseGameInput::get_devices()
extern void InHouseGameInput_get_devices_m2EA8A13530C60E8F733B0012503DAB6C3193FB11 (void);
// 0x000000E6 System.Void InHouseGameInput::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void InHouseGameInput_set_devices_mEEB0A510A2114CB439028B56D6257CE5AD74D3D1 (void);
// 0x000000E7 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> InHouseGameInput::get_controlSchemes()
extern void InHouseGameInput_get_controlSchemes_mBB10365F858A2287F569C199515FF5C0C92CF921 (void);
// 0x000000E8 System.Boolean InHouseGameInput::Contains(UnityEngine.InputSystem.InputAction)
extern void InHouseGameInput_Contains_m6F12F25F959078BE1EED8DBBE4316DDE44E87BE2 (void);
// 0x000000E9 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> InHouseGameInput::GetEnumerator()
extern void InHouseGameInput_GetEnumerator_m606746F09D2D8C81473882998DBEFC12E6670E85 (void);
// 0x000000EA System.Collections.IEnumerator InHouseGameInput::System.Collections.IEnumerable.GetEnumerator()
extern void InHouseGameInput_System_Collections_IEnumerable_GetEnumerator_m6BEAC6E9D6E7F31445424C486FAF5C507E01B73C (void);
// 0x000000EB System.Void InHouseGameInput::Enable()
extern void InHouseGameInput_Enable_mE275DB9C4685968BB0241DEB4FB3DD26B52552EA (void);
// 0x000000EC System.Void InHouseGameInput::Disable()
extern void InHouseGameInput_Disable_mFB9CB2681F4A1D6F16EDDA42849410F473C42C81 (void);
// 0x000000ED InHouseGameInput_GameActions InHouseGameInput::get_Game()
extern void InHouseGameInput_get_Game_mCC127585DF46B7AE55F2B32B1BBE591958002DE8 (void);
// 0x000000EE UnityEngine.InputSystem.InputControlScheme InHouseGameInput::get_KeyboardScheme()
extern void InHouseGameInput_get_KeyboardScheme_m6B60D7258BCF8F9B807223D0BCD9640B1CD3201F (void);
// 0x000000EF System.Void InHouseGameInput_GameActions::.ctor(InHouseGameInput)
extern void GameActions__ctor_m27C6C0064D9FE1BDB406234230A2DC9E45AEDF17_AdjustorThunk (void);
// 0x000000F0 UnityEngine.InputSystem.InputAction InHouseGameInput_GameActions::get_Move()
extern void GameActions_get_Move_mE87A1909517A65952BDF2A99AAF1C2E2A533B2AF_AdjustorThunk (void);
// 0x000000F1 UnityEngine.InputSystem.InputActionMap InHouseGameInput_GameActions::Get()
extern void GameActions_Get_m5B79991937C94C9553CEA32AC6CE562AE2D47CAF_AdjustorThunk (void);
// 0x000000F2 System.Void InHouseGameInput_GameActions::Enable()
extern void GameActions_Enable_mCF988CC75D8936B63321870F553908CBE01D280D_AdjustorThunk (void);
// 0x000000F3 System.Void InHouseGameInput_GameActions::Disable()
extern void GameActions_Disable_mE3AE7CE6DC957EA06A442D66E73CDC7A361B50A9_AdjustorThunk (void);
// 0x000000F4 System.Boolean InHouseGameInput_GameActions::get_enabled()
extern void GameActions_get_enabled_m9EC312F534FD7BA341334AE73D0D787BC3F45DE8_AdjustorThunk (void);
// 0x000000F5 UnityEngine.InputSystem.InputActionMap InHouseGameInput_GameActions::op_Implicit(InHouseGameInput_GameActions)
extern void GameActions_op_Implicit_mCFA4F9034DC2FAE960E69CE6E2A39486CF68B7CC (void);
// 0x000000F6 System.Void InHouseGameInput_GameActions::SetCallbacks(InHouseGameInput_IGameActions)
extern void GameActions_SetCallbacks_m0D12BF74753D0D7F717BF695973CB7A2F261FE5E_AdjustorThunk (void);
// 0x000000F7 System.Void InHouseGameInput_IGameActions::OnMove(UnityEngine.InputSystem.InputAction_CallbackContext)
// 0x000000F8 System.Void TestAnimation::Start()
extern void TestAnimation_Start_mF04ABE6EA7992CB2B9A70A8E75B50E11CEA0C252 (void);
// 0x000000F9 System.Void TestAnimation::Update()
extern void TestAnimation_Update_m263C79D072FFB04A9038521E7995C9ABB14B4BBA (void);
// 0x000000FA System.Void TestAnimation::.ctor()
extern void TestAnimation__ctor_m79FD67C8378B0720750A34DA476A22B46058308F (void);
// 0x000000FB System.Void TestMotionParameter::Start()
extern void TestMotionParameter_Start_mC1E22F7F05F8E2A9ED3A0D85CB9EDA5214E361C5 (void);
// 0x000000FC System.Void TestMotionParameter::Update()
extern void TestMotionParameter_Update_m2CA9C45AF5E1762436B1076ADE52A007F20AE6A7 (void);
// 0x000000FD System.Void TestMotionParameter::.ctor()
extern void TestMotionParameter__ctor_m5842144F7D36FD74F8C355A0437A29E2A9B86B12 (void);
// 0x000000FE System.Boolean EffectCore::GetIsSetUp()
extern void EffectCore_GetIsSetUp_m92A6227E710D395FB85FBCD7BB5D9687359AA197 (void);
// 0x000000FF System.Void EffectCore::Start()
extern void EffectCore_Start_m0DBB4A2241CDBD9F54544F11A3929B29A45B2918 (void);
// 0x00000100 System.Void EffectCore::Update()
extern void EffectCore_Update_m476B6B7B75C0797DEC876DDEBBE833D27D0996A6 (void);
// 0x00000101 System.Void EffectCore::LoadEffect(System.Int32)
extern void EffectCore_LoadEffect_m78F77BE853B42B9D5E4A16BDA1F43B831EA566E3 (void);
// 0x00000102 UnityEngine.GameObject EffectCore::GetEffectObject(System.Int32)
extern void EffectCore_GetEffectObject_m4A88E594E418F9F7DCF77A63DDCFE34EB66F2A88 (void);
// 0x00000103 System.Void EffectCore::AddEffectGenerate(EffectAction)
extern void EffectCore_AddEffectGenerate_m0EA93CFF681952DCA1DEF2186C5F769A9FEAEE9B (void);
// 0x00000104 System.Void EffectCore::OnDestroy()
extern void EffectCore_OnDestroy_m02A18E2B8BE5202A618B14B0FCA4AA0F49EEFA08 (void);
// 0x00000105 System.Void EffectCore::.ctor()
extern void EffectCore__ctor_m54B3356980516BCA8C43F3909B80C81A4246D7B3 (void);
// 0x00000106 EffectAction EffectCreate::Appearance(System.Int32,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Single)
extern void EffectCreate_Appearance_m89193CEC0FB1F8659666F40FAA067433F9C3DC3D (void);
// 0x00000107 System.Void EffectAction::SetLifeFrame(System.Single)
extern void EffectAction_SetLifeFrame_m719224EFA67CC5515FDEE61A0C489D3034D8A713 (void);
// 0x00000108 System.Void EffectAction::OnStop()
extern void EffectAction_OnStop_mDE9549639D9232013AEA9E7C5AEA28A8B3DE7186 (void);
// 0x00000109 System.Void EffectAction::OffStop()
extern void EffectAction_OffStop_m4A3CE9DEFE5C712F029DB04B88148DECEE172734 (void);
// 0x0000010A System.Void EffectAction::Start()
extern void EffectAction_Start_m8D669543832CE4C18E219B8DA3D77863A0D183FC (void);
// 0x0000010B System.Void EffectAction::Update()
extern void EffectAction_Update_mA23971CF3DB901824D25B213D51D2B1BDAEC2499 (void);
// 0x0000010C System.Void EffectAction::EffectDestroy()
extern void EffectAction_EffectDestroy_mD3EE907D9481EE7AC627F7FED900F6A4C15FDC34 (void);
// 0x0000010D System.Void EffectAction::.ctor()
extern void EffectAction__ctor_m8545F8CA6476E33A4E62F6F55FC39ABC98776FA8 (void);
// 0x0000010E System.Void EffectGenerateRepository::AddEffectAction(EffectAction)
extern void EffectGenerateRepository_AddEffectAction_m654A5233B0EFBFA1985064217EE5C08EBD485AB3 (void);
// 0x0000010F System.Void EffectGenerateRepository::Update()
extern void EffectGenerateRepository_Update_mB6D00152984279CDDAB504951914DF0B59DA80AA (void);
// 0x00000110 System.Void EffectGenerateRepository::ReleaseAll()
extern void EffectGenerateRepository_ReleaseAll_mF7E2B9583FF23444B536CF6D2EDAD6BBDCDC238F (void);
// 0x00000111 System.Void EffectGenerateRepository::OnStopAll()
extern void EffectGenerateRepository_OnStopAll_mA94CCED8EFD8F9AEE61927E9E0AB190A0E9A2D09 (void);
// 0x00000112 System.Void EffectGenerateRepository::OffStopAll()
extern void EffectGenerateRepository_OffStopAll_m9F3AB5DE472B4E79FDE4422D138F4F88216CF7DE (void);
// 0x00000113 System.Void EffectGenerateRepository::.ctor()
extern void EffectGenerateRepository__ctor_m2FEAF747D55B19BEE6CBDF7BE77738CE83935E81 (void);
// 0x00000114 System.Void EffectGenerateRepository_<>c::.cctor()
extern void U3CU3Ec__cctor_m8F6901762032BEF0C377C87E8AC6514943C167FD (void);
// 0x00000115 System.Void EffectGenerateRepository_<>c::.ctor()
extern void U3CU3Ec__ctor_mD8F880EA0BEAE73CA5824E5E19AE4E0DE34E3EC3 (void);
// 0x00000116 System.Boolean EffectGenerateRepository_<>c::<Update>b__2_0(EffectAction)
extern void U3CU3Ec_U3CUpdateU3Eb__2_0_mB7C7F6088F72319901009BB5E87D4F7BC869ADD0 (void);
// 0x00000117 System.Boolean EffectGenerateRepository_<>c::<ReleaseAll>b__3_0(EffectAction)
extern void U3CU3Ec_U3CReleaseAllU3Eb__3_0_mDF11BBEBD98E1BA1B2569A13433C6FE39A7ECF78 (void);
// 0x00000118 System.Boolean EffectStorageRepository::GetIsSetUp()
extern void EffectStorageRepository_GetIsSetUp_mFE1BEBD44601ADA1FC420C424991E3A36E2CA65E (void);
// 0x00000119 System.Void EffectStorageRepository::LoadEffectObject(System.Int32)
extern void EffectStorageRepository_LoadEffectObject_m8BC99A4E61F51060F313813ADA3765652D651E44 (void);
// 0x0000011A System.Void EffectStorageRepository::Update()
extern void EffectStorageRepository_Update_m652712959DE5D8E89AC90A66CDE40AE3D36E76E2 (void);
// 0x0000011B UnityEngine.GameObject EffectStorageRepository::GetEffectObject(System.Int32)
extern void EffectStorageRepository_GetEffectObject_mF339FB3051E9F043F199F5830BE6ED0DF4F181C2 (void);
// 0x0000011C System.Void EffectStorageRepository::ReleaseAll()
extern void EffectStorageRepository_ReleaseAll_mA97B2D345A3D85BAB4BBBDCD45A23B06109391B2 (void);
// 0x0000011D System.Void EffectStorageRepository::.ctor()
extern void EffectStorageRepository__ctor_mE11FB75C82F99AEB6A54600496E80D8A7839376E (void);
// 0x0000011E System.Void EffectRestraint::OnAutoDestroy()
extern void EffectRestraint_OnAutoDestroy_m000DF48295DA70C03CD0576928077B88B47ED8F6 (void);
// 0x0000011F System.Void EffectRestraint::OffAutoDestroy()
extern void EffectRestraint_OffAutoDestroy_m338D96EB214647D02A6BF7C771D962CDA723A473 (void);
// 0x00000120 System.Void EffectRestraint::OnStop()
extern void EffectRestraint_OnStop_m52F853F84D6315AC8A58C8EB4F7E7BEB3D78B72E (void);
// 0x00000121 System.Void EffectRestraint::OffStop()
extern void EffectRestraint_OffStop_m5B970D093DB7B4643422E6416EF65803549F9946 (void);
// 0x00000122 System.Boolean EffectRestraint::NullCheck()
extern void EffectRestraint_NullCheck_m4B4D1C2A4710EF2FB82016870921342585D2A7E4 (void);
// 0x00000123 System.Void EffectRestraint::Init(UnityEngine.GameObject)
extern void EffectRestraint_Init_m331EF5692C52126A10C0B881D57678B49CE9DD3D (void);
// 0x00000124 System.Void EffectRestraint::Update()
extern void EffectRestraint_Update_m0D646504E245AAF5CB2CDD4988DA2CB33FF6D2D9 (void);
// 0x00000125 System.Void EffectRestraint::Destroy()
extern void EffectRestraint_Destroy_mF4B513377596EE18E65B9F8CC41BF1E579A77DD9 (void);
// 0x00000126 System.Void EffectRestraint::.ctor()
extern void EffectRestraint__ctor_mBED18FEE7DE07698023C23A1A07375EEE3FCBB21 (void);
// 0x00000127 System.Boolean EffectRestraintControl::NullCheck()
extern void EffectRestraintControl_NullCheck_m8C6E1AB1303124ECD32A9E1FFB03DCA6A0668F71 (void);
// 0x00000128 System.Void EffectRestraintControl::Init(UnityEngine.GameObject,System.Single)
extern void EffectRestraintControl_Init_m2406A86346C06D9A3A325D9B67B339DE114F7038 (void);
// 0x00000129 System.Void EffectRestraintControl::OnStop()
extern void EffectRestraintControl_OnStop_m63F78E280A710300697E86CA2B47A702A27FF960 (void);
// 0x0000012A System.Void EffectRestraintControl::OffStop()
extern void EffectRestraintControl_OffStop_m91E1555A5BC1735EFBFD7196EF2497E85F334706 (void);
// 0x0000012B System.Void EffectRestraintControl::Update()
extern void EffectRestraintControl_Update_mB035DEEB076CBCB918510B0FF1DEF5759C3446FC (void);
// 0x0000012C System.Void EffectRestraintControl::UpdateLifeFrame()
extern void EffectRestraintControl_UpdateLifeFrame_mEF74B8F44537467E791154E13757670C8186CF37 (void);
// 0x0000012D System.Void EffectRestraintControl::Destroy()
extern void EffectRestraintControl_Destroy_mEBB09549EA9B0D7525863DE10B466ED7E06E4EB1 (void);
// 0x0000012E System.Void EffectRestraintControl::.ctor()
extern void EffectRestraintControl__ctor_m122FF5D170943E4136CAD5BB292D247CA291A9D0 (void);
// 0x0000012F System.Void EffectRestraintControl_<>c::.cctor()
extern void U3CU3Ec__cctor_m72A20AB9DE273C94877E02C4A5D53B120FA41D29 (void);
// 0x00000130 System.Void EffectRestraintControl_<>c::.ctor()
extern void U3CU3Ec__ctor_mCEFEC0E228AD9E0ADF285D958872335D7D9496B0 (void);
// 0x00000131 System.Boolean EffectRestraintControl_<>c::<Update>b__8_0(EffectRestraint)
extern void U3CU3Ec_U3CUpdateU3Eb__8_0_m2E187D615034763E9C467293DD7FDA430D4A4094 (void);
// 0x00000132 System.Boolean BattleCore::GetIsSetUp()
extern void BattleCore_GetIsSetUp_mE627FB1A91ABD78A9AEA35491717C5FAF39C0FFD (void);
// 0x00000133 System.Void BattleCore::Start()
extern void BattleCore_Start_m15CDA64A265DDE12CF240E155C6A4B1CF4F79012 (void);
// 0x00000134 System.Void BattleCore::Update()
extern void BattleCore_Update_mAA000FEBD7A242C26848E6E9DD1404B0D6092DDD (void);
// 0x00000135 System.Void BattleCore::AddPlayerUnit(BattleUnit)
extern void BattleCore_AddPlayerUnit_m0E557339FA4EE7C7AC25A6F30008584540A2C83C (void);
// 0x00000136 System.Void BattleCore::AddEnemyUnit(BattleUnit)
extern void BattleCore_AddEnemyUnit_mA7F548510E16571A6E883359D9566EC49A6B642C (void);
// 0x00000137 BattleUnit BattleCore::CreateBattleUnit(System.Int32)
extern void BattleCore_CreateBattleUnit_m6126A6843D91F06FEC227A8A65B8D7FE4B8F1F52 (void);
// 0x00000138 System.Void BattleCore::CreatePlayerBattleUnit(System.Int32)
extern void BattleCore_CreatePlayerBattleUnit_m42CD590988D7928598253273C16A6E0589A32AF5 (void);
// 0x00000139 System.Void BattleCore::CreateEnemyBattleUnit(System.Int32)
extern void BattleCore_CreateEnemyBattleUnit_m560158A499A0C05A97844BA66C05F5A9FBB5E868 (void);
// 0x0000013A System.Void BattleCore::SetUp()
extern void BattleCore_SetUp_mDAC07463F8D288BB2A7B0CF8A58CF31928F34E59 (void);
// 0x0000013B System.Void BattleCore::.ctor()
extern void BattleCore__ctor_mE10FB3E904E9F88083F994C5CC139893F9CF7E9E (void);
// 0x0000013C System.Int32 BattleDefinition::GetMaxBattleUnitPlayer()
extern void BattleDefinition_GetMaxBattleUnitPlayer_mF0AD1785AE8382F6240490A80F15179F5E0998C1 (void);
// 0x0000013D System.Int32 BattleDefinition::GetMaxBattleUnitEnemy()
extern void BattleDefinition_GetMaxBattleUnitEnemy_m6A2A19558A94C8336E4D7089ABFE3D04667FD1E8 (void);
// 0x0000013E System.Void BattleDefinition::.ctor()
extern void BattleDefinition__ctor_m8793A47364E6C315FB52AC47AD249BEC8765C051 (void);
// 0x0000013F System.Void BattleDefinition::.cctor()
extern void BattleDefinition__cctor_m35E766BB68ECC5B198CCA0A9605BB4F3BBD831C5 (void);
// 0x00000140 System.Collections.Generic.List`1<BattleUnit> BattleMemberControl::GetPlayerMemberUnit()
extern void BattleMemberControl_GetPlayerMemberUnit_m3414FEE47548DEA952207F6AAA6003357C36719D (void);
// 0x00000141 System.Collections.Generic.List`1<BattleUnit> BattleMemberControl::GetEnemyMemberUnit()
extern void BattleMemberControl_GetEnemyMemberUnit_mD77FC952E4A53B2F74A0489D330F633A8E3C9F9F (void);
// 0x00000142 System.Collections.Generic.List`1<BattleUnit> BattleMemberControl::GetAllBattleMemberUnit()
extern void BattleMemberControl_GetAllBattleMemberUnit_mCCFB4A6B7484AF49C4702DF681E952D2D6FC6F3D (void);
// 0x00000143 System.Void BattleMemberControl::Update()
extern void BattleMemberControl_Update_mAA8CD0A312FD991C3DD49520A534DB89B60E6056 (void);
// 0x00000144 System.Void BattleMemberControl::AutoRelease()
extern void BattleMemberControl_AutoRelease_mD05AF863F6988F7DF823B440B69233E8164890FC (void);
// 0x00000145 System.Void BattleMemberControl::AddPlayerUnit(BattleUnit)
extern void BattleMemberControl_AddPlayerUnit_mEB0D0AB734A74D57F1BF616FA3A8DCA33393C7D7 (void);
// 0x00000146 System.Void BattleMemberControl::AddEnemyUnit(BattleUnit)
extern void BattleMemberControl_AddEnemyUnit_mC8CCB286758A745F0E2DBE6E02CE8CA911FEB0D0 (void);
// 0x00000147 System.Void BattleMemberControl::.ctor()
extern void BattleMemberControl__ctor_m78B1CFDE8D5356C49CA60D23EC435834CB2FB361 (void);
// 0x00000148 System.Void BattleMemberControl_<>c::.cctor()
extern void U3CU3Ec__cctor_mACD911A818033C51F367657CCE9B2B859AD8D6CD (void);
// 0x00000149 System.Void BattleMemberControl_<>c::.ctor()
extern void U3CU3Ec__ctor_mEB13E58DD6974A1F27A2D4C25C0B3979843B478F (void);
// 0x0000014A System.Boolean BattleMemberControl_<>c::<AutoRelease>b__7_0(BattleUnit)
extern void U3CU3Ec_U3CAutoReleaseU3Eb__7_0_mD9924984CA2E64BDB815CDAC1BDED852AFD3B76E (void);
// 0x0000014B System.Boolean BattleMemberControl_<>c::<AutoRelease>b__7_1(BattleUnit)
extern void U3CU3Ec_U3CAutoReleaseU3Eb__7_1_m57FC1AB2A11B69F60E32BF1E1A6699D38E1E71EF (void);
// 0x0000014C System.Boolean BattleMemberControl_<>c::<AutoRelease>b__7_2(BattleUnit)
extern void U3CU3Ec_U3CAutoReleaseU3Eb__7_2_mCCC0064B1AC36BC7F0B2286D9DC5F857607B8ED8 (void);
// 0x0000014D System.Boolean GameCore::GetIsSetUp()
extern void GameCore_GetIsSetUp_m50DE8A0BC9160260BDC704F78ED6EEB64E33CE2D (void);
// 0x0000014E System.Void GameCore::Start()
extern void GameCore_Start_m24C57D5D23F84240EA1D92D5F7AB4D4EB943774B (void);
// 0x0000014F System.Void GameCore::Update()
extern void GameCore_Update_mA3EAB0146A0881D876B58FF91643C82E7D3812AB (void);
// 0x00000150 System.Void GameCore::.ctor()
extern void GameCore__ctor_m277C89012E5C42EC18AEFD307333EADC56F8EF19 (void);
// 0x00000151 System.Int32 DungeonBattleMemberExcelData::GetIndex()
extern void DungeonBattleMemberExcelData_GetIndex_m4EAE678BFE0986F8F810EBE0CE5DCB2AF03B9100 (void);
// 0x00000152 System.Int32 DungeonBattleMemberExcelData::GetID()
extern void DungeonBattleMemberExcelData_GetID_mCBA848C8F8F2AE02BD38560202C90A40B86A17D3 (void);
// 0x00000153 System.Int32 DungeonBattleMemberExcelData::GetPin01UnitID()
extern void DungeonBattleMemberExcelData_GetPin01UnitID_m9DD5CA290B93E0D62AE581172646EFA0DF71EC99 (void);
// 0x00000154 System.Int32 DungeonBattleMemberExcelData::GetPin02UnitID()
extern void DungeonBattleMemberExcelData_GetPin02UnitID_mD5F6CAB9C072E24857A8E7F375D481EDC1F5AC90 (void);
// 0x00000155 System.Int32 DungeonBattleMemberExcelData::GetPin03UnitID()
extern void DungeonBattleMemberExcelData_GetPin03UnitID_m40694F107B3899BDC706C2520A7F87AFD50AD0C8 (void);
// 0x00000156 System.Int32 DungeonBattleMemberExcelData::GetPin04UnitID()
extern void DungeonBattleMemberExcelData_GetPin04UnitID_mD9C23D7D12DE575DECE16A1D29F3000068393964 (void);
// 0x00000157 System.Int32 DungeonBattleMemberExcelData::GetPin05UnitID()
extern void DungeonBattleMemberExcelData_GetPin05UnitID_m7EB1956ADC91E9BFE6992098FC2E21A930309AE1 (void);
// 0x00000158 System.Int32 DungeonBattleMemberExcelData::GetPin06UnitID()
extern void DungeonBattleMemberExcelData_GetPin06UnitID_m1A89F7F3D09F39AB4EE4D4287A0F7956C515FC05 (void);
// 0x00000159 System.Void DungeonBattleMemberExcelData::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void DungeonBattleMemberExcelData_ManualSetUp_m702CE9AEA92BA14E6A80B936014B0D07FF230A1A (void);
// 0x0000015A System.Void DungeonBattleMemberExcelData::.ctor()
extern void DungeonBattleMemberExcelData__ctor_m7E05BD5B2A0805DBB8F9737501E487D35C88BE1D (void);
// 0x0000015B System.Int32 DungeonBattleMemberGamelData::GetIndex()
extern void DungeonBattleMemberGamelData_GetIndex_m2263A7DE7179581F3867DA3E4FC561F6EBDCB095 (void);
// 0x0000015C System.Int32 DungeonBattleMemberGamelData::GetID()
extern void DungeonBattleMemberGamelData_GetID_m7E68AA502D075B051BADE486F7C520BD675C0C42 (void);
// 0x0000015D System.Int32 DungeonBattleMemberGamelData::GetPin01UnitID()
extern void DungeonBattleMemberGamelData_GetPin01UnitID_m78BFC1AD7F790009CD4D09C7D630A95215326D90 (void);
// 0x0000015E System.Int32 DungeonBattleMemberGamelData::GetPin02UnitID()
extern void DungeonBattleMemberGamelData_GetPin02UnitID_m0ADC8E33F7F30C3EC3E07E633F5A81268CAB3782 (void);
// 0x0000015F System.Int32 DungeonBattleMemberGamelData::GetPin03UnitID()
extern void DungeonBattleMemberGamelData_GetPin03UnitID_m159D46F6EA0BDA3F25AF9D413BB1D732EDBAA272 (void);
// 0x00000160 System.Int32 DungeonBattleMemberGamelData::GetPin04UnitID()
extern void DungeonBattleMemberGamelData_GetPin04UnitID_mE066A98DAEEF17841AA5AEEF689EDB76FCF2955D (void);
// 0x00000161 System.Int32 DungeonBattleMemberGamelData::GetPin05UnitID()
extern void DungeonBattleMemberGamelData_GetPin05UnitID_mC3647D4959422519BC5F1BA3043EC25F0D4E916F (void);
// 0x00000162 System.Int32 DungeonBattleMemberGamelData::GetPin06UnitID()
extern void DungeonBattleMemberGamelData_GetPin06UnitID_mB49252ED9201B2E256286907490D144DF5EACBA8 (void);
// 0x00000163 System.Void DungeonBattleMemberGamelData::SetValue(DungeonBattleMemberExcelData)
extern void DungeonBattleMemberGamelData_SetValue_m28179DB90F6612269D6E98BE4ACE8862E7E14DDA (void);
// 0x00000164 System.Void DungeonBattleMemberGamelData::.ctor()
extern void DungeonBattleMemberGamelData__ctor_m69592F939D91D67F5A89BC49566A21253BE3EA36 (void);
// 0x00000165 System.Void DungeonBattleMemberScriptable::.ctor()
extern void DungeonBattleMemberScriptable__ctor_m4798A7AC7C5DAD9EA213284FB2A1CC76C3FAA222 (void);
// 0x00000166 System.Int32 DungeonBattlePlacementExcelData::GetIndex()
extern void DungeonBattlePlacementExcelData_GetIndex_m4A4CB0E97A709766A7991A1055E50A82E45F9E74 (void);
// 0x00000167 System.Int32 DungeonBattlePlacementExcelData::GetID()
extern void DungeonBattlePlacementExcelData_GetID_m05D3775AB6CE531B96423894FFAD820B76D3E827 (void);
// 0x00000168 System.Single DungeonBattlePlacementExcelData::GetBattleAreaLength()
extern void DungeonBattlePlacementExcelData_GetBattleAreaLength_m87B5365351816374C38BB6D6E6C84B09AFDE5A8F (void);
// 0x00000169 System.Single DungeonBattlePlacementExcelData::GetBattleAreaStartPositionX()
extern void DungeonBattlePlacementExcelData_GetBattleAreaStartPositionX_mC067C00143A78A26ED201EA523BAC7D76D9D65C5 (void);
// 0x0000016A System.Single DungeonBattlePlacementExcelData::GetBattleAreaStartPositionY()
extern void DungeonBattlePlacementExcelData_GetBattleAreaStartPositionY_mF15C71445173EA1FA0D0845E7B68B5F35EC04CBF (void);
// 0x0000016B System.Single DungeonBattlePlacementExcelData::GetBattleAreaStartPositionZ()
extern void DungeonBattlePlacementExcelData_GetBattleAreaStartPositionZ_m0B1B5EA47EBEF643F9ED658EB395CF08DD1381B4 (void);
// 0x0000016C System.Single DungeonBattlePlacementExcelData::GetPin01StartPositionX()
extern void DungeonBattlePlacementExcelData_GetPin01StartPositionX_m9DE3056D0A3A196962CC039B0AEEF5BFA5F2F01E (void);
// 0x0000016D System.Single DungeonBattlePlacementExcelData::GetPin01StartPositionY()
extern void DungeonBattlePlacementExcelData_GetPin01StartPositionY_m22B645FADFB8BF3E646224987026EBE28212E9F8 (void);
// 0x0000016E System.Single DungeonBattlePlacementExcelData::GetPin01StartPositionZ()
extern void DungeonBattlePlacementExcelData_GetPin01StartPositionZ_mCC3FAC3FCDCDBF9413AAAFE1DFAA06B2CC18562D (void);
// 0x0000016F System.Single DungeonBattlePlacementExcelData::GetPin01StartScale()
extern void DungeonBattlePlacementExcelData_GetPin01StartScale_m807E21A850DAEA0930930E4FE0E0AEE08FFCA83F (void);
// 0x00000170 System.Single DungeonBattlePlacementExcelData::GetPin01StartRotation()
extern void DungeonBattlePlacementExcelData_GetPin01StartRotation_m7811808B0F2D7AD449F1F77DDB52632D6D33F611 (void);
// 0x00000171 System.Single DungeonBattlePlacementExcelData::GetPin02StartPositionX()
extern void DungeonBattlePlacementExcelData_GetPin02StartPositionX_m392E896523154E0FA541B002D6449221DDF68807 (void);
// 0x00000172 System.Single DungeonBattlePlacementExcelData::GetPin02StartPositionY()
extern void DungeonBattlePlacementExcelData_GetPin02StartPositionY_m3B58DDFA24087EBE89F25781416558C002FABA67 (void);
// 0x00000173 System.Single DungeonBattlePlacementExcelData::GetPin02StartPositionZ()
extern void DungeonBattlePlacementExcelData_GetPin02StartPositionZ_m66E049F7F9C1E51EDE1B700A36195EF4FF9820DC (void);
// 0x00000174 System.Single DungeonBattlePlacementExcelData::GetPin02StartScale()
extern void DungeonBattlePlacementExcelData_GetPin02StartScale_m3F491479CADD02E97B13A04CAD8FEDF506F02529 (void);
// 0x00000175 System.Single DungeonBattlePlacementExcelData::GetPin02StartRotation()
extern void DungeonBattlePlacementExcelData_GetPin02StartRotation_m615A5ACE6AFE45DA005184C97425F6F3A0872404 (void);
// 0x00000176 System.Single DungeonBattlePlacementExcelData::GetPin03StartPositionX()
extern void DungeonBattlePlacementExcelData_GetPin03StartPositionX_mF2FD048EDA3D8EC7C7D7C65DF3BBF2ACC4A42460 (void);
// 0x00000177 System.Single DungeonBattlePlacementExcelData::GetPin03StartPositionY()
extern void DungeonBattlePlacementExcelData_GetPin03StartPositionY_m3E662529CB13FBBD27F026E7629288C17A8AEB1A (void);
// 0x00000178 System.Single DungeonBattlePlacementExcelData::GetPin03StartPositionZ()
extern void DungeonBattlePlacementExcelData_GetPin03StartPositionZ_m1C6BC309439028C0538D8FE4FAFF0B98F9170F7C (void);
// 0x00000179 System.Single DungeonBattlePlacementExcelData::GetPin03StartScale()
extern void DungeonBattlePlacementExcelData_GetPin03StartScale_mBDFFDFB9463B6A6DE2B8F38F297D5CCFA6E14CE8 (void);
// 0x0000017A System.Single DungeonBattlePlacementExcelData::GetPin03StartRotation()
extern void DungeonBattlePlacementExcelData_GetPin03StartRotation_m9F436FBF167157EB64DEEDC3C921A85DD0A3FACB (void);
// 0x0000017B System.Single DungeonBattlePlacementExcelData::GetPin04StartPositionX()
extern void DungeonBattlePlacementExcelData_GetPin04StartPositionX_mD5D12B89159D6FD090BCB2B560BFBEF9B8A5D15D (void);
// 0x0000017C System.Single DungeonBattlePlacementExcelData::GetPin04StartPositionY()
extern void DungeonBattlePlacementExcelData_GetPin04StartPositionY_m89EF93AA23CAE5BEC23AF0F860C60FE7D0D9077E (void);
// 0x0000017D System.Single DungeonBattlePlacementExcelData::GetPin04StartPositionZ()
extern void DungeonBattlePlacementExcelData_GetPin04StartPositionZ_m4F5CF00BE3D8EB3683AFA856DF5DC4DA3C56D510 (void);
// 0x0000017E System.Single DungeonBattlePlacementExcelData::GetPin04StartScale()
extern void DungeonBattlePlacementExcelData_GetPin04StartScale_m18F9E8630EC76AC41D3CECD026EDA9A68A5F206F (void);
// 0x0000017F System.Single DungeonBattlePlacementExcelData::GetPin04StartRotation()
extern void DungeonBattlePlacementExcelData_GetPin04StartRotation_m5364FE9F91AD5F977AF5319D225DC305C5EAFA6A (void);
// 0x00000180 System.Single DungeonBattlePlacementExcelData::GetPin05StartPositionX()
extern void DungeonBattlePlacementExcelData_GetPin05StartPositionX_mE8768F2AE0EE1652962A30A11AAA3D7BCA48771A (void);
// 0x00000181 System.Single DungeonBattlePlacementExcelData::GetPin05StartPositionY()
extern void DungeonBattlePlacementExcelData_GetPin05StartPositionY_mE95353EA85F04679C6F0E22EB5A50A7023D6BABA (void);
// 0x00000182 System.Single DungeonBattlePlacementExcelData::GetPin05StartPositionZ()
extern void DungeonBattlePlacementExcelData_GetPin05StartPositionZ_m130EF2727FF992D1298435389DCEF7CB1ED6DF99 (void);
// 0x00000183 System.Single DungeonBattlePlacementExcelData::GetPin05StartScale()
extern void DungeonBattlePlacementExcelData_GetPin05StartScale_m8E020DB7D914F19C5F8C580777A008742C97283D (void);
// 0x00000184 System.Single DungeonBattlePlacementExcelData::GetPin05StartRotation()
extern void DungeonBattlePlacementExcelData_GetPin05StartRotation_m53F887286A478CB95060B88FA33F3DF191028903 (void);
// 0x00000185 System.Single DungeonBattlePlacementExcelData::GetPin06StartPositionX()
extern void DungeonBattlePlacementExcelData_GetPin06StartPositionX_m3DD2042B11A73503A7EBF8EA8720A8053DEB6349 (void);
// 0x00000186 System.Single DungeonBattlePlacementExcelData::GetPin06StartPositionY()
extern void DungeonBattlePlacementExcelData_GetPin06StartPositionY_m458BD68204109AD41CA10BE81C9B5BE7CFDCCE96 (void);
// 0x00000187 System.Single DungeonBattlePlacementExcelData::GetPin06StartPositionZ()
extern void DungeonBattlePlacementExcelData_GetPin06StartPositionZ_m524D07AC42AB905CB910DAC6917D3E75B18F45AF (void);
// 0x00000188 System.Single DungeonBattlePlacementExcelData::GetPin06StartScale()
extern void DungeonBattlePlacementExcelData_GetPin06StartScale_mD795D4DFFCC1AAA57FD48722256CCFEA12504FD2 (void);
// 0x00000189 System.Single DungeonBattlePlacementExcelData::GetPin06StartRotation()
extern void DungeonBattlePlacementExcelData_GetPin06StartRotation_m2DB5EFA39067B5BB49ACF08E75F20034C1176C8E (void);
// 0x0000018A System.Void DungeonBattlePlacementExcelData::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void DungeonBattlePlacementExcelData_ManualSetUp_mFDE90E7758D6569F10736AAE2A9D705031A2BC17 (void);
// 0x0000018B System.Void DungeonBattlePlacementExcelData::.ctor()
extern void DungeonBattlePlacementExcelData__ctor_mA6983F2437F20A8CEB3C845009B78927B8DBC86B (void);
// 0x0000018C System.Int32 DungeonBattlePlacementGamelData::GetIndex()
extern void DungeonBattlePlacementGamelData_GetIndex_mD9D90C795C2B464FD2AC9808F70D5125EA1F1C42 (void);
// 0x0000018D System.Int32 DungeonBattlePlacementGamelData::GetID()
extern void DungeonBattlePlacementGamelData_GetID_mEB22E863E0C4ABCCE156B1C295F0FD79E54766F3 (void);
// 0x0000018E System.Single DungeonBattlePlacementGamelData::GetBattleAreaLength()
extern void DungeonBattlePlacementGamelData_GetBattleAreaLength_m08ED6E9CD2B5CE6344A402DD4D5170C9A90F1E10 (void);
// 0x0000018F System.Single DungeonBattlePlacementGamelData::GetBattleAreaStartPositionX()
extern void DungeonBattlePlacementGamelData_GetBattleAreaStartPositionX_mB89D3C454A4393076FB202D285EE10F5B472CCBF (void);
// 0x00000190 System.Single DungeonBattlePlacementGamelData::GetBattleAreaStartPositionY()
extern void DungeonBattlePlacementGamelData_GetBattleAreaStartPositionY_mD3B6C05519368071F87535F5F8B93A314C09C8F1 (void);
// 0x00000191 System.Single DungeonBattlePlacementGamelData::GetBattleAreaStartPositionZ()
extern void DungeonBattlePlacementGamelData_GetBattleAreaStartPositionZ_m248CBA87BB4FDF677BD181C3B2F57306FD43FC58 (void);
// 0x00000192 System.Single DungeonBattlePlacementGamelData::GetPin01StartPositionX()
extern void DungeonBattlePlacementGamelData_GetPin01StartPositionX_mFB417D47954A1B741D1B40A6B0BA1B203F891889 (void);
// 0x00000193 System.Single DungeonBattlePlacementGamelData::GetPin01StartPositionY()
extern void DungeonBattlePlacementGamelData_GetPin01StartPositionY_mBDA233D8D5D2DF8F1E66C841573E0C22CC286EC3 (void);
// 0x00000194 System.Single DungeonBattlePlacementGamelData::GetPin01StartPositionZ()
extern void DungeonBattlePlacementGamelData_GetPin01StartPositionZ_m9D080840A39F76A884E57A9C9084313B89597EA5 (void);
// 0x00000195 System.Single DungeonBattlePlacementGamelData::GetPin01StartScale()
extern void DungeonBattlePlacementGamelData_GetPin01StartScale_mC4D75C91DADFBAAEF90D68FC6E5613B7EAA78B7C (void);
// 0x00000196 System.Single DungeonBattlePlacementGamelData::GetPin01StartRotation()
extern void DungeonBattlePlacementGamelData_GetPin01StartRotation_mB98831F6C979EEE85F3E2090F514FE59FC6C22D4 (void);
// 0x00000197 System.Single DungeonBattlePlacementGamelData::GetPin02StartPositionX()
extern void DungeonBattlePlacementGamelData_GetPin02StartPositionX_m7E251C88DF8449296F9FC4F7565114508DE199E5 (void);
// 0x00000198 System.Single DungeonBattlePlacementGamelData::GetPin02StartPositionY()
extern void DungeonBattlePlacementGamelData_GetPin02StartPositionY_m87ABE6111F3F5115CAE0499F7E56806EFEED9538 (void);
// 0x00000199 System.Single DungeonBattlePlacementGamelData::GetPin02StartPositionZ()
extern void DungeonBattlePlacementGamelData_GetPin02StartPositionZ_m0564ADDFF204AA22E40FDE618798134F00E1DD61 (void);
// 0x0000019A System.Single DungeonBattlePlacementGamelData::GetPin02StartScale()
extern void DungeonBattlePlacementGamelData_GetPin02StartScale_m86B9ED98E3A2F2D6168A895B922CBF9682120381 (void);
// 0x0000019B System.Single DungeonBattlePlacementGamelData::GetPin02StartRotation()
extern void DungeonBattlePlacementGamelData_GetPin02StartRotation_m9DFE225C050BE9F887244C0BE99AE3D16D06EC05 (void);
// 0x0000019C System.Single DungeonBattlePlacementGamelData::GetPin03StartPositionX()
extern void DungeonBattlePlacementGamelData_GetPin03StartPositionX_m0532D1FA537BEAE8E77901A73F612E13154F97CF (void);
// 0x0000019D System.Single DungeonBattlePlacementGamelData::GetPin03StartPositionY()
extern void DungeonBattlePlacementGamelData_GetPin03StartPositionY_mFAE1FDB535D9F3EE6542C543B7F4D5710A99CE73 (void);
// 0x0000019E System.Single DungeonBattlePlacementGamelData::GetPin03StartPositionZ()
extern void DungeonBattlePlacementGamelData_GetPin03StartPositionZ_m6AC0CD8781E0F9122F6DE89F2A592BCDCA18DE96 (void);
// 0x0000019F System.Single DungeonBattlePlacementGamelData::GetPin03StartScale()
extern void DungeonBattlePlacementGamelData_GetPin03StartScale_m1AFFBE214C39F155F6335BC3E1C37E557111DFB4 (void);
// 0x000001A0 System.Single DungeonBattlePlacementGamelData::GetPin03StartRotation()
extern void DungeonBattlePlacementGamelData_GetPin03StartRotation_mA2D9595E5FEEADEDAF43D71580D688AFFB7AFC60 (void);
// 0x000001A1 System.Single DungeonBattlePlacementGamelData::GetPin04StartPositionX()
extern void DungeonBattlePlacementGamelData_GetPin04StartPositionX_m27F6A176A9E2CA6272D0FE187CA10EEF06540950 (void);
// 0x000001A2 System.Single DungeonBattlePlacementGamelData::GetPin04StartPositionY()
extern void DungeonBattlePlacementGamelData_GetPin04StartPositionY_mCC184A7FC26D01369E861E91F85D8225BAAE0DEB (void);
// 0x000001A3 System.Single DungeonBattlePlacementGamelData::GetPin04StartPositionZ()
extern void DungeonBattlePlacementGamelData_GetPin04StartPositionZ_m34FDB41323D0BB998F45D8053FD4F80119470E2E (void);
// 0x000001A4 System.Single DungeonBattlePlacementGamelData::GetPin04StartScale()
extern void DungeonBattlePlacementGamelData_GetPin04StartScale_mF4AFB31A9CB7C40B5E46C084DC16DDC2DA56F49F (void);
// 0x000001A5 System.Single DungeonBattlePlacementGamelData::GetPin04StartRotation()
extern void DungeonBattlePlacementGamelData_GetPin04StartRotation_m7D3E2913B95C3ADDB48B788850479B8A95367048 (void);
// 0x000001A6 System.Single DungeonBattlePlacementGamelData::GetPin05StartPositionX()
extern void DungeonBattlePlacementGamelData_GetPin05StartPositionX_mDCFD3C05C45B3FE5609653AA9F16038AB5BEEE5E (void);
// 0x000001A7 System.Single DungeonBattlePlacementGamelData::GetPin05StartPositionY()
extern void DungeonBattlePlacementGamelData_GetPin05StartPositionY_mE3B5A283AAB07057D9760F29D2868B2EF0BF48C9 (void);
// 0x000001A8 System.Single DungeonBattlePlacementGamelData::GetPin05StartPositionZ()
extern void DungeonBattlePlacementGamelData_GetPin05StartPositionZ_m5F88E594A2D9588AEE4A6CEC6E8EC8739099B877 (void);
// 0x000001A9 System.Single DungeonBattlePlacementGamelData::GetPin05StartScale()
extern void DungeonBattlePlacementGamelData_GetPin05StartScale_mA047AC1EFA48DD234C25B6A78556374C33EEEDFF (void);
// 0x000001AA System.Single DungeonBattlePlacementGamelData::GetPin05StartRotation()
extern void DungeonBattlePlacementGamelData_GetPin05StartRotation_m358327A9D0CF8EE3C11E045BD0C1361A05E5A489 (void);
// 0x000001AB System.Single DungeonBattlePlacementGamelData::GetPin06StartPositionX()
extern void DungeonBattlePlacementGamelData_GetPin06StartPositionX_mB0A34C54DFDC397FEF249AAF8FAB62D18A483724 (void);
// 0x000001AC System.Single DungeonBattlePlacementGamelData::GetPin06StartPositionY()
extern void DungeonBattlePlacementGamelData_GetPin06StartPositionY_m21C73ACAC098B7DC9A5057BB7C0AEBF4ABF87004 (void);
// 0x000001AD System.Single DungeonBattlePlacementGamelData::GetPin06StartPositionZ()
extern void DungeonBattlePlacementGamelData_GetPin06StartPositionZ_m965C81BDC05A3D5C7D1A2AA121A59971D7DF077F (void);
// 0x000001AE System.Single DungeonBattlePlacementGamelData::GetPin06StartScale()
extern void DungeonBattlePlacementGamelData_GetPin06StartScale_mBB5FFF74568B40A88EE81658FD7AE3B4C1C28712 (void);
// 0x000001AF System.Single DungeonBattlePlacementGamelData::GetPin06StartRotation()
extern void DungeonBattlePlacementGamelData_GetPin06StartRotation_m69F9CE8160E274ACCA1B23366354CABD8DAEDBBC (void);
// 0x000001B0 System.Void DungeonBattlePlacementGamelData::SetValue(DungeonBattlePlacementExcelData)
extern void DungeonBattlePlacementGamelData_SetValue_m02E64C24DFDB3544F16E49F9EDEC539C469E4BBF (void);
// 0x000001B1 System.Void DungeonBattlePlacementGamelData::.ctor()
extern void DungeonBattlePlacementGamelData__ctor_mD46C42317A6346DDB6E54435565D2B796F08A4AF (void);
// 0x000001B2 System.Void DungeonBattlePlacementScriptable::.ctor()
extern void DungeonBattlePlacementScriptable__ctor_mC719FB9B492D2D58A7E4ECE98C8FE86320463679 (void);
// 0x000001B3 System.Int32 DungeonUnitExcelData::GetIndex()
extern void DungeonUnitExcelData_GetIndex_m624B86E2AB43BC757AC3292385D23B7B500DABC5 (void);
// 0x000001B4 System.Int32 DungeonUnitExcelData::GetID()
extern void DungeonUnitExcelData_GetID_mED3718B805AD4C859E9C4226A1C3202872CDD771 (void);
// 0x000001B5 System.Int32 DungeonUnitExcelData::GetUnitID()
extern void DungeonUnitExcelData_GetUnitID_m46EE0EA9A4EEDCE7CAD28377C6414FE3FCD01DD5 (void);
// 0x000001B6 System.Single DungeonUnitExcelData::GetUnitStartPositionX()
extern void DungeonUnitExcelData_GetUnitStartPositionX_m1C9F91305F948113B075A14AFD4D28D5C47C482F (void);
// 0x000001B7 System.Single DungeonUnitExcelData::GetUnitStartPositionY()
extern void DungeonUnitExcelData_GetUnitStartPositionY_mEAF84317FE1BB50EE82332C1361A61C9C9BBBFBF (void);
// 0x000001B8 System.Single DungeonUnitExcelData::GetUnitStartPositionZ()
extern void DungeonUnitExcelData_GetUnitStartPositionZ_m86BE3EFDCE4DBE156781FDCEE9EEB63F8D6D5278 (void);
// 0x000001B9 System.Boolean DungeonUnitExcelData::GetIsBoss()
extern void DungeonUnitExcelData_GetIsBoss_m9967837AD711D43F893C815F24B5C7102C7F10A8 (void);
// 0x000001BA System.Int32 DungeonUnitExcelData::GetRootID()
extern void DungeonUnitExcelData_GetRootID_mC64B8BEB771C94A7CB272955D360D236753D5124 (void);
// 0x000001BB System.Int32 DungeonUnitExcelData::GetMemberID()
extern void DungeonUnitExcelData_GetMemberID_mBF6581BB05A24D2394F916DB6A3905DD76689DB5 (void);
// 0x000001BC System.Int32 DungeonUnitExcelData::GetPlacementID()
extern void DungeonUnitExcelData_GetPlacementID_m6CB3E75719DE93C48CC6238627186A65575128DB (void);
// 0x000001BD System.Void DungeonUnitExcelData::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void DungeonUnitExcelData_ManualSetUp_m16E8D44372CEE59FDBAE83B8604FBBEC0EA66065 (void);
// 0x000001BE System.Void DungeonUnitExcelData::.ctor()
extern void DungeonUnitExcelData__ctor_m488C2ED56B1087CA5BCA1B68A8D1991907A79D19 (void);
// 0x000001BF System.Int32 DungeonUnitGameData::GetIndex()
extern void DungeonUnitGameData_GetIndex_m928E9A5E7908793D458D6E5CF4BB6331BFDEB02C (void);
// 0x000001C0 System.Int32 DungeonUnitGameData::GetID()
extern void DungeonUnitGameData_GetID_m64EE591CFB2D8FFB2E781540079CA893B4DD8E9F (void);
// 0x000001C1 System.Int32 DungeonUnitGameData::GetUnitID()
extern void DungeonUnitGameData_GetUnitID_m62644ADF03CAA95F6CD5AD13AE88B7A2E3777396 (void);
// 0x000001C2 System.Single DungeonUnitGameData::GetUnitStartPositionX()
extern void DungeonUnitGameData_GetUnitStartPositionX_mF03F2B90BB663CD586F76DE0E6C4E40C006D409F (void);
// 0x000001C3 System.Single DungeonUnitGameData::GetUnitStartPositionY()
extern void DungeonUnitGameData_GetUnitStartPositionY_m91BCCF04980C206E7E40FD6A0BC0E8C6316CD8FA (void);
// 0x000001C4 System.Single DungeonUnitGameData::GetUnitStartPositionZ()
extern void DungeonUnitGameData_GetUnitStartPositionZ_m49C2B30118C63537573F43C358FC68276192F4A9 (void);
// 0x000001C5 System.Boolean DungeonUnitGameData::GetIsBoss()
extern void DungeonUnitGameData_GetIsBoss_m25B958847ADE9E0CD1F50CBD68D9DB74DB62E8E0 (void);
// 0x000001C6 System.Int32 DungeonUnitGameData::GetRootID()
extern void DungeonUnitGameData_GetRootID_mFCDFC6040A09BBC4BB8EFEB0DB4B343E5854BC81 (void);
// 0x000001C7 System.Int32 DungeonUnitGameData::GetMemberID()
extern void DungeonUnitGameData_GetMemberID_m3E44244593F8E192827871E71DA4DDAEF81E1A38 (void);
// 0x000001C8 System.Int32 DungeonUnitGameData::GetPlacementID()
extern void DungeonUnitGameData_GetPlacementID_mBF46F41A104A287B98E4886C27D0CD4422341240 (void);
// 0x000001C9 System.Void DungeonUnitGameData::SetValue(DungeonUnitExcelData)
extern void DungeonUnitGameData_SetValue_mE19CF803AEC4E963BB141AD69E6F1F081FA260FB (void);
// 0x000001CA System.Void DungeonUnitGameData::.ctor()
extern void DungeonUnitGameData__ctor_m72D4249F5301A7EF11825D1FC2FEC840F5FE06B6 (void);
// 0x000001CB System.Void DungeonUnitScriptable::.ctor()
extern void DungeonUnitScriptable__ctor_mC57A7A8380B927ECC0DEC2572E87FD332F59D495 (void);
// 0x000001CC System.Boolean DungeonUnitDataStorage::GetIsSetUp()
extern void DungeonUnitDataStorage_GetIsSetUp_m7F550BEE5D09DA8849109B306B6D100177823A12 (void);
// 0x000001CD System.Void DungeonUnitDataStorage::.ctor()
extern void DungeonUnitDataStorage__ctor_mC97C7749D4277994F6198E445209E7774BA04B47 (void);
// 0x000001CE System.Void InputSystem::SetLeftStickVector(UnityEngine.Vector2)
extern void InputSystem_SetLeftStickVector_m5576A041DCD16416A06014AE02398F9AA468489A (void);
// 0x000001CF UnityEngine.Vector2 InputSystem::GetLeftStickVector()
extern void InputSystem_GetLeftStickVector_m7013256D0986B97A93ED6E27DC17FF5F69333249 (void);
// 0x000001D0 System.Void InputSystem::SetRightStickVector(UnityEngine.Vector2)
extern void InputSystem_SetRightStickVector_mEB0341A3C26512106530133B58A26D9614558BBD (void);
// 0x000001D1 UnityEngine.Vector2 InputSystem::GetRightStickVector()
extern void InputSystem_GetRightStickVector_m552448DEE7CCC3182505025030B4E9310D6AD877 (void);
// 0x000001D2 System.Void InputSystem::.ctor()
extern void InputSystem__ctor_m946B1ECB379AD9E7DDC6F7C4266D25CAD676B444 (void);
// 0x000001D3 System.Void CoroutineHandler::StartManualCoroutin(System.Collections.IEnumerator)
extern void CoroutineHandler_StartManualCoroutin_m0B81F0F3BB195CA55D306C7B2E1C615AF05B9467 (void);
// 0x000001D4 System.Void CoroutineHandler::.ctor()
extern void CoroutineHandler__ctor_m2F086A05A90813039FA5EE301F265E8F3F63A9AE (void);
// 0x000001D5 T Singleton`1::get_Instance()
// 0x000001D6 System.Void Singleton`1::.ctor()
// 0x000001D7 System.Void Singleton`1::.cctor()
// 0x000001D8 System.Int32 AccessorError::GetErrorInt()
extern void AccessorError_GetErrorInt_m79D20860467242C0F455B63EFE41B07B5E444ECE (void);
// 0x000001D9 System.String AccessorError::GetErrorString()
extern void AccessorError_GetErrorString_m5EADB5E64FB105CBBEFD41DF5729AED9C4EFB56A (void);
// 0x000001DA System.Void AccessorError::.ctor()
extern void AccessorError__ctor_m7CE516EF63F56B5F9BD984D8106726AA53B95D37 (void);
// 0x000001DB System.Void AccessorError::.cctor()
extern void AccessorError__cctor_mDE6D27875EC57494DFB4ABA95BF34EADCF1ACC69 (void);
// 0x000001DC System.UInt32 AddressableDataContainer::GetListAddressableDataCount()
extern void AddressableDataContainer_GetListAddressableDataCount_m65F06ED08745B5F2C5019F406F03F6F56BE79E6B (void);
// 0x000001DD System.Void AddressableDataContainer::AddAddressableData(BaseAddressableData)
extern void AddressableDataContainer_AddAddressableData_m2909ADEFA4464A4E2BBBF071F8EB208073B74ECE (void);
// 0x000001DE BaseAddressableData AddressableDataContainer::Find(System.UInt32)
extern void AddressableDataContainer_Find_m573ECB7C69A745053E48820F0D74B483DE53FBD8 (void);
// 0x000001DF BaseAddressableData AddressableDataContainer::Find(BaseAddressableData)
extern void AddressableDataContainer_Find_mA6B982BF0D0ED81A547DD22951E8A0ABF28A28C6 (void);
// 0x000001E0 System.Void AddressableDataContainer::AutoRelease()
extern void AddressableDataContainer_AutoRelease_mCEAA203D8D7F96390B5E23F5C121E831FEE7084A (void);
// 0x000001E1 System.Void AddressableDataContainer::.ctor()
extern void AddressableDataContainer__ctor_m67772C123FDCCAA604765FB1E4DA927B4CE6637C (void);
// 0x000001E2 System.Void AddressableDataContainer_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mA7A4F177942BE15F86E295F46847B17B13F0ACB7 (void);
// 0x000001E3 System.Boolean AddressableDataContainer_<>c__DisplayClass4_0::<Find>b__0(BaseAddressableData)
extern void U3CU3Ec__DisplayClass4_0_U3CFindU3Eb__0_m8D775BAF9052EE263595B2D70086396C456A173F (void);
// 0x000001E4 System.Void AddressableDataContainer_<>c::.cctor()
extern void U3CU3Ec__cctor_m0521BFA844A6EA00E52D557058D07069FE6B1833 (void);
// 0x000001E5 System.Void AddressableDataContainer_<>c::.ctor()
extern void U3CU3Ec__ctor_mEA8F568783FA58C38E54B98B1AA1C1295F5DAC2A (void);
// 0x000001E6 System.Boolean AddressableDataContainer_<>c::<AutoRelease>b__5_0(BaseAddressableData)
extern void U3CU3Ec_U3CAutoReleaseU3Eb__5_0_mC61831B359AE286BA23C107DFF6564F8299858F9 (void);
// 0x000001E7 System.Boolean AddressableDataContainer_<>c::<AutoRelease>b__5_1(BaseAddressableData)
extern void U3CU3Ec_U3CAutoReleaseU3Eb__5_1_m58292BCAA065AF8BDD4DEBF08BBDFDD0389A705B (void);
// 0x000001E8 AddressableDataCore AddressableDataCore::get_Instance()
extern void AddressableDataCore_get_Instance_m41609607B2AA92295DF1E52184586343EFF5395A (void);
// 0x000001E9 AddressableObject`1<T> AddressableDataCore::CreateAddressable(System.String)
// 0x000001EA AddressableDataContainer AddressableDataCore::GetAddressableDataContainer()
extern void AddressableDataCore_GetAddressableDataContainer_m63851DA0606EC7208327DADDFC3CB0DA94AF8E6C (void);
// 0x000001EB System.Void AddressableDataCore::Start()
extern void AddressableDataCore_Start_mE0B2481170F043F52BF5E70717D3E093B50F5107 (void);
// 0x000001EC System.Void AddressableDataCore::Update()
extern void AddressableDataCore_Update_mFC2EBE6DBB11DC4B53C343D70D30CF347BB2733E (void);
// 0x000001ED System.Void AddressableDataCore::AddAddressableData(BaseAddressableData)
extern void AddressableDataCore_AddAddressableData_m5AC94F4285749169D34A8CC39A15679E648F96D0 (void);
// 0x000001EE BaseAddressableData AddressableDataCore::Find(System.UInt32)
extern void AddressableDataCore_Find_m1EB3FA24AF37C1B539FA7413FB52BFC9F4838FA2 (void);
// 0x000001EF BaseAddressableData AddressableDataCore::Find(BaseAddressableData)
extern void AddressableDataCore_Find_m3B4853AFE4E7707E60E51CE0D86C62D67B07E193 (void);
// 0x000001F0 System.Void AddressableDataCore::AutoRelease()
extern void AddressableDataCore_AutoRelease_m17F44E06CEC9881C8854B06BF746223FE2456EC0 (void);
// 0x000001F1 System.Void AddressableDataCore::.ctor()
extern void AddressableDataCore__ctor_mF9CE47CDCD270E933885A60894A1BC905CC46B2F (void);
// 0x000001F2 System.Void AddressableDataCore::.cctor()
extern void AddressableDataCore__cctor_m564761740126505851A0F0491F1FDD645B27CC12 (void);
// 0x000001F3 System.Void AddressableDataGameObject::.ctor()
extern void AddressableDataGameObject__ctor_mB018D7EEA04C8D499BECF1B4C120D88EBAC7A68A (void);
// 0x000001F4 System.Boolean AddressableObject`1::get_IsSetUp()
// 0x000001F5 T AddressableObject`1::get_GetObject()
// 0x000001F6 System.Void AddressableObject`1::set_SetPath(System.String)
// 0x000001F7 System.Void AddressableObject`1::.ctor(System.String)
// 0x000001F8 System.Void AddressableObject`1::LoadStart()
// 0x000001F9 System.Void AddressableObject`1::LoadStart(UnityEngine.Object)
// 0x000001FA UnityEngine.GameObject AddressableObject`1::Instance()
// 0x000001FB System.Void AddressableObject`1::Release()
// 0x000001FC System.Void AddressableObject`1::<LoadStart>b__12_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T>)
// 0x000001FD System.Void AddressableObject`1_<>c__DisplayClass13_0::.ctor()
// 0x000001FE System.Void AddressableObject`1_<>c__DisplayClass13_0::<LoadStart>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T>)
// 0x000001FF System.Void BaseAddressableData::.ctor()
extern void BaseAddressableData__ctor_mE188F47F6143FCBEC9A0167A2B01E4B3DB7ED4EE (void);
// 0x00000200 System.Boolean BaseAddressableData::GetFlagAutoRelease()
extern void BaseAddressableData_GetFlagAutoRelease_m3E93BF211C34895911EEE0B68446D9CEC111FD9F (void);
// 0x00000201 System.Void BaseAddressableData::OnAutoRelease()
extern void BaseAddressableData_OnAutoRelease_mE6B103A1AD7A6366DCFB039EC36721678AE1A628 (void);
// 0x00000202 System.Boolean BaseAddressableData::GetIsUse()
extern void BaseAddressableData_GetIsUse_m9A3C6CA8F2003E6C94A789AFCF2A8C315B3F4EC4 (void);
// 0x00000203 System.Void BaseAddressableData::OnIsUse()
extern void BaseAddressableData_OnIsUse_mFAC450C7A184C56530632725CB6F8D28CE9CFF5A (void);
// 0x00000204 System.Boolean BaseAddressableData::GetFlagSetUpLoading()
extern void BaseAddressableData_GetFlagSetUpLoading_m80D5C3CE4C50AC601A2DB606780A5B0B43A2693B (void);
// 0x00000205 UnityEngine.Object BaseAddressableData::GetBaseAddressableData()
extern void BaseAddressableData_GetBaseAddressableData_mEB39B822373FB4119DB95A4F392927B42D8CB53A (void);
// 0x00000206 UnityEngine.Object[] BaseAddressableData::GetBaseArrayAddressableData()
extern void BaseAddressableData_GetBaseArrayAddressableData_mC6AACE86BDF62176612E2514AB89CF7E9F1D00C9 (void);
// 0x00000207 UnityEngine.Object BaseAddressableData::GetBaseArrayAddressableData(System.Int32)
extern void BaseAddressableData_GetBaseArrayAddressableData_m087AEFC6428011C99F74193E09B6D3DE2E70A585 (void);
// 0x00000208 System.UInt32 BaseAddressableData::GetArrayAddressableDataCount()
extern void BaseAddressableData_GetArrayAddressableDataCount_m988288920074A0DC7E7AED9CD9533D8CBF37B0F8 (void);
// 0x00000209 System.Void BaseAddressableData::LoadStart(System.String)
extern void BaseAddressableData_LoadStart_m06DD6FF2019816A6BC00A8587FA0C5E3C3B2BC29 (void);
// 0x0000020A System.Void BaseAddressableData::LoadArrayStart(System.String)
extern void BaseAddressableData_LoadArrayStart_m1094C083A1ABE1A62965458B46F92F5EE2EC8F48 (void);
// 0x0000020B System.Void BaseAddressableData::Release()
extern void BaseAddressableData_Release_mE1CAB646B061897499234B25A7B554ADB96570DC (void);
// 0x0000020C System.Void AddressableData`1::.ctor()
// 0x0000020D T AddressableData`1::GetAddressableData()
// 0x0000020E T AddressableData`1::GetArrayAddressableData(System.Int32)
// 0x0000020F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T> AddressableData`1::GetAddressableHandle()
// 0x00000210 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<T>> AddressableData`1::GetArrayAddressableHandle()
// 0x00000211 System.Void AddressableData`1::LoadStart(System.String)
// 0x00000212 System.Void AddressableData`1::LoadArrayStart(System.String)
// 0x00000213 System.Void AddressableData`1::Release()
// 0x00000214 System.Void AddressableData`1::<LoadStart>b__7_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T>)
// 0x00000215 System.Void AddressableData`1::<LoadArrayStart>b__8_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<T>>)
// 0x00000216 System.String AnimationConvert::GetAnimationName(AnimationConvert_AnimationState)
extern void AnimationConvert_GetAnimationName_m01E918D19210D6D4A66EE049DC986879DDE56D77 (void);
// 0x00000217 System.Void AnimationConvert::.ctor()
extern void AnimationConvert__ctor_m721EBA22230D6AA9F98FBA8FC9A6E25D7FDEC070 (void);
// 0x00000218 System.Void BulletAction::SetUp(UnityEngine.GameObject,BulletGameData,System.Collections.Generic.List`1<BattleCollisionGameData>)
extern void BulletAction_SetUp_mBF05AF8AF21B0FD82377FD96FD358030AA1EEF02 (void);
// 0x00000219 System.Void BulletAction::Start()
extern void BulletAction_Start_m2E6D9B4B85B57782E77A38C113C9629A0FAB78CB (void);
// 0x0000021A System.Void BulletAction::Update()
extern void BulletAction_Update_m26C2B94B2C5025D79B8E921C006EDF90330C2124 (void);
// 0x0000021B System.Void BulletAction::HitSecond(UnityEngine.Collider,UnityEngine.Collider)
extern void BulletAction_HitSecond_m7A7CAB2C44BF9DB60A3F2EA79A0B0649905AFA15 (void);
// 0x0000021C System.Void BulletAction::.ctor()
extern void BulletAction__ctor_m8A4DBC09046F89FB7910A863DA4FB0E7AE283479 (void);
// 0x0000021D System.Void BaseBulletRole::Init(BulletGameData,UnityEngine.Transform)
extern void BaseBulletRole_Init_mBAF1E4405516FA10F811D908708E42E68635A6BE (void);
// 0x0000021E System.Void BaseBulletRole::Update()
extern void BaseBulletRole_Update_mFEA7DCA97A1920E6039EBC47CA04637363487C56 (void);
// 0x0000021F System.Void BaseBulletRole::KeyFrameUpdate()
extern void BaseBulletRole_KeyFrameUpdate_mECB576573497B7518CCA4EF066575270FC9CD4B6 (void);
// 0x00000220 System.Void BaseBulletRole::RoleUpdate()
extern void BaseBulletRole_RoleUpdate_m60DFAFE5A843BA3DB34D61AE09EEA0B9D7BE609F (void);
// 0x00000221 System.Void BaseBulletRole::.ctor()
extern void BaseBulletRole__ctor_mF26BE3ABD6DEADA3411AC13BC67D654C455EFCCE (void);
// 0x00000222 System.Boolean BulletCore::GetIsSetUP()
extern void BulletCore_GetIsSetUP_m1D126010AACD78D9446A8FFCF30F3FD54D596823 (void);
// 0x00000223 System.Void BulletCore::Start()
extern void BulletCore_Start_m010E609F5A4F556CE6222CED72FBA150D404F67A (void);
// 0x00000224 System.Void BulletCore::Update()
extern void BulletCore_Update_m98BA92D03740C1CA1EE7503A88F909C731A504AB (void);
// 0x00000225 System.Void BulletCore::LoadBulletData(System.Int32)
extern void BulletCore_LoadBulletData_m0F8BFDB3BA1A50E05401CD54F3C4C38F556C4F24 (void);
// 0x00000226 System.Collections.Generic.List`1<BulletGameData> BulletCore::GetBulletData(System.Int32,System.Int32)
extern void BulletCore_GetBulletData_m3025D757944F3E93E4513C9BAB74C99A01F46420 (void);
// 0x00000227 System.Collections.Generic.List`1<System.Int32> BulletCore::GetBulletCollisionID(System.Int32,System.Int32)
extern void BulletCore_GetBulletCollisionID_m089C933ED8B08ACBD462052C58D0703D505A80F6 (void);
// 0x00000228 System.Collections.Generic.List`1<System.Int32> BulletCore::GetBulletEffectID(System.Int32,System.Int32)
extern void BulletCore_GetBulletEffectID_m534823DD703992A8EC4E916AA5174CC9B9E83006 (void);
// 0x00000229 System.Void BulletCore::.ctor()
extern void BulletCore__ctor_m55CBEACA9D3EACD91827330EEA5937C3FA4BF9A2 (void);
// 0x0000022A System.Void BulletCreate::Appearance(System.Int32,System.Int32,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void BulletCreate_Appearance_m189D1066F79941FCD843FE21BBB09FADF9B3041A (void);
// 0x0000022B System.Int32 BulletDataExcel::GetIndex()
extern void BulletDataExcel_GetIndex_mA46261A3DF2D8BEE166DA07DD2959E5B88673BE2 (void);
// 0x0000022C System.Int32 BulletDataExcel::GetBulletID()
extern void BulletDataExcel_GetBulletID_mA900F479C07C79D617BE9F5B6D5392FE38B24EE4 (void);
// 0x0000022D BulletDataExcel_BulletType BulletDataExcel::GetBulletType()
extern void BulletDataExcel_GetBulletType_m852084F50F799AAF2AE1289C3AEEEAD43AC8F169 (void);
// 0x0000022E System.Single BulletDataExcel::GetLifeFrame()
extern void BulletDataExcel_GetLifeFrame_m887AB9C712F7304A84AEFC6A15C017202DF8A5AD (void);
// 0x0000022F System.Single BulletDataExcel::GetFloat01()
extern void BulletDataExcel_GetFloat01_m3BFB7D3D93406018917F05734B10227ABEA9D4CB (void);
// 0x00000230 System.Single BulletDataExcel::GetFloat02()
extern void BulletDataExcel_GetFloat02_m668D43095A54FDBE3DE846C3D0DAECA5EA637F7E (void);
// 0x00000231 System.Single BulletDataExcel::GetPositionX()
extern void BulletDataExcel_GetPositionX_m7F22D5A4AB3FABF4F76097E00CD702F85D634283 (void);
// 0x00000232 System.Single BulletDataExcel::GetPositionY()
extern void BulletDataExcel_GetPositionY_m8F59CC2E7D25D76F755C29EEEC0CBB68D7F73035 (void);
// 0x00000233 System.Single BulletDataExcel::GetPositionZ()
extern void BulletDataExcel_GetPositionZ_m7285D96F38ADCC8D56A58E2EEE84366F6390E83D (void);
// 0x00000234 System.Int32 BulletDataExcel::GetBulletEffectID()
extern void BulletDataExcel_GetBulletEffectID_m1946B9A47171D982EC91BB182AB3F8AFBB1F54A3 (void);
// 0x00000235 System.Int32 BulletDataExcel::GetBulletCollisionID()
extern void BulletDataExcel_GetBulletCollisionID_m6E53C0D0D6B583616B459AA758DE1E2F6360175A (void);
// 0x00000236 System.Int32 BulletDataExcel::GetBulletSkillID()
extern void BulletDataExcel_GetBulletSkillID_mF63FB8F3B8E624D1D3D942A4110ECE17DD5E0B22 (void);
// 0x00000237 BulletDataExcel_BulletCollisionRole BulletDataExcel::GetBulletAttackCollisionRole()
extern void BulletDataExcel_GetBulletAttackCollisionRole_m80EE761B6B66A4B37C0558EC8BAE41C8CFDF21DB (void);
// 0x00000238 System.Int32 BulletDataExcel::GetBulletHitAttackCollisionNextID()
extern void BulletDataExcel_GetBulletHitAttackCollisionNextID_m549261BB5560E0E63FEAC181824C3D98A09F6CB6 (void);
// 0x00000239 BulletDataExcel_BulletCollisionRole BulletDataExcel::GetBulletBackGroundCollisionRole()
extern void BulletDataExcel_GetBulletBackGroundCollisionRole_mFC80EAF19C085517092895C70729A06E08CFF6EB (void);
// 0x0000023A System.Int32 BulletDataExcel::GetBulletHitBackGroundCollisionNextID()
extern void BulletDataExcel_GetBulletHitBackGroundCollisionNextID_m00611223247C8B4A943F495209A041AD3D1F0C2D (void);
// 0x0000023B System.Void BulletDataExcel::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void BulletDataExcel_ManualSetUp_mAABD4D5061AC751FFBF3EA1718CD66B9BB971079 (void);
// 0x0000023C System.Void BulletDataExcel::.ctor()
extern void BulletDataExcel__ctor_m3AD001FA730727CC9D77F1F49A422BB6608DC16A (void);
// 0x0000023D System.Void BulletDataScriptable::.ctor()
extern void BulletDataScriptable__ctor_m69A40930BFFB92DB09E1646B8EE55D05359E941B (void);
// 0x0000023E System.Int32 BulletGameData::GetIndex()
extern void BulletGameData_GetIndex_m967CC9430B4EE9701F2F05E601784853A9B485AD (void);
// 0x0000023F System.Int32 BulletGameData::GetBulletID()
extern void BulletGameData_GetBulletID_m2CCF00E8446B24E9F88E640429D587CA5A226B61 (void);
// 0x00000240 BulletDataExcel_BulletType BulletGameData::GetBulletType()
extern void BulletGameData_GetBulletType_mD9633E4E5FE3C31CBC9B25E6B6BC7D1E0BC3380D (void);
// 0x00000241 System.Single BulletGameData::GetLifeFrame()
extern void BulletGameData_GetLifeFrame_m9589E20674DDD003B8A7523B9A759EE5126D26C3 (void);
// 0x00000242 System.Single BulletGameData::GetFloat01()
extern void BulletGameData_GetFloat01_m6A40EA1BF8C233CF7FEAA067DD6F88C28D8852F9 (void);
// 0x00000243 System.Single BulletGameData::GetFloat02()
extern void BulletGameData_GetFloat02_mEC1112E78D3DF4BD5182F419A8263F9054943734 (void);
// 0x00000244 System.Single BulletGameData::GetPositionX()
extern void BulletGameData_GetPositionX_m0CB657A9856A1BEE4844A383D5102B897F5EC8A9 (void);
// 0x00000245 System.Single BulletGameData::GetPositionY()
extern void BulletGameData_GetPositionY_m6C91A594EC110088E66022C23094F03E2AB40F5F (void);
// 0x00000246 System.Single BulletGameData::GetPositionZ()
extern void BulletGameData_GetPositionZ_mE9C1C33DD8A74861AC9C0FBCA5A4B123C5519BF6 (void);
// 0x00000247 System.Int32 BulletGameData::GetBulletEffectID()
extern void BulletGameData_GetBulletEffectID_m8B96FEBEBA2376AFA357E9A5E200A347816725C4 (void);
// 0x00000248 System.Int32 BulletGameData::GetBulletCollisionID()
extern void BulletGameData_GetBulletCollisionID_m01ED1A70D5EE225DC6C9E0234F10B9EBC18D68F6 (void);
// 0x00000249 System.Int32 BulletGameData::GetBulletSkillID()
extern void BulletGameData_GetBulletSkillID_mA9F814C5C6A5639A647761258C9F65EBF1AD7B5D (void);
// 0x0000024A BulletDataExcel_BulletCollisionRole BulletGameData::GetBulletAttackCollisionRole()
extern void BulletGameData_GetBulletAttackCollisionRole_m3154AEECF509F24AFE95ABA836E69A5F9D096CB5 (void);
// 0x0000024B System.Int32 BulletGameData::GetBulletHitAttackCollisionNextID()
extern void BulletGameData_GetBulletHitAttackCollisionNextID_m4AC16006CC36F7771A17DF97FB565D6A17006E78 (void);
// 0x0000024C BulletDataExcel_BulletCollisionRole BulletGameData::GetBulletBackGroundCollisionRole()
extern void BulletGameData_GetBulletBackGroundCollisionRole_mD4B2ED32BF3C4A94AC13F899D77DFF970C71DFCD (void);
// 0x0000024D System.Int32 BulletGameData::GetBulletHitBackGroundCollisionNextID()
extern void BulletGameData_GetBulletHitBackGroundCollisionNextID_m75C710CBFE13BB4C7D4623AB88AD82E559181401 (void);
// 0x0000024E System.Void BulletGameData::SetValue(BulletDataExcel)
extern void BulletGameData_SetValue_m3CF2B1F0AC7781C028BD6B033B5DE4089771FB54 (void);
// 0x0000024F System.Void BulletGameData::.ctor()
extern void BulletGameData__ctor_m02A8F81FCBBBE016232DE78E7022401B7200B204 (void);
// 0x00000250 System.Collections.Generic.List`1<BulletGameData> BulletDataStorage::GetAllBulletGameData()
extern void BulletDataStorage_GetAllBulletGameData_mCAF83799EE28D857EDCA7FAE8EC1AAB26F5F24A2 (void);
// 0x00000251 System.Void BulletDataStorage::.ctor()
extern void BulletDataStorage__ctor_mD8D6754B4511EDB6187E872395A6EE48C8DAA42F (void);
// 0x00000252 System.Void BulletDataStorage::AddBulletData(System.Collections.Generic.List`1<BulletDataExcel>)
extern void BulletDataStorage_AddBulletData_m2AC3A305A06274FF9417F38622DE5392A5E70120 (void);
// 0x00000253 System.Collections.Generic.List`1<BulletGameData> BulletDataStorage::GetBulletData(System.Int32)
extern void BulletDataStorage_GetBulletData_mBE5D776DE91F83093DDC823545146A9F7870AB85 (void);
// 0x00000254 System.Collections.Generic.List`1<System.Int32> BulletDataStorage::GetBulletCollisionID(System.Int32)
extern void BulletDataStorage_GetBulletCollisionID_m2DB11CAA26AE420FA916457AE06C18F910BEE31D (void);
// 0x00000255 System.Collections.Generic.List`1<System.Int32> BulletDataStorage::GetBulletEffectID(System.Int32)
extern void BulletDataStorage_GetBulletEffectID_m709CC023EF8F6421CC510C04BCC989BBE2A13828 (void);
// 0x00000256 System.Void BulletDataStorage::ReleaseAll()
extern void BulletDataStorage_ReleaseAll_mAD57069463EFA156C357D3F64EDC3434E2BF4F98 (void);
// 0x00000257 System.Boolean BulletDataStorageManager::GetIsSetUp()
extern void BulletDataStorageManager_GetIsSetUp_mAFBE2164ECC2A8956B5AC0897E395A9BDAF772FB (void);
// 0x00000258 System.Void BulletDataStorageManager::LoadBulletData(System.Int32)
extern void BulletDataStorageManager_LoadBulletData_mA9244B8F73C48B7B6B53259EB946BE18BBBE58DF (void);
// 0x00000259 System.Void BulletDataStorageManager::Update()
extern void BulletDataStorageManager_Update_mD8C9A49BB8D3C5F64BF7B2C538CD10F05E05944C (void);
// 0x0000025A System.Collections.Generic.List`1<BulletGameData> BulletDataStorageManager::GetBulletData(System.Int32,System.Int32)
extern void BulletDataStorageManager_GetBulletData_m667574A5D0423BE37A436B8EA3F7D36CA9CB2ECC (void);
// 0x0000025B System.Collections.Generic.List`1<System.Int32> BulletDataStorageManager::GetBulletCollisionID(System.Int32,System.Int32)
extern void BulletDataStorageManager_GetBulletCollisionID_mF046AD57E87F3E2C889AA4ABFF9D21C5EF137023 (void);
// 0x0000025C System.Collections.Generic.List`1<System.Int32> BulletDataStorageManager::GetBulletEffectID(System.Int32,System.Int32)
extern void BulletDataStorageManager_GetBulletEffectID_mEB5FD880FE0448C9645E0A289545212A723E3F7A (void);
// 0x0000025D System.Void BulletDataStorageManager::SetUp()
extern void BulletDataStorageManager_SetUp_m58EE33D95435E822D63798CFA3C4747331AD6B37 (void);
// 0x0000025E System.Void BulletDataStorageManager::.ctor()
extern void BulletDataStorageManager__ctor_m802DF2E6E215C7CEAF79A4ECC1EB14A7294B7053 (void);
// 0x0000025F System.Boolean BattleCollisionCore::GetIsSetUP()
extern void BattleCollisionCore_GetIsSetUP_m6B7BD6FB161AAD6D9FCC9BF2D74173820434D4CE (void);
// 0x00000260 System.Void BattleCollisionCore::Start()
extern void BattleCollisionCore_Start_m2D277AE93789CAA8CD9FE6E5C980D9D6E0CC0C9C (void);
// 0x00000261 System.Void BattleCollisionCore::Update()
extern void BattleCollisionCore_Update_m8BAFE23628D7578CF2ABEBB666CC27D993EB0B62 (void);
// 0x00000262 System.Void BattleCollisionCore::LoadUnitCollision(System.Int32)
extern void BattleCollisionCore_LoadUnitCollision_m76464F601587AABF02ECA645B95E8DF799A1B5F9 (void);
// 0x00000263 System.Void BattleCollisionCore::LoadBulletCollision(System.Int32)
extern void BattleCollisionCore_LoadBulletCollision_m61693DF80BE18D3F47BC34098D6E59D0D9D6978C (void);
// 0x00000264 System.Collections.Generic.List`1<BattleCollisionGameData> BattleCollisionCore::GetBattleCollisionGameData(System.Int32,System.Int32)
extern void BattleCollisionCore_GetBattleCollisionGameData_mB6787DF90FE715588FEC8D3C5E040FCB85D988E0 (void);
// 0x00000265 BaseBattleCollisionGameSubstance BattleCollisionCore::CreateAttackCollision(System.Int32,System.Int32,System.Int32,UnityEngine.Vector3,BattleUnit)
extern void BattleCollisionCore_CreateAttackCollision_mAC7919B83DD938BD45A783123E2B77BFEB38CC6F (void);
// 0x00000266 System.Void BattleCollisionCore::OnDestroy()
extern void BattleCollisionCore_OnDestroy_mA94CBD016FDA80CE113B193399B0839103F4C426 (void);
// 0x00000267 System.Void BattleCollisionCore::.ctor()
extern void BattleCollisionCore__ctor_m664101FF29CCC0368C7F4A6711B5FA7B8C9BB85F (void);
// 0x00000268 System.Int32 BattleCollisionData::GetCollisionID()
extern void BattleCollisionData_GetCollisionID_m25DB5286FD9A6D4E6D4056B079BC851A0C480435 (void);
// 0x00000269 System.Single BattleCollisionData::GetFrameLife()
extern void BattleCollisionData_GetFrameLife_m762BA9B2BEB99D1DA19B39D6943A0303BAC4F034 (void);
// 0x0000026A System.Single BattleCollisionData::GetStartFrameChange()
extern void BattleCollisionData_GetStartFrameChange_mFB06EF787124DD3034DE4D3CBF57FE2FB0D4B0D0 (void);
// 0x0000026B System.Single BattleCollisionData::GetEndFrameChange()
extern void BattleCollisionData_GetEndFrameChange_m703D7842547F2CEAE485B3C5B7F5D46F67F1A5AB (void);
// 0x0000026C BattleCollisionData_CollisionType BattleCollisionData::GetBattleCollisionType()
extern void BattleCollisionData_GetBattleCollisionType_mA6AE20E4CA303B35F2B0E78776B3F49A49BBF64B (void);
// 0x0000026D System.Single BattleCollisionData::GetStartPositionX()
extern void BattleCollisionData_GetStartPositionX_m1BB206AE280541C54D9953043DCF5DDD88D73BEE (void);
// 0x0000026E System.Single BattleCollisionData::GetStartPositionY()
extern void BattleCollisionData_GetStartPositionY_m6558B2A63F295E6C0141965989460DB4A66ED7CD (void);
// 0x0000026F System.Single BattleCollisionData::GetStartPositionZ()
extern void BattleCollisionData_GetStartPositionZ_m0EB65E02701796E3A0E24C0DB766678CE1C3626B (void);
// 0x00000270 System.Single BattleCollisionData::GetEndPositionX()
extern void BattleCollisionData_GetEndPositionX_m4D0A96B4A2EBE55DDF45FFA0F02EC35545F6B9EE (void);
// 0x00000271 System.Single BattleCollisionData::GetEndPositionY()
extern void BattleCollisionData_GetEndPositionY_mDA89D3A40E5D72AAF453E9D59584C40CA3BA1F59 (void);
// 0x00000272 System.Single BattleCollisionData::GetEndPositionZ()
extern void BattleCollisionData_GetEndPositionZ_m6413AB85769B2AA447327DAF74B2DEE643C6A531 (void);
// 0x00000273 System.Single BattleCollisionData::GetStartRadius()
extern void BattleCollisionData_GetStartRadius_m4B04EADC372E1EE31BC8C5F53BF4D37983324B67 (void);
// 0x00000274 System.Single BattleCollisionData::GetEndRadius()
extern void BattleCollisionData_GetEndRadius_m695E61210BDC9008DA8F4B62D2A0622EAC5EDF4E (void);
// 0x00000275 System.Void BattleCollisionData::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void BattleCollisionData_ManualSetUp_mB97FD31BE57BC89759DE9EBEE27ED13A7C32F243 (void);
// 0x00000276 System.Void BattleCollisionData::.ctor()
extern void BattleCollisionData__ctor_m2B66E2966C466A42969EDC78E29D6DDE1F9CFA4A (void);
// 0x00000277 System.Int32 BattleCollisionGameData::GetCollisionID()
extern void BattleCollisionGameData_GetCollisionID_m8953386D401714B92C279DFABCCD1754EAE649FE (void);
// 0x00000278 System.Single BattleCollisionGameData::GetFrameLife()
extern void BattleCollisionGameData_GetFrameLife_mA3C0C6DEF136DC36C97595BCBBB5FB04D545C658 (void);
// 0x00000279 System.Single BattleCollisionGameData::GetStartFrameChange()
extern void BattleCollisionGameData_GetStartFrameChange_mA62DE0B4A0CCF0EDECE0571B147DDC0326B02C92 (void);
// 0x0000027A System.Single BattleCollisionGameData::GetEndFrameChange()
extern void BattleCollisionGameData_GetEndFrameChange_mE9278563C03C2E30AB49B8A2E9B425624E7C0E2F (void);
// 0x0000027B BattleCollisionData_CollisionType BattleCollisionGameData::GetBattleCollisionType()
extern void BattleCollisionGameData_GetBattleCollisionType_m3D8996CC0DA99ADC09E3ED3B6966D4EE95B61580 (void);
// 0x0000027C System.Single BattleCollisionGameData::GetStartPositionX()
extern void BattleCollisionGameData_GetStartPositionX_m5C852E61D79C4760B407914F6133E240DCDA9CD7 (void);
// 0x0000027D System.Single BattleCollisionGameData::GetStartPositionY()
extern void BattleCollisionGameData_GetStartPositionY_mD246C1BFB637E8FD9F6BF4AD2F9A84F3709FB923 (void);
// 0x0000027E System.Single BattleCollisionGameData::GetStartPositionZ()
extern void BattleCollisionGameData_GetStartPositionZ_m64FE81ACD4F1269D3A167686391F703F2FBFC9C9 (void);
// 0x0000027F System.Single BattleCollisionGameData::GetEndPositionX()
extern void BattleCollisionGameData_GetEndPositionX_mD7B26DD81FDA8C83E74AEA8E12D755DB48026354 (void);
// 0x00000280 System.Single BattleCollisionGameData::GetEndPositionY()
extern void BattleCollisionGameData_GetEndPositionY_mA6209D4245CD15BD04356E8E8913D07F9EF81C8F (void);
// 0x00000281 System.Single BattleCollisionGameData::GetEndPositionZ()
extern void BattleCollisionGameData_GetEndPositionZ_mC13086D04411AC1557B6F760486CC4DA608B624C (void);
// 0x00000282 System.Single BattleCollisionGameData::GetStartRadius()
extern void BattleCollisionGameData_GetStartRadius_mED3460D5A37A0AC4DCD13649A7E947040D82309B (void);
// 0x00000283 System.Single BattleCollisionGameData::GetEndRadius()
extern void BattleCollisionGameData_GetEndRadius_m2BE43B2303B12290C93161B17E366D007D79636C (void);
// 0x00000284 System.Void BattleCollisionGameData::DataValue(BattleCollisionData)
extern void BattleCollisionGameData_DataValue_m45A8B4C25CB96B5429975D05FDA037153F5855DA (void);
// 0x00000285 System.Void BattleCollisionGameData::.ctor()
extern void BattleCollisionGameData__ctor_m3022BF99CB12C3F6C5C2C1E1A89055FD4855CEA1 (void);
// 0x00000286 System.Void BattleCollisionScriptableObject::.ctor()
extern void BattleCollisionScriptableObject__ctor_mBE7E3781836297DBD86267484300F2FCCFEE28A2 (void);
// 0x00000287 System.Void BattleCollisionGameDataControl::Init()
extern void BattleCollisionGameDataControl_Init_mF48D19FF9BE6A7D2F0DB5199D7670FF185564F01 (void);
// 0x00000288 System.Void BattleCollisionGameDataControl::AddListMotionData(System.Collections.Generic.List`1<BattleCollisionData>)
extern void BattleCollisionGameDataControl_AddListMotionData_m15D2297041ED7AA40404395F1E27C15D0B1EC2A0 (void);
// 0x00000289 System.Collections.Generic.List`1<BattleCollisionGameData> BattleCollisionGameDataControl::GetBattleCollisionGameData(System.Int32)
extern void BattleCollisionGameDataControl_GetBattleCollisionGameData_mC70F94608FFC4A94A443B7D7CD82ADE87A92BBB5 (void);
// 0x0000028A BattleCollisionGameData BattleCollisionGameDataControl::GetMotionActionData(System.Int32,System.Int32)
extern void BattleCollisionGameDataControl_GetMotionActionData_mCEDFCE0F5E4EB1F2E3EDA3247AEF3A7F2B2A00CF (void);
// 0x0000028B System.UInt32 BattleCollisionGameDataControl::GetMotionActionLength(System.Int32)
extern void BattleCollisionGameDataControl_GetMotionActionLength_m21785F6265197081D50F4DDC82E6A5092154623C (void);
// 0x0000028C System.Void BattleCollisionGameDataControl::Release()
extern void BattleCollisionGameDataControl_Release_m0FE474BFE900CCAB72B8774F488B8073D48BC888 (void);
// 0x0000028D System.Void BattleCollisionGameDataControl::.ctor()
extern void BattleCollisionGameDataControl__ctor_mC526F8016077DAFBD2F2B5E2446B78287108127A (void);
// 0x0000028E System.Boolean BattleCollisionGameDataManagement::GetIsSetUp()
extern void BattleCollisionGameDataManagement_GetIsSetUp_m8491F1D2DDDAE4B79A151907613B2A67206789DF (void);
// 0x0000028F System.Void BattleCollisionGameDataManagement::Init()
extern void BattleCollisionGameDataManagement_Init_m7622D7AF7EF65518161E870E04C3DDCCFFD519F4 (void);
// 0x00000290 System.Void BattleCollisionGameDataManagement::LoadUnitCollision(System.Int32)
extern void BattleCollisionGameDataManagement_LoadUnitCollision_m50125E04A3D32EDE164D26EDE35903728F85C39C (void);
// 0x00000291 System.Void BattleCollisionGameDataManagement::LoadBulletCollision(System.Int32)
extern void BattleCollisionGameDataManagement_LoadBulletCollision_mAEEFE4F0DBE17A8BE36AFC467F443D06C3607B5E (void);
// 0x00000292 System.Void BattleCollisionGameDataManagement::Update()
extern void BattleCollisionGameDataManagement_Update_m89F37A663EE4CF5C0DE4239C4C4194EB26A28246 (void);
// 0x00000293 System.Void BattleCollisionGameDataManagement::SetUp()
extern void BattleCollisionGameDataManagement_SetUp_mD6AAB63A3C7248F176C0300394F25E864E677B1A (void);
// 0x00000294 System.Collections.Generic.List`1<BattleCollisionGameData> BattleCollisionGameDataManagement::GetBattleCollisionGameData(System.Int32,System.Int32)
extern void BattleCollisionGameDataManagement_GetBattleCollisionGameData_m7CFF3330B4C30135D594D0C979B3351EA07ACB44 (void);
// 0x00000295 System.Void BattleCollisionGameDataManagement::ReleaseAll()
extern void BattleCollisionGameDataManagement_ReleaseAll_mE3D54489D716A8B47123340682583E441410690E (void);
// 0x00000296 System.Void BattleCollisionGameDataManagement::.ctor()
extern void BattleCollisionGameDataManagement__ctor_m655D2E68B3DF1A4B4F5D5ADD861832509CF109C9 (void);
// 0x00000297 System.Void BaseBattleCollisionGameSubstance::Start()
extern void BaseBattleCollisionGameSubstance_Start_m4FF6E3BE7E5EC29CA72D4017EB2B489E97628415 (void);
// 0x00000298 System.Void BaseBattleCollisionGameSubstance::Init(System.Collections.Generic.List`1<BattleCollisionGameData>,SkillGameData,BattleUnit,UnityEngine.Quaternion)
extern void BaseBattleCollisionGameSubstance_Init_m113FBE3942CCC8598E5158462A50E2BE692A2F22 (void);
// 0x00000299 System.Void BaseBattleCollisionGameSubstance::Update()
extern void BaseBattleCollisionGameSubstance_Update_m4154D9AEF8AF853D79EB4B0AA0ED6D29E341B4E0 (void);
// 0x0000029A System.Void BaseBattleCollisionGameSubstance::UpdateDetails()
extern void BaseBattleCollisionGameSubstance_UpdateDetails_m6F475D788246A88AF59FA87A50421DB5082438F7 (void);
// 0x0000029B System.Void BaseBattleCollisionGameSubstance::SecondEnter(UnityEngine.Collider,UnityEngine.Collider)
extern void BaseBattleCollisionGameSubstance_SecondEnter_m9465964419E70A95B7AAEAA9A0DB8575FD7BFDF4 (void);
// 0x0000029C System.Void BaseBattleCollisionGameSubstance::.ctor()
extern void BaseBattleCollisionGameSubstance__ctor_m62F91F3C1FA3E7A644AD2BBAF5285AC59898D44D (void);
// 0x0000029D System.Void BattleCollisionDetails::SetUp(UnityEngine.SphereCollider,BattleCollisionGameData)
extern void BattleCollisionDetails_SetUp_mCCD806A4C9AD21E95DAB2DF2CD1D19E99F8EA476 (void);
// 0x0000029E System.Boolean BattleCollisionDetails::Update()
extern void BattleCollisionDetails_Update_mC9773316B411CCACBAC653F6E1AD5F75E9DBA563 (void);
// 0x0000029F System.Void BattleCollisionDetails::.ctor()
extern void BattleCollisionDetails__ctor_m342F9393C9618B8FF457079C24506B7F697B4CD4 (void);
// 0x000002A0 UnityEngine.Collider BaseBulletCollisionDetails::GetCollider()
extern void BaseBulletCollisionDetails_GetCollider_m13E91ED7028E4A337454700498B0ACD560AB5168 (void);
// 0x000002A1 System.Void BaseBulletCollisionDetails::InitSetUp(UnityEngine.Collider,BulletGameData,BattleCollisionGameData)
extern void BaseBulletCollisionDetails_InitSetUp_m6EE25A796C2A051599294C35ECE36F76B3B1DF4F (void);
// 0x000002A2 System.Boolean BaseBulletCollisionDetails::Update()
extern void BaseBulletCollisionDetails_Update_m309912D4C14C8E31445D563C4D661967E953D35A (void);
// 0x000002A3 System.Void BaseBulletCollisionDetails::.ctor()
extern void BaseBulletCollisionDetails__ctor_m2A2CD3EA383CA7E243A11A53C34A54994058B220 (void);
// 0x000002A4 System.Boolean RpgCollisionConnection::AddCollisionConstruction(BaseCollisionConstruction)
extern void RpgCollisionConnection_AddCollisionConstruction_m6193C42C3535C4300C8E30A58180D39BD1B23DF5 (void);
// 0x000002A5 System.Boolean RpgCollisionConnection::ErasureCollisionConstruction(BaseCollisionConstruction)
extern void RpgCollisionConnection_ErasureCollisionConstruction_m1A63504DC10F39897F08627829DB2F74E1213F48 (void);
// 0x000002A6 BaseCollisionConstruction RpgCollisionConnection::SearchCollisionConstruction(BaseCollisionConstruction)
extern void RpgCollisionConnection_SearchCollisionConstruction_m0E4CCA53638B39B4D736319C7F1E084521D400D7 (void);
// 0x000002A7 System.Boolean RpgCollisionConnection::RemoveCollisionConstruction(BaseCollisionConstruction)
extern void RpgCollisionConnection_RemoveCollisionConstruction_mE45BD9897E0D726DE90D371569492CF88AA26625 (void);
// 0x000002A8 RpgCollisionControl RpgCollisionControl::get_Instance()
extern void RpgCollisionControl_get_Instance_m3559C7C538AFB0C06887F4FDC9015DD53CEA59FB (void);
// 0x000002A9 System.Collections.Generic.List`1<BaseCollisionConstruction> RpgCollisionControl::get_ListAllCollisionConstruction()
extern void RpgCollisionControl_get_ListAllCollisionConstruction_m7D3FCFD1736BD549CE8E6BB59F761767C17D95B9 (void);
// 0x000002AA System.Boolean RpgCollisionControl::AddCollisionConstruction(BaseCollisionConstruction)
extern void RpgCollisionControl_AddCollisionConstruction_mB6F36BA68D0CEF9ED818DD8E76630A943FBACAEB (void);
// 0x000002AB System.Boolean RpgCollisionControl::ErasureCollisionConstruction(BaseCollisionConstruction)
extern void RpgCollisionControl_ErasureCollisionConstruction_mE9F607F5E974D1B473D1BE4E5F9D9555AAC6FACD (void);
// 0x000002AC BaseCollisionConstruction RpgCollisionControl::SearchCollisionConstruction(BaseCollisionConstruction)
extern void RpgCollisionControl_SearchCollisionConstruction_mD37DC741306A120BC964330766235395C7C7651B (void);
// 0x000002AD System.Boolean RpgCollisionControl::RemoveCollisionConstruction(BaseCollisionConstruction)
extern void RpgCollisionControl_RemoveCollisionConstruction_mA91376ADB4A934A8BE95B7C6EDCBCE35DFAD492B (void);
// 0x000002AE System.Void RpgCollisionControl::AutoRemoveCollisionConstruction()
extern void RpgCollisionControl_AutoRemoveCollisionConstruction_m46B2C1BA95F64BFFDFC481417A5082ADF1F2F823 (void);
// 0x000002AF System.Void RpgCollisionControl::CollisionUpdate()
extern void RpgCollisionControl_CollisionUpdate_mFC444534E8232A80BFEE83FD236E75BFFDB1BBD1 (void);
// 0x000002B0 System.Void RpgCollisionControl::CollisionSort()
extern void RpgCollisionControl_CollisionSort_mB5CB77C40DDCA80CD8BA075C87C11D369FA4D1D3 (void);
// 0x000002B1 System.Void RpgCollisionControl::Update()
extern void RpgCollisionControl_Update_m3AD09B079C14B37D91938082B9DB4899959B493B (void);
// 0x000002B2 System.Void RpgCollisionControl::.ctor()
extern void RpgCollisionControl__ctor_m42383BDAC1A93D7B1DBFFFAF93CF2239A88C313E (void);
// 0x000002B3 System.Void RpgCollisionControl_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mC9AFB3568D69CA9524783CC3B37658DD81513182 (void);
// 0x000002B4 System.Boolean RpgCollisionControl_<>c__DisplayClass8_0::<SearchCollisionConstruction>b__0(BaseCollisionConstruction)
extern void U3CU3Ec__DisplayClass8_0_U3CSearchCollisionConstructionU3Eb__0_m44740300811AA8930BBF6C7D1BE5DD3CB139CBF8 (void);
// 0x000002B5 System.Void RpgCollisionControl_<>c::.cctor()
extern void U3CU3Ec__cctor_m5FDBC7E92CE89B0D57886F04114CB3C28AF5EC36 (void);
// 0x000002B6 System.Void RpgCollisionControl_<>c::.ctor()
extern void U3CU3Ec__ctor_m5393EA137E7C5A3C4884701DA465AC85F868682D (void);
// 0x000002B7 System.Boolean RpgCollisionControl_<>c::<AutoRemoveCollisionConstruction>b__10_0(BaseCollisionConstruction)
extern void U3CU3Ec_U3CAutoRemoveCollisionConstructionU3Eb__10_0_mD2B62D3845780D00549BC162D5202C9D0ABA6370 (void);
// 0x000002B8 System.Int32 RpgCollisionControl_<>c::<CollisionSort>b__12_0(BaseCollisionConstruction,BaseCollisionConstruction)
extern void U3CU3Ec_U3CCollisionSortU3Eb__12_0_mB57CA5C4755195880C1E708FBF69D4C9A83AB4A0 (void);
// 0x000002B9 UnityEngine.Collider[] RpgCollisionCalculation::CapsuleCalculation(UnityEngine.CapsuleCollider)
extern void RpgCollisionCalculation_CapsuleCalculation_m29FB88798C5F006107D99C6608E57836881D5DB1 (void);
// 0x000002BA UnityEngine.Collider[] RpgCollisionCalculation::OverlapCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void RpgCollisionCalculation_OverlapCapsule_m7233DB39980BCC584619FBDE982118B06BC50D99 (void);
// 0x000002BB UnityEngine.Collider[] RpgCollisionCalculation::SphereCalculation(UnityEngine.SphereCollider)
extern void RpgCollisionCalculation_SphereCalculation_m3BFE95B85DBAA978F259F066F201A681565B42C3 (void);
// 0x000002BC UnityEngine.Collider[] RpgCollisionCalculation::OverlapSphere(UnityEngine.Vector3,System.Single)
extern void RpgCollisionCalculation_OverlapSphere_m8B74412BA7993734EB3BF5F191130A296921B086 (void);
// 0x000002BD UnityEngine.Collider[] RpgCollisionCalculation::BoxCalculation(UnityEngine.BoxCollider)
extern void RpgCollisionCalculation_BoxCalculation_m8CEEBEAD10A855974265720A8A613D85602F3CED (void);
// 0x000002BE UnityEngine.Collider[] RpgCollisionCalculation::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void RpgCollisionCalculation_OverlapBox_m6DD8A427A0A231D695368D6BF99E4BE692744FA1 (void);
// 0x000002BF System.Boolean RpgCollisionCalculation::ComputePenetration(CollisionPushType,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single&)
extern void RpgCollisionCalculation_ComputePenetration_m14D92C1A9D6D866290C021402E94CBC377DAD352 (void);
// 0x000002C0 System.Boolean RpgCollisionCalculation::ComputePenetrationThisPush(UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single&)
extern void RpgCollisionCalculation_ComputePenetrationThisPush_m740E6F9429FE9B2E4BE345F0C1B00229A351A3BC (void);
// 0x000002C1 System.Boolean RpgCollisionCalculation::ComputePenetrationOpponentPush(UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single&)
extern void RpgCollisionCalculation_ComputePenetrationOpponentPush_m3D065B0C1218A3EEC8F52FAD373EB9FACE22496D (void);
// 0x000002C2 UnityEngine.Vector3 RpgCollisionCalculation::BattlAreaCollisionCorrection(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void RpgCollisionCalculation_BattlAreaCollisionCorrection_mEBFD977655A0BCCA1A2F7E9C2FB083AB5E721CD7 (void);
// 0x000002C3 System.Void RpgCollisionCalculation::CollisionCorrection(BaseCollisionConstruction,RpgCollisionDetailsControl,RpgCollisionDetails,System.Collections.Generic.List`1<BaseCollisionConstruction>)
extern void RpgCollisionCalculation_CollisionCorrection_m795C7781FA41EF45AD27570D0FE2E4D8CECD22AC (void);
// 0x000002C4 System.Boolean RpgCollisionCalculation::CollisionLayerIsHit(CollisionLayer,CollisionLayer)
extern void RpgCollisionCalculation_CollisionLayerIsHit_mBECFF1E4191FC26D1CB6F885E1A34D1C942983DD (void);
// 0x000002C5 System.Void RpgCollisionCalculation::Exit(System.Int32)
extern void RpgCollisionCalculation_Exit_mF7CBD2EA6F97FBBAA46C61F96FB28497173ECC43 (void);
// 0x000002C6 System.Void RpgCollisionCalculation::.cctor()
extern void RpgCollisionCalculation__cctor_m2D165E06F57FE3B724E7317E16A797FE60BFF39E (void);
// 0x000002C7 System.Void RpgCollisionCalculation_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mC8FD6F42302735004E30CBA33AC9C0B225D50D92 (void);
// 0x000002C8 System.Boolean RpgCollisionCalculation_<>c__DisplayClass2_0::<CapsuleCalculation>b__0(UnityEngine.Collider)
extern void U3CU3Ec__DisplayClass2_0_U3CCapsuleCalculationU3Eb__0_mDD4FC68B122D2CE4C294CD7E496D6B4272553297 (void);
// 0x000002C9 System.Void RpgCollisionCalculation_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m173B0EA8B1BB8860B66CAA3AF98F72F2C01B23C0 (void);
// 0x000002CA System.Boolean RpgCollisionCalculation_<>c__DisplayClass4_0::<SphereCalculation>b__0(UnityEngine.Collider)
extern void U3CU3Ec__DisplayClass4_0_U3CSphereCalculationU3Eb__0_m1983EEA351276C7531ED964E0EABD9A29500111A (void);
// 0x000002CB System.Void RpgCollisionCalculation_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m69D9778B13B9E154521F3931C0DF8B0A81404B47 (void);
// 0x000002CC System.Boolean RpgCollisionCalculation_<>c__DisplayClass6_0::<BoxCalculation>b__0(UnityEngine.Collider)
extern void U3CU3Ec__DisplayClass6_0_U3CBoxCalculationU3Eb__0_m486AEA48F18561BFE36A7BB7C9BF11584A628CB0 (void);
// 0x000002CD RpgCollisionDetailsControl BaseCollisionConstruction::get_RpgCollisionDetailsControl()
extern void BaseCollisionConstruction_get_RpgCollisionDetailsControl_m37795727C398D04AAE727CDD3783216287F0F88F (void);
// 0x000002CE UnityEngine.GameObject BaseCollisionConstruction::get_ThisObject()
extern void BaseCollisionConstruction_get_ThisObject_mD6B662D80688B2D3D8C81B55914D1798F680FFC2 (void);
// 0x000002CF CollisionLayer BaseCollisionConstruction::get_CollisionLayer()
extern void BaseCollisionConstruction_get_CollisionLayer_mC0668605BB247715026889F8BDBC382B4B4FA1B3 (void);
// 0x000002D0 CollisionPushType BaseCollisionConstruction::get_CollisionPushType()
extern void BaseCollisionConstruction_get_CollisionPushType_m4219C2B39F7276841ADE1C8A6FB463D4105ED177 (void);
// 0x000002D1 System.Void BaseCollisionConstruction::.ctor()
extern void BaseCollisionConstruction__ctor_m3400A3C5024F33C1275E699830EBDD51094CB596 (void);
// 0x000002D2 System.Void BaseCollisionConstruction::Init(UnityEngine.GameObject,CollisionLayer,CollisionPushType,UnityEngine.GameObject)
extern void BaseCollisionConstruction_Init_mD9DE0608B9A288FE9D75A78656AAF23381247C74 (void);
// 0x000002D3 System.Void BaseCollisionConstruction::Init(UnityEngine.GameObject,CollisionLayer,CollisionPushType,System.Int32,UnityEngine.GameObject)
extern void BaseCollisionConstruction_Init_mF1E2837A225032986ABAA3522E72CBD76D263DC7 (void);
// 0x000002D4 System.Void BaseCollisionConstruction::Update()
extern void BaseCollisionConstruction_Update_mFB0832E5CB8E4A2161965577FD690B880C39D6B0 (void);
// 0x000002D5 System.Void BaseCollisionConstruction::UpdateCollision(System.Collections.Generic.List`1<BaseCollisionConstruction>)
extern void BaseCollisionConstruction_UpdateCollision_m72501A91DD1D3800B9FD4307D40EA2CABC3ABBFB (void);
// 0x000002D6 System.Void BaseCollisionConstruction::UpdateCollisionFlag()
extern void BaseCollisionConstruction_UpdateCollisionFlag_m0CE40374F5608783A0EE4377AD2CFA0B5A8C197A (void);
// 0x000002D7 System.Void RpgCollisionDetailsControl::set_HitEnterFunc(RpgCollisionDetailsControl_HitEnter)
extern void RpgCollisionDetailsControl_set_HitEnterFunc_mFA74716E8E0A3F1D5E349915D493E348F93D8C88 (void);
// 0x000002D8 System.Void RpgCollisionDetailsControl::set_HitSecondFunc(RpgCollisionDetailsControl_HitSecond)
extern void RpgCollisionDetailsControl_set_HitSecondFunc_m1E1166A2B41953F097DACCA4C649F6EDA9085530 (void);
// 0x000002D9 System.Void RpgCollisionDetailsControl::set_HitReleaseFunc(RpgCollisionDetailsControl_HitRelease)
extern void RpgCollisionDetailsControl_set_HitReleaseFunc_m3CB99570CA1BC5EA6AD6A0E9C51D811198FA3815 (void);
// 0x000002DA System.Collections.Generic.List`1<RpgCollisionDetails> RpgCollisionDetailsControl::get_ListRpgCollisionDetaile()
extern void RpgCollisionDetailsControl_get_ListRpgCollisionDetaile_m1BD19C367E0CFCD5F8EB46BFC22F86BF8A8DD5B2 (void);
// 0x000002DB UnityEngine.GameObject RpgCollisionDetailsControl::get_ThisObject()
extern void RpgCollisionDetailsControl_get_ThisObject_m5A98DF693E0E7A5C3125CFC34D0B7F427FB74E58 (void);
// 0x000002DC System.Void RpgCollisionDetailsControl::Init(UnityEngine.GameObject)
extern void RpgCollisionDetailsControl_Init_m168F605D7CABD1FB1372B64C62A336E6A523D26E (void);
// 0x000002DD System.Void RpgCollisionDetailsControl::Update(BaseCollisionConstruction)
extern void RpgCollisionDetailsControl_Update_m93B2037225396D0B444650BA170DF8BB975F3732 (void);
// 0x000002DE System.Void RpgCollisionDetailsControl::UpdateCollision(BaseCollisionConstruction,System.Collections.Generic.List`1<BaseCollisionConstruction>)
extern void RpgCollisionDetailsControl_UpdateCollision_mA95740080F7AA0BCE5F42BBDAC77F5F40446A6BB (void);
// 0x000002DF System.Void RpgCollisionDetailsControl::UpdateCollisionFlag()
extern void RpgCollisionDetailsControl_UpdateCollisionFlag_m26487328954AD2F4810078BA921472A131D079BD (void);
// 0x000002E0 System.Boolean RpgCollisionDetailsControl::AddCollisionDetails(RpgCollisionDetails)
extern void RpgCollisionDetailsControl_AddCollisionDetails_m133F77F8BF342FC4EBC91B7F66D4686C840A145E (void);
// 0x000002E1 RpgCollisionHit RpgCollisionDetailsControl::AddCollisionHit(UnityEngine.Collider,UnityEngine.Collider,RpgCollisionDetails,System.Boolean)
extern void RpgCollisionDetailsControl_AddCollisionHit_m7419273F6612BF6D0FC3B56515627685C2E5CCA0 (void);
// 0x000002E2 System.Boolean RpgCollisionDetailsControl::CollisionHitEnter(UnityEngine.Collider,UnityEngine.Collider,RpgCollisionDetails)
extern void RpgCollisionDetailsControl_CollisionHitEnter_m327898A49F8C8AC7875EB831004CD812D593818E (void);
// 0x000002E3 System.Boolean RpgCollisionDetailsControl::AutoAddCollisionDetails(BaseCollisionConstruction)
extern void RpgCollisionDetailsControl_AutoAddCollisionDetails_m4FE58672D8A5142442DE745777EAB19DC6E08392 (void);
// 0x000002E4 System.Boolean RpgCollisionDetailsControl::AutoAddCollisionDetails(BaseCollisionConstruction,System.Int32)
extern void RpgCollisionDetailsControl_AutoAddCollisionDetails_m7848A9CC2D18A0350A413F1367013D7CB11A18AE (void);
// 0x000002E5 System.Boolean RpgCollisionDetailsControl::ErasureCollisionDetails(RpgCollisionDetails)
extern void RpgCollisionDetailsControl_ErasureCollisionDetails_m8F71FD10B65E608BA84D80B013F020B619ABEF55 (void);
// 0x000002E6 RpgCollisionDetails RpgCollisionDetailsControl::SearchCollisionDetails(RpgCollisionDetails)
extern void RpgCollisionDetailsControl_SearchCollisionDetails_m2CC0B2E676E13B03B2469435F3C4EA19C8C01252 (void);
// 0x000002E7 RpgCollisionDetails RpgCollisionDetailsControl::SearchCollisionDetails(UnityEngine.Collider)
extern void RpgCollisionDetailsControl_SearchCollisionDetails_m339643242DD0F08C347249D340ADC62D69BE4CE0 (void);
// 0x000002E8 RpgCollisionHit RpgCollisionDetailsControl::SearchCollisionHit(RpgCollisionDetails)
extern void RpgCollisionDetailsControl_SearchCollisionHit_m1093427B424A34AB749B51D19441E5E80630024E (void);
// 0x000002E9 RpgCollisionHit RpgCollisionDetailsControl::SearchCollisionHit(UnityEngine.Collider,RpgCollisionDetails)
extern void RpgCollisionDetailsControl_SearchCollisionHit_m6790B624B162D551A3E5414540978FA1774B86F3 (void);
// 0x000002EA System.Boolean RpgCollisionDetailsControl::RemoveCollisionDetails(RpgCollisionDetails)
extern void RpgCollisionDetailsControl_RemoveCollisionDetails_mDA051A4C77918CB79C5133900A1A39E7D2B0AB9E (void);
// 0x000002EB System.Void RpgCollisionDetailsControl::RemoveAllCollisionHit()
extern void RpgCollisionDetailsControl_RemoveAllCollisionHit_m6DEC609C815A2D8D5DD07E29525B07E312B73BB7 (void);
// 0x000002EC System.Void RpgCollisionDetailsControl::HitUpdate(RpgCollisionHit)
extern void RpgCollisionDetailsControl_HitUpdate_m06127C7433D609ADEFFFB90A0D085FB3F5BC99EC (void);
// 0x000002ED System.Void RpgCollisionDetailsControl::CollisionPositionCalculation(UnityEngine.Vector3)
extern void RpgCollisionDetailsControl_CollisionPositionCalculation_m77BA3554C48DBEAAA4BAFC5114E04954B9C09F40 (void);
// 0x000002EE System.Void RpgCollisionDetailsControl::.ctor()
extern void RpgCollisionDetailsControl__ctor_m8B7342AA458DC6AD8600554BC215318F455242F2 (void);
// 0x000002EF System.Void RpgCollisionDetailsControl_HitEnter::.ctor(System.Object,System.IntPtr)
extern void HitEnter__ctor_m6CF8AF024D17561D5DFDB1A12B6859114FB08A96 (void);
// 0x000002F0 System.Void RpgCollisionDetailsControl_HitEnter::Invoke(UnityEngine.Collider,UnityEngine.Collider)
extern void HitEnter_Invoke_mBBDF6CF32359E3754CAC22E9C7EE861CC21946CE (void);
// 0x000002F1 System.IAsyncResult RpgCollisionDetailsControl_HitEnter::BeginInvoke(UnityEngine.Collider,UnityEngine.Collider,System.AsyncCallback,System.Object)
extern void HitEnter_BeginInvoke_m4A09B2D1296AF484FCCC02CAAFF540B87DCBF270 (void);
// 0x000002F2 System.Void RpgCollisionDetailsControl_HitEnter::EndInvoke(System.IAsyncResult)
extern void HitEnter_EndInvoke_m69B4EFFA40AE60631114B283707743D6DC72B861 (void);
// 0x000002F3 System.Void RpgCollisionDetailsControl_HitSecond::.ctor(System.Object,System.IntPtr)
extern void HitSecond__ctor_mBE28E2149814B03E1017183C0E9FF0C98A7C5E66 (void);
// 0x000002F4 System.Void RpgCollisionDetailsControl_HitSecond::Invoke(UnityEngine.Collider,UnityEngine.Collider)
extern void HitSecond_Invoke_m313E7E2756BEFDCE17477ECA07E0CB70C57919F2 (void);
// 0x000002F5 System.IAsyncResult RpgCollisionDetailsControl_HitSecond::BeginInvoke(UnityEngine.Collider,UnityEngine.Collider,System.AsyncCallback,System.Object)
extern void HitSecond_BeginInvoke_m97DC9D8E2714368C6A33F846D23F58CA18114303 (void);
// 0x000002F6 System.Void RpgCollisionDetailsControl_HitSecond::EndInvoke(System.IAsyncResult)
extern void HitSecond_EndInvoke_m915CB021F808E081BA7E400A4252DC1D94E9E2F1 (void);
// 0x000002F7 System.Void RpgCollisionDetailsControl_HitRelease::.ctor(System.Object,System.IntPtr)
extern void HitRelease__ctor_m46DFDAA7445AE14B4D5BEFDFF8C65E60CC64BDC5 (void);
// 0x000002F8 System.Void RpgCollisionDetailsControl_HitRelease::Invoke(UnityEngine.Collider,UnityEngine.Collider)
extern void HitRelease_Invoke_m2B550F5E745258BC3EF45EC87E0367D7FE74BE7A (void);
// 0x000002F9 System.IAsyncResult RpgCollisionDetailsControl_HitRelease::BeginInvoke(UnityEngine.Collider,UnityEngine.Collider,System.AsyncCallback,System.Object)
extern void HitRelease_BeginInvoke_m0F68B1AF732BBCB48C26906490A1D27AFEBBD6AB (void);
// 0x000002FA System.Void RpgCollisionDetailsControl_HitRelease::EndInvoke(System.IAsyncResult)
extern void HitRelease_EndInvoke_mF2607D13D37A9C150DCC8FBF126DA2C99645C4AD (void);
// 0x000002FB System.Void RpgCollisionDetailsControl_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mFF0540F2290B8835BC1C9630ECE2ACDC5799E857 (void);
// 0x000002FC System.Boolean RpgCollisionDetailsControl_<>c__DisplayClass29_0::<SearchCollisionDetails>b__0(RpgCollisionDetails)
extern void U3CU3Ec__DisplayClass29_0_U3CSearchCollisionDetailsU3Eb__0_m8655A6E5BB065B1417B9C7C1987464168EE8EFA3 (void);
// 0x000002FD System.Void RpgCollisionDetailsControl_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mB5C361BED007B3C747C65C2EEF2A7DB7A17BC79A (void);
// 0x000002FE System.Boolean RpgCollisionDetailsControl_<>c__DisplayClass30_0::<SearchCollisionDetails>b__0(RpgCollisionDetails)
extern void U3CU3Ec__DisplayClass30_0_U3CSearchCollisionDetailsU3Eb__0_m31538AF37282AE512167555622B01141EAF0D09C (void);
// 0x000002FF System.Void RpgCollisionDetailsControl_<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m6B3A36B9AE26989CF3864A98F889F36D3A2DD4D9 (void);
// 0x00000300 System.Boolean RpgCollisionDetailsControl_<>c__DisplayClass31_0::<SearchCollisionHit>b__0(RpgCollisionHit)
extern void U3CU3Ec__DisplayClass31_0_U3CSearchCollisionHitU3Eb__0_m5F69AF6AFE28D471F03D53DD171C01BAB2CFFAEF (void);
// 0x00000301 System.Void RpgCollisionDetailsControl_<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mF9823D5FB927B57C647CD43795F0A651B8B205C2 (void);
// 0x00000302 System.Boolean RpgCollisionDetailsControl_<>c__DisplayClass32_0::<SearchCollisionHit>b__0(RpgCollisionHit)
extern void U3CU3Ec__DisplayClass32_0_U3CSearchCollisionHitU3Eb__0_mAE2C830D473B97F28001F548389DD48B89E64E5F (void);
// 0x00000303 System.Void RpgCollisionDetailsControl_<>c::.cctor()
extern void U3CU3Ec__cctor_mD2FE9F5B0D6B05F8F6A37A98525D793C495BEF39 (void);
// 0x00000304 System.Void RpgCollisionDetailsControl_<>c::.ctor()
extern void U3CU3Ec__ctor_mCD506C7C4F7C48FEB2F8B8BE5C678EFB130EE772 (void);
// 0x00000305 System.Boolean RpgCollisionDetailsControl_<>c::<RemoveAllCollisionHit>b__34_0(RpgCollisionHit)
extern void U3CU3Ec_U3CRemoveAllCollisionHitU3Eb__34_0_m85CD96A470436FDEF3299E51B54CB89826AF244E (void);
// 0x00000306 UnityEngine.Collider RpgCollisionDetails::get_RpgCollision()
extern void RpgCollisionDetails_get_RpgCollision_m6FBFFCA9A7FF3173EEC9C59E7E94F78B65521BA4 (void);
// 0x00000307 UnityEngine.GameObject RpgCollisionDetails::get_ThisObject()
extern void RpgCollisionDetails_get_ThisObject_m71DA4C87EB2F7FBEF2D2500991B6C2B5928D0FA0 (void);
// 0x00000308 RpgCollisionPosition RpgCollisionDetails::get_PositionCollision()
extern void RpgCollisionDetails_get_PositionCollision_mB48D18E388095D9771135C42AC237BB1D89705D5 (void);
// 0x00000309 System.Void RpgCollisionDetails::Init(BaseCollisionConstruction,UnityEngine.Collider,UnityEngine.GameObject)
extern void RpgCollisionDetails_Init_mFEDD4E66FDF3FBD45DF4447B4D6B7223D6CF8AA1 (void);
// 0x0000030A System.Void RpgCollisionDetails::Update(BaseCollisionConstruction)
extern void RpgCollisionDetails_Update_m4D44D5473AF3920155C11B2147DA545AC9E60BB0 (void);
// 0x0000030B System.Void RpgCollisionDetails::UpdatePosition(UnityEngine.Vector3)
extern void RpgCollisionDetails_UpdatePosition_m4578956F061126640A5253D6371BCB0FB0787841 (void);
// 0x0000030C System.Void RpgCollisionDetails::UpdateCollision(BaseCollisionConstruction,RpgCollisionDetailsControl,System.Collections.Generic.List`1<BaseCollisionConstruction>)
extern void RpgCollisionDetails_UpdateCollision_m3E0ACB761E6E3E916272432E7D0C0EF426F2B5B6 (void);
// 0x0000030D UnityEngine.Collider[] RpgCollisionDetails::CollisionTypeOverLap()
extern void RpgCollisionDetails_CollisionTypeOverLap_m791292F919A8875CE8FADBC4F65F248A7BCD2F68 (void);
// 0x0000030E System.Void RpgCollisionDetails::.ctor()
extern void RpgCollisionDetails__ctor_m01E5B93CB82219CB0199FAE8F9358107DC1B80C3 (void);
// 0x0000030F RpgCollisionPosition RpgCollisionDetailsAccessor::CollisionPosition(RpgCollisionDetails)
extern void RpgCollisionDetailsAccessor_CollisionPosition_m0184574C6A4CD611D83676F5516BAEECA692603E (void);
// 0x00000310 UnityEngine.Vector3 RpgCollisionDetailsAccessor::SaveCollisionPosition(RpgCollisionDetails)
extern void RpgCollisionDetailsAccessor_SaveCollisionPosition_m625FA5A9F86E66AAABC85F7F17A0B8E07AE511E4 (void);
// 0x00000311 UnityEngine.Vector3 RpgCollisionDetailsAccessor::SaveCollisionPosition(RpgCollisionDetails,UnityEngine.Vector3)
extern void RpgCollisionDetailsAccessor_SaveCollisionPosition_m3B75DEABE44B458FBC0111C462F0B07696C2BC27 (void);
// 0x00000312 UnityEngine.Vector3 RpgCollisionDetailsAccessor::SaveCollisionPositionCaculation(RpgCollisionDetails,UnityEngine.Vector3)
extern void RpgCollisionDetailsAccessor_SaveCollisionPositionCaculation_m7B94C2411DA4F84653797D7408947DF26603E234 (void);
// 0x00000313 UnityEngine.Vector3 RpgCollisionDetailsAccessor::OldToNowVector(RpgCollisionDetails)
extern void RpgCollisionDetailsAccessor_OldToNowVector_mA88E6B5355A7B0F57F04D6E0AD8403A1C1882870 (void);
// 0x00000314 UnityEngine.Vector3 RpgCollisionDetailsAccessor::CalculationPosition(RpgCollisionDetails)
extern void RpgCollisionDetailsAccessor_CalculationPosition_m9D80FA81707EA00C861D0FE450F173C14CDC1DFC (void);
// 0x00000315 UnityEngine.Vector3 RpgCollisionDetailsAccessor::CalculationPosition(RpgCollisionDetails,UnityEngine.Vector3)
extern void RpgCollisionDetailsAccessor_CalculationPosition_mCCF004CEBF6E2C0FC38CE21FCE0243E564CA4AE7 (void);
// 0x00000316 UnityEngine.Vector3 RpgCollisionDetailsAccessor::CalculationPositionCaculation(RpgCollisionDetails,UnityEngine.Vector3)
extern void RpgCollisionDetailsAccessor_CalculationPositionCaculation_m198EA6E83FFB7D2336E4D9CD009C70DED50B9B59 (void);
// 0x00000317 UnityEngine.Vector3 RpgCollisionDetailsAccessor::SaveOldToNowVector(RpgCollisionDetails)
extern void RpgCollisionDetailsAccessor_SaveOldToNowVector_m44BDC5E44FA05FF5C4B9CAB5422172B0B4B188B8 (void);
// 0x00000318 UnityEngine.Collider RpgCollisionHit::get_HitCollision()
extern void RpgCollisionHit_get_HitCollision_m8FE2FA6188CEBFD6BF6E0CA54D24FA40962D86F1 (void);
// 0x00000319 UnityEngine.Collider RpgCollisionHit::get_ThisHitCollision()
extern void RpgCollisionHit_get_ThisHitCollision_m9235BF22A9A72E1F5E7EC50424DF9DB1553524AB (void);
// 0x0000031A System.Boolean RpgCollisionHit::get_GetHitEnter()
extern void RpgCollisionHit_get_GetHitEnter_m760A5A334A7BAB643BE734DD51487EA12955F60A (void);
// 0x0000031B System.Boolean RpgCollisionHit::get_GetHitSecond()
extern void RpgCollisionHit_get_GetHitSecond_m3EA7D0FC408F36B37898F13FF1D636CF46EB0EF9 (void);
// 0x0000031C System.Boolean RpgCollisionHit::get_GetHitRelease()
extern void RpgCollisionHit_get_GetHitRelease_m75E101423FF45196EB0F925E6EEDCCF1E0E47E34 (void);
// 0x0000031D System.Boolean RpgCollisionHit::get_GetCollisionHit()
extern void RpgCollisionHit_get_GetCollisionHit_mC2A74DCD2D2DB78DB70D8CD808E1FAD546E52FE3 (void);
// 0x0000031E UnityEngine.GameObject RpgCollisionHit::get_ThisObject()
extern void RpgCollisionHit_get_ThisObject_m0DECFD6319976A36D25222EE36BF474D4ACC45E6 (void);
// 0x0000031F System.Void RpgCollisionHit::Init(UnityEngine.Collider,UnityEngine.Collider,UnityEngine.GameObject)
extern void RpgCollisionHit_Init_m4938D36B00405388E12250723713F962A7A9BFBE (void);
// 0x00000320 System.Void RpgCollisionHit::FlagUpdate()
extern void RpgCollisionHit_FlagUpdate_mE071AEA360A91F9D44A3796C1A862058D51D06E5 (void);
// 0x00000321 System.Void RpgCollisionHit::FlagHit()
extern void RpgCollisionHit_FlagHit_mA2AB74B373B7E20529EB174A56F76267386C20C8 (void);
// 0x00000322 System.Boolean RpgCollisionHit::CheckFlag()
extern void RpgCollisionHit_CheckFlag_m13B5A452CD8FC4E339EE79CC19B60FDFCA615E0D (void);
// 0x00000323 System.Void RpgCollisionHit::.ctor()
extern void RpgCollisionHit__ctor_mBE4B37123E83B516365BA5CC0C3B7CB1DA76F609 (void);
// 0x00000324 System.Boolean RpgCollisionHitFlag::get_HitEnter()
extern void RpgCollisionHitFlag_get_HitEnter_m257CBE79504F92C1ECF93ABE48CDAD3CD26F2B4F (void);
// 0x00000325 System.Boolean RpgCollisionHitFlag::get_HitSecond()
extern void RpgCollisionHitFlag_get_HitSecond_m8E8E712162D6DDBB327AD00619E2183A51CFFB43 (void);
// 0x00000326 System.Boolean RpgCollisionHitFlag::get_HitRelease()
extern void RpgCollisionHitFlag_get_HitRelease_mFE1C3707570CAC54E52B2D6AAC2B12ECECB2930E (void);
// 0x00000327 System.Boolean RpgCollisionHitFlag::FlagUpdate(System.Boolean)
extern void RpgCollisionHitFlag_FlagUpdate_m5F11E0CF973615E77FDCFD2B77C4CF3C250DE899 (void);
// 0x00000328 System.Boolean RpgCollisionHitFlag::CheckHit()
extern void RpgCollisionHitFlag_CheckHit_mCB097C9DB966A151007A6822A2FE724C56A8989F (void);
// 0x00000329 System.Void RpgCollisionHitFlag::.ctor()
extern void RpgCollisionHitFlag__ctor_mBF024F0167C952FE475366474B2EB1DDFC9665BE (void);
// 0x0000032A System.Void RpgCollisionPosition::set_SaveCollisionPosition(UnityEngine.Vector3)
extern void RpgCollisionPosition_set_SaveCollisionPosition_mA0E235F8F99A76C584FE52394CDB51C7018EB378 (void);
// 0x0000032B UnityEngine.Vector3 RpgCollisionPosition::get_SaveCollisionPosition()
extern void RpgCollisionPosition_get_SaveCollisionPosition_m731C117A5BA1AD97223D1B7BE3FA974A18AC1187 (void);
// 0x0000032C UnityEngine.Vector3 RpgCollisionPosition::get_OldToNowVector()
extern void RpgCollisionPosition_get_OldToNowVector_mBBF1EA3CF55FB2C45266D1D65F86840314F43AF3 (void);
// 0x0000032D UnityEngine.Vector3 RpgCollisionPosition::get_SaveOldToNowVector()
extern void RpgCollisionPosition_get_SaveOldToNowVector_m447D3F6B750A2E63FD038DE9065CE27CEF2D922F (void);
// 0x0000032E System.Void RpgCollisionPosition::set_CalculationPosition(UnityEngine.Vector3)
extern void RpgCollisionPosition_set_CalculationPosition_mA92A936F995FE0E7DF390BA03B315437CE1A2DD7 (void);
// 0x0000032F UnityEngine.Vector3 RpgCollisionPosition::get_CalculationPosition()
extern void RpgCollisionPosition_get_CalculationPosition_m4587B2D4A73FA5A097B4B92D65377B5D6AAF0DDF (void);
// 0x00000330 System.Void RpgCollisionPosition::Init(UnityEngine.Vector3)
extern void RpgCollisionPosition_Init_m921424C62FFB3E9C59050EE86FE3215E2B9E21D1 (void);
// 0x00000331 System.Void RpgCollisionPosition::Update(UnityEngine.Vector3)
extern void RpgCollisionPosition_Update_m0C406F526AB227A937B92A360C149B269D82B983 (void);
// 0x00000332 System.Void RpgCollisionPosition::.ctor()
extern void RpgCollisionPosition__ctor_mAEBB4C39FFDB26FE519F1198420FDDD55896D0E5 (void);
// 0x00000333 System.Void DebugCollision::OnDraw()
extern void DebugCollision_OnDraw_m4B5FFE37634FB91836F353F28293675E688DDE0A (void);
// 0x00000334 System.Void DebugCollision::OffDraw()
extern void DebugCollision_OffDraw_m9F1250D180410CBCFA6A8E67FC21118E81F7D39E (void);
// 0x00000335 System.Boolean DebugCollision::GetRelease()
extern void DebugCollision_GetRelease_m3FC32556C6EB5DDB398EA7945FE058C7CDC4AA63 (void);
// 0x00000336 UnityEngine.GameObject DebugCollision::GetGameObject()
extern void DebugCollision_GetGameObject_m68E51B6F0A6E839FE93E416BF2535FE497FB2C0E (void);
// 0x00000337 System.Int32 DebugCollision::GetInstanceID()
extern void DebugCollision_GetInstanceID_mDE51A1F0D4884470CD59B8130BFEEABF39FD238D (void);
// 0x00000338 System.Void DebugCollision::Init(BaseCollisionConstruction,RpgCollisionDetails,UnityEngine.GameObject,System.String)
extern void DebugCollision_Init_m002933FBB3EA3D202C594DEFA46F3468EF4BA317 (void);
// 0x00000339 System.Void DebugCollision::Update()
extern void DebugCollision_Update_mDAEB91C6D6E5E0EA9384F1E13F8C742FB5791781 (void);
// 0x0000033A System.Void DebugCollision::UpdateColor()
extern void DebugCollision_UpdateColor_m741ADA16D9A6C7058B101C42861C72475780F130 (void);
// 0x0000033B System.Void DebugCollision::UpdateTransform()
extern void DebugCollision_UpdateTransform_m2C2181618CB66A979B6D9EF9FA6F22E314DBA6C2 (void);
// 0x0000033C System.Void DebugCollision::DrawMesh()
extern void DebugCollision_DrawMesh_m92FF54B7284221AA0B77FB09E18CF2B118DB7FC3 (void);
// 0x0000033D System.Void DebugCollision::DrawDisable()
extern void DebugCollision_DrawDisable_mFC485AE3B36FAC1EC68D9EE464A53B4DC990D6B6 (void);
// 0x0000033E System.Boolean DebugCollision::Release()
extern void DebugCollision_Release_mAC17D22433C8B378A8440775C77DF9276040771D (void);
// 0x0000033F System.Void DebugCollision::.ctor()
extern void DebugCollision__ctor_m0FEFE1592041B3D100A3FB2A5B491D13D9836C91 (void);
// 0x00000340 System.Void DebugCollisionControl::OnDebugDraw()
extern void DebugCollisionControl_OnDebugDraw_mC5C379273DA76BC52A73849824B945127F3FED14 (void);
// 0x00000341 System.Void DebugCollisionControl::OffDebugDraw()
extern void DebugCollisionControl_OffDebugDraw_m86AB2AEC9CCE19DC781A6455FF0A18F3503029EB (void);
// 0x00000342 System.Boolean DebugCollisionControl::GetFlagDraw()
extern void DebugCollisionControl_GetFlagDraw_mA4B1EAD16CCF15BE31850B06926E7B5DC73D0A7E (void);
// 0x00000343 System.Void DebugCollisionControl::Init()
extern void DebugCollisionControl_Init_m6A7A0777C959F611EFED355CF280E17DA37C5A73 (void);
// 0x00000344 System.Void DebugCollisionControl::Update()
extern void DebugCollisionControl_Update_mB11A9314E43E9EC11C6D7A79400F1718B2672C74 (void);
// 0x00000345 System.Void DebugCollisionControl::ManualUpdate()
extern void DebugCollisionControl_ManualUpdate_m889FEA8E87BC427092FBCD9FD2DBD2E20B4838D6 (void);
// 0x00000346 System.Void DebugCollisionControl::AutoAddCollision()
extern void DebugCollisionControl_AutoAddCollision_m60723983837257B24F3CDD58E042E61D710DC4A2 (void);
// 0x00000347 System.Void DebugCollisionControl::ReleaseUpdate()
extern void DebugCollisionControl_ReleaseUpdate_mF0677C507404F71883776C28854B29A2F5B618F1 (void);
// 0x00000348 System.Boolean DebugCollisionControl::SearchFlag(UnityEngine.GameObject)
extern void DebugCollisionControl_SearchFlag_m1CC3E5BBB25EF26A1B7E0E92DE114DABB4D04F52 (void);
// 0x00000349 DebugCollision DebugCollisionControl::SearchCollision(UnityEngine.GameObject)
extern void DebugCollisionControl_SearchCollision_m43CB033B7A0A355E722F48C36CB5375CFCF983BC (void);
// 0x0000034A System.Void DebugCollisionControl::.ctor()
extern void DebugCollisionControl__ctor_m4492F5CDF20722F084745398DE1D17DEEA9EE538 (void);
// 0x0000034B System.Void DebugCollisionControl_<>c::.cctor()
extern void U3CU3Ec__cctor_m4F410A4F54CCFBAA0ED3CE9AA4258E91EB399DB6 (void);
// 0x0000034C System.Void DebugCollisionControl_<>c::.ctor()
extern void U3CU3Ec__ctor_mD45947D00DDE60612DC1010FEA564FE00F4D7900 (void);
// 0x0000034D System.Boolean DebugCollisionControl_<>c::<ReleaseUpdate>b__9_0(DebugCollision)
extern void U3CU3Ec_U3CReleaseUpdateU3Eb__9_0_mC0B48B0BCEE6DB5CAF4302B28EFF65FF40F968FF (void);
// 0x0000034E System.Void DebugCollisionControl_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m5C3F242BFE61F6A7DED4AA900B437050FAE58874 (void);
// 0x0000034F System.Boolean DebugCollisionControl_<>c__DisplayClass11_0::<SearchCollision>b__0(DebugCollision)
extern void U3CU3Ec__DisplayClass11_0_U3CSearchCollisionU3Eb__0_m27CAA0C09AEE75A77E756DBDEB672F44FFF6CEA2 (void);
// 0x00000350 System.Int32 CharacterStatusDataExcel::GetIndex()
extern void CharacterStatusDataExcel_GetIndex_mB8C41A1F5D2E6A524B07D7DB43E1901D9DFDCF9F (void);
// 0x00000351 System.Int32 CharacterStatusDataExcel::GetCharacterDetailsID()
extern void CharacterStatusDataExcel_GetCharacterDetailsID_mF826E048000F39E236C391F2D81D8D243D954907 (void);
// 0x00000352 System.String CharacterStatusDataExcel::GetCharacterName()
extern void CharacterStatusDataExcel_GetCharacterName_m7E76D3A4C7BE6FE28C152987781D6F14806D9237 (void);
// 0x00000353 System.Int32 CharacterStatusDataExcel::GetCharacterHP()
extern void CharacterStatusDataExcel_GetCharacterHP_mEF7EE4B5FF03805D0F722A68DBCE0FC601D4AF4A (void);
// 0x00000354 System.Int32 CharacterStatusDataExcel::GetCharacterMP()
extern void CharacterStatusDataExcel_GetCharacterMP_m536C26DFF0A423CE88FC775C0A2E0BEBCECAC2DA (void);
// 0x00000355 System.Int32 CharacterStatusDataExcel::GetCharacterAttack()
extern void CharacterStatusDataExcel_GetCharacterAttack_m7C756E071D02E22FF077464F153C3262843D6CE6 (void);
// 0x00000356 System.Int32 CharacterStatusDataExcel::GetCharacterSpeed()
extern void CharacterStatusDataExcel_GetCharacterSpeed_m6474204350D98D6FD31E8704F08344AA79F6CC07 (void);
// 0x00000357 System.Int32 CharacterStatusDataExcel::GetCharacterDefence()
extern void CharacterStatusDataExcel_GetCharacterDefence_m8FC247E42964FCDF109B450CAC7F27DD77A97699 (void);
// 0x00000358 System.Void CharacterStatusDataExcel::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void CharacterStatusDataExcel_ManualSetUp_mD80A20E7DCAAE4EF668D9A1A6628C73EB5E863EC (void);
// 0x00000359 System.Void CharacterStatusDataExcel::.ctor()
extern void CharacterStatusDataExcel__ctor_m028A7FD176CE958B0DBF78DA78DD5AC4AD0CA031 (void);
// 0x0000035A System.Void CharacterStatusDataScriptable::.ctor()
extern void CharacterStatusDataScriptable__ctor_m8599DB7195453C1EEC57FAD49192A39FDDB23F87 (void);
// 0x0000035B System.Int32 CharacterStatusGameData::GetIndex()
extern void CharacterStatusGameData_GetIndex_m13756F0C23865D764139268A59D20A0CA577955A (void);
// 0x0000035C System.Int32 CharacterStatusGameData::GetCharacterDetailsID()
extern void CharacterStatusGameData_GetCharacterDetailsID_m64D0DB746EF845F9E16A0DE94EEDA36254551C52 (void);
// 0x0000035D System.String CharacterStatusGameData::GetCharacterName()
extern void CharacterStatusGameData_GetCharacterName_m9074DC53447D53E120EF90DC069D3C6455DE5F74 (void);
// 0x0000035E System.Int32 CharacterStatusGameData::GetCharacterHP()
extern void CharacterStatusGameData_GetCharacterHP_m8BCC25F2C04A5D9436BCFADF3AE238522A1A5D36 (void);
// 0x0000035F System.Int32 CharacterStatusGameData::GetCharacterMP()
extern void CharacterStatusGameData_GetCharacterMP_m46A0077EE223E93B7DC3BC6B44DE11FA586170DF (void);
// 0x00000360 System.Int32 CharacterStatusGameData::GetCharacterAttack()
extern void CharacterStatusGameData_GetCharacterAttack_mD5BB2405EB021D67C350C5C091EC00343ACAF143 (void);
// 0x00000361 System.Int32 CharacterStatusGameData::GetCharacterSpeed()
extern void CharacterStatusGameData_GetCharacterSpeed_m91DC75D8C72E21893C687863A5814EAB2DA7C434 (void);
// 0x00000362 System.Int32 CharacterStatusGameData::GetCharacterDefence()
extern void CharacterStatusGameData_GetCharacterDefence_mF929397249D7A330346AF206A1C513F00615135E (void);
// 0x00000363 System.Void CharacterStatusGameData::SetValue(CharacterStatusDataExcel)
extern void CharacterStatusGameData_SetValue_m9138688E6BD0001039C182FB116B7593EBAE2964 (void);
// 0x00000364 System.Void CharacterStatusGameData::.ctor()
extern void CharacterStatusGameData__ctor_m5524C06B528C01E9126B98B5D7F2CF0CEAE6CF13 (void);
// 0x00000365 System.Boolean CharacterStatusDataStorageManager::GetIsSetUp()
extern void CharacterStatusDataStorageManager_GetIsSetUp_mC4CECB30F017767E38DC076670C36928AC5EAAF1 (void);
// 0x00000366 CharacterStatusGameData CharacterStatusDataStorageManager::GetCharacterStatusData(System.Int32,System.Int32)
extern void CharacterStatusDataStorageManager_GetCharacterStatusData_m88C58E468647EE0AC1CE245E62195CB76EF3CFCA (void);
// 0x00000367 System.Void CharacterStatusDataStorageManager::LoadCharacterStatusData(System.Int32)
extern void CharacterStatusDataStorageManager_LoadCharacterStatusData_mB29123EB121B2D08CCCB6AD177B9D6AD6825FEFB (void);
// 0x00000368 System.Void CharacterStatusDataStorageManager::Update()
extern void CharacterStatusDataStorageManager_Update_m073872583DA7BE058044AAB226CED9AE73D79DBD (void);
// 0x00000369 System.Void CharacterStatusDataStorageManager::SetUp()
extern void CharacterStatusDataStorageManager_SetUp_mC9D1BDBA1455A4E9346B1D4855FB0F78957A4E9E (void);
// 0x0000036A System.Void CharacterStatusDataStorageManager::.ctor()
extern void CharacterStatusDataStorageManager__ctor_mBCC563903950FEE2B60BF167CDC5CF5D6331EBC7 (void);
// 0x0000036B System.Void CharacterStatusStorage::AddCharacterStatusGameData(System.Collections.Generic.List`1<CharacterStatusDataExcel>)
extern void CharacterStatusStorage_AddCharacterStatusGameData_m48C84D9D615F84F3A819E5C7C3C736BAF51ED0A2 (void);
// 0x0000036C CharacterStatusGameData CharacterStatusStorage::GetCharacterStatusData(System.Int32)
extern void CharacterStatusStorage_GetCharacterStatusData_mEE53FCBB5E03A16A3DE86C03E66EF95932BA8183 (void);
// 0x0000036D System.Void CharacterStatusStorage::.ctor()
extern void CharacterStatusStorage__ctor_mD3E0F8AA459072A8186CBE98973901D085FAAC45 (void);
// 0x0000036E System.Boolean DataCore::GetIsSetUp()
extern void DataCore_GetIsSetUp_m0A8B8C0E5D1CF7B7CE872A18A2987D250E8AF621 (void);
// 0x0000036F System.Void DataCore::Start()
extern void DataCore_Start_m8FEB69CC4208C3A223B0C3F1B7396396C70081D8 (void);
// 0x00000370 System.Void DataCore::Update()
extern void DataCore_Update_m605200142D589E27A50B839A457B086AC7FD96E8 (void);
// 0x00000371 System.Void DataCore::ReleaseAllDictionary()
extern void DataCore_ReleaseAllDictionary_mAD88AEE09CACA1A06F4B2F49906AAF3910679966 (void);
// 0x00000372 System.Void DataCore::OnDestroy()
extern void DataCore_OnDestroy_m470FCB60EE221CE33935F591D29C034412898F09 (void);
// 0x00000373 System.Void DataCore::SetUpDictionary()
extern void DataCore_SetUpDictionary_m0DF9AF67274DE0D17355E4044CD540F8978873E7 (void);
// 0x00000374 CharacterStatusGameData DataCore::GetCharacterStatusData(System.Int32,System.Int32)
extern void DataCore_GetCharacterStatusData_m0D9BFE3D3AB4861FDE76DD098801F7D0E5F7E20D (void);
// 0x00000375 System.Void DataCore::LoadCharacterStatusData(System.Int32)
extern void DataCore_LoadCharacterStatusData_m0255CE3643466BADC623480EEBE55965ADA59796 (void);
// 0x00000376 System.Collections.Generic.List`1<DictionaryData> DataCore::GetListDictionaryData(DictionaryType_Type)
extern void DataCore_GetListDictionaryData_m124D578213DF9AE42FCFB155C527E7E8638458EC (void);
// 0x00000377 DictionaryData DataCore::GetCharacterDictionaryData(System.Int32)
extern void DataCore_GetCharacterDictionaryData_mB23206E79918D05B5F17E1728B8B4D7CBD1E4DF0 (void);
// 0x00000378 DictionaryData DataCore::GetCharacterDictionaryData(System.String)
extern void DataCore_GetCharacterDictionaryData_mCC5E730945753AFCAA20B2EB4DA521AF501B2F1D (void);
// 0x00000379 DictionaryData DataCore::GetEffectDictionaryData(System.Int32)
extern void DataCore_GetEffectDictionaryData_m9B3269272C71E6839C6B413F39CA288248D137F8 (void);
// 0x0000037A DictionaryData DataCore::GetEffectDictionaryData(System.String)
extern void DataCore_GetEffectDictionaryData_m0429D7DE4EB67B0968D9CC7017AA073095546699 (void);
// 0x0000037B DictionaryData DataCore::GetAnimationDictionaryData(System.Int32)
extern void DataCore_GetAnimationDictionaryData_m12843100D835646B501AFD4BD751AE46C092FBE9 (void);
// 0x0000037C DictionaryData DataCore::GetAnimationDictionaryData(System.String)
extern void DataCore_GetAnimationDictionaryData_m036A35A52ED6B5BECFCF676D82D5FFE229AEFBC2 (void);
// 0x0000037D DictionaryData DataCore::GetMotionParameterActionDictionaryData(System.Int32)
extern void DataCore_GetMotionParameterActionDictionaryData_m002684EED9DBBF24BADA64285E9796AEE5A5CC3C (void);
// 0x0000037E DictionaryData DataCore::GetMotionParameterActionDictionaryData(System.String)
extern void DataCore_GetMotionParameterActionDictionaryData_m217E74B7B153987DAFC7D7A1CB19DD54B7D7F01F (void);
// 0x0000037F System.Collections.Generic.List`1<System.String> DataCore::GetMotionParameterActionDictionnaryName()
extern void DataCore_GetMotionParameterActionDictionnaryName_m18C527108C2815A7062033C390023AD89EF14E8C (void);
// 0x00000380 System.Void DataCore::.ctor()
extern void DataCore__ctor_m45A27E003FDC3A2134AF2A576290A0893277F0FC (void);
// 0x00000381 System.Int32 DictionaryAccessor::GetID(DictionaryData)
extern void DictionaryAccessor_GetID_m7684C8951AA1CBC2CAA606B8D76001D58F0E50FF (void);
// 0x00000382 System.String DictionaryAccessor::GetName(DictionaryData)
extern void DictionaryAccessor_GetName_m4F86102B103F5DF66B81E8A2AAAD9451E9ADD663 (void);
// 0x00000383 System.String DictionaryAccessor::GetDetails(DictionaryData)
extern void DictionaryAccessor_GetDetails_m753BE1F723D716FDB54BDA0F3109FE157B014611 (void);
// 0x00000384 DictionaryData DictionaryAccessor::GetCharacterDictionaryData(System.Int32)
extern void DictionaryAccessor_GetCharacterDictionaryData_m5B0A706E413CA222DC586B4CFEEB67E8A7C294DD (void);
// 0x00000385 DictionaryData DictionaryAccessor::GetCharacterDictionaryData(System.String)
extern void DictionaryAccessor_GetCharacterDictionaryData_mC82D073408213AABE1F9812EE19AA960300DF566 (void);
// 0x00000386 DictionaryData DictionaryAccessor::GetEffectDictionaryData(System.Int32)
extern void DictionaryAccessor_GetEffectDictionaryData_m2DEF734A419516E8E66481DE222CE8D916AFDACA (void);
// 0x00000387 DictionaryData DictionaryAccessor::GetEffectDictionaryData(System.String)
extern void DictionaryAccessor_GetEffectDictionaryData_mE7C860AEBDA3A7665B8CDEB4061D6C50F79CE681 (void);
// 0x00000388 DictionaryData DictionaryAccessor::GetAnimationDictionaryData(System.Int32)
extern void DictionaryAccessor_GetAnimationDictionaryData_mBF41EA7E7A04DD660948155F2D6B8ECF10B76489 (void);
// 0x00000389 DictionaryData DictionaryAccessor::GetAnimationDictionaryData(System.String)
extern void DictionaryAccessor_GetAnimationDictionaryData_mD9D22AA88DD1A47D446AEB2B87E1E42AE7DAAEE6 (void);
// 0x0000038A DictionaryData DictionaryAccessor::GetMotionParameterActionDictionaryData(System.Int32)
extern void DictionaryAccessor_GetMotionParameterActionDictionaryData_m41C51442F3478F3B20F11494406D6EA3DD2450A2 (void);
// 0x0000038B DictionaryData DictionaryAccessor::GetMotionParameterActionDictionaryData(System.String)
extern void DictionaryAccessor_GetMotionParameterActionDictionaryData_m778D7A115E70D4BAFE6E1F342026F50825390107 (void);
// 0x0000038C System.Collections.Generic.List`1<System.String> DictionaryAccessor::GetMotionParameterActionDictionaryName()
extern void DictionaryAccessor_GetMotionParameterActionDictionaryName_m836ED134790E004E8030F5B5DB4969840147DCB0 (void);
// 0x0000038D System.Collections.Generic.List`1<DictionaryData> DictionaryAccessor::GetListDictionaryData(DictionaryType_Type)
extern void DictionaryAccessor_GetListDictionaryData_m27DAE0CDA95F2CD8F9A2B79290634129B2C29E09 (void);
// 0x0000038E System.Int32 DictionaryData::GetID()
extern void DictionaryData_GetID_m746B5287B1FF36555D7C28EB17AA98B3BE60D4D8 (void);
// 0x0000038F System.String DictionaryData::GetName()
extern void DictionaryData_GetName_m373B190631BECE728BE7AD7984F86BA062C1F670 (void);
// 0x00000390 System.String DictionaryData::GetDetails()
extern void DictionaryData_GetDetails_m97CE55200B7CC7C6CF713BF085E0DD4B2DF11C34 (void);
// 0x00000391 System.Void DictionaryData::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void DictionaryData_ManualSetUp_m30FD4500863A82D0D12A3250DACD4D2C134269C2 (void);
// 0x00000392 System.Void DictionaryData::.ctor()
extern void DictionaryData__ctor_mD23E41B802502D2CD7ACE86E185A640E436F23C1 (void);
// 0x00000393 System.Void DictionaryScriptable::.ctor()
extern void DictionaryScriptable__ctor_m5032BEB1C0C4A4C6FFABE52049C8B080049D3685 (void);
// 0x00000394 System.Boolean DictionaryIntensive::GetIsSetUp()
extern void DictionaryIntensive_GetIsSetUp_mC2E2470F0E6254CF710F03645336966AEC7EA97B (void);
// 0x00000395 System.Void DictionaryIntensive::Init()
extern void DictionaryIntensive_Init_m34F1EFB998071DD25F5BE02E481E7E2E5AC2F634 (void);
// 0x00000396 System.Void DictionaryIntensive::Update()
extern void DictionaryIntensive_Update_m86DC2FFAC2701C22FC065BFF10983F752390B1B3 (void);
// 0x00000397 System.Void DictionaryIntensive::ReleaseAll()
extern void DictionaryIntensive_ReleaseAll_m3957B18FB2B9F41EF7F65939E4616AF7529A483F (void);
// 0x00000398 System.Collections.Generic.List`1<DictionaryData> DictionaryIntensive::GetListDictionaryData(DictionaryType_Type)
extern void DictionaryIntensive_GetListDictionaryData_mEED18B6E46091222C2BAB1D594BCFD38B1C09039 (void);
// 0x00000399 DictionaryData DictionaryIntensive::GetCharacterDictionaryData(System.Int32)
extern void DictionaryIntensive_GetCharacterDictionaryData_m61844688A105D74D0FB709421019C5A393672FA7 (void);
// 0x0000039A DictionaryData DictionaryIntensive::GetCharacterDictionaryData(System.String)
extern void DictionaryIntensive_GetCharacterDictionaryData_m8C453E9E9AF2F34D8D9FE60656507D9429419092 (void);
// 0x0000039B DictionaryData DictionaryIntensive::GetEffectDictionaryData(System.Int32)
extern void DictionaryIntensive_GetEffectDictionaryData_mC107A1800281F3F94AFBEB51C9B296CADCFA5321 (void);
// 0x0000039C DictionaryData DictionaryIntensive::GetEffectDictionaryData(System.String)
extern void DictionaryIntensive_GetEffectDictionaryData_m760E6B8E2775A2E291FEB240545E63E0CE9351A5 (void);
// 0x0000039D DictionaryData DictionaryIntensive::GetAnimationDictionaryData(System.Int32)
extern void DictionaryIntensive_GetAnimationDictionaryData_m0B8D10BA2234435E7933ED4DE6ABF741BE8CB60B (void);
// 0x0000039E DictionaryData DictionaryIntensive::GetAnimationDictionaryData(System.String)
extern void DictionaryIntensive_GetAnimationDictionaryData_m3A274602AF8A720A72777F38296DE7316376E9CC (void);
// 0x0000039F DictionaryData DictionaryIntensive::GetMotionParameterActionDictionaryData(System.Int32)
extern void DictionaryIntensive_GetMotionParameterActionDictionaryData_mA995265BB84433EAD02E5874EBDAA4531D724542 (void);
// 0x000003A0 System.Collections.Generic.List`1<System.String> DictionaryIntensive::GetMotionParameterActionDictionnaryName(DictionaryType_Type)
extern void DictionaryIntensive_GetMotionParameterActionDictionnaryName_mAE8A651E1B878C700ECD22186C838961AA60C27F (void);
// 0x000003A1 DictionaryData DictionaryIntensive::GetMotionParameterActionDictionaryData(System.String)
extern void DictionaryIntensive_GetMotionParameterActionDictionaryData_m9FCB01FEAC3BBBAAC8411C5FA0313FB5120EC5DF (void);
// 0x000003A2 DictionaryData DictionaryIntensive::GetDictionaryData(DictionaryType_Type,System.Int32)
extern void DictionaryIntensive_GetDictionaryData_m05759ADE85744C2A484CEE08F485999D634B7D49 (void);
// 0x000003A3 DictionaryData DictionaryIntensive::GetDictionaryData(DictionaryType_Type,System.String)
extern void DictionaryIntensive_GetDictionaryData_mFA2A3B01E01380BC70A60DDCD21897B61E9B288B (void);
// 0x000003A4 System.Collections.Generic.List`1<System.String> DictionaryIntensive::GetDictionnaryName(DictionaryType_Type)
extern void DictionaryIntensive_GetDictionnaryName_mC854EACDB357C4706149470E214F3B1FFF270DCB (void);
// 0x000003A5 System.Void DictionaryIntensive::.ctor()
extern void DictionaryIntensive__ctor_m4494220AB0F432F098A8B111487F6598C7F9F6DE (void);
// 0x000003A6 System.Collections.Generic.List`1<DictionaryData> DictionaryType::GetDictionaryData()
extern void DictionaryType_GetDictionaryData_m4E91EC1D07425D7B7E89BF9C168F3B1E5EBD97CF (void);
// 0x000003A7 System.Boolean DictionaryType::GetIsSetUp()
extern void DictionaryType_GetIsSetUp_m3B1EDAC50C04170393B4AD5DF2E18DDB0A8F1984 (void);
// 0x000003A8 System.Void DictionaryType::Load(DictionaryType_Type)
extern void DictionaryType_Load_mBD2A3A3F2C472DEB2045E3DEAF23298A54C0E447 (void);
// 0x000003A9 System.Void DictionaryType::Update()
extern void DictionaryType_Update_m060FF974AF4CE4DDCA4C503E5AEEE2DEA28BF029 (void);
// 0x000003AA System.String DictionaryType::GetTypeName(DictionaryType_Type)
extern void DictionaryType_GetTypeName_m32A534E487EC269FBE4B5F185448FED05C971F87 (void);
// 0x000003AB System.Void DictionaryType::ValueData()
extern void DictionaryType_ValueData_m1CB5A9C603441A96FE896446A9C4739C667E9559 (void);
// 0x000003AC DictionaryData DictionaryType::SerachID(System.Int32)
extern void DictionaryType_SerachID_mA785B29B0914582B408AC9D78CBA8BF68727B8F1 (void);
// 0x000003AD DictionaryData DictionaryType::SerachName(System.String)
extern void DictionaryType_SerachName_m3EE6DD094F1394D3C7EA9B571D66874135DCDC70 (void);
// 0x000003AE System.Collections.Generic.List`1<System.String> DictionaryType::GetDictionnaryName()
extern void DictionaryType_GetDictionnaryName_mD0BBCC5C1CF698D3F5EABF04B28BBF087DB3BBEA (void);
// 0x000003AF System.Void DictionaryType::Release()
extern void DictionaryType_Release_mE15BF3A91C507A16C6A5382955282583E25332AA (void);
// 0x000003B0 System.Void DictionaryType::.ctor()
extern void DictionaryType__ctor_m948B3A5E0E952B20F40CC82E879023131BBE9FF4 (void);
// 0x000003B1 System.Void BaseComposition::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void BaseComposition_ManualSetUp_mFC6BF9A6B7394C2EF9CD91757397F5ABD82C8B2C (void);
// 0x000003B2 System.UInt32 BaseComposition::CountField()
extern void BaseComposition_CountField_m3482591D26EEEE2AA0BDC9542042EEF0836E19F2 (void);
// 0x000003B3 System.String BaseComposition::GetVariableName(System.UInt32)
extern void BaseComposition_GetVariableName_mB65B081BE3A90A168A2131D8785D926C1C5A7857 (void);
// 0x000003B4 System.Object BaseComposition::GetValue(System.String)
extern void BaseComposition_GetValue_m98D6BEAA683D5A5E368984926F0BC9F9D3F32E70 (void);
// 0x000003B5 System.Void BaseComposition::SetValue(System.String,System.Object)
extern void BaseComposition_SetValue_m3CDC2E78A64BB6E9E9CB668905D18A90C9AD2843 (void);
// 0x000003B6 System.Void BaseComposition::.ctor()
extern void BaseComposition__ctor_mA047BEB8CDC3173DD1C7B3A575C011712CFCB3D2 (void);
// 0x000003B7 System.String DataFrame::get_DataName()
extern void DataFrame_get_DataName_m81205B4F31AF9F317715E1E1F5641710575F002B (void);
// 0x000003B8 System.String DataFrame::get_VariableName()
extern void DataFrame_get_VariableName_m8A742877D9BB6A1278C2D901A5E72BCA97178AA9 (void);
// 0x000003B9 System.Void DataFrame::set_Data(System.Object)
extern void DataFrame_set_Data_m42EC3E451707481CED9859B7C87E7054691110AD (void);
// 0x000003BA System.Object DataFrame::get_Data()
extern void DataFrame_get_Data_m1ACCC0F5ACE88B76B716528616569B4EC647F7EF (void);
// 0x000003BB System.Void DataFrame::SetUp(System.String,System.String,System.Object)
extern void DataFrame_SetUp_mF3EF36185252F0F065D30004BAF2A566EFDF8DDB (void);
// 0x000003BC System.Void DataFrame::.ctor()
extern void DataFrame__ctor_mD69CBBB27AD792B8C8C3F133A16F8EE860475460 (void);
// 0x000003BD System.Collections.Generic.List`1<T> DataFrameControl`1::get_GetListDataComposition()
// 0x000003BE System.Void DataFrameControl`1::SetUp()
// 0x000003BF System.Void DataFrameControl`1::Clear()
// 0x000003C0 DataFrameGroup DataFrameControl`1::AddDataFrameGrop()
// 0x000003C1 System.Void DataFrameControl`1::ValuData(System.UInt32,System.String,System.Object)
// 0x000003C2 DataFrameGroup DataFrameControl`1::GetDataFrameGroupIndex(System.UInt32)
// 0x000003C3 System.Void DataFrameControl`1::.ctor()
// 0x000003C4 System.Void DataFrameGroup::SetUp()
extern void DataFrameGroup_SetUp_m51D0A89F04C6BA194C9E24FF9881C6712735A8FB (void);
// 0x000003C5 System.Void DataFrameGroup::AddData(System.String,System.String,System.Object)
extern void DataFrameGroup_AddData_m222094A6FDB29234C36D03E39E62A1F14BFEE899 (void);
// 0x000003C6 System.Void DataFrameGroup::Clear()
extern void DataFrameGroup_Clear_mE011D71D5675F6FCC73D51F10B32ED370859A2FF (void);
// 0x000003C7 System.Void DataFrameGroup::ValueData(System.String,System.Object)
extern void DataFrameGroup_ValueData_m126533448E6DC55D2E5D6ECBEC60FE3709768E32 (void);
// 0x000003C8 DataFrame DataFrameGroup::GetSearchDataFrame(System.String)
extern void DataFrameGroup_GetSearchDataFrame_m76AC6F9F71E23E858F4DC613DFA49A4496B79C29 (void);
// 0x000003C9 System.Void DataFrameGroup::.ctor()
extern void DataFrameGroup__ctor_mAD765F6C072B84BD8FDDBB5421F13F2357888E01 (void);
// 0x000003CA System.Void DataFrameGroup_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m6A12B78BE79BE2997B791C9BC4AFD2F1C0A417BE (void);
// 0x000003CB System.Boolean DataFrameGroup_<>c__DisplayClass5_0::<GetSearchDataFrame>b__0(DataFrame)
extern void U3CU3Ec__DisplayClass5_0_U3CGetSearchDataFrameU3Eb__0_m4410D5DCC22D50668FBD8714D65F04DD432BEE4B (void);
// 0x000003CC System.Collections.Generic.List`1<T> ListDataObject`1::GetListDataObject()
// 0x000003CD System.Void ListDataObject`1::SetListDataObject(System.Collections.Generic.List`1<T>)
// 0x000003CE System.Void ListDataObject`1::SetUp()
// 0x000003CF System.Void ListDataObject`1::.ctor()
// 0x000003D0 System.Void ExcelScenarioText::.ctor()
extern void ExcelScenarioText__ctor_m138A3814195D23ACA2EA65D95932BA0A87A2997F (void);
// 0x000003D1 System.Int32 ScenarioText::GeID()
extern void ScenarioText_GeID_m49DA8A7D661B1D82B4379CA1897415C5F6AD477F (void);
// 0x000003D2 ScenarioText_type ScenarioText::GetName()
extern void ScenarioText_GetName_m33288A563BF19267C13C1782416B052BBC2AF22B (void);
// 0x000003D3 System.String ScenarioText::GetText()
extern void ScenarioText_GetText_m4266A056427F6F0DA9E25CB2C8C1825DE50AF2A3 (void);
// 0x000003D4 System.Void ScenarioText::.ctor()
extern void ScenarioText__ctor_m1A95B5EF6D31D3AC32D416A63A87CF63CB01FB05 (void);
// 0x000003D5 System.Void ScenarioText::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void ScenarioText_ManualSetUp_m412661E58E400676494A56F92A70353226F7B38B (void);
// 0x000003D6 System.Collections.Generic.List`1<T> BaseExcelScriptableObject`1::GetListData()
// 0x000003D7 T BaseExcelScriptableObject`1::GetDataObject(System.Int32)
// 0x000003D8 System.Void BaseExcelScriptableObject`1::SetListData(System.Collections.Generic.List`1<T>)
// 0x000003D9 System.Void BaseExcelScriptableObject`1::set_DataID(System.UInt32)
// 0x000003DA System.UInt32 BaseExcelScriptableObject`1::get_DataID()
// 0x000003DB System.Void BaseExcelScriptableObject`1::.ctor()
// 0x000003DC System.Void BaseExcelScriptableObject`1::Init()
// 0x000003DD System.Collections.Generic.List`1<T> Serialization`1::ToList()
// 0x000003DE System.Void Serialization`1::.ctor(System.Collections.Generic.List`1<T>)
// 0x000003DF System.Collections.Generic.Dictionary`2<TKey,TValue> Serialization`2::ToDictionary()
// 0x000003E0 System.Void Serialization`2::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// 0x000003E1 System.Void Serialization`2::OnBeforeSerialize()
// 0x000003E2 System.Void Serialization`2::OnAfterDeserialize()
// 0x000003E3 T ExpansionSystem::AllComponent(UnityEngine.GameObject)
// 0x000003E4 T[] ExpansionSystem::AllComponents(UnityEngine.GameObject)
// 0x000003E5 System.Int32 MotionData::GetActionIndex()
extern void MotionData_GetActionIndex_m641F66C41B544BA23F15E6B0B53160797FF495D4 (void);
// 0x000003E6 System.String MotionData::GetActionName()
extern void MotionData_GetActionName_mE5290A22C9B2335A830A380C9E6BB9C93B58DFC7 (void);
// 0x000003E7 System.String MotionData::GetActionRoleName()
extern void MotionData_GetActionRoleName_mB2D5E594BD6E57056042861CC15906F97F569412 (void);
// 0x000003E8 System.Int32 MotionData::GetActionID()
extern void MotionData_GetActionID_m41115383319DF933DDCCC2892921AFC4D87321C5 (void);
// 0x000003E9 System.Single MotionData::GetActionFrame()
extern void MotionData_GetActionFrame_mD7C5B2AB2D6C5B8E1B59B873637B3F80C704501C (void);
// 0x000003EA System.Single MotionData::GetActionLifeFrame()
extern void MotionData_GetActionLifeFrame_mCF3B4D99586C0D373B73F0EF5C2AF0D5729555FC (void);
// 0x000003EB System.Int32 MotionData::GetActionInt01()
extern void MotionData_GetActionInt01_m9AB1F5EA4FB2FB9F1D40E54E19090A80FD6069E1 (void);
// 0x000003EC System.Int32 MotionData::GetActionInt02()
extern void MotionData_GetActionInt02_mB8070BCA64DB57C920856A58163D00823DF5E02E (void);
// 0x000003ED System.Int32 MotionData::GetActionInt03()
extern void MotionData_GetActionInt03_m6F2628261D0A63218FAB1F939990F1D17042167C (void);
// 0x000003EE System.Int32 MotionData::GetActionInt04()
extern void MotionData_GetActionInt04_m2CE14490D9DF1859851B56843C0CFBC59C88DB9F (void);
// 0x000003EF System.Single MotionData::GetActionFloat01()
extern void MotionData_GetActionFloat01_mE95740A24F3119357301A2E5C01D39B231A24E3F (void);
// 0x000003F0 System.Single MotionData::GetActionFloat02()
extern void MotionData_GetActionFloat02_m9FC3F68D343267BD93C5B5F4C72B550EB914C273 (void);
// 0x000003F1 System.Single MotionData::GetActionFloat03()
extern void MotionData_GetActionFloat03_m5407DE1D24AE88B1AA1095E73F8D9269C452ADF3 (void);
// 0x000003F2 System.Single MotionData::GetActionFloat04()
extern void MotionData_GetActionFloat04_m3940E23FCE5EC616DB4E002797C7F1DD5E0C9D93 (void);
// 0x000003F3 System.String MotionData::GetActionString01()
extern void MotionData_GetActionString01_m39818C4FD40FEA9ABBF39021C17CB521E5A9BF14 (void);
// 0x000003F4 System.String MotionData::GetActionString02()
extern void MotionData_GetActionString02_mB2779642090130C0D396841C72681829A473F3EB (void);
// 0x000003F5 System.Void MotionData::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void MotionData_ManualSetUp_mB290665D65D668996652C9F761228E89A63F53D3 (void);
// 0x000003F6 System.Void MotionData::.ctor()
extern void MotionData__ctor_m317D9BB4CB3FBBDDEF649F3CF7346C20DB752B91 (void);
// 0x000003F7 System.Void MotionDataScriptable::.ctor()
extern void MotionDataScriptable__ctor_m961832DE37C6327B6DBB69DD3EC9E6085C7A6648 (void);
// 0x000003F8 System.Int32 MotionParameterConvert::ConvertMotionActionID(System.String)
extern void MotionParameterConvert_ConvertMotionActionID_m2BC2EC4EE419F1281F620F168517EF7C824FC630 (void);
// 0x000003F9 MotionParameterConvert_MotionActionName MotionParameterConvert::ConvertMotionActionNameID(System.String)
extern void MotionParameterConvert_ConvertMotionActionNameID_m7756966D052FABF19F14B2D73F7AE5179F794E39 (void);
// 0x000003FA System.Int32 MotionParameterConvert::ConvertMotionActionRoleID(System.String)
extern void MotionParameterConvert_ConvertMotionActionRoleID_m2ACA18E6505050CD7982D3465F1208F705EF163A (void);
// 0x000003FB MotionParameterConvert_MotionActionRoleName MotionParameterConvert::ConvertMotionActionRoleNameID(System.String)
extern void MotionParameterConvert_ConvertMotionActionRoleNameID_m57AB5A036378D57A7B846095ED78D2BD087E5742 (void);
// 0x000003FC System.Void MotionParameterConvert::.cctor()
extern void MotionParameterConvert__cctor_mBBD7CBFD4DED20CFA62861399F765B11D0ABC247 (void);
// 0x000003FD UnityEngine.GameObject MotionParameterDetails::GetThisObject()
extern void MotionParameterDetails_GetThisObject_m52ED51AD7348D4C14F33FEA7CDA186C16247FDD1 (void);
// 0x000003FE System.Void MotionParameterDetails::SetTargetObject(UnityEngine.GameObject)
extern void MotionParameterDetails_SetTargetObject_mDF4648226C2E8DD21D6A7879AF9EDC7C76FCB02E (void);
// 0x000003FF UnityEngine.GameObject MotionParameterDetails::GetTargetObject()
extern void MotionParameterDetails_GetTargetObject_m59F41575DCF8F33C168503BB1FE8CC7578A59904 (void);
// 0x00000400 System.Boolean MotionParameterDetails::GetIsSetUp()
extern void MotionParameterDetails_GetIsSetUp_mB25657608598060FA44C1CC96424FCFE1F043ABE (void);
// 0x00000401 System.Void MotionParameterDetails::Init(System.String,UnityEngine.GameObject)
extern void MotionParameterDetails_Init_m9DA1838C982D980D51DD6B23A3225B0D8C1023DC (void);
// 0x00000402 System.Void MotionParameterDetails::Update()
extern void MotionParameterDetails_Update_m73240D7E10760ECFE43F32EC91C9F2DFB5412B84 (void);
// 0x00000403 MotionParameterDetailsData MotionParameterDetails::GetMotionActionData(System.Int32,System.Int32)
extern void MotionParameterDetails_GetMotionActionData_m79FD90A9FDDCE1D142D0C7CF55A1206DD6D64DF9 (void);
// 0x00000404 System.Collections.Generic.List`1<System.Int32> MotionParameterDetails::GetEffectIDList()
extern void MotionParameterDetails_GetEffectIDList_m969C4F577789B102401602B2FAC91D2B4B7ED71B (void);
// 0x00000405 System.Collections.Generic.List`1<System.Int32> MotionParameterDetails::GetBulletIDList()
extern void MotionParameterDetails_GetBulletIDList_m4E1579A574529B6CDAC75CF446445DADBEB25662 (void);
// 0x00000406 System.UInt32 MotionParameterDetails::GetMotionActionLength(System.Int32)
extern void MotionParameterDetails_GetMotionActionLength_mBCA6730A8A3B4C030226DEE46A3F2C9BC33A247A (void);
// 0x00000407 System.Void MotionParameterDetails::DataInput()
extern void MotionParameterDetails_DataInput_m6EF243B6B8FEF55E1160DA6E5D1B45CC5F864E53 (void);
// 0x00000408 System.Void MotionParameterDetails::Release()
extern void MotionParameterDetails_Release_mC1086BAC98B59085935DF7B469F654F2D58A2920 (void);
// 0x00000409 System.Void MotionParameterDetails::.ctor()
extern void MotionParameterDetails__ctor_m6FD5F1EC78B0D145D0E6CC59561B8B97E3249C17 (void);
// 0x0000040A System.Int32 MotionParameterDetailsData::GetActionIndex()
extern void MotionParameterDetailsData_GetActionIndex_m26E712184010FAA32EC0259D149E997FC5F9DC79 (void);
// 0x0000040B System.String MotionParameterDetailsData::GetActionName()
extern void MotionParameterDetailsData_GetActionName_mC36AD17B48B738456D4F892C81E9D0E278AC861C (void);
// 0x0000040C System.String MotionParameterDetailsData::GetActionRoleName()
extern void MotionParameterDetailsData_GetActionRoleName_mFF3BB3AEA49617080895D451515C0E90A9B88171 (void);
// 0x0000040D System.Int32 MotionParameterDetailsData::GetActionID()
extern void MotionParameterDetailsData_GetActionID_mD291906C6947291EABCDC9194543E2E9287720DB (void);
// 0x0000040E System.Single MotionParameterDetailsData::GetActionFrame()
extern void MotionParameterDetailsData_GetActionFrame_mB985563B7402202A2531B19DB7D793B66096EE43 (void);
// 0x0000040F System.Single MotionParameterDetailsData::GetActionLifeFrame()
extern void MotionParameterDetailsData_GetActionLifeFrame_m455CD9FC8E29F0AE09B79C280629090A8F7E1676 (void);
// 0x00000410 System.Int32 MotionParameterDetailsData::GetActionInt01()
extern void MotionParameterDetailsData_GetActionInt01_m42C43703A64FE226CFFBCDDBA51509B5667D329A (void);
// 0x00000411 System.Int32 MotionParameterDetailsData::GetActionInt02()
extern void MotionParameterDetailsData_GetActionInt02_m60BEE9649ADE1EEDA66AD3E9EDDB8A940CB1A09D (void);
// 0x00000412 System.Int32 MotionParameterDetailsData::GetActionInt03()
extern void MotionParameterDetailsData_GetActionInt03_mE9B1F0CAD393C60DEEDA8831B17494CB453FF7E2 (void);
// 0x00000413 System.Int32 MotionParameterDetailsData::GetActionInt04()
extern void MotionParameterDetailsData_GetActionInt04_m11EE893F4BBE4F1EB004E0D1C3178E9EABF31F0B (void);
// 0x00000414 System.Single MotionParameterDetailsData::GetActionFloat01()
extern void MotionParameterDetailsData_GetActionFloat01_m85A68FBB7BECDA4B20185A84E098423BC80C4378 (void);
// 0x00000415 System.Single MotionParameterDetailsData::GetActionFloat02()
extern void MotionParameterDetailsData_GetActionFloat02_m539711EBEC42983C3B6FEFFFACE123E777952A83 (void);
// 0x00000416 System.Single MotionParameterDetailsData::GetActionFloat03()
extern void MotionParameterDetailsData_GetActionFloat03_mEDDE752EE854131AC0A77AB8987DD4F4E5E101DD (void);
// 0x00000417 System.Single MotionParameterDetailsData::GetActionFloat04()
extern void MotionParameterDetailsData_GetActionFloat04_m6E946AFFBD8C576D6BCF1BCFDAE2FDF80FA8B8FD (void);
// 0x00000418 System.String MotionParameterDetailsData::GetActionString01()
extern void MotionParameterDetailsData_GetActionString01_mCD40A4D81741026EF1433CF312ED8CE0F1801908 (void);
// 0x00000419 System.String MotionParameterDetailsData::GetActionString02()
extern void MotionParameterDetailsData_GetActionString02_mC7D78285CD33E1A18371E6DECC64CCB8E6B20AFD (void);
// 0x0000041A System.Void MotionParameterDetailsData::SetUp(MotionData)
extern void MotionParameterDetailsData_SetUp_mB8682A8F6444801FD2650EB19BF07FAF465DBD68 (void);
// 0x0000041B System.Void MotionParameterDetailsData::.ctor()
extern void MotionParameterDetailsData__ctor_m0DE2571A71E96C71169EDA68C8737A4D7312E0D0 (void);
// 0x0000041C System.Void MotionParameterDetailsDataControl::Init()
extern void MotionParameterDetailsDataControl_Init_mFD3A157CE36521B7F0D243B524DF4F525AE89487 (void);
// 0x0000041D System.Collections.Generic.List`1<System.Int32> MotionParameterDetailsDataControl::GetEffectIDList()
extern void MotionParameterDetailsDataControl_GetEffectIDList_mBAEA2CC54457414625B21720F8925EED2CB0F2A5 (void);
// 0x0000041E System.Collections.Generic.List`1<System.Int32> MotionParameterDetailsDataControl::GetBulletIDList()
extern void MotionParameterDetailsDataControl_GetBulletIDList_m5C94AFDAC14652101F7A75D3B1A5E2E47B367A79 (void);
// 0x0000041F System.Void MotionParameterDetailsDataControl::AddListMotionData(System.Collections.Generic.List`1<MotionData>)
extern void MotionParameterDetailsDataControl_AddListMotionData_m7A7E7A704F70B9B50D9B62801899C08F1442C4EC (void);
// 0x00000420 MotionParameterDetailsData MotionParameterDetailsDataControl::GetMotionActionData(System.Int32,System.Int32)
extern void MotionParameterDetailsDataControl_GetMotionActionData_m34D6BA4C49BE67B086F6A0F4D12DFE5FA0B12FBE (void);
// 0x00000421 System.UInt32 MotionParameterDetailsDataControl::GetMotionActionLength(System.Int32)
extern void MotionParameterDetailsDataControl_GetMotionActionLength_m5680B23A52927ED35616025221D9C077C84C772E (void);
// 0x00000422 System.Void MotionParameterDetailsDataControl::Release()
extern void MotionParameterDetailsDataControl_Release_m4C5EF3CEF12DADAF3ED0C4242869F156254D3BFA (void);
// 0x00000423 System.Void MotionParameterDetailsDataControl::.ctor()
extern void MotionParameterDetailsDataControl__ctor_m3D704BA8D726E5283465AEFF17E82D998A493F04 (void);
// 0x00000424 System.Boolean MotionParameter::GetIsSetUp()
extern void MotionParameter_GetIsSetUp_mAEBA047C7D4DC8B358CE0A6AC4E1B0E90DB7EEF5 (void);
// 0x00000425 System.Void MotionParameter::SetTargetGameObject(UnityEngine.GameObject)
extern void MotionParameter_SetTargetGameObject_m268CFF7B817F795554FA3D5D27A9F2362C00876E (void);
// 0x00000426 System.Collections.Generic.List`1<System.Int32> MotionParameter::GetEffectIDList()
extern void MotionParameter_GetEffectIDList_mC9B34F946D489316F4108886517ABE5735300EC1 (void);
// 0x00000427 System.Collections.Generic.List`1<System.Int32> MotionParameter::GetBulletIDList()
extern void MotionParameter_GetBulletIDList_m94867ACD4CEE92ED39488ABDD552BCCF44BFD3CE (void);
// 0x00000428 System.Void MotionParameter::Init(UnityEngine.GameObject,System.String,BattleUnit)
extern void MotionParameter_Init_mA450F1CCB85B39D4BBC7F9D208BFF8F6792A5A7E (void);
// 0x00000429 System.Void MotionParameter::Update(BattleUnit)
extern void MotionParameter_Update_m0AA30BA6A0CF2C81CA22ED5C597C4B886EA85EC9 (void);
// 0x0000042A System.Void MotionParameter::PlayAction(System.Int32,System.Boolean)
extern void MotionParameter_PlayAction_m3425B8C5B82CD92DE945FF7C3AD6F1C208A6723F (void);
// 0x0000042B System.Void MotionParameter::PlayAction(MotionParameterConvert_MotionActionName,System.Boolean)
extern void MotionParameter_PlayAction_m795C4EC5156580408CFDE85F05B4DA9042002E65 (void);
// 0x0000042C System.Void MotionParameter::Release()
extern void MotionParameter_Release_mFA7E5C01CDE48FDAEAC823A511CC01CD96AB7123 (void);
// 0x0000042D System.Void MotionParameter::.ctor()
extern void MotionParameter__ctor_m3E333F7411A9433A6540C75DEBA62B42C397512E (void);
// 0x0000042E System.Boolean BaseMotionParameterRoleDataControl::GetLifeOver()
extern void BaseMotionParameterRoleDataControl_GetLifeOver_mE15E2FA10B592F6FBFFAD0861E2FF76AAC6FDEBA (void);
// 0x0000042F System.Void BaseMotionParameterRoleDataControl::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void BaseMotionParameterRoleDataControl_Init_m7E0E7A502337EAE7CC67A335FA3DAF016296736C (void);
// 0x00000430 System.Void BaseMotionParameterRoleDataControl::ActionInit()
extern void BaseMotionParameterRoleDataControl_ActionInit_m68B8E535BD42D8AAD25688343D7F3A4EC875BD4A (void);
// 0x00000431 System.Void BaseMotionParameterRoleDataControl::TimeUpdate()
extern void BaseMotionParameterRoleDataControl_TimeUpdate_mC7EE3C7812EDBAA236DEF5356A212356322ACA75 (void);
// 0x00000432 System.Void BaseMotionParameterRoleDataControl::Updata()
extern void BaseMotionParameterRoleDataControl_Updata_m8770212E6FE80E272D1B9FD839F1C01F666E71B7 (void);
// 0x00000433 System.Void BaseMotionParameterRoleDataControl::AnotherUpdate()
extern void BaseMotionParameterRoleDataControl_AnotherUpdate_m98CE1DA1B99D7C934295F93F38885C9D4F528261 (void);
// 0x00000434 System.Void BaseMotionParameterRoleDataControl::.ctor()
extern void BaseMotionParameterRoleDataControl__ctor_mDE5EB050C1C6DD8DF57E6AB449DBADBEBC07E2E0 (void);
// 0x00000435 System.Int32 BaseMotionParameterRolePerformance::GetRoleDataID()
extern void BaseMotionParameterRolePerformance_GetRoleDataID_mF51583B24F60B40A90C60B8F0E7637D354FFB7E1 (void);
// 0x00000436 MotionParameterConvert_MotionActionRoleName BaseMotionParameterRolePerformance::GetRoleActionName()
extern void BaseMotionParameterRolePerformance_GetRoleActionName_mBC771DFEE0FC5C90C0EBDD72ED0DCFD079875B5E (void);
// 0x00000437 System.Void BaseMotionParameterRolePerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void BaseMotionParameterRolePerformance_Init_mF3E211556256E4DCBE51769008979E2BD81F4B0F (void);
// 0x00000438 System.Void BaseMotionParameterRolePerformance::ActionInit()
extern void BaseMotionParameterRolePerformance_ActionInit_m30489F1105F9C63A569E087EE7678321AFC75A04 (void);
// 0x00000439 System.Void BaseMotionParameterRolePerformance::Updata(MotionParameterRoleDataControl,BattleUnit)
extern void BaseMotionParameterRolePerformance_Updata_m4D51A2B20BCCD901A5A177147BF224F3CB2AB413 (void);
// 0x0000043A System.Void BaseMotionParameterRolePerformance::AnotherUpdate(MotionParameterRoleDataControl,BattleUnit)
extern void BaseMotionParameterRolePerformance_AnotherUpdate_m221E477B81558BC918D0396C83D0EBEEADAC23A3 (void);
// 0x0000043B System.Void BaseMotionParameterRolePerformance::.ctor()
extern void BaseMotionParameterRolePerformance__ctor_m71E8A2865A1A80D7F0A742CEDAEC377C53E2A2EC (void);
// 0x0000043C T BaseMotionParameterDataStorehouse`1::GetMotionParameterRoleData(System.Int32)
// 0x0000043D System.Void BaseMotionParameterDataStorehouse`1::PlayInit(MotionParameterDetails,MotionParameterDetailsData)
// 0x0000043E System.Void BaseMotionParameterDataStorehouse`1::Update()
// 0x0000043F System.Void BaseMotionParameterDataStorehouse`1::AutoRelease()
// 0x00000440 System.Void BaseMotionParameterDataStorehouse`1::ReleaseAll()
// 0x00000441 System.Void BaseMotionParameterDataStorehouse`1::.ctor()
// 0x00000442 UnityEngine.Vector3 MotionParameterDataStorehouseControl::GetAmoutMoveVector(System.Int32)
extern void MotionParameterDataStorehouseControl_GetAmoutMoveVector_m9AA11670C659A6B863F8338C3A5EC99128682604 (void);
// 0x00000443 UnityEngine.Vector3 MotionParameterDataStorehouseControl::GetTeleportPosition(System.Int32)
extern void MotionParameterDataStorehouseControl_GetTeleportPosition_m4849C2D0FCCA97E0AA5167958BF045C717CAE773 (void);
// 0x00000444 System.Boolean MotionParameterDataStorehouseControl::GetIsTeleport(System.Int32)
extern void MotionParameterDataStorehouseControl_GetIsTeleport_m9E5C2F9081EC409C69B7FF12022F58E3AEECDBDE (void);
// 0x00000445 System.Void MotionParameterDataStorehouseControl::OffIsTeleport(System.Int32)
extern void MotionParameterDataStorehouseControl_OffIsTeleport_m299E2D97FA425F0FE530875A784B765823A04FF7 (void);
// 0x00000446 System.Boolean MotionParameterDataStorehouseControl::GetIsAnimationChange(System.Int32)
extern void MotionParameterDataStorehouseControl_GetIsAnimationChange_m203592F9736D587B86778B9E5FC8A4CF48DC6C58 (void);
// 0x00000447 System.Void MotionParameterDataStorehouseControl::OffIsAnimation(System.Int32)
extern void MotionParameterDataStorehouseControl_OffIsAnimation_mCF62866249FC63643E57D0FC92CCA4B34FEA3BCD (void);
// 0x00000448 AnimationConvert_AnimationState MotionParameterDataStorehouseControl::GetAnimationState(System.Int32)
extern void MotionParameterDataStorehouseControl_GetAnimationState_mBC4785DADA71AD86CFC4A212D9A54B45FB2405D4 (void);
// 0x00000449 System.Boolean MotionParameterDataStorehouseControl::GetIsHoming(System.Int32)
extern void MotionParameterDataStorehouseControl_GetIsHoming_m05505DD3D3681DB05CBD498092E8FDC25EDD2A05 (void);
// 0x0000044A System.Void MotionParameterDataStorehouseControl::OffIsHoming(System.Int32)
extern void MotionParameterDataStorehouseControl_OffIsHoming_mA721C71BDA7111E8DC31036823FB8350E0DBE414 (void);
// 0x0000044B UnityEngine.Quaternion MotionParameterDataStorehouseControl::GetRotationHoming(System.Int32)
extern void MotionParameterDataStorehouseControl_GetRotationHoming_m554420635048C94AB16F19692E9DC8190FB6C591 (void);
// 0x0000044C UnityEngine.Quaternion MotionParameterDataStorehouseControl::GetLookRotation(System.Int32)
extern void MotionParameterDataStorehouseControl_GetLookRotation_m8CA8EFDFDF9F9744DFD55188FE58339AA4DEF263 (void);
// 0x0000044D System.Boolean MotionParameterDataStorehouseControl::GetIsLookRotation(System.Int32)
extern void MotionParameterDataStorehouseControl_GetIsLookRotation_mF2FF50D983AED8E6FA141473B75C8B59A2043A61 (void);
// 0x0000044E System.Boolean MotionParameterDataStorehouseControl::GetIsCreateCollisionAttack(System.Int32)
extern void MotionParameterDataStorehouseControl_GetIsCreateCollisionAttack_mC085682D8022D610CFCAF804F7325DEE25CEF4E7 (void);
// 0x0000044F System.Void MotionParameterDataStorehouseControl::OffIsCreateCollisionAttack(System.Int32)
extern void MotionParameterDataStorehouseControl_OffIsCreateCollisionAttack_m02C2C4825790901AB0C762BAA2A835C5FF1D0308 (void);
// 0x00000450 System.Int32 MotionParameterDataStorehouseControl::GetCollisionAttackID(System.Int32)
extern void MotionParameterDataStorehouseControl_GetCollisionAttackID_mCAA3888837BEADB620FC18CF784E3A8852D45590 (void);
// 0x00000451 System.Int32 MotionParameterDataStorehouseControl::GetCollisionAttackSkillID(System.Int32)
extern void MotionParameterDataStorehouseControl_GetCollisionAttackSkillID_mF5ED3B785D974F2B098ADCDB48E5DE12866520CC (void);
// 0x00000452 UnityEngine.Vector3 MotionParameterDataStorehouseControl::GetUnitAddPositionAttack(System.Int32)
extern void MotionParameterDataStorehouseControl_GetUnitAddPositionAttack_m36B74EA0BBC7FC7745348CA4712BC9F1DFB8844F (void);
// 0x00000453 UnityEngine.Vector3 MotionParameterDataStorehouseControl::GetUnitAddPositionSensing(System.Int32)
extern void MotionParameterDataStorehouseControl_GetUnitAddPositionSensing_mD3E247F991154532A49F5CCB7EC8C4224990C7AB (void);
// 0x00000454 System.Boolean MotionParameterDataStorehouseControl::GetIsCreateCollisionSensing(System.Int32)
extern void MotionParameterDataStorehouseControl_GetIsCreateCollisionSensing_mA7B97AEDA68EFDC6FD63544E70420315A746A768 (void);
// 0x00000455 System.Void MotionParameterDataStorehouseControl::OffIsCreateCollisionSensing(System.Int32)
extern void MotionParameterDataStorehouseControl_OffIsCreateCollisionSensing_m224B32A382A3EA7468729729EA37375260E5B194 (void);
// 0x00000456 System.Int32 MotionParameterDataStorehouseControl::GetCollisionSensingID(System.Int32)
extern void MotionParameterDataStorehouseControl_GetCollisionSensingID_m4B9652D19C11BBD41449D593DE3B4628AF780AF8 (void);
// 0x00000457 System.Int32 MotionParameterDataStorehouseControl::GetEffectID(System.Int32)
extern void MotionParameterDataStorehouseControl_GetEffectID_mC84D8B4761F3C34D70E4B88C5990EEE9C21D4AC7 (void);
// 0x00000458 System.Single MotionParameterDataStorehouseControl::GetEffectLifeFrame(System.Int32)
extern void MotionParameterDataStorehouseControl_GetEffectLifeFrame_mF51640846F29D82ED5D09D2DB99D3BB854DE01DE (void);
// 0x00000459 UnityEngine.Vector3 MotionParameterDataStorehouseControl::GetEffectPosition(System.Int32)
extern void MotionParameterDataStorehouseControl_GetEffectPosition_mBF6C83B82E332E2112F9B955A9D88D02709D6AE3 (void);
// 0x0000045A System.Int32 MotionParameterDataStorehouseControl::GetBulletID(System.Int32)
extern void MotionParameterDataStorehouseControl_GetBulletID_mABD30CB24FB5FC5747F25FB84CDFA002B4787C72 (void);
// 0x0000045B System.Int32 MotionParameterDataStorehouseControl::GetBulletDetailsID(System.Int32)
extern void MotionParameterDataStorehouseControl_GetBulletDetailsID_mCC372AAB9C1EFD5761BBAD9FCB6139FF417F2CBA (void);
// 0x0000045C UnityEngine.Vector3 MotionParameterDataStorehouseControl::GetBulletPosition(System.Int32)
extern void MotionParameterDataStorehouseControl_GetBulletPosition_m0051C4E707D9A70EA89820B10A4003A998722275 (void);
// 0x0000045D System.Boolean MotionParameterDataStorehouseControl::PlayInit(MotionParameterConvert_MotionActionRoleName,MotionParameterDetails,MotionParameterDetailsData)
extern void MotionParameterDataStorehouseControl_PlayInit_m21192CF8692B2DC179D944E427FAC38B246B105C (void);
// 0x0000045E System.Boolean MotionParameterDataStorehouseControl::MotionParameterDataActive(MotionParameterConvert_MotionActionRoleName,System.Int32)
extern void MotionParameterDataStorehouseControl_MotionParameterDataActive_m569E252147B213BD4441AF1944253C755EB5C3B6 (void);
// 0x0000045F BaseMotionParameterRoleDataControl MotionParameterDataStorehouseControl::GetMotionParameterData(MotionParameterConvert_MotionActionRoleName,System.Int32)
extern void MotionParameterDataStorehouseControl_GetMotionParameterData_m75BB2EC18DA19014C7B1337530D6F88FB77E69F3 (void);
// 0x00000460 System.Void MotionParameterDataStorehouseControl::Update()
extern void MotionParameterDataStorehouseControl_Update_m1859F134DAD8629BCAFEC5596301DAC2502CD6AC (void);
// 0x00000461 System.Void MotionParameterDataStorehouseControl::AutoReleaseUpdate()
extern void MotionParameterDataStorehouseControl_AutoReleaseUpdate_m25EBAB00667A71CCF75ACF767AA8D0FA62970CC8 (void);
// 0x00000462 System.Void MotionParameterDataStorehouseControl::ReleaseAll()
extern void MotionParameterDataStorehouseControl_ReleaseAll_m96186D2A6BDAB11126A61345F9F89AC9DBAD1A47 (void);
// 0x00000463 System.Void MotionParameterDataStorehouseControl::.ctor()
extern void MotionParameterDataStorehouseControl__ctor_m8671E71FA13E10451C01150910B94D0919AD5B85 (void);
// 0x00000464 System.Boolean MotionParameterRoleDataControl::GetMotionParameterDataActive(MotionParameterConvert_MotionActionRoleName,System.Int32)
extern void MotionParameterRoleDataControl_GetMotionParameterDataActive_m90F2A52947E6FF00702096D8AF124250D42ECD9F (void);
// 0x00000465 UnityEngine.Vector3 MotionParameterRoleDataControl::GetAmoutMoveVector(System.Int32)
extern void MotionParameterRoleDataControl_GetAmoutMoveVector_mD69063221FD57B96A9369275F0D1A718FFDFB0F3 (void);
// 0x00000466 UnityEngine.Vector3 MotionParameterRoleDataControl::GetTeleportPosition(System.Int32)
extern void MotionParameterRoleDataControl_GetTeleportPosition_m6F20D3845F833C65513FBB714131A7A65C2AD98A (void);
// 0x00000467 System.Boolean MotionParameterRoleDataControl::GetIsTeleport(System.Int32)
extern void MotionParameterRoleDataControl_GetIsTeleport_mCE76A879AADC000B4421AB16811879F031332BAF (void);
// 0x00000468 System.Void MotionParameterRoleDataControl::OffIsTeleport(System.Int32)
extern void MotionParameterRoleDataControl_OffIsTeleport_mF28D67C804C8D2A6BD6D79C555327A23AE0F33F6 (void);
// 0x00000469 System.Boolean MotionParameterRoleDataControl::GetIsAnimationChange(System.Int32)
extern void MotionParameterRoleDataControl_GetIsAnimationChange_m143B9C32AAD4AE6939361796BCC89AEAB7C47F10 (void);
// 0x0000046A System.Void MotionParameterRoleDataControl::OffIsAnimation(System.Int32)
extern void MotionParameterRoleDataControl_OffIsAnimation_mD0BA36639664C5FD87729903755E1B45D69FE277 (void);
// 0x0000046B AnimationConvert_AnimationState MotionParameterRoleDataControl::GetAnimationState(System.Int32)
extern void MotionParameterRoleDataControl_GetAnimationState_m4D2E94C3FA3FC1C4ED0B5D1D55984EBEB1E533FC (void);
// 0x0000046C System.Boolean MotionParameterRoleDataControl::GetIsHoming(System.Int32)
extern void MotionParameterRoleDataControl_GetIsHoming_m4075315CAF55BF60102D9A648DB864C06FD63071 (void);
// 0x0000046D System.Void MotionParameterRoleDataControl::OffIsHoming(System.Int32)
extern void MotionParameterRoleDataControl_OffIsHoming_m7C8F72958CF0055678E3A295A0B5CC2E2F258392 (void);
// 0x0000046E UnityEngine.Quaternion MotionParameterRoleDataControl::GetRotationHoming(System.Int32)
extern void MotionParameterRoleDataControl_GetRotationHoming_m2D8584926E6F5D0C5D78A50B277968792B4DE017 (void);
// 0x0000046F UnityEngine.Quaternion MotionParameterRoleDataControl::GetLookRotation(System.Int32)
extern void MotionParameterRoleDataControl_GetLookRotation_mD7CFE1BC1928B585890B95EFE1E563395332E531 (void);
// 0x00000470 System.Boolean MotionParameterRoleDataControl::GetIsLookRotation(System.Int32)
extern void MotionParameterRoleDataControl_GetIsLookRotation_mD18D091EDDE0F5032292ECFE83D6F6FE9CD78299 (void);
// 0x00000471 System.Boolean MotionParameterRoleDataControl::GetIsCreateCollisionAttack(System.Int32)
extern void MotionParameterRoleDataControl_GetIsCreateCollisionAttack_m1B22E7AA5880768932FA639E9818F6AAB11F7875 (void);
// 0x00000472 System.Void MotionParameterRoleDataControl::OffIsCreateCollisionAttack(System.Int32)
extern void MotionParameterRoleDataControl_OffIsCreateCollisionAttack_mB03827A59F0D819F1D84360F3E24D4A3F3FDD9F7 (void);
// 0x00000473 System.Int32 MotionParameterRoleDataControl::GetCollisionAttackID(System.Int32)
extern void MotionParameterRoleDataControl_GetCollisionAttackID_mA752A80372CB3D88AC268A7A0AFEBC6187932093 (void);
// 0x00000474 System.Int32 MotionParameterRoleDataControl::GetCollisionAttackSkillID(System.Int32)
extern void MotionParameterRoleDataControl_GetCollisionAttackSkillID_m60CE28615CF5CF8CE2B5449E6EFD5CF8E2031F0D (void);
// 0x00000475 UnityEngine.Vector3 MotionParameterRoleDataControl::GetUnitAddPositionAttack(System.Int32)
extern void MotionParameterRoleDataControl_GetUnitAddPositionAttack_m690525C48A4F2191FD79C2CF66B0D3615C3673DD (void);
// 0x00000476 System.Boolean MotionParameterRoleDataControl::GetIsCreateCollisionSensing(System.Int32)
extern void MotionParameterRoleDataControl_GetIsCreateCollisionSensing_m3A8CF6735C5D461861DA24947A2386FD3E6E95C8 (void);
// 0x00000477 System.Void MotionParameterRoleDataControl::OffIsCreateCollisionSensing(System.Int32)
extern void MotionParameterRoleDataControl_OffIsCreateCollisionSensing_m853A1CD28E3C199675C737471BC48CDF737ECA5C (void);
// 0x00000478 System.Int32 MotionParameterRoleDataControl::GetCollisionSensingID(System.Int32)
extern void MotionParameterRoleDataControl_GetCollisionSensingID_m5ED770352A7778B18A722708A0FF01DC50F844D3 (void);
// 0x00000479 UnityEngine.Vector3 MotionParameterRoleDataControl::GetUnitAddPositionSensing(System.Int32)
extern void MotionParameterRoleDataControl_GetUnitAddPositionSensing_m5710A869F26A0C0CAD2471F17EED47AAF159E3CE (void);
// 0x0000047A System.Int32 MotionParameterRoleDataControl::GetEffectID(System.Int32)
extern void MotionParameterRoleDataControl_GetEffectID_mE3710DB87B45024B2B06F90752F9CD82363EB923 (void);
// 0x0000047B System.Single MotionParameterRoleDataControl::GetEffectLifeFrame(System.Int32)
extern void MotionParameterRoleDataControl_GetEffectLifeFrame_mF325240013B00521B6B7B6E7E2F1C8C4031691F5 (void);
// 0x0000047C UnityEngine.Vector3 MotionParameterRoleDataControl::GetEffectPosition(System.Int32)
extern void MotionParameterRoleDataControl_GetEffectPosition_mCA72C897E196A10C8065F02BE8EA7ED1EE349879 (void);
// 0x0000047D System.Int32 MotionParameterRoleDataControl::GetBulletID(System.Int32)
extern void MotionParameterRoleDataControl_GetBulletID_m019C39A80F262C6BEA1A2CEECBB1CC76843ADF70 (void);
// 0x0000047E System.Int32 MotionParameterRoleDataControl::GetBulletDetailsID(System.Int32)
extern void MotionParameterRoleDataControl_GetBulletDetailsID_m6A5A7011FC59F0C5F037032CC412A4BFBFA3374B (void);
// 0x0000047F UnityEngine.Vector3 MotionParameterRoleDataControl::GetBulletPosition(System.Int32)
extern void MotionParameterRoleDataControl_GetBulletPosition_m1F9004038817ED9B172ED53FA7645F5EF6F61E33 (void);
// 0x00000480 System.Void MotionParameterRoleDataControl::Init()
extern void MotionParameterRoleDataControl_Init_mA494ECAE19067A9208252D6DA619765EAD58F64B (void);
// 0x00000481 System.Void MotionParameterRoleDataControl::ReleaseAll()
extern void MotionParameterRoleDataControl_ReleaseAll_m5D092AA5B0F41B9140FA11D49CEBC47B039AB825 (void);
// 0x00000482 System.Boolean MotionParameterRoleDataControl::PlayInit(MotionParameterDetails,MotionParameterDetailsData)
extern void MotionParameterRoleDataControl_PlayInit_m664CAB8BCBA422137782E21D088767339E3B89BA (void);
// 0x00000483 System.Void MotionParameterRoleDataControl::PlayUpdate()
extern void MotionParameterRoleDataControl_PlayUpdate_mE9DE15B81761713F4E97ABC788788AC4E23F5A7E (void);
// 0x00000484 System.Void MotionParameterRoleDataControl::PlayAutoReleaseUpdate()
extern void MotionParameterRoleDataControl_PlayAutoReleaseUpdate_m4675D4B5AC9E1174D7941FF0E19AFE21AC34D09F (void);
// 0x00000485 System.Void MotionParameterRoleDataControl::.ctor()
extern void MotionParameterRoleDataControl__ctor_m06723152219BE59DEBA09FB184CCEAA647E6CAA1 (void);
// 0x00000486 System.Boolean MotionParameterRoleDataControlManagement::GetIsSetUp()
extern void MotionParameterRoleDataControlManagement_GetIsSetUp_m26FD4E69DF6B7D7E3A9EBD2290AC1BEDADAB6A79 (void);
// 0x00000487 System.Boolean MotionParameterRoleDataControlManagement::GetIsPlay()
extern void MotionParameterRoleDataControlManagement_GetIsPlay_m8FD4549394860DCB1FC5F1319015FC885BF28353 (void);
// 0x00000488 MotionParameterRoleDataControl MotionParameterRoleDataControlManagement::GetMotionParameterRoleDataControl()
extern void MotionParameterRoleDataControlManagement_GetMotionParameterRoleDataControl_m2F9FCE381127473252145CE1887803B6E3716B3D (void);
// 0x00000489 System.Void MotionParameterRoleDataControlManagement::OnStop()
extern void MotionParameterRoleDataControlManagement_OnStop_m5E380117E4A38F73D5B982796322B02BF483E641 (void);
// 0x0000048A System.Void MotionParameterRoleDataControlManagement::OffStop()
extern void MotionParameterRoleDataControlManagement_OffStop_m04EA19BCC1F27B83B46B3F294A5CDD555A4523DA (void);
// 0x0000048B System.Boolean MotionParameterRoleDataControlManagement::GetIsActionEnd()
extern void MotionParameterRoleDataControlManagement_GetIsActionEnd_m307CBA65E6E480E8EE703A076A1499E27CB98405 (void);
// 0x0000048C System.Void MotionParameterRoleDataControlManagement::SetTargetObject(UnityEngine.GameObject)
extern void MotionParameterRoleDataControlManagement_SetTargetObject_m888AE07B2BFE847E868319217E4E2ED1EE88EDE6 (void);
// 0x0000048D System.Void MotionParameterRoleDataControlManagement::Init(UnityEngine.GameObject,System.String,BattleUnit)
extern void MotionParameterRoleDataControlManagement_Init_m087796116B34C9EADD93CCFBD14631AC106ECDD7 (void);
// 0x0000048E System.Collections.Generic.List`1<System.Int32> MotionParameterRoleDataControlManagement::GetEffectIDList()
extern void MotionParameterRoleDataControlManagement_GetEffectIDList_mCE091F5D99C208CC317EBF96D88FC8605D1ADD5F (void);
// 0x0000048F System.Collections.Generic.List`1<System.Int32> MotionParameterRoleDataControlManagement::GetBulletIDList()
extern void MotionParameterRoleDataControlManagement_GetBulletIDList_m0BE270BC13DA295176FCABF16D15C7BBDA73F2E0 (void);
// 0x00000490 System.Void MotionParameterRoleDataControlManagement::ActionInit()
extern void MotionParameterRoleDataControlManagement_ActionInit_m3607F2A8AC5A25688D54DA32D7075C94E8F56592 (void);
// 0x00000491 System.Void MotionParameterRoleDataControlManagement::PlayAction(System.Int32,System.Boolean)
extern void MotionParameterRoleDataControlManagement_PlayAction_m241A7DBA251A67184ABB7B3FB751B41C14D1384A (void);
// 0x00000492 System.Void MotionParameterRoleDataControlManagement::Update(BattleUnit)
extern void MotionParameterRoleDataControlManagement_Update_m0928330AEE4E34A2B93E199AD5DB5164C7F65F00 (void);
// 0x00000493 System.Void MotionParameterRoleDataControlManagement::UpdateMotionParameter(BattleUnit)
extern void MotionParameterRoleDataControlManagement_UpdateMotionParameter_m0B4EB8A6F7B20B02DB04500DC5F98B1BD0D204B9 (void);
// 0x00000494 System.Void MotionParameterRoleDataControlManagement::UpdateMotionParameterRole(BattleUnit)
extern void MotionParameterRoleDataControlManagement_UpdateMotionParameterRole_mD1CCBCBCE3372E5E22EA5018C6624AECAF552998 (void);
// 0x00000495 System.Void MotionParameterRoleDataControlManagement::ActionInitListMotionParameterRole()
extern void MotionParameterRoleDataControlManagement_ActionInitListMotionParameterRole_m594DAD60D79B0B730706A2F97751EBB0D522A3DA (void);
// 0x00000496 System.Void MotionParameterRoleDataControlManagement::UpdateListMotionParameterRole(BattleUnit)
extern void MotionParameterRoleDataControlManagement_UpdateListMotionParameterRole_mF60737C9312BE5975BB21459089604AC38BFEDD0 (void);
// 0x00000497 System.Void MotionParameterRoleDataControlManagement::UpdateKeyFrame()
extern void MotionParameterRoleDataControlManagement_UpdateKeyFrame_m5B62C207B7ED27007D0383F2332198A4474D0C73 (void);
// 0x00000498 System.Void MotionParameterRoleDataControlManagement::Release()
extern void MotionParameterRoleDataControlManagement_Release_m85427AC70CD45B0D3AA7580B93F327B98848945A (void);
// 0x00000499 System.Void MotionParameterRoleDataControlManagement::.ctor()
extern void MotionParameterRoleDataControlManagement__ctor_m6AA891107C11ECEB9AD4FFCAA51B23BE3B5EC8AA (void);
// 0x0000049A System.Void MotionParameterRolePerformanceControl::Init(MotionParameterRoleDataControlManagement,BattleUnit)
extern void MotionParameterRolePerformanceControl_Init_m429EBF53C9D62CACE0935097B575C04637D01EB1 (void);
// 0x0000049B System.Void MotionParameterRolePerformanceControl::PlayInit(MotionParameterDetailsData,MotionParameterRoleDataControlManagement,BattleUnit)
extern void MotionParameterRolePerformanceControl_PlayInit_m3DAAA831012E72FE8C402D36081CF78757D67A65 (void);
// 0x0000049C BaseMotionParameterRolePerformance MotionParameterRolePerformanceControl::CreateMotionParameterPerformance(MotionParameterConvert_MotionActionRoleName)
extern void MotionParameterRolePerformanceControl_CreateMotionParameterPerformance_mC36EFFFDD98EFC5458B79AFBDF6FC6BB631AB762 (void);
// 0x0000049D System.Void MotionParameterRolePerformanceControl::AllInit()
extern void MotionParameterRolePerformanceControl_AllInit_mC987D8D1E55CA5668DDA03B68653704A8E0C94A6 (void);
// 0x0000049E System.Void MotionParameterRolePerformanceControl::Update(MotionParameterRoleDataControlManagement,BattleUnit)
extern void MotionParameterRolePerformanceControl_Update_m8B0191FCD8A94BA3CC44B2E9AB4E8C8A1AE6CB13 (void);
// 0x0000049F System.Void MotionParameterRolePerformanceControl::.ctor()
extern void MotionParameterRolePerformanceControl__ctor_m9C7653DF616E70AB0BDEBE437B25430602A9935B (void);
// 0x000004A0 System.Void MotionParameterRolePerformanceControl_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mE389EF0DFCCD499A0E91A99B68B9C9E50ED3A81F (void);
// 0x000004A1 System.Boolean MotionParameterRolePerformanceControl_<>c__DisplayClass5_0::<Update>b__0(BaseMotionParameterRolePerformance)
extern void U3CU3Ec__DisplayClass5_0_U3CUpdateU3Eb__0_m900AD09B30041486660EC4C87A242C5EC970AC13 (void);
// 0x000004A2 System.Void SkillResult::SetUp(BattleUnit,SkillGameData)
extern void SkillResult_SetUp_m8C9FA215B5124BD96258DC81AC58459B6A931160 (void);
// 0x000004A3 System.Void SkillResult::AddResultOpponentUnit(BattleUnit)
extern void SkillResult_AddResultOpponentUnit_m4CC402A963050E31207BD7978029A58BD18D7F15 (void);
// 0x000004A4 System.Void SkillResult::Calculation()
extern void SkillResult_Calculation_m00F9BBED694667E2466597627F8A5C3F97D7BD98 (void);
// 0x000004A5 System.Void SkillResult::DamageCalulation()
extern void SkillResult_DamageCalulation_mA2E72ED81372598129A9968C264AB56E5A694944 (void);
// 0x000004A6 System.Void SkillResult::.ctor()
extern void SkillResult__ctor_mEC7A1CE39AB21C71952A7C7C84AC4844E1A3268A (void);
// 0x000004A7 System.Boolean SkillCore::GetIsSetUp()
extern void SkillCore_GetIsSetUp_mAA73BC2E2BA959BE4CD6892F27318A050A5EF308 (void);
// 0x000004A8 System.Void SkillCore::Start()
extern void SkillCore_Start_m0F4F3746DCAD38B830371936CAF1D4D026431FF2 (void);
// 0x000004A9 System.Void SkillCore::Update()
extern void SkillCore_Update_mEF63CEA89881AE60D5AE240A74D4A303DE728900 (void);
// 0x000004AA SkillGameData SkillCore::GetSkillGameData(System.Int32)
extern void SkillCore_GetSkillGameData_m55D2F714299AD325E4B26F334E6C25F3BC550463 (void);
// 0x000004AB System.Void SkillCore::.ctor()
extern void SkillCore__ctor_m1D2268B41C6CA28B4CE202E5FB20C47990E4AA1B (void);
// 0x000004AC System.Void SkillDataScriptable::.ctor()
extern void SkillDataScriptable__ctor_mF0A46AB43F2CF459716BB4FC13C1DD4E7AEEE582 (void);
// 0x000004AD System.Int32 SkillExcelData::GetIndex()
extern void SkillExcelData_GetIndex_m0E896228A514C62C0BFBC1B5D6522F2E7D0A75E4 (void);
// 0x000004AE System.Int32 SkillExcelData::GetSkillID()
extern void SkillExcelData_GetSkillID_m90E0FE4E2020177670951B05EDD599A0A3DDCB1B (void);
// 0x000004AF System.String SkillExcelData::GetSkillName()
extern void SkillExcelData_GetSkillName_m15A45A37687E6625DEEF9BD7784613A91618F191 (void);
// 0x000004B0 SkillExcelData_SkillType SkillExcelData::GetSkillType()
extern void SkillExcelData_GetSkillType_m9676536261E6C8C78205C5FFCE3E422ED7FD02B5 (void);
// 0x000004B1 SkillExcelData_SkillBuffTargetType SkillExcelData::GetSkillBuffTargetType()
extern void SkillExcelData_GetSkillBuffTargetType_mEB0EB2C5D157E1977AB93C4040901BD797111238 (void);
// 0x000004B2 SkillExcelData_SkillRangeType SkillExcelData::GetSkillRangeType()
extern void SkillExcelData_GetSkillRangeType_m53E2D7C76961E623855C5F7357A4FFE87E58353B (void);
// 0x000004B3 System.Int32 SkillExcelData::GetSkillPoint()
extern void SkillExcelData_GetSkillPoint_m3FF5B6E2F5AE461A7F1F029ED2854D5CC25F1D0D (void);
// 0x000004B4 System.Single SkillExcelData::GetFloat01()
extern void SkillExcelData_GetFloat01_mB46D2D5681BDCCC7FA864090BD3362971922AE2C (void);
// 0x000004B5 System.String SkillExcelData::GetSkillExplanation()
extern void SkillExcelData_GetSkillExplanation_mC94D3203C2BE001D2D74668F88C754D1A34D7CB5 (void);
// 0x000004B6 System.Void SkillExcelData::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void SkillExcelData_ManualSetUp_m639A2B6C61B944DA1B3A0C8389A7A419EBD8CFDD (void);
// 0x000004B7 System.Void SkillExcelData::.ctor()
extern void SkillExcelData__ctor_m516C65323E754307EE55D0F919B6655D12E3425F (void);
// 0x000004B8 System.Int32 SkillGameData::GetIndex()
extern void SkillGameData_GetIndex_m2FE7432E7DCB509E28868D0B84E783FCF8AE99FF (void);
// 0x000004B9 System.Int32 SkillGameData::GetSkillID()
extern void SkillGameData_GetSkillID_m6214990EB5FF2504C08286ABFA5D544BEDC58DBB (void);
// 0x000004BA System.String SkillGameData::GetSkillName()
extern void SkillGameData_GetSkillName_m5D9F1E5F4559F788A7B7359EABBCB44C43AC404B (void);
// 0x000004BB SkillExcelData_SkillType SkillGameData::GetSkillType()
extern void SkillGameData_GetSkillType_m54EC91FEF8E9FB2E1BEC319EA1519B59A625682E (void);
// 0x000004BC SkillExcelData_SkillBuffTargetType SkillGameData::GetSkillBuffTargetType()
extern void SkillGameData_GetSkillBuffTargetType_mE845CB5E3A68C180B5A240FEEC2BBCDD5C03C024 (void);
// 0x000004BD SkillExcelData_SkillRangeType SkillGameData::GetSkillRangeType()
extern void SkillGameData_GetSkillRangeType_m362C1F55839CAB115B428B4BE5034D826197CD7C (void);
// 0x000004BE System.Int32 SkillGameData::GetSkillPoint()
extern void SkillGameData_GetSkillPoint_mED2C11D0C068073BF10BEE4C2055FF2D51B69856 (void);
// 0x000004BF System.Single SkillGameData::GetFloat01()
extern void SkillGameData_GetFloat01_m3BD95365627B68A706D0F785290B36BC6E5C81AA (void);
// 0x000004C0 System.String SkillGameData::GetSkillExplanation()
extern void SkillGameData_GetSkillExplanation_m70CB9F35985A589768BC7284A1A3C92D56C707D3 (void);
// 0x000004C1 System.Void SkillGameData::SetValue(SkillExcelData)
extern void SkillGameData_SetValue_mD79441B5F28B4B54E97227A74A278C2F511A6986 (void);
// 0x000004C2 System.Void SkillGameData::.ctor()
extern void SkillGameData__ctor_m986D4D9F3EA61C584AC7A867E22BF52409ED9840 (void);
// 0x000004C3 System.Boolean SkillDataStorage::GetIsSetUp()
extern void SkillDataStorage_GetIsSetUp_m29A264A109CA211F19B17CB866AF7F7A671C746C (void);
// 0x000004C4 SkillGameData SkillDataStorage::GetSkillGameData(System.Int32)
extern void SkillDataStorage_GetSkillGameData_m579443DC6BEC5DC0BB0FF0FF9D7609FE668649AC (void);
// 0x000004C5 System.Void SkillDataStorage::Init()
extern void SkillDataStorage_Init_m5097204E999EE8A97E4F1019EAE9F43058BFE6AD (void);
// 0x000004C6 System.Void SkillDataStorage::Update()
extern void SkillDataStorage_Update_m197D558049C96A883D19F6D5839F886C0545C7AC (void);
// 0x000004C7 System.Void SkillDataStorage::SetUp()
extern void SkillDataStorage_SetUp_mA29CF61DB294A2BED2BAA79117CB5156F6C33013 (void);
// 0x000004C8 System.Void SkillDataStorage::.ctor()
extern void SkillDataStorage__ctor_m5EE115FEF954121873910957FA51F12661B668A6 (void);
// 0x000004C9 System.Boolean BaseDataStorage`3::GetIsSetUp()
// 0x000004CA System.String BaseDataStorage`3::GetPath(System.Int32)
// 0x000004CB System.Void BaseDataStorage`3::LoadStart(System.Int32)
// 0x000004CC System.Void BaseDataStorage`3::Update()
// 0x000004CD System.Void BaseDataStorage`3::UpdateLoad()
// 0x000004CE System.Void BaseDataStorage`3::SetForValue(System.Collections.Generic.List`1<T>)
// 0x000004CF System.Void BaseDataStorage`3::SetValue(T)
// 0x000004D0 System.Void BaseDataStorage`3::.ctor()
// 0x000004D1 System.Void UnitAnimation::Init(UnityEngine.GameObject)
extern void UnitAnimation_Init_mC3F7D21695D2998E73B27B8501DA6F6BB17FFE7B (void);
// 0x000004D2 System.Void UnitAnimation::OnStop()
extern void UnitAnimation_OnStop_mCFC7315162F0CADA5E58D280AE4359C90739845D (void);
// 0x000004D3 System.Void UnitAnimation::OnPlay()
extern void UnitAnimation_OnPlay_m92A3A8770001B222375B2694BAC1058846EA2171 (void);
// 0x000004D4 System.Void UnitAnimation::ChangeAnimation(AnimationConvert_AnimationState,System.Boolean)
extern void UnitAnimation_ChangeAnimation_m53E39A0CB1BDB2DB52C3658406BD7522D1188174 (void);
// 0x000004D5 System.Void UnitAnimation::.ctor()
extern void UnitAnimation__ctor_m463FFEFBE7DAD94CEC2FE24277749D3DAEEC144C (void);
// 0x000004D6 System.Int32 BaseUnit::GetUnitID()
extern void BaseUnit_GetUnitID_mB91F3B1776DF88A7149C21FCD382A8ADA33FD1B2 (void);
// 0x000004D7 System.Int32 BaseUnit::GetUnitDetailsID()
extern void BaseUnit_GetUnitDetailsID_m950BE89E4B6FF92E6F43854870B0E34B70446149 (void);
// 0x000004D8 System.Boolean BaseUnit::GetIsSetUp()
extern void BaseUnit_GetIsSetUp_m2F2B8A5EF1916F1AC2F55C1A472FE17C1E0B4789 (void);
// 0x000004D9 System.Void BaseUnit::ChangeAnimation(AnimationConvert_AnimationState,System.Boolean)
extern void BaseUnit_ChangeAnimation_m503B51052984BEC9216D68A59A515DAD483E038B (void);
// 0x000004DA System.Void BaseUnit::InitUnit(System.Int32,System.Int32)
extern void BaseUnit_InitUnit_mC5F5447CCFB14F61B2C050FD8D28CE869A065D1A (void);
// 0x000004DB System.Void BaseUnit::Start()
extern void BaseUnit_Start_mA1B438CD3809BEC0A19632783F936FB96C46470F (void);
// 0x000004DC System.Void BaseUnit::Update()
extern void BaseUnit_Update_m60C293086D8949269871D861C16401B5AD9BA5CC (void);
// 0x000004DD System.Void BaseUnit::.ctor()
extern void BaseUnit__ctor_mBDFE5DA5AF152AFF86D245E01FD4E8F388923296 (void);
// 0x000004DE System.Collections.Generic.List`1<System.Int32> BattleUnit::GetEffectIDList()
extern void BattleUnit_GetEffectIDList_m0CACEF38E72A50827AEB87BDFEE56D9C8A81445F (void);
// 0x000004DF System.Collections.Generic.List`1<System.Int32> BattleUnit::GetBulletIDList()
extern void BattleUnit_GetBulletIDList_m1332A40F7C2E4E969B13F11FA53644A0E6492870 (void);
// 0x000004E0 System.Int32 BattleUnit::GetMaxHP()
extern void BattleUnit_GetMaxHP_mA954A7C4F2EF0CCCD19DD05BD77B62E3091D9632 (void);
// 0x000004E1 System.Int32 BattleUnit::GetMaxMP()
extern void BattleUnit_GetMaxMP_m330E7665391BF719F551D1EAEFFFBC9ECBABE82D (void);
// 0x000004E2 System.Int32 BattleUnit::GetBaseAttack()
extern void BattleUnit_GetBaseAttack_m80FB2271AA6F5947E122C3C7663CBB6D5C2C8C99 (void);
// 0x000004E3 System.Int32 BattleUnit::GetBaseSpeed()
extern void BattleUnit_GetBaseSpeed_m791FA6CC8D67567655DBC7605C64038C6D5A3318 (void);
// 0x000004E4 System.Int32 BattleUnit::GetBaseDefence()
extern void BattleUnit_GetBaseDefence_mE6F0F9D8A8B0ADC4EF1A57B6C981350129D03192 (void);
// 0x000004E5 System.Int32 BattleUnit::GetNowHP()
extern void BattleUnit_GetNowHP_m08A53ADD02DA043907D732E54DDA92A071A8D43F (void);
// 0x000004E6 System.Void BattleUnit::AddHP(System.Int32)
extern void BattleUnit_AddHP_mB443ECC2245929196B54D5D42E4DF32C09F89B11 (void);
// 0x000004E7 System.Int32 BattleUnit::GetNowMP()
extern void BattleUnit_GetNowMP_mA016C04B2BA2276D93426AA45EBAAFDE5C5E4FF1 (void);
// 0x000004E8 System.Void BattleUnit::AddMP(System.Int32)
extern void BattleUnit_AddMP_mBA5B38352C90DDC4B9B4EFC3A48AD8041BEED40A (void);
// 0x000004E9 System.Int32 BattleUnit::GetNowAttack()
extern void BattleUnit_GetNowAttack_m50F6F5ABB0DF19D27A7DD965CF3A4476E44D32FD (void);
// 0x000004EA System.Void BattleUnit::StartAttackBuff(System.Int32,System.Single)
extern void BattleUnit_StartAttackBuff_m5C2D3EE6C07B63C3EEA0E22988BBB4E7B993B1ED (void);
// 0x000004EB System.Void BattleUnit::StartAttackDeBuff(System.Int32,System.Single)
extern void BattleUnit_StartAttackDeBuff_m7EFA079649B8443DD99CDB61877EF4DE01836C89 (void);
// 0x000004EC System.Int32 BattleUnit::GetNowSpeed()
extern void BattleUnit_GetNowSpeed_m5354A86A7571B6A2FEA53AC6AB22BF0B41DE3F44 (void);
// 0x000004ED System.Void BattleUnit::StartSpeedBuff(System.Int32,System.Single)
extern void BattleUnit_StartSpeedBuff_mFA5C8BFD228ED521BF98AE3AB8429ABA9FB36411 (void);
// 0x000004EE System.Void BattleUnit::StartSpeedDeBuff(System.Int32,System.Single)
extern void BattleUnit_StartSpeedDeBuff_mB2D5112E5B1847420AF85041C3F103E697E911F8 (void);
// 0x000004EF System.Int32 BattleUnit::GetNowDefence()
extern void BattleUnit_GetNowDefence_m53F1EE21EA81F87DC212061C32BFA31CE10C9EFC (void);
// 0x000004F0 System.Void BattleUnit::StartDefenceBuff(System.Int32,System.Single)
extern void BattleUnit_StartDefenceBuff_mC44BA4B63C5DC2694E3D484B7CB91AA7E5228E26 (void);
// 0x000004F1 System.Void BattleUnit::StartDefenceDeBuff(System.Int32,System.Single)
extern void BattleUnit_StartDefenceDeBuff_m19827344E032414C77A3F00F470E81C133647E1F (void);
// 0x000004F2 System.Void BattleUnit::Start()
extern void BattleUnit_Start_mDD4EAD79CCD20B11ED169E860F261B3CDF31E0EE (void);
// 0x000004F3 System.Void BattleUnit::InitUnit(System.Int32,System.Int32)
extern void BattleUnit_InitUnit_mFBD89AA3053A3C8703FAFD3C20B6A7436AD293C6 (void);
// 0x000004F4 System.Void BattleUnit::Update()
extern void BattleUnit_Update_mE9E55193434E64B4EA1705C0B258DD794478A794 (void);
// 0x000004F5 System.Boolean BattleUnit::SetUp()
extern void BattleUnit_SetUp_mE389972FE4460824DE7B7C02644DE73348334B4B (void);
// 0x000004F6 System.Void BattleUnit::OnDestroy()
extern void BattleUnit_OnDestroy_m6CE8EB6DE1CFBEFB229B061A056FDDB576D57816 (void);
// 0x000004F7 System.Void BattleUnit::.ctor()
extern void BattleUnit__ctor_m25A425D03AA6DC0E066BDBE5A4CBD7B21C64909C (void);
// 0x000004F8 System.Int32 BattleCharacterStatusData::GetMaxHP()
extern void BattleCharacterStatusData_GetMaxHP_m160BD5D552D7BBD3FC11A04B453747618453291C (void);
// 0x000004F9 System.Int32 BattleCharacterStatusData::GetMaxMP()
extern void BattleCharacterStatusData_GetMaxMP_m310643A9561085C8103D8A997B2ECEBA5A9BCB83 (void);
// 0x000004FA System.Int32 BattleCharacterStatusData::GetBaseAttack()
extern void BattleCharacterStatusData_GetBaseAttack_mCA167B4B1A3A536A946E230B777CFAC7562DC54A (void);
// 0x000004FB System.Int32 BattleCharacterStatusData::GetBaseSpeed()
extern void BattleCharacterStatusData_GetBaseSpeed_mDC97E4E0D664091DBD5AEEF0182BA001A6E4D1E8 (void);
// 0x000004FC System.Int32 BattleCharacterStatusData::GetBaseDefence()
extern void BattleCharacterStatusData_GetBaseDefence_m4C76ABE957A7AA3D535ED2D6B962C712D760C04D (void);
// 0x000004FD System.Int32 BattleCharacterStatusData::GetNowHP()
extern void BattleCharacterStatusData_GetNowHP_m5B6E8EC753397B8A02B0244C97CA03620B8DF310 (void);
// 0x000004FE System.Void BattleCharacterStatusData::AddHP(System.Int32)
extern void BattleCharacterStatusData_AddHP_m5755A93277DA88BD5E0C4E0020592335AEAC7D36 (void);
// 0x000004FF System.Int32 BattleCharacterStatusData::GetNowMP()
extern void BattleCharacterStatusData_GetNowMP_m124413E878FF826A1C5DF981A1FBBA052E41956E (void);
// 0x00000500 System.Void BattleCharacterStatusData::AddMP(System.Int32)
extern void BattleCharacterStatusData_AddMP_mCA11A4B498030834D727504AA1A8EB5E7193B9A2 (void);
// 0x00000501 System.Int32 BattleCharacterStatusData::GetNowAttack()
extern void BattleCharacterStatusData_GetNowAttack_m3FF20643C4DAF374CCFCA174E6DDC39B42880830 (void);
// 0x00000502 System.Int32 BattleCharacterStatusData::GetNowSpeed()
extern void BattleCharacterStatusData_GetNowSpeed_m98C56210118F3EAE555CE1B11A5A26AB6E566E75 (void);
// 0x00000503 System.Int32 BattleCharacterStatusData::GetNowDefence()
extern void BattleCharacterStatusData_GetNowDefence_m0B9A09CDC152ED3B90AE801721C029CCD21AFCDB (void);
// 0x00000504 System.Void BattleCharacterStatusData::StartAttackBuff(System.Int32,System.Single)
extern void BattleCharacterStatusData_StartAttackBuff_m54D865746CC27BCF93F0D048202D0DF74A1A0256 (void);
// 0x00000505 System.Void BattleCharacterStatusData::StartAttackDeBuff(System.Int32,System.Single)
extern void BattleCharacterStatusData_StartAttackDeBuff_mA1757D629EBDE73F382429175853343AB8687B16 (void);
// 0x00000506 System.Void BattleCharacterStatusData::StartSpeedBuff(System.Int32,System.Single)
extern void BattleCharacterStatusData_StartSpeedBuff_m81623B577908025CE75AADC815F239573815EFF9 (void);
// 0x00000507 System.Void BattleCharacterStatusData::StartSpeedDeBuff(System.Int32,System.Single)
extern void BattleCharacterStatusData_StartSpeedDeBuff_m5C7C22E7C6FC4F5FFB7701892B604B24106F8593 (void);
// 0x00000508 System.Void BattleCharacterStatusData::StartDefenceBuff(System.Int32,System.Single)
extern void BattleCharacterStatusData_StartDefenceBuff_m236EBD077D5AD146BA04F754681DF31A322F4EB2 (void);
// 0x00000509 System.Void BattleCharacterStatusData::StartDefenceDeBuff(System.Int32,System.Single)
extern void BattleCharacterStatusData_StartDefenceDeBuff_mE5E31E1F364ACAE702C9B2846EEE79409F5471C9 (void);
// 0x0000050A System.Void BattleCharacterStatusData::InitSetUp(CharacterStatusGameData)
extern void BattleCharacterStatusData_InitSetUp_m759F1A8D142EAE0E1FCDF0DB6BCC6B7021C35408 (void);
// 0x0000050B System.Void BattleCharacterStatusData::Update()
extern void BattleCharacterStatusData_Update_m218D4F4FE0394BFBB0DEDFFED62DC022A3CECA65 (void);
// 0x0000050C System.Void BattleCharacterStatusData::BuffUpdate()
extern void BattleCharacterStatusData_BuffUpdate_mFAF4D3B5B8C6EB8B494458518F0C6FF962E00C18 (void);
// 0x0000050D System.Void BattleCharacterStatusData::.ctor()
extern void BattleCharacterStatusData__ctor_mEE236A6058FE1AA1960F079D0881FC7C9A9ECBC2 (void);
// 0x0000050E System.Int32 BaseBattleCharacterStatusBuffData::GetAddBuffPoint()
extern void BaseBattleCharacterStatusBuffData_GetAddBuffPoint_m8B413E1BE3003B2C3F7461462FC9CD6F9511F97A (void);
// 0x0000050F System.Boolean BaseBattleCharacterStatusBuffData::GetBuffTimeOver()
extern void BaseBattleCharacterStatusBuffData_GetBuffTimeOver_m9A9D3F8C7CF61FDB1CAAD0606E3B6D4B3DD781B0 (void);
// 0x00000510 System.Void BaseBattleCharacterStatusBuffData::Init()
extern void BaseBattleCharacterStatusBuffData_Init_m5BCA264733588125A9704CEBF21FA4DCFE247EBA (void);
// 0x00000511 System.Void BaseBattleCharacterStatusBuffData::StartBuff(System.Int32,System.Single)
extern void BaseBattleCharacterStatusBuffData_StartBuff_m0CCDBE64B9399781AAB466FF662D1E6416022768 (void);
// 0x00000512 System.Void BaseBattleCharacterStatusBuffData::Update()
extern void BaseBattleCharacterStatusBuffData_Update_mA650DDB263D95E1B542C36AA14270D84B426E389 (void);
// 0x00000513 System.Void BaseBattleCharacterStatusBuffData::.ctor()
extern void BaseBattleCharacterStatusBuffData__ctor_mBD9CE8F145D7F90B843384C2F4014D65C6C6F50D (void);
// 0x00000514 System.Int32 BattleCharasterBuffControlData::GetAddBuffPoint()
extern void BattleCharasterBuffControlData_GetAddBuffPoint_mBB4582A38E88A5332FD83621801CC361AD3E60C3 (void);
// 0x00000515 System.Boolean BattleCharasterBuffControlData::GetBuffTimeOver()
extern void BattleCharasterBuffControlData_GetBuffTimeOver_m50C5B15A4E0BD129E6EBC8087188DA1AF029753F (void);
// 0x00000516 System.Int32 BattleCharasterBuffControlData::GetAddDeBuffPoint()
extern void BattleCharasterBuffControlData_GetAddDeBuffPoint_mDFF555D2E1EB11CC02E5BCC820CE603C429B2DDB (void);
// 0x00000517 System.Boolean BattleCharasterBuffControlData::GetDeBuffTimeOver()
extern void BattleCharasterBuffControlData_GetDeBuffTimeOver_m3619E091E3C89736F6A70165DB3E568CE7A0F9D8 (void);
// 0x00000518 System.Void BattleCharasterBuffControlData::BuffInit()
extern void BattleCharasterBuffControlData_BuffInit_m36D7794B73BD4E2D46687ADB35C70F617BD0641C (void);
// 0x00000519 System.Void BattleCharasterBuffControlData::DeBuffInit()
extern void BattleCharasterBuffControlData_DeBuffInit_m775C17E7612DF5A7843BED9A24E0DE986346B458 (void);
// 0x0000051A System.Void BattleCharasterBuffControlData::AllBuffInit()
extern void BattleCharasterBuffControlData_AllBuffInit_m7153D46BB2DE419BDCA2345957C4FC644B3531F3 (void);
// 0x0000051B System.Void BattleCharasterBuffControlData::StartBuff(System.Int32,System.Single)
extern void BattleCharasterBuffControlData_StartBuff_m0C9468B58641FBB8686D8DCC3F4CCAAE17D36060 (void);
// 0x0000051C System.Void BattleCharasterBuffControlData::StartDeBuff(System.Int32,System.Single)
extern void BattleCharasterBuffControlData_StartDeBuff_mC01E7EC0C736F9014185DC3F3CEBA6B5AEA438D2 (void);
// 0x0000051D System.Void BattleCharasterBuffControlData::Update()
extern void BattleCharasterBuffControlData_Update_m79835A0EEC3CBD0471B252DC37FC4C1BDB036438 (void);
// 0x0000051E System.Void BattleCharasterBuffControlData::.ctor()
extern void BattleCharasterBuffControlData__ctor_mBC87F1189EC39F246DC20F600E48E5C71DF4768F (void);
// 0x0000051F System.Collections.Generic.List`1<System.Int32> UnitCore::GetLoadUnitIDList()
extern void UnitCore_GetLoadUnitIDList_mAAE25F0EA0C1326DCE1A867BD53EC65A5F4CA916 (void);
// 0x00000520 System.Boolean UnitCore::GetIsSetUp()
extern void UnitCore_GetIsSetUp_m2C6F5B617BBE81EDD1B38AD8C2157D2B75812D11 (void);
// 0x00000521 System.Void UnitCore::Start()
extern void UnitCore_Start_mB1A62BE7DEB34FF9A9B889671EB2F0619CC5D2F9 (void);
// 0x00000522 System.Void UnitCore::Update()
extern void UnitCore_Update_mAC49C509E3DF7BA3C81533242456AF118F42530C (void);
// 0x00000523 System.Void UnitCore::CreateUnit(System.Int32)
extern void UnitCore_CreateUnit_mD182DCF3CE3423EA387E2F5BDC2204F0F4708605 (void);
// 0x00000524 UnityEngine.GameObject UnitCore::GetUnitGameObject(System.Int32)
extern void UnitCore_GetUnitGameObject_mB458A8F91C52B5A559E2F9C3B64E42295C218225 (void);
// 0x00000525 System.Void UnitCore::OnDestroy()
extern void UnitCore_OnDestroy_m52C3F34F67E77C155B595CA2085CCE073B86A281 (void);
// 0x00000526 System.Void UnitCore::.ctor()
extern void UnitCore__ctor_mE327D1F81384B8DF080950E36B3EEE1B7A56B370 (void);
// 0x00000527 UnityEngine.GameObject UnitManager::GetUnitGameObject(System.Int32)
extern void UnitManager_GetUnitGameObject_m0839C79A09A92561499A9744501CBC7BDC9DDA5F (void);
// 0x00000528 System.Collections.Generic.List`1<System.Int32> UnitManager::GetLoadUnitIDList()
extern void UnitManager_GetLoadUnitIDList_m7B57ED951D1D6E88F5E0FC47567C0E8B712FDC63 (void);
// 0x00000529 System.Boolean UnitManager::GetIsSetUpLoad()
extern void UnitManager_GetIsSetUpLoad_m3A3AEB481BF9D2D744E8A31FA8F774AA8D9CDCA1 (void);
// 0x0000052A System.Boolean UnitManager::GetIsSetUpUnit()
extern void UnitManager_GetIsSetUpUnit_m56C00F8672AB0670EE33B9AFC13AD4174DFBA657 (void);
// 0x0000052B System.Boolean UnitManager::GetIsSetUp()
extern void UnitManager_GetIsSetUp_mC33319257B08F0219E41C0853A8C6FBEEB9FCEDE (void);
// 0x0000052C System.Void UnitManager::CreateUnit(System.Int32)
extern void UnitManager_CreateUnit_m9AE62AF56791F03521738ADB7F38B3FBA86D835A (void);
// 0x0000052D System.Void UnitManager::Update()
extern void UnitManager_Update_m8B5212541BE1DA2D668C457E17CFA039DF49527E (void);
// 0x0000052E System.Void UnitManager::LoadUpdate()
extern void UnitManager_LoadUpdate_m7F0C5CA6E3CEDC81B241C00813E30C558D89DA4A (void);
// 0x0000052F System.Void UnitManager::SetUpBattleUnit()
extern void UnitManager_SetUpBattleUnit_m6A3265009F97FA913907B9344630F44B67E07050 (void);
// 0x00000530 System.Void UnitManager::ReleaseUnitAll()
extern void UnitManager_ReleaseUnitAll_mE3E6C0D5FF3C9F6D58955A8D3942378202A02B05 (void);
// 0x00000531 System.Void UnitManager::.ctor()
extern void UnitManager__ctor_mFBA75CB247D818525EAC92DE6B65CC429F368026 (void);
// 0x00000532 UnityEngine.Playables.Playable TestPlayAssets::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject)
extern void TestPlayAssets_CreatePlayable_m6F6EA858CD5B5DFD408576CD3F6F645C56A81F34 (void);
// 0x00000533 System.Void TestPlayAssets::.ctor()
extern void TestPlayAssets__ctor_mA87AF921086D2ABE3F8C34114D70C4C7F95C7756 (void);
// 0x00000534 System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x00000535 System.Void Readme_Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x00000536 System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C (void);
// 0x00000537 UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A (void);
// 0x00000538 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A (void);
// 0x00000539 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87 (void);
// 0x0000053A System.Void UnityTemplateProjects.SimpleCameraController_CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647 (void);
// 0x0000053B System.Void UnityTemplateProjects.SimpleCameraController_CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96 (void);
// 0x0000053C System.Void UnityTemplateProjects.SimpleCameraController_CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController_CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326 (void);
// 0x0000053D System.Void UnityTemplateProjects.SimpleCameraController_CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410 (void);
// 0x0000053E System.Void UnityTemplateProjects.SimpleCameraController_CameraState::.ctor()
extern void CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D (void);
// 0x0000053F AnimationConvert_AnimationState MotionActionRole.AnimationChangeData::GetAnimationState()
extern void AnimationChangeData_GetAnimationState_m631544C80C2DC0D5CF5C27BFFDFA757D4EEC92F7 (void);
// 0x00000540 System.Boolean MotionActionRole.AnimationChangeData::GetIsAnimationChange()
extern void AnimationChangeData_GetIsAnimationChange_m585CD2F8A65D491F14E3227319F4639337CE0007 (void);
// 0x00000541 System.Void MotionActionRole.AnimationChangeData::OffIsAnimation()
extern void AnimationChangeData_OffIsAnimation_mC7DCB9541A7F85F04C81BD28CF21D6D6BF681B8E (void);
// 0x00000542 System.Void MotionActionRole.AnimationChangeData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void AnimationChangeData_Init_m8040C63D92D22CA9B755B0885988E1B77DC35AF7 (void);
// 0x00000543 System.Void MotionActionRole.AnimationChangeData::ActionInit()
extern void AnimationChangeData_ActionInit_m8FC0807148E67A9DF513FEA3E315B4CC59051E8C (void);
// 0x00000544 System.Void MotionActionRole.AnimationChangeData::Updata()
extern void AnimationChangeData_Updata_m4ADA82259119D70E45F8773D6FDFFC809F6FACEF (void);
// 0x00000545 System.Void MotionActionRole.AnimationChangeData::.ctor()
extern void AnimationChangeData__ctor_m218901DBBE91289CE7EB5CADB5A6C10C1BE81000 (void);
// 0x00000546 System.Void MotionActionRole.AnimationChangePerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void AnimationChangePerformance_Init_m390490A7D76BDE2F8B5D0A3147ACD42BC1FF5234 (void);
// 0x00000547 System.Void MotionActionRole.AnimationChangePerformance::ActionInit()
extern void AnimationChangePerformance_ActionInit_m14D8DC80CE648B60721283EF9BBCDD2D340D8743 (void);
// 0x00000548 System.Void MotionActionRole.AnimationChangePerformance::Updata(MotionParameterRoleDataControl,BattleUnit)
extern void AnimationChangePerformance_Updata_m7051F3CBDBA8CE692EA09F05B5ED539DC9AD3DA5 (void);
// 0x00000549 System.Void MotionActionRole.AnimationChangePerformance::AnotherUpdate(MotionParameterRoleDataControl,BattleUnit)
extern void AnimationChangePerformance_AnotherUpdate_m9D6801A59607157462028EB136635B4E9DD5373C (void);
// 0x0000054A System.Void MotionActionRole.AnimationChangePerformance::.ctor()
extern void AnimationChangePerformance__ctor_mAF8E3A7C6BFAF315BA538DC6FD2C940F6ADF1C50 (void);
// 0x0000054B System.Boolean MotionActionRole.BaseCollisionData::GetIsCreateCollision()
extern void BaseCollisionData_GetIsCreateCollision_m21D22FA182D0C6A248F8E8E3EF426CB100995B8D (void);
// 0x0000054C System.Void MotionActionRole.BaseCollisionData::OffIsCreateCollision()
extern void BaseCollisionData_OffIsCreateCollision_mC068C4CB0BE41ADFD31AE13143E6F66CAD5DE89C (void);
// 0x0000054D System.Int32 MotionActionRole.BaseCollisionData::GetCollisionID()
extern void BaseCollisionData_GetCollisionID_m523C34B5A1802542EA3F4313A070CBA44E73FF65 (void);
// 0x0000054E UnityEngine.Vector3 MotionActionRole.BaseCollisionData::GetUnitAddPosition()
extern void BaseCollisionData_GetUnitAddPosition_m0549769B99553ABA59AAAB627F5E7FA59C69CEF9 (void);
// 0x0000054F System.Void MotionActionRole.BaseCollisionData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void BaseCollisionData_Init_m66828F601B62967856794B7BA2637E240C377E3F (void);
// 0x00000550 System.Void MotionActionRole.BaseCollisionData::ActionInit()
extern void BaseCollisionData_ActionInit_mF4886EDFAD91C3E2B6299815E001E36093E69C4D (void);
// 0x00000551 System.Void MotionActionRole.BaseCollisionData::.ctor()
extern void BaseCollisionData__ctor_m6F7456AF1E01ECB7B772B9C36C469BEB174ED830 (void);
// 0x00000552 System.Void MotionActionRole.BaseCollisionPerformance::.ctor()
extern void BaseCollisionPerformance__ctor_mAE79AD3842D4ECB050FE0C8B6293FA0AA4B285FE (void);
// 0x00000553 System.Int32 MotionActionRole.BulletGenerateData::GetBulletID()
extern void BulletGenerateData_GetBulletID_m57099A022968AAFF01C563DF9BED5D7453D95FF7 (void);
// 0x00000554 System.Int32 MotionActionRole.BulletGenerateData::GetBulletDetailsID()
extern void BulletGenerateData_GetBulletDetailsID_mB520050BD140EC73C88E1DDD0E83E50DD7064447 (void);
// 0x00000555 UnityEngine.Vector3 MotionActionRole.BulletGenerateData::GetBulletPosition()
extern void BulletGenerateData_GetBulletPosition_m7400A3AEDE227E84E46B4D2B40279742333AB38E (void);
// 0x00000556 System.Void MotionActionRole.BulletGenerateData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void BulletGenerateData_Init_m398C5B7CFBC19B8E9088748B8AAF707468F74C8F (void);
// 0x00000557 System.Void MotionActionRole.BulletGenerateData::ActionInit()
extern void BulletGenerateData_ActionInit_mEA08598CC9E9AAA23A8B3BAFDCADD34875033926 (void);
// 0x00000558 System.Void MotionActionRole.BulletGenerateData::Updata()
extern void BulletGenerateData_Updata_mDD357712EB7CA2B8138AB81DB51C2E7DBEB61FFE (void);
// 0x00000559 System.Void MotionActionRole.BulletGenerateData::.ctor()
extern void BulletGenerateData__ctor_m1A12BF40D4C0018B9386F191BE9DD8079A016146 (void);
// 0x0000055A System.Void MotionActionRole.BulletGeneratePerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void BulletGeneratePerformance_Init_m6BA4E0B9479CF2B716A5C038DFA5BFDEA0BF36ED (void);
// 0x0000055B System.Void MotionActionRole.BulletGeneratePerformance::.ctor()
extern void BulletGeneratePerformance__ctor_mB349AF62C250D620BA25A2230DBE1168F28A680E (void);
// 0x0000055C System.Int32 MotionActionRole.CollisionAttackData::GetAttackCollisionSkillID()
extern void CollisionAttackData_GetAttackCollisionSkillID_m0279BBBCA3C03DBF642F8AED64AF7CBBBD5B54E6 (void);
// 0x0000055D System.Void MotionActionRole.CollisionAttackData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void CollisionAttackData_Init_mDBEA03DB8A6F1FC195E097506D3D1A21CBF10732 (void);
// 0x0000055E System.Void MotionActionRole.CollisionAttackData::ActionInit()
extern void CollisionAttackData_ActionInit_mCD32080B8A77AB76D490B25E15F9A8FBAD23C84E (void);
// 0x0000055F System.Void MotionActionRole.CollisionAttackData::Updata()
extern void CollisionAttackData_Updata_m03D72A6DE9BFCB2AE7F5069DDBF33739979E5AE7 (void);
// 0x00000560 System.Void MotionActionRole.CollisionAttackData::.ctor()
extern void CollisionAttackData__ctor_m5D0BB48CC041177EC87ECF31060CB49EFC5F5C82 (void);
// 0x00000561 System.Void MotionActionRole.CollisionAttackPerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void CollisionAttackPerformance_Init_m845969B6F2BA8BD51FB6F45B37E693709EA26147 (void);
// 0x00000562 System.Void MotionActionRole.CollisionAttackPerformance::Updata(MotionParameterRoleDataControl,BattleUnit)
extern void CollisionAttackPerformance_Updata_mC34BA1074E3ACB8E856AD27191E681A80E5C0F69 (void);
// 0x00000563 System.Void MotionActionRole.CollisionAttackPerformance::.ctor()
extern void CollisionAttackPerformance__ctor_m23C7B764F85277646DC484050AA5F379616EB73D (void);
// 0x00000564 System.Void MotionActionRole.CollisionSensingData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void CollisionSensingData_Init_m7A7E88B3F62F96532131CF9B3F1B2E49C4E32E6E (void);
// 0x00000565 System.Void MotionActionRole.CollisionSensingData::ActionInit()
extern void CollisionSensingData_ActionInit_mD42ABB3C0A7C2F9D223AE305E57F244A7112425F (void);
// 0x00000566 System.Void MotionActionRole.CollisionSensingData::Updata()
extern void CollisionSensingData_Updata_mF39226409050DCD3D35092CD1B6E3B928200A13A (void);
// 0x00000567 System.Void MotionActionRole.CollisionSensingData::.ctor()
extern void CollisionSensingData__ctor_mF402733ABFEDCFDDC3A467DE8F34CB9B8A5B9525 (void);
// 0x00000568 System.Void MotionActionRole.CollisionSensingPerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void CollisionSensingPerformance_Init_m5C98CC333FC74D45E2B70F34286FEE7C61D4FF52 (void);
// 0x00000569 System.Void MotionActionRole.CollisionSensingPerformance::.ctor()
extern void CollisionSensingPerformance__ctor_m472E6D69379EFA458F18639F38E47051E6C8C543 (void);
// 0x0000056A System.Int32 MotionActionRole.EffectGenerateData::GetEffectID()
extern void EffectGenerateData_GetEffectID_mC0100FDE6363FFD5215F341C52FFDEA22B485E44 (void);
// 0x0000056B System.Single MotionActionRole.EffectGenerateData::GetEffectLifeFrame()
extern void EffectGenerateData_GetEffectLifeFrame_m6D7E6055940F2129FF97D6F1A0568AE38DD49DC0 (void);
// 0x0000056C UnityEngine.Vector3 MotionActionRole.EffectGenerateData::GetEffectPosition()
extern void EffectGenerateData_GetEffectPosition_m946E1662C063DCBF83E80F388E9BC5C52EFF71E0 (void);
// 0x0000056D System.Void MotionActionRole.EffectGenerateData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void EffectGenerateData_Init_mB471363848B1A2F268F64A8F1C400E2D990945CC (void);
// 0x0000056E System.Void MotionActionRole.EffectGenerateData::ActionInit()
extern void EffectGenerateData_ActionInit_m186BC161598998ED1DFCC103AEF0CA014D82D61C (void);
// 0x0000056F System.Void MotionActionRole.EffectGenerateData::Updata()
extern void EffectGenerateData_Updata_m7ACEB0BD2C31C2DFFC8498530902362E5B5A1614 (void);
// 0x00000570 System.Void MotionActionRole.EffectGenerateData::.ctor()
extern void EffectGenerateData__ctor_m3706DF7E00732756A54B45F988286991092A15AB (void);
// 0x00000571 System.Void MotionActionRole.EffectGeneratePerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void EffectGeneratePerformance_Init_mD538ACB96BA9D3EF41E6F11A4C513D85B025D8DC (void);
// 0x00000572 System.Void MotionActionRole.EffectGeneratePerformance::.ctor()
extern void EffectGeneratePerformance__ctor_mC5CD1292DA4050C577F0F6BCB47BE09963E00545 (void);
// 0x00000573 UnityEngine.Vector3 MotionActionRole.TransformPositionData::GetAmoutoMoveVector()
extern void TransformPositionData_GetAmoutoMoveVector_m32C2A56F47249413700688527A30D9A04AFF1A8E (void);
// 0x00000574 System.Void MotionActionRole.TransformPositionData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void TransformPositionData_Init_m002CBB933589193FBB27F306E5986F29E663723E (void);
// 0x00000575 System.Void MotionActionRole.TransformPositionData::ActionInit()
extern void TransformPositionData_ActionInit_m9005F9C2810315228F280C1D0BFB26B966427ADC (void);
// 0x00000576 System.Void MotionActionRole.TransformPositionData::Updata()
extern void TransformPositionData_Updata_m16678733FA1C01AC2F46D7C41C1AFA78680957ED (void);
// 0x00000577 System.Void MotionActionRole.TransformPositionData::.ctor()
extern void TransformPositionData__ctor_m96E00C8880CFF64EB951E8BA6E6797F6E791B310 (void);
// 0x00000578 System.Void MotionActionRole.TransformPositionPerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void TransformPositionPerformance_Init_m0F6455FF2BF9261A36EB68F062399CE4977D0D19 (void);
// 0x00000579 System.Void MotionActionRole.TransformPositionPerformance::ActionInit()
extern void TransformPositionPerformance_ActionInit_m49A6B57A90E49D588270325C9C6157FCEC46254F (void);
// 0x0000057A System.Void MotionActionRole.TransformPositionPerformance::Updata(MotionParameterRoleDataControl,BattleUnit)
extern void TransformPositionPerformance_Updata_mE2ACF7D4C555F1E2F3E554FA1D52AA4CC5AFF156 (void);
// 0x0000057B System.Void MotionActionRole.TransformPositionPerformance::AnotherUpdate(MotionParameterRoleDataControl,BattleUnit)
extern void TransformPositionPerformance_AnotherUpdate_m1F9FC2A9F7A5C54829BDA29568D1C993CFB8D24F (void);
// 0x0000057C System.Void MotionActionRole.TransformPositionPerformance::.ctor()
extern void TransformPositionPerformance__ctor_mB7483B87DD2320E97A4A27ABD93BCFC807A64699 (void);
// 0x0000057D UnityEngine.Quaternion MotionActionRole.TransformTargetHomingData::GetRotationHoming()
extern void TransformTargetHomingData_GetRotationHoming_mDC0D3B128A2E743A3A37288735DEB1E899583E62 (void);
// 0x0000057E System.Boolean MotionActionRole.TransformTargetHomingData::GetIsHoming()
extern void TransformTargetHomingData_GetIsHoming_m74245812DD36DBB2DDF5299EF972A687720D5044 (void);
// 0x0000057F System.Void MotionActionRole.TransformTargetHomingData::OffIsHoming()
extern void TransformTargetHomingData_OffIsHoming_mE4E6F8362AB6CFAED177BEF5D490BA1A6A495804 (void);
// 0x00000580 System.Void MotionActionRole.TransformTargetHomingData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void TransformTargetHomingData_Init_m170D5DDC93E26F8C78D39E7D38600C2AEDD17841 (void);
// 0x00000581 System.Void MotionActionRole.TransformTargetHomingData::ActionInit()
extern void TransformTargetHomingData_ActionInit_m5B67DC6309695DFBCD923CE89A41CD53CA88BD3C (void);
// 0x00000582 System.Void MotionActionRole.TransformTargetHomingData::Updata()
extern void TransformTargetHomingData_Updata_m85C1F24188E5B149965BBE7CDE0175883AC9ACCB (void);
// 0x00000583 System.Void MotionActionRole.TransformTargetHomingData::.ctor()
extern void TransformTargetHomingData__ctor_m7FC6BF7FCB3B7922E55F38AA9C54CEB2C75E5E84 (void);
// 0x00000584 System.Void MotionActionRole.TransformTargetHomingPerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void TransformTargetHomingPerformance_Init_m9F5476569CE725456E72F45CA75EA5E03B0954A4 (void);
// 0x00000585 System.Void MotionActionRole.TransformTargetHomingPerformance::ActionInit()
extern void TransformTargetHomingPerformance_ActionInit_mCFC2D0D6525FC5D06C72A7A585F1F5209CBE1BAB (void);
// 0x00000586 System.Void MotionActionRole.TransformTargetHomingPerformance::Updata(MotionParameterRoleDataControl,BattleUnit)
extern void TransformTargetHomingPerformance_Updata_m37090C0BCAAFDA4EC162CAABB97DC0ABC0B3932E (void);
// 0x00000587 System.Void MotionActionRole.TransformTargetHomingPerformance::AnotherUpdate(MotionParameterRoleDataControl,BattleUnit)
extern void TransformTargetHomingPerformance_AnotherUpdate_m699C0ED0DFEC7508A7C97919944C00886A8D1170 (void);
// 0x00000588 System.Void MotionActionRole.TransformTargetHomingPerformance::.ctor()
extern void TransformTargetHomingPerformance__ctor_m8BD2056D2D2BCB3E314B16F8FF20676866B494F2 (void);
// 0x00000589 UnityEngine.Quaternion MotionActionRole.TransformTargetLookRotationData::GetLookRotation()
extern void TransformTargetLookRotationData_GetLookRotation_m3B203D92FA7A20F4B36AE05C3968D0F3A37F04E0 (void);
// 0x0000058A System.Boolean MotionActionRole.TransformTargetLookRotationData::GetIsLookRotation()
extern void TransformTargetLookRotationData_GetIsLookRotation_m52D65F00419DD22FB39B9B75FC48A94E81E3C949 (void);
// 0x0000058B System.Void MotionActionRole.TransformTargetLookRotationData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void TransformTargetLookRotationData_Init_mEEF517A44AB04A7D2BE3F6A979D61C72A6E74CAA (void);
// 0x0000058C System.Void MotionActionRole.TransformTargetLookRotationData::ActionInit()
extern void TransformTargetLookRotationData_ActionInit_m00BBECE0F0311641F7AA25E23CF86E4CB136F0A0 (void);
// 0x0000058D System.Void MotionActionRole.TransformTargetLookRotationData::Updata()
extern void TransformTargetLookRotationData_Updata_m7F16E13FCA657E9D23FD6521AAF0E6367EDA45A7 (void);
// 0x0000058E System.Void MotionActionRole.TransformTargetLookRotationData::.ctor()
extern void TransformTargetLookRotationData__ctor_m8810E0434CB5A2E35EFB480DEC32002F68CCFE26 (void);
// 0x0000058F System.Void MotionActionRole.TransformTargetLookRotationPerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void TransformTargetLookRotationPerformance_Init_mE04AAF20BDF4CF1CB4B92B9CAA75AC443E1847A6 (void);
// 0x00000590 System.Void MotionActionRole.TransformTargetLookRotationPerformance::ActionInit()
extern void TransformTargetLookRotationPerformance_ActionInit_mF37D10995A0BF4CDC57C3B4FCD1ED76569BD88CB (void);
// 0x00000591 System.Void MotionActionRole.TransformTargetLookRotationPerformance::Updata(MotionParameterRoleDataControl,BattleUnit)
extern void TransformTargetLookRotationPerformance_Updata_mB3A7F14BA2131E3779AA7DAA931C88451A22886C (void);
// 0x00000592 System.Void MotionActionRole.TransformTargetLookRotationPerformance::AnotherUpdate(MotionParameterRoleDataControl,BattleUnit)
extern void TransformTargetLookRotationPerformance_AnotherUpdate_m868FB22196F3E1629B63A58CB1C32E7F7AA44307 (void);
// 0x00000593 System.Void MotionActionRole.TransformTargetLookRotationPerformance::.ctor()
extern void TransformTargetLookRotationPerformance__ctor_m2CB8ECD39A8C545F01B8A94E835B751833A4AC42 (void);
// 0x00000594 UnityEngine.Vector3 MotionActionRole.TransformTargetTeleportPositionData::GetTeleportPosition()
extern void TransformTargetTeleportPositionData_GetTeleportPosition_m51EC513D0FDD03B53789270DB2A81FEF0A27E9AE (void);
// 0x00000595 System.Boolean MotionActionRole.TransformTargetTeleportPositionData::GetIsTeleport()
extern void TransformTargetTeleportPositionData_GetIsTeleport_m49C6431C5484770CB823B2F529F31C1816E89D6B (void);
// 0x00000596 System.Void MotionActionRole.TransformTargetTeleportPositionData::OffIsTeleport()
extern void TransformTargetTeleportPositionData_OffIsTeleport_m69D7CE001C08F560000FAD1220146992370AB50E (void);
// 0x00000597 System.Void MotionActionRole.TransformTargetTeleportPositionData::Init(MotionParameterDetails,MotionParameterDetailsData)
extern void TransformTargetTeleportPositionData_Init_m2AD2017C44B688CDB49B70CCCCDE0F79A6000505 (void);
// 0x00000598 System.Void MotionActionRole.TransformTargetTeleportPositionData::ActionInit()
extern void TransformTargetTeleportPositionData_ActionInit_mC3979AB25C6C481347AD556A14FAA010B581E23B (void);
// 0x00000599 System.Void MotionActionRole.TransformTargetTeleportPositionData::Updata()
extern void TransformTargetTeleportPositionData_Updata_mBE31E5F0ED44E8CEBA1FF4419B6430F0A9C2A27F (void);
// 0x0000059A System.Void MotionActionRole.TransformTargetTeleportPositionData::.ctor()
extern void TransformTargetTeleportPositionData__ctor_mF052F8834636EA4CC592B9F9187613DFF68D4EFC (void);
// 0x0000059B System.Void MotionActionRole.TransformTargetTeleportPositionPerformance::Init(System.Int32,MotionParameterRoleDataControl,BattleUnit)
extern void TransformTargetTeleportPositionPerformance_Init_m707FE198C4E0E90A2A8B2ACCC381AB84C40DEBE1 (void);
// 0x0000059C System.Void MotionActionRole.TransformTargetTeleportPositionPerformance::ActionInit()
extern void TransformTargetTeleportPositionPerformance_ActionInit_m1C64521A91E6EF5FFEC36502FE8DBA9B9EF70E2F (void);
// 0x0000059D System.Void MotionActionRole.TransformTargetTeleportPositionPerformance::Updata(MotionParameterRoleDataControl,BattleUnit)
extern void TransformTargetTeleportPositionPerformance_Updata_m2A756744840B93E90D0CE5D00FDAAB45DA4DB46A (void);
// 0x0000059E System.Void MotionActionRole.TransformTargetTeleportPositionPerformance::AnotherUpdate(MotionParameterRoleDataControl,BattleUnit)
extern void TransformTargetTeleportPositionPerformance_AnotherUpdate_mFEA83956F375242D96D15CBDA8E40129E76BED1B (void);
// 0x0000059F System.Void MotionActionRole.TransformTargetTeleportPositionPerformance::.ctor()
extern void TransformTargetTeleportPositionPerformance__ctor_mDF922F9FF8EBB639474E0D2D574C39000C3AF18F (void);
// 0x000005A0 System.Void ProjectSystem.ExcelBinarySystem`1::SaveBinary(System.Collections.Generic.List`1<T>,System.String,System.String)
// 0x000005A1 System.Collections.Generic.List`1<T> ProjectSystem.ExcelBinarySystem`1::LoadBinary(System.String,System.String)
// 0x000005A2 System.Void ProjectSystem.ExcelBinarySystem`1::.ctor()
// 0x000005A3 System.Void ProjectSystem.ExcelBinarySystem`1_ExcelFileBinary::SetUp(System.String,System.String,System.IO.FileMode)
// 0x000005A4 System.Void ProjectSystem.ExcelBinarySystem`1_ExcelFileBinary::SaveList(System.Collections.Generic.List`1<T>)
// 0x000005A5 System.Collections.Generic.List`1<T> ProjectSystem.ExcelBinarySystem`1_ExcelFileBinary::LoadList()
// 0x000005A6 T ProjectSystem.ExcelBinarySystem`1_ExcelFileBinary::LoadData()
// 0x000005A7 System.Void ProjectSystem.ExcelBinarySystem`1_ExcelFileBinary::SaveData(T)
// 0x000005A8 System.Void ProjectSystem.ExcelBinarySystem`1_ExcelFileBinary::Close()
// 0x000005A9 System.Void ProjectSystem.ExcelBinarySystem`1_ExcelFileBinary::.ctor()
// 0x000005AA ProjectSystem.ExcelSystem_DataControl ProjectSystem.ExcelSystem::GetExcelLoadData(System.String,System.String)
extern void ExcelSystem_GetExcelLoadData_mFA9AD63AABC6EB67194CC4623B63FCC923AEFC79 (void);
// 0x000005AB System.Void ProjectSystem.ExcelSystem::GetExcelLoadExportData(System.String,System.String,BaseExcelScriptableObject`1<T>)
// 0x000005AC ProjectSystem.ExcelSystem_DataControl ProjectSystem.ExcelSystem::CreateData(NPOI.SS.UserModel.ISheet)
extern void ExcelSystem_CreateData_m9E7B82AAF2D5A127222782CA9C763BEC37639DFB (void);
// 0x000005AD System.Void ProjectSystem.ExcelSystem::CreateExportData(NPOI.SS.UserModel.ISheet,BaseExcelScriptableObject`1<T>)
// 0x000005AE System.Void ProjectSystem.ExcelSystem::SetUpExport(NPOI.SS.UserModel.ISheet,System.UInt32,System.UInt32,System.Collections.Generic.List`1<System.String>,BaseExcelScriptableObject`1<T>)
// 0x000005AF ProjectSystem.ExcelSystem_DataControl ProjectSystem.ExcelSystem::SetUpDataControl(NPOI.SS.UserModel.ISheet,System.UInt32,System.UInt32,System.Collections.Generic.List`1<System.String>)
extern void ExcelSystem_SetUpDataControl_m07F324CCC246C96CDE2EF78B2FC00290BAA2DC9B (void);
// 0x000005B0 System.String ProjectSystem.ExcelSystem::GetData(NPOI.SS.UserModel.ICell)
extern void ExcelSystem_GetData_m0DE54A6DAD8ADE4AE6639C952D85870C4B4273B4 (void);
// 0x000005B1 System.ValueTuple`3<System.Collections.Generic.List`1<System.String>,System.UInt32,System.UInt32> ProjectSystem.ExcelSystem::VoidCheckSheet(NPOI.SS.UserModel.ISheet)
extern void ExcelSystem_VoidCheckSheet_m6F77D17A4527B318FCA0416B708A2A671A63B884 (void);
// 0x000005B2 NPOI.SS.UserModel.IWorkbook ProjectSystem.ExcelSystem::ExceLoadBook(System.String)
extern void ExcelSystem_ExceLoadBook_m55E156F6F1FB84B572F67C7459518230421A1E82 (void);
// 0x000005B3 NPOI.SS.UserModel.ISheet ProjectSystem.ExcelSystem::ExcelLoadSheet(NPOI.SS.UserModel.IWorkbook,System.String)
extern void ExcelSystem_ExcelLoadSheet_mF7A434A01DCF21799F32BF042ADF238AFDA7E8FB (void);
// 0x000005B4 System.Boolean ProjectSystem.ExcelSystem::VoidCheck(NPOI.SS.UserModel.ICell)
extern void ExcelSystem_VoidCheck_mAF05B4DD2A5CBE4DC0930F6DFF610788712C8F6D (void);
// 0x000005B5 NPOI.SS.UserModel.ICell ProjectSystem.ExcelSystem::ExcelLoadCell(NPOI.SS.UserModel.ISheet,System.Int32,System.Int32)
extern void ExcelSystem_ExcelLoadCell_m225A9D11A0154778EE7692F172833F83D95475C1 (void);
// 0x000005B6 NPOI.SS.UserModel.IRow ProjectSystem.ExcelSystem::GetRow(NPOI.SS.UserModel.ISheet,System.Int32)
extern void ExcelSystem_GetRow_m92752C83045BDAEF98A0BC76AA68447B18B87F4B (void);
// 0x000005B7 NPOI.SS.UserModel.ICell ProjectSystem.ExcelSystem::GetCell(NPOI.SS.UserModel.IRow,System.Int32)
extern void ExcelSystem_GetCell_mC9C3131F3229CFDE731D9E751F517A72B8838890 (void);
// 0x000005B8 System.String ProjectSystem.ExcelSystem_Data::get_GetDataName()
extern void Data_get_GetDataName_m69CB424EFBE26F935672DFF0C6072999D47CB613 (void);
// 0x000005B9 System.String ProjectSystem.ExcelSystem_Data::get_GetDataObject()
extern void Data_get_GetDataObject_m8DA0AE4283FC1249DB49ED9013466E3061AF5219 (void);
// 0x000005BA System.Void ProjectSystem.ExcelSystem_Data::SetUp(System.String,System.String)
extern void Data_SetUp_mC68BC524B40C4992FD253C25AA8E18C38BD13326 (void);
// 0x000005BB System.Void ProjectSystem.ExcelSystem_Data::.ctor()
extern void Data__ctor_m4340D536ABF1859CB018283B97BF4633F91F9681 (void);
// 0x000005BC System.UInt32 ProjectSystem.ExcelSystem_DataGroup::get_GetGroupID()
extern void DataGroup_get_GetGroupID_m855252B51AA11599BB6606BF5AC76808BCB6EE80 (void);
// 0x000005BD System.UInt32 ProjectSystem.ExcelSystem_DataGroup::get_GetListDataCount()
extern void DataGroup_get_GetListDataCount_m71667D404F57DA73985CA21EF2990E57217B6F8D (void);
// 0x000005BE System.Void ProjectSystem.ExcelSystem_DataGroup::Clear()
extern void DataGroup_Clear_mCA788E2B6F59881B881E20028A71ED3C697F5BD1 (void);
// 0x000005BF System.Void ProjectSystem.ExcelSystem_DataGroup::SetUp(System.UInt32)
extern void DataGroup_SetUp_mBA6369D4A01B114392A2E7F05CD263CA45219963 (void);
// 0x000005C0 System.Void ProjectSystem.ExcelSystem_DataGroup::SetAdd(System.String,System.String)
extern void DataGroup_SetAdd_m6EEAC711516A403DE3099047FDDD38FA33C5284A (void);
// 0x000005C1 ProjectSystem.ExcelSystem_Data ProjectSystem.ExcelSystem_DataGroup::GetData(System.String)
extern void DataGroup_GetData_m7D9270187EC9D8CD20332E95C288E5BD6324B14A (void);
// 0x000005C2 ProjectSystem.ExcelSystem_Data ProjectSystem.ExcelSystem_DataGroup::GetData(System.UInt32)
extern void DataGroup_GetData_mF27D21832C082FD3648B76B3618F553A280E6F42 (void);
// 0x000005C3 System.Void ProjectSystem.ExcelSystem_DataGroup::.ctor()
extern void DataGroup__ctor_mD5C971AA782129944B045EEBAC6174A92518D830 (void);
// 0x000005C4 System.Void ProjectSystem.ExcelSystem_DataGroup_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m08A2D458F1C3D9D902F4EA47DE95EDCC2E490162 (void);
// 0x000005C5 System.Boolean ProjectSystem.ExcelSystem_DataGroup_<>c__DisplayClass9_0::<GetData>b__0(ProjectSystem.ExcelSystem_Data)
extern void U3CU3Ec__DisplayClass9_0_U3CGetDataU3Eb__0_mC5B55F4A8AF9601FE3352B09913529D28E0CBEF2 (void);
// 0x000005C6 System.UInt32 ProjectSystem.ExcelSystem_DataControl::get_GetDataGroupCount()
extern void DataControl_get_GetDataGroupCount_mA23F2A9AE4DC146A4C107A023FEFB2C261C953E1 (void);
// 0x000005C7 System.Void ProjectSystem.ExcelSystem_DataControl::SetUp(System.Collections.Generic.List`1<System.String>)
extern void DataControl_SetUp_m555ED73F6AE7418FEA34DFF0AFC22579985552CB (void);
// 0x000005C8 System.Void ProjectSystem.ExcelSystem_DataControl::Clear()
extern void DataControl_Clear_mB3D9E3247F84AF59A501B4E3CB77C77095AE8BA3 (void);
// 0x000005C9 ProjectSystem.ExcelSystem_DataGroup ProjectSystem.ExcelSystem_DataControl::AddGroup(ProjectSystem.ExcelSystem_DataGroup)
extern void DataControl_AddGroup_m7EE12AAA3701DE28C067A33884E8E0BB9232AAD7 (void);
// 0x000005CA ProjectSystem.ExcelSystem_DataGroup ProjectSystem.ExcelSystem_DataControl::GetDataGroupID(System.UInt32)
extern void DataControl_GetDataGroupID_m6684EE7920F2EC5397063EED9FE1B57772F0B27A (void);
// 0x000005CB ProjectSystem.ExcelSystem_DataGroup ProjectSystem.ExcelSystem_DataControl::GetDataGroupIndex(System.UInt32)
extern void DataControl_GetDataGroupIndex_m45A7A3235A43675E86DB8EDFF0031C6B15BF59DF (void);
// 0x000005CC ProjectSystem.ExcelSystem_Data ProjectSystem.ExcelSystem_DataControl::GetDataName(System.UInt32,System.String)
extern void DataControl_GetDataName_m82AD7920E56F1836475F6226D0F1AA54FCE11C76 (void);
// 0x000005CD ProjectSystem.ExcelSystem_Data ProjectSystem.ExcelSystem_DataControl::GetDataName(System.UInt32,System.UInt32)
extern void DataControl_GetDataName_mDABEBA4FB3229CBCBC4E18372752C4BB535732F6 (void);
// 0x000005CE ProjectSystem.ExcelSystem_Data ProjectSystem.ExcelSystem_DataControl::GetDataNameIndex(System.UInt32,System.String)
extern void DataControl_GetDataNameIndex_m4E0881EAC849982EFAA187D0FA3717D196D74D40 (void);
// 0x000005CF ProjectSystem.ExcelSystem_Data ProjectSystem.ExcelSystem_DataControl::GetDataNameIndex(System.UInt32,System.UInt32)
extern void DataControl_GetDataNameIndex_mA75BFB47C15DF80832342D6568E20A1BD49C7DD5 (void);
// 0x000005D0 System.String ProjectSystem.ExcelSystem_DataControl::GetDataListNameIndex(System.UInt32)
extern void DataControl_GetDataListNameIndex_m98D5FC9CE437106C25D0FA4ED93DA0FE4BAB233C (void);
// 0x000005D1 System.Void ProjectSystem.ExcelSystem_DataControl::CovertDataList(System.Object&,System.Int32,System.String)
extern void DataControl_CovertDataList_m8F105D8A68D291D4FD1A31D5E6D17C8669CE5778 (void);
// 0x000005D2 System.Object ProjectSystem.ExcelSystem_DataControl::ConvertDataEnum(System.Object&,System.Int32,System.String)
extern void DataControl_ConvertDataEnum_m8E91CC84C9CB8573CE2DA6217A04AA4D3B2A4BBB (void);
// 0x000005D3 System.Int32 ProjectSystem.ExcelSystem_DataControl::ConvertDataInt(System.Int32&,System.Int32,System.String)
extern void DataControl_ConvertDataInt_m3964E6934518AEAB31BFE1AAB3DECC4C7695C599 (void);
// 0x000005D4 System.Single ProjectSystem.ExcelSystem_DataControl::ConvertDataFloat(System.Single&,System.Int32,System.String)
extern void DataControl_ConvertDataFloat_m6787580F8AF5E8125C6789DFB6768040AF99FA19 (void);
// 0x000005D5 System.String ProjectSystem.ExcelSystem_DataControl::ConvertDataString(System.String&,System.Int32,System.String)
extern void DataControl_ConvertDataString_mBEB449CD352C21311E6884719B261A256066A1A7 (void);
// 0x000005D6 System.Boolean ProjectSystem.ExcelSystem_DataControl::ConvertDataBool(System.Boolean&,System.Int32,System.String)
extern void DataControl_ConvertDataBool_mD960C3D2981C2EBFC55A3D749364633897280E6C (void);
// 0x000005D7 System.Void ProjectSystem.ExcelSystem_DataControl::.ctor()
extern void DataControl__ctor_mF2DCE95E1016ACB0320B641DE91027BA8320197C (void);
// 0x000005D8 System.Void ProjectSystem.ExcelSystem_DataControl_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mD49827AB8EB00025F95E16EEEC8FC325164D5C77 (void);
// 0x000005D9 System.Boolean ProjectSystem.ExcelSystem_DataControl_<>c__DisplayClass7_0::<GetDataGroupID>b__0(ProjectSystem.ExcelSystem_DataGroup)
extern void U3CU3Ec__DisplayClass7_0_U3CGetDataGroupIDU3Eb__0_m95522C8A5D67AACD1DC5DAEE1A4A841A6BEA529B (void);
// 0x000005DA System.Void ProjectSystem.ExcelJsonSystem`1::LoadJson(ListDataObject`1<T>&,System.String)
// 0x000005DB System.Collections.IEnumerator ProjectSystem.ExcelJsonSystem`1::LoadJsonCoroutin(ListDataObject`1<T>,AddressableObject`1<UnityEngine.TextAsset>)
// 0x000005DC System.Void ProjectSystem.ExcelJsonSystem`1::.ctor()
// 0x000005DD System.Void ProjectSystem.ExcelJsonSystem`1_<LoadJsonCoroutin>d__1::.ctor(System.Int32)
// 0x000005DE System.Void ProjectSystem.ExcelJsonSystem`1_<LoadJsonCoroutin>d__1::System.IDisposable.Dispose()
// 0x000005DF System.Boolean ProjectSystem.ExcelJsonSystem`1_<LoadJsonCoroutin>d__1::MoveNext()
// 0x000005E0 System.Object ProjectSystem.ExcelJsonSystem`1_<LoadJsonCoroutin>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x000005E1 System.Void ProjectSystem.ExcelJsonSystem`1_<LoadJsonCoroutin>d__1::System.Collections.IEnumerator.Reset()
// 0x000005E2 System.Object ProjectSystem.ExcelJsonSystem`1_<LoadJsonCoroutin>d__1::System.Collections.IEnumerator.get_Current()
// 0x000005E3 System.Void Sample.TestMono::Start()
extern void TestMono_Start_mAE1E32125C520C2BFE499F4E73EAEB37AC3C56F5 (void);
// 0x000005E4 System.Void Sample.TestMono::Update()
extern void TestMono_Update_mFF1B843A538AB4901358DF231F81676BF984F31E (void);
// 0x000005E5 System.Void Sample.TestMono::Enter(UnityEngine.Collider)
extern void TestMono_Enter_m555888FDCFFF80A7397E6FDFEB2195640B4B165D (void);
// 0x000005E6 System.Void Sample.TestMono::Second(UnityEngine.Collider)
extern void TestMono_Second_mB97241B8AA9FDD6D732B41CC971E29A9B5AB2D5C (void);
// 0x000005E7 System.Void Sample.TestMono::Release(UnityEngine.Collider)
extern void TestMono_Release_mA8C194328DA921A0258E7457E1858DDA7E9C4382 (void);
// 0x000005E8 System.Void Sample.TestMono::.ctor()
extern void TestMono__ctor_mF1EF304A0F447403FCF3C93673D498A060704D5B (void);
// 0x000005E9 System.String Sample.TokimemoData::GetGirlName()
extern void TokimemoData_GetGirlName_m9A1DEBDCE02B1F3B6FC26218A743493ABDA7F1D1 (void);
// 0x000005EA System.Int32 Sample.TokimemoData::GetBirthMonth()
extern void TokimemoData_GetBirthMonth_m2810F3B54A99256B5F981772A885D47FD0E8A35C (void);
// 0x000005EB System.Int32 Sample.TokimemoData::GetBirthDay()
extern void TokimemoData_GetBirthDay_m481379C2B965B7EBD6FCA12E8588659F1F7E6413 (void);
// 0x000005EC System.Single Sample.TokimemoData::GetHeight()
extern void TokimemoData_GetHeight_m27AFFC16AED916EF84B35B2793C5F9FF1CE85D81 (void);
// 0x000005ED System.Single Sample.TokimemoData::GetWeight()
extern void TokimemoData_GetWeight_mC75EB8195BD31914D7919AAE0E238D081DBEA467 (void);
// 0x000005EE System.Single Sample.TokimemoData::GetBreast()
extern void TokimemoData_GetBreast_mC44445B8236E64160B4675C817BEF46032A03611 (void);
// 0x000005EF System.Single Sample.TokimemoData::GetWaist()
extern void TokimemoData_GetWaist_m6978BB9588F7BD8C249370405FB2710D6354084F (void);
// 0x000005F0 System.Single Sample.TokimemoData::GetHip()
extern void TokimemoData_GetHip_mE1FA2C3F81F2D024758BE13BFDE0D32BC6052093 (void);
// 0x000005F1 System.Void Sample.TokimemoData::ManualSetUp(DataFrameGroup&,ProjectSystem.ExcelSystem_DataGroup&)
extern void TokimemoData_ManualSetUp_m6F5C31696B9130841BE07A21C1365F5F7E7B19DA (void);
// 0x000005F2 System.Void Sample.TokimemoData::.ctor()
extern void TokimemoData__ctor_m4330ADD93D11091036C76B3AFF61AC2D29A29542 (void);
// 0x000005F3 System.Void Sample.TokimemoScriptableData::.ctor()
extern void TokimemoScriptableData__ctor_m955D9B3662BB08204B1020D50126EEF5703C7EA3 (void);
// 0x000005F4 UnityEngine.Material UnityStandardAssets.ImageEffects.Antialiasing::CurrentAAMaterial()
extern void Antialiasing_CurrentAAMaterial_mC7319A5A6AAA7090BDDC7A34B8C3FA6EC4046EB4 (void);
// 0x000005F5 System.Boolean UnityStandardAssets.ImageEffects.Antialiasing::CheckResources()
extern void Antialiasing_CheckResources_mED7198880F986E66B80E4D74A30273E3D1009EAC (void);
// 0x000005F6 System.Void UnityStandardAssets.ImageEffects.Antialiasing::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Antialiasing_OnRenderImage_m92C85AC4BA9CBD1EF26D247C3412BD6ADC16CB7E (void);
// 0x000005F7 System.Void UnityStandardAssets.ImageEffects.Antialiasing::.ctor()
extern void Antialiasing__ctor_m51CAAD6D2F9515AEFD3CB6C61C7BF8CA1D11E221 (void);
// 0x000005F8 System.Boolean UnityStandardAssets.ImageEffects.Bloom::CheckResources()
extern void Bloom_CheckResources_m7F6DA8B2BB6563257CC3E067DAF8B800C8C9D460 (void);
// 0x000005F9 System.Void UnityStandardAssets.ImageEffects.Bloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_OnRenderImage_m9E04CDDA91EBF9CE543417E1326FB89E771E3329 (void);
// 0x000005FA System.Void UnityStandardAssets.ImageEffects.Bloom::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_AddTo_mE9619BE3C2641B447503A370C89FC639D7B5AF4D (void);
// 0x000005FB System.Void UnityStandardAssets.ImageEffects.Bloom::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_BlendFlares_m34738C3C9263CD5584C8EB15EB78507F975E98B5 (void);
// 0x000005FC System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_BrightFilter_m6C6D8BFB58961F35054F43E065A77797883CB39B (void);
// 0x000005FD System.Void UnityStandardAssets.ImageEffects.Bloom::BrightFilter(UnityEngine.Color,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_BrightFilter_mDB8E6E61C66AA4000F9E8F91DC8CE4BF4C2ED220 (void);
// 0x000005FE System.Void UnityStandardAssets.ImageEffects.Bloom::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Bloom_Vignette_m3FA8F2D04EA6A142424E133BCB8FBE663E3988CC (void);
// 0x000005FF System.Void UnityStandardAssets.ImageEffects.Bloom::.ctor()
extern void Bloom__ctor_m9CE8D3A0AF0E8090F05E79AE479FEEC869C4A2BC (void);
// 0x00000600 System.Boolean UnityStandardAssets.ImageEffects.BloomAndFlares::CheckResources()
extern void BloomAndFlares_CheckResources_m5D302EB81163C3C7C73E15BD16B22EA430F136D7 (void);
// 0x00000601 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_OnRenderImage_m820BD8E7E5CD76FB1A0D8727FE55C68CA4D9E4A0 (void);
// 0x00000602 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::AddTo(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_AddTo_m96E721A8767A6E55237EB23A193309006A13A376 (void);
// 0x00000603 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BlendFlares(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_BlendFlares_m9EFFC2222A16EC1BE33B83E870F8670D480FAE0A (void);
// 0x00000604 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::BrightFilter(System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_BrightFilter_mA7575BBFC217875E02F0454025CE76D6CF05FDAE (void);
// 0x00000605 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::Vignette(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomAndFlares_Vignette_m671C8CAE9ED6BD64797AEF8CF982EE8414AA353E (void);
// 0x00000606 System.Void UnityStandardAssets.ImageEffects.BloomAndFlares::.ctor()
extern void BloomAndFlares__ctor_mF5F51BCA83ACC994B808494A7D673B0EE2B6B3C3 (void);
// 0x00000607 System.Boolean UnityStandardAssets.ImageEffects.BloomOptimized::CheckResources()
extern void BloomOptimized_CheckResources_m4FFE780FF754CB37C782696BD8D692DF29B2940A (void);
// 0x00000608 System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnDisable()
extern void BloomOptimized_OnDisable_m4039BC1075AEAB38751103C799EFBF7894CDAC35 (void);
// 0x00000609 System.Void UnityStandardAssets.ImageEffects.BloomOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BloomOptimized_OnRenderImage_m58EC1CE2EA1E8727F06C57ED08ECB799594A5D16 (void);
// 0x0000060A System.Void UnityStandardAssets.ImageEffects.BloomOptimized::.ctor()
extern void BloomOptimized__ctor_m0E77B85184205FA45BE54D6F2215ACF6608179ED (void);
// 0x0000060B UnityEngine.Material UnityStandardAssets.ImageEffects.Blur::get_material()
extern void Blur_get_material_m7B726B085C3EE53F708B326C2B362DCEB39BF999 (void);
// 0x0000060C System.Void UnityStandardAssets.ImageEffects.Blur::OnDisable()
extern void Blur_OnDisable_m297600A2F390F10D3AF51B93CAFAB5C03A1EC18D (void);
// 0x0000060D System.Void UnityStandardAssets.ImageEffects.Blur::Start()
extern void Blur_Start_mDA794F4F2883431D466813AD57DDF9886731C3D4 (void);
// 0x0000060E System.Void UnityStandardAssets.ImageEffects.Blur::FourTapCone(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32)
extern void Blur_FourTapCone_m1AC39BD3A8913BEF402C3C1C4797293DAF13921F (void);
// 0x0000060F System.Void UnityStandardAssets.ImageEffects.Blur::DownSample4x(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Blur_DownSample4x_m648F40010C0E6CDDEAA991C89E008A5A5358F243 (void);
// 0x00000610 System.Void UnityStandardAssets.ImageEffects.Blur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Blur_OnRenderImage_m1666FFFCC5C9F2D224ADCFFEDB271B767316C771 (void);
// 0x00000611 System.Void UnityStandardAssets.ImageEffects.Blur::.ctor()
extern void Blur__ctor_m402B569E15418AE90A4E1208254262FE1A524076 (void);
// 0x00000612 System.Void UnityStandardAssets.ImageEffects.Blur::.cctor()
extern void Blur__cctor_mF1E0CBBDB58F1F397F7CEB6805D8E124608F2298 (void);
// 0x00000613 System.Boolean UnityStandardAssets.ImageEffects.BlurOptimized::CheckResources()
extern void BlurOptimized_CheckResources_m4690B1D08CF4BEF70FC4AD9A5D0FB99378600D0B (void);
// 0x00000614 System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnDisable()
extern void BlurOptimized_OnDisable_m99433F61DD851EC7C4F30DB6F7515E5AC6756EEC (void);
// 0x00000615 System.Void UnityStandardAssets.ImageEffects.BlurOptimized::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void BlurOptimized_OnRenderImage_mC2B4151D0C82311AEB06B47189387AC604F9615C (void);
// 0x00000616 System.Void UnityStandardAssets.ImageEffects.BlurOptimized::.ctor()
extern void BlurOptimized__ctor_mB38631A090D409384457B18624700A57C6CBA68D (void);
// 0x00000617 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::CalculateViewProjection()
extern void CameraMotionBlur_CalculateViewProjection_mE265F7EDE22E2195C327E41AD18C5AB199BA2C17 (void);
// 0x00000618 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Start()
extern void CameraMotionBlur_Start_m0E1A32F74934F5EB27F267181D64FC1249FD741C (void);
// 0x00000619 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnEnable()
extern void CameraMotionBlur_OnEnable_m4040081CC7078E6B824F21AB72EF83AFA9568890 (void);
// 0x0000061A System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnDisable()
extern void CameraMotionBlur_OnDisable_m496FCA193BFD68D9302A87764CC80B726D5A94D8 (void);
// 0x0000061B System.Boolean UnityStandardAssets.ImageEffects.CameraMotionBlur::CheckResources()
extern void CameraMotionBlur_CheckResources_m6003DE71C47C031D582DF3B6F1C720977B649009 (void);
// 0x0000061C System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void CameraMotionBlur_OnRenderImage_mFD34E9ED689F6807901BAC6AFD041023C39C1137 (void);
// 0x0000061D System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::Remember()
extern void CameraMotionBlur_Remember_mD5E7AB7BDE951D208218FC34819554FDDE25A2F8 (void);
// 0x0000061E UnityEngine.Camera UnityStandardAssets.ImageEffects.CameraMotionBlur::GetTmpCam()
extern void CameraMotionBlur_GetTmpCam_mE9841487AA47FD7F4A680DFCEE74CDB4897DB562 (void);
// 0x0000061F System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::StartFrame()
extern void CameraMotionBlur_StartFrame_mF9FDC411A58A43A7A15D500DC771C9E08612B1B8 (void);
// 0x00000620 System.Int32 UnityStandardAssets.ImageEffects.CameraMotionBlur::divRoundUp(System.Int32,System.Int32)
extern void CameraMotionBlur_divRoundUp_m82F809436B9B186C5E476D5FE042001EC257DDF6 (void);
// 0x00000621 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.ctor()
extern void CameraMotionBlur__ctor_mC57B0D630BDBF1C79BB0A789EC8665ED5F4BB613 (void);
// 0x00000622 System.Void UnityStandardAssets.ImageEffects.CameraMotionBlur::.cctor()
extern void CameraMotionBlur__cctor_mE7142222C58ABB321445EFC0A3BDFB1BC81F6711 (void);
// 0x00000623 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Start()
extern void ColorCorrectionCurves_Start_m0ECE820F6B53EBCCFE8B2CE8FAD4CAE3D28781B5 (void);
// 0x00000624 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::Awake()
extern void ColorCorrectionCurves_Awake_mBD1BC43FC119A70FB689CB7629685C8E7686FB32 (void);
// 0x00000625 System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionCurves::CheckResources()
extern void ColorCorrectionCurves_CheckResources_m20A1ECBF13300DF54085714D437F60E16697744B (void);
// 0x00000626 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateParameters()
extern void ColorCorrectionCurves_UpdateParameters_m7D529BD5037CCD9508A66A5DA1652C8258B227B2 (void);
// 0x00000627 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::UpdateTextures()
extern void ColorCorrectionCurves_UpdateTextures_m89C95473600F6CA76C01674B0BBFF4507AD66D28 (void);
// 0x00000628 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ColorCorrectionCurves_OnRenderImage_m5031CDB14594542347FFD364FC36AAF495818FEE (void);
// 0x00000629 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionCurves::.ctor()
extern void ColorCorrectionCurves__ctor_m3ED2C81BF81F071598710B1134EF0651337927FF (void);
// 0x0000062A System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::CheckResources()
extern void ColorCorrectionLookup_CheckResources_m91294402B660B238BFBFDAB36AD8A5F40EAD04FB (void);
// 0x0000062B System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDisable()
extern void ColorCorrectionLookup_OnDisable_m6CFE877E53288C93592EF70F057F084B02670C99 (void);
// 0x0000062C System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnDestroy()
extern void ColorCorrectionLookup_OnDestroy_m420305849D7035FC6FA2CEB6DED416305F99C560 (void);
// 0x0000062D System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::SetIdentityLut()
extern void ColorCorrectionLookup_SetIdentityLut_m2A52E467FB7F1BD651AAA26E870F8F784308E7F3 (void);
// 0x0000062E System.Boolean UnityStandardAssets.ImageEffects.ColorCorrectionLookup::ValidDimensions(UnityEngine.Texture2D)
extern void ColorCorrectionLookup_ValidDimensions_m38F9BDE2D756E21F5FB2FB74DAE3242DB7519DCE (void);
// 0x0000062F System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::Convert(UnityEngine.Texture2D,System.String)
extern void ColorCorrectionLookup_Convert_m878152C99CE1BC6F175B7419E63B27B08AD887D6 (void);
// 0x00000630 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ColorCorrectionLookup_OnRenderImage_m51B30A601852A59447B3967A70C0F6000A958909 (void);
// 0x00000631 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionLookup::.ctor()
extern void ColorCorrectionLookup__ctor_mD85720C2F562F4FD5129BAA5A4BB57B26A42A8F4 (void);
// 0x00000632 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ColorCorrectionRamp_OnRenderImage_mB611ACFBD7BD21D3C5BE0CBB7A4009C5A9327E3E (void);
// 0x00000633 System.Void UnityStandardAssets.ImageEffects.ColorCorrectionRamp::.ctor()
extern void ColorCorrectionRamp__ctor_m5D26F8245696524783210DD30C4981B56E9EC62C (void);
// 0x00000634 System.Boolean UnityStandardAssets.ImageEffects.ContrastEnhance::CheckResources()
extern void ContrastEnhance_CheckResources_mD7A8290FD95DAA481E11EB510E27F9709A2C9707 (void);
// 0x00000635 System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ContrastEnhance_OnRenderImage_mA03990E8EEBA2A65EAFADF627327B34926E88B98 (void);
// 0x00000636 System.Void UnityStandardAssets.ImageEffects.ContrastEnhance::.ctor()
extern void ContrastEnhance__ctor_mC6A17229536BFB620278B49F8D6CA4CE61FD641D (void);
// 0x00000637 UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialLum()
extern void ContrastStretch_get_materialLum_mFA3F36EC235BC13BD87A580B33D29321850147FA (void);
// 0x00000638 UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialReduce()
extern void ContrastStretch_get_materialReduce_mEC419049AC77A1D0D3AA1CCF5FC8908DCF609EC0 (void);
// 0x00000639 UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialAdapt()
extern void ContrastStretch_get_materialAdapt_mCF400586EE0D8AF64073C6FC7EB6640EBC4235CF (void);
// 0x0000063A UnityEngine.Material UnityStandardAssets.ImageEffects.ContrastStretch::get_materialApply()
extern void ContrastStretch_get_materialApply_m2536CDCE7550DC3B9B2908501DBE2931C44E16D4 (void);
// 0x0000063B System.Void UnityStandardAssets.ImageEffects.ContrastStretch::Start()
extern void ContrastStretch_Start_m036007088F06A37063A9580F392F2593C4F2F081 (void);
// 0x0000063C System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnEnable()
extern void ContrastStretch_OnEnable_m17DC4417344C6175F77919E452DEBD54AEBD66A5 (void);
// 0x0000063D System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnDisable()
extern void ContrastStretch_OnDisable_m3CB5068DF0BEE880259C931340092A699E3DD713 (void);
// 0x0000063E System.Void UnityStandardAssets.ImageEffects.ContrastStretch::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ContrastStretch_OnRenderImage_m84EA66287D48055F498FF5DFEB0083C504AE7A62 (void);
// 0x0000063F System.Void UnityStandardAssets.ImageEffects.ContrastStretch::CalculateAdaptation(UnityEngine.Texture)
extern void ContrastStretch_CalculateAdaptation_mD5D9C8B371B907226D0F93D6A0DD0E869CDD7CDD (void);
// 0x00000640 System.Void UnityStandardAssets.ImageEffects.ContrastStretch::.ctor()
extern void ContrastStretch__ctor_m1A8EA45A045275D5020396872CBFE779C8278949 (void);
// 0x00000641 System.Boolean UnityStandardAssets.ImageEffects.CreaseShading::CheckResources()
extern void CreaseShading_CheckResources_mB279AB4EACCE1FB253362A078F90A056C741B123 (void);
// 0x00000642 System.Void UnityStandardAssets.ImageEffects.CreaseShading::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void CreaseShading_OnRenderImage_m2DC7E912357ED3A0751757F797878437DA86950D (void);
// 0x00000643 System.Void UnityStandardAssets.ImageEffects.CreaseShading::.ctor()
extern void CreaseShading__ctor_mE1D8508608690E8646D5B36E6185404A249C956A (void);
// 0x00000644 System.Boolean UnityStandardAssets.ImageEffects.DepthOfField::CheckResources()
extern void DepthOfField_CheckResources_m0C249CBD6A22AC2677AA97453F9C1AB8607F7784 (void);
// 0x00000645 System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnEnable()
extern void DepthOfField_OnEnable_m6741B3049B5E4E17C093FAC5D44A0EFCE658C593 (void);
// 0x00000646 System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnDisable()
extern void DepthOfField_OnDisable_m89B2E7EF9C7D5E026484BBB17E8178DA83913F84 (void);
// 0x00000647 System.Void UnityStandardAssets.ImageEffects.DepthOfField::ReleaseComputeResources()
extern void DepthOfField_ReleaseComputeResources_m9FFAAE0615BA2F81031CD9E3A1F17C2E8ACF1167 (void);
// 0x00000648 System.Void UnityStandardAssets.ImageEffects.DepthOfField::CreateComputeResources()
extern void DepthOfField_CreateComputeResources_m144C19BB16C05831CD0B4CE5DC80E1347F37D53D (void);
// 0x00000649 System.Single UnityStandardAssets.ImageEffects.DepthOfField::FocalDistance01(System.Single)
extern void DepthOfField_FocalDistance01_mAD7B2FD38F77D486FCEAFAA8D54DEF300AE73E1F (void);
// 0x0000064A System.Void UnityStandardAssets.ImageEffects.DepthOfField::WriteCoc(UnityEngine.RenderTexture,System.Boolean)
extern void DepthOfField_WriteCoc_m0F58251DD44C9B7A25E28EC45C9EB89A77746174 (void);
// 0x0000064B System.Void UnityStandardAssets.ImageEffects.DepthOfField::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfField_OnRenderImage_mBD7B49FD25DBB86F6B728D7961D595DCFE11B269 (void);
// 0x0000064C System.Void UnityStandardAssets.ImageEffects.DepthOfField::.ctor()
extern void DepthOfField__ctor_mBDF0338A503491D917E7FA343F342953773C9858 (void);
// 0x0000064D System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CreateMaterials()
extern void DepthOfFieldDeprecated_CreateMaterials_m7987189EDBE1F72FD49068B802AC6EBD86368A81 (void);
// 0x0000064E System.Boolean UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::CheckResources()
extern void DepthOfFieldDeprecated_CheckResources_m78092683ED5689E422911AEE89454D084AE05C51 (void);
// 0x0000064F System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnDisable()
extern void DepthOfFieldDeprecated_OnDisable_mDF53A066AD9036C99071FB930285BD19AC923696 (void);
// 0x00000650 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnEnable()
extern void DepthOfFieldDeprecated_OnEnable_m8F596341E9ABE3D53D374E07D74B3875CE2B8137 (void);
// 0x00000651 System.Single UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::FocalDistance01(System.Single)
extern void DepthOfFieldDeprecated_FocalDistance01_m00D69A3198F8D54F6DE0DA0A74BA2C148B73C28D (void);
// 0x00000652 System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetDividerBasedOnQuality()
extern void DepthOfFieldDeprecated_GetDividerBasedOnQuality_m2E1CC9B27087D14152D5AF975B52F0AB69F46776 (void);
// 0x00000653 System.Int32 UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::GetLowResolutionDividerBasedOnQuality(System.Int32)
extern void DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m3DBDA9D7474E15B50D6F31E3F91DF3AA564D17AD (void);
// 0x00000654 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_OnRenderImage_m7373016EBF1B049922A25960B8681AE44D47ED98 (void);
// 0x00000655 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Blur(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated_DofBlurriness,System.Int32,System.Single)
extern void DepthOfFieldDeprecated_Blur_mE97ADDA1CB43F84B428198D4EDCA12435DA8B107 (void);
// 0x00000656 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurFg(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated_DofBlurriness,System.Int32,System.Single)
extern void DepthOfFieldDeprecated_BlurFg_m9E7C81AF2E17E3AD14C44B63874CCD717EAB0D66 (void);
// 0x00000657 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::BlurHex(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32,System.Single,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_BlurHex_m5EE76E316221C6877542EBC8F9A131BE176F631F (void);
// 0x00000658 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::Downsample(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_Downsample_m73770942AAB7667353A5478BE15202ABCE6190E3 (void);
// 0x00000659 System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AddBokeh(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void DepthOfFieldDeprecated_AddBokeh_mDAC12FD0F5711BF8D8920F86C0D3251934519222 (void);
// 0x0000065A System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::ReleaseTextures()
extern void DepthOfFieldDeprecated_ReleaseTextures_mD4E8FDDB6EFE5782BDCD188BBA06BA80672B2676 (void);
// 0x0000065B System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::AllocateTextures(System.Boolean,UnityEngine.RenderTexture,System.Int32,System.Int32)
extern void DepthOfFieldDeprecated_AllocateTextures_mDFEBA3201E4C2069FEDE43AFF46C063F0F42D84B (void);
// 0x0000065C System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.ctor()
extern void DepthOfFieldDeprecated__ctor_m361158B5F9744812D8BE55DADAE5EBA104166113 (void);
// 0x0000065D System.Void UnityStandardAssets.ImageEffects.DepthOfFieldDeprecated::.cctor()
extern void DepthOfFieldDeprecated__cctor_m175F67C5F188A0EC8F033A647B6A48C689DB60F2 (void);
// 0x0000065E System.Boolean UnityStandardAssets.ImageEffects.EdgeDetection::CheckResources()
extern void EdgeDetection_CheckResources_m45732EC74CFF88B1AE6CAE84FEA357CDCC997EF9 (void);
// 0x0000065F System.Void UnityStandardAssets.ImageEffects.EdgeDetection::Start()
extern void EdgeDetection_Start_mAE2E27055817D0943C8420925359639EB18DE40D (void);
// 0x00000660 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::SetCameraFlag()
extern void EdgeDetection_SetCameraFlag_mBD3DAF292965786705B18B5B47B2CEDDC869ECCF (void);
// 0x00000661 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnEnable()
extern void EdgeDetection_OnEnable_m1FAB65EA14C9F934FDBDB62FD778ADDF936C6D52 (void);
// 0x00000662 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void EdgeDetection_OnRenderImage_mFAAB0A7E520C095829699660B33C39FCC919455A (void);
// 0x00000663 System.Void UnityStandardAssets.ImageEffects.EdgeDetection::.ctor()
extern void EdgeDetection__ctor_m0B166B98FAE308D02F125499FE85AF36A31EE669 (void);
// 0x00000664 System.Boolean UnityStandardAssets.ImageEffects.Fisheye::CheckResources()
extern void Fisheye_CheckResources_mB7CC03501BDA172511EDCE8CDA1500E7C63DFEE9 (void);
// 0x00000665 System.Void UnityStandardAssets.ImageEffects.Fisheye::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Fisheye_OnRenderImage_m0C11DC23898D79F4312B63B3FA8B4424D14324A2 (void);
// 0x00000666 System.Void UnityStandardAssets.ImageEffects.Fisheye::.ctor()
extern void Fisheye__ctor_m864A7A8F587F109C6AD0D9FE7940E9766B293AD1 (void);
// 0x00000667 System.Boolean UnityStandardAssets.ImageEffects.GlobalFog::CheckResources()
extern void GlobalFog_CheckResources_m1B2A82D23CC96D26D69451935E3ABFF226880F5C (void);
// 0x00000668 System.Void UnityStandardAssets.ImageEffects.GlobalFog::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void GlobalFog_OnRenderImage_mB4D89A1A081A4AA1684BA89B78305C15CCD35FAE (void);
// 0x00000669 System.Void UnityStandardAssets.ImageEffects.GlobalFog::CustomGraphicsBlit(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern void GlobalFog_CustomGraphicsBlit_m6D2C9F3F0EDD6141EAB19FA67B3A46C7CC486E13 (void);
// 0x0000066A System.Void UnityStandardAssets.ImageEffects.GlobalFog::.ctor()
extern void GlobalFog__ctor_m009B89438058D759F3968069E3DCCC2D495E0561 (void);
// 0x0000066B System.Void UnityStandardAssets.ImageEffects.Grayscale::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Grayscale_OnRenderImage_mC16ADFCEA6F222E4CC6D87D58A94C21D6F2452F6 (void);
// 0x0000066C System.Void UnityStandardAssets.ImageEffects.Grayscale::.ctor()
extern void Grayscale__ctor_mAB9F7F563BED5BC793D1470EB44C98A7C01808FA (void);
// 0x0000066D System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::Start()
extern void ImageEffectBase_Start_mCB74F1AB8BDB35BE0C8567AAABB24F59A10416C9 (void);
// 0x0000066E UnityEngine.Material UnityStandardAssets.ImageEffects.ImageEffectBase::get_material()
extern void ImageEffectBase_get_material_mCBA3A9537019FAFB5146CB9F3A0EC2F7C1A53C4E (void);
// 0x0000066F System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::OnDisable()
extern void ImageEffectBase_OnDisable_mCFD2365FAD014C1DE029AAAF6EB1AC35EFE91232 (void);
// 0x00000670 System.Void UnityStandardAssets.ImageEffects.ImageEffectBase::.ctor()
extern void ImageEffectBase__ctor_mC2A208B662D5A6F5E9F24A615737C6FE1432DC06 (void);
// 0x00000671 System.Void UnityStandardAssets.ImageEffects.ImageEffects::RenderDistortion(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Single,UnityEngine.Vector2,UnityEngine.Vector2)
extern void ImageEffects_RenderDistortion_m03C3F0F20E8A046F982E69B10AF813A0B3076A11 (void);
// 0x00000672 System.Void UnityStandardAssets.ImageEffects.ImageEffects::Blit(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ImageEffects_Blit_m8DF0ACAD0C4FC759E3D36B3DD563B1BBCACD3C98 (void);
// 0x00000673 System.Void UnityStandardAssets.ImageEffects.ImageEffects::BlitWithMaterial(UnityEngine.Material,UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ImageEffects_BlitWithMaterial_mAC6EE561D0CF9841CC0090F94922925585EC171D (void);
// 0x00000674 System.Void UnityStandardAssets.ImageEffects.ImageEffects::.ctor()
extern void ImageEffects__ctor_mBBA6CA1A07C99F9297736099B9B17EA8F6675449 (void);
// 0x00000675 System.Void UnityStandardAssets.ImageEffects.MotionBlur::Start()
extern void MotionBlur_Start_mAABD58AA810F8D43064FE0BF556A099DE2CEAEAA (void);
// 0x00000676 System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnDisable()
extern void MotionBlur_OnDisable_m059E6B3E17CD646DF39503611C342E45B9AA39F4 (void);
// 0x00000677 System.Void UnityStandardAssets.ImageEffects.MotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void MotionBlur_OnRenderImage_m17271973D79B9D3AA421068E5E34E8ADCFEE972A (void);
// 0x00000678 System.Void UnityStandardAssets.ImageEffects.MotionBlur::.ctor()
extern void MotionBlur__ctor_m05B8A16AF2B26108A1C5D557FA73483D796AEC05 (void);
// 0x00000679 System.Boolean UnityStandardAssets.ImageEffects.NoiseAndGrain::CheckResources()
extern void NoiseAndGrain_CheckResources_mB4552F2BE077ED9C74DF437BA8176B475AD9F1BD (void);
// 0x0000067A System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void NoiseAndGrain_OnRenderImage_m2BCF8B0B93F98249F947B076B1DAF03F0F08C5F6 (void);
// 0x0000067B System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::DrawNoiseQuadGrid(UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Texture2D,System.Int32)
extern void NoiseAndGrain_DrawNoiseQuadGrid_m5E5CB6D173379C752EB5A8D2FAA6C76933F99DBB (void);
// 0x0000067C System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.ctor()
extern void NoiseAndGrain__ctor_mE912CF5485D816E97569AE3A52AAFE52334B98C2 (void);
// 0x0000067D System.Void UnityStandardAssets.ImageEffects.NoiseAndGrain::.cctor()
extern void NoiseAndGrain__cctor_m642AF4DDF9AA7FA27831AE695BD1487BA209431B (void);
// 0x0000067E System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::Start()
extern void NoiseAndScratches_Start_mC0CFF72F8784074A52552A4DF9EB8CDAD4CEBF0C (void);
// 0x0000067F UnityEngine.Material UnityStandardAssets.ImageEffects.NoiseAndScratches::get_material()
extern void NoiseAndScratches_get_material_mD26BC7251CC4505EBE44E87F7C17A1D25E17CFDA (void);
// 0x00000680 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnDisable()
extern void NoiseAndScratches_OnDisable_m088BBEEFE0DCD54C27CFAFAE04E1B5D570536954 (void);
// 0x00000681 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::SanitizeParameters()
extern void NoiseAndScratches_SanitizeParameters_m26D2C93FF2024D33753774B701D768E5491B3B49 (void);
// 0x00000682 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void NoiseAndScratches_OnRenderImage_mED39D37A141027AFBED7EABCB28E5AF21E8FE82C (void);
// 0x00000683 System.Void UnityStandardAssets.ImageEffects.NoiseAndScratches::.ctor()
extern void NoiseAndScratches__ctor_mA4847141C71255A0B3435442FE696248AC915F70 (void);
// 0x00000684 UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShaderAndCreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern void PostEffectsBase_CheckShaderAndCreateMaterial_mA0855C41F4D32605063FAFD0836FFB78A0BCF490 (void);
// 0x00000685 UnityEngine.Material UnityStandardAssets.ImageEffects.PostEffectsBase::CreateMaterial(UnityEngine.Shader,UnityEngine.Material)
extern void PostEffectsBase_CreateMaterial_m58573EB3A5D193A92003C5DF82DAACB9ED8E8078 (void);
// 0x00000686 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::OnEnable()
extern void PostEffectsBase_OnEnable_m0794D55F439C915D930543BD5BC3DD98D0900B27 (void);
// 0x00000687 System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport()
extern void PostEffectsBase_CheckSupport_mEC1CB870A1763E3545F7829BAC85CE42275DF4B6 (void);
// 0x00000688 System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckResources()
extern void PostEffectsBase_CheckResources_m304B5191F98669642E68716BD21E97DC4651836B (void);
// 0x00000689 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::Start()
extern void PostEffectsBase_Start_mB6D8676C7127E664315ADFAAD40A44071D778121 (void);
// 0x0000068A System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean)
extern void PostEffectsBase_CheckSupport_m20B1CB0EC0F019D345427F851C67A5FD2E01061A (void);
// 0x0000068B System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckSupport(System.Boolean,System.Boolean)
extern void PostEffectsBase_CheckSupport_m8016EB7CDCA60B4E5334AA1CEFE2EB2AA0A717ED (void);
// 0x0000068C System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::Dx11Support()
extern void PostEffectsBase_Dx11Support_mD0D2A3A932771541E700EE9420F37038759DBC27 (void);
// 0x0000068D System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::ReportAutoDisable()
extern void PostEffectsBase_ReportAutoDisable_m732B3B3112769D5ADA588AEFA3F470A4EB5A3D31 (void);
// 0x0000068E System.Boolean UnityStandardAssets.ImageEffects.PostEffectsBase::CheckShader(UnityEngine.Shader)
extern void PostEffectsBase_CheckShader_mC043E14AD142392993ECD52EC5882B6C5E0501BD (void);
// 0x0000068F System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::NotSupported()
extern void PostEffectsBase_NotSupported_mB15B8F953382CBA0AC80B39828B807399B730F12 (void);
// 0x00000690 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern void PostEffectsBase_DrawBorder_m0FA412FECC786A5623F734DBD9D1F7EE016BE7E2 (void);
// 0x00000691 System.Void UnityStandardAssets.ImageEffects.PostEffectsBase::.ctor()
extern void PostEffectsBase__ctor_mFA1F5C2F42295DE5BC5B12E891F7EFAE11D91728 (void);
// 0x00000692 System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void PostEffectsHelper_OnRenderImage_m3249CDADE845507F040B3BA27CDB6718DD0CFBF6 (void);
// 0x00000693 System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelPlaneAlignedWithCamera(System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Camera)
extern void PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m8B5222689A79EAD6CC172A892EDE45EF8EA75EEF (void);
// 0x00000694 System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawBorder(UnityEngine.RenderTexture,UnityEngine.Material)
extern void PostEffectsHelper_DrawBorder_mF93C2573512E51B1C1F1FC27823B02CB9239E153 (void);
// 0x00000695 System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::DrawLowLevelQuad(System.Single,System.Single,System.Single,System.Single,UnityEngine.RenderTexture,UnityEngine.RenderTexture,UnityEngine.Material)
extern void PostEffectsHelper_DrawLowLevelQuad_mBD073127C1A29721D7EEFC2112B2AB4F63C6E95F (void);
// 0x00000696 System.Void UnityStandardAssets.ImageEffects.PostEffectsHelper::.ctor()
extern void PostEffectsHelper__ctor_mA8DA1D61B8FEED9DA832C63BC79D4ECD9C628E85 (void);
// 0x00000697 System.Boolean UnityStandardAssets.ImageEffects.Quads::HasMeshes()
extern void Quads_HasMeshes_m4BC0E7A503FDD62C1B6AFD11BF42C48C44D4ABF3 (void);
// 0x00000698 System.Void UnityStandardAssets.ImageEffects.Quads::Cleanup()
extern void Quads_Cleanup_mD8D12694A940EB6155B22BE54FDF1A5048E97FBE (void);
// 0x00000699 UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Quads::GetMeshes(System.Int32,System.Int32)
extern void Quads_GetMeshes_mE0C1253983124C6022E99BF9C690AB87E12834B2 (void);
// 0x0000069A UnityEngine.Mesh UnityStandardAssets.ImageEffects.Quads::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Quads_GetMesh_m389C16D7D2D2163D185AFF636C94B799C16DFC19 (void);
// 0x0000069B System.Void UnityStandardAssets.ImageEffects.Quads::.ctor()
extern void Quads__ctor_m0F21717E48723759E99AA626D14C571F65C14880 (void);
// 0x0000069C System.Void UnityStandardAssets.ImageEffects.Quads::.cctor()
extern void Quads__cctor_m63271B518EF4D21A7DA56D8AC00BF587E24C33BC (void);
// 0x0000069D System.Boolean UnityStandardAssets.ImageEffects.ScreenOverlay::CheckResources()
extern void ScreenOverlay_CheckResources_mDC30CB8BAB81DA4EB1176628FDFEB5F650DE351C (void);
// 0x0000069E System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenOverlay_OnRenderImage_m5A4BA1B7FB006E1AA855C1CF5589E7379B4A55E6 (void);
// 0x0000069F System.Void UnityStandardAssets.ImageEffects.ScreenOverlay::.ctor()
extern void ScreenOverlay__ctor_mD098EF34DFC76C199E76E8052230EB389A0CC13C (void);
// 0x000006A0 System.Boolean UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::CheckResources()
extern void ScreenSpaceAmbientObscurance_CheckResources_m36D34C25745A60AF409689461F575BB772922F9E (void);
// 0x000006A1 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnDisable()
extern void ScreenSpaceAmbientObscurance_OnDisable_m5DF34448FF84C4115C79492BEA74FB7B97C945F3 (void);
// 0x000006A2 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenSpaceAmbientObscurance_OnRenderImage_mCD1F333B91A96F8D3937714AA8CCBE9BFB31F743 (void);
// 0x000006A3 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientObscurance::.ctor()
extern void ScreenSpaceAmbientObscurance__ctor_m724A9A963558BA931EAD2C370A44C966D8BA8C05 (void);
// 0x000006A4 UnityEngine.Material UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterial(UnityEngine.Shader)
extern void ScreenSpaceAmbientOcclusion_CreateMaterial_m355EF1BDA9A9F63522A7592DB105A3C867F0B51B (void);
// 0x000006A5 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::DestroyMaterial(UnityEngine.Material)
extern void ScreenSpaceAmbientOcclusion_DestroyMaterial_mC77694CF92CCFD7D0E49D6FAA6A2909024A23CCA (void);
// 0x000006A6 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnDisable()
extern void ScreenSpaceAmbientOcclusion_OnDisable_mD706BCF8E1F4D738DA0CBBC8106752BEB441C795 (void);
// 0x000006A7 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::Start()
extern void ScreenSpaceAmbientOcclusion_Start_m98A9C6AC986C0A01BF123A0ECC67C4633D777E2A (void);
// 0x000006A8 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnEnable()
extern void ScreenSpaceAmbientOcclusion_OnEnable_m5AD62EDC645E3221C96AC5E67E912C7FEA179FDF (void);
// 0x000006A9 System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::CreateMaterials()
extern void ScreenSpaceAmbientOcclusion_CreateMaterials_mB9DA32D885036E3970FBFF9C8AC8410B7C26DBC1 (void);
// 0x000006AA System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void ScreenSpaceAmbientOcclusion_OnRenderImage_m9F10B99EBD986D865F3B595DFF7697B9271972B5 (void);
// 0x000006AB System.Void UnityStandardAssets.ImageEffects.ScreenSpaceAmbientOcclusion::.ctor()
extern void ScreenSpaceAmbientOcclusion__ctor_m85891921B77C832330BE8B476E5107A64F18E9F0 (void);
// 0x000006AC System.Void UnityStandardAssets.ImageEffects.SepiaTone::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void SepiaTone_OnRenderImage_mA99E06A944A72FCA3A89A6054C1A9934B21EA4BC (void);
// 0x000006AD System.Void UnityStandardAssets.ImageEffects.SepiaTone::.ctor()
extern void SepiaTone__ctor_mAA07BEFC9B027350BE71818E18972B19A8D2C4C6 (void);
// 0x000006AE System.Boolean UnityStandardAssets.ImageEffects.SunShafts::CheckResources()
extern void SunShafts_CheckResources_mB28807554E1C72E8EF1788C47C0C35FFD8542C92 (void);
// 0x000006AF System.Void UnityStandardAssets.ImageEffects.SunShafts::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void SunShafts_OnRenderImage_mC38080C46A73ECC5566648935E08F1BDE5387FBB (void);
// 0x000006B0 System.Void UnityStandardAssets.ImageEffects.SunShafts::.ctor()
extern void SunShafts__ctor_mB7B9475D995407A720DFAD133A94BA799C5B2AE3 (void);
// 0x000006B1 System.Boolean UnityStandardAssets.ImageEffects.TiltShift::CheckResources()
extern void TiltShift_CheckResources_m3A988CBE570076BF064EB28141134153C94470A5 (void);
// 0x000006B2 System.Void UnityStandardAssets.ImageEffects.TiltShift::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void TiltShift_OnRenderImage_m29793FAA4D1EC712C6551F2854254A3474915BF9 (void);
// 0x000006B3 System.Void UnityStandardAssets.ImageEffects.TiltShift::.ctor()
extern void TiltShift__ctor_m17DF846193FB9DE098FB22F7BF16012EBB82AACE (void);
// 0x000006B4 System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CheckResources()
extern void Tonemapping_CheckResources_m8A3A3C61145E53BCD8E8F6DEA36F26AB76A3C742 (void);
// 0x000006B5 System.Single UnityStandardAssets.ImageEffects.Tonemapping::UpdateCurve()
extern void Tonemapping_UpdateCurve_m57D0F9B2859479DE71D507B77153C8C031B193F0 (void);
// 0x000006B6 System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnDisable()
extern void Tonemapping_OnDisable_m934B31423514B72A8446C42E330E2E647F72653F (void);
// 0x000006B7 System.Boolean UnityStandardAssets.ImageEffects.Tonemapping::CreateInternalRenderTexture()
extern void Tonemapping_CreateInternalRenderTexture_mE42415E8EA497B2F9C96DE5B936516FECC2EE69A (void);
// 0x000006B8 System.Void UnityStandardAssets.ImageEffects.Tonemapping::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Tonemapping_OnRenderImage_m5477DF1F9F844ECEDC1CA7DCE5554A40A1048440 (void);
// 0x000006B9 System.Void UnityStandardAssets.ImageEffects.Tonemapping::.ctor()
extern void Tonemapping__ctor_m771270B9AC12276987A5B176AC01CDA873FF089F (void);
// 0x000006BA System.Boolean UnityStandardAssets.ImageEffects.Triangles::HasMeshes()
extern void Triangles_HasMeshes_m3FBACEA56E7B0437BE854A153B647480531C2F7A (void);
// 0x000006BB System.Void UnityStandardAssets.ImageEffects.Triangles::Cleanup()
extern void Triangles_Cleanup_m08DA8845F1CF35B11916649B0C6ED78406D1ED97 (void);
// 0x000006BC UnityEngine.Mesh[] UnityStandardAssets.ImageEffects.Triangles::GetMeshes(System.Int32,System.Int32)
extern void Triangles_GetMeshes_m3DB03D7EDEE1CBDB6D28072770AE20AA44003234 (void);
// 0x000006BD UnityEngine.Mesh UnityStandardAssets.ImageEffects.Triangles::GetMesh(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Triangles_GetMesh_m28C77B830B07AECCBA2E163BFA8B37768E738602 (void);
// 0x000006BE System.Void UnityStandardAssets.ImageEffects.Triangles::.ctor()
extern void Triangles__ctor_mD0658EDAF0999D9BDA2C1FBFCBB83A551FCEBF18 (void);
// 0x000006BF System.Void UnityStandardAssets.ImageEffects.Triangles::.cctor()
extern void Triangles__cctor_mB6CCFBA963D829EA0C65E5DE04B12BA437FA97B7 (void);
// 0x000006C0 System.Void UnityStandardAssets.ImageEffects.Twirl::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Twirl_OnRenderImage_m54CAD9C6DEDA4B0A8B1C4C348776076725BDA20E (void);
// 0x000006C1 System.Void UnityStandardAssets.ImageEffects.Twirl::.ctor()
extern void Twirl__ctor_m4C83ABBF0D08D5D522E186EDCFB6596866B36785 (void);
// 0x000006C2 System.Boolean UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::CheckResources()
extern void VignetteAndChromaticAberration_CheckResources_mE7E2561C3849055304B86F898782CA2A8DFE78A2 (void);
// 0x000006C3 System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void VignetteAndChromaticAberration_OnRenderImage_m1623BDFA946B14103D45AD9FF10BCE1497E352C2 (void);
// 0x000006C4 System.Void UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration::.ctor()
extern void VignetteAndChromaticAberration__ctor_m3DF39E4E746B7E100F55B1CE70DCF349BD37F0E2 (void);
// 0x000006C5 System.Void UnityStandardAssets.ImageEffects.Vortex::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void Vortex_OnRenderImage_mCA066061BDBF67BB18DF25CC410A4917A8E1D2B2 (void);
// 0x000006C6 System.Void UnityStandardAssets.ImageEffects.Vortex::.ctor()
extern void Vortex__ctor_m07B90386422BDB2135F82236DD44688266759A7E (void);
// 0x000006C7 System.Void RPGCharacterAnimsFREE.GUIControls::Start()
extern void GUIControls_Start_m7B3461CB662A9F9C8DBB05FC5C042A5D36BE3031 (void);
// 0x000006C8 System.Void RPGCharacterAnimsFREE.GUIControls::OnGUI()
extern void GUIControls_OnGUI_m0DFD9CBC17B07DF65EF0B45D8485E472B33917D4 (void);
// 0x000006C9 System.Void RPGCharacterAnimsFREE.GUIControls::Navigation()
extern void GUIControls_Navigation_m635085B4C12B6AED8F1E8157B03A4873FBCA0CB0 (void);
// 0x000006CA System.Void RPGCharacterAnimsFREE.GUIControls::Attacks()
extern void GUIControls_Attacks_m8A0B7B99092D7D0DEB505C84B7CA3DACEC3B1502 (void);
// 0x000006CB System.Void RPGCharacterAnimsFREE.GUIControls::Damage()
extern void GUIControls_Damage_mC32B8B66640C39FD0970896A15B82D6C41DAD6D5 (void);
// 0x000006CC System.Void RPGCharacterAnimsFREE.GUIControls::RollDodgeTurn()
extern void GUIControls_RollDodgeTurn_m31A56363CF58CDBBC064122CB2900DC373CBBB06 (void);
// 0x000006CD System.Void RPGCharacterAnimsFREE.GUIControls::Jumping()
extern void GUIControls_Jumping_mEC54815880A59B522660F1887D1970C30CCA65A2 (void);
// 0x000006CE System.Void RPGCharacterAnimsFREE.GUIControls::WeaponSwitching()
extern void GUIControls_WeaponSwitching_mEC1766EFA27FEB3278AAAC9A4B12793B8EB9075E (void);
// 0x000006CF System.Void RPGCharacterAnimsFREE.GUIControls::Misc()
extern void GUIControls_Misc_m11987BFBD57BB1C4A33E1F76EBA446683058B724 (void);
// 0x000006D0 System.Void RPGCharacterAnimsFREE.GUIControls::.ctor()
extern void GUIControls__ctor_mEEF77699806C309D756A4D20C5217D8A1AA7E150 (void);
// 0x000006D1 System.Void RPGCharacterAnimsFREE.HighJumpTrampoline::Update()
extern void HighJumpTrampoline_Update_mE3B7C7D05D9BCECC5F91D63FBE88A11E68DD8632 (void);
// 0x000006D2 System.Void RPGCharacterAnimsFREE.HighJumpTrampoline::OnTriggerEnter(UnityEngine.Collider)
extern void HighJumpTrampoline_OnTriggerEnter_mC2EA3F9740EAE39143490F6B693B3919F3A99317 (void);
// 0x000006D3 System.Void RPGCharacterAnimsFREE.HighJumpTrampoline::OnTriggerExit(UnityEngine.Collider)
extern void HighJumpTrampoline_OnTriggerExit_m02BDBA9ACBFB7C5FFC14DF93C4AC6034C41AB9DB (void);
// 0x000006D4 System.Void RPGCharacterAnimsFREE.HighJumpTrampoline::.ctor()
extern void HighJumpTrampoline__ctor_m945E18B2E7EAF50B666B3C9D4A26F236655D4322 (void);
// 0x000006D5 System.Void RPGCharacterAnimsFREE.NoJumpSlime::OnTriggerEnter(UnityEngine.Collider)
extern void NoJumpSlime_OnTriggerEnter_m1157E87CD7D16FB9A0B420E440B53E4DBD7EFB6D (void);
// 0x000006D6 System.Void RPGCharacterAnimsFREE.NoJumpSlime::OnTriggerExit(UnityEngine.Collider)
extern void NoJumpSlime_OnTriggerExit_mE06B2D612DA503953B9EACBDE2B4D8443C2668B4 (void);
// 0x000006D7 System.Void RPGCharacterAnimsFREE.NoJumpSlime::.ctor()
extern void NoJumpSlime__ctor_m6F24C671661EB70753A76F3ACAB950E1BF2A232F (void);
// 0x000006D8 System.Void RPGCharacterAnimsFREE.NoJumpSlime::<OnTriggerEnter>b__2_0()
extern void NoJumpSlime_U3COnTriggerEnterU3Eb__2_0_mD984EA98B17D3D2ADF86B52050A075667C8EAB32 (void);
// 0x000006D9 System.Void RPGCharacterAnimsFREE.NoJumpSlime_<>c::.cctor()
extern void U3CU3Ec__cctor_m2D08DA4F3433ABB64AC25246788DBC92EB7FBCCB (void);
// 0x000006DA System.Void RPGCharacterAnimsFREE.NoJumpSlime_<>c::.ctor()
extern void U3CU3Ec__ctor_m499E2FE5E8AFFFD03D5090E3229E555BD1ADB3DC (void);
// 0x000006DB System.Void RPGCharacterAnimsFREE.NoJumpSlime_<>c::<OnTriggerEnter>b__2_1()
extern void U3CU3Ec_U3COnTriggerEnterU3Eb__2_1_m84F6C5D94EB1FFAB8A46F85BF76895047ED060C3 (void);
// 0x000006DC System.Int32 RPGCharacterAnimsFREE.AnimationData::ConvertToAnimatorWeapon(System.Int32,System.Int32)
extern void AnimationData_ConvertToAnimatorWeapon_m0C5BF66650847636771C5CC716C468E66006E397 (void);
// 0x000006DD System.Int32 RPGCharacterAnimsFREE.AnimationData::ConvertToAnimatorLeftRight(System.Int32,System.Int32)
extern void AnimationData_ConvertToAnimatorLeftRight_mEBA008080AB835566B806BFF32FE7C9702BD46B4 (void);
// 0x000006DE System.Boolean RPGCharacterAnimsFREE.AnimationData::IsNoWeapon(System.Int32)
extern void AnimationData_IsNoWeapon_m3CEBEC8B4DCA0CF141BDEBE6F824EDA166A26712 (void);
// 0x000006DF System.Boolean RPGCharacterAnimsFREE.AnimationData::IsLeftWeapon(System.Int32)
extern void AnimationData_IsLeftWeapon_m51A55B4C636BD64D01F5A68F698FE350A5FE0B10 (void);
// 0x000006E0 System.Boolean RPGCharacterAnimsFREE.AnimationData::IsRightWeapon(System.Int32)
extern void AnimationData_IsRightWeapon_mB224E0A2B53A963143C2420F14A801AFA8986632 (void);
// 0x000006E1 System.Boolean RPGCharacterAnimsFREE.AnimationData::Is1HandedWeapon(System.Int32)
extern void AnimationData_Is1HandedWeapon_m120049602DA5ECB4D9DEE07C58CF8106A7CC9512 (void);
// 0x000006E2 System.Boolean RPGCharacterAnimsFREE.AnimationData::Is2HandedWeapon(System.Int32)
extern void AnimationData_Is2HandedWeapon_m3163EAC44D0C1DA72F5A82E08F3F182381CD7C63 (void);
// 0x000006E3 System.Boolean RPGCharacterAnimsFREE.AnimationData::IsIKWeapon(System.Int32)
extern void AnimationData_IsIKWeapon_m4A9FA1B0E26AE7119E55F28979E459CB0B6535EF (void);
// 0x000006E4 System.Single RPGCharacterAnimsFREE.AnimationData::AttackDuration(System.Int32,System.Int32,System.Int32)
extern void AnimationData_AttackDuration_m005A097CAE9A2982E5E7A886DC1D7DFD8F50E5AD (void);
// 0x000006E5 System.Single RPGCharacterAnimsFREE.AnimationData::SheathDuration(System.Int32,System.Int32)
extern void AnimationData_SheathDuration_m2F0C64058DAAAD2DBC63E4C07B1ED7252047F672 (void);
// 0x000006E6 System.Int32 RPGCharacterAnimsFREE.AnimationData::RandomAttackNumber(System.Int32,System.Int32)
extern void AnimationData_RandomAttackNumber_m38C11BC73FD745AE64B3D53C3167DAF2BCA29BEC (void);
// 0x000006E7 System.Int32 RPGCharacterAnimsFREE.AnimationData::RandomKickNumber(System.Int32)
extern void AnimationData_RandomKickNumber_mAFD9D15B8015D93D96B8FBF7D34E44FF559DAC71 (void);
// 0x000006E8 System.Int32 RPGCharacterAnimsFREE.AnimationData::RandomCastNumber(System.String)
extern void AnimationData_RandomCastNumber_mF26142FFC3FBC154963AE4ACB0C75D8DAFF460CC (void);
// 0x000006E9 System.Int32 RPGCharacterAnimsFREE.AnimationData::RandomConversationNumber()
extern void AnimationData_RandomConversationNumber_m754C42157851C6455244A578168D56BE93D62EF4 (void);
// 0x000006EA System.Int32 RPGCharacterAnimsFREE.AnimationData::RandomHitNumber(System.String)
extern void AnimationData_RandomHitNumber_m6F61DB328D204CED9C061F9A764341340085C47F (void);
// 0x000006EB UnityEngine.Vector3 RPGCharacterAnimsFREE.AnimationData::HitDirection(System.String,System.Int32)
extern void AnimationData_HitDirection_mCD5A20A41A404CF52E3BA7BD72131D41D9434171 (void);
// 0x000006EC System.Void RPGCharacterAnimsFREE.AnimationData::.ctor()
extern void AnimationData__ctor_mEB50DEF6DC9E2F93DB16BD5F435416494F41B10C (void);
// 0x000006ED System.Void RPGCharacterAnimsFREE.CoroutineQueue::.ctor(System.UInt32,System.Func`2<System.Collections.IEnumerator,UnityEngine.Coroutine>)
extern void CoroutineQueue__ctor_m2E4C576AF563FD7AEBAD0BC149D1E20299316A09 (void);
// 0x000006EE System.Void RPGCharacterAnimsFREE.CoroutineQueue::Run(System.Collections.IEnumerator)
extern void CoroutineQueue_Run_m9FA23443F7F3B042FC0077B0E675AA9BACAA0347 (void);
// 0x000006EF System.Void RPGCharacterAnimsFREE.CoroutineQueue::RunCallback(System.Action)
extern void CoroutineQueue_RunCallback_mD106058F02DCCD05FFCFB9475042E4D0B0A03761 (void);
// 0x000006F0 System.Collections.IEnumerator RPGCharacterAnimsFREE.CoroutineQueue::CoroutineCallback(System.Action)
extern void CoroutineQueue_CoroutineCallback_m042C197B3DA7388569A88780A04EF99F76BF7E59 (void);
// 0x000006F1 System.Collections.IEnumerator RPGCharacterAnimsFREE.CoroutineQueue::CoroutineRunner(System.Collections.IEnumerator)
extern void CoroutineQueue_CoroutineRunner_mB12CDB4CC9A811BB0B307466B3C56CF5EB919C0A (void);
// 0x000006F2 System.Void RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineCallback>d__7::.ctor(System.Int32)
extern void U3CCoroutineCallbackU3Ed__7__ctor_m5FAE6DBDB1EC86E7252E7F7F7BC062FD1D17DA03 (void);
// 0x000006F3 System.Void RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineCallback>d__7::System.IDisposable.Dispose()
extern void U3CCoroutineCallbackU3Ed__7_System_IDisposable_Dispose_m185B09AAE625FAEE1C05B0A592D53044D55E0E91 (void);
// 0x000006F4 System.Boolean RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineCallback>d__7::MoveNext()
extern void U3CCoroutineCallbackU3Ed__7_MoveNext_mB07F84F6943B7DD9A32C4D21AC18B2A368EDC6ED (void);
// 0x000006F5 System.Object RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineCallback>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineCallbackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E048AD633FD8060E90BBF336FF5066DAC2A8D13 (void);
// 0x000006F6 System.Void RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineCallback>d__7::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineCallbackU3Ed__7_System_Collections_IEnumerator_Reset_mCCFCFC560831454E579DD36522F17C0BAD2D9CA9 (void);
// 0x000006F7 System.Object RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineCallback>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineCallbackU3Ed__7_System_Collections_IEnumerator_get_Current_m28792216D44AA758CB7580BADF0CCE3591983554 (void);
// 0x000006F8 System.Void RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineRunner>d__8::.ctor(System.Int32)
extern void U3CCoroutineRunnerU3Ed__8__ctor_m396768941F3C41FA2F67ECB19A04EDA9859909B4 (void);
// 0x000006F9 System.Void RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineRunner>d__8::System.IDisposable.Dispose()
extern void U3CCoroutineRunnerU3Ed__8_System_IDisposable_Dispose_m81259E29D5A3DF8F365BCE7014D557EE5B594CC0 (void);
// 0x000006FA System.Boolean RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineRunner>d__8::MoveNext()
extern void U3CCoroutineRunnerU3Ed__8_MoveNext_m13C3E3197334163BE236193B0929B8899F3A27BE (void);
// 0x000006FB System.Object RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineRunner>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoroutineRunnerU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0ECFF44F9AA848163A4BFEA8457EFC3B8C52C43D (void);
// 0x000006FC System.Void RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineRunner>d__8::System.Collections.IEnumerator.Reset()
extern void U3CCoroutineRunnerU3Ed__8_System_Collections_IEnumerator_Reset_m93A3F25041318245CE1C8C44BDA2D3E825DBF69A (void);
// 0x000006FD System.Object RPGCharacterAnimsFREE.CoroutineQueue_<CoroutineRunner>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CCoroutineRunnerU3Ed__8_System_Collections_IEnumerator_get_Current_m54E3B207FAF47EE1E97983F5B274ACDA4C78883F (void);
// 0x000006FE System.Void RPGCharacterAnimsFREE.IKHands::Awake()
extern void IKHands_Awake_m24BE66EC320CDE99AA742CBB08C0CE7726324640 (void);
// 0x000006FF System.Void RPGCharacterAnimsFREE.IKHands::OnAnimatorIK(System.Int32)
extern void IKHands_OnAnimatorIK_mB2CB172EA495554C1BF25596DA6D141CAC867F89 (void);
// 0x00000700 System.Void RPGCharacterAnimsFREE.IKHands::BlendIK(System.Boolean,System.Single,System.Single)
extern void IKHands_BlendIK_m66C430F802CDE10212D7210AAEDA106132D02D21 (void);
// 0x00000701 System.Collections.IEnumerator RPGCharacterAnimsFREE.IKHands::_BlendIK(System.Boolean,System.Single,System.Single)
extern void IKHands__BlendIK_mDC923286558A09866E7D917AFE82CC55BB2E23B7 (void);
// 0x00000702 System.Void RPGCharacterAnimsFREE.IKHands::SetIKPause(System.Single)
extern void IKHands_SetIKPause_m940BC10FE16A77A6C809C5B2215706891E0D514D (void);
// 0x00000703 System.Collections.IEnumerator RPGCharacterAnimsFREE.IKHands::_SetIKPause(System.Single)
extern void IKHands__SetIKPause_m650B98DF468CE27BB9FC8086D37C792E1F3DF1D6 (void);
// 0x00000704 System.Void RPGCharacterAnimsFREE.IKHands::SetIKOff()
extern void IKHands_SetIKOff_m104F67ECA8EE480761892A375288F9744C217F09 (void);
// 0x00000705 System.Void RPGCharacterAnimsFREE.IKHands::SetIKOn()
extern void IKHands_SetIKOn_m8A6A1D16C40B2C9C5F16EF9C1CB1AA7F97006D15 (void);
// 0x00000706 System.Void RPGCharacterAnimsFREE.IKHands::GetCurrentWeaponAttachPoint(System.Int32)
extern void IKHands_GetCurrentWeaponAttachPoint_m9DEE496C276A198AEF207DE8BE785E868C468A21 (void);
// 0x00000707 System.Void RPGCharacterAnimsFREE.IKHands::.ctor()
extern void IKHands__ctor_m504AA0CC075396FD83AC935CE2C4A70DACF4BCB7 (void);
// 0x00000708 System.Void RPGCharacterAnimsFREE.IKHands_<_BlendIK>d__13::.ctor(System.Int32)
extern void U3C_BlendIKU3Ed__13__ctor_m35C94D3791AE8D06B52ED6BBCBFDD4AAFBC16F48 (void);
// 0x00000709 System.Void RPGCharacterAnimsFREE.IKHands_<_BlendIK>d__13::System.IDisposable.Dispose()
extern void U3C_BlendIKU3Ed__13_System_IDisposable_Dispose_m4730DC2CB97A015A4EDD99E7776355B94BF1D91E (void);
// 0x0000070A System.Boolean RPGCharacterAnimsFREE.IKHands_<_BlendIK>d__13::MoveNext()
extern void U3C_BlendIKU3Ed__13_MoveNext_m77CF4D9E80111F21D2F673CC9892351E49BB6CCE (void);
// 0x0000070B System.Object RPGCharacterAnimsFREE.IKHands_<_BlendIK>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_BlendIKU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC5831B0D2B977B898CA7768B3B10E0A7B4BC2B6 (void);
// 0x0000070C System.Void RPGCharacterAnimsFREE.IKHands_<_BlendIK>d__13::System.Collections.IEnumerator.Reset()
extern void U3C_BlendIKU3Ed__13_System_Collections_IEnumerator_Reset_m052C9B7CE91AF875C2AE57BAA3A4142C3389BD62 (void);
// 0x0000070D System.Object RPGCharacterAnimsFREE.IKHands_<_BlendIK>d__13::System.Collections.IEnumerator.get_Current()
extern void U3C_BlendIKU3Ed__13_System_Collections_IEnumerator_get_Current_m14BA963C2DD263EDCEA0A40C985CE1ED3A0FDDAC (void);
// 0x0000070E System.Void RPGCharacterAnimsFREE.IKHands_<_SetIKPause>d__15::.ctor(System.Int32)
extern void U3C_SetIKPauseU3Ed__15__ctor_m0B185DEA9D1B4BFDE62FB1B6A48CF51CC5E3E2E4 (void);
// 0x0000070F System.Void RPGCharacterAnimsFREE.IKHands_<_SetIKPause>d__15::System.IDisposable.Dispose()
extern void U3C_SetIKPauseU3Ed__15_System_IDisposable_Dispose_mBFB1D02F353724F6AA5363A4FA126407B469BAE2 (void);
// 0x00000710 System.Boolean RPGCharacterAnimsFREE.IKHands_<_SetIKPause>d__15::MoveNext()
extern void U3C_SetIKPauseU3Ed__15_MoveNext_m7B54D94D8F7AD21C39CDBA25122A23AE925AACA4 (void);
// 0x00000711 System.Object RPGCharacterAnimsFREE.IKHands_<_SetIKPause>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_SetIKPauseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE68F278AC0C3D98EC03ECB1A20B42FDBDE8FBDAA (void);
// 0x00000712 System.Void RPGCharacterAnimsFREE.IKHands_<_SetIKPause>d__15::System.Collections.IEnumerator.Reset()
extern void U3C_SetIKPauseU3Ed__15_System_Collections_IEnumerator_Reset_mC207DEB3C249F714C184CCCB8F235F1494465394 (void);
// 0x00000713 System.Object RPGCharacterAnimsFREE.IKHands_<_SetIKPause>d__15::System.Collections.IEnumerator.get_Current()
extern void U3C_SetIKPauseU3Ed__15_System_Collections_IEnumerator_get_Current_m3CCADB7117BA3267152C40F3B2B71FC7FEECEA6C (void);
// 0x00000714 System.Void RPGCharacterAnimsFREE.RPGCharacterAnimatorEvents::Awake()
extern void RPGCharacterAnimatorEvents_Awake_m93BB9257CA75AAF2FD34D88C122B885984733F8F (void);
// 0x00000715 System.Void RPGCharacterAnimsFREE.RPGCharacterAnimatorEvents::Hit()
extern void RPGCharacterAnimatorEvents_Hit_mF6C6C565057288B9BEDD7E676250BDD8FAC5DE57 (void);
// 0x00000716 System.Void RPGCharacterAnimsFREE.RPGCharacterAnimatorEvents::Shoot()
extern void RPGCharacterAnimatorEvents_Shoot_m8A4B7BF8F598314956B2E4CC13C32FD0CE166729 (void);
// 0x00000717 System.Void RPGCharacterAnimsFREE.RPGCharacterAnimatorEvents::FootR()
extern void RPGCharacterAnimatorEvents_FootR_m6263CBD9FE348EDEC4CED8D3093B6177EDFA8089 (void);
// 0x00000718 System.Void RPGCharacterAnimsFREE.RPGCharacterAnimatorEvents::FootL()
extern void RPGCharacterAnimatorEvents_FootL_m323CB1E89CDD41B3269B06AEAB7FE884EFCE816C (void);
// 0x00000719 System.Void RPGCharacterAnimsFREE.RPGCharacterAnimatorEvents::Land()
extern void RPGCharacterAnimatorEvents_Land_m032649D8B8BDF7FF002F3CC36B50BC9A5E60CC54 (void);
// 0x0000071A System.Void RPGCharacterAnimsFREE.RPGCharacterAnimatorEvents::WeaponSwitch()
extern void RPGCharacterAnimatorEvents_WeaponSwitch_m9D98CD1EB88D9FDCB9CCF4C2D846EE98A1044720 (void);
// 0x0000071B System.Void RPGCharacterAnimsFREE.RPGCharacterAnimatorEvents::OnAnimatorMove()
extern void RPGCharacterAnimatorEvents_OnAnimatorMove_m02C6A23315EFB83C40AF6FFE2794C57B7A23E20F (void);
// 0x0000071C System.Void RPGCharacterAnimsFREE.RPGCharacterAnimatorEvents::.ctor()
extern void RPGCharacterAnimatorEvents__ctor_m04F18169DC3CF8912D37035F5E8BA69087272744 (void);
// 0x0000071D System.Void RPGCharacterAnimsFREE.RPGCharacterController::add_OnLockActions(System.Action)
extern void RPGCharacterController_add_OnLockActions_mA2229D3598D11F08C255EF41FC8500CA937AB7BB (void);
// 0x0000071E System.Void RPGCharacterAnimsFREE.RPGCharacterController::remove_OnLockActions(System.Action)
extern void RPGCharacterController_remove_OnLockActions_mF3AE7AA2B49E8904DFE1D50519A39824AEAC86E1 (void);
// 0x0000071F System.Void RPGCharacterAnimsFREE.RPGCharacterController::add_OnUnlockActions(System.Action)
extern void RPGCharacterController_add_OnUnlockActions_m1EECAE820C02E8FC990B3ECCD8E0539B1FBD948B (void);
// 0x00000720 System.Void RPGCharacterAnimsFREE.RPGCharacterController::remove_OnUnlockActions(System.Action)
extern void RPGCharacterController_remove_OnUnlockActions_mAF0186F3002A40F97D049ED5B50A93677F4247BA (void);
// 0x00000721 System.Void RPGCharacterAnimsFREE.RPGCharacterController::add_OnLockMovement(System.Action)
extern void RPGCharacterController_add_OnLockMovement_m9D3B1AC2A04893974BD69EE499E540148B00DBA4 (void);
// 0x00000722 System.Void RPGCharacterAnimsFREE.RPGCharacterController::remove_OnLockMovement(System.Action)
extern void RPGCharacterController_remove_OnLockMovement_m34BA627CA1E6A98FFB829CD8EBCD581A452850AE (void);
// 0x00000723 System.Void RPGCharacterAnimsFREE.RPGCharacterController::add_OnUnlockMovement(System.Action)
extern void RPGCharacterController_add_OnUnlockMovement_m8A2B698EA14CEC1D67B608AF07ADCB08BB3DF8E8 (void);
// 0x00000724 System.Void RPGCharacterAnimsFREE.RPGCharacterController::remove_OnUnlockMovement(System.Action)
extern void RPGCharacterController_remove_OnUnlockMovement_mCC6B4F8B836B03FA5C2ACA9337A8B6022F2B178B (void);
// 0x00000725 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_canAction()
extern void RPGCharacterController_get_canAction_m2448A8FAFA7AF559BA4690C16EA539C1B0533380 (void);
// 0x00000726 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_canMove()
extern void RPGCharacterController_get_canMove_mF1A2EF8478909EC59AAAC995265AA38D8C0E204B (void);
// 0x00000727 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_canStrafe()
extern void RPGCharacterController_get_canStrafe_m36D5642FE5D0C48920C35C23026E9ADA5CE6B99D (void);
// 0x00000728 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_acquiringGround()
extern void RPGCharacterController_get_acquiringGround_m749D03870BD603279D098A240893A287D6AFB83D (void);
// 0x00000729 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_isDead()
extern void RPGCharacterController_get_isDead_m3771CDA23C33D4E3B471597DAB3AEADDE4314E4A (void);
// 0x0000072A System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_isFalling()
extern void RPGCharacterController_get_isFalling_m2B7F5BD2EBE75F52F11F7A78777A8D737F595EA0 (void);
// 0x0000072B System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_isIdle()
extern void RPGCharacterController_get_isIdle_m75173782D807644BD77B55727CEC7146AB7E37D2 (void);
// 0x0000072C System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_isInjured()
extern void RPGCharacterController_get_isInjured_m05D67A22501AB82907A9B08932A8A775F756B200 (void);
// 0x0000072D System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_isMoving()
extern void RPGCharacterController_get_isMoving_m5A1527E70F3EDDB0D943151CA8E3465199E3148C (void);
// 0x0000072E System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_isNavigating()
extern void RPGCharacterController_get_isNavigating_m33D5BDB9EEBB77BFBCC4BE31E14C1B1692B70B91 (void);
// 0x0000072F System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_isRolling()
extern void RPGCharacterController_get_isRolling_m343AB3B5AB44B16D551AE01182E7B333F5DFF4C6 (void);
// 0x00000730 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_isKnockback()
extern void RPGCharacterController_get_isKnockback_mEAA2656B2156D1F0750B931B8DBC70F2C38C128C (void);
// 0x00000731 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_isStrafing()
extern void RPGCharacterController_get_isStrafing_mBED3DB243D303B8AC77870C1BA5CC3D430918726 (void);
// 0x00000732 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_maintainingGround()
extern void RPGCharacterController_get_maintainingGround_mEC30D218E90FB337E09ED3102DEA133A77582501 (void);
// 0x00000733 UnityEngine.Vector3 RPGCharacterAnimsFREE.RPGCharacterController::get_moveInput()
extern void RPGCharacterController_get_moveInput_m6AA5E5188EC030BB09F7A0C5D41E249F2D8F7F28 (void);
// 0x00000734 UnityEngine.Vector3 RPGCharacterAnimsFREE.RPGCharacterController::get_aimInput()
extern void RPGCharacterController_get_aimInput_mB29E8C79EB375542F4A793814F73FA9B8AEC0486 (void);
// 0x00000735 UnityEngine.Vector3 RPGCharacterAnimsFREE.RPGCharacterController::get_jumpInput()
extern void RPGCharacterController_get_jumpInput_m289A1A0836E6D14B78F5A33C6D479AD3754A774B (void);
// 0x00000736 UnityEngine.Vector3 RPGCharacterAnimsFREE.RPGCharacterController::get_cameraRelativeInput()
extern void RPGCharacterController_get_cameraRelativeInput_m7E5A606F966794EA7B0E72F11BFD9B20CC89BD2E (void);
// 0x00000737 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_hasRightWeapon()
extern void RPGCharacterController_get_hasRightWeapon_m5ADAAF533CFDD323804669D3137E463048B18E53 (void);
// 0x00000738 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_hasLeftWeapon()
extern void RPGCharacterController_get_hasLeftWeapon_mE8A8859449C1E8EAE77DB94B7E23EEA966E7BA1F (void);
// 0x00000739 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_hasTwoHandedWeapon()
extern void RPGCharacterController_get_hasTwoHandedWeapon_m6CE8068976612CCD755749C58D0F0CEA4830DC23 (void);
// 0x0000073A System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::get_hasNoWeapon()
extern void RPGCharacterController_get_hasNoWeapon_mBBD56D0A0D7172E478A34B164AB6887B1025B62C (void);
// 0x0000073B System.Void RPGCharacterAnimsFREE.RPGCharacterController::Awake()
extern void RPGCharacterController_Awake_mAAD4E45BB08B9C5857B43B669AC66C7C19A0E1C8 (void);
// 0x0000073C System.Void RPGCharacterAnimsFREE.RPGCharacterController::SetHandler(System.String,RPGCharacterAnimsFREE.Actions.IActionHandler)
extern void RPGCharacterController_SetHandler_m9116693F4A2E5F51B357D05CF23FEAA59DD7353B (void);
// 0x0000073D RPGCharacterAnimsFREE.Actions.IActionHandler RPGCharacterAnimsFREE.RPGCharacterController::GetHandler(System.String)
extern void RPGCharacterController_GetHandler_m5973E46478E4CEED5F626E2052804992E39C9053 (void);
// 0x0000073E System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::HandlerExists(System.String)
extern void RPGCharacterController_HandlerExists_m63EA91C5629427E13F369F665D6A9367AEB2FC85 (void);
// 0x0000073F System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::IsActive(System.String)
extern void RPGCharacterController_IsActive_m130306AAC8F5EC95A48F81ADB3609FEE9BAEDABB (void);
// 0x00000740 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::CanStartAction(System.String)
extern void RPGCharacterController_CanStartAction_mB2D54D4669237ECA6AE68E437D3B65A7FBA25D4E (void);
// 0x00000741 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController::CanEndAction(System.String)
extern void RPGCharacterController_CanEndAction_m3F61F5A65376F8855E9A5415860E6866AEF2DC87 (void);
// 0x00000742 System.Void RPGCharacterAnimsFREE.RPGCharacterController::StartAction(System.String,System.Object)
extern void RPGCharacterController_StartAction_mE7EDEAD7102BC1316F154CD27A07B3C5D388CCF3 (void);
// 0x00000743 System.Void RPGCharacterAnimsFREE.RPGCharacterController::EndAction(System.String)
extern void RPGCharacterController_EndAction_m885A0B955D5CF4A8534A825F764CFC92E9467666 (void);
// 0x00000744 System.Void RPGCharacterAnimsFREE.RPGCharacterController::LateUpdate()
extern void RPGCharacterController_LateUpdate_m3D14C58EBF1871360382FE30A2DBBD2FEB6F7D92 (void);
// 0x00000745 System.Void RPGCharacterAnimsFREE.RPGCharacterController::SetMoveInput(UnityEngine.Vector3)
extern void RPGCharacterController_SetMoveInput_m97638EDFEDF6B267DD16287FB77F18FAC86A8E82 (void);
// 0x00000746 System.Void RPGCharacterAnimsFREE.RPGCharacterController::SetAimInput(UnityEngine.Vector3)
extern void RPGCharacterController_SetAimInput_mBF6096273CB4EC9B28609CBC7415F89893B02A38 (void);
// 0x00000747 System.Void RPGCharacterAnimsFREE.RPGCharacterController::SetJumpInput(UnityEngine.Vector3)
extern void RPGCharacterController_SetJumpInput_m0C9B4CE96C58726A6F4E7A9195B3CC6CC1B70F25 (void);
// 0x00000748 System.Void RPGCharacterAnimsFREE.RPGCharacterController::DiveRoll(System.Int32)
extern void RPGCharacterController_DiveRoll_m4F1AA9BC365DFB813ACDA618E6D33B6AB56386A8 (void);
// 0x00000749 System.Void RPGCharacterAnimsFREE.RPGCharacterController::Knockback(System.Int32)
extern void RPGCharacterController_Knockback_mE66E0C0154CA968D01BBB2ED1C5B9E8CC9B9BCC4 (void);
// 0x0000074A System.Void RPGCharacterAnimsFREE.RPGCharacterController::Attack(System.Int32,System.Int32,System.Int32,System.Single)
extern void RPGCharacterController_Attack_m1BD352F348904EFB89774656924398ABD308E897 (void);
// 0x0000074B System.Void RPGCharacterAnimsFREE.RPGCharacterController::RunningAttack(System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void RPGCharacterController_RunningAttack_mA1BE49BAA87A3F65C2062CAA36DF908500635F88 (void);
// 0x0000074C System.Void RPGCharacterAnimsFREE.RPGCharacterController::StartStrafe()
extern void RPGCharacterController_StartStrafe_m0A24FB4CA4AB08EBEA8E7EF6E5B43A9237644D61 (void);
// 0x0000074D System.Void RPGCharacterAnimsFREE.RPGCharacterController::EndStrafe()
extern void RPGCharacterController_EndStrafe_m76B25FFACC1AC638412A10CCE36A53E6E2F83D60 (void);
// 0x0000074E System.Void RPGCharacterAnimsFREE.RPGCharacterController::GetHit(System.Int32)
extern void RPGCharacterController_GetHit_m78DD096282A23D86CDCA0ED20F295024FB3156A7 (void);
// 0x0000074F System.Void RPGCharacterAnimsFREE.RPGCharacterController::Death()
extern void RPGCharacterController_Death_mED7178D069DAEE9387937D04DF7E5CE281706C32 (void);
// 0x00000750 System.Void RPGCharacterAnimsFREE.RPGCharacterController::Revive()
extern void RPGCharacterController_Revive_mDB73235F185EF6DF8C16A1DBC58507488728915B (void);
// 0x00000751 System.Void RPGCharacterAnimsFREE.RPGCharacterController::StartInjured()
extern void RPGCharacterController_StartInjured_m7A5817839A46F5FDC86C0D83925FFE038E74EB34 (void);
// 0x00000752 System.Void RPGCharacterAnimsFREE.RPGCharacterController::EndInjured()
extern void RPGCharacterController_EndInjured_m6F8E7E6E122377741ACE3514DF85E5498408FBBB (void);
// 0x00000753 UnityEngine.GameObject RPGCharacterAnimsFREE.RPGCharacterController::GetAnimatorTarget()
extern void RPGCharacterController_GetAnimatorTarget_m27C62C2B59FA42AEA2EBB01BC3039EC53634EAF8 (void);
// 0x00000754 System.Collections.IEnumerator RPGCharacterAnimsFREE.RPGCharacterController::_GetCurrentAnimationLength()
extern void RPGCharacterController__GetCurrentAnimationLength_mD229559665FA6AC1F35612ACDB196C62C09C7F68 (void);
// 0x00000755 System.Void RPGCharacterAnimsFREE.RPGCharacterController::Lock(System.Boolean,System.Boolean,System.Boolean,System.Single,System.Single)
extern void RPGCharacterController_Lock_mD15D9B5C5E2BA2D30A010C9E255EDE6EAA5FF1AF (void);
// 0x00000756 System.Collections.IEnumerator RPGCharacterAnimsFREE.RPGCharacterController::_Lock(System.Boolean,System.Boolean,System.Boolean,System.Single,System.Single)
extern void RPGCharacterController__Lock_mE9C9D7E89F7BB32274C0B89CC747FAC80868BC62 (void);
// 0x00000757 System.Void RPGCharacterAnimsFREE.RPGCharacterController::Unlock(System.Boolean,System.Boolean)
extern void RPGCharacterController_Unlock_m20DDA6D8F0CF68AA113DE6F8F6D67F19E0E0BD66 (void);
// 0x00000758 System.Void RPGCharacterAnimsFREE.RPGCharacterController::SetIKOff()
extern void RPGCharacterController_SetIKOff_mE603631F11507DC946E15BC3197C1E756D3DE964 (void);
// 0x00000759 System.Void RPGCharacterAnimsFREE.RPGCharacterController::SetIKOn()
extern void RPGCharacterController_SetIKOn_mAFC3A260B1F24DAB794F1BB37613A971B4827D75 (void);
// 0x0000075A System.Void RPGCharacterAnimsFREE.RPGCharacterController::SetIKPause(System.Single)
extern void RPGCharacterController_SetIKPause_m3B3CE1A11B39CA2399CC1D1DB212F5D75D2594FE (void);
// 0x0000075B System.Void RPGCharacterAnimsFREE.RPGCharacterController::SetAnimatorTrigger(RPGCharacterAnimsFREE.AnimatorTrigger)
extern void RPGCharacterController_SetAnimatorTrigger_m266B287DB456F11A211BCB5BEF7FC9F36DDC35FF (void);
// 0x0000075C System.Void RPGCharacterAnimsFREE.RPGCharacterController::LegacySetAnimationTrigger(System.String)
extern void RPGCharacterController_LegacySetAnimationTrigger_mA9641480FF05F1524F3489391F20A5C7B5D872DC (void);
// 0x0000075D System.Void RPGCharacterAnimsFREE.RPGCharacterController::AnimatorDebug()
extern void RPGCharacterController_AnimatorDebug_mB88782186EEE035871F025968AE705BC27ECC7DC (void);
// 0x0000075E System.Void RPGCharacterAnimsFREE.RPGCharacterController::ControllerDebug()
extern void RPGCharacterController_ControllerDebug_m832587564677230B3A58F0D6524684E5C662A5BA (void);
// 0x0000075F System.Void RPGCharacterAnimsFREE.RPGCharacterController::.ctor()
extern void RPGCharacterController__ctor_mEE10A2E0B01F4553EDE74FC595DE8065D177718E (void);
// 0x00000760 System.Void RPGCharacterAnimsFREE.RPGCharacterController_<_GetCurrentAnimationLength>d__95::.ctor(System.Int32)
extern void U3C_GetCurrentAnimationLengthU3Ed__95__ctor_m820E1614FA3BF119F4E6D408FB35B7C5C518248C (void);
// 0x00000761 System.Void RPGCharacterAnimsFREE.RPGCharacterController_<_GetCurrentAnimationLength>d__95::System.IDisposable.Dispose()
extern void U3C_GetCurrentAnimationLengthU3Ed__95_System_IDisposable_Dispose_mAE6F7CE54CD62FA8B0AA7E229A810594DFFD0BBF (void);
// 0x00000762 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController_<_GetCurrentAnimationLength>d__95::MoveNext()
extern void U3C_GetCurrentAnimationLengthU3Ed__95_MoveNext_mD7E61D8E5510BC7083DE70E02508BAC13247D471 (void);
// 0x00000763 System.Object RPGCharacterAnimsFREE.RPGCharacterController_<_GetCurrentAnimationLength>d__95::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_GetCurrentAnimationLengthU3Ed__95_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m075F873D32C075CF5005C95D402DBE4AA5E4A49F (void);
// 0x00000764 System.Void RPGCharacterAnimsFREE.RPGCharacterController_<_GetCurrentAnimationLength>d__95::System.Collections.IEnumerator.Reset()
extern void U3C_GetCurrentAnimationLengthU3Ed__95_System_Collections_IEnumerator_Reset_mC8A64041A2D031C6A64593FD5B09F37320CF929B (void);
// 0x00000765 System.Object RPGCharacterAnimsFREE.RPGCharacterController_<_GetCurrentAnimationLength>d__95::System.Collections.IEnumerator.get_Current()
extern void U3C_GetCurrentAnimationLengthU3Ed__95_System_Collections_IEnumerator_get_Current_mE452D06B222A0149C226E2C402206483D92AF08D (void);
// 0x00000766 System.Void RPGCharacterAnimsFREE.RPGCharacterController_<_Lock>d__97::.ctor(System.Int32)
extern void U3C_LockU3Ed__97__ctor_m858C96D1D1601724CAC79BAAB47BEB6B4DAE2857 (void);
// 0x00000767 System.Void RPGCharacterAnimsFREE.RPGCharacterController_<_Lock>d__97::System.IDisposable.Dispose()
extern void U3C_LockU3Ed__97_System_IDisposable_Dispose_m446A96C040584FAC614CB318B52342DD023DDE08 (void);
// 0x00000768 System.Boolean RPGCharacterAnimsFREE.RPGCharacterController_<_Lock>d__97::MoveNext()
extern void U3C_LockU3Ed__97_MoveNext_m1DE3B9EF500AFDE6C40910ACC0AC93E7A191750A (void);
// 0x00000769 System.Object RPGCharacterAnimsFREE.RPGCharacterController_<_Lock>d__97::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_LockU3Ed__97_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC55693EFED731C44A826ABB2BA3828414978EF62 (void);
// 0x0000076A System.Void RPGCharacterAnimsFREE.RPGCharacterController_<_Lock>d__97::System.Collections.IEnumerator.Reset()
extern void U3C_LockU3Ed__97_System_Collections_IEnumerator_Reset_mA81A2532FF5357D390B2E33E7A583C760A0FD550 (void);
// 0x0000076B System.Object RPGCharacterAnimsFREE.RPGCharacterController_<_Lock>d__97::System.Collections.IEnumerator.get_Current()
extern void U3C_LockU3Ed__97_System_Collections_IEnumerator_get_Current_m8455282670C26C0A5E541D4A9284A859286F3C8D (void);
// 0x0000076C System.Void RPGCharacterAnimsFREE.RPGCharacterController_<>c::.cctor()
extern void U3CU3Ec__cctor_m0723C99271C9596F69E826A059174115A3BB7D96 (void);
// 0x0000076D System.Void RPGCharacterAnimsFREE.RPGCharacterController_<>c::.ctor()
extern void U3CU3Ec__ctor_mEC97A3E7DFE4F6395CEFB2ADEAB329A11013659C (void);
// 0x0000076E System.Void RPGCharacterAnimsFREE.RPGCharacterController_<>c::<.ctor>b__106_0()
extern void U3CU3Ec_U3C_ctorU3Eb__106_0_mF4FCA891562CB83A1BE082382A767980766B9BE8 (void);
// 0x0000076F System.Void RPGCharacterAnimsFREE.RPGCharacterController_<>c::<.ctor>b__106_1()
extern void U3CU3Ec_U3C_ctorU3Eb__106_1_m33EBA404EA830A801D26D2B6FE229E4BC8F24F2E (void);
// 0x00000770 System.Void RPGCharacterAnimsFREE.RPGCharacterController_<>c::<.ctor>b__106_2()
extern void U3CU3Ec_U3C_ctorU3Eb__106_2_m61D85B7D4FF42825A16602B77680882225FE9788 (void);
// 0x00000771 System.Void RPGCharacterAnimsFREE.RPGCharacterController_<>c::<.ctor>b__106_3()
extern void U3CU3Ec_U3C_ctorU3Eb__106_3_m03CA1E5F177F63BDB7075C232848924D26CC931C (void);
// 0x00000772 System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::Awake()
extern void RPGCharacterInputController_Awake_m88C7BB4984C505C1CAB24E369622EAB097BC21D1 (void);
// 0x00000773 System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::Update()
extern void RPGCharacterInputController_Update_mE713950642286938915194EDE77ABB787B984AC7 (void);
// 0x00000774 System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::PauseInput(System.Single)
extern void RPGCharacterInputController_PauseInput_m225B8D30C020B8D84CD40C41C9A0921F44AA047B (void);
// 0x00000775 System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::Inputs()
extern void RPGCharacterInputController_Inputs_m725873F9F947A3A936881EEBFB3B5BABEBDEB9AC (void);
// 0x00000776 System.Boolean RPGCharacterAnimsFREE.RPGCharacterInputController::HasMoveInput()
extern void RPGCharacterInputController_HasMoveInput_mD39B73192ED7DEBE118A88C64944BABA98E9060C (void);
// 0x00000777 System.Boolean RPGCharacterAnimsFREE.RPGCharacterInputController::HasAimInput()
extern void RPGCharacterInputController_HasAimInput_m51C0202C2DAFCF3143BFC48C6CD84A7958975289 (void);
// 0x00000778 System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::Moving()
extern void RPGCharacterInputController_Moving_m109FD4A29578F751BE030FE5140E0C5D16C12397 (void);
// 0x00000779 System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::Rolling()
extern void RPGCharacterInputController_Rolling_m3EFD8E6ABEFB952C5D96D65BAF521D83E272BAAD (void);
// 0x0000077A System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::Strafing()
extern void RPGCharacterInputController_Strafing_mEE037B343A3E34ECDF9FA2EB919D2F7D86CC7CDA (void);
// 0x0000077B System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::Attacking()
extern void RPGCharacterInputController_Attacking_m1F42AFD204ED9730B076F6CD216B1087196666D7 (void);
// 0x0000077C System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::Damage()
extern void RPGCharacterInputController_Damage_mE2AA7326CFEED2F8457078E112736E60D3FD66BE (void);
// 0x0000077D System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::SwitchWeapons()
extern void RPGCharacterInputController_SwitchWeapons_mD71B0D8F35E592933A71066FCF7DF0F36A34729C (void);
// 0x0000077E System.Void RPGCharacterAnimsFREE.RPGCharacterInputController::.ctor()
extern void RPGCharacterInputController__ctor_mF4BC171F71CE94AD516054DA29FE05BFDC6983B3 (void);
// 0x0000077F UnityEngine.Vector3 RPGCharacterAnimsFREE.RPGCharacterMovementController::get_lookDirection()
extern void RPGCharacterMovementController_get_lookDirection_m063925638F4666CA88AE6E7CCE64BA415276B65E (void);
// 0x00000780 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::set_lookDirection(UnityEngine.Vector3)
extern void RPGCharacterMovementController_set_lookDirection_m9469D5CA2F3606C0BD50968AB5C7328B2A692929 (void);
// 0x00000781 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Awake()
extern void RPGCharacterMovementController_Awake_m3E92C25FECCDDDBB543430ADB251B7358A339E69 (void);
// 0x00000782 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Start()
extern void RPGCharacterMovementController_Start_m5F5EBEC8AB067EE13C5A98A90AFA6E34F86D5F3A (void);
// 0x00000783 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Update()
extern void RPGCharacterMovementController_Update_mE750A8552DFD8F18795BE23617F0AB0D025FD77C (void);
// 0x00000784 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::EarlyGlobalSuperUpdate()
extern void RPGCharacterMovementController_EarlyGlobalSuperUpdate_m1258850E717C2F7F80B88319CB87BB2D28893D7B (void);
// 0x00000785 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::LateGlobalSuperUpdate()
extern void RPGCharacterMovementController_LateGlobalSuperUpdate_mF4EB679227E3FD5AB2BC7330A68FA22A36D0C2C0 (void);
// 0x00000786 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Idle_EnterState()
extern void RPGCharacterMovementController_Idle_EnterState_m5FC22CB2D2B59F3223A269B768237F6C44C140E9 (void);
// 0x00000787 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Idle_SuperUpdate()
extern void RPGCharacterMovementController_Idle_SuperUpdate_m104FC1874037BBDA3BAC0C12EC956379728BFEED (void);
// 0x00000788 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Move_SuperUpdate()
extern void RPGCharacterMovementController_Move_SuperUpdate_mE8289D3C01B9AED1C01CA457A886417D01A8E9B8 (void);
// 0x00000789 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Jump_EnterState()
extern void RPGCharacterMovementController_Jump_EnterState_m33AE1E8660CD1410A818CBB43152F7616168F956 (void);
// 0x0000078A System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Jump_SuperUpdate()
extern void RPGCharacterMovementController_Jump_SuperUpdate_mA656509E44851A2EB3088270F55BDB31A2FF942D (void);
// 0x0000078B System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Fall_EnterState()
extern void RPGCharacterMovementController_Fall_EnterState_m454558B2670EFC79387F54441B6893C0755511B7 (void);
// 0x0000078C System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Fall_SuperUpdate()
extern void RPGCharacterMovementController_Fall_SuperUpdate_mDD168651DB8D21AA961E9861D20F06EAF375CD04 (void);
// 0x0000078D System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Fall_ExitState()
extern void RPGCharacterMovementController_Fall_ExitState_m1BDA6B72420BAC493BAFC2A01B01AF06CB3F7554 (void);
// 0x0000078E System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::DiveRoll_EnterState()
extern void RPGCharacterMovementController_DiveRoll_EnterState_m7037A844EBE2BC33E9AC370AF71803166379342E (void);
// 0x0000078F System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::DiveRoll_SuperUpdate()
extern void RPGCharacterMovementController_DiveRoll_SuperUpdate_mB4EA80D5A5253DDC0D308FA05765878A537F397F (void);
// 0x00000790 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Knockback_EnterState()
extern void RPGCharacterMovementController_Knockback_EnterState_m465DC6B4EB9A2A486ED0F36F3C093C5281BE1EFF (void);
// 0x00000791 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Knockdown_EnterState()
extern void RPGCharacterMovementController_Knockdown_EnterState_m9F7E93F5A81D8E7E32701A18138C538E174F9104 (void);
// 0x00000792 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::Land()
extern void RPGCharacterMovementController_Land_mEE32343C8752029F587177A722D4B6CC43577531 (void);
// 0x00000793 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::RotateGravity(UnityEngine.Vector3)
extern void RPGCharacterMovementController_RotateGravity_mE89BEF0301E7A4C59409FBE6A54656036D427511 (void);
// 0x00000794 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::RotateTowardsMovementDir()
extern void RPGCharacterMovementController_RotateTowardsMovementDir_m4F13CBC4965303597DF26457755B293981CE374C (void);
// 0x00000795 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::RotateTowardsTarget(UnityEngine.Vector3)
extern void RPGCharacterMovementController_RotateTowardsTarget_m52595AD70988C697765CD52086B6297A6331926B (void);
// 0x00000796 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::KnockbackForce(UnityEngine.Vector3,System.Single,System.Single)
extern void RPGCharacterMovementController_KnockbackForce_m81BC5D0F2C73F298A619B3DB727DBA27CE7141A7 (void);
// 0x00000797 System.Collections.IEnumerator RPGCharacterAnimsFREE.RPGCharacterMovementController::_KnockbackForce(UnityEngine.Vector3,System.Single,System.Single)
extern void RPGCharacterMovementController__KnockbackForce_m5374C2961CE16214C46F5D6AD4377C463DD44F6B (void);
// 0x00000798 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::LockMovement()
extern void RPGCharacterMovementController_LockMovement_m3EE25EF7A5E5694E2634B762FDC7817461923CF1 (void);
// 0x00000799 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::UnlockMovement()
extern void RPGCharacterMovementController_UnlockMovement_mE29B65F100F03DA3F0A7486EB69AD6F4D2A10A36 (void);
// 0x0000079A System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::AnimatorMove(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void RPGCharacterMovementController_AnimatorMove_m5BFBB21BCEF30BB0B95A83E97881CE28F86AF9D8 (void);
// 0x0000079B System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::IdleOnceAfterMoveUnlock()
extern void RPGCharacterMovementController_IdleOnceAfterMoveUnlock_m47889CB628E046A6604BBDE1F2B0881617D9B74C (void);
// 0x0000079C System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController::.ctor()
extern void RPGCharacterMovementController__ctor_m1B9D84D3EA692AF132EF3B877862CF62539F3F81 (void);
// 0x0000079D System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController_<>c::.cctor()
extern void U3CU3Ec__cctor_m159ED8142E888F7E00041B2010434307B738624A (void);
// 0x0000079E System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController_<>c::.ctor()
extern void U3CU3Ec__ctor_m91D938C59956AFA6ED0E0B89CA70E41EBAC2B0C4 (void);
// 0x0000079F System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController_<>c::<Awake>b__27_0()
extern void U3CU3Ec_U3CAwakeU3Eb__27_0_mD5453EEF8CEA57D9BD4FA4711DDBB8ED12964E99 (void);
// 0x000007A0 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController_<>c::<Awake>b__27_1()
extern void U3CU3Ec_U3CAwakeU3Eb__27_1_mAEF85EE523DE23F87F45F9C11195486B038CE616 (void);
// 0x000007A1 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController_<>c::<Awake>b__27_2()
extern void U3CU3Ec_U3CAwakeU3Eb__27_2_m595CFCB4564A7F08DEBBAD36035B66B34E215C49 (void);
// 0x000007A2 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController_<>c::<Awake>b__27_3()
extern void U3CU3Ec_U3CAwakeU3Eb__27_3_m9F87991FB9E2FB73899F78F2334946A8CC4E9A9A (void);
// 0x000007A3 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController_<_KnockbackForce>d__49::.ctor(System.Int32)
extern void U3C_KnockbackForceU3Ed__49__ctor_mC26880244CCC211FBC58FF9D772E305A2DEFC4FD (void);
// 0x000007A4 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController_<_KnockbackForce>d__49::System.IDisposable.Dispose()
extern void U3C_KnockbackForceU3Ed__49_System_IDisposable_Dispose_m6F93F7266248AA297DCCDB8D24C46B6B7BF64E9A (void);
// 0x000007A5 System.Boolean RPGCharacterAnimsFREE.RPGCharacterMovementController_<_KnockbackForce>d__49::MoveNext()
extern void U3C_KnockbackForceU3Ed__49_MoveNext_m1EE1A8FE2C8A6031EADA494FE1B3A12A9346946A (void);
// 0x000007A6 System.Object RPGCharacterAnimsFREE.RPGCharacterMovementController_<_KnockbackForce>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_KnockbackForceU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF48814913A2B0C28A945B01958A2B714E16F397D (void);
// 0x000007A7 System.Void RPGCharacterAnimsFREE.RPGCharacterMovementController_<_KnockbackForce>d__49::System.Collections.IEnumerator.Reset()
extern void U3C_KnockbackForceU3Ed__49_System_Collections_IEnumerator_Reset_m76B57B78E1A54B8B0C25D0D0CACCE6472CD19A6A (void);
// 0x000007A8 System.Object RPGCharacterAnimsFREE.RPGCharacterMovementController_<_KnockbackForce>d__49::System.Collections.IEnumerator.get_Current()
extern void U3C_KnockbackForceU3Ed__49_System_Collections_IEnumerator_get_Current_m2E6B145E5970B0197E9F3BACDDDF6F6894DB4856 (void);
// 0x000007A9 System.Void RPGCharacterAnimsFREE.RPGCharacterNavigationController::Awake()
extern void RPGCharacterNavigationController_Awake_m4774C7E1BFF348302FC56FEA09444D129803F0F2 (void);
// 0x000007AA System.Void RPGCharacterAnimsFREE.RPGCharacterNavigationController::Start()
extern void RPGCharacterNavigationController_Start_mF66E228D321AD920B11AB3081C46AEECFB660AEE (void);
// 0x000007AB System.Void RPGCharacterAnimsFREE.RPGCharacterNavigationController::Update()
extern void RPGCharacterNavigationController_Update_m719FAACCAFED8E998E0A6D2D93C5CE6CB3DD1668 (void);
// 0x000007AC System.Void RPGCharacterAnimsFREE.RPGCharacterNavigationController::MeshNavToPoint(UnityEngine.Vector3)
extern void RPGCharacterNavigationController_MeshNavToPoint_mEA52E62F4AB475BC1E0A673267041739AB5EF169 (void);
// 0x000007AD System.Void RPGCharacterAnimsFREE.RPGCharacterNavigationController::StopNavigating()
extern void RPGCharacterNavigationController_StopNavigating_m2D0AC4FFEA41FC1FDF7AAB286B35EDB45391D8FA (void);
// 0x000007AE System.Void RPGCharacterAnimsFREE.RPGCharacterNavigationController::RotateTowardsMovementDir()
extern void RPGCharacterNavigationController_RotateTowardsMovementDir_mDE21029AD6E36B6890B0FDB71714913D001CE963 (void);
// 0x000007AF System.Void RPGCharacterAnimsFREE.RPGCharacterNavigationController::.ctor()
extern void RPGCharacterNavigationController__ctor_m41A486C2FF90611822BA7DAF461823524D84F956 (void);
// 0x000007B0 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::Awake()
extern void RPGCharacterWeaponController_Awake_m3EB2F9F68A368A24E16D21697F229FA851C7925B (void);
// 0x000007B1 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::Start()
extern void RPGCharacterWeaponController_Start_m58027A54865BC735B18C8D2D7226784BF362FAC9 (void);
// 0x000007B2 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::AddCallback(System.Action)
extern void RPGCharacterWeaponController_AddCallback_mB1FBF08DC56DCB99EB3D152C5DEEE841DD263D52 (void);
// 0x000007B3 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::UnsheathWeapon(System.Int32,System.Boolean)
extern void RPGCharacterWeaponController_UnsheathWeapon_m7B48E75415C2AF55BA9ABCCB64E10277AF67E609 (void);
// 0x000007B4 System.Collections.IEnumerator RPGCharacterAnimsFREE.RPGCharacterWeaponController::_UnSheathWeapon(System.Int32)
extern void RPGCharacterWeaponController__UnSheathWeapon_mCBE1E8D97A50B27043582606A59D3C2E84DB1167 (void);
// 0x000007B5 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::SheathWeapon(System.Int32,System.Int32,System.Boolean)
extern void RPGCharacterWeaponController_SheathWeapon_m8A26AC6ECD6E98B0AB0D295F8B9F24538ECBCA69 (void);
// 0x000007B6 System.Collections.IEnumerator RPGCharacterAnimsFREE.RPGCharacterWeaponController::_SheathWeapon(System.Int32,System.Int32)
extern void RPGCharacterWeaponController__SheathWeapon_mF34A3E18E72E697D7E34D6DE56AAB85E883BEB3B (void);
// 0x000007B7 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::InstantWeaponSwitch(System.Int32)
extern void RPGCharacterWeaponController_InstantWeaponSwitch_mA7B3B4C922B14D24617F3DECECCE3EAD3802EE75 (void);
// 0x000007B8 System.Collections.IEnumerator RPGCharacterAnimsFREE.RPGCharacterWeaponController::_InstantWeaponSwitch(System.Int32)
extern void RPGCharacterWeaponController__InstantWeaponSwitch_m6CFAE0E20135EB42F641F2ABFC0B22F9B21B6BC1 (void);
// 0x000007B9 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::DoWeaponSwitch(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void RPGCharacterWeaponController_DoWeaponSwitch_mC27544BD18561AD99CC3D93A145C172C14E38D85 (void);
// 0x000007BA System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::SetAnimator(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void RPGCharacterWeaponController_SetAnimator_mC1BC8001A763D69AE30CDEDFFE94B69AD1D17887 (void);
// 0x000007BB System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::WeaponSwitch()
extern void RPGCharacterWeaponController_WeaponSwitch_mA7479BAA2BC72D6591A6B70940B701E69301DBB2 (void);
// 0x000007BC System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::SafeSetVisibility(UnityEngine.GameObject,System.Boolean)
extern void RPGCharacterWeaponController_SafeSetVisibility_m9A16D26CE59C1B89E4231D182BB96EA50E485EF9 (void);
// 0x000007BD System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::HideAllWeapons()
extern void RPGCharacterWeaponController_HideAllWeapons_m4A5BDF0597E52CE74C43E3126B3F26793BD8EAFB (void);
// 0x000007BE System.Collections.IEnumerator RPGCharacterAnimsFREE.RPGCharacterWeaponController::_HideAllWeapons(System.Boolean,System.Boolean)
extern void RPGCharacterWeaponController__HideAllWeapons_m5F559824E2D7B73AD31F853C3388B06EDD867D8D (void);
// 0x000007BF System.Collections.IEnumerator RPGCharacterAnimsFREE.RPGCharacterWeaponController::_WeaponVisibility(System.Int32,System.Boolean,System.Boolean)
extern void RPGCharacterWeaponController__WeaponVisibility_m31582BCD48FA5682C44B494793DC7FD64C49DA68 (void);
// 0x000007C0 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::SyncWeaponVisibility()
extern void RPGCharacterWeaponController_SyncWeaponVisibility_m46355FFFFF4CD7FF8EC826C24E8F92C45929828E (void);
// 0x000007C1 System.Collections.IEnumerator RPGCharacterAnimsFREE.RPGCharacterWeaponController::_SyncWeaponVisibility()
extern void RPGCharacterWeaponController__SyncWeaponVisibility_mCF49A056C5EC851FE38BEAA0CBA3822C2832C741 (void);
// 0x000007C2 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController::.ctor()
extern void RPGCharacterWeaponController__ctor_m1D3D183B3F07BF4729AEAAB7D9D710870129A4C4 (void);
// 0x000007C3 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_UnSheathWeapon>d__9::.ctor(System.Int32)
extern void U3C_UnSheathWeaponU3Ed__9__ctor_m35EF04213975D5024096F3C5203F3F3E3CF563DB (void);
// 0x000007C4 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_UnSheathWeapon>d__9::System.IDisposable.Dispose()
extern void U3C_UnSheathWeaponU3Ed__9_System_IDisposable_Dispose_mFFDB7C9AADCAD47507EBCA608C0125F97449F64F (void);
// 0x000007C5 System.Boolean RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_UnSheathWeapon>d__9::MoveNext()
extern void U3C_UnSheathWeaponU3Ed__9_MoveNext_mD9ABF285126C71C418C045E781D49AE3AA024DFE (void);
// 0x000007C6 System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_UnSheathWeapon>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_UnSheathWeaponU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13824AADAF17EE515BE9818C86AD40D0EEE6B853 (void);
// 0x000007C7 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_UnSheathWeapon>d__9::System.Collections.IEnumerator.Reset()
extern void U3C_UnSheathWeaponU3Ed__9_System_Collections_IEnumerator_Reset_mF8A2AE8748E3D770E7204F5242535345291B8537 (void);
// 0x000007C8 System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_UnSheathWeapon>d__9::System.Collections.IEnumerator.get_Current()
extern void U3C_UnSheathWeaponU3Ed__9_System_Collections_IEnumerator_get_Current_m00675F56A30AFFA495A568595D6D46DFD88DA802 (void);
// 0x000007C9 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SheathWeapon>d__11::.ctor(System.Int32)
extern void U3C_SheathWeaponU3Ed__11__ctor_mD8C60AD3822076D0195EE8D0014484C5E329D5A0 (void);
// 0x000007CA System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SheathWeapon>d__11::System.IDisposable.Dispose()
extern void U3C_SheathWeaponU3Ed__11_System_IDisposable_Dispose_mCECFAA1D79B9B4AAF1989E8966E9EEEE0A562FE3 (void);
// 0x000007CB System.Boolean RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SheathWeapon>d__11::MoveNext()
extern void U3C_SheathWeaponU3Ed__11_MoveNext_m5302FC3A8B7A531E9FF3A2D4FEA9C2216F675ACA (void);
// 0x000007CC System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SheathWeapon>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_SheathWeaponU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99DA089381A786F95D08201701BBE86B3F423279 (void);
// 0x000007CD System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SheathWeapon>d__11::System.Collections.IEnumerator.Reset()
extern void U3C_SheathWeaponU3Ed__11_System_Collections_IEnumerator_Reset_mF316EA696832C83D880B8F4501148582BCC7A117 (void);
// 0x000007CE System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SheathWeapon>d__11::System.Collections.IEnumerator.get_Current()
extern void U3C_SheathWeaponU3Ed__11_System_Collections_IEnumerator_get_Current_m2A5C387020C654BB643BB48A47F48295C59056B3 (void);
// 0x000007CF System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_InstantWeaponSwitch>d__13::.ctor(System.Int32)
extern void U3C_InstantWeaponSwitchU3Ed__13__ctor_m054A86DAF9D6CE1731E3C81D09562B4BC53F3B0B (void);
// 0x000007D0 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_InstantWeaponSwitch>d__13::System.IDisposable.Dispose()
extern void U3C_InstantWeaponSwitchU3Ed__13_System_IDisposable_Dispose_mC08D1CCB13EC4395A8659AA769ACC4FDE16FB574 (void);
// 0x000007D1 System.Boolean RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_InstantWeaponSwitch>d__13::MoveNext()
extern void U3C_InstantWeaponSwitchU3Ed__13_MoveNext_mCCA604506384190AC09F366BC379633025811291 (void);
// 0x000007D2 System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_InstantWeaponSwitch>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_InstantWeaponSwitchU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC58A3DFADCCFCACEF3619D890E9859AD8ADEE088 (void);
// 0x000007D3 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_InstantWeaponSwitch>d__13::System.Collections.IEnumerator.Reset()
extern void U3C_InstantWeaponSwitchU3Ed__13_System_Collections_IEnumerator_Reset_m7908D76E026D81EA56A3A75F79FD8A642DE14550 (void);
// 0x000007D4 System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_InstantWeaponSwitch>d__13::System.Collections.IEnumerator.get_Current()
extern void U3C_InstantWeaponSwitchU3Ed__13_System_Collections_IEnumerator_get_Current_m5D327883E84027F957F96C903E1BCF63ACD04A5E (void);
// 0x000007D5 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_HideAllWeapons>d__19::.ctor(System.Int32)
extern void U3C_HideAllWeaponsU3Ed__19__ctor_m5F459CBCCE6E36951C8FBDFD3406A3FD3CDA7938 (void);
// 0x000007D6 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_HideAllWeapons>d__19::System.IDisposable.Dispose()
extern void U3C_HideAllWeaponsU3Ed__19_System_IDisposable_Dispose_m3254389CD405A77B2D627C7777BBD9FFEE2DA484 (void);
// 0x000007D7 System.Boolean RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_HideAllWeapons>d__19::MoveNext()
extern void U3C_HideAllWeaponsU3Ed__19_MoveNext_m92E07C9F9F9C471A0C6C6096E7AB7DA575126198 (void);
// 0x000007D8 System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_HideAllWeapons>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_HideAllWeaponsU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32AF76043CDAD36506A8C6E1B6EBE7725B24D84C (void);
// 0x000007D9 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_HideAllWeapons>d__19::System.Collections.IEnumerator.Reset()
extern void U3C_HideAllWeaponsU3Ed__19_System_Collections_IEnumerator_Reset_m70B0F68F4E3BC5FEFAE803B377BB6F863401AECB (void);
// 0x000007DA System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_HideAllWeapons>d__19::System.Collections.IEnumerator.get_Current()
extern void U3C_HideAllWeaponsU3Ed__19_System_Collections_IEnumerator_get_Current_mD58C7C87115170DDEAE9F7D1C864C7F08844ACB9 (void);
// 0x000007DB System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_WeaponVisibility>d__20::.ctor(System.Int32)
extern void U3C_WeaponVisibilityU3Ed__20__ctor_m3E0075C29D08E0D2C8569909146F58122470DC9E (void);
// 0x000007DC System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_WeaponVisibility>d__20::System.IDisposable.Dispose()
extern void U3C_WeaponVisibilityU3Ed__20_System_IDisposable_Dispose_m1B5DF7513EB6D60AEDE8E3352F3777EF4C73C27A (void);
// 0x000007DD System.Boolean RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_WeaponVisibility>d__20::MoveNext()
extern void U3C_WeaponVisibilityU3Ed__20_MoveNext_mBDC6DF064CF8F303C68FD0A6FA207BE43F857111 (void);
// 0x000007DE System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_WeaponVisibility>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_WeaponVisibilityU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m858E70C7B28AC5D9F5D3E86D15401B554E62EB48 (void);
// 0x000007DF System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_WeaponVisibility>d__20::System.Collections.IEnumerator.Reset()
extern void U3C_WeaponVisibilityU3Ed__20_System_Collections_IEnumerator_Reset_m02BDEAD0D20D8B473485DB85D6D69E6E5804526A (void);
// 0x000007E0 System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_WeaponVisibility>d__20::System.Collections.IEnumerator.get_Current()
extern void U3C_WeaponVisibilityU3Ed__20_System_Collections_IEnumerator_get_Current_m3DB300525C9D2E2F1EB03F8D2163BFA877500618 (void);
// 0x000007E1 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SyncWeaponVisibility>d__22::.ctor(System.Int32)
extern void U3C_SyncWeaponVisibilityU3Ed__22__ctor_m9895919C033AB1121CDB7355806D934CBF9118C8 (void);
// 0x000007E2 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SyncWeaponVisibility>d__22::System.IDisposable.Dispose()
extern void U3C_SyncWeaponVisibilityU3Ed__22_System_IDisposable_Dispose_m7FCEA4AA9D8BAC6F32518E9B04ACC2E5333A3CF5 (void);
// 0x000007E3 System.Boolean RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SyncWeaponVisibility>d__22::MoveNext()
extern void U3C_SyncWeaponVisibilityU3Ed__22_MoveNext_m253A6F7A375AE0775D2CBDF0F292A3C435E878C9 (void);
// 0x000007E4 System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SyncWeaponVisibility>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_SyncWeaponVisibilityU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC34B2E46A81F4D4302BDFF891014441B5EFC3A8B (void);
// 0x000007E5 System.Void RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SyncWeaponVisibility>d__22::System.Collections.IEnumerator.Reset()
extern void U3C_SyncWeaponVisibilityU3Ed__22_System_Collections_IEnumerator_Reset_mFC0CC2C310E15F4BAB476698361D8564E28A2B77 (void);
// 0x000007E6 System.Object RPGCharacterAnimsFREE.RPGCharacterWeaponController_<_SyncWeaponVisibility>d__22::System.Collections.IEnumerator.get_Current()
extern void U3C_SyncWeaponVisibilityU3Ed__22_System_Collections_IEnumerator_get_Current_m771FD86E17A757E999DAC38D10C9C81833F550CE (void);
// 0x000007E7 System.Boolean RPGCharacterAnimsFREE.Actions.IActionHandler::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
// 0x000007E8 System.Void RPGCharacterAnimsFREE.Actions.IActionHandler::StartAction(RPGCharacterAnimsFREE.RPGCharacterController,System.Object)
// 0x000007E9 System.Void RPGCharacterAnimsFREE.Actions.IActionHandler::AddStartListener(System.Action)
// 0x000007EA System.Void RPGCharacterAnimsFREE.Actions.IActionHandler::RemoveStartListener(System.Action)
// 0x000007EB System.Boolean RPGCharacterAnimsFREE.Actions.IActionHandler::IsActive()
// 0x000007EC System.Boolean RPGCharacterAnimsFREE.Actions.IActionHandler::CanEndAction(RPGCharacterAnimsFREE.RPGCharacterController)
// 0x000007ED System.Void RPGCharacterAnimsFREE.Actions.IActionHandler::EndAction(RPGCharacterAnimsFREE.RPGCharacterController)
// 0x000007EE System.Void RPGCharacterAnimsFREE.Actions.IActionHandler::AddEndListener(System.Action)
// 0x000007EF System.Void RPGCharacterAnimsFREE.Actions.IActionHandler::RemoveEndListener(System.Action)
// 0x000007F0 System.Void RPGCharacterAnimsFREE.Actions.EmptyContext::.ctor()
extern void EmptyContext__ctor_m021DE6350B2BC16C16D0BDB7EAEF607240FBA370 (void);
// 0x000007F1 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::add_OnStart(System.Action)
// 0x000007F2 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::remove_OnStart(System.Action)
// 0x000007F3 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::add_OnEnd(System.Action)
// 0x000007F4 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::remove_OnEnd(System.Action)
// 0x000007F5 System.Boolean RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
// 0x000007F6 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::StartAction(RPGCharacterAnimsFREE.RPGCharacterController,System.Object)
// 0x000007F7 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::AddStartListener(System.Action)
// 0x000007F8 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::RemoveStartListener(System.Action)
// 0x000007F9 System.Boolean RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::IsActive()
// 0x000007FA System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,TContext)
// 0x000007FB System.Boolean RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::CanEndAction(RPGCharacterAnimsFREE.RPGCharacterController)
// 0x000007FC System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::EndAction(RPGCharacterAnimsFREE.RPGCharacterController)
// 0x000007FD System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::AddEndListener(System.Action)
// 0x000007FE System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::RemoveEndListener(System.Action)
// 0x000007FF System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::_EndAction(RPGCharacterAnimsFREE.RPGCharacterController)
// 0x00000800 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1::.ctor()
// 0x00000801 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1_<>c::.cctor()
// 0x00000802 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1_<>c::.ctor()
// 0x00000803 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1_<>c::<.ctor>b__18_0()
// 0x00000804 System.Void RPGCharacterAnimsFREE.Actions.BaseActionHandler`1_<>c::<.ctor>b__18_1()
// 0x00000805 System.Void RPGCharacterAnimsFREE.Actions.AttackContext::.ctor(System.String,System.Int32,System.Int32)
extern void AttackContext__ctor_mD29DB3230B2A6694F3E2AB34A63A363E7651B285 (void);
// 0x00000806 System.Void RPGCharacterAnimsFREE.Actions.AttackContext::.ctor(System.String,System.String,System.Int32)
extern void AttackContext__ctor_m88D946E1909E5FAD47D02D71119246BF54948A72 (void);
// 0x00000807 System.Boolean RPGCharacterAnimsFREE.Actions.Attack::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Attack_CanStartAction_m088DE64C01FB9E1DC4AEC66584E81626CA0BB8F0 (void);
// 0x00000808 System.Boolean RPGCharacterAnimsFREE.Actions.Attack::CanEndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Attack_CanEndAction_m0D7BEB9B5B306F2FF983A80903D8CCE4983D3E7A (void);
// 0x00000809 System.Void RPGCharacterAnimsFREE.Actions.Attack::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.AttackContext)
extern void Attack__StartAction_mC35588F8F80B12300AA79B871D897282572C8722 (void);
// 0x0000080A System.Void RPGCharacterAnimsFREE.Actions.Attack::_EndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Attack__EndAction_m3561DE66897FC4BA463B9C0B83E5836B78F2030E (void);
// 0x0000080B System.Void RPGCharacterAnimsFREE.Actions.Attack::.ctor()
extern void Attack__ctor_m6D7AEE5D9BB61F5C489DBCC85B3ACBDC2BDCE4B8 (void);
// 0x0000080C System.Boolean RPGCharacterAnimsFREE.Actions.SlowTime::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void SlowTime_CanStartAction_m638FFA209DDFA26E3AE30E3F302929B376D72D7C (void);
// 0x0000080D System.Boolean RPGCharacterAnimsFREE.Actions.SlowTime::CanEndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void SlowTime_CanEndAction_m26167CB69668805F4F8B9B4A8B0DA0A2E1EE9CE6 (void);
// 0x0000080E System.Void RPGCharacterAnimsFREE.Actions.SlowTime::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,System.Single)
extern void SlowTime__StartAction_m8D92DC8BE56729E937636E2DA29539627009763A (void);
// 0x0000080F System.Void RPGCharacterAnimsFREE.Actions.SlowTime::_EndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void SlowTime__EndAction_m760B68FEEBE039EF14EB4692C0431D2A51BB907D (void);
// 0x00000810 System.Void RPGCharacterAnimsFREE.Actions.SlowTime::.ctor()
extern void SlowTime__ctor_m4B3A3C2B751CBA8A9BC52C426965EE87A74A1E7D (void);
// 0x00000811 System.Void RPGCharacterAnimsFREE.Actions.InstantActionHandler`1::StartAction(RPGCharacterAnimsFREE.RPGCharacterController,System.Object)
// 0x00000812 System.Boolean RPGCharacterAnimsFREE.Actions.InstantActionHandler`1::IsActive()
// 0x00000813 System.Boolean RPGCharacterAnimsFREE.Actions.InstantActionHandler`1::CanEndAction(RPGCharacterAnimsFREE.RPGCharacterController)
// 0x00000814 System.Void RPGCharacterAnimsFREE.Actions.InstantActionHandler`1::_EndAction(RPGCharacterAnimsFREE.RPGCharacterController)
// 0x00000815 System.Void RPGCharacterAnimsFREE.Actions.InstantActionHandler`1::.ctor()
// 0x00000816 System.Void RPGCharacterAnimsFREE.Actions.DiveRoll::.ctor(RPGCharacterAnimsFREE.RPGCharacterMovementController)
extern void DiveRoll__ctor_mAD1D9E48A34A0F61EE94F3F669908AF5FC413F9C (void);
// 0x00000817 System.Boolean RPGCharacterAnimsFREE.Actions.DiveRoll::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void DiveRoll_CanStartAction_mAB302B41A0DD18E3FBFBE15E50B007F3C45FB1F3 (void);
// 0x00000818 System.Void RPGCharacterAnimsFREE.Actions.DiveRoll::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,System.Int32)
extern void DiveRoll__StartAction_m8318DF3EDE8C6FD6CB652DE3ED33B6A1FFE0765E (void);
// 0x00000819 System.Boolean RPGCharacterAnimsFREE.Actions.DiveRoll::IsActive()
extern void DiveRoll_IsActive_m1A44941F01874B84DC93351F2C32EA6C2857780B (void);
// 0x0000081A System.Void RPGCharacterAnimsFREE.Actions.Fall::.ctor(RPGCharacterAnimsFREE.RPGCharacterMovementController)
extern void Fall__ctor_mC1900315D39F1EE3ED3A80A017178087573C3F22 (void);
// 0x0000081B System.Boolean RPGCharacterAnimsFREE.Actions.Fall::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Fall_CanStartAction_m5227886882ACCC9526C72D6BED310F30DE9B07BC (void);
// 0x0000081C System.Void RPGCharacterAnimsFREE.Actions.Fall::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.EmptyContext)
extern void Fall__StartAction_mF8DE606A2272B3D17CDC43D614A6796D0378C829 (void);
// 0x0000081D System.Boolean RPGCharacterAnimsFREE.Actions.Fall::IsActive()
extern void Fall_IsActive_m43D1662EC4F2180EA73AC172267AC9587B5036A6 (void);
// 0x0000081E System.Void RPGCharacterAnimsFREE.Actions.HitContext::.ctor()
extern void HitContext__ctor_m42A9FFCA8D88CD97594266A26F08EB2C6867C114 (void);
// 0x0000081F System.Void RPGCharacterAnimsFREE.Actions.HitContext::.ctor(System.Int32,UnityEngine.Vector3,System.Single,System.Single,System.Boolean)
extern void HitContext__ctor_m0C3CF9C94A3010B7B3A7CE378B469805C9F1D380 (void);
// 0x00000820 System.Void RPGCharacterAnimsFREE.Actions.GetHit::.ctor(RPGCharacterAnimsFREE.RPGCharacterMovementController)
extern void GetHit__ctor_m0D39F4B3FD52624518C9118D7E2D25287A76BB12 (void);
// 0x00000821 System.Boolean RPGCharacterAnimsFREE.Actions.GetHit::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void GetHit_CanStartAction_m317BA828DF5AA2361677720DCA4AB3EEC068D574 (void);
// 0x00000822 System.Void RPGCharacterAnimsFREE.Actions.GetHit::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.HitContext)
extern void GetHit__StartAction_mCDD4CCFF729CC41AEE2B0FB6C8599353576E4EFD (void);
// 0x00000823 System.Void RPGCharacterAnimsFREE.Actions.Idle::.ctor(RPGCharacterAnimsFREE.RPGCharacterMovementController)
extern void Idle__ctor_mBB8BB9B9F8F9E622215981DEC915949226632F39 (void);
// 0x00000824 System.Boolean RPGCharacterAnimsFREE.Actions.Idle::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Idle_CanStartAction_m8299ACF180735866C908B126221EBBD75E964BAA (void);
// 0x00000825 System.Void RPGCharacterAnimsFREE.Actions.Idle::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.EmptyContext)
extern void Idle__StartAction_m027917ECE016835D6D631B2AAA85C5E6FD334EB7 (void);
// 0x00000826 System.Boolean RPGCharacterAnimsFREE.Actions.Idle::IsActive()
extern void Idle_IsActive_m30C0B56D3E42B399518BCB33934A73EDFB4AEB1E (void);
// 0x00000827 System.Void RPGCharacterAnimsFREE.Actions.Jump::.ctor(RPGCharacterAnimsFREE.RPGCharacterMovementController)
extern void Jump__ctor_m9BD135F97F57736CDBB800AEBF80C30C5FD0061E (void);
// 0x00000828 System.Boolean RPGCharacterAnimsFREE.Actions.Jump::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Jump_CanStartAction_mA3E123D4B58517B0A786688587ED26E7ED6A3AD7 (void);
// 0x00000829 System.Void RPGCharacterAnimsFREE.Actions.Jump::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.EmptyContext)
extern void Jump__StartAction_mFA8C73EF73075DD69D03122304F04F049FF57F5B (void);
// 0x0000082A System.Boolean RPGCharacterAnimsFREE.Actions.Jump::IsActive()
extern void Jump_IsActive_m07F981BC441BDDBDE275874D26FA5B928174EE58 (void);
// 0x0000082B System.Void RPGCharacterAnimsFREE.Actions.Knockback::.ctor(RPGCharacterAnimsFREE.RPGCharacterMovementController)
extern void Knockback__ctor_mDB8378DC573ECFC6591F757F7AFD07994881319B (void);
// 0x0000082C System.Boolean RPGCharacterAnimsFREE.Actions.Knockback::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Knockback_CanStartAction_m50DE29784539C5024114D2DABD81A364DD472F4C (void);
// 0x0000082D System.Void RPGCharacterAnimsFREE.Actions.Knockback::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.HitContext)
extern void Knockback__StartAction_mB65ECED100898611282317D4185314A118E7DF06 (void);
// 0x0000082E System.Boolean RPGCharacterAnimsFREE.Actions.Knockback::IsActive()
extern void Knockback_IsActive_mF54369D4AA12C9CC022C75206CE26F7617A86BBF (void);
// 0x0000082F System.Void RPGCharacterAnimsFREE.Actions.Move::.ctor(RPGCharacterAnimsFREE.RPGCharacterMovementController)
extern void Move__ctor_m099534491010C8D474D34FBE822EA77FC00FE122 (void);
// 0x00000830 System.Boolean RPGCharacterAnimsFREE.Actions.Move::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Move_CanStartAction_m365CE578BC195EBBB2A558253B52870CF324EC75 (void);
// 0x00000831 System.Void RPGCharacterAnimsFREE.Actions.Move::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.EmptyContext)
extern void Move__StartAction_m8632CA4C4CAEF6320A6E2F35874B20323F39BC64 (void);
// 0x00000832 System.Boolean RPGCharacterAnimsFREE.Actions.Move::IsActive()
extern void Move_IsActive_m96CAAFFAF9180A2241E2724EE1A010444BF38B0C (void);
// 0x00000833 System.Void RPGCharacterAnimsFREE.Actions.MovementActionHandler`1::.ctor(RPGCharacterAnimsFREE.RPGCharacterMovementController)
// 0x00000834 System.Void RPGCharacterAnimsFREE.Actions.Navigation::.ctor(RPGCharacterAnimsFREE.RPGCharacterNavigationController)
extern void Navigation__ctor_m642C4D9CFF3C270BFBF0341EC9E11BE8F4531E91 (void);
// 0x00000835 System.Boolean RPGCharacterAnimsFREE.Actions.Navigation::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Navigation_CanStartAction_m0A93A8BC29B3470B0FD7745E89DC1F2E1480DD2E (void);
// 0x00000836 System.Boolean RPGCharacterAnimsFREE.Actions.Navigation::CanEndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Navigation_CanEndAction_mA4A7C343CCE894FEC8973D487CC1455FDEDE8FA9 (void);
// 0x00000837 System.Void RPGCharacterAnimsFREE.Actions.Navigation::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,UnityEngine.Vector3)
extern void Navigation__StartAction_mBD9D174CD6432900CECE45ABFEBFAFA6C1406B54 (void);
// 0x00000838 System.Boolean RPGCharacterAnimsFREE.Actions.Navigation::IsActive()
extern void Navigation_IsActive_mE5B36A8E723A9A4F28184E197BD2AEF4CA003339 (void);
// 0x00000839 System.Void RPGCharacterAnimsFREE.Actions.Navigation::_EndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Navigation__EndAction_mE36C6DEB720793A1808F39F748A929BAD1EE5870 (void);
// 0x0000083A System.Boolean RPGCharacterAnimsFREE.Actions.Null::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void Null_CanStartAction_m9053403901143F6D862BAC9F5DFC7552853CE705 (void);
// 0x0000083B System.Void RPGCharacterAnimsFREE.Actions.Null::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.EmptyContext)
extern void Null__StartAction_m785795E5A5C39E45AA6114877F8DF399153C0D33 (void);
// 0x0000083C System.Void RPGCharacterAnimsFREE.Actions.Null::.ctor()
extern void Null__ctor_m02B8F546ABDD54D5DF30F34A8F2C7CFBD740DB1B (void);
// 0x0000083D System.Void RPGCharacterAnimsFREE.Actions.SimpleActionHandler::.ctor(System.Action,System.Action)
extern void SimpleActionHandler__ctor_m9A2E08FFBE5C4F2E005F66DE0F7B8A95D61C1DCE (void);
// 0x0000083E System.Boolean RPGCharacterAnimsFREE.Actions.SimpleActionHandler::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void SimpleActionHandler_CanStartAction_mB85DFAF1D3667D528330040D9DD999C7624B48E0 (void);
// 0x0000083F System.Boolean RPGCharacterAnimsFREE.Actions.SimpleActionHandler::CanEndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void SimpleActionHandler_CanEndAction_m54C4081AC73564364D6BFE77D44D3A86EB96B99C (void);
// 0x00000840 System.Void RPGCharacterAnimsFREE.Actions.SimpleActionHandler::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.EmptyContext)
extern void SimpleActionHandler__StartAction_mEC5C915984682D1A5485ECA29D307909C7DD0EDE (void);
// 0x00000841 System.Void RPGCharacterAnimsFREE.Actions.SimpleActionHandler::_EndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void SimpleActionHandler__EndAction_m9C3F76A950F4D5FC2C25DCC1B0509CA6E7BDA629 (void);
// 0x00000842 System.Void RPGCharacterAnimsFREE.Actions.SwitchWeaponContext::.ctor()
extern void SwitchWeaponContext__ctor_m03A1E045A724AFBEBAE199E108236AB6339EFB8E (void);
// 0x00000843 System.Void RPGCharacterAnimsFREE.Actions.SwitchWeaponContext::.ctor(System.String,System.String,System.String,System.Int32,System.Int32)
extern void SwitchWeaponContext__ctor_m14A194BFA56E31109676567AC7288CB4F63CDC36 (void);
// 0x00000844 System.Void RPGCharacterAnimsFREE.Actions.SwitchWeaponContext::LowercaseStrings()
extern void SwitchWeaponContext_LowercaseStrings_m0E3F486C47FFFF2989329E21DFA2B1FB5E927128 (void);
// 0x00000845 System.Boolean RPGCharacterAnimsFREE.Actions.SwitchWeapon::CanStartAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void SwitchWeapon_CanStartAction_mD59FB525E543166C6855682712DED119BA3842C2 (void);
// 0x00000846 System.Boolean RPGCharacterAnimsFREE.Actions.SwitchWeapon::CanEndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void SwitchWeapon_CanEndAction_mB8F81811FDA6F42D9018EA2DAEB73CE2797AC4C1 (void);
// 0x00000847 System.Void RPGCharacterAnimsFREE.Actions.SwitchWeapon::_StartAction(RPGCharacterAnimsFREE.RPGCharacterController,RPGCharacterAnimsFREE.Actions.SwitchWeaponContext)
extern void SwitchWeapon__StartAction_m97BE2E361D3F4CCC9132615376F4F681626E2744 (void);
// 0x00000848 System.Void RPGCharacterAnimsFREE.Actions.SwitchWeapon::_EndAction(RPGCharacterAnimsFREE.RPGCharacterController)
extern void SwitchWeapon__EndAction_m56397AE4418E7641FA932C4CF95B8DA98734043D (void);
// 0x00000849 System.Void RPGCharacterAnimsFREE.Actions.SwitchWeapon::.ctor()
extern void SwitchWeapon__ctor_m6218D9A97B13CC7B80BB0F4F319B457941D9D443 (void);
// 0x0000084A System.Void RPGCharacterAnimsFREE.Actions.SwitchWeapon_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m451C31F453B0C3B4672BA3C704CD78AD74E5A025 (void);
// 0x0000084B System.Void RPGCharacterAnimsFREE.Actions.SwitchWeapon_<>c__DisplayClass2_0::<_StartAction>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3C_StartActionU3Eb__0_m5602C48389E0FBD3F67C9B52D566A10389CF8AF4 (void);
// 0x0000084C System.Void RPGCharacterAnims.CameraController::Start()
extern void CameraController_Start_m903E8D535DC295F2251E321100B1CE43F44BD2AE (void);
// 0x0000084D System.Void RPGCharacterAnims.CameraController::Update()
extern void CameraController_Update_m063E87A31AB973C689F6581C92CA311602D45C8A (void);
// 0x0000084E System.Void RPGCharacterAnims.CameraController::CameraFollow()
extern void CameraController_CameraFollow_m8418659B4F607A54981DE39C59AE0495F0DA716F (void);
// 0x0000084F System.Void RPGCharacterAnims.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m1218496F933C9BA09028DD7C5832C66E698FF318 (void);
// 0x00000850 System.Void RPGCharacterAnims.CameraController::.ctor()
extern void CameraController__ctor_mC45448DAC196F8DD678E07D8B869C939706150F5 (void);
static Il2CppMethodPointer s_methodPointers[2128] = 
{
	AnimatorMoveEvent__ctor_m4CF537CC75E3B901B2E42BB16F13D56FE93A6D23,
	Gravity_Update_m3F26C341A7C867384B6F6F1F79917BA88A9352F8,
	Gravity__ctor_mCE9306F61EE4AA3FD6BCB49955BD241D70FFFDC0,
	PlayerCamera_Start_m82AB289AD7CDCCE13772830C711CCD7C7FC835F6,
	PlayerCamera_LateUpdate_m63AE5C85B32C3BE1C76CA69E89B1F383E2D9C159,
	PlayerCamera__ctor_mEC7D02708FA0EE1799559A768E8CA74A42E3C26F,
	PlayerInputController_Start_m7E1EA7300A0ABAB5BFBE64B03C6C308E4842CC14,
	PlayerInputController_Update_m87BB74A84FB5ACED4D74DE2C4870402DA3B53D44,
	PlayerInputController__ctor_m4D6570072769159A1458FB4FB7CE42C3B82F4B81,
	PlayerMachine_get_lookDirection_m22611B931E75B30FD267FA7D46C94E71E4A1D0AB,
	PlayerMachine_set_lookDirection_m982B1A55B493E1702E5569D6698D532406491E51,
	PlayerMachine_Start_m29E18119D1199DBEFACE854873FBA5CDB4571A44,
	PlayerMachine_EarlyGlobalSuperUpdate_mF99AD5B0952F1B2E1E01C6C9A8AF4FAF9F6FE036,
	PlayerMachine_LateGlobalSuperUpdate_m19A2A81CC8FE1CA798BB45F96940B3693CDE5218,
	PlayerMachine_AcquiringGround_mA904C0126801D9CB3A70C82EE49D5F9F71BA7D70,
	PlayerMachine_MaintainingGround_m4CFE4EC092FFDEB8EEE74ECC0231392725C7638A,
	PlayerMachine_RotateGravity_m1BF3C223D1EC4E278AE663F32F1EA021A12B6CD5,
	PlayerMachine_LocalMovement_m3676E72DF892292E85836E0BE0BB7B96CCA73CA9,
	PlayerMachine_CalculateJumpSpeed_m63F7B8B35F37A474CB177D6C2C087605468BB64A,
	PlayerMachine_Idle_EnterState_m4FB783C114BF66FA8C15B3B7C6E48ACC86C50A21,
	PlayerMachine_Idle_SuperUpdate_m276FACF0FB8B33E6F3DFCFE8DFC8AAB7737BC921,
	PlayerMachine_Idle_ExitState_m0CD95C239192AB0036C31338C71B181A2D7CD31A,
	PlayerMachine_Walk_SuperUpdate_mED9705562ADAF6E83F6AF0B7DDBA83D929865184,
	PlayerMachine_Jump_EnterState_mE058669471ACF2F519FD076E43A6EE29F0A1B265,
	PlayerMachine_Jump_SuperUpdate_mA59CE36B7156F91E60575B92AFBEFF5FEE81BA4D,
	PlayerMachine_Fall_EnterState_m88BD4E64D1102B4AF23DFAF8BE5C1620C4B01311,
	PlayerMachine_Fall_SuperUpdate_mA9A8968D79B743E8B03D90E08BE5AD323B470413,
	PlayerMachine__ctor_mFB76217EE0E65F0C5D8F02DE88E8B939969B3611,
	PlayerMachineDebug_Awake_mBFB9CD13E55C3CD2905524BAB4AB1D6744A6D921,
	PlayerMachineDebug_OnGUI_m39D8235D6159982339822EB3B0946B3AE62EB011,
	PlayerMachineDebug__ctor_mE58BDBCEADC357269199913D6CC4368394C85F96,
	SimpleStateMachine_OnGUI_m73F3DD6B06CA64A695E4DE96BD8969FFD26F2A96,
	SimpleStateMachine_get_currentState_mF623DA6531D792B45BE168DE37F8FF79CD8CE868,
	SimpleStateMachine_set_currentState_mAB779E4E78092DAF4E8937194C736F2D82FAF771,
	SimpleStateMachine_ChangingState_mB80E5504E17DD09F593279E82A8960785E0100AB,
	SimpleStateMachine_ConfigureCurrentState_mF2D435CC444FF954CFCD0CB41164AD254A8B0A70,
	NULL,
	SimpleStateMachine_Update_m4433D56185EB32990DB65BE9ECFECABF0A3803F7,
	SimpleStateMachine_FixedUpdate_m47BCDA01586AA33827ED9CDFA026E9ABD8952D20,
	SimpleStateMachine_LateUpdate_mC79C69BF865F66D3941B7C0593C74BEB89DE1596,
	SimpleStateMachine_EarlyGlobalSuperUpdate_m804C277EFD52FDB4AB492F419158CD8C1C6315BF,
	SimpleStateMachine_LateGlobalSuperUpdate_m8F004C99B826FED6BE52DD1F1DF9A5E922D570E3,
	SimpleStateMachine_DoNothing_mCE49620E3865FA5F9048CADD7EEB326066DC60C0,
	SimpleStateMachine__ctor_m557202ABAB66066393F33FF4DF68783A080D0AC7,
	State__ctor_m64C836AE0CF303AA0ADDBA20966318C57965B433,
	BSPTree_Awake_mB3085EC0835714A3446013B8E9DF5DF269ACC6C3,
	BSPTree_Start_m896AD5B5EDF0398EE210F08E6644C71670123E05,
	BSPTree_ClosestPointOn_mC73316D12D5FE601325F643F068BCC3B04E62C05,
	BSPTree_FindClosestTriangles_m1851BEA73EA689AC46A4BF6FCB6BDA8540CFBA23,
	BSPTree_ClosestPointOnTriangle_m4ECE85FD2F27FEA7DC6E8483684219A92AFA85B8,
	BSPTree_BuildTriangleTree_m101B54A435C1B45C35E7588865225985F20A33D9,
	BSPTree_RecursivePartition_m5C2380883C695E20CC26A38384E5D2532AB6CA5B,
	BSPTree_Split_mB6E29CE71073EFD8163FC5F90DF26F00A9F4205A,
	BSPTree_PointAbovePlane_mF46597D4E01D45B229AB8E8A1F54E47FB487C80D,
	BSPTree_PointDistanceFromPlane_m265353EC3CA9DE5215C3B5661AC63A4FA9207CA9,
	BSPTree_ClosestPointOnTriangleToPoint_mC28E42DBE5F1E451ECD16D23CC9D665E5DEE759C,
	BSPTree_DrawTriangleSet_mB5F38185503EDF064DDFDBAAA59EE396AEA3B9C7,
	BSPTree__ctor_mE69746BDDD4B533075DFDCAA760384FFD334D4CE,
	Node__ctor_m82E5A39F45DC3AF3494A23AA12AF0575BA81CFFA,
	BruteForceMesh_Awake_m933DA912CE7BE2E6537B19F032049A4B13510CEB,
	BruteForceMesh_ClosestPointOn_mFCE8DF9442EA847DC21170D3E140A3995B339C0D,
	BruteForceMesh_ClosestPointOnTriangle_m9EC0A1E4DE935758750059589F5B68B12D16EF57,
	BruteForceMesh_ClosestPointOnTriangleToPoint_mE877F486C3B2317A017C76FFFE86B8C4133F1176,
	BruteForceMesh__ctor_m1F09509780DAA44B7D6CBE01806DF989AF905578,
	SuperCharacterController_get_deltaTime_m4B1717B6D963AC19D88C7C44D6D40C79D90693D2,
	SuperCharacterController_set_deltaTime_mDEE5BF0E7A708A32B2DA5999637273D310096E4F,
	SuperCharacterController_get_currentGround_mB2132C21D00976C32CF8C7EEE5BC9F71728A53D9,
	SuperCharacterController_set_currentGround_m0510FA181E9366A157C24F9BF6A3A21D6DF2DBBD,
	SuperCharacterController_get_feet_mAE07373444C1B61902863383345B8E402779059E,
	SuperCharacterController_set_feet_m50C50603EF4C8C5320F5A1AF0BBDA99A641649CC,
	SuperCharacterController_get_head_m13C74CB339AE3B2370EBF90AAA600C2726B6A6D2,
	SuperCharacterController_set_head_mEB5C9D0B8F8A30EB3247CB26D9C478E9A6DA948C,
	SuperCharacterController_get_height_mC02590B4097E19329BA5755139A9E89E4F241DCD,
	SuperCharacterController_get_up_mBB7061D99C5B6BF348BDC84B4B28E5E21615E93E,
	SuperCharacterController_get_down_m237E6665B7F7B91275CE8B4BC1643F5114CD428D,
	SuperCharacterController_get_collisionData_mA67D64ACEF134851999B3258C1543D10105C404D,
	SuperCharacterController_set_collisionData_m91786FAE60DB3345EE983622EBD8162F8EE39E78,
	SuperCharacterController_get_currentlyClampedTo_mD50E9D486A93FBD6524704AC146518B892D91E6D,
	SuperCharacterController_set_currentlyClampedTo_m8730779A4BA87DCDDA87B908B5D76A8A7BDBEFC7,
	SuperCharacterController_get_heightScale_mE7962B085838953382B8D570D39E6A0ACB793E53,
	SuperCharacterController_set_heightScale_m9B9FA91DA93838E42C33DF84C176FF586A806F78,
	SuperCharacterController_get_radiusScale_m3DEB0666E2CACB226AD8A61899BC7575292FAE26,
	SuperCharacterController_set_radiusScale_mDF7CD58D84A40F55F816EA23FAA6A0CA3BBE1FD6,
	SuperCharacterController_get_manualUpdateOnly_mDA89864515B45AA8BCB2FA0808CC78EDD3B0E91E,
	SuperCharacterController_set_manualUpdateOnly_mD4CA74FFEBDF33B8CCF4231BC299DEF1AA5C97E5,
	SuperCharacterController_add_AfterSingleUpdate_m64828D285C315ED77004B726B727099EE426C31D,
	SuperCharacterController_remove_AfterSingleUpdate_m367F0FB219BC87378E09AC5031526767A93D6A89,
	SuperCharacterController_Awake_m8EE1F24EED8FF3907BFECD036AADF864A2C53558,
	SuperCharacterController_Update_m2C1E011366D2682253D29D1FEDD533EFB802E059,
	SuperCharacterController_ManualUpdate_mB58C0CB410ED0D4F7675238EE0D94FDA420157BC,
	SuperCharacterController_SingleUpdate_mBA8DBAA0C232987C95B4A999FCF232C0FCFB7625,
	SuperCharacterController_ProbeGround_mB8B7994CA2781090FF01E555F122013B9F1EB04A,
	SuperCharacterController_SlopeLimit_m80D890535CF1499C95012CBE2BB7728490BA37BA,
	SuperCharacterController_ClampToGround_mA18B2AC04A4A1D062FA28E76DF414C0CF34E0C08,
	SuperCharacterController_EnableClamping_m0366ADA4BB9D7A2AA4214E5C508C31AD1F7B9CF2,
	SuperCharacterController_DisableClamping_m7D34CDAB1A2AEF9C8E61C380204766F64E2A5328,
	SuperCharacterController_EnableSlopeLimit_m243D5CC6FABC7B4C0DE7B8C7035C23F761CA48BD,
	SuperCharacterController_DisableSlopeLimit_m79847B0600319D58623FE3A057863F641EF1D41E,
	SuperCharacterController_IsClamping_m6758DB8AD0B3299DC996D18DA35ED45DC96A5A26,
	SuperCharacterController_RecursivePushback_mACF618F9C4A105A170A3B66DD89CA0B002E01220,
	SuperCharacterController_PushIgnoredColliders_m645C54EFEC0ADA624ACE4989B00C87FA8483EFDE,
	SuperCharacterController_PopIgnoredColliders_m0E9F1294732E9EFC1AD931BAE6ACEEF4DE6BE194,
	SuperCharacterController_OnDrawGizmos_mD1F90083B67DB1A260C3AC7F3EB2041C00BE5B77,
	SuperCharacterController_SpherePosition_m70EB65363266470FA633544AE301113A39CEAF16,
	SuperCharacterController_PointBelowHead_mD6410CFC7909693DD11E3C599DCE45AEB8A9CC6D,
	SuperCharacterController_PointAboveFeet_mC7C358C7AF52C7FF6365EC70E675A449AA20A8B0,
	SuperCharacterController_IgnoreCollider_mFFF3DF2CA22C6C75E7FA93A729F67AD4EB84B08C,
	SuperCharacterController_RemoveIgnoredCollider_mBE71B551F84F4C62EF4CB483076C1A667758DECD,
	SuperCharacterController_ClearIgnoredColliders_mC74409A2F8912286B433551A724342F601884D97,
	SuperCharacterController__ctor_m63390420E07A777BA78E97A2E62D99D07477B1A6,
	Ground_get_hit_mCDBAC5B9F772B2E150B99D546B52AB6C22F05CB9_AdjustorThunk,
	Ground_set_hit_mA6C1C6158A183FF3877531BDA67E1086AEF842F1_AdjustorThunk,
	Ground_get_nearHit_m598B18C65FE5795A3D247E1635379C7540C8FE8F_AdjustorThunk,
	Ground_set_nearHit_mE42C9DA3AF9F83A00E690111990F9D392D420162_AdjustorThunk,
	Ground_get_farHit_m8D2E3316676770CE4C8C630636A402A9573DAC06_AdjustorThunk,
	Ground_set_farHit_m77928112BF9AEC65022C84A1787201A32D43C0E6_AdjustorThunk,
	Ground_get_secondaryHit_m330ACA2CD1D0E381A78C82DB59BE01F45AFD2214_AdjustorThunk,
	Ground_set_secondaryHit_m1B485989BFB235D4A70CD88A8A2702A8420FC333_AdjustorThunk,
	Ground_get_collisionType_m681F70A85A656A8EA0043ECA6AE8051D8B7C10A5_AdjustorThunk,
	Ground_set_collisionType_mFDE77082DCA8DFBE5B76692957ACA9821BB85C66_AdjustorThunk,
	Ground_get_transform_m85C700E28EBB85313CBDDA9B7784EE1CBA88974D_AdjustorThunk,
	Ground_set_transform_mAA8A16FE791804601DBDCD66D43FA1CDC696CE2C_AdjustorThunk,
	Ground__ctor_mBAF5C5D866DFBDE3651139DB4BC5EC7DDECA79CD_AdjustorThunk,
	UpdateDelegate__ctor_m69C25AB9EA029AC2BDCEF5282E45ADF3AB3418AF,
	UpdateDelegate_Invoke_m3B1A8722CE871383E5D9D4916F5F1EFDACDDE01B,
	UpdateDelegate_BeginInvoke_m58E72D637AF7049B8EBB4BF5A59F3A6A0ED4AE89,
	UpdateDelegate_EndInvoke_m17688328E5916F4C6A7513664F32904DBE519BCC,
	IgnoredCollider__ctor_m5913C6929A7A48164018EF1DEAC0DE6E5FDD5ACC_AdjustorThunk,
	SuperGround__ctor_m16B316409835B253D60853B2D3E459C58FA9B5E5,
	SuperGround_get_superCollisionType_mA15833F8BA17B592C7EC905A3C9361AFB7A5D45D,
	SuperGround_set_superCollisionType_m800172A31CA12CE74A1D1A104ECE6C4AC32A2F79,
	SuperGround_get_transform_m24A16453BDEFB23A57C0209653BEAF3C62B428EC,
	SuperGround_set_transform_m2E890EE165C2918FD25663FF4B485062BA4AED17,
	SuperGround_ProbeGround_mEEA7A129E1C50F49D520C075176FB4E525F3AE44,
	SuperGround_ResetGrounds_mB49F3AF4D561509AC053F36BD18883D71FC763AA,
	SuperGround_IsGrounded_mA2EE3770FF6EA2CD2EEEEC55AD0C103F45A2CAA4,
	SuperGround_IsGrounded_mBB93BBD3FC1AE29566A904B2B4F7089EA03D6138,
	SuperGround_OnSteadyGround_m89403ADF08A4E0CC17F027774D2F1A80B4E95D12,
	SuperGround_PrimaryNormal_mDA40AC4E872D05973E9CD6F26E83C472AC8E6BC2,
	SuperGround_Distance_mF5D6B3771181AFD9BE48997DF208AFE444665A83,
	SuperGround_DebugGround_m3996448158025B7A3A433AA5E43F692FED45EAF4,
	SuperGround_SimulateSphereCast_m15F85ADCB950D1B1E2C33E484D58026D5A804D79,
	GroundHit_get_point_m23F49DFD517AC9F37753A1AC3FDEB9788AB5C9E6,
	GroundHit_set_point_mD2DFF334DC1B71D33E0EE19616E9B25D56FF47DE,
	GroundHit_get_normal_mBA93744A7C9AFA06E3F00BBB06A5343EB53665C3,
	GroundHit_set_normal_mD2ED0F20A538F5409BA8C06F6A8056B1C4641B4B,
	GroundHit_get_distance_m21A6DFE8F4BC011B5F57CC79DEF3D6BB7DAC6963,
	GroundHit_set_distance_m49F38D09A08B448BCCBE35AD454C8F614616D3BE,
	GroundHit__ctor_m9C5BA2C1D9B27270E1CCD6137CD62BCDF919A58E,
	CollisionSphere__ctor_m8B6896929AC46724C7176DE7D0769E52C6E89051,
	SuperCollider_ClosestPointOnSurface_m0E241D3340730B3924E93CF40498884A61CCAD78,
	SuperCollider_ClosestPointOnSurface_m8A3388260168C5F43200F3DBFF0C3015EC065B50,
	SuperCollider_ClosestPointOnSurface_m867A36263673F54F2E801ECD80C57D06E3F3EE0A,
	SuperCollider_ClosestPointOnSurface_m6215B50B2406B7CB3412AD293E60F0BDA807525A,
	SuperCollider_ClosestPointOnSurface_m60F4A006038B267AE4DC2959A6545FBC0B6EEFD7,
	SuperCollisionType__ctor_m58AD25094EB498E254E0EF2292BEDFEFA8A20345,
	SuperMath_ClampAngleOnPlane_m5BB0A3A83ED516466A7B7C6FB98B8BB6597DAC0C,
	SuperMath_BoundedInterpolation_m834D66312D42D645887BD3FD4E30C74BD03BD31E,
	SuperMath_PointAbovePlane_m8CB95E8E19A589A854B40A80D8CEE23673FAABB0,
	SuperMath_Timer_mADABCD91D257BE558916109355BD2BD6936525BF,
	SuperMath_ClampAngle_m9830852BD369E529AB4A9E80B8F8B0895F6ADE91,
	SuperMath_CalculateJumpSpeed_m60F2AFEA8C036ED0216AE49A12CE6A8AC7EBDAF7,
	SuperStateMachine_get_currentState_mB32DC68BCD3EC5879ED163868C3909D70C0FD22F,
	SuperStateMachine_set_currentState_mF61AB7F790DDB8F384C2F985B0E85FA499C5E70F,
	SuperStateMachine_ChangingState_m0E4A6EA42581C579A9D4AA2E169C67E70395AAB3,
	SuperStateMachine_ConfigureCurrentState_mAF64784AB940093D532E8F1666B354E88CCD7D68,
	NULL,
	SuperStateMachine_SuperUpdate_m550110B2775A0F1C4B1952F198104239F44B682F,
	SuperStateMachine_EarlyGlobalSuperUpdate_m755E4B3BFBC2F3657F63A94B1DE63A02754DA42B,
	SuperStateMachine_LateGlobalSuperUpdate_m1FBDC9014B829263350CB8E3C1F56ED568FBBF0F,
	SuperStateMachine_DoNothing_m203B2026577A03CF9EA025EAE48DD692639629FA,
	SuperStateMachine__ctor_m75032D44E6CE0D427E15E28A60D051C3329334CE,
	State__ctor_m2EF844EB75FFA4BFA7E7878CED14B95DAA9E9073,
	DebugDraw_DrawMarker_m7A0BE38CF30CCB1AFF13F5C56CCF725232529DE7,
	DebugDraw_DrawPlane_m6FE87BA9E0D89D72E9630D07D49C9655888F2907,
	DebugDraw_DrawVector_mD11E4A557147A5A5946DFC0A9CF171F3D82C34CF,
	DebugDraw_DrawTriangle_m70B96CEDF7796717F31E4134F58033F6E7B72723,
	DebugDraw_DrawTriangle_mB64BF1DF3EFC758DCAF112ED6B96627EC8798A65,
	DebugDraw_DrawMesh_mC058ABA78E2AA7D5F21881185FB09D9F44862C0D,
	DebugDraw_RandomColor_m6AF9C52FCF7C86BA392749753BFAB182206C4962,
	Math3d_Init_m34E98B06D3C5330AF1B0AA54D38B6D118B4B7B09,
	Math3d_AddVectorLength_mBBF07717EDE6A156B5EF654E86E656611B7574B0,
	Math3d_SetVectorLength_mCF6F873ED18FFEDDA5D2CEC48BB4D27595B46D29,
	Math3d_SubtractRotation_m577BFC25ED2D262CC545BB5426CD5EAE5B746E38,
	Math3d_PlanePlaneIntersection_m82CFA8805252E35FAE28C48F3D999B81232143E9,
	Math3d_LinePlaneIntersection_m20A69BA0741B5FC28BE4570339B52632DA3C3152,
	Math3d_LineLineIntersection_m5BF9048638CD6AE0079E1691E8465FBF6E52CAFE,
	Math3d_ClosestPointsOnTwoLines_mB8A1F4EEBD17E40FD2A5E6CF0A2AC6B3D194D397,
	Math3d_ProjectPointOnLine_m1AA7F9F87D468123DC274702E2D15003721A2123,
	Math3d_ProjectPointOnLineSegment_m49A3362FC58074CAE5F06A4108B217817CD5737B,
	Math3d_ProjectPointOnPlane_mCC932441F87E842ED16EAE8A350E46E0B4B504CF,
	Math3d_ProjectVectorOnPlane_m7B24068B9C069286A580B2E0170F8AADC9FD2734,
	Math3d_SignedDistancePlanePoint_mDA8C61C5FB25CCA1F5369D9C8499294C4F616FDB,
	Math3d_SignedDotProduct_mFAB0824BDCF7FBEBC7D1DEF1F6F599B708F4F98C,
	Math3d_SignedVectorAngle_m7775F79314F3319122A6AA338517EA1EABA42518,
	Math3d_AngleVectorPlane_mFB19C119BBD07236B0A5A6AAF26242C370BD7EA5,
	Math3d_DotProductAngle_m1244D537C89756D878F39DBA444A9745B7346A4E,
	Math3d_PlaneFrom3Points_mBDC00FA958FB6FF946D707E5745777A38855FE2C,
	Math3d_GetForwardVector_m91D1149165764C0BBD02ADB3890BC8C7D6C0B900,
	Math3d_GetUpVector_m827E4EA310C248A5CBEC0166F87A211E5D3177F0,
	Math3d_GetRightVector_m822F4E11B4E2E98539A527C66754A2EEF707C94F,
	Math3d_QuaternionFromMatrix_m3CD776834DF4ACF1A372DD0B70033EB8BE65C521,
	Math3d_PositionFromMatrix_m3081C8A5C48D696AC36E13C803F1AB308D67FE54,
	Math3d_LookRotationExtended_m9043427D32E9829190BCE6C9ED63B6AB34F9053B,
	Math3d_TransformWithParent_m44EF02AF2D7440EFC55EE2F1DB7C947880571454,
	Math3d_PreciseAlign_m2888D1FFBB41827EDA60084850C672609D41CE68,
	Math3d_VectorsToTransform_mF2C6B0D307BF868B713E23C306FC559C88C0625A,
	Math3d_PointOnWhichSideOfLineSegment_m129C3CBD061CE335131BF3352957C2B81255EE6A,
	Math3d_MouseDistanceToLine_m2C1605490D4F149BAA6D98DEDDBF7FAB83994BDF,
	Math3d_MouseDistanceToCircle_mE32D5174D0B2819451F52E547A8C9DB74C6E1A21,
	Math3d_IsLineInRectangle_m40CC653F190F0645118B5A8E25181A847B62F56B,
	Math3d_IsPointInRectangle_m78445C7CE181733DAFC35106646C1E9A670259AC,
	Math3d_AreLineSegmentsCrossing_m7B9495FA3B4B058B357E2BDDBD4974EB42692AF4,
	Math3d__ctor_mF697AAF66443A2CB51A167502795B0F6DAB9B84F,
	Math3d__cctor_m02D33B3F572D37724D0A8D065083C503BBB7AA3D,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	PowerUp_OnTriggerEnter_m6C53E6F682A799FFFB99B5C8CFDADF969AACB3A2,
	PowerUp_Pickup_mD3966CDAC8FB2810C6B16FF76E4554EA12BE411A,
	PowerUp__ctor_mDAF4AED1581D2E6AFD14782F86F935DFDE5E1A87,
	Quit_Start_m4E42BFD36424A5BE5406B3BEC3BB8348E532CD13,
	Quit_Update_m50901908B023B77D852F6F63B3C5D392AD313C33,
	Quit__ctor_mEB6E7FE6F53362692AD96E4DEE1D3906A592A7C4,
	InHouseGameInput_get_asset_mA2689FA21A8D9EC2DCE5318E6DE11A11ADBD3C92,
	InHouseGameInput__ctor_m8E835E3FE78639D99D80BC46D69FCA30AD89656B,
	InHouseGameInput_Dispose_mE9D906CB0A3555B42B232B56F51B819B5C2F0E35,
	InHouseGameInput_get_bindingMask_mC1981059D9205E4EEC52F23DD2436DB7A7C86D00,
	InHouseGameInput_set_bindingMask_m09A42F8D9878FEF7DBD1C5680D3AADB9333994E2,
	InHouseGameInput_get_devices_m2EA8A13530C60E8F733B0012503DAB6C3193FB11,
	InHouseGameInput_set_devices_mEEB0A510A2114CB439028B56D6257CE5AD74D3D1,
	InHouseGameInput_get_controlSchemes_mBB10365F858A2287F569C199515FF5C0C92CF921,
	InHouseGameInput_Contains_m6F12F25F959078BE1EED8DBBE4316DDE44E87BE2,
	InHouseGameInput_GetEnumerator_m606746F09D2D8C81473882998DBEFC12E6670E85,
	InHouseGameInput_System_Collections_IEnumerable_GetEnumerator_m6BEAC6E9D6E7F31445424C486FAF5C507E01B73C,
	InHouseGameInput_Enable_mE275DB9C4685968BB0241DEB4FB3DD26B52552EA,
	InHouseGameInput_Disable_mFB9CB2681F4A1D6F16EDDA42849410F473C42C81,
	InHouseGameInput_get_Game_mCC127585DF46B7AE55F2B32B1BBE591958002DE8,
	InHouseGameInput_get_KeyboardScheme_m6B60D7258BCF8F9B807223D0BCD9640B1CD3201F,
	GameActions__ctor_m27C6C0064D9FE1BDB406234230A2DC9E45AEDF17_AdjustorThunk,
	GameActions_get_Move_mE87A1909517A65952BDF2A99AAF1C2E2A533B2AF_AdjustorThunk,
	GameActions_Get_m5B79991937C94C9553CEA32AC6CE562AE2D47CAF_AdjustorThunk,
	GameActions_Enable_mCF988CC75D8936B63321870F553908CBE01D280D_AdjustorThunk,
	GameActions_Disable_mE3AE7CE6DC957EA06A442D66E73CDC7A361B50A9_AdjustorThunk,
	GameActions_get_enabled_m9EC312F534FD7BA341334AE73D0D787BC3F45DE8_AdjustorThunk,
	GameActions_op_Implicit_mCFA4F9034DC2FAE960E69CE6E2A39486CF68B7CC,
	GameActions_SetCallbacks_m0D12BF74753D0D7F717BF695973CB7A2F261FE5E_AdjustorThunk,
	NULL,
	TestAnimation_Start_mF04ABE6EA7992CB2B9A70A8E75B50E11CEA0C252,
	TestAnimation_Update_m263C79D072FFB04A9038521E7995C9ABB14B4BBA,
	TestAnimation__ctor_m79FD67C8378B0720750A34DA476A22B46058308F,
	TestMotionParameter_Start_mC1E22F7F05F8E2A9ED3A0D85CB9EDA5214E361C5,
	TestMotionParameter_Update_m2CA9C45AF5E1762436B1076ADE52A007F20AE6A7,
	TestMotionParameter__ctor_m5842144F7D36FD74F8C355A0437A29E2A9B86B12,
	EffectCore_GetIsSetUp_m92A6227E710D395FB85FBCD7BB5D9687359AA197,
	EffectCore_Start_m0DBB4A2241CDBD9F54544F11A3929B29A45B2918,
	EffectCore_Update_m476B6B7B75C0797DEC876DDEBBE833D27D0996A6,
	EffectCore_LoadEffect_m78F77BE853B42B9D5E4A16BDA1F43B831EA566E3,
	EffectCore_GetEffectObject_m4A88E594E418F9F7DCF77A63DDCFE34EB66F2A88,
	EffectCore_AddEffectGenerate_m0EA93CFF681952DCA1DEF2186C5F769A9FEAEE9B,
	EffectCore_OnDestroy_m02A18E2B8BE5202A618B14B0FCA4AA0F49EEFA08,
	EffectCore__ctor_m54B3356980516BCA8C43F3909B80C81A4246D7B3,
	EffectCreate_Appearance_m89193CEC0FB1F8659666F40FAA067433F9C3DC3D,
	EffectAction_SetLifeFrame_m719224EFA67CC5515FDEE61A0C489D3034D8A713,
	EffectAction_OnStop_mDE9549639D9232013AEA9E7C5AEA28A8B3DE7186,
	EffectAction_OffStop_m4A3CE9DEFE5C712F029DB04B88148DECEE172734,
	EffectAction_Start_m8D669543832CE4C18E219B8DA3D77863A0D183FC,
	EffectAction_Update_mA23971CF3DB901824D25B213D51D2B1BDAEC2499,
	EffectAction_EffectDestroy_mD3EE907D9481EE7AC627F7FED900F6A4C15FDC34,
	EffectAction__ctor_m8545F8CA6476E33A4E62F6F55FC39ABC98776FA8,
	EffectGenerateRepository_AddEffectAction_m654A5233B0EFBFA1985064217EE5C08EBD485AB3,
	EffectGenerateRepository_Update_mB6D00152984279CDDAB504951914DF0B59DA80AA,
	EffectGenerateRepository_ReleaseAll_mF7E2B9583FF23444B536CF6D2EDAD6BBDCDC238F,
	EffectGenerateRepository_OnStopAll_mA94CCED8EFD8F9AEE61927E9E0AB190A0E9A2D09,
	EffectGenerateRepository_OffStopAll_m9F3AB5DE472B4E79FDE4422D138F4F88216CF7DE,
	EffectGenerateRepository__ctor_m2FEAF747D55B19BEE6CBDF7BE77738CE83935E81,
	U3CU3Ec__cctor_m8F6901762032BEF0C377C87E8AC6514943C167FD,
	U3CU3Ec__ctor_mD8F880EA0BEAE73CA5824E5E19AE4E0DE34E3EC3,
	U3CU3Ec_U3CUpdateU3Eb__2_0_mB7C7F6088F72319901009BB5E87D4F7BC869ADD0,
	U3CU3Ec_U3CReleaseAllU3Eb__3_0_mDF11BBEBD98E1BA1B2569A13433C6FE39A7ECF78,
	EffectStorageRepository_GetIsSetUp_mFE1BEBD44601ADA1FC420C424991E3A36E2CA65E,
	EffectStorageRepository_LoadEffectObject_m8BC99A4E61F51060F313813ADA3765652D651E44,
	EffectStorageRepository_Update_m652712959DE5D8E89AC90A66CDE40AE3D36E76E2,
	EffectStorageRepository_GetEffectObject_mF339FB3051E9F043F199F5830BE6ED0DF4F181C2,
	EffectStorageRepository_ReleaseAll_mA97B2D345A3D85BAB4BBBDCD45A23B06109391B2,
	EffectStorageRepository__ctor_mE11FB75C82F99AEB6A54600496E80D8A7839376E,
	EffectRestraint_OnAutoDestroy_m000DF48295DA70C03CD0576928077B88B47ED8F6,
	EffectRestraint_OffAutoDestroy_m338D96EB214647D02A6BF7C771D962CDA723A473,
	EffectRestraint_OnStop_m52F853F84D6315AC8A58C8EB4F7E7BEB3D78B72E,
	EffectRestraint_OffStop_m5B970D093DB7B4643422E6416EF65803549F9946,
	EffectRestraint_NullCheck_m4B4D1C2A4710EF2FB82016870921342585D2A7E4,
	EffectRestraint_Init_m331EF5692C52126A10C0B881D57678B49CE9DD3D,
	EffectRestraint_Update_m0D646504E245AAF5CB2CDD4988DA2CB33FF6D2D9,
	EffectRestraint_Destroy_mF4B513377596EE18E65B9F8CC41BF1E579A77DD9,
	EffectRestraint__ctor_mBED18FEE7DE07698023C23A1A07375EEE3FCBB21,
	EffectRestraintControl_NullCheck_m8C6E1AB1303124ECD32A9E1FFB03DCA6A0668F71,
	EffectRestraintControl_Init_m2406A86346C06D9A3A325D9B67B339DE114F7038,
	EffectRestraintControl_OnStop_m63F78E280A710300697E86CA2B47A702A27FF960,
	EffectRestraintControl_OffStop_m91E1555A5BC1735EFBFD7196EF2497E85F334706,
	EffectRestraintControl_Update_mB035DEEB076CBCB918510B0FF1DEF5759C3446FC,
	EffectRestraintControl_UpdateLifeFrame_mEF74B8F44537467E791154E13757670C8186CF37,
	EffectRestraintControl_Destroy_mEBB09549EA9B0D7525863DE10B466ED7E06E4EB1,
	EffectRestraintControl__ctor_m122FF5D170943E4136CAD5BB292D247CA291A9D0,
	U3CU3Ec__cctor_m72A20AB9DE273C94877E02C4A5D53B120FA41D29,
	U3CU3Ec__ctor_mCEFEC0E228AD9E0ADF285D958872335D7D9496B0,
	U3CU3Ec_U3CUpdateU3Eb__8_0_m2E187D615034763E9C467293DD7FDA430D4A4094,
	BattleCore_GetIsSetUp_mE627FB1A91ABD78A9AEA35491717C5FAF39C0FFD,
	BattleCore_Start_m15CDA64A265DDE12CF240E155C6A4B1CF4F79012,
	BattleCore_Update_mAA000FEBD7A242C26848E6E9DD1404B0D6092DDD,
	BattleCore_AddPlayerUnit_m0E557339FA4EE7C7AC25A6F30008584540A2C83C,
	BattleCore_AddEnemyUnit_mA7F548510E16571A6E883359D9566EC49A6B642C,
	BattleCore_CreateBattleUnit_m6126A6843D91F06FEC227A8A65B8D7FE4B8F1F52,
	BattleCore_CreatePlayerBattleUnit_m42CD590988D7928598253273C16A6E0589A32AF5,
	BattleCore_CreateEnemyBattleUnit_m560158A499A0C05A97844BA66C05F5A9FBB5E868,
	BattleCore_SetUp_mDAC07463F8D288BB2A7B0CF8A58CF31928F34E59,
	BattleCore__ctor_mE10FB3E904E9F88083F994C5CC139893F9CF7E9E,
	BattleDefinition_GetMaxBattleUnitPlayer_mF0AD1785AE8382F6240490A80F15179F5E0998C1,
	BattleDefinition_GetMaxBattleUnitEnemy_m6A2A19558A94C8336E4D7089ABFE3D04667FD1E8,
	BattleDefinition__ctor_m8793A47364E6C315FB52AC47AD249BEC8765C051,
	BattleDefinition__cctor_m35E766BB68ECC5B198CCA0A9605BB4F3BBD831C5,
	BattleMemberControl_GetPlayerMemberUnit_m3414FEE47548DEA952207F6AAA6003357C36719D,
	BattleMemberControl_GetEnemyMemberUnit_mD77FC952E4A53B2F74A0489D330F633A8E3C9F9F,
	BattleMemberControl_GetAllBattleMemberUnit_mCCFB4A6B7484AF49C4702DF681E952D2D6FC6F3D,
	BattleMemberControl_Update_mAA8CD0A312FD991C3DD49520A534DB89B60E6056,
	BattleMemberControl_AutoRelease_mD05AF863F6988F7DF823B440B69233E8164890FC,
	BattleMemberControl_AddPlayerUnit_mEB0D0AB734A74D57F1BF616FA3A8DCA33393C7D7,
	BattleMemberControl_AddEnemyUnit_mC8CCB286758A745F0E2DBE6E02CE8CA911FEB0D0,
	BattleMemberControl__ctor_m78B1CFDE8D5356C49CA60D23EC435834CB2FB361,
	U3CU3Ec__cctor_mACD911A818033C51F367657CCE9B2B859AD8D6CD,
	U3CU3Ec__ctor_mEB13E58DD6974A1F27A2D4C25C0B3979843B478F,
	U3CU3Ec_U3CAutoReleaseU3Eb__7_0_mD9924984CA2E64BDB815CDAC1BDED852AFD3B76E,
	U3CU3Ec_U3CAutoReleaseU3Eb__7_1_m57FC1AB2A11B69F60E32BF1E1A6699D38E1E71EF,
	U3CU3Ec_U3CAutoReleaseU3Eb__7_2_mCCC0064B1AC36BC7F0B2286D9DC5F857607B8ED8,
	GameCore_GetIsSetUp_m50DE8A0BC9160260BDC704F78ED6EEB64E33CE2D,
	GameCore_Start_m24C57D5D23F84240EA1D92D5F7AB4D4EB943774B,
	GameCore_Update_mA3EAB0146A0881D876B58FF91643C82E7D3812AB,
	GameCore__ctor_m277C89012E5C42EC18AEFD307333EADC56F8EF19,
	DungeonBattleMemberExcelData_GetIndex_m4EAE678BFE0986F8F810EBE0CE5DCB2AF03B9100,
	DungeonBattleMemberExcelData_GetID_mCBA848C8F8F2AE02BD38560202C90A40B86A17D3,
	DungeonBattleMemberExcelData_GetPin01UnitID_m9DD5CA290B93E0D62AE581172646EFA0DF71EC99,
	DungeonBattleMemberExcelData_GetPin02UnitID_mD5F6CAB9C072E24857A8E7F375D481EDC1F5AC90,
	DungeonBattleMemberExcelData_GetPin03UnitID_m40694F107B3899BDC706C2520A7F87AFD50AD0C8,
	DungeonBattleMemberExcelData_GetPin04UnitID_mD9C23D7D12DE575DECE16A1D29F3000068393964,
	DungeonBattleMemberExcelData_GetPin05UnitID_m7EB1956ADC91E9BFE6992098FC2E21A930309AE1,
	DungeonBattleMemberExcelData_GetPin06UnitID_m1A89F7F3D09F39AB4EE4D4287A0F7956C515FC05,
	DungeonBattleMemberExcelData_ManualSetUp_m702CE9AEA92BA14E6A80B936014B0D07FF230A1A,
	DungeonBattleMemberExcelData__ctor_m7E05BD5B2A0805DBB8F9737501E487D35C88BE1D,
	DungeonBattleMemberGamelData_GetIndex_m2263A7DE7179581F3867DA3E4FC561F6EBDCB095,
	DungeonBattleMemberGamelData_GetID_m7E68AA502D075B051BADE486F7C520BD675C0C42,
	DungeonBattleMemberGamelData_GetPin01UnitID_m78BFC1AD7F790009CD4D09C7D630A95215326D90,
	DungeonBattleMemberGamelData_GetPin02UnitID_m0ADC8E33F7F30C3EC3E07E633F5A81268CAB3782,
	DungeonBattleMemberGamelData_GetPin03UnitID_m159D46F6EA0BDA3F25AF9D413BB1D732EDBAA272,
	DungeonBattleMemberGamelData_GetPin04UnitID_mE066A98DAEEF17841AA5AEEF689EDB76FCF2955D,
	DungeonBattleMemberGamelData_GetPin05UnitID_mC3647D4959422519BC5F1BA3043EC25F0D4E916F,
	DungeonBattleMemberGamelData_GetPin06UnitID_mB49252ED9201B2E256286907490D144DF5EACBA8,
	DungeonBattleMemberGamelData_SetValue_m28179DB90F6612269D6E98BE4ACE8862E7E14DDA,
	DungeonBattleMemberGamelData__ctor_m69592F939D91D67F5A89BC49566A21253BE3EA36,
	DungeonBattleMemberScriptable__ctor_m4798A7AC7C5DAD9EA213284FB2A1CC76C3FAA222,
	DungeonBattlePlacementExcelData_GetIndex_m4A4CB0E97A709766A7991A1055E50A82E45F9E74,
	DungeonBattlePlacementExcelData_GetID_m05D3775AB6CE531B96423894FFAD820B76D3E827,
	DungeonBattlePlacementExcelData_GetBattleAreaLength_m87B5365351816374C38BB6D6E6C84B09AFDE5A8F,
	DungeonBattlePlacementExcelData_GetBattleAreaStartPositionX_mC067C00143A78A26ED201EA523BAC7D76D9D65C5,
	DungeonBattlePlacementExcelData_GetBattleAreaStartPositionY_mF15C71445173EA1FA0D0845E7B68B5F35EC04CBF,
	DungeonBattlePlacementExcelData_GetBattleAreaStartPositionZ_m0B1B5EA47EBEF643F9ED658EB395CF08DD1381B4,
	DungeonBattlePlacementExcelData_GetPin01StartPositionX_m9DE3056D0A3A196962CC039B0AEEF5BFA5F2F01E,
	DungeonBattlePlacementExcelData_GetPin01StartPositionY_m22B645FADFB8BF3E646224987026EBE28212E9F8,
	DungeonBattlePlacementExcelData_GetPin01StartPositionZ_mCC3FAC3FCDCDBF9413AAAFE1DFAA06B2CC18562D,
	DungeonBattlePlacementExcelData_GetPin01StartScale_m807E21A850DAEA0930930E4FE0E0AEE08FFCA83F,
	DungeonBattlePlacementExcelData_GetPin01StartRotation_m7811808B0F2D7AD449F1F77DDB52632D6D33F611,
	DungeonBattlePlacementExcelData_GetPin02StartPositionX_m392E896523154E0FA541B002D6449221DDF68807,
	DungeonBattlePlacementExcelData_GetPin02StartPositionY_m3B58DDFA24087EBE89F25781416558C002FABA67,
	DungeonBattlePlacementExcelData_GetPin02StartPositionZ_m66E049F7F9C1E51EDE1B700A36195EF4FF9820DC,
	DungeonBattlePlacementExcelData_GetPin02StartScale_m3F491479CADD02E97B13A04CAD8FEDF506F02529,
	DungeonBattlePlacementExcelData_GetPin02StartRotation_m615A5ACE6AFE45DA005184C97425F6F3A0872404,
	DungeonBattlePlacementExcelData_GetPin03StartPositionX_mF2FD048EDA3D8EC7C7D7C65DF3BBF2ACC4A42460,
	DungeonBattlePlacementExcelData_GetPin03StartPositionY_m3E662529CB13FBBD27F026E7629288C17A8AEB1A,
	DungeonBattlePlacementExcelData_GetPin03StartPositionZ_m1C6BC309439028C0538D8FE4FAFF0B98F9170F7C,
	DungeonBattlePlacementExcelData_GetPin03StartScale_mBDFFDFB9463B6A6DE2B8F38F297D5CCFA6E14CE8,
	DungeonBattlePlacementExcelData_GetPin03StartRotation_m9F436FBF167157EB64DEEDC3C921A85DD0A3FACB,
	DungeonBattlePlacementExcelData_GetPin04StartPositionX_mD5D12B89159D6FD090BCB2B560BFBEF9B8A5D15D,
	DungeonBattlePlacementExcelData_GetPin04StartPositionY_m89EF93AA23CAE5BEC23AF0F860C60FE7D0D9077E,
	DungeonBattlePlacementExcelData_GetPin04StartPositionZ_m4F5CF00BE3D8EB3683AFA856DF5DC4DA3C56D510,
	DungeonBattlePlacementExcelData_GetPin04StartScale_m18F9E8630EC76AC41D3CECD026EDA9A68A5F206F,
	DungeonBattlePlacementExcelData_GetPin04StartRotation_m5364FE9F91AD5F977AF5319D225DC305C5EAFA6A,
	DungeonBattlePlacementExcelData_GetPin05StartPositionX_mE8768F2AE0EE1652962A30A11AAA3D7BCA48771A,
	DungeonBattlePlacementExcelData_GetPin05StartPositionY_mE95353EA85F04679C6F0E22EB5A50A7023D6BABA,
	DungeonBattlePlacementExcelData_GetPin05StartPositionZ_m130EF2727FF992D1298435389DCEF7CB1ED6DF99,
	DungeonBattlePlacementExcelData_GetPin05StartScale_m8E020DB7D914F19C5F8C580777A008742C97283D,
	DungeonBattlePlacementExcelData_GetPin05StartRotation_m53F887286A478CB95060B88FA33F3DF191028903,
	DungeonBattlePlacementExcelData_GetPin06StartPositionX_m3DD2042B11A73503A7EBF8EA8720A8053DEB6349,
	DungeonBattlePlacementExcelData_GetPin06StartPositionY_m458BD68204109AD41CA10BE81C9B5BE7CFDCCE96,
	DungeonBattlePlacementExcelData_GetPin06StartPositionZ_m524D07AC42AB905CB910DAC6917D3E75B18F45AF,
	DungeonBattlePlacementExcelData_GetPin06StartScale_mD795D4DFFCC1AAA57FD48722256CCFEA12504FD2,
	DungeonBattlePlacementExcelData_GetPin06StartRotation_m2DB5EFA39067B5BB49ACF08E75F20034C1176C8E,
	DungeonBattlePlacementExcelData_ManualSetUp_mFDE90E7758D6569F10736AAE2A9D705031A2BC17,
	DungeonBattlePlacementExcelData__ctor_mA6983F2437F20A8CEB3C845009B78927B8DBC86B,
	DungeonBattlePlacementGamelData_GetIndex_mD9D90C795C2B464FD2AC9808F70D5125EA1F1C42,
	DungeonBattlePlacementGamelData_GetID_mEB22E863E0C4ABCCE156B1C295F0FD79E54766F3,
	DungeonBattlePlacementGamelData_GetBattleAreaLength_m08ED6E9CD2B5CE6344A402DD4D5170C9A90F1E10,
	DungeonBattlePlacementGamelData_GetBattleAreaStartPositionX_mB89D3C454A4393076FB202D285EE10F5B472CCBF,
	DungeonBattlePlacementGamelData_GetBattleAreaStartPositionY_mD3B6C05519368071F87535F5F8B93A314C09C8F1,
	DungeonBattlePlacementGamelData_GetBattleAreaStartPositionZ_m248CBA87BB4FDF677BD181C3B2F57306FD43FC58,
	DungeonBattlePlacementGamelData_GetPin01StartPositionX_mFB417D47954A1B741D1B40A6B0BA1B203F891889,
	DungeonBattlePlacementGamelData_GetPin01StartPositionY_mBDA233D8D5D2DF8F1E66C841573E0C22CC286EC3,
	DungeonBattlePlacementGamelData_GetPin01StartPositionZ_m9D080840A39F76A884E57A9C9084313B89597EA5,
	DungeonBattlePlacementGamelData_GetPin01StartScale_mC4D75C91DADFBAAEF90D68FC6E5613B7EAA78B7C,
	DungeonBattlePlacementGamelData_GetPin01StartRotation_mB98831F6C979EEE85F3E2090F514FE59FC6C22D4,
	DungeonBattlePlacementGamelData_GetPin02StartPositionX_m7E251C88DF8449296F9FC4F7565114508DE199E5,
	DungeonBattlePlacementGamelData_GetPin02StartPositionY_m87ABE6111F3F5115CAE0499F7E56806EFEED9538,
	DungeonBattlePlacementGamelData_GetPin02StartPositionZ_m0564ADDFF204AA22E40FDE618798134F00E1DD61,
	DungeonBattlePlacementGamelData_GetPin02StartScale_m86B9ED98E3A2F2D6168A895B922CBF9682120381,
	DungeonBattlePlacementGamelData_GetPin02StartRotation_m9DFE225C050BE9F887244C0BE99AE3D16D06EC05,
	DungeonBattlePlacementGamelData_GetPin03StartPositionX_m0532D1FA537BEAE8E77901A73F612E13154F97CF,
	DungeonBattlePlacementGamelData_GetPin03StartPositionY_mFAE1FDB535D9F3EE6542C543B7F4D5710A99CE73,
	DungeonBattlePlacementGamelData_GetPin03StartPositionZ_m6AC0CD8781E0F9122F6DE89F2A592BCDCA18DE96,
	DungeonBattlePlacementGamelData_GetPin03StartScale_m1AFFBE214C39F155F6335BC3E1C37E557111DFB4,
	DungeonBattlePlacementGamelData_GetPin03StartRotation_mA2D9595E5FEEADEDAF43D71580D688AFFB7AFC60,
	DungeonBattlePlacementGamelData_GetPin04StartPositionX_m27F6A176A9E2CA6272D0FE187CA10EEF06540950,
	DungeonBattlePlacementGamelData_GetPin04StartPositionY_mCC184A7FC26D01369E861E91F85D8225BAAE0DEB,
	DungeonBattlePlacementGamelData_GetPin04StartPositionZ_m34FDB41323D0BB998F45D8053FD4F80119470E2E,
	DungeonBattlePlacementGamelData_GetPin04StartScale_mF4AFB31A9CB7C40B5E46C084DC16DDC2DA56F49F,
	DungeonBattlePlacementGamelData_GetPin04StartRotation_m7D3E2913B95C3ADDB48B788850479B8A95367048,
	DungeonBattlePlacementGamelData_GetPin05StartPositionX_mDCFD3C05C45B3FE5609653AA9F16038AB5BEEE5E,
	DungeonBattlePlacementGamelData_GetPin05StartPositionY_mE3B5A283AAB07057D9760F29D2868B2EF0BF48C9,
	DungeonBattlePlacementGamelData_GetPin05StartPositionZ_m5F88E594A2D9588AEE4A6CEC6E8EC8739099B877,
	DungeonBattlePlacementGamelData_GetPin05StartScale_mA047AC1EFA48DD234C25B6A78556374C33EEEDFF,
	DungeonBattlePlacementGamelData_GetPin05StartRotation_m358327A9D0CF8EE3C11E045BD0C1361A05E5A489,
	DungeonBattlePlacementGamelData_GetPin06StartPositionX_mB0A34C54DFDC397FEF249AAF8FAB62D18A483724,
	DungeonBattlePlacementGamelData_GetPin06StartPositionY_m21C73ACAC098B7DC9A5057BB7C0AEBF4ABF87004,
	DungeonBattlePlacementGamelData_GetPin06StartPositionZ_m965C81BDC05A3D5C7D1A2AA121A59971D7DF077F,
	DungeonBattlePlacementGamelData_GetPin06StartScale_mBB5FFF74568B40A88EE81658FD7AE3B4C1C28712,
	DungeonBattlePlacementGamelData_GetPin06StartRotation_m69F9CE8160E274ACCA1B23366354CABD8DAEDBBC,
	DungeonBattlePlacementGamelData_SetValue_m02E64C24DFDB3544F16E49F9EDEC539C469E4BBF,
	DungeonBattlePlacementGamelData__ctor_mD46C42317A6346DDB6E54435565D2B796F08A4AF,
	DungeonBattlePlacementScriptable__ctor_mC719FB9B492D2D58A7E4ECE98C8FE86320463679,
	DungeonUnitExcelData_GetIndex_m624B86E2AB43BC757AC3292385D23B7B500DABC5,
	DungeonUnitExcelData_GetID_mED3718B805AD4C859E9C4226A1C3202872CDD771,
	DungeonUnitExcelData_GetUnitID_m46EE0EA9A4EEDCE7CAD28377C6414FE3FCD01DD5,
	DungeonUnitExcelData_GetUnitStartPositionX_m1C9F91305F948113B075A14AFD4D28D5C47C482F,
	DungeonUnitExcelData_GetUnitStartPositionY_mEAF84317FE1BB50EE82332C1361A61C9C9BBBFBF,
	DungeonUnitExcelData_GetUnitStartPositionZ_m86BE3EFDCE4DBE156781FDCEE9EEB63F8D6D5278,
	DungeonUnitExcelData_GetIsBoss_m9967837AD711D43F893C815F24B5C7102C7F10A8,
	DungeonUnitExcelData_GetRootID_mC64B8BEB771C94A7CB272955D360D236753D5124,
	DungeonUnitExcelData_GetMemberID_mBF6581BB05A24D2394F916DB6A3905DD76689DB5,
	DungeonUnitExcelData_GetPlacementID_m6CB3E75719DE93C48CC6238627186A65575128DB,
	DungeonUnitExcelData_ManualSetUp_m16E8D44372CEE59FDBAE83B8604FBBEC0EA66065,
	DungeonUnitExcelData__ctor_m488C2ED56B1087CA5BCA1B68A8D1991907A79D19,
	DungeonUnitGameData_GetIndex_m928E9A5E7908793D458D6E5CF4BB6331BFDEB02C,
	DungeonUnitGameData_GetID_m64EE591CFB2D8FFB2E781540079CA893B4DD8E9F,
	DungeonUnitGameData_GetUnitID_m62644ADF03CAA95F6CD5AD13AE88B7A2E3777396,
	DungeonUnitGameData_GetUnitStartPositionX_mF03F2B90BB663CD586F76DE0E6C4E40C006D409F,
	DungeonUnitGameData_GetUnitStartPositionY_m91BCCF04980C206E7E40FD6A0BC0E8C6316CD8FA,
	DungeonUnitGameData_GetUnitStartPositionZ_m49C2B30118C63537573F43C358FC68276192F4A9,
	DungeonUnitGameData_GetIsBoss_m25B958847ADE9E0CD1F50CBD68D9DB74DB62E8E0,
	DungeonUnitGameData_GetRootID_mFCDFC6040A09BBC4BB8EFEB0DB4B343E5854BC81,
	DungeonUnitGameData_GetMemberID_m3E44244593F8E192827871E71DA4DDAEF81E1A38,
	DungeonUnitGameData_GetPlacementID_mBF46F41A104A287B98E4886C27D0CD4422341240,
	DungeonUnitGameData_SetValue_mE19CF803AEC4E963BB141AD69E6F1F081FA260FB,
	DungeonUnitGameData__ctor_m72D4249F5301A7EF11825D1FC2FEC840F5FE06B6,
	DungeonUnitScriptable__ctor_mC57A7A8380B927ECC0DEC2572E87FD332F59D495,
	DungeonUnitDataStorage_GetIsSetUp_m7F550BEE5D09DA8849109B306B6D100177823A12,
	DungeonUnitDataStorage__ctor_mC97C7749D4277994F6198E445209E7774BA04B47,
	InputSystem_SetLeftStickVector_m5576A041DCD16416A06014AE02398F9AA468489A,
	InputSystem_GetLeftStickVector_m7013256D0986B97A93ED6E27DC17FF5F69333249,
	InputSystem_SetRightStickVector_mEB0341A3C26512106530133B58A26D9614558BBD,
	InputSystem_GetRightStickVector_m552448DEE7CCC3182505025030B4E9310D6AD877,
	InputSystem__ctor_m946B1ECB379AD9E7DDC6F7C4266D25CAD676B444,
	CoroutineHandler_StartManualCoroutin_m0B81F0F3BB195CA55D306C7B2E1C615AF05B9467,
	CoroutineHandler__ctor_m2F086A05A90813039FA5EE301F265E8F3F63A9AE,
	NULL,
	NULL,
	NULL,
	AccessorError_GetErrorInt_m79D20860467242C0F455B63EFE41B07B5E444ECE,
	AccessorError_GetErrorString_m5EADB5E64FB105CBBEFD41DF5729AED9C4EFB56A,
	AccessorError__ctor_m7CE516EF63F56B5F9BD984D8106726AA53B95D37,
	AccessorError__cctor_mDE6D27875EC57494DFB4ABA95BF34EADCF1ACC69,
	AddressableDataContainer_GetListAddressableDataCount_m65F06ED08745B5F2C5019F406F03F6F56BE79E6B,
	AddressableDataContainer_AddAddressableData_m2909ADEFA4464A4E2BBBF071F8EB208073B74ECE,
	AddressableDataContainer_Find_m573ECB7C69A745053E48820F0D74B483DE53FBD8,
	AddressableDataContainer_Find_mA6B982BF0D0ED81A547DD22951E8A0ABF28A28C6,
	AddressableDataContainer_AutoRelease_mCEAA203D8D7F96390B5E23F5C121E831FEE7084A,
	AddressableDataContainer__ctor_m67772C123FDCCAA604765FB1E4DA927B4CE6637C,
	U3CU3Ec__DisplayClass4_0__ctor_mA7A4F177942BE15F86E295F46847B17B13F0ACB7,
	U3CU3Ec__DisplayClass4_0_U3CFindU3Eb__0_m8D775BAF9052EE263595B2D70086396C456A173F,
	U3CU3Ec__cctor_m0521BFA844A6EA00E52D557058D07069FE6B1833,
	U3CU3Ec__ctor_mEA8F568783FA58C38E54B98B1AA1C1295F5DAC2A,
	U3CU3Ec_U3CAutoReleaseU3Eb__5_0_mC61831B359AE286BA23C107DFF6564F8299858F9,
	U3CU3Ec_U3CAutoReleaseU3Eb__5_1_m58292BCAA065AF8BDD4DEBF08BBDFDD0389A705B,
	AddressableDataCore_get_Instance_m41609607B2AA92295DF1E52184586343EFF5395A,
	NULL,
	AddressableDataCore_GetAddressableDataContainer_m63851DA0606EC7208327DADDFC3CB0DA94AF8E6C,
	AddressableDataCore_Start_mE0B2481170F043F52BF5E70717D3E093B50F5107,
	AddressableDataCore_Update_mFC2EBE6DBB11DC4B53C343D70D30CF347BB2733E,
	AddressableDataCore_AddAddressableData_m5AC94F4285749169D34A8CC39A15679E648F96D0,
	AddressableDataCore_Find_m1EB3FA24AF37C1B539FA7413FB52BFC9F4838FA2,
	AddressableDataCore_Find_m3B4853AFE4E7707E60E51CE0D86C62D67B07E193,
	AddressableDataCore_AutoRelease_m17F44E06CEC9881C8854B06BF746223FE2456EC0,
	AddressableDataCore__ctor_mF9CE47CDCD270E933885A60894A1BC905CC46B2F,
	AddressableDataCore__cctor_m564761740126505851A0F0491F1FDD645B27CC12,
	AddressableDataGameObject__ctor_mB018D7EEA04C8D499BECF1B4C120D88EBAC7A68A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseAddressableData__ctor_mE188F47F6143FCBEC9A0167A2B01E4B3DB7ED4EE,
	BaseAddressableData_GetFlagAutoRelease_m3E93BF211C34895911EEE0B68446D9CEC111FD9F,
	BaseAddressableData_OnAutoRelease_mE6B103A1AD7A6366DCFB039EC36721678AE1A628,
	BaseAddressableData_GetIsUse_m9A3C6CA8F2003E6C94A789AFCF2A8C315B3F4EC4,
	BaseAddressableData_OnIsUse_mFAC450C7A184C56530632725CB6F8D28CE9CFF5A,
	BaseAddressableData_GetFlagSetUpLoading_m80D5C3CE4C50AC601A2DB606780A5B0B43A2693B,
	BaseAddressableData_GetBaseAddressableData_mEB39B822373FB4119DB95A4F392927B42D8CB53A,
	BaseAddressableData_GetBaseArrayAddressableData_mC6AACE86BDF62176612E2514AB89CF7E9F1D00C9,
	BaseAddressableData_GetBaseArrayAddressableData_m087AEFC6428011C99F74193E09B6D3DE2E70A585,
	BaseAddressableData_GetArrayAddressableDataCount_m988288920074A0DC7E7AED9CD9533D8CBF37B0F8,
	BaseAddressableData_LoadStart_m06DD6FF2019816A6BC00A8587FA0C5E3C3B2BC29,
	BaseAddressableData_LoadArrayStart_m1094C083A1ABE1A62965458B46F92F5EE2EC8F48,
	BaseAddressableData_Release_mE1CAB646B061897499234B25A7B554ADB96570DC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AnimationConvert_GetAnimationName_m01E918D19210D6D4A66EE049DC986879DDE56D77,
	AnimationConvert__ctor_m721EBA22230D6AA9F98FBA8FC9A6E25D7FDEC070,
	BulletAction_SetUp_mBF05AF8AF21B0FD82377FD96FD358030AA1EEF02,
	BulletAction_Start_m2E6D9B4B85B57782E77A38C113C9629A0FAB78CB,
	BulletAction_Update_m26C2B94B2C5025D79B8E921C006EDF90330C2124,
	BulletAction_HitSecond_m7A7CAB2C44BF9DB60A3F2EA79A0B0649905AFA15,
	BulletAction__ctor_m8A4DBC09046F89FB7910A863DA4FB0E7AE283479,
	BaseBulletRole_Init_mBAF1E4405516FA10F811D908708E42E68635A6BE,
	BaseBulletRole_Update_mFEA7DCA97A1920E6039EBC47CA04637363487C56,
	BaseBulletRole_KeyFrameUpdate_mECB576573497B7518CCA4EF066575270FC9CD4B6,
	BaseBulletRole_RoleUpdate_m60DFAFE5A843BA3DB34D61AE09EEA0B9D7BE609F,
	BaseBulletRole__ctor_mF26BE3ABD6DEADA3411AC13BC67D654C455EFCCE,
	BulletCore_GetIsSetUP_m1D126010AACD78D9446A8FFCF30F3FD54D596823,
	BulletCore_Start_m010E609F5A4F556CE6222CED72FBA150D404F67A,
	BulletCore_Update_m98BA92D03740C1CA1EE7503A88F909C731A504AB,
	BulletCore_LoadBulletData_m0F8BFDB3BA1A50E05401CD54F3C4C38F556C4F24,
	BulletCore_GetBulletData_m3025D757944F3E93E4513C9BAB74C99A01F46420,
	BulletCore_GetBulletCollisionID_m089C933ED8B08ACBD462052C58D0703D505A80F6,
	BulletCore_GetBulletEffectID_m534823DD703992A8EC4E916AA5174CC9B9E83006,
	BulletCore__ctor_m55CBEACA9D3EACD91827330EEA5937C3FA4BF9A2,
	BulletCreate_Appearance_m189D1066F79941FCD843FE21BBB09FADF9B3041A,
	BulletDataExcel_GetIndex_mA46261A3DF2D8BEE166DA07DD2959E5B88673BE2,
	BulletDataExcel_GetBulletID_mA900F479C07C79D617BE9F5B6D5392FE38B24EE4,
	BulletDataExcel_GetBulletType_m852084F50F799AAF2AE1289C3AEEEAD43AC8F169,
	BulletDataExcel_GetLifeFrame_m887AB9C712F7304A84AEFC6A15C017202DF8A5AD,
	BulletDataExcel_GetFloat01_m3BFB7D3D93406018917F05734B10227ABEA9D4CB,
	BulletDataExcel_GetFloat02_m668D43095A54FDBE3DE846C3D0DAECA5EA637F7E,
	BulletDataExcel_GetPositionX_m7F22D5A4AB3FABF4F76097E00CD702F85D634283,
	BulletDataExcel_GetPositionY_m8F59CC2E7D25D76F755C29EEEC0CBB68D7F73035,
	BulletDataExcel_GetPositionZ_m7285D96F38ADCC8D56A58E2EEE84366F6390E83D,
	BulletDataExcel_GetBulletEffectID_m1946B9A47171D982EC91BB182AB3F8AFBB1F54A3,
	BulletDataExcel_GetBulletCollisionID_m6E53C0D0D6B583616B459AA758DE1E2F6360175A,
	BulletDataExcel_GetBulletSkillID_mF63FB8F3B8E624D1D3D942A4110ECE17DD5E0B22,
	BulletDataExcel_GetBulletAttackCollisionRole_m80EE761B6B66A4B37C0558EC8BAE41C8CFDF21DB,
	BulletDataExcel_GetBulletHitAttackCollisionNextID_m549261BB5560E0E63FEAC181824C3D98A09F6CB6,
	BulletDataExcel_GetBulletBackGroundCollisionRole_mFC80EAF19C085517092895C70729A06E08CFF6EB,
	BulletDataExcel_GetBulletHitBackGroundCollisionNextID_m00611223247C8B4A943F495209A041AD3D1F0C2D,
	BulletDataExcel_ManualSetUp_mAABD4D5061AC751FFBF3EA1718CD66B9BB971079,
	BulletDataExcel__ctor_m3AD001FA730727CC9D77F1F49A422BB6608DC16A,
	BulletDataScriptable__ctor_m69A40930BFFB92DB09E1646B8EE55D05359E941B,
	BulletGameData_GetIndex_m967CC9430B4EE9701F2F05E601784853A9B485AD,
	BulletGameData_GetBulletID_m2CCF00E8446B24E9F88E640429D587CA5A226B61,
	BulletGameData_GetBulletType_mD9633E4E5FE3C31CBC9B25E6B6BC7D1E0BC3380D,
	BulletGameData_GetLifeFrame_m9589E20674DDD003B8A7523B9A759EE5126D26C3,
	BulletGameData_GetFloat01_m6A40EA1BF8C233CF7FEAA067DD6F88C28D8852F9,
	BulletGameData_GetFloat02_mEC1112E78D3DF4BD5182F419A8263F9054943734,
	BulletGameData_GetPositionX_m0CB657A9856A1BEE4844A383D5102B897F5EC8A9,
	BulletGameData_GetPositionY_m6C91A594EC110088E66022C23094F03E2AB40F5F,
	BulletGameData_GetPositionZ_mE9C1C33DD8A74861AC9C0FBCA5A4B123C5519BF6,
	BulletGameData_GetBulletEffectID_m8B96FEBEBA2376AFA357E9A5E200A347816725C4,
	BulletGameData_GetBulletCollisionID_m01ED1A70D5EE225DC6C9E0234F10B9EBC18D68F6,
	BulletGameData_GetBulletSkillID_mA9F814C5C6A5639A647761258C9F65EBF1AD7B5D,
	BulletGameData_GetBulletAttackCollisionRole_m3154AEECF509F24AFE95ABA836E69A5F9D096CB5,
	BulletGameData_GetBulletHitAttackCollisionNextID_m4AC16006CC36F7771A17DF97FB565D6A17006E78,
	BulletGameData_GetBulletBackGroundCollisionRole_mD4B2ED32BF3C4A94AC13F899D77DFF970C71DFCD,
	BulletGameData_GetBulletHitBackGroundCollisionNextID_m75C710CBFE13BB4C7D4623AB88AD82E559181401,
	BulletGameData_SetValue_m3CF2B1F0AC7781C028BD6B033B5DE4089771FB54,
	BulletGameData__ctor_m02A8F81FCBBBE016232DE78E7022401B7200B204,
	BulletDataStorage_GetAllBulletGameData_mCAF83799EE28D857EDCA7FAE8EC1AAB26F5F24A2,
	BulletDataStorage__ctor_mD8D6754B4511EDB6187E872395A6EE48C8DAA42F,
	BulletDataStorage_AddBulletData_m2AC3A305A06274FF9417F38622DE5392A5E70120,
	BulletDataStorage_GetBulletData_mBE5D776DE91F83093DDC823545146A9F7870AB85,
	BulletDataStorage_GetBulletCollisionID_m2DB11CAA26AE420FA916457AE06C18F910BEE31D,
	BulletDataStorage_GetBulletEffectID_m709CC023EF8F6421CC510C04BCC989BBE2A13828,
	BulletDataStorage_ReleaseAll_mAD57069463EFA156C357D3F64EDC3434E2BF4F98,
	BulletDataStorageManager_GetIsSetUp_mAFBE2164ECC2A8956B5AC0897E395A9BDAF772FB,
	BulletDataStorageManager_LoadBulletData_mA9244B8F73C48B7B6B53259EB946BE18BBBE58DF,
	BulletDataStorageManager_Update_mD8C9A49BB8D3C5F64BF7B2C538CD10F05E05944C,
	BulletDataStorageManager_GetBulletData_m667574A5D0423BE37A436B8EA3F7D36CA9CB2ECC,
	BulletDataStorageManager_GetBulletCollisionID_mF046AD57E87F3E2C889AA4ABFF9D21C5EF137023,
	BulletDataStorageManager_GetBulletEffectID_mEB5FD880FE0448C9645E0A289545212A723E3F7A,
	BulletDataStorageManager_SetUp_m58EE33D95435E822D63798CFA3C4747331AD6B37,
	BulletDataStorageManager__ctor_m802DF2E6E215C7CEAF79A4ECC1EB14A7294B7053,
	BattleCollisionCore_GetIsSetUP_m6B7BD6FB161AAD6D9FCC9BF2D74173820434D4CE,
	BattleCollisionCore_Start_m2D277AE93789CAA8CD9FE6E5C980D9D6E0CC0C9C,
	BattleCollisionCore_Update_m8BAFE23628D7578CF2ABEBB666CC27D993EB0B62,
	BattleCollisionCore_LoadUnitCollision_m76464F601587AABF02ECA645B95E8DF799A1B5F9,
	BattleCollisionCore_LoadBulletCollision_m61693DF80BE18D3F47BC34098D6E59D0D9D6978C,
	BattleCollisionCore_GetBattleCollisionGameData_mB6787DF90FE715588FEC8D3C5E040FCB85D988E0,
	BattleCollisionCore_CreateAttackCollision_mAC7919B83DD938BD45A783123E2B77BFEB38CC6F,
	BattleCollisionCore_OnDestroy_mA94CBD016FDA80CE113B193399B0839103F4C426,
	BattleCollisionCore__ctor_m664101FF29CCC0368C7F4A6711B5FA7B8C9BB85F,
	BattleCollisionData_GetCollisionID_m25DB5286FD9A6D4E6D4056B079BC851A0C480435,
	BattleCollisionData_GetFrameLife_m762BA9B2BEB99D1DA19B39D6943A0303BAC4F034,
	BattleCollisionData_GetStartFrameChange_mFB06EF787124DD3034DE4D3CBF57FE2FB0D4B0D0,
	BattleCollisionData_GetEndFrameChange_m703D7842547F2CEAE485B3C5B7F5D46F67F1A5AB,
	BattleCollisionData_GetBattleCollisionType_mA6AE20E4CA303B35F2B0E78776B3F49A49BBF64B,
	BattleCollisionData_GetStartPositionX_m1BB206AE280541C54D9953043DCF5DDD88D73BEE,
	BattleCollisionData_GetStartPositionY_m6558B2A63F295E6C0141965989460DB4A66ED7CD,
	BattleCollisionData_GetStartPositionZ_m0EB65E02701796E3A0E24C0DB766678CE1C3626B,
	BattleCollisionData_GetEndPositionX_m4D0A96B4A2EBE55DDF45FFA0F02EC35545F6B9EE,
	BattleCollisionData_GetEndPositionY_mDA89D3A40E5D72AAF453E9D59584C40CA3BA1F59,
	BattleCollisionData_GetEndPositionZ_m6413AB85769B2AA447327DAF74B2DEE643C6A531,
	BattleCollisionData_GetStartRadius_m4B04EADC372E1EE31BC8C5F53BF4D37983324B67,
	BattleCollisionData_GetEndRadius_m695E61210BDC9008DA8F4B62D2A0622EAC5EDF4E,
	BattleCollisionData_ManualSetUp_mB97FD31BE57BC89759DE9EBEE27ED13A7C32F243,
	BattleCollisionData__ctor_m2B66E2966C466A42969EDC78E29D6DDE1F9CFA4A,
	BattleCollisionGameData_GetCollisionID_m8953386D401714B92C279DFABCCD1754EAE649FE,
	BattleCollisionGameData_GetFrameLife_mA3C0C6DEF136DC36C97595BCBBB5FB04D545C658,
	BattleCollisionGameData_GetStartFrameChange_mA62DE0B4A0CCF0EDECE0571B147DDC0326B02C92,
	BattleCollisionGameData_GetEndFrameChange_mE9278563C03C2E30AB49B8A2E9B425624E7C0E2F,
	BattleCollisionGameData_GetBattleCollisionType_m3D8996CC0DA99ADC09E3ED3B6966D4EE95B61580,
	BattleCollisionGameData_GetStartPositionX_m5C852E61D79C4760B407914F6133E240DCDA9CD7,
	BattleCollisionGameData_GetStartPositionY_mD246C1BFB637E8FD9F6BF4AD2F9A84F3709FB923,
	BattleCollisionGameData_GetStartPositionZ_m64FE81ACD4F1269D3A167686391F703F2FBFC9C9,
	BattleCollisionGameData_GetEndPositionX_mD7B26DD81FDA8C83E74AEA8E12D755DB48026354,
	BattleCollisionGameData_GetEndPositionY_mA6209D4245CD15BD04356E8E8913D07F9EF81C8F,
	BattleCollisionGameData_GetEndPositionZ_mC13086D04411AC1557B6F760486CC4DA608B624C,
	BattleCollisionGameData_GetStartRadius_mED3460D5A37A0AC4DCD13649A7E947040D82309B,
	BattleCollisionGameData_GetEndRadius_m2BE43B2303B12290C93161B17E366D007D79636C,
	BattleCollisionGameData_DataValue_m45A8B4C25CB96B5429975D05FDA037153F5855DA,
	BattleCollisionGameData__ctor_m3022BF99CB12C3F6C5C2C1E1A89055FD4855CEA1,
	BattleCollisionScriptableObject__ctor_mBE7E3781836297DBD86267484300F2FCCFEE28A2,
	BattleCollisionGameDataControl_Init_mF48D19FF9BE6A7D2F0DB5199D7670FF185564F01,
	BattleCollisionGameDataControl_AddListMotionData_m15D2297041ED7AA40404395F1E27C15D0B1EC2A0,
	BattleCollisionGameDataControl_GetBattleCollisionGameData_mC70F94608FFC4A94A443B7D7CD82ADE87A92BBB5,
	BattleCollisionGameDataControl_GetMotionActionData_mCEDFCE0F5E4EB1F2E3EDA3247AEF3A7F2B2A00CF,
	BattleCollisionGameDataControl_GetMotionActionLength_m21785F6265197081D50F4DDC82E6A5092154623C,
	BattleCollisionGameDataControl_Release_m0FE474BFE900CCAB72B8774F488B8073D48BC888,
	BattleCollisionGameDataControl__ctor_mC526F8016077DAFBD2F2B5E2446B78287108127A,
	BattleCollisionGameDataManagement_GetIsSetUp_m8491F1D2DDDAE4B79A151907613B2A67206789DF,
	BattleCollisionGameDataManagement_Init_m7622D7AF7EF65518161E870E04C3DDCCFFD519F4,
	BattleCollisionGameDataManagement_LoadUnitCollision_m50125E04A3D32EDE164D26EDE35903728F85C39C,
	BattleCollisionGameDataManagement_LoadBulletCollision_mAEEFE4F0DBE17A8BE36AFC467F443D06C3607B5E,
	BattleCollisionGameDataManagement_Update_m89F37A663EE4CF5C0DE4239C4C4194EB26A28246,
	BattleCollisionGameDataManagement_SetUp_mD6AAB63A3C7248F176C0300394F25E864E677B1A,
	BattleCollisionGameDataManagement_GetBattleCollisionGameData_m7CFF3330B4C30135D594D0C979B3351EA07ACB44,
	BattleCollisionGameDataManagement_ReleaseAll_mE3D54489D716A8B47123340682583E441410690E,
	BattleCollisionGameDataManagement__ctor_m655D2E68B3DF1A4B4F5D5ADD861832509CF109C9,
	BaseBattleCollisionGameSubstance_Start_m4FF6E3BE7E5EC29CA72D4017EB2B489E97628415,
	BaseBattleCollisionGameSubstance_Init_m113FBE3942CCC8598E5158462A50E2BE692A2F22,
	BaseBattleCollisionGameSubstance_Update_m4154D9AEF8AF853D79EB4B0AA0ED6D29E341B4E0,
	BaseBattleCollisionGameSubstance_UpdateDetails_m6F475D788246A88AF59FA87A50421DB5082438F7,
	BaseBattleCollisionGameSubstance_SecondEnter_m9465964419E70A95B7AAEAA9A0DB8575FD7BFDF4,
	BaseBattleCollisionGameSubstance__ctor_m62F91F3C1FA3E7A644AD2BBAF5285AC59898D44D,
	BattleCollisionDetails_SetUp_mCCD806A4C9AD21E95DAB2DF2CD1D19E99F8EA476,
	BattleCollisionDetails_Update_mC9773316B411CCACBAC653F6E1AD5F75E9DBA563,
	BattleCollisionDetails__ctor_m342F9393C9618B8FF457079C24506B7F697B4CD4,
	BaseBulletCollisionDetails_GetCollider_m13E91ED7028E4A337454700498B0ACD560AB5168,
	BaseBulletCollisionDetails_InitSetUp_m6EE25A796C2A051599294C35ECE36F76B3B1DF4F,
	BaseBulletCollisionDetails_Update_m309912D4C14C8E31445D563C4D661967E953D35A,
	BaseBulletCollisionDetails__ctor_m2A2CD3EA383CA7E243A11A53C34A54994058B220,
	RpgCollisionConnection_AddCollisionConstruction_m6193C42C3535C4300C8E30A58180D39BD1B23DF5,
	RpgCollisionConnection_ErasureCollisionConstruction_m1A63504DC10F39897F08627829DB2F74E1213F48,
	RpgCollisionConnection_SearchCollisionConstruction_m0E4CCA53638B39B4D736319C7F1E084521D400D7,
	RpgCollisionConnection_RemoveCollisionConstruction_mE45BD9897E0D726DE90D371569492CF88AA26625,
	RpgCollisionControl_get_Instance_m3559C7C538AFB0C06887F4FDC9015DD53CEA59FB,
	RpgCollisionControl_get_ListAllCollisionConstruction_m7D3FCFD1736BD549CE8E6BB59F761767C17D95B9,
	RpgCollisionControl_AddCollisionConstruction_mB6F36BA68D0CEF9ED818DD8E76630A943FBACAEB,
	RpgCollisionControl_ErasureCollisionConstruction_mE9F607F5E974D1B473D1BE4E5F9D9555AAC6FACD,
	RpgCollisionControl_SearchCollisionConstruction_mD37DC741306A120BC964330766235395C7C7651B,
	RpgCollisionControl_RemoveCollisionConstruction_mA91376ADB4A934A8BE95B7C6EDCBCE35DFAD492B,
	RpgCollisionControl_AutoRemoveCollisionConstruction_m46B2C1BA95F64BFFDFC481417A5082ADF1F2F823,
	RpgCollisionControl_CollisionUpdate_mFC444534E8232A80BFEE83FD236E75BFFDB1BBD1,
	RpgCollisionControl_CollisionSort_mB5CB77C40DDCA80CD8BA075C87C11D369FA4D1D3,
	RpgCollisionControl_Update_m3AD09B079C14B37D91938082B9DB4899959B493B,
	RpgCollisionControl__ctor_m42383BDAC1A93D7B1DBFFFAF93CF2239A88C313E,
	U3CU3Ec__DisplayClass8_0__ctor_mC9AFB3568D69CA9524783CC3B37658DD81513182,
	U3CU3Ec__DisplayClass8_0_U3CSearchCollisionConstructionU3Eb__0_m44740300811AA8930BBF6C7D1BE5DD3CB139CBF8,
	U3CU3Ec__cctor_m5FDBC7E92CE89B0D57886F04114CB3C28AF5EC36,
	U3CU3Ec__ctor_m5393EA137E7C5A3C4884701DA465AC85F868682D,
	U3CU3Ec_U3CAutoRemoveCollisionConstructionU3Eb__10_0_mD2B62D3845780D00549BC162D5202C9D0ABA6370,
	U3CU3Ec_U3CCollisionSortU3Eb__12_0_mB57CA5C4755195880C1E708FBF69D4C9A83AB4A0,
	RpgCollisionCalculation_CapsuleCalculation_m29FB88798C5F006107D99C6608E57836881D5DB1,
	RpgCollisionCalculation_OverlapCapsule_m7233DB39980BCC584619FBDE982118B06BC50D99,
	RpgCollisionCalculation_SphereCalculation_m3BFE95B85DBAA978F259F066F201A681565B42C3,
	RpgCollisionCalculation_OverlapSphere_m8B74412BA7993734EB3BF5F191130A296921B086,
	RpgCollisionCalculation_BoxCalculation_m8CEEBEAD10A855974265720A8A613D85602F3CED,
	RpgCollisionCalculation_OverlapBox_m6DD8A427A0A231D695368D6BF99E4BE692744FA1,
	RpgCollisionCalculation_ComputePenetration_m14D92C1A9D6D866290C021402E94CBC377DAD352,
	RpgCollisionCalculation_ComputePenetrationThisPush_m740E6F9429FE9B2E4BE345F0C1B00229A351A3BC,
	RpgCollisionCalculation_ComputePenetrationOpponentPush_m3D065B0C1218A3EEC8F52FAD373EB9FACE22496D,
	RpgCollisionCalculation_BattlAreaCollisionCorrection_mEBFD977655A0BCCA1A2F7E9C2FB083AB5E721CD7,
	RpgCollisionCalculation_CollisionCorrection_m795C7781FA41EF45AD27570D0FE2E4D8CECD22AC,
	RpgCollisionCalculation_CollisionLayerIsHit_mBECFF1E4191FC26D1CB6F885E1A34D1C942983DD,
	RpgCollisionCalculation_Exit_mF7CBD2EA6F97FBBAA46C61F96FB28497173ECC43,
	RpgCollisionCalculation__cctor_m2D165E06F57FE3B724E7317E16A797FE60BFF39E,
	U3CU3Ec__DisplayClass2_0__ctor_mC8FD6F42302735004E30CBA33AC9C0B225D50D92,
	U3CU3Ec__DisplayClass2_0_U3CCapsuleCalculationU3Eb__0_mDD4FC68B122D2CE4C294CD7E496D6B4272553297,
	U3CU3Ec__DisplayClass4_0__ctor_m173B0EA8B1BB8860B66CAA3AF98F72F2C01B23C0,
	U3CU3Ec__DisplayClass4_0_U3CSphereCalculationU3Eb__0_m1983EEA351276C7531ED964E0EABD9A29500111A,
	U3CU3Ec__DisplayClass6_0__ctor_m69D9778B13B9E154521F3931C0DF8B0A81404B47,
	U3CU3Ec__DisplayClass6_0_U3CBoxCalculationU3Eb__0_m486AEA48F18561BFE36A7BB7C9BF11584A628CB0,
	BaseCollisionConstruction_get_RpgCollisionDetailsControl_m37795727C398D04AAE727CDD3783216287F0F88F,
	BaseCollisionConstruction_get_ThisObject_mD6B662D80688B2D3D8C81B55914D1798F680FFC2,
	BaseCollisionConstruction_get_CollisionLayer_mC0668605BB247715026889F8BDBC382B4B4FA1B3,
	BaseCollisionConstruction_get_CollisionPushType_m4219C2B39F7276841ADE1C8A6FB463D4105ED177,
	BaseCollisionConstruction__ctor_m3400A3C5024F33C1275E699830EBDD51094CB596,
	BaseCollisionConstruction_Init_mD9DE0608B9A288FE9D75A78656AAF23381247C74,
	BaseCollisionConstruction_Init_mF1E2837A225032986ABAA3522E72CBD76D263DC7,
	BaseCollisionConstruction_Update_mFB0832E5CB8E4A2161965577FD690B880C39D6B0,
	BaseCollisionConstruction_UpdateCollision_m72501A91DD1D3800B9FD4307D40EA2CABC3ABBFB,
	BaseCollisionConstruction_UpdateCollisionFlag_m0CE40374F5608783A0EE4377AD2CFA0B5A8C197A,
	RpgCollisionDetailsControl_set_HitEnterFunc_mFA74716E8E0A3F1D5E349915D493E348F93D8C88,
	RpgCollisionDetailsControl_set_HitSecondFunc_m1E1166A2B41953F097DACCA4C649F6EDA9085530,
	RpgCollisionDetailsControl_set_HitReleaseFunc_m3CB99570CA1BC5EA6AD6A0E9C51D811198FA3815,
	RpgCollisionDetailsControl_get_ListRpgCollisionDetaile_m1BD19C367E0CFCD5F8EB46BFC22F86BF8A8DD5B2,
	RpgCollisionDetailsControl_get_ThisObject_m5A98DF693E0E7A5C3125CFC34D0B7F427FB74E58,
	RpgCollisionDetailsControl_Init_m168F605D7CABD1FB1372B64C62A336E6A523D26E,
	RpgCollisionDetailsControl_Update_m93B2037225396D0B444650BA170DF8BB975F3732,
	RpgCollisionDetailsControl_UpdateCollision_mA95740080F7AA0BCE5F42BBDAC77F5F40446A6BB,
	RpgCollisionDetailsControl_UpdateCollisionFlag_m26487328954AD2F4810078BA921472A131D079BD,
	RpgCollisionDetailsControl_AddCollisionDetails_m133F77F8BF342FC4EBC91B7F66D4686C840A145E,
	RpgCollisionDetailsControl_AddCollisionHit_m7419273F6612BF6D0FC3B56515627685C2E5CCA0,
	RpgCollisionDetailsControl_CollisionHitEnter_m327898A49F8C8AC7875EB831004CD812D593818E,
	RpgCollisionDetailsControl_AutoAddCollisionDetails_m4FE58672D8A5142442DE745777EAB19DC6E08392,
	RpgCollisionDetailsControl_AutoAddCollisionDetails_m7848A9CC2D18A0350A413F1367013D7CB11A18AE,
	RpgCollisionDetailsControl_ErasureCollisionDetails_m8F71FD10B65E608BA84D80B013F020B619ABEF55,
	RpgCollisionDetailsControl_SearchCollisionDetails_m2CC0B2E676E13B03B2469435F3C4EA19C8C01252,
	RpgCollisionDetailsControl_SearchCollisionDetails_m339643242DD0F08C347249D340ADC62D69BE4CE0,
	RpgCollisionDetailsControl_SearchCollisionHit_m1093427B424A34AB749B51D19441E5E80630024E,
	RpgCollisionDetailsControl_SearchCollisionHit_m6790B624B162D551A3E5414540978FA1774B86F3,
	RpgCollisionDetailsControl_RemoveCollisionDetails_mDA051A4C77918CB79C5133900A1A39E7D2B0AB9E,
	RpgCollisionDetailsControl_RemoveAllCollisionHit_m6DEC609C815A2D8D5DD07E29525B07E312B73BB7,
	RpgCollisionDetailsControl_HitUpdate_m06127C7433D609ADEFFFB90A0D085FB3F5BC99EC,
	RpgCollisionDetailsControl_CollisionPositionCalculation_m77BA3554C48DBEAAA4BAFC5114E04954B9C09F40,
	RpgCollisionDetailsControl__ctor_m8B7342AA458DC6AD8600554BC215318F455242F2,
	HitEnter__ctor_m6CF8AF024D17561D5DFDB1A12B6859114FB08A96,
	HitEnter_Invoke_mBBDF6CF32359E3754CAC22E9C7EE861CC21946CE,
	HitEnter_BeginInvoke_m4A09B2D1296AF484FCCC02CAAFF540B87DCBF270,
	HitEnter_EndInvoke_m69B4EFFA40AE60631114B283707743D6DC72B861,
	HitSecond__ctor_mBE28E2149814B03E1017183C0E9FF0C98A7C5E66,
	HitSecond_Invoke_m313E7E2756BEFDCE17477ECA07E0CB70C57919F2,
	HitSecond_BeginInvoke_m97DC9D8E2714368C6A33F846D23F58CA18114303,
	HitSecond_EndInvoke_m915CB021F808E081BA7E400A4252DC1D94E9E2F1,
	HitRelease__ctor_m46DFDAA7445AE14B4D5BEFDFF8C65E60CC64BDC5,
	HitRelease_Invoke_m2B550F5E745258BC3EF45EC87E0367D7FE74BE7A,
	HitRelease_BeginInvoke_m0F68B1AF732BBCB48C26906490A1D27AFEBBD6AB,
	HitRelease_EndInvoke_mF2607D13D37A9C150DCC8FBF126DA2C99645C4AD,
	U3CU3Ec__DisplayClass29_0__ctor_mFF0540F2290B8835BC1C9630ECE2ACDC5799E857,
	U3CU3Ec__DisplayClass29_0_U3CSearchCollisionDetailsU3Eb__0_m8655A6E5BB065B1417B9C7C1987464168EE8EFA3,
	U3CU3Ec__DisplayClass30_0__ctor_mB5C361BED007B3C747C65C2EEF2A7DB7A17BC79A,
	U3CU3Ec__DisplayClass30_0_U3CSearchCollisionDetailsU3Eb__0_m31538AF37282AE512167555622B01141EAF0D09C,
	U3CU3Ec__DisplayClass31_0__ctor_m6B3A36B9AE26989CF3864A98F889F36D3A2DD4D9,
	U3CU3Ec__DisplayClass31_0_U3CSearchCollisionHitU3Eb__0_m5F69AF6AFE28D471F03D53DD171C01BAB2CFFAEF,
	U3CU3Ec__DisplayClass32_0__ctor_mF9823D5FB927B57C647CD43795F0A651B8B205C2,
	U3CU3Ec__DisplayClass32_0_U3CSearchCollisionHitU3Eb__0_mAE2C830D473B97F28001F548389DD48B89E64E5F,
	U3CU3Ec__cctor_mD2FE9F5B0D6B05F8F6A37A98525D793C495BEF39,
	U3CU3Ec__ctor_mCD506C7C4F7C48FEB2F8B8BE5C678EFB130EE772,
	U3CU3Ec_U3CRemoveAllCollisionHitU3Eb__34_0_m85CD96A470436FDEF3299E51B54CB89826AF244E,
	RpgCollisionDetails_get_RpgCollision_m6FBFFCA9A7FF3173EEC9C59E7E94F78B65521BA4,
	RpgCollisionDetails_get_ThisObject_m71DA4C87EB2F7FBEF2D2500991B6C2B5928D0FA0,
	RpgCollisionDetails_get_PositionCollision_mB48D18E388095D9771135C42AC237BB1D89705D5,
	RpgCollisionDetails_Init_mFEDD4E66FDF3FBD45DF4447B4D6B7223D6CF8AA1,
	RpgCollisionDetails_Update_m4D44D5473AF3920155C11B2147DA545AC9E60BB0,
	RpgCollisionDetails_UpdatePosition_m4578956F061126640A5253D6371BCB0FB0787841,
	RpgCollisionDetails_UpdateCollision_m3E0ACB761E6E3E916272432E7D0C0EF426F2B5B6,
	RpgCollisionDetails_CollisionTypeOverLap_m791292F919A8875CE8FADBC4F65F248A7BCD2F68,
	RpgCollisionDetails__ctor_m01E5B93CB82219CB0199FAE8F9358107DC1B80C3,
	RpgCollisionDetailsAccessor_CollisionPosition_m0184574C6A4CD611D83676F5516BAEECA692603E,
	RpgCollisionDetailsAccessor_SaveCollisionPosition_m625FA5A9F86E66AAABC85F7F17A0B8E07AE511E4,
	RpgCollisionDetailsAccessor_SaveCollisionPosition_m3B75DEABE44B458FBC0111C462F0B07696C2BC27,
	RpgCollisionDetailsAccessor_SaveCollisionPositionCaculation_m7B94C2411DA4F84653797D7408947DF26603E234,
	RpgCollisionDetailsAccessor_OldToNowVector_mA88E6B5355A7B0F57F04D6E0AD8403A1C1882870,
	RpgCollisionDetailsAccessor_CalculationPosition_m9D80FA81707EA00C861D0FE450F173C14CDC1DFC,
	RpgCollisionDetailsAccessor_CalculationPosition_mCCF004CEBF6E2C0FC38CE21FCE0243E564CA4AE7,
	RpgCollisionDetailsAccessor_CalculationPositionCaculation_m198EA6E83FFB7D2336E4D9CD009C70DED50B9B59,
	RpgCollisionDetailsAccessor_SaveOldToNowVector_m44BDC5E44FA05FF5C4B9CAB5422172B0B4B188B8,
	RpgCollisionHit_get_HitCollision_m8FE2FA6188CEBFD6BF6E0CA54D24FA40962D86F1,
	RpgCollisionHit_get_ThisHitCollision_m9235BF22A9A72E1F5E7EC50424DF9DB1553524AB,
	RpgCollisionHit_get_GetHitEnter_m760A5A334A7BAB643BE734DD51487EA12955F60A,
	RpgCollisionHit_get_GetHitSecond_m3EA7D0FC408F36B37898F13FF1D636CF46EB0EF9,
	RpgCollisionHit_get_GetHitRelease_m75E101423FF45196EB0F925E6EEDCCF1E0E47E34,
	RpgCollisionHit_get_GetCollisionHit_mC2A74DCD2D2DB78DB70D8CD808E1FAD546E52FE3,
	RpgCollisionHit_get_ThisObject_m0DECFD6319976A36D25222EE36BF474D4ACC45E6,
	RpgCollisionHit_Init_m4938D36B00405388E12250723713F962A7A9BFBE,
	RpgCollisionHit_FlagUpdate_mE071AEA360A91F9D44A3796C1A862058D51D06E5,
	RpgCollisionHit_FlagHit_mA2AB74B373B7E20529EB174A56F76267386C20C8,
	RpgCollisionHit_CheckFlag_m13B5A452CD8FC4E339EE79CC19B60FDFCA615E0D,
	RpgCollisionHit__ctor_mBE4B37123E83B516365BA5CC0C3B7CB1DA76F609,
	RpgCollisionHitFlag_get_HitEnter_m257CBE79504F92C1ECF93ABE48CDAD3CD26F2B4F,
	RpgCollisionHitFlag_get_HitSecond_m8E8E712162D6DDBB327AD00619E2183A51CFFB43,
	RpgCollisionHitFlag_get_HitRelease_mFE1C3707570CAC54E52B2D6AAC2B12ECECB2930E,
	RpgCollisionHitFlag_FlagUpdate_m5F11E0CF973615E77FDCFD2B77C4CF3C250DE899,
	RpgCollisionHitFlag_CheckHit_mCB097C9DB966A151007A6822A2FE724C56A8989F,
	RpgCollisionHitFlag__ctor_mBF024F0167C952FE475366474B2EB1DDFC9665BE,
	RpgCollisionPosition_set_SaveCollisionPosition_mA0E235F8F99A76C584FE52394CDB51C7018EB378,
	RpgCollisionPosition_get_SaveCollisionPosition_m731C117A5BA1AD97223D1B7BE3FA974A18AC1187,
	RpgCollisionPosition_get_OldToNowVector_mBBF1EA3CF55FB2C45266D1D65F86840314F43AF3,
	RpgCollisionPosition_get_SaveOldToNowVector_m447D3F6B750A2E63FD038DE9065CE27CEF2D922F,
	RpgCollisionPosition_set_CalculationPosition_mA92A936F995FE0E7DF390BA03B315437CE1A2DD7,
	RpgCollisionPosition_get_CalculationPosition_m4587B2D4A73FA5A097B4B92D65377B5D6AAF0DDF,
	RpgCollisionPosition_Init_m921424C62FFB3E9C59050EE86FE3215E2B9E21D1,
	RpgCollisionPosition_Update_m0C406F526AB227A937B92A360C149B269D82B983,
	RpgCollisionPosition__ctor_mAEBB4C39FFDB26FE519F1198420FDDD55896D0E5,
	DebugCollision_OnDraw_m4B5FFE37634FB91836F353F28293675E688DDE0A,
	DebugCollision_OffDraw_m9F1250D180410CBCFA6A8E67FC21118E81F7D39E,
	DebugCollision_GetRelease_m3FC32556C6EB5DDB398EA7945FE058C7CDC4AA63,
	DebugCollision_GetGameObject_m68E51B6F0A6E839FE93E416BF2535FE497FB2C0E,
	DebugCollision_GetInstanceID_mDE51A1F0D4884470CD59B8130BFEEABF39FD238D,
	DebugCollision_Init_m002933FBB3EA3D202C594DEFA46F3468EF4BA317,
	DebugCollision_Update_mDAEB91C6D6E5E0EA9384F1E13F8C742FB5791781,
	DebugCollision_UpdateColor_m741ADA16D9A6C7058B101C42861C72475780F130,
	DebugCollision_UpdateTransform_m2C2181618CB66A979B6D9EF9FA6F22E314DBA6C2,
	DebugCollision_DrawMesh_m92FF54B7284221AA0B77FB09E18CF2B118DB7FC3,
	DebugCollision_DrawDisable_mFC485AE3B36FAC1EC68D9EE464A53B4DC990D6B6,
	DebugCollision_Release_mAC17D22433C8B378A8440775C77DF9276040771D,
	DebugCollision__ctor_m0FEFE1592041B3D100A3FB2A5B491D13D9836C91,
	DebugCollisionControl_OnDebugDraw_mC5C379273DA76BC52A73849824B945127F3FED14,
	DebugCollisionControl_OffDebugDraw_m86AB2AEC9CCE19DC781A6455FF0A18F3503029EB,
	DebugCollisionControl_GetFlagDraw_mA4B1EAD16CCF15BE31850B06926E7B5DC73D0A7E,
	DebugCollisionControl_Init_m6A7A0777C959F611EFED355CF280E17DA37C5A73,
	DebugCollisionControl_Update_mB11A9314E43E9EC11C6D7A79400F1718B2672C74,
	DebugCollisionControl_ManualUpdate_m889FEA8E87BC427092FBCD9FD2DBD2E20B4838D6,
	DebugCollisionControl_AutoAddCollision_m60723983837257B24F3CDD58E042E61D710DC4A2,
	DebugCollisionControl_ReleaseUpdate_mF0677C507404F71883776C28854B29A2F5B618F1,
	DebugCollisionControl_SearchFlag_m1CC3E5BBB25EF26A1B7E0E92DE114DABB4D04F52,
	DebugCollisionControl_SearchCollision_m43CB033B7A0A355E722F48C36CB5375CFCF983BC,
	DebugCollisionControl__ctor_m4492F5CDF20722F084745398DE1D17DEEA9EE538,
	U3CU3Ec__cctor_m4F410A4F54CCFBAA0ED3CE9AA4258E91EB399DB6,
	U3CU3Ec__ctor_mD45947D00DDE60612DC1010FEA564FE00F4D7900,
	U3CU3Ec_U3CReleaseUpdateU3Eb__9_0_mC0B48B0BCEE6DB5CAF4302B28EFF65FF40F968FF,
	U3CU3Ec__DisplayClass11_0__ctor_m5C3F242BFE61F6A7DED4AA900B437050FAE58874,
	U3CU3Ec__DisplayClass11_0_U3CSearchCollisionU3Eb__0_m27CAA0C09AEE75A77E756DBDEB672F44FFF6CEA2,
	CharacterStatusDataExcel_GetIndex_mB8C41A1F5D2E6A524B07D7DB43E1901D9DFDCF9F,
	CharacterStatusDataExcel_GetCharacterDetailsID_mF826E048000F39E236C391F2D81D8D243D954907,
	CharacterStatusDataExcel_GetCharacterName_m7E76D3A4C7BE6FE28C152987781D6F14806D9237,
	CharacterStatusDataExcel_GetCharacterHP_mEF7EE4B5FF03805D0F722A68DBCE0FC601D4AF4A,
	CharacterStatusDataExcel_GetCharacterMP_m536C26DFF0A423CE88FC775C0A2E0BEBCECAC2DA,
	CharacterStatusDataExcel_GetCharacterAttack_m7C756E071D02E22FF077464F153C3262843D6CE6,
	CharacterStatusDataExcel_GetCharacterSpeed_m6474204350D98D6FD31E8704F08344AA79F6CC07,
	CharacterStatusDataExcel_GetCharacterDefence_m8FC247E42964FCDF109B450CAC7F27DD77A97699,
	CharacterStatusDataExcel_ManualSetUp_mD80A20E7DCAAE4EF668D9A1A6628C73EB5E863EC,
	CharacterStatusDataExcel__ctor_m028A7FD176CE958B0DBF78DA78DD5AC4AD0CA031,
	CharacterStatusDataScriptable__ctor_m8599DB7195453C1EEC57FAD49192A39FDDB23F87,
	CharacterStatusGameData_GetIndex_m13756F0C23865D764139268A59D20A0CA577955A,
	CharacterStatusGameData_GetCharacterDetailsID_m64D0DB746EF845F9E16A0DE94EEDA36254551C52,
	CharacterStatusGameData_GetCharacterName_m9074DC53447D53E120EF90DC069D3C6455DE5F74,
	CharacterStatusGameData_GetCharacterHP_m8BCC25F2C04A5D9436BCFADF3AE238522A1A5D36,
	CharacterStatusGameData_GetCharacterMP_m46A0077EE223E93B7DC3BC6B44DE11FA586170DF,
	CharacterStatusGameData_GetCharacterAttack_mD5BB2405EB021D67C350C5C091EC00343ACAF143,
	CharacterStatusGameData_GetCharacterSpeed_m91DC75D8C72E21893C687863A5814EAB2DA7C434,
	CharacterStatusGameData_GetCharacterDefence_mF929397249D7A330346AF206A1C513F00615135E,
	CharacterStatusGameData_SetValue_m9138688E6BD0001039C182FB116B7593EBAE2964,
	CharacterStatusGameData__ctor_m5524C06B528C01E9126B98B5D7F2CF0CEAE6CF13,
	CharacterStatusDataStorageManager_GetIsSetUp_mC4CECB30F017767E38DC076670C36928AC5EAAF1,
	CharacterStatusDataStorageManager_GetCharacterStatusData_m88C58E468647EE0AC1CE245E62195CB76EF3CFCA,
	CharacterStatusDataStorageManager_LoadCharacterStatusData_mB29123EB121B2D08CCCB6AD177B9D6AD6825FEFB,
	CharacterStatusDataStorageManager_Update_m073872583DA7BE058044AAB226CED9AE73D79DBD,
	CharacterStatusDataStorageManager_SetUp_mC9D1BDBA1455A4E9346B1D4855FB0F78957A4E9E,
	CharacterStatusDataStorageManager__ctor_mBCC563903950FEE2B60BF167CDC5CF5D6331EBC7,
	CharacterStatusStorage_AddCharacterStatusGameData_m48C84D9D615F84F3A819E5C7C3C736BAF51ED0A2,
	CharacterStatusStorage_GetCharacterStatusData_mEE53FCBB5E03A16A3DE86C03E66EF95932BA8183,
	CharacterStatusStorage__ctor_mD3E0F8AA459072A8186CBE98973901D085FAAC45,
	DataCore_GetIsSetUp_m0A8B8C0E5D1CF7B7CE872A18A2987D250E8AF621,
	DataCore_Start_m8FEB69CC4208C3A223B0C3F1B7396396C70081D8,
	DataCore_Update_m605200142D589E27A50B839A457B086AC7FD96E8,
	DataCore_ReleaseAllDictionary_mAD88AEE09CACA1A06F4B2F49906AAF3910679966,
	DataCore_OnDestroy_m470FCB60EE221CE33935F591D29C034412898F09,
	DataCore_SetUpDictionary_m0DF9AF67274DE0D17355E4044CD540F8978873E7,
	DataCore_GetCharacterStatusData_m0D9BFE3D3AB4861FDE76DD098801F7D0E5F7E20D,
	DataCore_LoadCharacterStatusData_m0255CE3643466BADC623480EEBE55965ADA59796,
	DataCore_GetListDictionaryData_m124D578213DF9AE42FCFB155C527E7E8638458EC,
	DataCore_GetCharacterDictionaryData_mB23206E79918D05B5F17E1728B8B4D7CBD1E4DF0,
	DataCore_GetCharacterDictionaryData_mCC5E730945753AFCAA20B2EB4DA521AF501B2F1D,
	DataCore_GetEffectDictionaryData_m9B3269272C71E6839C6B413F39CA288248D137F8,
	DataCore_GetEffectDictionaryData_m0429D7DE4EB67B0968D9CC7017AA073095546699,
	DataCore_GetAnimationDictionaryData_m12843100D835646B501AFD4BD751AE46C092FBE9,
	DataCore_GetAnimationDictionaryData_m036A35A52ED6B5BECFCF676D82D5FFE229AEFBC2,
	DataCore_GetMotionParameterActionDictionaryData_m002684EED9DBBF24BADA64285E9796AEE5A5CC3C,
	DataCore_GetMotionParameterActionDictionaryData_m217E74B7B153987DAFC7D7A1CB19DD54B7D7F01F,
	DataCore_GetMotionParameterActionDictionnaryName_m18C527108C2815A7062033C390023AD89EF14E8C,
	DataCore__ctor_m45A27E003FDC3A2134AF2A576290A0893277F0FC,
	DictionaryAccessor_GetID_m7684C8951AA1CBC2CAA606B8D76001D58F0E50FF,
	DictionaryAccessor_GetName_m4F86102B103F5DF66B81E8A2AAAD9451E9ADD663,
	DictionaryAccessor_GetDetails_m753BE1F723D716FDB54BDA0F3109FE157B014611,
	DictionaryAccessor_GetCharacterDictionaryData_m5B0A706E413CA222DC586B4CFEEB67E8A7C294DD,
	DictionaryAccessor_GetCharacterDictionaryData_mC82D073408213AABE1F9812EE19AA960300DF566,
	DictionaryAccessor_GetEffectDictionaryData_m2DEF734A419516E8E66481DE222CE8D916AFDACA,
	DictionaryAccessor_GetEffectDictionaryData_mE7C860AEBDA3A7665B8CDEB4061D6C50F79CE681,
	DictionaryAccessor_GetAnimationDictionaryData_mBF41EA7E7A04DD660948155F2D6B8ECF10B76489,
	DictionaryAccessor_GetAnimationDictionaryData_mD9D22AA88DD1A47D446AEB2B87E1E42AE7DAAEE6,
	DictionaryAccessor_GetMotionParameterActionDictionaryData_m41C51442F3478F3B20F11494406D6EA3DD2450A2,
	DictionaryAccessor_GetMotionParameterActionDictionaryData_m778D7A115E70D4BAFE6E1F342026F50825390107,
	DictionaryAccessor_GetMotionParameterActionDictionaryName_m836ED134790E004E8030F5B5DB4969840147DCB0,
	DictionaryAccessor_GetListDictionaryData_m27DAE0CDA95F2CD8F9A2B79290634129B2C29E09,
	DictionaryData_GetID_m746B5287B1FF36555D7C28EB17AA98B3BE60D4D8,
	DictionaryData_GetName_m373B190631BECE728BE7AD7984F86BA062C1F670,
	DictionaryData_GetDetails_m97CE55200B7CC7C6CF713BF085E0DD4B2DF11C34,
	DictionaryData_ManualSetUp_m30FD4500863A82D0D12A3250DACD4D2C134269C2,
	DictionaryData__ctor_mD23E41B802502D2CD7ACE86E185A640E436F23C1,
	DictionaryScriptable__ctor_m5032BEB1C0C4A4C6FFABE52049C8B080049D3685,
	DictionaryIntensive_GetIsSetUp_mC2E2470F0E6254CF710F03645336966AEC7EA97B,
	DictionaryIntensive_Init_m34F1EFB998071DD25F5BE02E481E7E2E5AC2F634,
	DictionaryIntensive_Update_m86DC2FFAC2701C22FC065BFF10983F752390B1B3,
	DictionaryIntensive_ReleaseAll_m3957B18FB2B9F41EF7F65939E4616AF7529A483F,
	DictionaryIntensive_GetListDictionaryData_mEED18B6E46091222C2BAB1D594BCFD38B1C09039,
	DictionaryIntensive_GetCharacterDictionaryData_m61844688A105D74D0FB709421019C5A393672FA7,
	DictionaryIntensive_GetCharacterDictionaryData_m8C453E9E9AF2F34D8D9FE60656507D9429419092,
	DictionaryIntensive_GetEffectDictionaryData_mC107A1800281F3F94AFBEB51C9B296CADCFA5321,
	DictionaryIntensive_GetEffectDictionaryData_m760E6B8E2775A2E291FEB240545E63E0CE9351A5,
	DictionaryIntensive_GetAnimationDictionaryData_m0B8D10BA2234435E7933ED4DE6ABF741BE8CB60B,
	DictionaryIntensive_GetAnimationDictionaryData_m3A274602AF8A720A72777F38296DE7316376E9CC,
	DictionaryIntensive_GetMotionParameterActionDictionaryData_mA995265BB84433EAD02E5874EBDAA4531D724542,
	DictionaryIntensive_GetMotionParameterActionDictionnaryName_mAE8A651E1B878C700ECD22186C838961AA60C27F,
	DictionaryIntensive_GetMotionParameterActionDictionaryData_m9FCB01FEAC3BBBAAC8411C5FA0313FB5120EC5DF,
	DictionaryIntensive_GetDictionaryData_m05759ADE85744C2A484CEE08F485999D634B7D49,
	DictionaryIntensive_GetDictionaryData_mFA2A3B01E01380BC70A60DDCD21897B61E9B288B,
	DictionaryIntensive_GetDictionnaryName_mC854EACDB357C4706149470E214F3B1FFF270DCB,
	DictionaryIntensive__ctor_m4494220AB0F432F098A8B111487F6598C7F9F6DE,
	DictionaryType_GetDictionaryData_m4E91EC1D07425D7B7E89BF9C168F3B1E5EBD97CF,
	DictionaryType_GetIsSetUp_m3B1EDAC50C04170393B4AD5DF2E18DDB0A8F1984,
	DictionaryType_Load_mBD2A3A3F2C472DEB2045E3DEAF23298A54C0E447,
	DictionaryType_Update_m060FF974AF4CE4DDCA4C503E5AEEE2DEA28BF029,
	DictionaryType_GetTypeName_m32A534E487EC269FBE4B5F185448FED05C971F87,
	DictionaryType_ValueData_m1CB5A9C603441A96FE896446A9C4739C667E9559,
	DictionaryType_SerachID_mA785B29B0914582B408AC9D78CBA8BF68727B8F1,
	DictionaryType_SerachName_m3EE6DD094F1394D3C7EA9B571D66874135DCDC70,
	DictionaryType_GetDictionnaryName_mD0BBCC5C1CF698D3F5EABF04B28BBF087DB3BBEA,
	DictionaryType_Release_mE15BF3A91C507A16C6A5382955282583E25332AA,
	DictionaryType__ctor_m948B3A5E0E952B20F40CC82E879023131BBE9FF4,
	BaseComposition_ManualSetUp_mFC6BF9A6B7394C2EF9CD91757397F5ABD82C8B2C,
	BaseComposition_CountField_m3482591D26EEEE2AA0BDC9542042EEF0836E19F2,
	BaseComposition_GetVariableName_mB65B081BE3A90A168A2131D8785D926C1C5A7857,
	BaseComposition_GetValue_m98D6BEAA683D5A5E368984926F0BC9F9D3F32E70,
	BaseComposition_SetValue_m3CDC2E78A64BB6E9E9CB668905D18A90C9AD2843,
	BaseComposition__ctor_mA047BEB8CDC3173DD1C7B3A575C011712CFCB3D2,
	DataFrame_get_DataName_m81205B4F31AF9F317715E1E1F5641710575F002B,
	DataFrame_get_VariableName_m8A742877D9BB6A1278C2D901A5E72BCA97178AA9,
	DataFrame_set_Data_m42EC3E451707481CED9859B7C87E7054691110AD,
	DataFrame_get_Data_m1ACCC0F5ACE88B76B716528616569B4EC647F7EF,
	DataFrame_SetUp_mF3EF36185252F0F065D30004BAF2A566EFDF8DDB,
	DataFrame__ctor_mD69CBBB27AD792B8C8C3F133A16F8EE860475460,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DataFrameGroup_SetUp_m51D0A89F04C6BA194C9E24FF9881C6712735A8FB,
	DataFrameGroup_AddData_m222094A6FDB29234C36D03E39E62A1F14BFEE899,
	DataFrameGroup_Clear_mE011D71D5675F6FCC73D51F10B32ED370859A2FF,
	DataFrameGroup_ValueData_m126533448E6DC55D2E5D6ECBEC60FE3709768E32,
	DataFrameGroup_GetSearchDataFrame_m76AC6F9F71E23E858F4DC613DFA49A4496B79C29,
	DataFrameGroup__ctor_mAD765F6C072B84BD8FDDBB5421F13F2357888E01,
	U3CU3Ec__DisplayClass5_0__ctor_m6A12B78BE79BE2997B791C9BC4AFD2F1C0A417BE,
	U3CU3Ec__DisplayClass5_0_U3CGetSearchDataFrameU3Eb__0_m4410D5DCC22D50668FBD8714D65F04DD432BEE4B,
	NULL,
	NULL,
	NULL,
	NULL,
	ExcelScenarioText__ctor_m138A3814195D23ACA2EA65D95932BA0A87A2997F,
	ScenarioText_GeID_m49DA8A7D661B1D82B4379CA1897415C5F6AD477F,
	ScenarioText_GetName_m33288A563BF19267C13C1782416B052BBC2AF22B,
	ScenarioText_GetText_m4266A056427F6F0DA9E25CB2C8C1825DE50AF2A3,
	ScenarioText__ctor_m1A95B5EF6D31D3AC32D416A63A87CF63CB01FB05,
	ScenarioText_ManualSetUp_m412661E58E400676494A56F92A70353226F7B38B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MotionData_GetActionIndex_m641F66C41B544BA23F15E6B0B53160797FF495D4,
	MotionData_GetActionName_mE5290A22C9B2335A830A380C9E6BB9C93B58DFC7,
	MotionData_GetActionRoleName_mB2D5E594BD6E57056042861CC15906F97F569412,
	MotionData_GetActionID_m41115383319DF933DDCCC2892921AFC4D87321C5,
	MotionData_GetActionFrame_mD7C5B2AB2D6C5B8E1B59B873637B3F80C704501C,
	MotionData_GetActionLifeFrame_mCF3B4D99586C0D373B73F0EF5C2AF0D5729555FC,
	MotionData_GetActionInt01_m9AB1F5EA4FB2FB9F1D40E54E19090A80FD6069E1,
	MotionData_GetActionInt02_mB8070BCA64DB57C920856A58163D00823DF5E02E,
	MotionData_GetActionInt03_m6F2628261D0A63218FAB1F939990F1D17042167C,
	MotionData_GetActionInt04_m2CE14490D9DF1859851B56843C0CFBC59C88DB9F,
	MotionData_GetActionFloat01_mE95740A24F3119357301A2E5C01D39B231A24E3F,
	MotionData_GetActionFloat02_m9FC3F68D343267BD93C5B5F4C72B550EB914C273,
	MotionData_GetActionFloat03_m5407DE1D24AE88B1AA1095E73F8D9269C452ADF3,
	MotionData_GetActionFloat04_m3940E23FCE5EC616DB4E002797C7F1DD5E0C9D93,
	MotionData_GetActionString01_m39818C4FD40FEA9ABBF39021C17CB521E5A9BF14,
	MotionData_GetActionString02_mB2779642090130C0D396841C72681829A473F3EB,
	MotionData_ManualSetUp_mB290665D65D668996652C9F761228E89A63F53D3,
	MotionData__ctor_m317D9BB4CB3FBBDDEF649F3CF7346C20DB752B91,
	MotionDataScriptable__ctor_m961832DE37C6327B6DBB69DD3EC9E6085C7A6648,
	MotionParameterConvert_ConvertMotionActionID_m2BC2EC4EE419F1281F620F168517EF7C824FC630,
	MotionParameterConvert_ConvertMotionActionNameID_m7756966D052FABF19F14B2D73F7AE5179F794E39,
	MotionParameterConvert_ConvertMotionActionRoleID_m2ACA18E6505050CD7982D3465F1208F705EF163A,
	MotionParameterConvert_ConvertMotionActionRoleNameID_m57AB5A036378D57A7B846095ED78D2BD087E5742,
	MotionParameterConvert__cctor_mBBD7CBFD4DED20CFA62861399F765B11D0ABC247,
	MotionParameterDetails_GetThisObject_m52ED51AD7348D4C14F33FEA7CDA186C16247FDD1,
	MotionParameterDetails_SetTargetObject_mDF4648226C2E8DD21D6A7879AF9EDC7C76FCB02E,
	MotionParameterDetails_GetTargetObject_m59F41575DCF8F33C168503BB1FE8CC7578A59904,
	MotionParameterDetails_GetIsSetUp_mB25657608598060FA44C1CC96424FCFE1F043ABE,
	MotionParameterDetails_Init_m9DA1838C982D980D51DD6B23A3225B0D8C1023DC,
	MotionParameterDetails_Update_m73240D7E10760ECFE43F32EC91C9F2DFB5412B84,
	MotionParameterDetails_GetMotionActionData_m79FD90A9FDDCE1D142D0C7CF55A1206DD6D64DF9,
	MotionParameterDetails_GetEffectIDList_m969C4F577789B102401602B2FAC91D2B4B7ED71B,
	MotionParameterDetails_GetBulletIDList_m4E1579A574529B6CDAC75CF446445DADBEB25662,
	MotionParameterDetails_GetMotionActionLength_mBCA6730A8A3B4C030226DEE46A3F2C9BC33A247A,
	MotionParameterDetails_DataInput_m6EF243B6B8FEF55E1160DA6E5D1B45CC5F864E53,
	MotionParameterDetails_Release_mC1086BAC98B59085935DF7B469F654F2D58A2920,
	MotionParameterDetails__ctor_m6FD5F1EC78B0D145D0E6CC59561B8B97E3249C17,
	MotionParameterDetailsData_GetActionIndex_m26E712184010FAA32EC0259D149E997FC5F9DC79,
	MotionParameterDetailsData_GetActionName_mC36AD17B48B738456D4F892C81E9D0E278AC861C,
	MotionParameterDetailsData_GetActionRoleName_mFF3BB3AEA49617080895D451515C0E90A9B88171,
	MotionParameterDetailsData_GetActionID_mD291906C6947291EABCDC9194543E2E9287720DB,
	MotionParameterDetailsData_GetActionFrame_mB985563B7402202A2531B19DB7D793B66096EE43,
	MotionParameterDetailsData_GetActionLifeFrame_m455CD9FC8E29F0AE09B79C280629090A8F7E1676,
	MotionParameterDetailsData_GetActionInt01_m42C43703A64FE226CFFBCDDBA51509B5667D329A,
	MotionParameterDetailsData_GetActionInt02_m60BEE9649ADE1EEDA66AD3E9EDDB8A940CB1A09D,
	MotionParameterDetailsData_GetActionInt03_mE9B1F0CAD393C60DEEDA8831B17494CB453FF7E2,
	MotionParameterDetailsData_GetActionInt04_m11EE893F4BBE4F1EB004E0D1C3178E9EABF31F0B,
	MotionParameterDetailsData_GetActionFloat01_m85A68FBB7BECDA4B20185A84E098423BC80C4378,
	MotionParameterDetailsData_GetActionFloat02_m539711EBEC42983C3B6FEFFFACE123E777952A83,
	MotionParameterDetailsData_GetActionFloat03_mEDDE752EE854131AC0A77AB8987DD4F4E5E101DD,
	MotionParameterDetailsData_GetActionFloat04_m6E946AFFBD8C576D6BCF1BCFDAE2FDF80FA8B8FD,
	MotionParameterDetailsData_GetActionString01_mCD40A4D81741026EF1433CF312ED8CE0F1801908,
	MotionParameterDetailsData_GetActionString02_mC7D78285CD33E1A18371E6DECC64CCB8E6B20AFD,
	MotionParameterDetailsData_SetUp_mB8682A8F6444801FD2650EB19BF07FAF465DBD68,
	MotionParameterDetailsData__ctor_m0DE2571A71E96C71169EDA68C8737A4D7312E0D0,
	MotionParameterDetailsDataControl_Init_mFD3A157CE36521B7F0D243B524DF4F525AE89487,
	MotionParameterDetailsDataControl_GetEffectIDList_mBAEA2CC54457414625B21720F8925EED2CB0F2A5,
	MotionParameterDetailsDataControl_GetBulletIDList_m5C94AFDAC14652101F7A75D3B1A5E2E47B367A79,
	MotionParameterDetailsDataControl_AddListMotionData_m7A7E7A704F70B9B50D9B62801899C08F1442C4EC,
	MotionParameterDetailsDataControl_GetMotionActionData_m34D6BA4C49BE67B086F6A0F4D12DFE5FA0B12FBE,
	MotionParameterDetailsDataControl_GetMotionActionLength_m5680B23A52927ED35616025221D9C077C84C772E,
	MotionParameterDetailsDataControl_Release_m4C5EF3CEF12DADAF3ED0C4242869F156254D3BFA,
	MotionParameterDetailsDataControl__ctor_m3D704BA8D726E5283465AEFF17E82D998A493F04,
	MotionParameter_GetIsSetUp_mAEBA047C7D4DC8B358CE0A6AC4E1B0E90DB7EEF5,
	MotionParameter_SetTargetGameObject_m268CFF7B817F795554FA3D5D27A9F2362C00876E,
	MotionParameter_GetEffectIDList_mC9B34F946D489316F4108886517ABE5735300EC1,
	MotionParameter_GetBulletIDList_m94867ACD4CEE92ED39488ABDD552BCCF44BFD3CE,
	MotionParameter_Init_mA450F1CCB85B39D4BBC7F9D208BFF8F6792A5A7E,
	MotionParameter_Update_m0AA30BA6A0CF2C81CA22ED5C597C4B886EA85EC9,
	MotionParameter_PlayAction_m3425B8C5B82CD92DE945FF7C3AD6F1C208A6723F,
	MotionParameter_PlayAction_m795C4EC5156580408CFDE85F05B4DA9042002E65,
	MotionParameter_Release_mFA7E5C01CDE48FDAEAC823A511CC01CD96AB7123,
	MotionParameter__ctor_m3E333F7411A9433A6540C75DEBA62B42C397512E,
	BaseMotionParameterRoleDataControl_GetLifeOver_mE15E2FA10B592F6FBFFAD0861E2FF76AAC6FDEBA,
	BaseMotionParameterRoleDataControl_Init_m7E0E7A502337EAE7CC67A335FA3DAF016296736C,
	BaseMotionParameterRoleDataControl_ActionInit_m68B8E535BD42D8AAD25688343D7F3A4EC875BD4A,
	BaseMotionParameterRoleDataControl_TimeUpdate_mC7EE3C7812EDBAA236DEF5356A212356322ACA75,
	BaseMotionParameterRoleDataControl_Updata_m8770212E6FE80E272D1B9FD839F1C01F666E71B7,
	BaseMotionParameterRoleDataControl_AnotherUpdate_m98CE1DA1B99D7C934295F93F38885C9D4F528261,
	BaseMotionParameterRoleDataControl__ctor_mDE5EB050C1C6DD8DF57E6AB449DBADBEBC07E2E0,
	BaseMotionParameterRolePerformance_GetRoleDataID_mF51583B24F60B40A90C60B8F0E7637D354FFB7E1,
	BaseMotionParameterRolePerformance_GetRoleActionName_mBC771DFEE0FC5C90C0EBDD72ED0DCFD079875B5E,
	BaseMotionParameterRolePerformance_Init_mF3E211556256E4DCBE51769008979E2BD81F4B0F,
	BaseMotionParameterRolePerformance_ActionInit_m30489F1105F9C63A569E087EE7678321AFC75A04,
	BaseMotionParameterRolePerformance_Updata_m4D51A2B20BCCD901A5A177147BF224F3CB2AB413,
	BaseMotionParameterRolePerformance_AnotherUpdate_m221E477B81558BC918D0396C83D0EBEEADAC23A3,
	BaseMotionParameterRolePerformance__ctor_m71E8A2865A1A80D7F0A742CEDAEC377C53E2A2EC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MotionParameterDataStorehouseControl_GetAmoutMoveVector_m9AA11670C659A6B863F8338C3A5EC99128682604,
	MotionParameterDataStorehouseControl_GetTeleportPosition_m4849C2D0FCCA97E0AA5167958BF045C717CAE773,
	MotionParameterDataStorehouseControl_GetIsTeleport_m9E5C2F9081EC409C69B7FF12022F58E3AEECDBDE,
	MotionParameterDataStorehouseControl_OffIsTeleport_m299E2D97FA425F0FE530875A784B765823A04FF7,
	MotionParameterDataStorehouseControl_GetIsAnimationChange_m203592F9736D587B86778B9E5FC8A4CF48DC6C58,
	MotionParameterDataStorehouseControl_OffIsAnimation_mCF62866249FC63643E57D0FC92CCA4B34FEA3BCD,
	MotionParameterDataStorehouseControl_GetAnimationState_mBC4785DADA71AD86CFC4A212D9A54B45FB2405D4,
	MotionParameterDataStorehouseControl_GetIsHoming_m05505DD3D3681DB05CBD498092E8FDC25EDD2A05,
	MotionParameterDataStorehouseControl_OffIsHoming_mA721C71BDA7111E8DC31036823FB8350E0DBE414,
	MotionParameterDataStorehouseControl_GetRotationHoming_m554420635048C94AB16F19692E9DC8190FB6C591,
	MotionParameterDataStorehouseControl_GetLookRotation_m8CA8EFDFDF9F9744DFD55188FE58339AA4DEF263,
	MotionParameterDataStorehouseControl_GetIsLookRotation_mF2FF50D983AED8E6FA141473B75C8B59A2043A61,
	MotionParameterDataStorehouseControl_GetIsCreateCollisionAttack_mC085682D8022D610CFCAF804F7325DEE25CEF4E7,
	MotionParameterDataStorehouseControl_OffIsCreateCollisionAttack_m02C2C4825790901AB0C762BAA2A835C5FF1D0308,
	MotionParameterDataStorehouseControl_GetCollisionAttackID_mCAA3888837BEADB620FC18CF784E3A8852D45590,
	MotionParameterDataStorehouseControl_GetCollisionAttackSkillID_mF5ED3B785D974F2B098ADCDB48E5DE12866520CC,
	MotionParameterDataStorehouseControl_GetUnitAddPositionAttack_m36B74EA0BBC7FC7745348CA4712BC9F1DFB8844F,
	MotionParameterDataStorehouseControl_GetUnitAddPositionSensing_mD3E247F991154532A49F5CCB7EC8C4224990C7AB,
	MotionParameterDataStorehouseControl_GetIsCreateCollisionSensing_mA7B97AEDA68EFDC6FD63544E70420315A746A768,
	MotionParameterDataStorehouseControl_OffIsCreateCollisionSensing_m224B32A382A3EA7468729729EA37375260E5B194,
	MotionParameterDataStorehouseControl_GetCollisionSensingID_m4B9652D19C11BBD41449D593DE3B4628AF780AF8,
	MotionParameterDataStorehouseControl_GetEffectID_mC84D8B4761F3C34D70E4B88C5990EEE9C21D4AC7,
	MotionParameterDataStorehouseControl_GetEffectLifeFrame_mF51640846F29D82ED5D09D2DB99D3BB854DE01DE,
	MotionParameterDataStorehouseControl_GetEffectPosition_mBF6C83B82E332E2112F9B955A9D88D02709D6AE3,
	MotionParameterDataStorehouseControl_GetBulletID_mABD30CB24FB5FC5747F25FB84CDFA002B4787C72,
	MotionParameterDataStorehouseControl_GetBulletDetailsID_mCC372AAB9C1EFD5761BBAD9FCB6139FF417F2CBA,
	MotionParameterDataStorehouseControl_GetBulletPosition_m0051C4E707D9A70EA89820B10A4003A998722275,
	MotionParameterDataStorehouseControl_PlayInit_m21192CF8692B2DC179D944E427FAC38B246B105C,
	MotionParameterDataStorehouseControl_MotionParameterDataActive_m569E252147B213BD4441AF1944253C755EB5C3B6,
	MotionParameterDataStorehouseControl_GetMotionParameterData_m75BB2EC18DA19014C7B1337530D6F88FB77E69F3,
	MotionParameterDataStorehouseControl_Update_m1859F134DAD8629BCAFEC5596301DAC2502CD6AC,
	MotionParameterDataStorehouseControl_AutoReleaseUpdate_m25EBAB00667A71CCF75ACF767AA8D0FA62970CC8,
	MotionParameterDataStorehouseControl_ReleaseAll_m96186D2A6BDAB11126A61345F9F89AC9DBAD1A47,
	MotionParameterDataStorehouseControl__ctor_m8671E71FA13E10451C01150910B94D0919AD5B85,
	MotionParameterRoleDataControl_GetMotionParameterDataActive_m90F2A52947E6FF00702096D8AF124250D42ECD9F,
	MotionParameterRoleDataControl_GetAmoutMoveVector_mD69063221FD57B96A9369275F0D1A718FFDFB0F3,
	MotionParameterRoleDataControl_GetTeleportPosition_m6F20D3845F833C65513FBB714131A7A65C2AD98A,
	MotionParameterRoleDataControl_GetIsTeleport_mCE76A879AADC000B4421AB16811879F031332BAF,
	MotionParameterRoleDataControl_OffIsTeleport_mF28D67C804C8D2A6BD6D79C555327A23AE0F33F6,
	MotionParameterRoleDataControl_GetIsAnimationChange_m143B9C32AAD4AE6939361796BCC89AEAB7C47F10,
	MotionParameterRoleDataControl_OffIsAnimation_mD0BA36639664C5FD87729903755E1B45D69FE277,
	MotionParameterRoleDataControl_GetAnimationState_m4D2E94C3FA3FC1C4ED0B5D1D55984EBEB1E533FC,
	MotionParameterRoleDataControl_GetIsHoming_m4075315CAF55BF60102D9A648DB864C06FD63071,
	MotionParameterRoleDataControl_OffIsHoming_m7C8F72958CF0055678E3A295A0B5CC2E2F258392,
	MotionParameterRoleDataControl_GetRotationHoming_m2D8584926E6F5D0C5D78A50B277968792B4DE017,
	MotionParameterRoleDataControl_GetLookRotation_mD7CFE1BC1928B585890B95EFE1E563395332E531,
	MotionParameterRoleDataControl_GetIsLookRotation_mD18D091EDDE0F5032292ECFE83D6F6FE9CD78299,
	MotionParameterRoleDataControl_GetIsCreateCollisionAttack_m1B22E7AA5880768932FA639E9818F6AAB11F7875,
	MotionParameterRoleDataControl_OffIsCreateCollisionAttack_mB03827A59F0D819F1D84360F3E24D4A3F3FDD9F7,
	MotionParameterRoleDataControl_GetCollisionAttackID_mA752A80372CB3D88AC268A7A0AFEBC6187932093,
	MotionParameterRoleDataControl_GetCollisionAttackSkillID_m60CE28615CF5CF8CE2B5449E6EFD5CF8E2031F0D,
	MotionParameterRoleDataControl_GetUnitAddPositionAttack_m690525C48A4F2191FD79C2CF66B0D3615C3673DD,
	MotionParameterRoleDataControl_GetIsCreateCollisionSensing_m3A8CF6735C5D461861DA24947A2386FD3E6E95C8,
	MotionParameterRoleDataControl_OffIsCreateCollisionSensing_m853A1CD28E3C199675C737471BC48CDF737ECA5C,
	MotionParameterRoleDataControl_GetCollisionSensingID_m5ED770352A7778B18A722708A0FF01DC50F844D3,
	MotionParameterRoleDataControl_GetUnitAddPositionSensing_m5710A869F26A0C0CAD2471F17EED47AAF159E3CE,
	MotionParameterRoleDataControl_GetEffectID_mE3710DB87B45024B2B06F90752F9CD82363EB923,
	MotionParameterRoleDataControl_GetEffectLifeFrame_mF325240013B00521B6B7B6E7E2F1C8C4031691F5,
	MotionParameterRoleDataControl_GetEffectPosition_mCA72C897E196A10C8065F02BE8EA7ED1EE349879,
	MotionParameterRoleDataControl_GetBulletID_m019C39A80F262C6BEA1A2CEECBB1CC76843ADF70,
	MotionParameterRoleDataControl_GetBulletDetailsID_m6A5A7011FC59F0C5F037032CC412A4BFBFA3374B,
	MotionParameterRoleDataControl_GetBulletPosition_m1F9004038817ED9B172ED53FA7645F5EF6F61E33,
	MotionParameterRoleDataControl_Init_mA494ECAE19067A9208252D6DA619765EAD58F64B,
	MotionParameterRoleDataControl_ReleaseAll_m5D092AA5B0F41B9140FA11D49CEBC47B039AB825,
	MotionParameterRoleDataControl_PlayInit_m664CAB8BCBA422137782E21D088767339E3B89BA,
	MotionParameterRoleDataControl_PlayUpdate_mE9DE15B81761713F4E97ABC788788AC4E23F5A7E,
	MotionParameterRoleDataControl_PlayAutoReleaseUpdate_m4675D4B5AC9E1174D7941FF0E19AFE21AC34D09F,
	MotionParameterRoleDataControl__ctor_m06723152219BE59DEBA09FB184CCEAA647E6CAA1,
	MotionParameterRoleDataControlManagement_GetIsSetUp_m26FD4E69DF6B7D7E3A9EBD2290AC1BEDADAB6A79,
	MotionParameterRoleDataControlManagement_GetIsPlay_m8FD4549394860DCB1FC5F1319015FC885BF28353,
	MotionParameterRoleDataControlManagement_GetMotionParameterRoleDataControl_m2F9FCE381127473252145CE1887803B6E3716B3D,
	MotionParameterRoleDataControlManagement_OnStop_m5E380117E4A38F73D5B982796322B02BF483E641,
	MotionParameterRoleDataControlManagement_OffStop_m04EA19BCC1F27B83B46B3F294A5CDD555A4523DA,
	MotionParameterRoleDataControlManagement_GetIsActionEnd_m307CBA65E6E480E8EE703A076A1499E27CB98405,
	MotionParameterRoleDataControlManagement_SetTargetObject_m888AE07B2BFE847E868319217E4E2ED1EE88EDE6,
	MotionParameterRoleDataControlManagement_Init_m087796116B34C9EADD93CCFBD14631AC106ECDD7,
	MotionParameterRoleDataControlManagement_GetEffectIDList_mCE091F5D99C208CC317EBF96D88FC8605D1ADD5F,
	MotionParameterRoleDataControlManagement_GetBulletIDList_m0BE270BC13DA295176FCABF16D15C7BBDA73F2E0,
	MotionParameterRoleDataControlManagement_ActionInit_m3607F2A8AC5A25688D54DA32D7075C94E8F56592,
	MotionParameterRoleDataControlManagement_PlayAction_m241A7DBA251A67184ABB7B3FB751B41C14D1384A,
	MotionParameterRoleDataControlManagement_Update_m0928330AEE4E34A2B93E199AD5DB5164C7F65F00,
	MotionParameterRoleDataControlManagement_UpdateMotionParameter_m0B4EB8A6F7B20B02DB04500DC5F98B1BD0D204B9,
	MotionParameterRoleDataControlManagement_UpdateMotionParameterRole_mD1CCBCBCE3372E5E22EA5018C6624AECAF552998,
	MotionParameterRoleDataControlManagement_ActionInitListMotionParameterRole_m594DAD60D79B0B730706A2F97751EBB0D522A3DA,
	MotionParameterRoleDataControlManagement_UpdateListMotionParameterRole_mF60737C9312BE5975BB21459089604AC38BFEDD0,
	MotionParameterRoleDataControlManagement_UpdateKeyFrame_m5B62C207B7ED27007D0383F2332198A4474D0C73,
	MotionParameterRoleDataControlManagement_Release_m85427AC70CD45B0D3AA7580B93F327B98848945A,
	MotionParameterRoleDataControlManagement__ctor_m6AA891107C11ECEB9AD4FFCAA51B23BE3B5EC8AA,
	MotionParameterRolePerformanceControl_Init_m429EBF53C9D62CACE0935097B575C04637D01EB1,
	MotionParameterRolePerformanceControl_PlayInit_m3DAAA831012E72FE8C402D36081CF78757D67A65,
	MotionParameterRolePerformanceControl_CreateMotionParameterPerformance_mC36EFFFDD98EFC5458B79AFBDF6FC6BB631AB762,
	MotionParameterRolePerformanceControl_AllInit_mC987D8D1E55CA5668DDA03B68653704A8E0C94A6,
	MotionParameterRolePerformanceControl_Update_m8B0191FCD8A94BA3CC44B2E9AB4E8C8A1AE6CB13,
	MotionParameterRolePerformanceControl__ctor_m9C7653DF616E70AB0BDEBE437B25430602A9935B,
	U3CU3Ec__DisplayClass5_0__ctor_mE389EF0DFCCD499A0E91A99B68B9C9E50ED3A81F,
	U3CU3Ec__DisplayClass5_0_U3CUpdateU3Eb__0_m900AD09B30041486660EC4C87A242C5EC970AC13,
	SkillResult_SetUp_m8C9FA215B5124BD96258DC81AC58459B6A931160,
	SkillResult_AddResultOpponentUnit_m4CC402A963050E31207BD7978029A58BD18D7F15,
	SkillResult_Calculation_m00F9BBED694667E2466597627F8A5C3F97D7BD98,
	SkillResult_DamageCalulation_mA2E72ED81372598129A9968C264AB56E5A694944,
	SkillResult__ctor_mEC7A1CE39AB21C71952A7C7C84AC4844E1A3268A,
	SkillCore_GetIsSetUp_mAA73BC2E2BA959BE4CD6892F27318A050A5EF308,
	SkillCore_Start_m0F4F3746DCAD38B830371936CAF1D4D026431FF2,
	SkillCore_Update_mEF63CEA89881AE60D5AE240A74D4A303DE728900,
	SkillCore_GetSkillGameData_m55D2F714299AD325E4B26F334E6C25F3BC550463,
	SkillCore__ctor_m1D2268B41C6CA28B4CE202E5FB20C47990E4AA1B,
	SkillDataScriptable__ctor_mF0A46AB43F2CF459716BB4FC13C1DD4E7AEEE582,
	SkillExcelData_GetIndex_m0E896228A514C62C0BFBC1B5D6522F2E7D0A75E4,
	SkillExcelData_GetSkillID_m90E0FE4E2020177670951B05EDD599A0A3DDCB1B,
	SkillExcelData_GetSkillName_m15A45A37687E6625DEEF9BD7784613A91618F191,
	SkillExcelData_GetSkillType_m9676536261E6C8C78205C5FFCE3E422ED7FD02B5,
	SkillExcelData_GetSkillBuffTargetType_mEB0EB2C5D157E1977AB93C4040901BD797111238,
	SkillExcelData_GetSkillRangeType_m53E2D7C76961E623855C5F7357A4FFE87E58353B,
	SkillExcelData_GetSkillPoint_m3FF5B6E2F5AE461A7F1F029ED2854D5CC25F1D0D,
	SkillExcelData_GetFloat01_mB46D2D5681BDCCC7FA864090BD3362971922AE2C,
	SkillExcelData_GetSkillExplanation_mC94D3203C2BE001D2D74668F88C754D1A34D7CB5,
	SkillExcelData_ManualSetUp_m639A2B6C61B944DA1B3A0C8389A7A419EBD8CFDD,
	SkillExcelData__ctor_m516C65323E754307EE55D0F919B6655D12E3425F,
	SkillGameData_GetIndex_m2FE7432E7DCB509E28868D0B84E783FCF8AE99FF,
	SkillGameData_GetSkillID_m6214990EB5FF2504C08286ABFA5D544BEDC58DBB,
	SkillGameData_GetSkillName_m5D9F1E5F4559F788A7B7359EABBCB44C43AC404B,
	SkillGameData_GetSkillType_m54EC91FEF8E9FB2E1BEC319EA1519B59A625682E,
	SkillGameData_GetSkillBuffTargetType_mE845CB5E3A68C180B5A240FEEC2BBCDD5C03C024,
	SkillGameData_GetSkillRangeType_m362C1F55839CAB115B428B4BE5034D826197CD7C,
	SkillGameData_GetSkillPoint_mED2C11D0C068073BF10BEE4C2055FF2D51B69856,
	SkillGameData_GetFloat01_m3BD95365627B68A706D0F785290B36BC6E5C81AA,
	SkillGameData_GetSkillExplanation_m70CB9F35985A589768BC7284A1A3C92D56C707D3,
	SkillGameData_SetValue_mD79441B5F28B4B54E97227A74A278C2F511A6986,
	SkillGameData__ctor_m986D4D9F3EA61C584AC7A867E22BF52409ED9840,
	SkillDataStorage_GetIsSetUp_m29A264A109CA211F19B17CB866AF7F7A671C746C,
	SkillDataStorage_GetSkillGameData_m579443DC6BEC5DC0BB0FF0FF9D7609FE668649AC,
	SkillDataStorage_Init_m5097204E999EE8A97E4F1019EAE9F43058BFE6AD,
	SkillDataStorage_Update_m197D558049C96A883D19F6D5839F886C0545C7AC,
	SkillDataStorage_SetUp_mA29CF61DB294A2BED2BAA79117CB5156F6C33013,
	SkillDataStorage__ctor_m5EE115FEF954121873910957FA51F12661B668A6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UnitAnimation_Init_mC3F7D21695D2998E73B27B8501DA6F6BB17FFE7B,
	UnitAnimation_OnStop_mCFC7315162F0CADA5E58D280AE4359C90739845D,
	UnitAnimation_OnPlay_m92A3A8770001B222375B2694BAC1058846EA2171,
	UnitAnimation_ChangeAnimation_m53E39A0CB1BDB2DB52C3658406BD7522D1188174,
	UnitAnimation__ctor_m463FFEFBE7DAD94CEC2FE24277749D3DAEEC144C,
	BaseUnit_GetUnitID_mB91F3B1776DF88A7149C21FCD382A8ADA33FD1B2,
	BaseUnit_GetUnitDetailsID_m950BE89E4B6FF92E6F43854870B0E34B70446149,
	BaseUnit_GetIsSetUp_m2F2B8A5EF1916F1AC2F55C1A472FE17C1E0B4789,
	BaseUnit_ChangeAnimation_m503B51052984BEC9216D68A59A515DAD483E038B,
	BaseUnit_InitUnit_mC5F5447CCFB14F61B2C050FD8D28CE869A065D1A,
	BaseUnit_Start_mA1B438CD3809BEC0A19632783F936FB96C46470F,
	BaseUnit_Update_m60C293086D8949269871D861C16401B5AD9BA5CC,
	BaseUnit__ctor_mBDFE5DA5AF152AFF86D245E01FD4E8F388923296,
	BattleUnit_GetEffectIDList_m0CACEF38E72A50827AEB87BDFEE56D9C8A81445F,
	BattleUnit_GetBulletIDList_m1332A40F7C2E4E969B13F11FA53644A0E6492870,
	BattleUnit_GetMaxHP_mA954A7C4F2EF0CCCD19DD05BD77B62E3091D9632,
	BattleUnit_GetMaxMP_m330E7665391BF719F551D1EAEFFFBC9ECBABE82D,
	BattleUnit_GetBaseAttack_m80FB2271AA6F5947E122C3C7663CBB6D5C2C8C99,
	BattleUnit_GetBaseSpeed_m791FA6CC8D67567655DBC7605C64038C6D5A3318,
	BattleUnit_GetBaseDefence_mE6F0F9D8A8B0ADC4EF1A57B6C981350129D03192,
	BattleUnit_GetNowHP_m08A53ADD02DA043907D732E54DDA92A071A8D43F,
	BattleUnit_AddHP_mB443ECC2245929196B54D5D42E4DF32C09F89B11,
	BattleUnit_GetNowMP_mA016C04B2BA2276D93426AA45EBAAFDE5C5E4FF1,
	BattleUnit_AddMP_mBA5B38352C90DDC4B9B4EFC3A48AD8041BEED40A,
	BattleUnit_GetNowAttack_m50F6F5ABB0DF19D27A7DD965CF3A4476E44D32FD,
	BattleUnit_StartAttackBuff_m5C2D3EE6C07B63C3EEA0E22988BBB4E7B993B1ED,
	BattleUnit_StartAttackDeBuff_m7EFA079649B8443DD99CDB61877EF4DE01836C89,
	BattleUnit_GetNowSpeed_m5354A86A7571B6A2FEA53AC6AB22BF0B41DE3F44,
	BattleUnit_StartSpeedBuff_mFA5C8BFD228ED521BF98AE3AB8429ABA9FB36411,
	BattleUnit_StartSpeedDeBuff_mB2D5112E5B1847420AF85041C3F103E697E911F8,
	BattleUnit_GetNowDefence_m53F1EE21EA81F87DC212061C32BFA31CE10C9EFC,
	BattleUnit_StartDefenceBuff_mC44BA4B63C5DC2694E3D484B7CB91AA7E5228E26,
	BattleUnit_StartDefenceDeBuff_m19827344E032414C77A3F00F470E81C133647E1F,
	BattleUnit_Start_mDD4EAD79CCD20B11ED169E860F261B3CDF31E0EE,
	BattleUnit_InitUnit_mFBD89AA3053A3C8703FAFD3C20B6A7436AD293C6,
	BattleUnit_Update_mE9E55193434E64B4EA1705C0B258DD794478A794,
	BattleUnit_SetUp_mE389972FE4460824DE7B7C02644DE73348334B4B,
	BattleUnit_OnDestroy_m6CE8EB6DE1CFBEFB229B061A056FDDB576D57816,
	BattleUnit__ctor_m25A425D03AA6DC0E066BDBE5A4CBD7B21C64909C,
	BattleCharacterStatusData_GetMaxHP_m160BD5D552D7BBD3FC11A04B453747618453291C,
	BattleCharacterStatusData_GetMaxMP_m310643A9561085C8103D8A997B2ECEBA5A9BCB83,
	BattleCharacterStatusData_GetBaseAttack_mCA167B4B1A3A536A946E230B777CFAC7562DC54A,
	BattleCharacterStatusData_GetBaseSpeed_mDC97E4E0D664091DBD5AEEF0182BA001A6E4D1E8,
	BattleCharacterStatusData_GetBaseDefence_m4C76ABE957A7AA3D535ED2D6B962C712D760C04D,
	BattleCharacterStatusData_GetNowHP_m5B6E8EC753397B8A02B0244C97CA03620B8DF310,
	BattleCharacterStatusData_AddHP_m5755A93277DA88BD5E0C4E0020592335AEAC7D36,
	BattleCharacterStatusData_GetNowMP_m124413E878FF826A1C5DF981A1FBBA052E41956E,
	BattleCharacterStatusData_AddMP_mCA11A4B498030834D727504AA1A8EB5E7193B9A2,
	BattleCharacterStatusData_GetNowAttack_m3FF20643C4DAF374CCFCA174E6DDC39B42880830,
	BattleCharacterStatusData_GetNowSpeed_m98C56210118F3EAE555CE1B11A5A26AB6E566E75,
	BattleCharacterStatusData_GetNowDefence_m0B9A09CDC152ED3B90AE801721C029CCD21AFCDB,
	BattleCharacterStatusData_StartAttackBuff_m54D865746CC27BCF93F0D048202D0DF74A1A0256,
	BattleCharacterStatusData_StartAttackDeBuff_mA1757D629EBDE73F382429175853343AB8687B16,
	BattleCharacterStatusData_StartSpeedBuff_m81623B577908025CE75AADC815F239573815EFF9,
	BattleCharacterStatusData_StartSpeedDeBuff_m5C7C22E7C6FC4F5FFB7701892B604B24106F8593,
	BattleCharacterStatusData_StartDefenceBuff_m236EBD077D5AD146BA04F754681DF31A322F4EB2,
	BattleCharacterStatusData_StartDefenceDeBuff_mE5E31E1F364ACAE702C9B2846EEE79409F5471C9,
	BattleCharacterStatusData_InitSetUp_m759F1A8D142EAE0E1FCDF0DB6BCC6B7021C35408,
	BattleCharacterStatusData_Update_m218D4F4FE0394BFBB0DEDFFED62DC022A3CECA65,
	BattleCharacterStatusData_BuffUpdate_mFAF4D3B5B8C6EB8B494458518F0C6FF962E00C18,
	BattleCharacterStatusData__ctor_mEE236A6058FE1AA1960F079D0881FC7C9A9ECBC2,
	BaseBattleCharacterStatusBuffData_GetAddBuffPoint_m8B413E1BE3003B2C3F7461462FC9CD6F9511F97A,
	BaseBattleCharacterStatusBuffData_GetBuffTimeOver_m9A9D3F8C7CF61FDB1CAAD0606E3B6D4B3DD781B0,
	BaseBattleCharacterStatusBuffData_Init_m5BCA264733588125A9704CEBF21FA4DCFE247EBA,
	BaseBattleCharacterStatusBuffData_StartBuff_m0CCDBE64B9399781AAB466FF662D1E6416022768,
	BaseBattleCharacterStatusBuffData_Update_mA650DDB263D95E1B542C36AA14270D84B426E389,
	BaseBattleCharacterStatusBuffData__ctor_mBD9CE8F145D7F90B843384C2F4014D65C6C6F50D,
	BattleCharasterBuffControlData_GetAddBuffPoint_mBB4582A38E88A5332FD83621801CC361AD3E60C3,
	BattleCharasterBuffControlData_GetBuffTimeOver_m50C5B15A4E0BD129E6EBC8087188DA1AF029753F,
	BattleCharasterBuffControlData_GetAddDeBuffPoint_mDFF555D2E1EB11CC02E5BCC820CE603C429B2DDB,
	BattleCharasterBuffControlData_GetDeBuffTimeOver_m3619E091E3C89736F6A70165DB3E568CE7A0F9D8,
	BattleCharasterBuffControlData_BuffInit_m36D7794B73BD4E2D46687ADB35C70F617BD0641C,
	BattleCharasterBuffControlData_DeBuffInit_m775C17E7612DF5A7843BED9A24E0DE986346B458,
	BattleCharasterBuffControlData_AllBuffInit_m7153D46BB2DE419BDCA2345957C4FC644B3531F3,
	BattleCharasterBuffControlData_StartBuff_m0C9468B58641FBB8686D8DCC3F4CCAAE17D36060,
	BattleCharasterBuffControlData_StartDeBuff_mC01E7EC0C736F9014185DC3F3CEBA6B5AEA438D2,
	BattleCharasterBuffControlData_Update_m79835A0EEC3CBD0471B252DC37FC4C1BDB036438,
	BattleCharasterBuffControlData__ctor_mBC87F1189EC39F246DC20F600E48E5C71DF4768F,
	UnitCore_GetLoadUnitIDList_mAAE25F0EA0C1326DCE1A867BD53EC65A5F4CA916,
	UnitCore_GetIsSetUp_m2C6F5B617BBE81EDD1B38AD8C2157D2B75812D11,
	UnitCore_Start_mB1A62BE7DEB34FF9A9B889671EB2F0619CC5D2F9,
	UnitCore_Update_mAC49C509E3DF7BA3C81533242456AF118F42530C,
	UnitCore_CreateUnit_mD182DCF3CE3423EA387E2F5BDC2204F0F4708605,
	UnitCore_GetUnitGameObject_mB458A8F91C52B5A559E2F9C3B64E42295C218225,
	UnitCore_OnDestroy_m52C3F34F67E77C155B595CA2085CCE073B86A281,
	UnitCore__ctor_mE327D1F81384B8DF080950E36B3EEE1B7A56B370,
	UnitManager_GetUnitGameObject_m0839C79A09A92561499A9744501CBC7BDC9DDA5F,
	UnitManager_GetLoadUnitIDList_m7B57ED951D1D6E88F5E0FC47567C0E8B712FDC63,
	UnitManager_GetIsSetUpLoad_m3A3AEB481BF9D2D744E8A31FA8F774AA8D9CDCA1,
	UnitManager_GetIsSetUpUnit_m56C00F8672AB0670EE33B9AFC13AD4174DFBA657,
	UnitManager_GetIsSetUp_mC33319257B08F0219E41C0853A8C6FBEEB9FCEDE,
	UnitManager_CreateUnit_m9AE62AF56791F03521738ADB7F38B3FBA86D835A,
	UnitManager_Update_m8B5212541BE1DA2D668C457E17CFA039DF49527E,
	UnitManager_LoadUpdate_m7F0C5CA6E3CEDC81B241C00813E30C558D89DA4A,
	UnitManager_SetUpBattleUnit_m6A3265009F97FA913907B9344630F44B67E07050,
	UnitManager_ReleaseUnitAll_mE3E6C0D5FF3C9F6D58955A8D3942378202A02B05,
	UnitManager__ctor_mFBA75CB247D818525EAC92DE6B65CC429F368026,
	TestPlayAssets_CreatePlayable_m6F6EA858CD5B5DFD408576CD3F6F645C56A81F34,
	TestPlayAssets__ctor_mA87AF921086D2ABE3F8C34114D70C4C7F95C7756,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C,
	SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A,
	SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A,
	SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87,
	CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647,
	CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96,
	CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326,
	CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410,
	CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D,
	AnimationChangeData_GetAnimationState_m631544C80C2DC0D5CF5C27BFFDFA757D4EEC92F7,
	AnimationChangeData_GetIsAnimationChange_m585CD2F8A65D491F14E3227319F4639337CE0007,
	AnimationChangeData_OffIsAnimation_mC7DCB9541A7F85F04C81BD28CF21D6D6BF681B8E,
	AnimationChangeData_Init_m8040C63D92D22CA9B755B0885988E1B77DC35AF7,
	AnimationChangeData_ActionInit_m8FC0807148E67A9DF513FEA3E315B4CC59051E8C,
	AnimationChangeData_Updata_m4ADA82259119D70E45F8773D6FDFFC809F6FACEF,
	AnimationChangeData__ctor_m218901DBBE91289CE7EB5CADB5A6C10C1BE81000,
	AnimationChangePerformance_Init_m390490A7D76BDE2F8B5D0A3147ACD42BC1FF5234,
	AnimationChangePerformance_ActionInit_m14D8DC80CE648B60721283EF9BBCDD2D340D8743,
	AnimationChangePerformance_Updata_m7051F3CBDBA8CE692EA09F05B5ED539DC9AD3DA5,
	AnimationChangePerformance_AnotherUpdate_m9D6801A59607157462028EB136635B4E9DD5373C,
	AnimationChangePerformance__ctor_mAF8E3A7C6BFAF315BA538DC6FD2C940F6ADF1C50,
	BaseCollisionData_GetIsCreateCollision_m21D22FA182D0C6A248F8E8E3EF426CB100995B8D,
	BaseCollisionData_OffIsCreateCollision_mC068C4CB0BE41ADFD31AE13143E6F66CAD5DE89C,
	BaseCollisionData_GetCollisionID_m523C34B5A1802542EA3F4313A070CBA44E73FF65,
	BaseCollisionData_GetUnitAddPosition_m0549769B99553ABA59AAAB627F5E7FA59C69CEF9,
	BaseCollisionData_Init_m66828F601B62967856794B7BA2637E240C377E3F,
	BaseCollisionData_ActionInit_mF4886EDFAD91C3E2B6299815E001E36093E69C4D,
	BaseCollisionData__ctor_m6F7456AF1E01ECB7B772B9C36C469BEB174ED830,
	BaseCollisionPerformance__ctor_mAE79AD3842D4ECB050FE0C8B6293FA0AA4B285FE,
	BulletGenerateData_GetBulletID_m57099A022968AAFF01C563DF9BED5D7453D95FF7,
	BulletGenerateData_GetBulletDetailsID_mB520050BD140EC73C88E1DDD0E83E50DD7064447,
	BulletGenerateData_GetBulletPosition_m7400A3AEDE227E84E46B4D2B40279742333AB38E,
	BulletGenerateData_Init_m398C5B7CFBC19B8E9088748B8AAF707468F74C8F,
	BulletGenerateData_ActionInit_mEA08598CC9E9AAA23A8B3BAFDCADD34875033926,
	BulletGenerateData_Updata_mDD357712EB7CA2B8138AB81DB51C2E7DBEB61FFE,
	BulletGenerateData__ctor_m1A12BF40D4C0018B9386F191BE9DD8079A016146,
	BulletGeneratePerformance_Init_m6BA4E0B9479CF2B716A5C038DFA5BFDEA0BF36ED,
	BulletGeneratePerformance__ctor_mB349AF62C250D620BA25A2230DBE1168F28A680E,
	CollisionAttackData_GetAttackCollisionSkillID_m0279BBBCA3C03DBF642F8AED64AF7CBBBD5B54E6,
	CollisionAttackData_Init_mDBEA03DB8A6F1FC195E097506D3D1A21CBF10732,
	CollisionAttackData_ActionInit_mCD32080B8A77AB76D490B25E15F9A8FBAD23C84E,
	CollisionAttackData_Updata_m03D72A6DE9BFCB2AE7F5069DDBF33739979E5AE7,
	CollisionAttackData__ctor_m5D0BB48CC041177EC87ECF31060CB49EFC5F5C82,
	CollisionAttackPerformance_Init_m845969B6F2BA8BD51FB6F45B37E693709EA26147,
	CollisionAttackPerformance_Updata_mC34BA1074E3ACB8E856AD27191E681A80E5C0F69,
	CollisionAttackPerformance__ctor_m23C7B764F85277646DC484050AA5F379616EB73D,
	CollisionSensingData_Init_m7A7E88B3F62F96532131CF9B3F1B2E49C4E32E6E,
	CollisionSensingData_ActionInit_mD42ABB3C0A7C2F9D223AE305E57F244A7112425F,
	CollisionSensingData_Updata_mF39226409050DCD3D35092CD1B6E3B928200A13A,
	CollisionSensingData__ctor_mF402733ABFEDCFDDC3A467DE8F34CB9B8A5B9525,
	CollisionSensingPerformance_Init_m5C98CC333FC74D45E2B70F34286FEE7C61D4FF52,
	CollisionSensingPerformance__ctor_m472E6D69379EFA458F18639F38E47051E6C8C543,
	EffectGenerateData_GetEffectID_mC0100FDE6363FFD5215F341C52FFDEA22B485E44,
	EffectGenerateData_GetEffectLifeFrame_m6D7E6055940F2129FF97D6F1A0568AE38DD49DC0,
	EffectGenerateData_GetEffectPosition_m946E1662C063DCBF83E80F388E9BC5C52EFF71E0,
	EffectGenerateData_Init_mB471363848B1A2F268F64A8F1C400E2D990945CC,
	EffectGenerateData_ActionInit_m186BC161598998ED1DFCC103AEF0CA014D82D61C,
	EffectGenerateData_Updata_m7ACEB0BD2C31C2DFFC8498530902362E5B5A1614,
	EffectGenerateData__ctor_m3706DF7E00732756A54B45F988286991092A15AB,
	EffectGeneratePerformance_Init_mD538ACB96BA9D3EF41E6F11A4C513D85B025D8DC,
	EffectGeneratePerformance__ctor_mC5CD1292DA4050C577F0F6BCB47BE09963E00545,
	TransformPositionData_GetAmoutoMoveVector_m32C2A56F47249413700688527A30D9A04AFF1A8E,
	TransformPositionData_Init_m002CBB933589193FBB27F306E5986F29E663723E,
	TransformPositionData_ActionInit_m9005F9C2810315228F280C1D0BFB26B966427ADC,
	TransformPositionData_Updata_m16678733FA1C01AC2F46D7C41C1AFA78680957ED,
	TransformPositionData__ctor_m96E00C8880CFF64EB951E8BA6E6797F6E791B310,
	TransformPositionPerformance_Init_m0F6455FF2BF9261A36EB68F062399CE4977D0D19,
	TransformPositionPerformance_ActionInit_m49A6B57A90E49D588270325C9C6157FCEC46254F,
	TransformPositionPerformance_Updata_mE2ACF7D4C555F1E2F3E554FA1D52AA4CC5AFF156,
	TransformPositionPerformance_AnotherUpdate_m1F9FC2A9F7A5C54829BDA29568D1C993CFB8D24F,
	TransformPositionPerformance__ctor_mB7483B87DD2320E97A4A27ABD93BCFC807A64699,
	TransformTargetHomingData_GetRotationHoming_mDC0D3B128A2E743A3A37288735DEB1E899583E62,
	TransformTargetHomingData_GetIsHoming_m74245812DD36DBB2DDF5299EF972A687720D5044,
	TransformTargetHomingData_OffIsHoming_mE4E6F8362AB6CFAED177BEF5D490BA1A6A495804,
	TransformTargetHomingData_Init_m170D5DDC93E26F8C78D39E7D38600C2AEDD17841,
	TransformTargetHomingData_ActionInit_m5B67DC6309695DFBCD923CE89A41CD53CA88BD3C,
	TransformTargetHomingData_Updata_m85C1F24188E5B149965BBE7CDE0175883AC9ACCB,
	TransformTargetHomingData__ctor_m7FC6BF7FCB3B7922E55F38AA9C54CEB2C75E5E84,
	TransformTargetHomingPerformance_Init_m9F5476569CE725456E72F45CA75EA5E03B0954A4,
	TransformTargetHomingPerformance_ActionInit_mCFC2D0D6525FC5D06C72A7A585F1F5209CBE1BAB,
	TransformTargetHomingPerformance_Updata_m37090C0BCAAFDA4EC162CAABB97DC0ABC0B3932E,
	TransformTargetHomingPerformance_AnotherUpdate_m699C0ED0DFEC7508A7C97919944C00886A8D1170,
	TransformTargetHomingPerformance__ctor_m8BD2056D2D2BCB3E314B16F8FF20676866B494F2,
	TransformTargetLookRotationData_GetLookRotation_m3B203D92FA7A20F4B36AE05C3968D0F3A37F04E0,
	TransformTargetLookRotationData_GetIsLookRotation_m52D65F00419DD22FB39B9B75FC48A94E81E3C949,
	TransformTargetLookRotationData_Init_mEEF517A44AB04A7D2BE3F6A979D61C72A6E74CAA,
	TransformTargetLookRotationData_ActionInit_m00BBECE0F0311641F7AA25E23CF86E4CB136F0A0,
	TransformTargetLookRotationData_Updata_m7F16E13FCA657E9D23FD6521AAF0E6367EDA45A7,
	TransformTargetLookRotationData__ctor_m8810E0434CB5A2E35EFB480DEC32002F68CCFE26,
	TransformTargetLookRotationPerformance_Init_mE04AAF20BDF4CF1CB4B92B9CAA75AC443E1847A6,
	TransformTargetLookRotationPerformance_ActionInit_mF37D10995A0BF4CDC57C3B4FCD1ED76569BD88CB,
	TransformTargetLookRotationPerformance_Updata_mB3A7F14BA2131E3779AA7DAA931C88451A22886C,
	TransformTargetLookRotationPerformance_AnotherUpdate_m868FB22196F3E1629B63A58CB1C32E7F7AA44307,
	TransformTargetLookRotationPerformance__ctor_m2CB8ECD39A8C545F01B8A94E835B751833A4AC42,
	TransformTargetTeleportPositionData_GetTeleportPosition_m51EC513D0FDD03B53789270DB2A81FEF0A27E9AE,
	TransformTargetTeleportPositionData_GetIsTeleport_m49C6431C5484770CB823B2F529F31C1816E89D6B,
	TransformTargetTeleportPositionData_OffIsTeleport_m69D7CE001C08F560000FAD1220146992370AB50E,
	TransformTargetTeleportPositionData_Init_m2AD2017C44B688CDB49B70CCCCDE0F79A6000505,
	TransformTargetTeleportPositionData_ActionInit_mC3979AB25C6C481347AD556A14FAA010B581E23B,
	TransformTargetTeleportPositionData_Updata_mBE31E5F0ED44E8CEBA1FF4419B6430F0A9C2A27F,
	TransformTargetTeleportPositionData__ctor_mF052F8834636EA4CC592B9F9187613DFF68D4EFC,
	TransformTargetTeleportPositionPerformance_Init_m707FE198C4E0E90A2A8B2ACCC381AB84C40DEBE1,
	TransformTargetTeleportPositionPerformance_ActionInit_m1C64521A91E6EF5FFEC36502FE8DBA9B9EF70E2F,
	TransformTargetTeleportPositionPerformance_Updata_m2A756744840B93E90D0CE5D00FDAAB45DA4DB46A,
	TransformTargetTeleportPositionPerformance_AnotherUpdate_mFEA83956F375242D96D15CBDA8E40129E76BED1B,
	TransformTargetTeleportPositionPerformance__ctor_mDF922F9FF8EBB639474E0D2D574C39000C3AF18F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExcelSystem_GetExcelLoadData_mFA9AD63AABC6EB67194CC4623B63FCC923AEFC79,
	NULL,
	ExcelSystem_CreateData_m9E7B82AAF2D5A127222782CA9C763BEC37639DFB,
	NULL,
	NULL,
	ExcelSystem_SetUpDataControl_m07F324CCC246C96CDE2EF78B2FC00290BAA2DC9B,
	ExcelSystem_GetData_m0DE54A6DAD8ADE4AE6639C952D85870C4B4273B4,
	ExcelSystem_VoidCheckSheet_m6F77D17A4527B318FCA0416B708A2A671A63B884,
	ExcelSystem_ExceLoadBook_m55E156F6F1FB84B572F67C7459518230421A1E82,
	ExcelSystem_ExcelLoadSheet_mF7A434A01DCF21799F32BF042ADF238AFDA7E8FB,
	ExcelSystem_VoidCheck_mAF05B4DD2A5CBE4DC0930F6DFF610788712C8F6D,
	ExcelSystem_ExcelLoadCell_m225A9D11A0154778EE7692F172833F83D95475C1,
	ExcelSystem_GetRow_m92752C83045BDAEF98A0BC76AA68447B18B87F4B,
	ExcelSystem_GetCell_mC9C3131F3229CFDE731D9E751F517A72B8838890,
	Data_get_GetDataName_m69CB424EFBE26F935672DFF0C6072999D47CB613,
	Data_get_GetDataObject_m8DA0AE4283FC1249DB49ED9013466E3061AF5219,
	Data_SetUp_mC68BC524B40C4992FD253C25AA8E18C38BD13326,
	Data__ctor_m4340D536ABF1859CB018283B97BF4633F91F9681,
	DataGroup_get_GetGroupID_m855252B51AA11599BB6606BF5AC76808BCB6EE80,
	DataGroup_get_GetListDataCount_m71667D404F57DA73985CA21EF2990E57217B6F8D,
	DataGroup_Clear_mCA788E2B6F59881B881E20028A71ED3C697F5BD1,
	DataGroup_SetUp_mBA6369D4A01B114392A2E7F05CD263CA45219963,
	DataGroup_SetAdd_m6EEAC711516A403DE3099047FDDD38FA33C5284A,
	DataGroup_GetData_m7D9270187EC9D8CD20332E95C288E5BD6324B14A,
	DataGroup_GetData_mF27D21832C082FD3648B76B3618F553A280E6F42,
	DataGroup__ctor_mD5C971AA782129944B045EEBAC6174A92518D830,
	U3CU3Ec__DisplayClass9_0__ctor_m08A2D458F1C3D9D902F4EA47DE95EDCC2E490162,
	U3CU3Ec__DisplayClass9_0_U3CGetDataU3Eb__0_mC5B55F4A8AF9601FE3352B09913529D28E0CBEF2,
	DataControl_get_GetDataGroupCount_mA23F2A9AE4DC146A4C107A023FEFB2C261C953E1,
	DataControl_SetUp_m555ED73F6AE7418FEA34DFF0AFC22579985552CB,
	DataControl_Clear_mB3D9E3247F84AF59A501B4E3CB77C77095AE8BA3,
	DataControl_AddGroup_m7EE12AAA3701DE28C067A33884E8E0BB9232AAD7,
	DataControl_GetDataGroupID_m6684EE7920F2EC5397063EED9FE1B57772F0B27A,
	DataControl_GetDataGroupIndex_m45A7A3235A43675E86DB8EDFF0031C6B15BF59DF,
	DataControl_GetDataName_m82AD7920E56F1836475F6226D0F1AA54FCE11C76,
	DataControl_GetDataName_mDABEBA4FB3229CBCBC4E18372752C4BB535732F6,
	DataControl_GetDataNameIndex_m4E0881EAC849982EFAA187D0FA3717D196D74D40,
	DataControl_GetDataNameIndex_mA75BFB47C15DF80832342D6568E20A1BD49C7DD5,
	DataControl_GetDataListNameIndex_m98D5FC9CE437106C25D0FA4ED93DA0FE4BAB233C,
	DataControl_CovertDataList_m8F105D8A68D291D4FD1A31D5E6D17C8669CE5778,
	DataControl_ConvertDataEnum_m8E91CC84C9CB8573CE2DA6217A04AA4D3B2A4BBB,
	DataControl_ConvertDataInt_m3964E6934518AEAB31BFE1AAB3DECC4C7695C599,
	DataControl_ConvertDataFloat_m6787580F8AF5E8125C6789DFB6768040AF99FA19,
	DataControl_ConvertDataString_mBEB449CD352C21311E6884719B261A256066A1A7,
	DataControl_ConvertDataBool_mD960C3D2981C2EBFC55A3D749364633897280E6C,
	DataControl__ctor_mF2DCE95E1016ACB0320B641DE91027BA8320197C,
	U3CU3Ec__DisplayClass7_0__ctor_mD49827AB8EB00025F95E16EEEC8FC325164D5C77,
	U3CU3Ec__DisplayClass7_0_U3CGetDataGroupIDU3Eb__0_m95522C8A5D67AACD1DC5DAEE1A4A841A6BEA529B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TestMono_Start_mAE1E32125C520C2BFE499F4E73EAEB37AC3C56F5,
	TestMono_Update_mFF1B843A538AB4901358DF231F81676BF984F31E,
	TestMono_Enter_m555888FDCFFF80A7397E6FDFEB2195640B4B165D,
	TestMono_Second_mB97241B8AA9FDD6D732B41CC971E29A9B5AB2D5C,
	TestMono_Release_mA8C194328DA921A0258E7457E1858DDA7E9C4382,
	TestMono__ctor_mF1EF304A0F447403FCF3C93673D498A060704D5B,
	TokimemoData_GetGirlName_m9A1DEBDCE02B1F3B6FC26218A743493ABDA7F1D1,
	TokimemoData_GetBirthMonth_m2810F3B54A99256B5F981772A885D47FD0E8A35C,
	TokimemoData_GetBirthDay_m481379C2B965B7EBD6FCA12E8588659F1F7E6413,
	TokimemoData_GetHeight_m27AFFC16AED916EF84B35B2793C5F9FF1CE85D81,
	TokimemoData_GetWeight_mC75EB8195BD31914D7919AAE0E238D081DBEA467,
	TokimemoData_GetBreast_mC44445B8236E64160B4675C817BEF46032A03611,
	TokimemoData_GetWaist_m6978BB9588F7BD8C249370405FB2710D6354084F,
	TokimemoData_GetHip_mE1FA2C3F81F2D024758BE13BFDE0D32BC6052093,
	TokimemoData_ManualSetUp_m6F5C31696B9130841BE07A21C1365F5F7E7B19DA,
	TokimemoData__ctor_m4330ADD93D11091036C76B3AFF61AC2D29A29542,
	TokimemoScriptableData__ctor_m955D9B3662BB08204B1020D50126EEF5703C7EA3,
	Antialiasing_CurrentAAMaterial_mC7319A5A6AAA7090BDDC7A34B8C3FA6EC4046EB4,
	Antialiasing_CheckResources_mED7198880F986E66B80E4D74A30273E3D1009EAC,
	Antialiasing_OnRenderImage_m92C85AC4BA9CBD1EF26D247C3412BD6ADC16CB7E,
	Antialiasing__ctor_m51CAAD6D2F9515AEFD3CB6C61C7BF8CA1D11E221,
	Bloom_CheckResources_m7F6DA8B2BB6563257CC3E067DAF8B800C8C9D460,
	Bloom_OnRenderImage_m9E04CDDA91EBF9CE543417E1326FB89E771E3329,
	Bloom_AddTo_mE9619BE3C2641B447503A370C89FC639D7B5AF4D,
	Bloom_BlendFlares_m34738C3C9263CD5584C8EB15EB78507F975E98B5,
	Bloom_BrightFilter_m6C6D8BFB58961F35054F43E065A77797883CB39B,
	Bloom_BrightFilter_mDB8E6E61C66AA4000F9E8F91DC8CE4BF4C2ED220,
	Bloom_Vignette_m3FA8F2D04EA6A142424E133BCB8FBE663E3988CC,
	Bloom__ctor_m9CE8D3A0AF0E8090F05E79AE479FEEC869C4A2BC,
	BloomAndFlares_CheckResources_m5D302EB81163C3C7C73E15BD16B22EA430F136D7,
	BloomAndFlares_OnRenderImage_m820BD8E7E5CD76FB1A0D8727FE55C68CA4D9E4A0,
	BloomAndFlares_AddTo_m96E721A8767A6E55237EB23A193309006A13A376,
	BloomAndFlares_BlendFlares_m9EFFC2222A16EC1BE33B83E870F8670D480FAE0A,
	BloomAndFlares_BrightFilter_mA7575BBFC217875E02F0454025CE76D6CF05FDAE,
	BloomAndFlares_Vignette_m671C8CAE9ED6BD64797AEF8CF982EE8414AA353E,
	BloomAndFlares__ctor_mF5F51BCA83ACC994B808494A7D673B0EE2B6B3C3,
	BloomOptimized_CheckResources_m4FFE780FF754CB37C782696BD8D692DF29B2940A,
	BloomOptimized_OnDisable_m4039BC1075AEAB38751103C799EFBF7894CDAC35,
	BloomOptimized_OnRenderImage_m58EC1CE2EA1E8727F06C57ED08ECB799594A5D16,
	BloomOptimized__ctor_m0E77B85184205FA45BE54D6F2215ACF6608179ED,
	Blur_get_material_m7B726B085C3EE53F708B326C2B362DCEB39BF999,
	Blur_OnDisable_m297600A2F390F10D3AF51B93CAFAB5C03A1EC18D,
	Blur_Start_mDA794F4F2883431D466813AD57DDF9886731C3D4,
	Blur_FourTapCone_m1AC39BD3A8913BEF402C3C1C4797293DAF13921F,
	Blur_DownSample4x_m648F40010C0E6CDDEAA991C89E008A5A5358F243,
	Blur_OnRenderImage_m1666FFFCC5C9F2D224ADCFFEDB271B767316C771,
	Blur__ctor_m402B569E15418AE90A4E1208254262FE1A524076,
	Blur__cctor_mF1E0CBBDB58F1F397F7CEB6805D8E124608F2298,
	BlurOptimized_CheckResources_m4690B1D08CF4BEF70FC4AD9A5D0FB99378600D0B,
	BlurOptimized_OnDisable_m99433F61DD851EC7C4F30DB6F7515E5AC6756EEC,
	BlurOptimized_OnRenderImage_mC2B4151D0C82311AEB06B47189387AC604F9615C,
	BlurOptimized__ctor_mB38631A090D409384457B18624700A57C6CBA68D,
	CameraMotionBlur_CalculateViewProjection_mE265F7EDE22E2195C327E41AD18C5AB199BA2C17,
	CameraMotionBlur_Start_m0E1A32F74934F5EB27F267181D64FC1249FD741C,
	CameraMotionBlur_OnEnable_m4040081CC7078E6B824F21AB72EF83AFA9568890,
	CameraMotionBlur_OnDisable_m496FCA193BFD68D9302A87764CC80B726D5A94D8,
	CameraMotionBlur_CheckResources_m6003DE71C47C031D582DF3B6F1C720977B649009,
	CameraMotionBlur_OnRenderImage_mFD34E9ED689F6807901BAC6AFD041023C39C1137,
	CameraMotionBlur_Remember_mD5E7AB7BDE951D208218FC34819554FDDE25A2F8,
	CameraMotionBlur_GetTmpCam_mE9841487AA47FD7F4A680DFCEE74CDB4897DB562,
	CameraMotionBlur_StartFrame_mF9FDC411A58A43A7A15D500DC771C9E08612B1B8,
	CameraMotionBlur_divRoundUp_m82F809436B9B186C5E476D5FE042001EC257DDF6,
	CameraMotionBlur__ctor_mC57B0D630BDBF1C79BB0A789EC8665ED5F4BB613,
	CameraMotionBlur__cctor_mE7142222C58ABB321445EFC0A3BDFB1BC81F6711,
	ColorCorrectionCurves_Start_m0ECE820F6B53EBCCFE8B2CE8FAD4CAE3D28781B5,
	ColorCorrectionCurves_Awake_mBD1BC43FC119A70FB689CB7629685C8E7686FB32,
	ColorCorrectionCurves_CheckResources_m20A1ECBF13300DF54085714D437F60E16697744B,
	ColorCorrectionCurves_UpdateParameters_m7D529BD5037CCD9508A66A5DA1652C8258B227B2,
	ColorCorrectionCurves_UpdateTextures_m89C95473600F6CA76C01674B0BBFF4507AD66D28,
	ColorCorrectionCurves_OnRenderImage_m5031CDB14594542347FFD364FC36AAF495818FEE,
	ColorCorrectionCurves__ctor_m3ED2C81BF81F071598710B1134EF0651337927FF,
	ColorCorrectionLookup_CheckResources_m91294402B660B238BFBFDAB36AD8A5F40EAD04FB,
	ColorCorrectionLookup_OnDisable_m6CFE877E53288C93592EF70F057F084B02670C99,
	ColorCorrectionLookup_OnDestroy_m420305849D7035FC6FA2CEB6DED416305F99C560,
	ColorCorrectionLookup_SetIdentityLut_m2A52E467FB7F1BD651AAA26E870F8F784308E7F3,
	ColorCorrectionLookup_ValidDimensions_m38F9BDE2D756E21F5FB2FB74DAE3242DB7519DCE,
	ColorCorrectionLookup_Convert_m878152C99CE1BC6F175B7419E63B27B08AD887D6,
	ColorCorrectionLookup_OnRenderImage_m51B30A601852A59447B3967A70C0F6000A958909,
	ColorCorrectionLookup__ctor_mD85720C2F562F4FD5129BAA5A4BB57B26A42A8F4,
	ColorCorrectionRamp_OnRenderImage_mB611ACFBD7BD21D3C5BE0CBB7A4009C5A9327E3E,
	ColorCorrectionRamp__ctor_m5D26F8245696524783210DD30C4981B56E9EC62C,
	ContrastEnhance_CheckResources_mD7A8290FD95DAA481E11EB510E27F9709A2C9707,
	ContrastEnhance_OnRenderImage_mA03990E8EEBA2A65EAFADF627327B34926E88B98,
	ContrastEnhance__ctor_mC6A17229536BFB620278B49F8D6CA4CE61FD641D,
	ContrastStretch_get_materialLum_mFA3F36EC235BC13BD87A580B33D29321850147FA,
	ContrastStretch_get_materialReduce_mEC419049AC77A1D0D3AA1CCF5FC8908DCF609EC0,
	ContrastStretch_get_materialAdapt_mCF400586EE0D8AF64073C6FC7EB6640EBC4235CF,
	ContrastStretch_get_materialApply_m2536CDCE7550DC3B9B2908501DBE2931C44E16D4,
	ContrastStretch_Start_m036007088F06A37063A9580F392F2593C4F2F081,
	ContrastStretch_OnEnable_m17DC4417344C6175F77919E452DEBD54AEBD66A5,
	ContrastStretch_OnDisable_m3CB5068DF0BEE880259C931340092A699E3DD713,
	ContrastStretch_OnRenderImage_m84EA66287D48055F498FF5DFEB0083C504AE7A62,
	ContrastStretch_CalculateAdaptation_mD5D9C8B371B907226D0F93D6A0DD0E869CDD7CDD,
	ContrastStretch__ctor_m1A8EA45A045275D5020396872CBFE779C8278949,
	CreaseShading_CheckResources_mB279AB4EACCE1FB253362A078F90A056C741B123,
	CreaseShading_OnRenderImage_m2DC7E912357ED3A0751757F797878437DA86950D,
	CreaseShading__ctor_mE1D8508608690E8646D5B36E6185404A249C956A,
	DepthOfField_CheckResources_m0C249CBD6A22AC2677AA97453F9C1AB8607F7784,
	DepthOfField_OnEnable_m6741B3049B5E4E17C093FAC5D44A0EFCE658C593,
	DepthOfField_OnDisable_m89B2E7EF9C7D5E026484BBB17E8178DA83913F84,
	DepthOfField_ReleaseComputeResources_m9FFAAE0615BA2F81031CD9E3A1F17C2E8ACF1167,
	DepthOfField_CreateComputeResources_m144C19BB16C05831CD0B4CE5DC80E1347F37D53D,
	DepthOfField_FocalDistance01_mAD7B2FD38F77D486FCEAFAA8D54DEF300AE73E1F,
	DepthOfField_WriteCoc_m0F58251DD44C9B7A25E28EC45C9EB89A77746174,
	DepthOfField_OnRenderImage_mBD7B49FD25DBB86F6B728D7961D595DCFE11B269,
	DepthOfField__ctor_mBDF0338A503491D917E7FA343F342953773C9858,
	DepthOfFieldDeprecated_CreateMaterials_m7987189EDBE1F72FD49068B802AC6EBD86368A81,
	DepthOfFieldDeprecated_CheckResources_m78092683ED5689E422911AEE89454D084AE05C51,
	DepthOfFieldDeprecated_OnDisable_mDF53A066AD9036C99071FB930285BD19AC923696,
	DepthOfFieldDeprecated_OnEnable_m8F596341E9ABE3D53D374E07D74B3875CE2B8137,
	DepthOfFieldDeprecated_FocalDistance01_m00D69A3198F8D54F6DE0DA0A74BA2C148B73C28D,
	DepthOfFieldDeprecated_GetDividerBasedOnQuality_m2E1CC9B27087D14152D5AF975B52F0AB69F46776,
	DepthOfFieldDeprecated_GetLowResolutionDividerBasedOnQuality_m3DBDA9D7474E15B50D6F31E3F91DF3AA564D17AD,
	DepthOfFieldDeprecated_OnRenderImage_m7373016EBF1B049922A25960B8681AE44D47ED98,
	DepthOfFieldDeprecated_Blur_mE97ADDA1CB43F84B428198D4EDCA12435DA8B107,
	DepthOfFieldDeprecated_BlurFg_m9E7C81AF2E17E3AD14C44B63874CCD717EAB0D66,
	DepthOfFieldDeprecated_BlurHex_m5EE76E316221C6877542EBC8F9A131BE176F631F,
	DepthOfFieldDeprecated_Downsample_m73770942AAB7667353A5478BE15202ABCE6190E3,
	DepthOfFieldDeprecated_AddBokeh_mDAC12FD0F5711BF8D8920F86C0D3251934519222,
	DepthOfFieldDeprecated_ReleaseTextures_mD4E8FDDB6EFE5782BDCD188BBA06BA80672B2676,
	DepthOfFieldDeprecated_AllocateTextures_mDFEBA3201E4C2069FEDE43AFF46C063F0F42D84B,
	DepthOfFieldDeprecated__ctor_m361158B5F9744812D8BE55DADAE5EBA104166113,
	DepthOfFieldDeprecated__cctor_m175F67C5F188A0EC8F033A647B6A48C689DB60F2,
	EdgeDetection_CheckResources_m45732EC74CFF88B1AE6CAE84FEA357CDCC997EF9,
	EdgeDetection_Start_mAE2E27055817D0943C8420925359639EB18DE40D,
	EdgeDetection_SetCameraFlag_mBD3DAF292965786705B18B5B47B2CEDDC869ECCF,
	EdgeDetection_OnEnable_m1FAB65EA14C9F934FDBDB62FD778ADDF936C6D52,
	EdgeDetection_OnRenderImage_mFAAB0A7E520C095829699660B33C39FCC919455A,
	EdgeDetection__ctor_m0B166B98FAE308D02F125499FE85AF36A31EE669,
	Fisheye_CheckResources_mB7CC03501BDA172511EDCE8CDA1500E7C63DFEE9,
	Fisheye_OnRenderImage_m0C11DC23898D79F4312B63B3FA8B4424D14324A2,
	Fisheye__ctor_m864A7A8F587F109C6AD0D9FE7940E9766B293AD1,
	GlobalFog_CheckResources_m1B2A82D23CC96D26D69451935E3ABFF226880F5C,
	GlobalFog_OnRenderImage_mB4D89A1A081A4AA1684BA89B78305C15CCD35FAE,
	GlobalFog_CustomGraphicsBlit_m6D2C9F3F0EDD6141EAB19FA67B3A46C7CC486E13,
	GlobalFog__ctor_m009B89438058D759F3968069E3DCCC2D495E0561,
	Grayscale_OnRenderImage_mC16ADFCEA6F222E4CC6D87D58A94C21D6F2452F6,
	Grayscale__ctor_mAB9F7F563BED5BC793D1470EB44C98A7C01808FA,
	ImageEffectBase_Start_mCB74F1AB8BDB35BE0C8567AAABB24F59A10416C9,
	ImageEffectBase_get_material_mCBA3A9537019FAFB5146CB9F3A0EC2F7C1A53C4E,
	ImageEffectBase_OnDisable_mCFD2365FAD014C1DE029AAAF6EB1AC35EFE91232,
	ImageEffectBase__ctor_mC2A208B662D5A6F5E9F24A615737C6FE1432DC06,
	ImageEffects_RenderDistortion_m03C3F0F20E8A046F982E69B10AF813A0B3076A11,
	ImageEffects_Blit_m8DF0ACAD0C4FC759E3D36B3DD563B1BBCACD3C98,
	ImageEffects_BlitWithMaterial_mAC6EE561D0CF9841CC0090F94922925585EC171D,
	ImageEffects__ctor_mBBA6CA1A07C99F9297736099B9B17EA8F6675449,
	MotionBlur_Start_mAABD58AA810F8D43064FE0BF556A099DE2CEAEAA,
	MotionBlur_OnDisable_m059E6B3E17CD646DF39503611C342E45B9AA39F4,
	MotionBlur_OnRenderImage_m17271973D79B9D3AA421068E5E34E8ADCFEE972A,
	MotionBlur__ctor_m05B8A16AF2B26108A1C5D557FA73483D796AEC05,
	NoiseAndGrain_CheckResources_mB4552F2BE077ED9C74DF437BA8176B475AD9F1BD,
	NoiseAndGrain_OnRenderImage_m2BCF8B0B93F98249F947B076B1DAF03F0F08C5F6,
	NoiseAndGrain_DrawNoiseQuadGrid_m5E5CB6D173379C752EB5A8D2FAA6C76933F99DBB,
	NoiseAndGrain__ctor_mE912CF5485D816E97569AE3A52AAFE52334B98C2,
	NoiseAndGrain__cctor_m642AF4DDF9AA7FA27831AE695BD1487BA209431B,
	NoiseAndScratches_Start_mC0CFF72F8784074A52552A4DF9EB8CDAD4CEBF0C,
	NoiseAndScratches_get_material_mD26BC7251CC4505EBE44E87F7C17A1D25E17CFDA,
	NoiseAndScratches_OnDisable_m088BBEEFE0DCD54C27CFAFAE04E1B5D570536954,
	NoiseAndScratches_SanitizeParameters_m26D2C93FF2024D33753774B701D768E5491B3B49,
	NoiseAndScratches_OnRenderImage_mED39D37A141027AFBED7EABCB28E5AF21E8FE82C,
	NoiseAndScratches__ctor_mA4847141C71255A0B3435442FE696248AC915F70,
	PostEffectsBase_CheckShaderAndCreateMaterial_mA0855C41F4D32605063FAFD0836FFB78A0BCF490,
	PostEffectsBase_CreateMaterial_m58573EB3A5D193A92003C5DF82DAACB9ED8E8078,
	PostEffectsBase_OnEnable_m0794D55F439C915D930543BD5BC3DD98D0900B27,
	PostEffectsBase_CheckSupport_mEC1CB870A1763E3545F7829BAC85CE42275DF4B6,
	PostEffectsBase_CheckResources_m304B5191F98669642E68716BD21E97DC4651836B,
	PostEffectsBase_Start_mB6D8676C7127E664315ADFAAD40A44071D778121,
	PostEffectsBase_CheckSupport_m20B1CB0EC0F019D345427F851C67A5FD2E01061A,
	PostEffectsBase_CheckSupport_m8016EB7CDCA60B4E5334AA1CEFE2EB2AA0A717ED,
	PostEffectsBase_Dx11Support_mD0D2A3A932771541E700EE9420F37038759DBC27,
	PostEffectsBase_ReportAutoDisable_m732B3B3112769D5ADA588AEFA3F470A4EB5A3D31,
	PostEffectsBase_CheckShader_mC043E14AD142392993ECD52EC5882B6C5E0501BD,
	PostEffectsBase_NotSupported_mB15B8F953382CBA0AC80B39828B807399B730F12,
	PostEffectsBase_DrawBorder_m0FA412FECC786A5623F734DBD9D1F7EE016BE7E2,
	PostEffectsBase__ctor_mFA1F5C2F42295DE5BC5B12E891F7EFAE11D91728,
	PostEffectsHelper_OnRenderImage_m3249CDADE845507F040B3BA27CDB6718DD0CFBF6,
	PostEffectsHelper_DrawLowLevelPlaneAlignedWithCamera_m8B5222689A79EAD6CC172A892EDE45EF8EA75EEF,
	PostEffectsHelper_DrawBorder_mF93C2573512E51B1C1F1FC27823B02CB9239E153,
	PostEffectsHelper_DrawLowLevelQuad_mBD073127C1A29721D7EEFC2112B2AB4F63C6E95F,
	PostEffectsHelper__ctor_mA8DA1D61B8FEED9DA832C63BC79D4ECD9C628E85,
	Quads_HasMeshes_m4BC0E7A503FDD62C1B6AFD11BF42C48C44D4ABF3,
	Quads_Cleanup_mD8D12694A940EB6155B22BE54FDF1A5048E97FBE,
	Quads_GetMeshes_mE0C1253983124C6022E99BF9C690AB87E12834B2,
	Quads_GetMesh_m389C16D7D2D2163D185AFF636C94B799C16DFC19,
	Quads__ctor_m0F21717E48723759E99AA626D14C571F65C14880,
	Quads__cctor_m63271B518EF4D21A7DA56D8AC00BF587E24C33BC,
	ScreenOverlay_CheckResources_mDC30CB8BAB81DA4EB1176628FDFEB5F650DE351C,
	ScreenOverlay_OnRenderImage_m5A4BA1B7FB006E1AA855C1CF5589E7379B4A55E6,
	ScreenOverlay__ctor_mD098EF34DFC76C199E76E8052230EB389A0CC13C,
	ScreenSpaceAmbientObscurance_CheckResources_m36D34C25745A60AF409689461F575BB772922F9E,
	ScreenSpaceAmbientObscurance_OnDisable_m5DF34448FF84C4115C79492BEA74FB7B97C945F3,
	ScreenSpaceAmbientObscurance_OnRenderImage_mCD1F333B91A96F8D3937714AA8CCBE9BFB31F743,
	ScreenSpaceAmbientObscurance__ctor_m724A9A963558BA931EAD2C370A44C966D8BA8C05,
	ScreenSpaceAmbientOcclusion_CreateMaterial_m355EF1BDA9A9F63522A7592DB105A3C867F0B51B,
	ScreenSpaceAmbientOcclusion_DestroyMaterial_mC77694CF92CCFD7D0E49D6FAA6A2909024A23CCA,
	ScreenSpaceAmbientOcclusion_OnDisable_mD706BCF8E1F4D738DA0CBBC8106752BEB441C795,
	ScreenSpaceAmbientOcclusion_Start_m98A9C6AC986C0A01BF123A0ECC67C4633D777E2A,
	ScreenSpaceAmbientOcclusion_OnEnable_m5AD62EDC645E3221C96AC5E67E912C7FEA179FDF,
	ScreenSpaceAmbientOcclusion_CreateMaterials_mB9DA32D885036E3970FBFF9C8AC8410B7C26DBC1,
	ScreenSpaceAmbientOcclusion_OnRenderImage_m9F10B99EBD986D865F3B595DFF7697B9271972B5,
	ScreenSpaceAmbientOcclusion__ctor_m85891921B77C832330BE8B476E5107A64F18E9F0,
	SepiaTone_OnRenderImage_mA99E06A944A72FCA3A89A6054C1A9934B21EA4BC,
	SepiaTone__ctor_mAA07BEFC9B027350BE71818E18972B19A8D2C4C6,
	SunShafts_CheckResources_mB28807554E1C72E8EF1788C47C0C35FFD8542C92,
	SunShafts_OnRenderImage_mC38080C46A73ECC5566648935E08F1BDE5387FBB,
	SunShafts__ctor_mB7B9475D995407A720DFAD133A94BA799C5B2AE3,
	TiltShift_CheckResources_m3A988CBE570076BF064EB28141134153C94470A5,
	TiltShift_OnRenderImage_m29793FAA4D1EC712C6551F2854254A3474915BF9,
	TiltShift__ctor_m17DF846193FB9DE098FB22F7BF16012EBB82AACE,
	Tonemapping_CheckResources_m8A3A3C61145E53BCD8E8F6DEA36F26AB76A3C742,
	Tonemapping_UpdateCurve_m57D0F9B2859479DE71D507B77153C8C031B193F0,
	Tonemapping_OnDisable_m934B31423514B72A8446C42E330E2E647F72653F,
	Tonemapping_CreateInternalRenderTexture_mE42415E8EA497B2F9C96DE5B936516FECC2EE69A,
	Tonemapping_OnRenderImage_m5477DF1F9F844ECEDC1CA7DCE5554A40A1048440,
	Tonemapping__ctor_m771270B9AC12276987A5B176AC01CDA873FF089F,
	Triangles_HasMeshes_m3FBACEA56E7B0437BE854A153B647480531C2F7A,
	Triangles_Cleanup_m08DA8845F1CF35B11916649B0C6ED78406D1ED97,
	Triangles_GetMeshes_m3DB03D7EDEE1CBDB6D28072770AE20AA44003234,
	Triangles_GetMesh_m28C77B830B07AECCBA2E163BFA8B37768E738602,
	Triangles__ctor_mD0658EDAF0999D9BDA2C1FBFCBB83A551FCEBF18,
	Triangles__cctor_mB6CCFBA963D829EA0C65E5DE04B12BA437FA97B7,
	Twirl_OnRenderImage_m54CAD9C6DEDA4B0A8B1C4C348776076725BDA20E,
	Twirl__ctor_m4C83ABBF0D08D5D522E186EDCFB6596866B36785,
	VignetteAndChromaticAberration_CheckResources_mE7E2561C3849055304B86F898782CA2A8DFE78A2,
	VignetteAndChromaticAberration_OnRenderImage_m1623BDFA946B14103D45AD9FF10BCE1497E352C2,
	VignetteAndChromaticAberration__ctor_m3DF39E4E746B7E100F55B1CE70DCF349BD37F0E2,
	Vortex_OnRenderImage_mCA066061BDBF67BB18DF25CC410A4917A8E1D2B2,
	Vortex__ctor_m07B90386422BDB2135F82236DD44688266759A7E,
	GUIControls_Start_m7B3461CB662A9F9C8DBB05FC5C042A5D36BE3031,
	GUIControls_OnGUI_m0DFD9CBC17B07DF65EF0B45D8485E472B33917D4,
	GUIControls_Navigation_m635085B4C12B6AED8F1E8157B03A4873FBCA0CB0,
	GUIControls_Attacks_m8A0B7B99092D7D0DEB505C84B7CA3DACEC3B1502,
	GUIControls_Damage_mC32B8B66640C39FD0970896A15B82D6C41DAD6D5,
	GUIControls_RollDodgeTurn_m31A56363CF58CDBBC064122CB2900DC373CBBB06,
	GUIControls_Jumping_mEC54815880A59B522660F1887D1970C30CCA65A2,
	GUIControls_WeaponSwitching_mEC1766EFA27FEB3278AAAC9A4B12793B8EB9075E,
	GUIControls_Misc_m11987BFBD57BB1C4A33E1F76EBA446683058B724,
	GUIControls__ctor_mEEF77699806C309D756A4D20C5217D8A1AA7E150,
	HighJumpTrampoline_Update_mE3B7C7D05D9BCECC5F91D63FBE88A11E68DD8632,
	HighJumpTrampoline_OnTriggerEnter_mC2EA3F9740EAE39143490F6B693B3919F3A99317,
	HighJumpTrampoline_OnTriggerExit_m02BDBA9ACBFB7C5FFC14DF93C4AC6034C41AB9DB,
	HighJumpTrampoline__ctor_m945E18B2E7EAF50B666B3C9D4A26F236655D4322,
	NoJumpSlime_OnTriggerEnter_m1157E87CD7D16FB9A0B420E440B53E4DBD7EFB6D,
	NoJumpSlime_OnTriggerExit_mE06B2D612DA503953B9EACBDE2B4D8443C2668B4,
	NoJumpSlime__ctor_m6F24C671661EB70753A76F3ACAB950E1BF2A232F,
	NoJumpSlime_U3COnTriggerEnterU3Eb__2_0_mD984EA98B17D3D2ADF86B52050A075667C8EAB32,
	U3CU3Ec__cctor_m2D08DA4F3433ABB64AC25246788DBC92EB7FBCCB,
	U3CU3Ec__ctor_m499E2FE5E8AFFFD03D5090E3229E555BD1ADB3DC,
	U3CU3Ec_U3COnTriggerEnterU3Eb__2_1_m84F6C5D94EB1FFAB8A46F85BF76895047ED060C3,
	AnimationData_ConvertToAnimatorWeapon_m0C5BF66650847636771C5CC716C468E66006E397,
	AnimationData_ConvertToAnimatorLeftRight_mEBA008080AB835566B806BFF32FE7C9702BD46B4,
	AnimationData_IsNoWeapon_m3CEBEC8B4DCA0CF141BDEBE6F824EDA166A26712,
	AnimationData_IsLeftWeapon_m51A55B4C636BD64D01F5A68F698FE350A5FE0B10,
	AnimationData_IsRightWeapon_mB224E0A2B53A963143C2420F14A801AFA8986632,
	AnimationData_Is1HandedWeapon_m120049602DA5ECB4D9DEE07C58CF8106A7CC9512,
	AnimationData_Is2HandedWeapon_m3163EAC44D0C1DA72F5A82E08F3F182381CD7C63,
	AnimationData_IsIKWeapon_m4A9FA1B0E26AE7119E55F28979E459CB0B6535EF,
	AnimationData_AttackDuration_m005A097CAE9A2982E5E7A886DC1D7DFD8F50E5AD,
	AnimationData_SheathDuration_m2F0C64058DAAAD2DBC63E4C07B1ED7252047F672,
	AnimationData_RandomAttackNumber_m38C11BC73FD745AE64B3D53C3167DAF2BCA29BEC,
	AnimationData_RandomKickNumber_mAFD9D15B8015D93D96B8FBF7D34E44FF559DAC71,
	AnimationData_RandomCastNumber_mF26142FFC3FBC154963AE4ACB0C75D8DAFF460CC,
	AnimationData_RandomConversationNumber_m754C42157851C6455244A578168D56BE93D62EF4,
	AnimationData_RandomHitNumber_m6F61DB328D204CED9C061F9A764341340085C47F,
	AnimationData_HitDirection_mCD5A20A41A404CF52E3BA7BD72131D41D9434171,
	AnimationData__ctor_mEB50DEF6DC9E2F93DB16BD5F435416494F41B10C,
	CoroutineQueue__ctor_m2E4C576AF563FD7AEBAD0BC149D1E20299316A09,
	CoroutineQueue_Run_m9FA23443F7F3B042FC0077B0E675AA9BACAA0347,
	CoroutineQueue_RunCallback_mD106058F02DCCD05FFCFB9475042E4D0B0A03761,
	CoroutineQueue_CoroutineCallback_m042C197B3DA7388569A88780A04EF99F76BF7E59,
	CoroutineQueue_CoroutineRunner_mB12CDB4CC9A811BB0B307466B3C56CF5EB919C0A,
	U3CCoroutineCallbackU3Ed__7__ctor_m5FAE6DBDB1EC86E7252E7F7F7BC062FD1D17DA03,
	U3CCoroutineCallbackU3Ed__7_System_IDisposable_Dispose_m185B09AAE625FAEE1C05B0A592D53044D55E0E91,
	U3CCoroutineCallbackU3Ed__7_MoveNext_mB07F84F6943B7DD9A32C4D21AC18B2A368EDC6ED,
	U3CCoroutineCallbackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8E048AD633FD8060E90BBF336FF5066DAC2A8D13,
	U3CCoroutineCallbackU3Ed__7_System_Collections_IEnumerator_Reset_mCCFCFC560831454E579DD36522F17C0BAD2D9CA9,
	U3CCoroutineCallbackU3Ed__7_System_Collections_IEnumerator_get_Current_m28792216D44AA758CB7580BADF0CCE3591983554,
	U3CCoroutineRunnerU3Ed__8__ctor_m396768941F3C41FA2F67ECB19A04EDA9859909B4,
	U3CCoroutineRunnerU3Ed__8_System_IDisposable_Dispose_m81259E29D5A3DF8F365BCE7014D557EE5B594CC0,
	U3CCoroutineRunnerU3Ed__8_MoveNext_m13C3E3197334163BE236193B0929B8899F3A27BE,
	U3CCoroutineRunnerU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0ECFF44F9AA848163A4BFEA8457EFC3B8C52C43D,
	U3CCoroutineRunnerU3Ed__8_System_Collections_IEnumerator_Reset_m93A3F25041318245CE1C8C44BDA2D3E825DBF69A,
	U3CCoroutineRunnerU3Ed__8_System_Collections_IEnumerator_get_Current_m54E3B207FAF47EE1E97983F5B274ACDA4C78883F,
	IKHands_Awake_m24BE66EC320CDE99AA742CBB08C0CE7726324640,
	IKHands_OnAnimatorIK_mB2CB172EA495554C1BF25596DA6D141CAC867F89,
	IKHands_BlendIK_m66C430F802CDE10212D7210AAEDA106132D02D21,
	IKHands__BlendIK_mDC923286558A09866E7D917AFE82CC55BB2E23B7,
	IKHands_SetIKPause_m940BC10FE16A77A6C809C5B2215706891E0D514D,
	IKHands__SetIKPause_m650B98DF468CE27BB9FC8086D37C792E1F3DF1D6,
	IKHands_SetIKOff_m104F67ECA8EE480761892A375288F9744C217F09,
	IKHands_SetIKOn_m8A6A1D16C40B2C9C5F16EF9C1CB1AA7F97006D15,
	IKHands_GetCurrentWeaponAttachPoint_m9DEE496C276A198AEF207DE8BE785E868C468A21,
	IKHands__ctor_m504AA0CC075396FD83AC935CE2C4A70DACF4BCB7,
	U3C_BlendIKU3Ed__13__ctor_m35C94D3791AE8D06B52ED6BBCBFDD4AAFBC16F48,
	U3C_BlendIKU3Ed__13_System_IDisposable_Dispose_m4730DC2CB97A015A4EDD99E7776355B94BF1D91E,
	U3C_BlendIKU3Ed__13_MoveNext_m77CF4D9E80111F21D2F673CC9892351E49BB6CCE,
	U3C_BlendIKU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC5831B0D2B977B898CA7768B3B10E0A7B4BC2B6,
	U3C_BlendIKU3Ed__13_System_Collections_IEnumerator_Reset_m052C9B7CE91AF875C2AE57BAA3A4142C3389BD62,
	U3C_BlendIKU3Ed__13_System_Collections_IEnumerator_get_Current_m14BA963C2DD263EDCEA0A40C985CE1ED3A0FDDAC,
	U3C_SetIKPauseU3Ed__15__ctor_m0B185DEA9D1B4BFDE62FB1B6A48CF51CC5E3E2E4,
	U3C_SetIKPauseU3Ed__15_System_IDisposable_Dispose_mBFB1D02F353724F6AA5363A4FA126407B469BAE2,
	U3C_SetIKPauseU3Ed__15_MoveNext_m7B54D94D8F7AD21C39CDBA25122A23AE925AACA4,
	U3C_SetIKPauseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE68F278AC0C3D98EC03ECB1A20B42FDBDE8FBDAA,
	U3C_SetIKPauseU3Ed__15_System_Collections_IEnumerator_Reset_mC207DEB3C249F714C184CCCB8F235F1494465394,
	U3C_SetIKPauseU3Ed__15_System_Collections_IEnumerator_get_Current_m3CCADB7117BA3267152C40F3B2B71FC7FEECEA6C,
	RPGCharacterAnimatorEvents_Awake_m93BB9257CA75AAF2FD34D88C122B885984733F8F,
	RPGCharacterAnimatorEvents_Hit_mF6C6C565057288B9BEDD7E676250BDD8FAC5DE57,
	RPGCharacterAnimatorEvents_Shoot_m8A4B7BF8F598314956B2E4CC13C32FD0CE166729,
	RPGCharacterAnimatorEvents_FootR_m6263CBD9FE348EDEC4CED8D3093B6177EDFA8089,
	RPGCharacterAnimatorEvents_FootL_m323CB1E89CDD41B3269B06AEAB7FE884EFCE816C,
	RPGCharacterAnimatorEvents_Land_m032649D8B8BDF7FF002F3CC36B50BC9A5E60CC54,
	RPGCharacterAnimatorEvents_WeaponSwitch_m9D98CD1EB88D9FDCB9CCF4C2D846EE98A1044720,
	RPGCharacterAnimatorEvents_OnAnimatorMove_m02C6A23315EFB83C40AF6FFE2794C57B7A23E20F,
	RPGCharacterAnimatorEvents__ctor_m04F18169DC3CF8912D37035F5E8BA69087272744,
	RPGCharacterController_add_OnLockActions_mA2229D3598D11F08C255EF41FC8500CA937AB7BB,
	RPGCharacterController_remove_OnLockActions_mF3AE7AA2B49E8904DFE1D50519A39824AEAC86E1,
	RPGCharacterController_add_OnUnlockActions_m1EECAE820C02E8FC990B3ECCD8E0539B1FBD948B,
	RPGCharacterController_remove_OnUnlockActions_mAF0186F3002A40F97D049ED5B50A93677F4247BA,
	RPGCharacterController_add_OnLockMovement_m9D3B1AC2A04893974BD69EE499E540148B00DBA4,
	RPGCharacterController_remove_OnLockMovement_m34BA627CA1E6A98FFB829CD8EBCD581A452850AE,
	RPGCharacterController_add_OnUnlockMovement_m8A2B698EA14CEC1D67B608AF07ADCB08BB3DF8E8,
	RPGCharacterController_remove_OnUnlockMovement_mCC6B4F8B836B03FA5C2ACA9337A8B6022F2B178B,
	RPGCharacterController_get_canAction_m2448A8FAFA7AF559BA4690C16EA539C1B0533380,
	RPGCharacterController_get_canMove_mF1A2EF8478909EC59AAAC995265AA38D8C0E204B,
	RPGCharacterController_get_canStrafe_m36D5642FE5D0C48920C35C23026E9ADA5CE6B99D,
	RPGCharacterController_get_acquiringGround_m749D03870BD603279D098A240893A287D6AFB83D,
	RPGCharacterController_get_isDead_m3771CDA23C33D4E3B471597DAB3AEADDE4314E4A,
	RPGCharacterController_get_isFalling_m2B7F5BD2EBE75F52F11F7A78777A8D737F595EA0,
	RPGCharacterController_get_isIdle_m75173782D807644BD77B55727CEC7146AB7E37D2,
	RPGCharacterController_get_isInjured_m05D67A22501AB82907A9B08932A8A775F756B200,
	RPGCharacterController_get_isMoving_m5A1527E70F3EDDB0D943151CA8E3465199E3148C,
	RPGCharacterController_get_isNavigating_m33D5BDB9EEBB77BFBCC4BE31E14C1B1692B70B91,
	RPGCharacterController_get_isRolling_m343AB3B5AB44B16D551AE01182E7B333F5DFF4C6,
	RPGCharacterController_get_isKnockback_mEAA2656B2156D1F0750B931B8DBC70F2C38C128C,
	RPGCharacterController_get_isStrafing_mBED3DB243D303B8AC77870C1BA5CC3D430918726,
	RPGCharacterController_get_maintainingGround_mEC30D218E90FB337E09ED3102DEA133A77582501,
	RPGCharacterController_get_moveInput_m6AA5E5188EC030BB09F7A0C5D41E249F2D8F7F28,
	RPGCharacterController_get_aimInput_mB29E8C79EB375542F4A793814F73FA9B8AEC0486,
	RPGCharacterController_get_jumpInput_m289A1A0836E6D14B78F5A33C6D479AD3754A774B,
	RPGCharacterController_get_cameraRelativeInput_m7E5A606F966794EA7B0E72F11BFD9B20CC89BD2E,
	RPGCharacterController_get_hasRightWeapon_m5ADAAF533CFDD323804669D3137E463048B18E53,
	RPGCharacterController_get_hasLeftWeapon_mE8A8859449C1E8EAE77DB94B7E23EEA966E7BA1F,
	RPGCharacterController_get_hasTwoHandedWeapon_m6CE8068976612CCD755749C58D0F0CEA4830DC23,
	RPGCharacterController_get_hasNoWeapon_mBBD56D0A0D7172E478A34B164AB6887B1025B62C,
	RPGCharacterController_Awake_mAAD4E45BB08B9C5857B43B669AC66C7C19A0E1C8,
	RPGCharacterController_SetHandler_m9116693F4A2E5F51B357D05CF23FEAA59DD7353B,
	RPGCharacterController_GetHandler_m5973E46478E4CEED5F626E2052804992E39C9053,
	RPGCharacterController_HandlerExists_m63EA91C5629427E13F369F665D6A9367AEB2FC85,
	RPGCharacterController_IsActive_m130306AAC8F5EC95A48F81ADB3609FEE9BAEDABB,
	RPGCharacterController_CanStartAction_mB2D54D4669237ECA6AE68E437D3B65A7FBA25D4E,
	RPGCharacterController_CanEndAction_m3F61F5A65376F8855E9A5415860E6866AEF2DC87,
	RPGCharacterController_StartAction_mE7EDEAD7102BC1316F154CD27A07B3C5D388CCF3,
	RPGCharacterController_EndAction_m885A0B955D5CF4A8534A825F764CFC92E9467666,
	RPGCharacterController_LateUpdate_m3D14C58EBF1871360382FE30A2DBBD2FEB6F7D92,
	RPGCharacterController_SetMoveInput_m97638EDFEDF6B267DD16287FB77F18FAC86A8E82,
	RPGCharacterController_SetAimInput_mBF6096273CB4EC9B28609CBC7415F89893B02A38,
	RPGCharacterController_SetJumpInput_m0C9B4CE96C58726A6F4E7A9195B3CC6CC1B70F25,
	RPGCharacterController_DiveRoll_m4F1AA9BC365DFB813ACDA618E6D33B6AB56386A8,
	RPGCharacterController_Knockback_mE66E0C0154CA968D01BBB2ED1C5B9E8CC9B9BCC4,
	RPGCharacterController_Attack_m1BD352F348904EFB89774656924398ABD308E897,
	RPGCharacterController_RunningAttack_mA1BE49BAA87A3F65C2062CAA36DF908500635F88,
	RPGCharacterController_StartStrafe_m0A24FB4CA4AB08EBEA8E7EF6E5B43A9237644D61,
	RPGCharacterController_EndStrafe_m76B25FFACC1AC638412A10CCE36A53E6E2F83D60,
	RPGCharacterController_GetHit_m78DD096282A23D86CDCA0ED20F295024FB3156A7,
	RPGCharacterController_Death_mED7178D069DAEE9387937D04DF7E5CE281706C32,
	RPGCharacterController_Revive_mDB73235F185EF6DF8C16A1DBC58507488728915B,
	RPGCharacterController_StartInjured_m7A5817839A46F5FDC86C0D83925FFE038E74EB34,
	RPGCharacterController_EndInjured_m6F8E7E6E122377741ACE3514DF85E5498408FBBB,
	RPGCharacterController_GetAnimatorTarget_m27C62C2B59FA42AEA2EBB01BC3039EC53634EAF8,
	RPGCharacterController__GetCurrentAnimationLength_mD229559665FA6AC1F35612ACDB196C62C09C7F68,
	RPGCharacterController_Lock_mD15D9B5C5E2BA2D30A010C9E255EDE6EAA5FF1AF,
	RPGCharacterController__Lock_mE9C9D7E89F7BB32274C0B89CC747FAC80868BC62,
	RPGCharacterController_Unlock_m20DDA6D8F0CF68AA113DE6F8F6D67F19E0E0BD66,
	RPGCharacterController_SetIKOff_mE603631F11507DC946E15BC3197C1E756D3DE964,
	RPGCharacterController_SetIKOn_mAFC3A260B1F24DAB794F1BB37613A971B4827D75,
	RPGCharacterController_SetIKPause_m3B3CE1A11B39CA2399CC1D1DB212F5D75D2594FE,
	RPGCharacterController_SetAnimatorTrigger_m266B287DB456F11A211BCB5BEF7FC9F36DDC35FF,
	RPGCharacterController_LegacySetAnimationTrigger_mA9641480FF05F1524F3489391F20A5C7B5D872DC,
	RPGCharacterController_AnimatorDebug_mB88782186EEE035871F025968AE705BC27ECC7DC,
	RPGCharacterController_ControllerDebug_m832587564677230B3A58F0D6524684E5C662A5BA,
	RPGCharacterController__ctor_mEE10A2E0B01F4553EDE74FC595DE8065D177718E,
	U3C_GetCurrentAnimationLengthU3Ed__95__ctor_m820E1614FA3BF119F4E6D408FB35B7C5C518248C,
	U3C_GetCurrentAnimationLengthU3Ed__95_System_IDisposable_Dispose_mAE6F7CE54CD62FA8B0AA7E229A810594DFFD0BBF,
	U3C_GetCurrentAnimationLengthU3Ed__95_MoveNext_mD7E61D8E5510BC7083DE70E02508BAC13247D471,
	U3C_GetCurrentAnimationLengthU3Ed__95_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m075F873D32C075CF5005C95D402DBE4AA5E4A49F,
	U3C_GetCurrentAnimationLengthU3Ed__95_System_Collections_IEnumerator_Reset_mC8A64041A2D031C6A64593FD5B09F37320CF929B,
	U3C_GetCurrentAnimationLengthU3Ed__95_System_Collections_IEnumerator_get_Current_mE452D06B222A0149C226E2C402206483D92AF08D,
	U3C_LockU3Ed__97__ctor_m858C96D1D1601724CAC79BAAB47BEB6B4DAE2857,
	U3C_LockU3Ed__97_System_IDisposable_Dispose_m446A96C040584FAC614CB318B52342DD023DDE08,
	U3C_LockU3Ed__97_MoveNext_m1DE3B9EF500AFDE6C40910ACC0AC93E7A191750A,
	U3C_LockU3Ed__97_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC55693EFED731C44A826ABB2BA3828414978EF62,
	U3C_LockU3Ed__97_System_Collections_IEnumerator_Reset_mA81A2532FF5357D390B2E33E7A583C760A0FD550,
	U3C_LockU3Ed__97_System_Collections_IEnumerator_get_Current_m8455282670C26C0A5E541D4A9284A859286F3C8D,
	U3CU3Ec__cctor_m0723C99271C9596F69E826A059174115A3BB7D96,
	U3CU3Ec__ctor_mEC97A3E7DFE4F6395CEFB2ADEAB329A11013659C,
	U3CU3Ec_U3C_ctorU3Eb__106_0_mF4FCA891562CB83A1BE082382A767980766B9BE8,
	U3CU3Ec_U3C_ctorU3Eb__106_1_m33EBA404EA830A801D26D2B6FE229E4BC8F24F2E,
	U3CU3Ec_U3C_ctorU3Eb__106_2_m61D85B7D4FF42825A16602B77680882225FE9788,
	U3CU3Ec_U3C_ctorU3Eb__106_3_m03CA1E5F177F63BDB7075C232848924D26CC931C,
	RPGCharacterInputController_Awake_m88C7BB4984C505C1CAB24E369622EAB097BC21D1,
	RPGCharacterInputController_Update_mE713950642286938915194EDE77ABB787B984AC7,
	RPGCharacterInputController_PauseInput_m225B8D30C020B8D84CD40C41C9A0921F44AA047B,
	RPGCharacterInputController_Inputs_m725873F9F947A3A936881EEBFB3B5BABEBDEB9AC,
	RPGCharacterInputController_HasMoveInput_mD39B73192ED7DEBE118A88C64944BABA98E9060C,
	RPGCharacterInputController_HasAimInput_m51C0202C2DAFCF3143BFC48C6CD84A7958975289,
	RPGCharacterInputController_Moving_m109FD4A29578F751BE030FE5140E0C5D16C12397,
	RPGCharacterInputController_Rolling_m3EFD8E6ABEFB952C5D96D65BAF521D83E272BAAD,
	RPGCharacterInputController_Strafing_mEE037B343A3E34ECDF9FA2EB919D2F7D86CC7CDA,
	RPGCharacterInputController_Attacking_m1F42AFD204ED9730B076F6CD216B1087196666D7,
	RPGCharacterInputController_Damage_mE2AA7326CFEED2F8457078E112736E60D3FD66BE,
	RPGCharacterInputController_SwitchWeapons_mD71B0D8F35E592933A71066FCF7DF0F36A34729C,
	RPGCharacterInputController__ctor_mF4BC171F71CE94AD516054DA29FE05BFDC6983B3,
	RPGCharacterMovementController_get_lookDirection_m063925638F4666CA88AE6E7CCE64BA415276B65E,
	RPGCharacterMovementController_set_lookDirection_m9469D5CA2F3606C0BD50968AB5C7328B2A692929,
	RPGCharacterMovementController_Awake_m3E92C25FECCDDDBB543430ADB251B7358A339E69,
	RPGCharacterMovementController_Start_m5F5EBEC8AB067EE13C5A98A90AFA6E34F86D5F3A,
	RPGCharacterMovementController_Update_mE750A8552DFD8F18795BE23617F0AB0D025FD77C,
	RPGCharacterMovementController_EarlyGlobalSuperUpdate_m1258850E717C2F7F80B88319CB87BB2D28893D7B,
	RPGCharacterMovementController_LateGlobalSuperUpdate_mF4EB679227E3FD5AB2BC7330A68FA22A36D0C2C0,
	RPGCharacterMovementController_Idle_EnterState_m5FC22CB2D2B59F3223A269B768237F6C44C140E9,
	RPGCharacterMovementController_Idle_SuperUpdate_m104FC1874037BBDA3BAC0C12EC956379728BFEED,
	RPGCharacterMovementController_Move_SuperUpdate_mE8289D3C01B9AED1C01CA457A886417D01A8E9B8,
	RPGCharacterMovementController_Jump_EnterState_m33AE1E8660CD1410A818CBB43152F7616168F956,
	RPGCharacterMovementController_Jump_SuperUpdate_mA656509E44851A2EB3088270F55BDB31A2FF942D,
	RPGCharacterMovementController_Fall_EnterState_m454558B2670EFC79387F54441B6893C0755511B7,
	RPGCharacterMovementController_Fall_SuperUpdate_mDD168651DB8D21AA961E9861D20F06EAF375CD04,
	RPGCharacterMovementController_Fall_ExitState_m1BDA6B72420BAC493BAFC2A01B01AF06CB3F7554,
	RPGCharacterMovementController_DiveRoll_EnterState_m7037A844EBE2BC33E9AC370AF71803166379342E,
	RPGCharacterMovementController_DiveRoll_SuperUpdate_mB4EA80D5A5253DDC0D308FA05765878A537F397F,
	RPGCharacterMovementController_Knockback_EnterState_m465DC6B4EB9A2A486ED0F36F3C093C5281BE1EFF,
	RPGCharacterMovementController_Knockdown_EnterState_m9F7E93F5A81D8E7E32701A18138C538E174F9104,
	RPGCharacterMovementController_Land_mEE32343C8752029F587177A722D4B6CC43577531,
	RPGCharacterMovementController_RotateGravity_mE89BEF0301E7A4C59409FBE6A54656036D427511,
	RPGCharacterMovementController_RotateTowardsMovementDir_m4F13CBC4965303597DF26457755B293981CE374C,
	RPGCharacterMovementController_RotateTowardsTarget_m52595AD70988C697765CD52086B6297A6331926B,
	RPGCharacterMovementController_KnockbackForce_m81BC5D0F2C73F298A619B3DB727DBA27CE7141A7,
	RPGCharacterMovementController__KnockbackForce_m5374C2961CE16214C46F5D6AD4377C463DD44F6B,
	RPGCharacterMovementController_LockMovement_m3EE25EF7A5E5694E2634B762FDC7817461923CF1,
	RPGCharacterMovementController_UnlockMovement_mE29B65F100F03DA3F0A7486EB69AD6F4D2A10A36,
	RPGCharacterMovementController_AnimatorMove_m5BFBB21BCEF30BB0B95A83E97881CE28F86AF9D8,
	RPGCharacterMovementController_IdleOnceAfterMoveUnlock_m47889CB628E046A6604BBDE1F2B0881617D9B74C,
	RPGCharacterMovementController__ctor_m1B9D84D3EA692AF132EF3B877862CF62539F3F81,
	U3CU3Ec__cctor_m159ED8142E888F7E00041B2010434307B738624A,
	U3CU3Ec__ctor_m91D938C59956AFA6ED0E0B89CA70E41EBAC2B0C4,
	U3CU3Ec_U3CAwakeU3Eb__27_0_mD5453EEF8CEA57D9BD4FA4711DDBB8ED12964E99,
	U3CU3Ec_U3CAwakeU3Eb__27_1_mAEF85EE523DE23F87F45F9C11195486B038CE616,
	U3CU3Ec_U3CAwakeU3Eb__27_2_m595CFCB4564A7F08DEBBAD36035B66B34E215C49,
	U3CU3Ec_U3CAwakeU3Eb__27_3_m9F87991FB9E2FB73899F78F2334946A8CC4E9A9A,
	U3C_KnockbackForceU3Ed__49__ctor_mC26880244CCC211FBC58FF9D772E305A2DEFC4FD,
	U3C_KnockbackForceU3Ed__49_System_IDisposable_Dispose_m6F93F7266248AA297DCCDB8D24C46B6B7BF64E9A,
	U3C_KnockbackForceU3Ed__49_MoveNext_m1EE1A8FE2C8A6031EADA494FE1B3A12A9346946A,
	U3C_KnockbackForceU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF48814913A2B0C28A945B01958A2B714E16F397D,
	U3C_KnockbackForceU3Ed__49_System_Collections_IEnumerator_Reset_m76B57B78E1A54B8B0C25D0D0CACCE6472CD19A6A,
	U3C_KnockbackForceU3Ed__49_System_Collections_IEnumerator_get_Current_m2E6B145E5970B0197E9F3BACDDDF6F6894DB4856,
	RPGCharacterNavigationController_Awake_m4774C7E1BFF348302FC56FEA09444D129803F0F2,
	RPGCharacterNavigationController_Start_mF66E228D321AD920B11AB3081C46AEECFB660AEE,
	RPGCharacterNavigationController_Update_m719FAACCAFED8E998E0A6D2D93C5CE6CB3DD1668,
	RPGCharacterNavigationController_MeshNavToPoint_mEA52E62F4AB475BC1E0A673267041739AB5EF169,
	RPGCharacterNavigationController_StopNavigating_m2D0AC4FFEA41FC1FDF7AAB286B35EDB45391D8FA,
	RPGCharacterNavigationController_RotateTowardsMovementDir_mDE21029AD6E36B6890B0FDB71714913D001CE963,
	RPGCharacterNavigationController__ctor_m41A486C2FF90611822BA7DAF461823524D84F956,
	RPGCharacterWeaponController_Awake_m3EB2F9F68A368A24E16D21697F229FA851C7925B,
	RPGCharacterWeaponController_Start_m58027A54865BC735B18C8D2D7226784BF362FAC9,
	RPGCharacterWeaponController_AddCallback_mB1FBF08DC56DCB99EB3D152C5DEEE841DD263D52,
	RPGCharacterWeaponController_UnsheathWeapon_m7B48E75415C2AF55BA9ABCCB64E10277AF67E609,
	RPGCharacterWeaponController__UnSheathWeapon_mCBE1E8D97A50B27043582606A59D3C2E84DB1167,
	RPGCharacterWeaponController_SheathWeapon_m8A26AC6ECD6E98B0AB0D295F8B9F24538ECBCA69,
	RPGCharacterWeaponController__SheathWeapon_mF34A3E18E72E697D7E34D6DE56AAB85E883BEB3B,
	RPGCharacterWeaponController_InstantWeaponSwitch_mA7B3B4C922B14D24617F3DECECCE3EAD3802EE75,
	RPGCharacterWeaponController__InstantWeaponSwitch_m6CFAE0E20135EB42F641F2ABFC0B22F9B21B6BC1,
	RPGCharacterWeaponController_DoWeaponSwitch_mC27544BD18561AD99CC3D93A145C172C14E38D85,
	RPGCharacterWeaponController_SetAnimator_mC1BC8001A763D69AE30CDEDFFE94B69AD1D17887,
	RPGCharacterWeaponController_WeaponSwitch_mA7479BAA2BC72D6591A6B70940B701E69301DBB2,
	RPGCharacterWeaponController_SafeSetVisibility_m9A16D26CE59C1B89E4231D182BB96EA50E485EF9,
	RPGCharacterWeaponController_HideAllWeapons_m4A5BDF0597E52CE74C43E3126B3F26793BD8EAFB,
	RPGCharacterWeaponController__HideAllWeapons_m5F559824E2D7B73AD31F853C3388B06EDD867D8D,
	RPGCharacterWeaponController__WeaponVisibility_m31582BCD48FA5682C44B494793DC7FD64C49DA68,
	RPGCharacterWeaponController_SyncWeaponVisibility_m46355FFFFF4CD7FF8EC826C24E8F92C45929828E,
	RPGCharacterWeaponController__SyncWeaponVisibility_mCF49A056C5EC851FE38BEAA0CBA3822C2832C741,
	RPGCharacterWeaponController__ctor_m1D3D183B3F07BF4729AEAAB7D9D710870129A4C4,
	U3C_UnSheathWeaponU3Ed__9__ctor_m35EF04213975D5024096F3C5203F3F3E3CF563DB,
	U3C_UnSheathWeaponU3Ed__9_System_IDisposable_Dispose_mFFDB7C9AADCAD47507EBCA608C0125F97449F64F,
	U3C_UnSheathWeaponU3Ed__9_MoveNext_mD9ABF285126C71C418C045E781D49AE3AA024DFE,
	U3C_UnSheathWeaponU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13824AADAF17EE515BE9818C86AD40D0EEE6B853,
	U3C_UnSheathWeaponU3Ed__9_System_Collections_IEnumerator_Reset_mF8A2AE8748E3D770E7204F5242535345291B8537,
	U3C_UnSheathWeaponU3Ed__9_System_Collections_IEnumerator_get_Current_m00675F56A30AFFA495A568595D6D46DFD88DA802,
	U3C_SheathWeaponU3Ed__11__ctor_mD8C60AD3822076D0195EE8D0014484C5E329D5A0,
	U3C_SheathWeaponU3Ed__11_System_IDisposable_Dispose_mCECFAA1D79B9B4AAF1989E8966E9EEEE0A562FE3,
	U3C_SheathWeaponU3Ed__11_MoveNext_m5302FC3A8B7A531E9FF3A2D4FEA9C2216F675ACA,
	U3C_SheathWeaponU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99DA089381A786F95D08201701BBE86B3F423279,
	U3C_SheathWeaponU3Ed__11_System_Collections_IEnumerator_Reset_mF316EA696832C83D880B8F4501148582BCC7A117,
	U3C_SheathWeaponU3Ed__11_System_Collections_IEnumerator_get_Current_m2A5C387020C654BB643BB48A47F48295C59056B3,
	U3C_InstantWeaponSwitchU3Ed__13__ctor_m054A86DAF9D6CE1731E3C81D09562B4BC53F3B0B,
	U3C_InstantWeaponSwitchU3Ed__13_System_IDisposable_Dispose_mC08D1CCB13EC4395A8659AA769ACC4FDE16FB574,
	U3C_InstantWeaponSwitchU3Ed__13_MoveNext_mCCA604506384190AC09F366BC379633025811291,
	U3C_InstantWeaponSwitchU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC58A3DFADCCFCACEF3619D890E9859AD8ADEE088,
	U3C_InstantWeaponSwitchU3Ed__13_System_Collections_IEnumerator_Reset_m7908D76E026D81EA56A3A75F79FD8A642DE14550,
	U3C_InstantWeaponSwitchU3Ed__13_System_Collections_IEnumerator_get_Current_m5D327883E84027F957F96C903E1BCF63ACD04A5E,
	U3C_HideAllWeaponsU3Ed__19__ctor_m5F459CBCCE6E36951C8FBDFD3406A3FD3CDA7938,
	U3C_HideAllWeaponsU3Ed__19_System_IDisposable_Dispose_m3254389CD405A77B2D627C7777BBD9FFEE2DA484,
	U3C_HideAllWeaponsU3Ed__19_MoveNext_m92E07C9F9F9C471A0C6C6096E7AB7DA575126198,
	U3C_HideAllWeaponsU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32AF76043CDAD36506A8C6E1B6EBE7725B24D84C,
	U3C_HideAllWeaponsU3Ed__19_System_Collections_IEnumerator_Reset_m70B0F68F4E3BC5FEFAE803B377BB6F863401AECB,
	U3C_HideAllWeaponsU3Ed__19_System_Collections_IEnumerator_get_Current_mD58C7C87115170DDEAE9F7D1C864C7F08844ACB9,
	U3C_WeaponVisibilityU3Ed__20__ctor_m3E0075C29D08E0D2C8569909146F58122470DC9E,
	U3C_WeaponVisibilityU3Ed__20_System_IDisposable_Dispose_m1B5DF7513EB6D60AEDE8E3352F3777EF4C73C27A,
	U3C_WeaponVisibilityU3Ed__20_MoveNext_mBDC6DF064CF8F303C68FD0A6FA207BE43F857111,
	U3C_WeaponVisibilityU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m858E70C7B28AC5D9F5D3E86D15401B554E62EB48,
	U3C_WeaponVisibilityU3Ed__20_System_Collections_IEnumerator_Reset_m02BDEAD0D20D8B473485DB85D6D69E6E5804526A,
	U3C_WeaponVisibilityU3Ed__20_System_Collections_IEnumerator_get_Current_m3DB300525C9D2E2F1EB03F8D2163BFA877500618,
	U3C_SyncWeaponVisibilityU3Ed__22__ctor_m9895919C033AB1121CDB7355806D934CBF9118C8,
	U3C_SyncWeaponVisibilityU3Ed__22_System_IDisposable_Dispose_m7FCEA4AA9D8BAC6F32518E9B04ACC2E5333A3CF5,
	U3C_SyncWeaponVisibilityU3Ed__22_MoveNext_m253A6F7A375AE0775D2CBDF0F292A3C435E878C9,
	U3C_SyncWeaponVisibilityU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC34B2E46A81F4D4302BDFF891014441B5EFC3A8B,
	U3C_SyncWeaponVisibilityU3Ed__22_System_Collections_IEnumerator_Reset_mFC0CC2C310E15F4BAB476698361D8564E28A2B77,
	U3C_SyncWeaponVisibilityU3Ed__22_System_Collections_IEnumerator_get_Current_m771FD86E17A757E999DAC38D10C9C81833F550CE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmptyContext__ctor_m021DE6350B2BC16C16D0BDB7EAEF607240FBA370,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AttackContext__ctor_mD29DB3230B2A6694F3E2AB34A63A363E7651B285,
	AttackContext__ctor_m88D946E1909E5FAD47D02D71119246BF54948A72,
	Attack_CanStartAction_m088DE64C01FB9E1DC4AEC66584E81626CA0BB8F0,
	Attack_CanEndAction_m0D7BEB9B5B306F2FF983A80903D8CCE4983D3E7A,
	Attack__StartAction_mC35588F8F80B12300AA79B871D897282572C8722,
	Attack__EndAction_m3561DE66897FC4BA463B9C0B83E5836B78F2030E,
	Attack__ctor_m6D7AEE5D9BB61F5C489DBCC85B3ACBDC2BDCE4B8,
	SlowTime_CanStartAction_m638FFA209DDFA26E3AE30E3F302929B376D72D7C,
	SlowTime_CanEndAction_m26167CB69668805F4F8B9B4A8B0DA0A2E1EE9CE6,
	SlowTime__StartAction_m8D92DC8BE56729E937636E2DA29539627009763A,
	SlowTime__EndAction_m760B68FEEBE039EF14EB4692C0431D2A51BB907D,
	SlowTime__ctor_m4B3A3C2B751CBA8A9BC52C426965EE87A74A1E7D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DiveRoll__ctor_mAD1D9E48A34A0F61EE94F3F669908AF5FC413F9C,
	DiveRoll_CanStartAction_mAB302B41A0DD18E3FBFBE15E50B007F3C45FB1F3,
	DiveRoll__StartAction_m8318DF3EDE8C6FD6CB652DE3ED33B6A1FFE0765E,
	DiveRoll_IsActive_m1A44941F01874B84DC93351F2C32EA6C2857780B,
	Fall__ctor_mC1900315D39F1EE3ED3A80A017178087573C3F22,
	Fall_CanStartAction_m5227886882ACCC9526C72D6BED310F30DE9B07BC,
	Fall__StartAction_mF8DE606A2272B3D17CDC43D614A6796D0378C829,
	Fall_IsActive_m43D1662EC4F2180EA73AC172267AC9587B5036A6,
	HitContext__ctor_m42A9FFCA8D88CD97594266A26F08EB2C6867C114,
	HitContext__ctor_m0C3CF9C94A3010B7B3A7CE378B469805C9F1D380,
	GetHit__ctor_m0D39F4B3FD52624518C9118D7E2D25287A76BB12,
	GetHit_CanStartAction_m317BA828DF5AA2361677720DCA4AB3EEC068D574,
	GetHit__StartAction_mCDD4CCFF729CC41AEE2B0FB6C8599353576E4EFD,
	Idle__ctor_mBB8BB9B9F8F9E622215981DEC915949226632F39,
	Idle_CanStartAction_m8299ACF180735866C908B126221EBBD75E964BAA,
	Idle__StartAction_m027917ECE016835D6D631B2AAA85C5E6FD334EB7,
	Idle_IsActive_m30C0B56D3E42B399518BCB33934A73EDFB4AEB1E,
	Jump__ctor_m9BD135F97F57736CDBB800AEBF80C30C5FD0061E,
	Jump_CanStartAction_mA3E123D4B58517B0A786688587ED26E7ED6A3AD7,
	Jump__StartAction_mFA8C73EF73075DD69D03122304F04F049FF57F5B,
	Jump_IsActive_m07F981BC441BDDBDE275874D26FA5B928174EE58,
	Knockback__ctor_mDB8378DC573ECFC6591F757F7AFD07994881319B,
	Knockback_CanStartAction_m50DE29784539C5024114D2DABD81A364DD472F4C,
	Knockback__StartAction_mB65ECED100898611282317D4185314A118E7DF06,
	Knockback_IsActive_mF54369D4AA12C9CC022C75206CE26F7617A86BBF,
	Move__ctor_m099534491010C8D474D34FBE822EA77FC00FE122,
	Move_CanStartAction_m365CE578BC195EBBB2A558253B52870CF324EC75,
	Move__StartAction_m8632CA4C4CAEF6320A6E2F35874B20323F39BC64,
	Move_IsActive_m96CAAFFAF9180A2241E2724EE1A010444BF38B0C,
	NULL,
	Navigation__ctor_m642C4D9CFF3C270BFBF0341EC9E11BE8F4531E91,
	Navigation_CanStartAction_m0A93A8BC29B3470B0FD7745E89DC1F2E1480DD2E,
	Navigation_CanEndAction_mA4A7C343CCE894FEC8973D487CC1455FDEDE8FA9,
	Navigation__StartAction_mBD9D174CD6432900CECE45ABFEBFAFA6C1406B54,
	Navigation_IsActive_mE5B36A8E723A9A4F28184E197BD2AEF4CA003339,
	Navigation__EndAction_mE36C6DEB720793A1808F39F748A929BAD1EE5870,
	Null_CanStartAction_m9053403901143F6D862BAC9F5DFC7552853CE705,
	Null__StartAction_m785795E5A5C39E45AA6114877F8DF399153C0D33,
	Null__ctor_m02B8F546ABDD54D5DF30F34A8F2C7CFBD740DB1B,
	SimpleActionHandler__ctor_m9A2E08FFBE5C4F2E005F66DE0F7B8A95D61C1DCE,
	SimpleActionHandler_CanStartAction_mB85DFAF1D3667D528330040D9DD999C7624B48E0,
	SimpleActionHandler_CanEndAction_m54C4081AC73564364D6BFE77D44D3A86EB96B99C,
	SimpleActionHandler__StartAction_mEC5C915984682D1A5485ECA29D307909C7DD0EDE,
	SimpleActionHandler__EndAction_m9C3F76A950F4D5FC2C25DCC1B0509CA6E7BDA629,
	SwitchWeaponContext__ctor_m03A1E045A724AFBEBAE199E108236AB6339EFB8E,
	SwitchWeaponContext__ctor_m14A194BFA56E31109676567AC7288CB4F63CDC36,
	SwitchWeaponContext_LowercaseStrings_m0E3F486C47FFFF2989329E21DFA2B1FB5E927128,
	SwitchWeapon_CanStartAction_mD59FB525E543166C6855682712DED119BA3842C2,
	SwitchWeapon_CanEndAction_mB8F81811FDA6F42D9018EA2DAEB73CE2797AC4C1,
	SwitchWeapon__StartAction_m97BE2E361D3F4CCC9132615376F4F681626E2744,
	SwitchWeapon__EndAction_m56397AE4418E7641FA932C4CF95B8DA98734043D,
	SwitchWeapon__ctor_m6218D9A97B13CC7B80BB0F4F319B457941D9D443,
	U3CU3Ec__DisplayClass2_0__ctor_m451C31F453B0C3B4672BA3C704CD78AD74E5A025,
	U3CU3Ec__DisplayClass2_0_U3C_StartActionU3Eb__0_m5602C48389E0FBD3F67C9B52D566A10389CF8AF4,
	CameraController_Start_m903E8D535DC295F2251E321100B1CE43F44BD2AE,
	CameraController_Update_m063E87A31AB973C689F6581C92CA311602D45C8A,
	CameraController_CameraFollow_m8418659B4F607A54981DE39C59AE0495F0DA716F,
	CameraController_LateUpdate_m1218496F933C9BA09028DD7C5832C66E698FF318,
	CameraController__ctor_mC45448DAC196F8DD678E07D8B869C939706150F5,
};
static const int32_t s_InvokerIndices[2128] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1552,
	1553,
	23,
	23,
	23,
	89,
	89,
	1553,
	1552,
	3184,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	-1,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	3185,
	3186,
	3187,
	23,
	107,
	3188,
	3189,
	3190,
	3191,
	1627,
	23,
	23,
	23,
	1538,
	3187,
	3192,
	23,
	737,
	340,
	14,
	26,
	14,
	26,
	14,
	26,
	737,
	1552,
	1552,
	14,
	26,
	14,
	26,
	737,
	340,
	737,
	340,
	89,
	31,
	26,
	26,
	23,
	23,
	340,
	23,
	32,
	89,
	23,
	23,
	23,
	23,
	23,
	89,
	129,
	23,
	23,
	23,
	2158,
	1563,
	1563,
	26,
	26,
	23,
	23,
	3193,
	3194,
	3193,
	3194,
	3193,
	3194,
	3193,
	3194,
	14,
	26,
	14,
	26,
	3195,
	124,
	23,
	105,
	26,
	130,
	3196,
	14,
	26,
	14,
	26,
	1819,
	23,
	3197,
	3198,
	3199,
	1552,
	737,
	3200,
	3201,
	1552,
	1553,
	1552,
	1553,
	737,
	340,
	1828,
	2987,
	3202,
	3203,
	3203,
	3203,
	3204,
	23,
	3205,
	2349,
	3206,
	1772,
	443,
	445,
	14,
	26,
	23,
	23,
	-1,
	23,
	23,
	23,
	3,
	23,
	23,
	3207,
	3208,
	3209,
	3210,
	3211,
	3212,
	1734,
	3,
	1753,
	1753,
	1764,
	3213,
	3214,
	3214,
	3213,
	3215,
	3215,
	3215,
	1748,
	1750,
	1750,
	1750,
	1749,
	1749,
	3216,
	1761,
	1761,
	1761,
	3217,
	3218,
	3219,
	3220,
	3221,
	3222,
	3223,
	1749,
	3224,
	3225,
	3226,
	3227,
	23,
	3,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	2489,
	2490,
	2491,
	2492,
	2493,
	9,
	14,
	14,
	23,
	23,
	3228,
	2538,
	26,
	14,
	14,
	23,
	23,
	89,
	3229,
	26,
	2650,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	23,
	23,
	32,
	34,
	26,
	23,
	23,
	3230,
	340,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	9,
	9,
	89,
	32,
	23,
	34,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	26,
	23,
	23,
	23,
	89,
	951,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	9,
	89,
	23,
	23,
	26,
	26,
	34,
	32,
	32,
	23,
	23,
	106,
	106,
	23,
	3,
	14,
	14,
	14,
	23,
	23,
	26,
	26,
	23,
	3,
	23,
	9,
	9,
	9,
	89,
	23,
	23,
	23,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	579,
	23,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	26,
	23,
	23,
	10,
	10,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	579,
	23,
	10,
	10,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	26,
	23,
	23,
	10,
	10,
	10,
	737,
	737,
	737,
	89,
	10,
	10,
	10,
	579,
	23,
	10,
	10,
	10,
	737,
	737,
	737,
	89,
	10,
	10,
	10,
	26,
	23,
	23,
	89,
	23,
	1561,
	1560,
	1561,
	1560,
	23,
	26,
	23,
	-1,
	-1,
	-1,
	106,
	4,
	23,
	3,
	10,
	26,
	34,
	28,
	23,
	23,
	23,
	9,
	3,
	23,
	9,
	9,
	4,
	-1,
	14,
	23,
	23,
	26,
	34,
	28,
	23,
	23,
	3,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	89,
	23,
	89,
	23,
	89,
	14,
	14,
	34,
	10,
	26,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	43,
	23,
	209,
	23,
	23,
	27,
	23,
	27,
	23,
	23,
	23,
	23,
	89,
	23,
	23,
	32,
	202,
	202,
	202,
	23,
	3231,
	10,
	10,
	10,
	737,
	737,
	737,
	737,
	737,
	737,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	579,
	23,
	23,
	10,
	10,
	10,
	737,
	737,
	737,
	737,
	737,
	737,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	26,
	23,
	14,
	23,
	26,
	34,
	34,
	34,
	23,
	89,
	32,
	23,
	202,
	202,
	202,
	23,
	23,
	89,
	23,
	23,
	32,
	32,
	202,
	3232,
	23,
	23,
	10,
	737,
	737,
	737,
	10,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	579,
	23,
	10,
	737,
	737,
	737,
	10,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	737,
	26,
	23,
	23,
	23,
	26,
	34,
	202,
	37,
	23,
	23,
	89,
	23,
	32,
	32,
	23,
	23,
	202,
	23,
	23,
	23,
	3233,
	23,
	23,
	27,
	23,
	27,
	89,
	23,
	14,
	209,
	89,
	23,
	114,
	114,
	0,
	114,
	4,
	14,
	9,
	9,
	28,
	9,
	23,
	23,
	23,
	23,
	23,
	23,
	9,
	3,
	23,
	9,
	41,
	0,
	2097,
	0,
	2108,
	0,
	2112,
	3234,
	3235,
	3235,
	3236,
	1223,
	53,
	173,
	3,
	23,
	9,
	23,
	9,
	23,
	9,
	14,
	14,
	10,
	10,
	23,
	1159,
	1092,
	23,
	26,
	23,
	26,
	26,
	26,
	14,
	14,
	26,
	26,
	27,
	23,
	9,
	1206,
	944,
	9,
	141,
	9,
	28,
	28,
	28,
	105,
	9,
	23,
	26,
	1553,
	23,
	124,
	27,
	125,
	26,
	124,
	27,
	125,
	26,
	124,
	27,
	125,
	26,
	23,
	9,
	23,
	9,
	23,
	9,
	23,
	9,
	3,
	23,
	9,
	14,
	14,
	14,
	209,
	26,
	1553,
	209,
	14,
	23,
	0,
	3237,
	3203,
	3203,
	3237,
	3237,
	3203,
	3203,
	3237,
	14,
	14,
	89,
	89,
	89,
	89,
	14,
	209,
	23,
	23,
	89,
	23,
	89,
	89,
	89,
	225,
	89,
	23,
	1553,
	1552,
	1552,
	1552,
	1553,
	1552,
	1553,
	1553,
	23,
	23,
	23,
	89,
	14,
	10,
	446,
	23,
	23,
	23,
	23,
	23,
	89,
	23,
	23,
	23,
	89,
	23,
	23,
	23,
	23,
	23,
	9,
	28,
	23,
	3,
	23,
	9,
	23,
	9,
	10,
	10,
	14,
	10,
	10,
	10,
	10,
	10,
	579,
	23,
	23,
	10,
	10,
	14,
	10,
	10,
	10,
	10,
	10,
	26,
	23,
	89,
	202,
	32,
	23,
	23,
	23,
	26,
	34,
	23,
	89,
	23,
	23,
	23,
	23,
	23,
	202,
	32,
	34,
	34,
	28,
	34,
	28,
	34,
	28,
	34,
	28,
	14,
	23,
	94,
	0,
	0,
	43,
	0,
	43,
	0,
	43,
	0,
	43,
	0,
	4,
	43,
	10,
	14,
	14,
	579,
	23,
	23,
	89,
	23,
	23,
	23,
	34,
	34,
	28,
	34,
	28,
	34,
	28,
	34,
	34,
	28,
	202,
	136,
	34,
	23,
	14,
	89,
	32,
	23,
	34,
	23,
	34,
	28,
	14,
	23,
	23,
	579,
	10,
	34,
	28,
	27,
	23,
	14,
	14,
	26,
	14,
	209,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	209,
	23,
	27,
	28,
	23,
	23,
	9,
	-1,
	-1,
	-1,
	-1,
	23,
	10,
	10,
	14,
	23,
	579,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	10,
	14,
	14,
	10,
	737,
	737,
	10,
	10,
	10,
	10,
	737,
	737,
	737,
	737,
	14,
	14,
	579,
	23,
	23,
	94,
	94,
	94,
	94,
	3,
	14,
	26,
	14,
	89,
	27,
	23,
	202,
	14,
	14,
	37,
	23,
	23,
	23,
	10,
	14,
	14,
	10,
	737,
	737,
	10,
	10,
	10,
	10,
	737,
	737,
	737,
	737,
	14,
	14,
	26,
	23,
	23,
	14,
	14,
	26,
	202,
	37,
	23,
	23,
	89,
	26,
	14,
	14,
	209,
	26,
	133,
	133,
	23,
	23,
	89,
	27,
	23,
	23,
	23,
	23,
	23,
	10,
	10,
	372,
	23,
	27,
	27,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1818,
	1818,
	30,
	32,
	30,
	32,
	37,
	30,
	32,
	2142,
	2142,
	30,
	30,
	32,
	37,
	37,
	1818,
	1818,
	30,
	32,
	37,
	37,
	1615,
	1818,
	37,
	37,
	1818,
	974,
	52,
	202,
	23,
	23,
	23,
	23,
	52,
	1818,
	1818,
	30,
	32,
	30,
	32,
	37,
	30,
	32,
	2142,
	2142,
	30,
	30,
	32,
	37,
	37,
	1818,
	30,
	32,
	37,
	1818,
	37,
	1615,
	1818,
	37,
	37,
	1818,
	23,
	23,
	90,
	23,
	23,
	23,
	89,
	89,
	14,
	23,
	23,
	89,
	26,
	209,
	14,
	14,
	23,
	133,
	26,
	26,
	26,
	23,
	26,
	23,
	23,
	23,
	27,
	209,
	34,
	23,
	27,
	23,
	23,
	9,
	27,
	26,
	23,
	23,
	23,
	89,
	23,
	23,
	34,
	23,
	23,
	10,
	10,
	14,
	10,
	10,
	10,
	10,
	737,
	14,
	579,
	23,
	10,
	10,
	14,
	10,
	10,
	10,
	10,
	737,
	14,
	26,
	23,
	89,
	34,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	23,
	23,
	133,
	23,
	10,
	10,
	89,
	133,
	129,
	23,
	23,
	23,
	14,
	14,
	10,
	10,
	10,
	10,
	10,
	10,
	32,
	10,
	32,
	10,
	1027,
	1027,
	10,
	1027,
	1027,
	10,
	1027,
	1027,
	23,
	129,
	23,
	89,
	23,
	23,
	10,
	10,
	10,
	10,
	10,
	10,
	32,
	10,
	32,
	10,
	10,
	10,
	1027,
	1027,
	1027,
	1027,
	1027,
	1027,
	26,
	23,
	23,
	23,
	10,
	89,
	23,
	1027,
	23,
	23,
	10,
	89,
	10,
	89,
	23,
	23,
	23,
	1027,
	1027,
	23,
	23,
	14,
	89,
	23,
	23,
	32,
	34,
	23,
	23,
	34,
	14,
	89,
	89,
	89,
	32,
	23,
	23,
	23,
	23,
	23,
	1941,
	23,
	23,
	23,
	23,
	1552,
	23,
	23,
	26,
	1553,
	1804,
	26,
	23,
	10,
	89,
	23,
	27,
	23,
	23,
	23,
	372,
	23,
	27,
	27,
	23,
	89,
	23,
	10,
	1552,
	27,
	23,
	23,
	23,
	10,
	10,
	1552,
	27,
	23,
	23,
	23,
	372,
	23,
	10,
	27,
	23,
	23,
	23,
	372,
	27,
	23,
	27,
	23,
	23,
	23,
	372,
	23,
	10,
	737,
	1552,
	27,
	23,
	23,
	23,
	372,
	23,
	1552,
	27,
	23,
	23,
	23,
	372,
	23,
	27,
	27,
	23,
	1820,
	89,
	23,
	27,
	23,
	23,
	23,
	372,
	23,
	27,
	27,
	23,
	1820,
	89,
	27,
	23,
	23,
	23,
	372,
	23,
	27,
	27,
	23,
	1552,
	89,
	23,
	27,
	23,
	23,
	23,
	372,
	23,
	27,
	27,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1,
	-1,
	0,
	-1,
	-1,
	561,
	0,
	3238,
	0,
	1,
	114,
	205,
	119,
	119,
	14,
	14,
	27,
	23,
	10,
	10,
	23,
	32,
	27,
	28,
	34,
	23,
	23,
	9,
	10,
	26,
	23,
	28,
	34,
	34,
	136,
	202,
	136,
	202,
	34,
	2586,
	3239,
	651,
	3240,
	3239,
	3241,
	23,
	23,
	9,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	26,
	26,
	26,
	23,
	14,
	10,
	10,
	737,
	737,
	737,
	737,
	737,
	579,
	23,
	23,
	14,
	89,
	27,
	23,
	89,
	27,
	3242,
	27,
	3242,
	3243,
	3242,
	23,
	89,
	27,
	3242,
	27,
	3244,
	3242,
	23,
	89,
	23,
	27,
	23,
	14,
	23,
	23,
	118,
	27,
	27,
	23,
	3,
	89,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	89,
	27,
	23,
	14,
	23,
	177,
	23,
	3,
	23,
	23,
	89,
	23,
	23,
	27,
	23,
	89,
	23,
	23,
	23,
	9,
	27,
	27,
	23,
	27,
	23,
	89,
	27,
	23,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	27,
	26,
	23,
	89,
	27,
	23,
	89,
	23,
	23,
	23,
	23,
	1516,
	459,
	27,
	23,
	23,
	89,
	23,
	23,
	1516,
	10,
	37,
	27,
	3245,
	3245,
	3246,
	27,
	209,
	23,
	3247,
	23,
	3,
	89,
	23,
	23,
	23,
	27,
	23,
	89,
	27,
	23,
	89,
	27,
	1586,
	23,
	27,
	23,
	23,
	14,
	23,
	23,
	3248,
	137,
	198,
	23,
	23,
	23,
	27,
	23,
	89,
	27,
	702,
	23,
	3,
	23,
	14,
	23,
	23,
	27,
	23,
	105,
	105,
	23,
	89,
	89,
	23,
	225,
	669,
	89,
	23,
	9,
	23,
	27,
	23,
	27,
	3249,
	137,
	3250,
	23,
	49,
	3,
	301,
	1714,
	23,
	3,
	89,
	27,
	23,
	89,
	23,
	27,
	23,
	0,
	163,
	23,
	23,
	23,
	23,
	27,
	23,
	27,
	23,
	89,
	27,
	23,
	89,
	27,
	23,
	89,
	737,
	23,
	89,
	27,
	23,
	49,
	3,
	301,
	1714,
	23,
	3,
	27,
	23,
	89,
	27,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	26,
	26,
	23,
	23,
	3,
	23,
	23,
	177,
	177,
	46,
	46,
	46,
	46,
	46,
	46,
	3251,
	3164,
	177,
	21,
	94,
	106,
	94,
	3252,
	23,
	62,
	26,
	26,
	28,
	28,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	32,
	3253,
	3254,
	340,
	1254,
	23,
	23,
	32,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	1552,
	1552,
	1552,
	1552,
	89,
	89,
	89,
	89,
	23,
	27,
	28,
	9,
	9,
	9,
	9,
	27,
	26,
	23,
	1553,
	1553,
	1553,
	32,
	32,
	3255,
	3256,
	23,
	23,
	32,
	23,
	23,
	23,
	23,
	14,
	14,
	3257,
	3258,
	42,
	23,
	23,
	340,
	32,
	26,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	340,
	23,
	89,
	89,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1552,
	1553,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1553,
	23,
	1553,
	3259,
	3260,
	23,
	23,
	1822,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	1553,
	23,
	23,
	23,
	23,
	23,
	26,
	133,
	34,
	1151,
	202,
	32,
	34,
	1686,
	523,
	23,
	459,
	23,
	373,
	3261,
	23,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	9,
	27,
	26,
	26,
	89,
	9,
	26,
	26,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	35,
	118,
	9,
	9,
	27,
	26,
	23,
	9,
	9,
	951,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	9,
	130,
	89,
	26,
	9,
	27,
	89,
	23,
	3262,
	26,
	9,
	27,
	26,
	9,
	27,
	89,
	26,
	9,
	27,
	89,
	26,
	9,
	27,
	89,
	26,
	9,
	27,
	89,
	-1,
	26,
	9,
	9,
	1829,
	89,
	26,
	9,
	27,
	23,
	27,
	9,
	9,
	27,
	26,
	23,
	1200,
	23,
	9,
	9,
	27,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[26] = 
{
	{ 0x02000040, { 4, 3 } },
	{ 0x02000047, { 9, 11 } },
	{ 0x02000048, { 20, 3 } },
	{ 0x0200004A, { 23, 19 } },
	{ 0x0200008E, { 42, 4 } },
	{ 0x02000091, { 46, 2 } },
	{ 0x02000095, { 48, 7 } },
	{ 0x02000097, { 55, 13 } },
	{ 0x020000A4, { 77, 15 } },
	{ 0x020000B3, { 92, 15 } },
	{ 0x020000D7, { 107, 6 } },
	{ 0x020000D8, { 113, 12 } },
	{ 0x020000DF, { 132, 5 } },
	{ 0x020000E0, { 137, 3 } },
	{ 0x0200014C, { 140, 12 } },
	{ 0x0200014D, { 152, 3 } },
	{ 0x02000151, { 155, 4 } },
	{ 0x0200015A, { 159, 3 } },
	{ 0x06000025, { 0, 2 } },
	{ 0x060000A7, { 2, 2 } },
	{ 0x060001E9, { 7, 2 } },
	{ 0x060003E3, { 68, 4 } },
	{ 0x060003E4, { 72, 5 } },
	{ 0x060005AB, { 125, 1 } },
	{ 0x060005AD, { 126, 1 } },
	{ 0x060005AE, { 127, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[162] = 
{
	{ (Il2CppRGCTXDataType)1, 21497 },
	{ (Il2CppRGCTXDataType)2, 21497 },
	{ (Il2CppRGCTXDataType)1, 21542 },
	{ (Il2CppRGCTXDataType)2, 21542 },
	{ (Il2CppRGCTXDataType)2, 22904 },
	{ (Il2CppRGCTXDataType)2, 21622 },
	{ (Il2CppRGCTXDataType)3, 33767 },
	{ (Il2CppRGCTXDataType)2, 21634 },
	{ (Il2CppRGCTXDataType)3, 33768 },
	{ (Il2CppRGCTXDataType)3, 33769 },
	{ (Il2CppRGCTXDataType)3, 33770 },
	{ (Il2CppRGCTXDataType)2, 22905 },
	{ (Il2CppRGCTXDataType)3, 33771 },
	{ (Il2CppRGCTXDataType)3, 33772 },
	{ (Il2CppRGCTXDataType)2, 821 },
	{ (Il2CppRGCTXDataType)3, 33773 },
	{ (Il2CppRGCTXDataType)3, 33774 },
	{ (Il2CppRGCTXDataType)2, 822 },
	{ (Il2CppRGCTXDataType)3, 33775 },
	{ (Il2CppRGCTXDataType)3, 33776 },
	{ (Il2CppRGCTXDataType)3, 33777 },
	{ (Il2CppRGCTXDataType)2, 21645 },
	{ (Il2CppRGCTXDataType)3, 33778 },
	{ (Il2CppRGCTXDataType)2, 21652 },
	{ (Il2CppRGCTXDataType)3, 33779 },
	{ (Il2CppRGCTXDataType)3, 33780 },
	{ (Il2CppRGCTXDataType)2, 22906 },
	{ (Il2CppRGCTXDataType)3, 33781 },
	{ (Il2CppRGCTXDataType)3, 33782 },
	{ (Il2CppRGCTXDataType)3, 33783 },
	{ (Il2CppRGCTXDataType)3, 33784 },
	{ (Il2CppRGCTXDataType)2, 22907 },
	{ (Il2CppRGCTXDataType)3, 33785 },
	{ (Il2CppRGCTXDataType)3, 33786 },
	{ (Il2CppRGCTXDataType)2, 21653 },
	{ (Il2CppRGCTXDataType)3, 33787 },
	{ (Il2CppRGCTXDataType)2, 21654 },
	{ (Il2CppRGCTXDataType)3, 33788 },
	{ (Il2CppRGCTXDataType)3, 33789 },
	{ (Il2CppRGCTXDataType)3, 33790 },
	{ (Il2CppRGCTXDataType)2, 22908 },
	{ (Il2CppRGCTXDataType)2, 21655 },
	{ (Il2CppRGCTXDataType)2, 21813 },
	{ (Il2CppRGCTXDataType)3, 33791 },
	{ (Il2CppRGCTXDataType)3, 33792 },
	{ (Il2CppRGCTXDataType)3, 33793 },
	{ (Il2CppRGCTXDataType)2, 21820 },
	{ (Il2CppRGCTXDataType)3, 33794 },
	{ (Il2CppRGCTXDataType)3, 33795 },
	{ (Il2CppRGCTXDataType)3, 33796 },
	{ (Il2CppRGCTXDataType)3, 33797 },
	{ (Il2CppRGCTXDataType)3, 33798 },
	{ (Il2CppRGCTXDataType)2, 22909 },
	{ (Il2CppRGCTXDataType)3, 33799 },
	{ (Il2CppRGCTXDataType)3, 33800 },
	{ (Il2CppRGCTXDataType)3, 33801 },
	{ (Il2CppRGCTXDataType)2, 22910 },
	{ (Il2CppRGCTXDataType)3, 33802 },
	{ (Il2CppRGCTXDataType)3, 33803 },
	{ (Il2CppRGCTXDataType)2, 22911 },
	{ (Il2CppRGCTXDataType)3, 33804 },
	{ (Il2CppRGCTXDataType)3, 33805 },
	{ (Il2CppRGCTXDataType)3, 33806 },
	{ (Il2CppRGCTXDataType)2, 21837 },
	{ (Il2CppRGCTXDataType)3, 33807 },
	{ (Il2CppRGCTXDataType)3, 33808 },
	{ (Il2CppRGCTXDataType)3, 33809 },
	{ (Il2CppRGCTXDataType)3, 33810 },
	{ (Il2CppRGCTXDataType)3, 33811 },
	{ (Il2CppRGCTXDataType)2, 877 },
	{ (Il2CppRGCTXDataType)3, 33812 },
	{ (Il2CppRGCTXDataType)3, 33813 },
	{ (Il2CppRGCTXDataType)3, 33814 },
	{ (Il2CppRGCTXDataType)3, 33815 },
	{ (Il2CppRGCTXDataType)3, 33816 },
	{ (Il2CppRGCTXDataType)3, 33817 },
	{ (Il2CppRGCTXDataType)3, 33818 },
	{ (Il2CppRGCTXDataType)3, 33819 },
	{ (Il2CppRGCTXDataType)3, 33820 },
	{ (Il2CppRGCTXDataType)3, 33821 },
	{ (Il2CppRGCTXDataType)2, 886 },
	{ (Il2CppRGCTXDataType)3, 33822 },
	{ (Il2CppRGCTXDataType)3, 33823 },
	{ (Il2CppRGCTXDataType)3, 33824 },
	{ (Il2CppRGCTXDataType)3, 33825 },
	{ (Il2CppRGCTXDataType)3, 33826 },
	{ (Il2CppRGCTXDataType)3, 33827 },
	{ (Il2CppRGCTXDataType)2, 22912 },
	{ (Il2CppRGCTXDataType)3, 33828 },
	{ (Il2CppRGCTXDataType)3, 33829 },
	{ (Il2CppRGCTXDataType)2, 22913 },
	{ (Il2CppRGCTXDataType)3, 33830 },
	{ (Il2CppRGCTXDataType)3, 33831 },
	{ (Il2CppRGCTXDataType)3, 33832 },
	{ (Il2CppRGCTXDataType)3, 33833 },
	{ (Il2CppRGCTXDataType)2, 21914 },
	{ (Il2CppRGCTXDataType)3, 33834 },
	{ (Il2CppRGCTXDataType)3, 33835 },
	{ (Il2CppRGCTXDataType)3, 33836 },
	{ (Il2CppRGCTXDataType)3, 33837 },
	{ (Il2CppRGCTXDataType)3, 33838 },
	{ (Il2CppRGCTXDataType)3, 33839 },
	{ (Il2CppRGCTXDataType)2, 22914 },
	{ (Il2CppRGCTXDataType)2, 22915 },
	{ (Il2CppRGCTXDataType)3, 33840 },
	{ (Il2CppRGCTXDataType)2, 22916 },
	{ (Il2CppRGCTXDataType)3, 33841 },
	{ (Il2CppRGCTXDataType)2, 903 },
	{ (Il2CppRGCTXDataType)3, 33842 },
	{ (Il2CppRGCTXDataType)3, 33843 },
	{ (Il2CppRGCTXDataType)3, 33844 },
	{ (Il2CppRGCTXDataType)3, 33845 },
	{ (Il2CppRGCTXDataType)3, 33846 },
	{ (Il2CppRGCTXDataType)3, 33847 },
	{ (Il2CppRGCTXDataType)3, 33848 },
	{ (Il2CppRGCTXDataType)3, 33849 },
	{ (Il2CppRGCTXDataType)3, 33850 },
	{ (Il2CppRGCTXDataType)3, 33851 },
	{ (Il2CppRGCTXDataType)2, 22917 },
	{ (Il2CppRGCTXDataType)2, 907 },
	{ (Il2CppRGCTXDataType)3, 33852 },
	{ (Il2CppRGCTXDataType)3, 33853 },
	{ (Il2CppRGCTXDataType)3, 33854 },
	{ (Il2CppRGCTXDataType)3, 33855 },
	{ (Il2CppRGCTXDataType)2, 906 },
	{ (Il2CppRGCTXDataType)3, 33856 },
	{ (Il2CppRGCTXDataType)3, 33857 },
	{ (Il2CppRGCTXDataType)3, 33858 },
	{ (Il2CppRGCTXDataType)3, 33859 },
	{ (Il2CppRGCTXDataType)3, 33860 },
	{ (Il2CppRGCTXDataType)3, 33861 },
	{ (Il2CppRGCTXDataType)2, 913 },
	{ (Il2CppRGCTXDataType)2, 920 },
	{ (Il2CppRGCTXDataType)3, 33862 },
	{ (Il2CppRGCTXDataType)3, 33863 },
	{ (Il2CppRGCTXDataType)2, 22918 },
	{ (Il2CppRGCTXDataType)3, 33864 },
	{ (Il2CppRGCTXDataType)3, 33865 },
	{ (Il2CppRGCTXDataType)3, 33866 },
	{ (Il2CppRGCTXDataType)3, 33867 },
	{ (Il2CppRGCTXDataType)3, 33868 },
	{ (Il2CppRGCTXDataType)2, 22252 },
	{ (Il2CppRGCTXDataType)3, 33869 },
	{ (Il2CppRGCTXDataType)3, 33870 },
	{ (Il2CppRGCTXDataType)3, 33871 },
	{ (Il2CppRGCTXDataType)3, 33872 },
	{ (Il2CppRGCTXDataType)3, 33873 },
	{ (Il2CppRGCTXDataType)3, 33874 },
	{ (Il2CppRGCTXDataType)3, 33875 },
	{ (Il2CppRGCTXDataType)2, 22920 },
	{ (Il2CppRGCTXDataType)3, 33876 },
	{ (Il2CppRGCTXDataType)3, 33877 },
	{ (Il2CppRGCTXDataType)2, 22921 },
	{ (Il2CppRGCTXDataType)3, 33878 },
	{ (Il2CppRGCTXDataType)2, 22921 },
	{ (Il2CppRGCTXDataType)3, 33879 },
	{ (Il2CppRGCTXDataType)3, 31870 },
	{ (Il2CppRGCTXDataType)3, 33880 },
	{ (Il2CppRGCTXDataType)2, 22262 },
	{ (Il2CppRGCTXDataType)3, 33881 },
	{ (Il2CppRGCTXDataType)2, 22276 },
	{ (Il2CppRGCTXDataType)2, 22922 },
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationAssemblyU2DCSharp;
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	2128,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	26,
	s_rgctxIndices,
	162,
	s_rgctxValues,
	&g_DebuggerMetadataRegistrationAssemblyU2DCSharp,
};
