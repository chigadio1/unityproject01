﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void DelegateList`1::.ctor(System.Func`2<System.Action`1<T>,System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>,System.Action`1<System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>)
// 0x00000002 System.Int32 DelegateList`1::get_Count()
// 0x00000003 System.Void DelegateList`1::Add(System.Action`1<T>)
// 0x00000004 System.Void DelegateList`1::Remove(System.Action`1<T>)
// 0x00000005 System.Void DelegateList`1::Invoke(T)
// 0x00000006 System.Void DelegateList`1::Clear()
// 0x00000007 DelegateList`1<T> DelegateList`1::CreateWithGlobalCache()
// 0x00000008 System.Void ListWithEvents`1::add_OnElementAdded(System.Action`1<T>)
// 0x00000009 System.Void ListWithEvents`1::remove_OnElementAdded(System.Action`1<T>)
// 0x0000000A System.Void ListWithEvents`1::add_OnElementRemoved(System.Action`1<T>)
// 0x0000000B System.Void ListWithEvents`1::remove_OnElementRemoved(System.Action`1<T>)
// 0x0000000C System.Void ListWithEvents`1::InvokeAdded(T)
// 0x0000000D System.Void ListWithEvents`1::InvokeRemoved(T)
// 0x0000000E T ListWithEvents`1::get_Item(System.Int32)
// 0x0000000F System.Void ListWithEvents`1::set_Item(System.Int32,T)
// 0x00000010 System.Int32 ListWithEvents`1::get_Count()
// 0x00000011 System.Boolean ListWithEvents`1::get_IsReadOnly()
// 0x00000012 System.Void ListWithEvents`1::Add(T)
// 0x00000013 System.Void ListWithEvents`1::Clear()
// 0x00000014 System.Boolean ListWithEvents`1::Contains(T)
// 0x00000015 System.Void ListWithEvents`1::CopyTo(T[],System.Int32)
// 0x00000016 System.Collections.Generic.IEnumerator`1<T> ListWithEvents`1::GetEnumerator()
// 0x00000017 System.Int32 ListWithEvents`1::IndexOf(T)
// 0x00000018 System.Void ListWithEvents`1::Insert(System.Int32,T)
// 0x00000019 System.Boolean ListWithEvents`1::Remove(T)
// 0x0000001A System.Void ListWithEvents`1::RemoveAt(System.Int32)
// 0x0000001B System.Collections.IEnumerator ListWithEvents`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001C System.Void ListWithEvents`1::.ctor()
// 0x0000001D System.Void MonoBehaviourCallbackHooks::add_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_add_OnUpdateDelegate_m71F16EDF13A73B69505988BF80F3BD3C15D657B6 (void);
// 0x0000001E System.Void MonoBehaviourCallbackHooks::remove_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m5A983C728E2EECA2E549DD44203A30BC9EA4488F (void);
// 0x0000001F System.Void MonoBehaviourCallbackHooks::Awake()
extern void MonoBehaviourCallbackHooks_Awake_m380AFA73E5C2B03FB2DC04F469CEC765362F3AE1 (void);
// 0x00000020 System.Void MonoBehaviourCallbackHooks::Update()
extern void MonoBehaviourCallbackHooks_Update_mF0F182D68DFD4098DEB677350B6C87935C23D9E2 (void);
// 0x00000021 System.Void MonoBehaviourCallbackHooks::.ctor()
extern void MonoBehaviourCallbackHooks__ctor_m6DA4F0AF02FEBFF30AA23A97945582D2884EB1C1 (void);
// 0x00000022 System.Void UnityEngine.ResourceManagement.ChainOperation`2::.ctor()
// 0x00000023 System.String UnityEngine.ResourceManagement.ChainOperation`2::get_DebugName()
// 0x00000024 System.Void UnityEngine.ResourceManagement.ChainOperation`2::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000025 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000026 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Execute()
// 0x00000027 System.Void UnityEngine.ResourceManagement.ChainOperation`2::OnWrappedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000028 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Destroy()
// 0x00000029 System.Single UnityEngine.ResourceManagement.ChainOperation`2::get_Progress()
// 0x0000002A System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::.ctor()
// 0x0000002B System.String UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_DebugName()
// 0x0000002C System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000002D System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x0000002E System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Execute()
// 0x0000002F System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::OnWrappedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000030 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Destroy()
// 0x00000031 System.Single UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_Progress()
// 0x00000032 System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception> UnityEngine.ResourceManagement.ResourceManager::get_ExceptionHandler()
extern void ResourceManager_get_ExceptionHandler_m7AA5E78ACAE04A2406EBA923EE0CBC2BFE39EB01 (void);
// 0x00000033 System.Void UnityEngine.ResourceManagement.ResourceManager::set_ExceptionHandler(System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception>)
extern void ResourceManager_set_ExceptionHandler_mA6F5EBE9E65F68C8A8610638985E34304FE15BA7 (void);
// 0x00000034 System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.ResourceManagement.ResourceManager::get_InternalIdTransformFunc()
extern void ResourceManager_get_InternalIdTransformFunc_mE09C9ABCDE9E35C821FFBEEF42FCE9FDA8FEB308 (void);
// 0x00000035 System.Void UnityEngine.ResourceManagement.ResourceManager::set_InternalIdTransformFunc(System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>)
extern void ResourceManager_set_InternalIdTransformFunc_m0287E07471C3786B84A9A320065C8E2B782E31C1 (void);
// 0x00000036 System.String UnityEngine.ResourceManagement.ResourceManager::TransformInternalId(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_TransformInternalId_m99309A473F0354A9A003C8269967FB49300DC06C (void);
// 0x00000037 System.Int32 UnityEngine.ResourceManagement.ResourceManager::get_OperationCacheCount()
extern void ResourceManager_get_OperationCacheCount_m088CAEA949AC929E003D39339F9A6CD77EEC9253 (void);
// 0x00000038 System.Int32 UnityEngine.ResourceManagement.ResourceManager::get_InstanceOperationCount()
extern void ResourceManager_get_InstanceOperationCount_m2A10D47B814899A0C7E5880E37DA48DE883D3ECC (void);
// 0x00000039 System.Void UnityEngine.ResourceManagement.ResourceManager::AddUpdateReceiver(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_AddUpdateReceiver_m8E85F81F33E25C178B60C465322AADE11C03AA1F (void);
// 0x0000003A System.Void UnityEngine.ResourceManagement.ResourceManager::RemoveUpdateReciever(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_RemoveUpdateReciever_mE9CC3445844CC1E22DDD4815184FD27412E07925 (void);
// 0x0000003B UnityEngine.ResourceManagement.Util.IAllocationStrategy UnityEngine.ResourceManagement.ResourceManager::get_Allocator()
extern void ResourceManager_get_Allocator_mEEC5A006F0FDDB3771E377F1A40C9AE319B5A474 (void);
// 0x0000003C System.Void UnityEngine.ResourceManagement.ResourceManager::set_Allocator(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void ResourceManager_set_Allocator_m5B678A15912CC5908DB81FFA8F49C625E7E14377 (void);
// 0x0000003D System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider> UnityEngine.ResourceManagement.ResourceManager::get_ResourceProviders()
extern void ResourceManager_get_ResourceProviders_m7B8444862B525921DCA90FFF0F89BAE11825677A (void);
// 0x0000003E UnityEngine.Networking.CertificateHandler UnityEngine.ResourceManagement.ResourceManager::get_CertificateHandlerInstance()
extern void ResourceManager_get_CertificateHandlerInstance_m12F938F5FEF5B4853F73BBF21EEE51C73716F9B9 (void);
// 0x0000003F System.Void UnityEngine.ResourceManagement.ResourceManager::set_CertificateHandlerInstance(UnityEngine.Networking.CertificateHandler)
extern void ResourceManager_set_CertificateHandlerInstance_m0DED0F9FE32208F8C3F3A995E93DE2AC7CFF2F9D (void);
// 0x00000040 System.Void UnityEngine.ResourceManagement.ResourceManager::.ctor(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void ResourceManager__ctor_m528366A74390F0F37D4F8D327B311E131484895F (void);
// 0x00000041 System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectAdded(System.Object)
extern void ResourceManager_OnObjectAdded_m0A54698AC9BA86E496F1E3BA0DFF1A8DDB00600E (void);
// 0x00000042 System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectRemoved(System.Object)
extern void ResourceManager_OnObjectRemoved_mFB8B23D34D88A56665DD72B6487456D7B0F73294 (void);
// 0x00000043 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForCallbacks()
extern void ResourceManager_RegisterForCallbacks_mDFD4D66966A06B27431E6CFA21C62653613C5395 (void);
// 0x00000044 System.Void UnityEngine.ResourceManagement.ResourceManager::ClearDiagnosticsCallback()
extern void ResourceManager_ClearDiagnosticsCallback_m21D48E1CD446FADEB667D05256F3B165FCC6B97C (void);
// 0x00000045 System.Void UnityEngine.ResourceManagement.ResourceManager::ClearDiagnosticCallbacks()
extern void ResourceManager_ClearDiagnosticCallbacks_mC2ACDDB95C7291580AB454B5FEA5D76F5C953A5F (void);
// 0x00000046 System.Void UnityEngine.ResourceManagement.ResourceManager::UnregisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext>)
extern void ResourceManager_UnregisterDiagnosticCallback_m94060E96C7D51B594484713FB4DE87A9B0A13454 (void);
// 0x00000047 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterDiagnosticCallback(System.Action`4<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventType,System.Int32,System.Object>)
extern void ResourceManager_RegisterDiagnosticCallback_m80A28924C61813B6E8E547E85B6F009514BD6F29 (void);
// 0x00000048 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext>)
extern void ResourceManager_RegisterDiagnosticCallback_m7C36E880E0E8C7924E0B74A6769D3BFBD76BD65C (void);
// 0x00000049 System.Void UnityEngine.ResourceManagement.ResourceManager::PostDiagnosticEvent(UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext)
extern void ResourceManager_PostDiagnosticEvent_m774EFC2B6BC9364BCF3B242FD3115BD3F94EA6BE (void);
// 0x0000004A UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider UnityEngine.ResourceManagement.ResourceManager::GetResourceProvider(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetResourceProvider_m38873363AA6A66617312E8F2ECE4277BA17B24A3 (void);
// 0x0000004B System.Type UnityEngine.ResourceManagement.ResourceManager::GetDefaultTypeForLocation(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetDefaultTypeForLocation_mEB119E0ACD2ECD658112DCE606E3BD8B74B8C3A2 (void);
// 0x0000004C System.Int32 UnityEngine.ResourceManagement.ResourceManager::CalculateLocationsHash(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Type)
extern void ResourceManager_CalculateLocationsHash_m9BE52AD42B5E6FB2082A455391477A87AF4A18AE (void);
// 0x0000004D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Type)
extern void ResourceManager_ProvideResource_m594F8B2A6122F7744DBCCE7316450F302D6AB7C2 (void);
// 0x0000004E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x0000004F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000050 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_StartOperation_mF350C51B1D49B0A1900C7B955595E50A560DE47A (void);
// 0x00000051 System.Void UnityEngine.ResourceManagement.ResourceManager::OnInstanceOperationDestroy(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnInstanceOperationDestroy_m0519061477A75DB2BF0F3F8A18CB34F25D908E9F (void);
// 0x00000052 System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyNonCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyNonCached_m98C9B9996EE251C99419A7C6D4AC674C41B487E7 (void);
// 0x00000053 System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyCached_mB4AFABDBC148F73495D45930E03AE05055ACABCC (void);
// 0x00000054 T UnityEngine.ResourceManagement.ResourceManager::CreateOperation(System.Type,System.Int32,System.Int32,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x00000055 System.Void UnityEngine.ResourceManagement.ResourceManager::AddOperationToCache(System.Int32,UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_AddOperationToCache_mF821E49A9CAE2772E817ECB95460480AC32AFC23 (void);
// 0x00000056 System.Boolean UnityEngine.ResourceManagement.ResourceManager::RemoveOperationFromCache(System.Int32)
extern void ResourceManager_RemoveOperationFromCache_m42791732D849ED8CB5B858AF11CA1A0D3ABA6590 (void);
// 0x00000057 System.Boolean UnityEngine.ResourceManagement.ResourceManager::IsOperationCached(System.Int32)
extern void ResourceManager_IsOperationCached_m83330F9DD03DE37659319CC3EE6B607B49EEBE63 (void);
// 0x00000058 System.Int32 UnityEngine.ResourceManagement.ResourceManager::CachedOperationCount()
extern void ResourceManager_CachedOperationCount_mA1EB8E0A0D61A773132C5BE029CAFC7A0C67C19A (void);
// 0x00000059 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperation(TObject,System.String)
// 0x0000005A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperation(TObject,System.Boolean,System.String)
// 0x0000005B System.Void UnityEngine.ResourceManagement.ResourceManager::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_Release_mD67A0632D3D7D0B7F97841BC1E4C7CF2FC191C57 (void);
// 0x0000005C System.Void UnityEngine.ResourceManagement.ResourceManager::Acquire(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_Acquire_m88572CDE3229742F390437973CDAFEC0C4B1074C (void);
// 0x0000005D UnityEngine.ResourceManagement.AsyncOperations.GroupOperation UnityEngine.ResourceManagement.ResourceManager::AcquireGroupOpFromCache(System.Int32)
extern void ResourceManager_AcquireGroupOpFromCache_m5782A7CFB719A51E06134911422E3C5F9567B355 (void);
// 0x0000005E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGroupOperation(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
// 0x0000005F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGenericGroupOperation(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
extern void ResourceManager_CreateGenericGroupOperation_m148289B454ABDC6D18F9BA386EFEE2C4FFA640F4 (void);
// 0x00000060 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::ProvideResourceGroupCached(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Int32,System.Type,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void ResourceManager_ProvideResourceGroupCached_mD1115A6CB5B547E6767CE7DC3289BB2C64D9E545 (void);
// 0x00000061 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager::ProvideResources(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>)
// 0x00000062 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000063 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000064 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceManager::ProvideScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void ResourceManager_ProvideScene_m003D41F2CBA62B7D1A53CED8E312CB9E5DC1F055 (void);
// 0x00000065 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceManager::ReleaseScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void ResourceManager_ReleaseScene_mBD5161D48EFD7ACAD02CB9A12378CAE0EA8A828E (void);
// 0x00000066 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.ResourceManagement.ResourceManager::ProvideInstance(UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
extern void ResourceManager_ProvideInstance_mAE65646633A09A4E51654766498DAA9D9EE3C951 (void);
// 0x00000067 System.Void UnityEngine.ResourceManagement.ResourceManager::CleanupSceneInstances(UnityEngine.SceneManagement.Scene)
extern void ResourceManager_CleanupSceneInstances_m2BBE6BCBFBA7DC2CEA5D38DE680FA6A52B0EA528 (void);
// 0x00000068 System.Void UnityEngine.ResourceManagement.ResourceManager::ExecuteDeferredCallbacks()
extern void ResourceManager_ExecuteDeferredCallbacks_m75C200EC542CDBCEB07DBD4720835323A34D144F (void);
// 0x00000069 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForDeferredCallback(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Boolean)
extern void ResourceManager_RegisterForDeferredCallback_m0436E85EDF6A341FEE22C1C7A360EC0643042272 (void);
// 0x0000006A System.Void UnityEngine.ResourceManagement.ResourceManager::Update(System.Single)
extern void ResourceManager_Update_m1038C44C2B864801F1D40DC503AA7F1029AAAF70 (void);
// 0x0000006B System.Void UnityEngine.ResourceManagement.ResourceManager::Dispose()
extern void ResourceManager_Dispose_m90EE4D872BF46DE7D69ED70BD540A2B46064D0B8 (void);
// 0x0000006C System.Void UnityEngine.ResourceManagement.ResourceManager::.cctor()
extern void ResourceManager__cctor_m9E01A63275781A7065E780992DFD1A0269EE76CB (void);
// 0x0000006D System.Void UnityEngine.ResourceManagement.ResourceManager::<.ctor>b__45_0(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_U3C_ctorU3Eb__45_0_mCECD009288795FC98DBCE498BA37927C1AD49273 (void);
// 0x0000006E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_OperationHandle()
extern void DiagnosticEventContext_get_OperationHandle_m6884404709298E7C22B1F7F8205C1B7E8796EE05_AdjustorThunk (void);
// 0x0000006F UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventType UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Type()
extern void DiagnosticEventContext_get_Type_mC90B555707F6837DA6A22722EF4353065334C905_AdjustorThunk (void);
// 0x00000070 System.Int32 UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_EventValue()
extern void DiagnosticEventContext_get_EventValue_mCD0F3458A7BCBE06550729AC816B34E5E19E14EF_AdjustorThunk (void);
// 0x00000071 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Location()
extern void DiagnosticEventContext_get_Location_mF350B540A75C4C08BA750678E3827EADBE2FDF75_AdjustorThunk (void);
// 0x00000072 System.Object UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Context()
extern void DiagnosticEventContext_get_Context_m4B8F30E7A090EB33FE0C1F8720C40273B3859C04_AdjustorThunk (void);
// 0x00000073 System.String UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Error()
extern void DiagnosticEventContext_get_Error_m1CF5543DF39540144965910BC6FFF71862753E60_AdjustorThunk (void);
// 0x00000074 System.Void UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventType,System.Int32,System.String,System.Object)
extern void DiagnosticEventContext__ctor_mC21627E8722C2E92B119E731AA890319A131FECA_AdjustorThunk (void);
// 0x00000075 System.Void UnityEngine.ResourceManagement.ResourceManager_CompletedOperation`1::.ctor()
// 0x00000076 System.Void UnityEngine.ResourceManagement.ResourceManager_CompletedOperation`1::Init(TObject,System.Boolean,System.String)
// 0x00000077 System.Void UnityEngine.ResourceManagement.ResourceManager_CompletedOperation`1::Execute()
// 0x00000078 System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void InstanceOperation_Init_mFEA262E8A7D70C9FFADB5F4076C5B9C7341026C9 (void);
// 0x00000079 System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void InstanceOperation_GetDependencies_m76864181C3947B5A9C885BC53983F10B45D29741 (void);
// 0x0000007A System.String UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::get_DebugName()
extern void InstanceOperation_get_DebugName_m2163DD9A80CE393626ECC58E61AE24BB697DD053 (void);
// 0x0000007B UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::InstanceScene()
extern void InstanceOperation_InstanceScene_mAC44625C564DE76E956CA8BCF4EC2C9F6178D9DE (void);
// 0x0000007C System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::Destroy()
extern void InstanceOperation_Destroy_mE795720CCC23566F64105C160595FE0D634D57FA (void);
// 0x0000007D System.Single UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::get_Progress()
extern void InstanceOperation_get_Progress_mED88CD3EA094C0A946CF1B31074C9C8D3D3E9268 (void);
// 0x0000007E System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::Execute()
extern void InstanceOperation_Execute_m8792B88CB3F74349E1FD1D35E9426D58894F19D3 (void);
// 0x0000007F System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::.ctor()
extern void InstanceOperation__ctor_m491135C07279DE258B3D9EE81BC28527EEF1BA21 (void);
// 0x00000080 System.Void UnityEngine.ResourceManagement.ResourceManager_<>c__DisplayClass80_0`1::.ctor()
// 0x00000081 System.Void UnityEngine.ResourceManagement.ResourceManager_<>c__DisplayClass80_0`1::<ProvideResources>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000082 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager_<>c__DisplayClass80_0`1::<ProvideResources>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x00000083 System.Void UnityEngine.ResourceManagement.IUpdateReceiver::Update(System.Single)
// 0x00000084 System.Boolean UnityEngine.ResourceManagement.WebRequestQueueOperation::get_IsDone()
extern void WebRequestQueueOperation_get_IsDone_mD60F4EB09496B7A2F86C696815E34491375CC9D0 (void);
// 0x00000085 System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueueOperation__ctor_m68CB315ACE251B662B8A4A603751432C8048F38A (void);
// 0x00000086 System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::Complete(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void WebRequestQueueOperation_Complete_m045307756A8EB68F5ADC3E6FF182361C60AABBF8 (void);
// 0x00000087 UnityEngine.ResourceManagement.WebRequestQueueOperation UnityEngine.ResourceManagement.WebRequestQueue::QueueRequest(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueue_QueueRequest_m9E882E5334EC6CB6FB517C1A0DF985B83779B78F (void);
// 0x00000088 System.Void UnityEngine.ResourceManagement.WebRequestQueue::OnWebAsyncOpComplete(UnityEngine.AsyncOperation)
extern void WebRequestQueue_OnWebAsyncOpComplete_mF0B3325CA9B1386C3DAB80E1D87C4C5DB16F624C (void);
// 0x00000089 System.Void UnityEngine.ResourceManagement.WebRequestQueue::.cctor()
extern void WebRequestQueue__cctor_m387136624EFB374B41B0EEE1A63506F97F28D9B6 (void);
// 0x0000008A System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor()
extern void ResourceManagerException__ctor_mAEDC8C69DA13800EC322613C6829018DBB1E07CB (void);
// 0x0000008B System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.String)
extern void ResourceManagerException__ctor_mDBACFBD3B06C3A8F3436E8A78F8B10E6ED6759A1 (void);
// 0x0000008C System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.String,System.Exception)
extern void ResourceManagerException__ctor_mA9EAADAF71BABEB9D6EB3063410E4245186ADD3B (void);
// 0x0000008D System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ResourceManagerException__ctor_mEDDA961AA897100A189BB8E88B3FD87FBA614544 (void);
// 0x0000008E UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Location()
extern void UnknownResourceProviderException_get_Location_mB274BAF53E19FE874A24CC67C522D494CCC8F4F0 (void);
// 0x0000008F System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::set_Location(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException_set_Location_mF93EB4AAA41E957EFFACCEA94DFF45AEC67EB619 (void);
// 0x00000090 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException__ctor_mB1E23AB4989DE1993682D165DD042F52E042AAE8 (void);
// 0x00000091 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor()
extern void UnknownResourceProviderException__ctor_mCB059067E6AF43FD4715E52CDA42C6D95901D796 (void);
// 0x00000092 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.String)
extern void UnknownResourceProviderException__ctor_mB8D96934D5DEE32F14F4AE30438961E70B361EE7 (void);
// 0x00000093 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.String,System.Exception)
extern void UnknownResourceProviderException__ctor_m9D021A7F0FE0B700DD55ED1060274872AA899098 (void);
// 0x00000094 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UnknownResourceProviderException__ctor_m70991EE39B986E427F59C87D1D52DB7885C03546 (void);
// 0x00000095 System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Message()
extern void UnknownResourceProviderException_get_Message_mC59322E40728F5119EB36A38F38BA109AFA55E28 (void);
// 0x00000096 System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::ToString()
extern void UnknownResourceProviderException_ToString_mE320AC807B6D64A5AAD350EB35DFB6781B571648 (void);
// 0x00000097 System.Collections.Generic.LinkedListNode`1<UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo> UnityEngine.ResourceManagement.Util.DelayedActionManager::GetNode(UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo&)
extern void DelayedActionManager_GetNode_m4B1CA12E533503A5C4D48BB1C80F10F62565E489 (void);
// 0x00000098 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::Clear()
extern void DelayedActionManager_Clear_m74693E78A10AB5620D5A91374EE31AAE959064FB (void);
// 0x00000099 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::DestroyWhenComplete()
extern void DelayedActionManager_DestroyWhenComplete_mB9B1B0A8DE58B2DBCACE37C5846FBFCAA6E18BCA (void);
// 0x0000009A System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::AddAction(System.Delegate,System.Single,System.Object[])
extern void DelayedActionManager_AddAction_m59A84189B25DF74719B2D26B7F50E9AD275F1742 (void);
// 0x0000009B System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::AddActionInternal(System.Delegate,System.Single,System.Object[])
extern void DelayedActionManager_AddActionInternal_m570A8E6E3923163BA9811F5569516807717659E0 (void);
// 0x0000009C System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::Awake()
extern void DelayedActionManager_Awake_m693AC7F17BF8583DBBAED409AB03408F0E02C964 (void);
// 0x0000009D System.Boolean UnityEngine.ResourceManagement.Util.DelayedActionManager::get_IsActive()
extern void DelayedActionManager_get_IsActive_m4E01D61FD7704242F5B12498F318DA7B79C43133 (void);
// 0x0000009E System.Boolean UnityEngine.ResourceManagement.Util.DelayedActionManager::Wait(System.Single,System.Single)
extern void DelayedActionManager_Wait_m42346772DD5598A885F50832E40928B438663245 (void);
// 0x0000009F System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::LateUpdate()
extern void DelayedActionManager_LateUpdate_m5DA30ECF97E705B5C199CB708EE73B39E676EDE7 (void);
// 0x000000A0 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::InternalLateUpdate(System.Single)
extern void DelayedActionManager_InternalLateUpdate_m0BDB5B3466E86462F1F6059ABAF70A9FCD7F7059 (void);
// 0x000000A1 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::OnApplicationQuit()
extern void DelayedActionManager_OnApplicationQuit_m50C8D15F97A47AFFB397A02E762E1328AF784979 (void);
// 0x000000A2 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::.ctor()
extern void DelayedActionManager__ctor_mF1F8365F8BCEE13A73CD17B17927DB087F9F70EF (void);
// 0x000000A3 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo::.ctor(System.Delegate,System.Single,System.Object[])
extern void DelegateInfo__ctor_m5F9781FFA3BB8947EC7DDE4D0E927D6C56AAED56_AdjustorThunk (void);
// 0x000000A4 System.Single UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo::get_InvocationTime()
extern void DelegateInfo_get_InvocationTime_mE58AD92E984581E6CF13DB1BB8E74A9E1993674C_AdjustorThunk (void);
// 0x000000A5 System.String UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo::ToString()
extern void DelegateInfo_ToString_m5D5AD0956A9A4B7FC994E4E37B5A5E0E8831C4E1_AdjustorThunk (void);
// 0x000000A6 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo::Invoke()
extern void DelegateInfo_Invoke_m6E94807D8DD79D90FEC652EA7AB2450CA3A2428B_AdjustorThunk (void);
// 0x000000A7 System.Boolean UnityEngine.ResourceManagement.Util.IInitializableObject::Initialize(System.String,System.String)
// 0x000000A8 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.Util.IInitializableObject::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
// 0x000000A9 System.String UnityEngine.ResourceManagement.Util.IObjectInitializationDataProvider::get_Name()
// 0x000000AA UnityEngine.ResourceManagement.Util.ObjectInitializationData UnityEngine.ResourceManagement.Util.IObjectInitializationDataProvider::CreateObjectInitializationData()
// 0x000000AB System.Object UnityEngine.ResourceManagement.Util.IAllocationStrategy::New(System.Type,System.Int32)
// 0x000000AC System.Void UnityEngine.ResourceManagement.Util.IAllocationStrategy::Release(System.Int32,System.Object)
// 0x000000AD System.Object UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::New(System.Type,System.Int32)
extern void DefaultAllocationStrategy_New_mB78CC5AFBF1B07035814191AB926E4ACBD41A6CE (void);
// 0x000000AE System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::Release(System.Int32,System.Object)
extern void DefaultAllocationStrategy_Release_mD958EA6925889742484435B1CD1AAF4087F60E4A (void);
// 0x000000AF System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::.ctor()
extern void DefaultAllocationStrategy__ctor_mC395409A503C0199396C35AA3B939CE682AB3C8A (void);
// 0x000000B0 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void LRUCacheAllocationStrategy__ctor_m1DE9B78742C8D5457040C65AFA15854983E33FC6 (void);
// 0x000000B1 System.Collections.Generic.List`1<System.Object> UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::GetPool()
extern void LRUCacheAllocationStrategy_GetPool_mFC7368E7F5D84E6CFDCC693520C2B2B6965627A8 (void);
// 0x000000B2 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::ReleasePool(System.Collections.Generic.List`1<System.Object>)
extern void LRUCacheAllocationStrategy_ReleasePool_m72D0EB4F039EE69BDB8BBB68AFB8BA7D62B92BCF (void);
// 0x000000B3 System.Object UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::New(System.Type,System.Int32)
extern void LRUCacheAllocationStrategy_New_m9A93EA16BC5C9C0F1933D6A19C8C19F854F9D2E3 (void);
// 0x000000B4 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::Release(System.Int32,System.Object)
extern void LRUCacheAllocationStrategy_Release_m42CAEAF3890309E4396717B9CE28D17082BA6300 (void);
// 0x000000B5 System.Void UnityEngine.ResourceManagement.Util.SerializedTypeRestrictionAttribute::.ctor()
extern void SerializedTypeRestrictionAttribute__ctor_m73D9657C618F99685B17D67B3C0E10CE9C16007A (void);
// 0x000000B6 System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Acquire(T)
// 0x000000B7 System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x000000B8 System.Int32 UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::get_CreatedNodeCount()
// 0x000000B9 System.Int32 UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::get_CachedNodeCount()
// 0x000000BA System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::.ctor()
// 0x000000BB System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Acquire(T)
// 0x000000BC System.Void UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x000000BD System.String UnityEngine.ResourceManagement.Util.SerializedType::get_AssemblyName()
extern void SerializedType_get_AssemblyName_m26AAB5BD9E0D42C3748D63FFFF829035428DF953_AdjustorThunk (void);
// 0x000000BE System.String UnityEngine.ResourceManagement.Util.SerializedType::get_ClassName()
extern void SerializedType_get_ClassName_mB9E5EB03E83676EECFA405A1A1369B87D9B69C61_AdjustorThunk (void);
// 0x000000BF System.String UnityEngine.ResourceManagement.Util.SerializedType::ToString()
extern void SerializedType_ToString_m1148F8BA12B705068191BD22F924E4570E0CF2BF_AdjustorThunk (void);
// 0x000000C0 System.Type UnityEngine.ResourceManagement.Util.SerializedType::get_Value()
extern void SerializedType_get_Value_m1BC4883C0CA3F911AC298F99BA3820D28B116AE4_AdjustorThunk (void);
// 0x000000C1 System.Void UnityEngine.ResourceManagement.Util.SerializedType::set_Value(System.Type)
extern void SerializedType_set_Value_m926E07D1B3ADDEC9AC75DC7EABCDF9C1D8B9FCFF_AdjustorThunk (void);
// 0x000000C2 System.Boolean UnityEngine.ResourceManagement.Util.SerializedType::get_ValueChanged()
extern void SerializedType_get_ValueChanged_m3B4DCF3F7B8B10EBCBB1CC9F51BDF9B90810022E_AdjustorThunk (void);
// 0x000000C3 System.Void UnityEngine.ResourceManagement.Util.SerializedType::set_ValueChanged(System.Boolean)
extern void SerializedType_set_ValueChanged_m237555FB2018F1106F72F91325662ED6779CCFF2_AdjustorThunk (void);
// 0x000000C4 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_Id()
extern void ObjectInitializationData_get_Id_mEC5373B1DDFE78FB72F291806ED9C869DA2B27B5_AdjustorThunk (void);
// 0x000000C5 UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_ObjectType()
extern void ObjectInitializationData_get_ObjectType_m14F9142B5C61D1F8285BA84C34B2747EBDE59DD0_AdjustorThunk (void);
// 0x000000C6 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_Data()
extern void ObjectInitializationData_get_Data_m0A9B73EED42A63E5DAF79EFD978AC64E38B23D71_AdjustorThunk (void);
// 0x000000C7 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::ToString()
extern void ObjectInitializationData_ToString_mA1CB0E03EDC5E862A576AC96DF6E768E018BE8EF_AdjustorThunk (void);
// 0x000000C8 TObject UnityEngine.ResourceManagement.Util.ObjectInitializationData::CreateInstance(System.String)
// 0x000000C9 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.Util.ObjectInitializationData::GetAsyncInitHandle(UnityEngine.ResourceManagement.ResourceManager,System.String)
extern void ObjectInitializationData_GetAsyncInitHandle_m1A3A5DA8AD774DC20CC561528A6305CC4F5999A6_AdjustorThunk (void);
// 0x000000CA System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ExtractKeyAndSubKey(System.Object,System.String&,System.String&)
extern void ResourceManagerConfig_ExtractKeyAndSubKey_m10E4749CC55275EB25BB3341FF729050BEFCABB0 (void);
// 0x000000CB System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::IsPathRemote(System.String)
extern void ResourceManagerConfig_IsPathRemote_m2626B711D462C076C9028B01395AFB5D905C58CB (void);
// 0x000000CC System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ShouldPathUseWebRequest(System.String)
extern void ResourceManagerConfig_ShouldPathUseWebRequest_m20ED59B08CD29D311DE947B60B3020B6447EA19A (void);
// 0x000000CD System.Array UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateArrayResult(System.Type,UnityEngine.Object[])
extern void ResourceManagerConfig_CreateArrayResult_mC0878410CBD7950D2E123DD3C8592AFF4CAD0426 (void);
// 0x000000CE TObject UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateArrayResult(UnityEngine.Object[])
// 0x000000CF System.Collections.IList UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateListResult(System.Type,UnityEngine.Object[])
extern void ResourceManagerConfig_CreateListResult_m5A686FF2B8B99AC174723DA1AF7DDAE9D04D5ADF (void);
// 0x000000D0 TObject UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateListResult(UnityEngine.Object[])
// 0x000000D1 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::IsInstance()
// 0x000000D2 UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource::GetAssetBundle()
// 0x000000D3 System.String UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Hash()
extern void AssetBundleRequestOptions_get_Hash_m7E8FFDF35E4F8490371194922EE7EA74D02060EA (void);
// 0x000000D4 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Hash(System.String)
extern void AssetBundleRequestOptions_set_Hash_m7139F049F7A9D7C605C213FA8183B6BBFE20427A (void);
// 0x000000D5 System.UInt32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Crc()
extern void AssetBundleRequestOptions_get_Crc_m5F918FDCC93369AA688F6EAC9FF56D1C901EB9E3 (void);
// 0x000000D6 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Crc(System.UInt32)
extern void AssetBundleRequestOptions_set_Crc_mB615B2A91B6C4EC004B9AD0290916A17CBC2ECF0 (void);
// 0x000000D7 System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Timeout()
extern void AssetBundleRequestOptions_get_Timeout_m0EC9538E2205B9E1313753385847B90A69D6DF6A (void);
// 0x000000D8 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Timeout(System.Int32)
extern void AssetBundleRequestOptions_set_Timeout_m5853BF9CE70EDD668859EF45CDEF31F365B710B5 (void);
// 0x000000D9 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_ChunkedTransfer()
extern void AssetBundleRequestOptions_get_ChunkedTransfer_m7FACD52559F14DEA310A70D42FF4065AE8A63E47 (void);
// 0x000000DA System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_ChunkedTransfer(System.Boolean)
extern void AssetBundleRequestOptions_set_ChunkedTransfer_m3EF05C676B21C63FA0904876A13CABF1EB0DFE3D (void);
// 0x000000DB System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_RedirectLimit()
extern void AssetBundleRequestOptions_get_RedirectLimit_m95B063F1734F63EFA277FAFF37904FB986BD091B (void);
// 0x000000DC System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_RedirectLimit(System.Int32)
extern void AssetBundleRequestOptions_set_RedirectLimit_m73E2D9E93B3EBA1702DA6F982861F71FB0FA0ABE (void);
// 0x000000DD System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_RetryCount()
extern void AssetBundleRequestOptions_get_RetryCount_m7998E1CF424014231B8181583BE8DE1DFFBFF79C (void);
// 0x000000DE System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_RetryCount(System.Int32)
extern void AssetBundleRequestOptions_set_RetryCount_mD353E67BE0A34AE43D8D5BAF8611C02998820358 (void);
// 0x000000DF System.String UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_BundleName()
extern void AssetBundleRequestOptions_get_BundleName_m03D99F2383AD124A7346CFD8BEAA3F803362982D (void);
// 0x000000E0 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_BundleName(System.String)
extern void AssetBundleRequestOptions_set_BundleName_m64C87ED93FAD47B98555EFAA5C2AF50CC425B93E (void);
// 0x000000E1 System.Int64 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_BundleSize()
extern void AssetBundleRequestOptions_get_BundleSize_m447FF9C828884DE530C97D4F8EF98B02C57E1D95 (void);
// 0x000000E2 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_BundleSize(System.Int64)
extern void AssetBundleRequestOptions_set_BundleSize_m3B30445CFA5ABFBBFE1BC62AAB53BCD7AE93A118 (void);
// 0x000000E3 System.Int64 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::ComputeSize(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceManager)
extern void AssetBundleRequestOptions_ComputeSize_m5E3CDC20AE5F1970B47BF96397A239005DD67161 (void);
// 0x000000E4 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::.ctor()
extern void AssetBundleRequestOptions__ctor_m75BC7C2F98DE1E2A506BDB5DFA1FF2E93ABF1284 (void);
// 0x000000E5 UnityEngine.Networking.UnityWebRequest UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::CreateWebRequest(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AssetBundleResource_CreateWebRequest_m6DDCA06801DAD8223CF1A68974B1C85D315E518A (void);
// 0x000000E6 System.Single UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::PercentComplete()
extern void AssetBundleResource_PercentComplete_m379EC2172D7AFCC9E9026847389969EF1AEB0913 (void);
// 0x000000E7 UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::GetAssetBundle()
extern void AssetBundleResource_GetAssetBundle_m7EAF635CD73ECAD7119B84CA933EB8A3D5E64A7E (void);
// 0x000000E8 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AssetBundleResource_Start_m40703643636E0DC5A289132532F961A8106AF675 (void);
// 0x000000E9 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::BeginOperation()
extern void AssetBundleResource_BeginOperation_m069554D609B10EB9691072B6334D4C31F4A14266 (void);
// 0x000000EA System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::LocalRequestOperationCompleted(UnityEngine.AsyncOperation)
extern void AssetBundleResource_LocalRequestOperationCompleted_mF195784E9CBE63D363B00629E44459AABA215477 (void);
// 0x000000EB System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::WebRequestOperationCompleted(UnityEngine.AsyncOperation)
extern void AssetBundleResource_WebRequestOperationCompleted_m8729ED7DC01163D46A6E486AE8690CECF2A7D52A (void);
// 0x000000EC System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::Unload()
extern void AssetBundleResource_Unload_mD88D286E0C628E6C5DFD34B7BCBFD58FDC6D2968 (void);
// 0x000000ED System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::.ctor()
extern void AssetBundleResource__ctor_m9A68288B5047A5A1D36A19E0D19C19EAAB51C591 (void);
// 0x000000EE System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::<BeginOperation>b__11_0(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void AssetBundleResource_U3CBeginOperationU3Eb__11_0_mC51B5EE30BA90EFB788E2397DE3FE99070B81D21 (void);
// 0x000000EF System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AssetBundleProvider_Provide_m97B5CBAF2B5AE8DF7B37591CB1D46C8779EF9200 (void);
// 0x000000F0 System.Type UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AssetBundleProvider_GetDefaultType_m66A9FDF33312277E8EEBE303539D452944901776 (void);
// 0x000000F1 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void AssetBundleProvider_Release_m3291510B7E9B4F159C2E474D59D0134D1E43DAE4 (void);
// 0x000000F2 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::.ctor()
extern void AssetBundleProvider__ctor_m1FC8A928B824E44695F4AF75A2CCD61602320831 (void);
// 0x000000F3 System.Void UnityEngine.ResourceManagement.ResourceProviders.AtlasSpriteProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AtlasSpriteProvider_Provide_m1EEB6E65B16F76C067088BEAF5CD12D9E040BA8E (void);
// 0x000000F4 System.Void UnityEngine.ResourceManagement.ResourceProviders.AtlasSpriteProvider::.ctor()
extern void AtlasSpriteProvider__ctor_m7B78828B1E7EF669E8F189BED2D40FF0D3B73094 (void);
// 0x000000F5 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void BundledAssetProvider_Provide_mC7B58A62D1D9778FC4A5B0EED0DA6D7D2A05649C (void);
// 0x000000F6 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider::.ctor()
extern void BundledAssetProvider__ctor_m5DF36E924DEBBDD1FEB2A32EDFE823478AE410EF (void);
// 0x000000F7 UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::LoadBundleFromDependecies(System.Collections.Generic.IList`1<System.Object>)
extern void InternalOp_LoadBundleFromDependecies_m46D5AC51CD34C01580FA458D0AFA12152A23DD5E (void);
// 0x000000F8 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void InternalOp_Start_m919D6A046CAD232EFEA23C9884033249FC9B35C8 (void);
// 0x000000F9 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::ActionComplete(UnityEngine.AsyncOperation)
extern void InternalOp_ActionComplete_m993FB0E901E8937DF0DA612852DF050C610350AF (void);
// 0x000000FA System.Single UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::ProgressCallback()
extern void InternalOp_ProgressCallback_mCC6E27B16074B9DE33ACC4676ED69F9B7EA3C784 (void);
// 0x000000FB System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::.ctor()
extern void InternalOp__ctor_m9842FA729C3DF56A9E53B8F9C13D90E1B964A30C (void);
// 0x000000FC UnityEngine.Vector3 UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Position()
extern void InstantiationParameters_get_Position_m513B350F34651CD2D5B865E9A67F3DF9299C7993_AdjustorThunk (void);
// 0x000000FD UnityEngine.Quaternion UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Rotation()
extern void InstantiationParameters_get_Rotation_mD3BCB21E3275C5BC81E27F94F0B5BD0CEC5AA2D0_AdjustorThunk (void);
// 0x000000FE UnityEngine.Transform UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Parent()
extern void InstantiationParameters_get_Parent_mCEF42A2F091CDD7B7C8ECAD9FE9EA2E843515893_AdjustorThunk (void);
// 0x000000FF System.Boolean UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_InstantiateInWorldPosition()
extern void InstantiationParameters_get_InstantiateInWorldPosition_m2F7E212FDFB4213E04066CF078A97AAA40D7BFF5_AdjustorThunk (void);
// 0x00000100 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_SetPositionRotation()
extern void InstantiationParameters_get_SetPositionRotation_m1A867DF4E1EAC95D9DFC3919CBC2E56590046EF3_AdjustorThunk (void);
// 0x00000101 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::.ctor(UnityEngine.Transform,System.Boolean)
extern void InstantiationParameters__ctor_mA7C5DA2011D4832617164C7B4DB467140A93014F_AdjustorThunk (void);
// 0x00000102 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void InstantiationParameters__ctor_m5A71887F734D777681811274B19E2327CB487A01_AdjustorThunk (void);
// 0x00000103 TObject UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::Instantiate(TObject)
// 0x00000104 UnityEngine.GameObject UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider::ProvideInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
// 0x00000105 System.Void UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider::ReleaseInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.GameObject)
// 0x00000106 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::.ctor(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation)
extern void ProvideHandle__ctor_m6162616DB274E0DF650DB36B4ECD1AB8FF7FF0AA_AdjustorThunk (void);
// 0x00000107 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_InternalOp()
extern void ProvideHandle_get_InternalOp_mBAFE92C7761F1994A9AC5542BC7085AE67C57051_AdjustorThunk (void);
// 0x00000108 UnityEngine.ResourceManagement.ResourceManager UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_ResourceManager()
extern void ProvideHandle_get_ResourceManager_m05D4E79DEBFDD6BD466CD6A2219958B00C6C3E8E_AdjustorThunk (void);
// 0x00000109 System.Type UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Type()
extern void ProvideHandle_get_Type_m9569FAA40332A3EFD53227FABC324CC03B4BAD3D_AdjustorThunk (void);
// 0x0000010A UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Location()
extern void ProvideHandle_get_Location_m5F7540397789E7214F8B9DD88C8E9B8A7A6ABE53_AdjustorThunk (void);
// 0x0000010B System.Int32 UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_DependencyCount()
extern void ProvideHandle_get_DependencyCount_m2E1FFD8268C87179D01E7E188CEEFF34B6D87DA9_AdjustorThunk (void);
// 0x0000010C TDepObject UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependency(System.Int32)
// 0x0000010D System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
extern void ProvideHandle_GetDependencies_m50B8C3466AED683B0F8581207B09D887350CB741_AdjustorThunk (void);
// 0x0000010E System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::SetProgressCallback(System.Func`1<System.Single>)
extern void ProvideHandle_SetProgressCallback_mCADBA6D92915BE6956E55E25A84F9A5E61BD289F_AdjustorThunk (void);
// 0x0000010F System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::Complete(T,System.Boolean,System.Exception)
// 0x00000110 System.String UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_ProviderId()
// 0x00000111 System.Type UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000112 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000113 System.Void UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x00000114 System.Void UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
// 0x00000115 UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_BehaviourFlags()
// 0x00000116 UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::get_Scene()
extern void SceneInstance_get_Scene_m941C7B9506F1E8C45785CD5F6AE03DF8939F3ABE_AdjustorThunk (void);
// 0x00000117 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::set_Scene(UnityEngine.SceneManagement.Scene)
extern void SceneInstance_set_Scene_m8339D0F8872EBEAA41C8D30E6D125AF19C98C8DD_AdjustorThunk (void);
// 0x00000118 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::Activate()
extern void SceneInstance_Activate_m530505E3BA0342410BE3523D387D5AB83B7B7971_AdjustorThunk (void);
// 0x00000119 UnityEngine.AsyncOperation UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::ActivateAsync()
extern void SceneInstance_ActivateAsync_m0B8BF6B8A556B71E075610FF9B47E62B9209120D_AdjustorThunk (void);
// 0x0000011A System.Int32 UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::GetHashCode()
extern void SceneInstance_GetHashCode_m1D0926F7DDB54F2D00B59B84CE8E3D25BB225204_AdjustorThunk (void);
// 0x0000011B System.Boolean UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::Equals(System.Object)
extern void SceneInstance_Equals_mD0482B6D8FF701E433A23F4047E02634128BF0AF_AdjustorThunk (void);
// 0x0000011C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider::ProvideScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
// 0x0000011D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
// 0x0000011E UnityEngine.GameObject UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::ProvideInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
extern void InstanceProvider_ProvideInstance_m8AA295CB4A86AD9E5F02B4AB4FEB5F21387F7847 (void);
// 0x0000011F System.Void UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::ReleaseInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.GameObject)
extern void InstanceProvider_ReleaseInstance_mE265735C7B7A4D6F0C9A51574202884016499AB6 (void);
// 0x00000120 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::.ctor()
extern void InstanceProvider__ctor_mA7E8EB0FA388084ABDBACA27DA2C01369E41917C (void);
// 0x00000121 System.Object UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::Convert(System.Type,System.String)
extern void JsonAssetProvider_Convert_mD40A593AF7815BA0370BF56680B7BDB3244C68F8 (void);
// 0x00000122 System.Void UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::.ctor()
extern void JsonAssetProvider__ctor_mFAF69C1316EEF5EC237555CF441B02D5BAEA662B (void);
// 0x00000123 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void LegacyResourcesProvider_Provide_mEF5558FFD039A3B21397A35C7E2AD125B4E703B7 (void);
// 0x00000124 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void LegacyResourcesProvider_Release_m0F75A0B59A5FD6F3E21B103E4E511BA2DBBFBA33 (void);
// 0x00000125 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::.ctor()
extern void LegacyResourcesProvider__ctor_mD1A1D4C58AA8F53CEEDD79DB11FE3BEDF5331D0E (void);
// 0x00000126 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider_InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void InternalOp_Start_m6C70A85A81F430309DB81F75DF8B73EC27723055 (void);
// 0x00000127 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider_InternalOp::AsyncOperationCompleted(UnityEngine.AsyncOperation)
extern void InternalOp_AsyncOperationCompleted_m42AA9B583E668AF5FE9905B5D007ABE74EB4B3A1 (void);
// 0x00000128 System.Single UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider_InternalOp::PercentComplete()
extern void InternalOp_PercentComplete_mF3F221B34AB03C94CE359A89E5203C168D40A3BD (void);
// 0x00000129 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider_InternalOp::.ctor()
extern void InternalOp__ctor_m93A886B5D9D47C4257A65225CC5CD7D08ACD54B6 (void);
// 0x0000012A System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::get_ProviderId()
extern void ResourceProviderBase_get_ProviderId_m5860108653362B09CF615C607BF9234FD97DC11D (void);
// 0x0000012B System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Initialize(System.String,System.String)
extern void ResourceProviderBase_Initialize_m34E104D3A5D94257BE270AF01B9B58593EB2A83B (void);
// 0x0000012C System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_CanProvide_mD9D178400A79CEF5294BFAE7B6079BAFA3EF653A (void);
// 0x0000012D System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::ToString()
extern void ResourceProviderBase_ToString_m80D7CDE36BB26C8DE21290352CAF1A2C1D753D7C (void);
// 0x0000012E System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void ResourceProviderBase_Release_mC35EDC061332BAE4B65E04429BCD0D199D0F1A06 (void);
// 0x0000012F System.Type UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_GetDefaultType_m2156E5A367DDA36A48496BDD01E01C4985700F57 (void);
// 0x00000130 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x00000131 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
extern void ResourceProviderBase_InitializeAsync_m80430AFAB1D2335A508216D5A110CB736F6171EA (void);
// 0x00000132 UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider.get_BehaviourFlags()
extern void ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mD7856EDEB725FA84A89DAD3F529B9D3FC656EEC6 (void);
// 0x00000133 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::.ctor()
extern void ResourceProviderBase__ctor_m7FCD7D94546A446A53C78AF135940F4F563DFAC7 (void);
// 0x00000134 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_BaseInitAsyncOp::Init(System.Func`1<System.Boolean>)
extern void BaseInitAsyncOp_Init_m50C6E027B3900B3DBCE301C140DE82F74CCE38D2 (void);
// 0x00000135 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_BaseInitAsyncOp::Execute()
extern void BaseInitAsyncOp_Execute_mC12E09D151DA1D4D838FCB2E38B58F824AEB0E3F (void);
// 0x00000136 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_BaseInitAsyncOp::.ctor()
extern void BaseInitAsyncOp__ctor_mE020A405B8D14A3269982E70A9C421A717A92FE3 (void);
// 0x00000137 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mB1D7DDAE2A506B4A834A35D720C18095BA4653C3 (void);
// 0x00000138 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_<>c__DisplayClass10_0::<InitializeAsync>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m3D6B8698FD2339D3145463DFEB3BB21BD687E747 (void);
// 0x00000139 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::ProvideScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void SceneProvider_ProvideScene_mE435E5C564B157899B826BBB6B190EDC54C6D706 (void);
// 0x0000013A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void SceneProvider_ReleaseScene_m9BB8637EA68C3B8DB8E25CDC4852A180DCAA5C8D (void);
// 0x0000013B System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::.ctor()
extern void SceneProvider__ctor_mEC43F97F97C21AF8819D707466AFC7A974213789 (void);
// 0x0000013C System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::.ctor(UnityEngine.ResourceManagement.ResourceManager)
extern void SceneOp__ctor_m6E840C0E940E2F47FCFB25E68AD7394B30A41A7A (void);
// 0x0000013D System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::Init(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
extern void SceneOp_Init_mEC11802072024619BCCBFBF331162214D6756897 (void);
// 0x0000013E System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void SceneOp_GetDependencies_mDFE658D9B04123FFADA4605EB3CC1BD9808E11FE (void);
// 0x0000013F System.String UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::get_DebugName()
extern void SceneOp_get_DebugName_m940D8BB960C1E6DDFCC790C1517666C2065AB06D (void);
// 0x00000140 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::Execute()
extern void SceneOp_Execute_m845979D53419C5D08F0207FC8C948D29612AA497 (void);
// 0x00000141 UnityEngine.ResourceManagement.ResourceProviders.SceneInstance UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::InternalLoadScene(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Boolean,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void SceneOp_InternalLoadScene_m369DFDA4064B0052AAB1FEC513594712DC1E8D5B (void);
// 0x00000142 UnityEngine.AsyncOperation UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::InternalLoad(System.String,System.Boolean,UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneOp_InternalLoad_m9436104CF3623A019EB51DD4C955F2D29B4D30AB (void);
// 0x00000143 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::Destroy()
extern void SceneOp_Destroy_m636C2F910A0170927DF4A69D01D43B04AA87337C (void);
// 0x00000144 System.Single UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::get_Progress()
extern void SceneOp_get_Progress_mFBEE2A7096ECEDB3F0074CC45FF5DD5DF3F9C5EC (void);
// 0x00000145 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::UnityEngine.ResourceManagement.IUpdateReceiver.Update(System.Single)
extern void SceneOp_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m4F862CC6D8E5FE79BA68C842C755AEB5F3B17283 (void);
// 0x00000146 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void UnloadSceneOp_Init_mB10A921B664C5806A883BDE33E1ED18117B6150C (void);
// 0x00000147 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::Execute()
extern void UnloadSceneOp_Execute_mA30928C0C721A4CF8F65EB4C878EE6C8B7E267EE (void);
// 0x00000148 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::UnloadSceneCompleted(UnityEngine.AsyncOperation)
extern void UnloadSceneOp_UnloadSceneCompleted_m30163335443361B263B8D2FA9A216CEA10962E7A (void);
// 0x00000149 System.Single UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::get_Progress()
extern void UnloadSceneOp_get_Progress_m764037909310241E17C9B4F82DCB28AF0ABCC1A3 (void);
// 0x0000014A System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::.ctor()
extern void UnloadSceneOp__ctor_mC1F956299122ACD2793E5D0A2AE8B3DD9A07383C (void);
// 0x0000014B System.Boolean UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::get_IgnoreFailures()
extern void TextDataProvider_get_IgnoreFailures_m54B8CF389A2EF6D0C92E97D23541CD730CE45851 (void);
// 0x0000014C System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::set_IgnoreFailures(System.Boolean)
extern void TextDataProvider_set_IgnoreFailures_m42340E49DD6BFBEE98EA6D24A0FB5B0673372A74 (void);
// 0x0000014D System.Object UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Convert(System.Type,System.String)
extern void TextDataProvider_Convert_m2B991FF6B1CB9CD871439B6F5089BE5AE6DF2518 (void);
// 0x0000014E System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void TextDataProvider_Provide_mF3742F8E9A17003FF5D555E4022419B9EE0F8989 (void);
// 0x0000014F System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::.ctor()
extern void TextDataProvider__ctor_m7AD4D795A1480E3CEC6DCDD478A37FD51E79812D (void);
// 0x00000150 System.Single UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::GetPercentComplete()
extern void InternalOp_GetPercentComplete_m7AC5C9BEC338F6C89783E233E8C532057E62BD2A (void);
// 0x00000151 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle,UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider,System.Boolean)
extern void InternalOp_Start_m2A6D3C16C95797FC3162CC981CFA28C4128E35E2 (void);
// 0x00000152 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::RequestOperation_completed(UnityEngine.AsyncOperation)
extern void InternalOp_RequestOperation_completed_m587EACB3BF4B624F485E87D20E661A8F0192BADE (void);
// 0x00000153 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::.ctor()
extern void InternalOp__ctor_mA96AC18D8B782C4508EB024210E260A8E899753E (void);
// 0x00000154 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::<Start>b__6_0(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void InternalOp_U3CStartU3Eb__6_0_mBDCB8E098F9284FC7C3A90C6461A8C107E159F4A (void);
// 0x00000155 System.Int64 UnityEngine.ResourceManagement.ResourceLocations.ILocationSizeData::ComputeSize(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceManager)
// 0x00000156 System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_InternalId()
// 0x00000157 System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ProviderId()
// 0x00000158 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_Dependencies()
// 0x00000159 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::Hash(System.Type)
// 0x0000015A System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_DependencyHashCode()
// 0x0000015B System.Boolean UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_HasDependencies()
// 0x0000015C System.Object UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_Data()
// 0x0000015D System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_PrimaryKey()
// 0x0000015E System.Type UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ResourceType()
// 0x0000015F System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_InternalId()
extern void ResourceLocationBase_get_InternalId_m446E746FAE3B40B1CDA9A588A319DDBDB1453ABB (void);
// 0x00000160 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ProviderId()
extern void ResourceLocationBase_get_ProviderId_mEE3324D2E4071C922810A050C10AFF5DDCA3EAA3 (void);
// 0x00000161 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_Dependencies()
extern void ResourceLocationBase_get_Dependencies_m054FEB15A19CB1591D9E11587725EAD00100F3EB (void);
// 0x00000162 System.Boolean UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_HasDependencies()
extern void ResourceLocationBase_get_HasDependencies_m25C0F08130902EFEAEF8DDD99D75073BD7377272 (void);
// 0x00000163 System.Object UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_Data()
extern void ResourceLocationBase_get_Data_m97D221A2A5D2CBC3B22C0E748AFC5E0E5AE4E8F8 (void);
// 0x00000164 System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::set_Data(System.Object)
extern void ResourceLocationBase_set_Data_m27679E141DAE40E2566189ABE8E951B9B2BB9E5C (void);
// 0x00000165 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_PrimaryKey()
extern void ResourceLocationBase_get_PrimaryKey_mAA5E2116F41249FE94A19BFB0E102605B8941B24 (void);
// 0x00000166 System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::set_PrimaryKey(System.String)
extern void ResourceLocationBase_set_PrimaryKey_m7EAFEAF682EE8771E9D4AB09C4C2A375A8ACEA48 (void);
// 0x00000167 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_DependencyHashCode()
extern void ResourceLocationBase_get_DependencyHashCode_m1CF137823A47AE4C1EC0503AA257AB7CCDE88CEB (void);
// 0x00000168 System.Type UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ResourceType()
extern void ResourceLocationBase_get_ResourceType_m8621BD5A50D22E8F3C5933811823DB63034A62FD (void);
// 0x00000169 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::Hash(System.Type)
extern void ResourceLocationBase_Hash_mF2BC6233AC5F1F90E1AC29823D2F0CFB639374D4 (void);
// 0x0000016A System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ToString()
extern void ResourceLocationBase_ToString_m828E1E56F19E2A35DF72ED842192DABE7386A9A0 (void);
// 0x0000016B System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::.ctor(System.String,System.String,System.String,System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation[])
extern void ResourceLocationBase__ctor_m9F01B99ECB49EFF4B75BF41E169FBFEF707809AA (void);
// 0x0000016C System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ComputeDependencyHash()
extern void ResourceLocationBase_ComputeDependencyHash_mB8A54AB1782AFCE00F30B2C22D0048B5006DBF52 (void);
// 0x0000016D System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Graph()
extern void DiagnosticEvent_get_Graph_m21E2F78A724957B7AA6BFECBB68D2CED69881BF1_AdjustorThunk (void);
// 0x0000016E System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_ObjectId()
extern void DiagnosticEvent_get_ObjectId_m1CCBE7BC50003514247D99544A60BD22DF70596D_AdjustorThunk (void);
// 0x0000016F System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_DisplayName()
extern void DiagnosticEvent_get_DisplayName_mD05DA6038C714D3FE520B15F9278E3CF36778B97_AdjustorThunk (void);
// 0x00000170 System.Int32[] UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Dependencies()
extern void DiagnosticEvent_get_Dependencies_m5FC6794AD6E24152ABDB0506516B46C3E3538B24_AdjustorThunk (void);
// 0x00000171 System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Stream()
extern void DiagnosticEvent_get_Stream_m2811009448086191841A0EBAC768BD8DEDCB17E7_AdjustorThunk (void);
// 0x00000172 System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Frame()
extern void DiagnosticEvent_get_Frame_mE28A9CF94A945044C148F653F95C4142ECA221B1_AdjustorThunk (void);
// 0x00000173 System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Value()
extern void DiagnosticEvent_get_Value_mE9BC9F537B62C5668DEE8929569E4E46C24ACB76_AdjustorThunk (void);
// 0x00000174 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::.ctor(System.String,System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern void DiagnosticEvent__ctor_m6A631EBC9C9EA934424340F88116686A83511D9B_AdjustorThunk (void);
// 0x00000175 System.Byte[] UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::Serialize()
extern void DiagnosticEvent_Serialize_m6255901EC86D38FBAFDE9069AF340608B45205E0_AdjustorThunk (void);
// 0x00000176 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::Deserialize(System.Byte[])
extern void DiagnosticEvent_Deserialize_m1C6167E45D907FA54A3287E6791D6D10E2A6A6F6 (void);
// 0x00000177 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::FindOrCreateGlobalInstance()
extern void DiagnosticEventCollector_FindOrCreateGlobalInstance_m5BD990B7652CB733C6A21BF644A7B81BE9E8817A (void);
// 0x00000178 System.Guid UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::get_PlayerConnectionGuid()
extern void DiagnosticEventCollector_get_PlayerConnectionGuid_m6515FF5B59417D6493D238D80D2581C489D6C796 (void);
// 0x00000179 System.Boolean UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>,System.Boolean,System.Boolean)
extern void DiagnosticEventCollector_RegisterEventHandler_m198D2A3243FDE6B6C976EB504675E61A39E8654E (void);
// 0x0000017A System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollector_RegisterEventHandler_mD2C0D4CC5D9CD3E276309315AE6411E256EBC51C (void);
// 0x0000017B System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::UnregisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollector_UnregisterEventHandler_m9B73D49777D673D625F01FFA911F2B15316F135E (void);
// 0x0000017C System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::PostEvent(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void DiagnosticEventCollector_PostEvent_m4CFA734CD011BA23C0661A3F2BCE6FFB572C9CB3 (void);
// 0x0000017D System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::Awake()
extern void DiagnosticEventCollector_Awake_mC7773B56D7B9412F5BA8905661D0A38255D75B94 (void);
// 0x0000017E System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::Update()
extern void DiagnosticEventCollector_Update_m0EA57F5DE2E327CF6682733E566A1179AFEB0FB0 (void);
// 0x0000017F System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::OnApplicationQuit()
extern void DiagnosticEventCollector_OnApplicationQuit_m44BBC89DD2585D108F9D349A99DC1CC998B84DA1 (void);
// 0x00000180 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::.ctor()
extern void DiagnosticEventCollector__ctor_m6EBDFC1BB47C875919F8A53247C417754DBFAA81 (void);
// 0x00000181 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector_<>c::.cctor()
extern void U3CU3Ec__cctor_m03501AE1E3ACCDF64A4518B76A3AEF49B8D6698E (void);
// 0x00000182 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector_<>c::.ctor()
extern void U3CU3Ec__ctor_mBD797CCC9A8BE99DFB69CEF9DD7DDA9AB80B0D38 (void);
// 0x00000183 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector_<>c::<Awake>b__12_0(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void U3CU3Ec_U3CAwakeU3Eb__12_0_m20C38A15E2C4BFE5025F0D3A6E77C757EDC719F7 (void);
// 0x00000184 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ICachable::get_Hash()
// 0x00000185 System.Void UnityEngine.ResourceManagement.AsyncOperations.ICachable::set_Hash(System.Int32)
// 0x00000186 System.Object UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetResultAsObject()
// 0x00000187 System.Type UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_ResultType()
// 0x00000188 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Version()
// 0x00000189 System.String UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_DebugName()
// 0x0000018A System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::DecrementReferenceCount()
// 0x0000018B System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::IncrementReferenceCount()
// 0x0000018C System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_ReferenceCount()
// 0x0000018D System.Single UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_PercentComplete()
// 0x0000018E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Status()
// 0x0000018F System.Exception UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_OperationException()
// 0x00000190 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_IsDone()
// 0x00000191 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x00000192 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000193 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000194 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000195 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000196 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000197 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::InvokeCompletionEvent()
// 0x00000198 System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Task()
// 0x00000199 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x0000019A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Handle()
// 0x0000019B System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Execute()
// 0x0000019C System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Destroy()
// 0x0000019D System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Progress()
// 0x0000019E System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_DebugName()
// 0x0000019F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001A0 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Result()
// 0x000001A1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_Result(TObject)
// 0x000001A2 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Version()
// 0x000001A3 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_CompletedEventHasListeners()
// 0x000001A4 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_DestroyedEventHasListeners()
// 0x000001A5 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_CompletedTypelessEventHasListeners()
// 0x000001A6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x000001A7 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_ReferenceCount()
// 0x000001A8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::.ctor()
// 0x000001A9 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ShortenPath(System.String,System.Boolean)
// 0x000001AA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::IncrementReferenceCount()
// 0x000001AB System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::DecrementReferenceCount()
// 0x000001AC System.Threading.WaitHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_WaitHandle()
// 0x000001AD System.Threading.Tasks.Task`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Task()
// 0x000001AE System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Task()
// 0x000001AF System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ToString()
// 0x000001B0 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::RegisterForDeferredCallbackEvent(System.Boolean)
// 0x000001B1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001B2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001B3 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B5 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B7 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Status()
// 0x000001B8 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_OperationException()
// 0x000001B9 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OperationException(System.Exception)
// 0x000001BA System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::MoveNext()
// 0x000001BB System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Reset()
// 0x000001BC System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Current()
// 0x000001BD System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_IsDone()
// 0x000001BE System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_PercentComplete()
// 0x000001BF System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeCompletionEvent()
// 0x000001C0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Handle()
// 0x000001C1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UpdateCallback(System.Single)
// 0x000001C2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Complete(TObject,System.Boolean,System.String)
// 0x000001C3 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x000001C4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeExecute()
// 0x000001C5 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001C6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001C7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001C8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001C9 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Version()
// 0x000001CA System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_ReferenceCount()
// 0x000001CB System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_PercentComplete()
// 0x000001CC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Status()
// 0x000001CD System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_OperationException()
// 0x000001CE System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_IsDone()
// 0x000001CF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Handle()
// 0x000001D0 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x000001D1 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_DebugName()
// 0x000001D2 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetResultAsObject()
// 0x000001D3 System.Type UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_ResultType()
// 0x000001D4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001D5 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.DecrementReferenceCount()
// 0x000001D6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.IncrementReferenceCount()
// 0x000001D7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.InvokeCompletionEvent()
// 0x000001D8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x000001D9 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::<.ctor>b__33_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x000001DA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1_<>c__DisplayClass41_0::.ctor()
// 0x000001DB TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1_<>c__DisplayClass41_0::<get_Task>b__0(System.Object)
// 0x000001DC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::op_Implicit(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000001DD System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>)
// 0x000001DE System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
// 0x000001DF System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32)
// 0x000001E0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Acquire()
// 0x000001E1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001E2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001E3 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E5 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_DebugName()
// 0x000001E6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E8 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Equals(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000001E9 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::GetHashCode()
// 0x000001EA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_InternalOp()
// 0x000001EB System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_IsDone()
// 0x000001EC System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::IsValid()
// 0x000001ED System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_OperationException()
// 0x000001EE System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_PercentComplete()
// 0x000001EF System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_ReferenceCount()
// 0x000001F0 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Release()
// 0x000001F1 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Result()
// 0x000001F2 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Status()
// 0x000001F3 System.Threading.Tasks.Task`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Task()
// 0x000001F4 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.get_Current()
// 0x000001F5 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.MoveNext()
// 0x000001F6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.Reset()
// 0x000001F7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void AsyncOperationHandle__ctor_m4A737468E34CBBCC7DDA85F5A0A5D461DB7F4913_AdjustorThunk (void);
// 0x000001F8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32)
extern void AsyncOperationHandle__ctor_m93AADC394EAFD7638E5B37B60CC52C229A8B74DC_AdjustorThunk (void);
// 0x000001F9 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Acquire()
extern void AsyncOperationHandle_Acquire_mC15C71C7BA8B95C49B8BCCD33CD2F7EB5B03BF7F_AdjustorThunk (void);
// 0x000001FA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Completed_mBD4A54B7E0322F292C67A62A5461193758726B20_AdjustorThunk (void);
// 0x000001FB System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Completed_mFF2A9DFC4D2E2A6D1302BFBC7EA49E01E3E14CEB_AdjustorThunk (void);
// 0x000001FC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Convert()
// 0x000001FD System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_DebugName()
extern void AsyncOperationHandle_get_DebugName_m9263AE2FF7206453566D317443292E71686910FE_AdjustorThunk (void);
// 0x000001FE System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Destroyed_mCDA263102547EEC0D53DD5B2B7F55881EC896235_AdjustorThunk (void);
// 0x000001FF System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Destroyed_m3BE893CECF7F7273A884984B6025867D7AA90FB2_AdjustorThunk (void);
// 0x00000200 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_GetDependencies_mD420A5EA523C940C0F90A216B971E8B6A060F71E_AdjustorThunk (void);
// 0x00000201 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetHashCode()
extern void AsyncOperationHandle_GetHashCode_m7764BEBFB86D7C0EB3D46D4AC116ADD56AEEAC68_AdjustorThunk (void);
// 0x00000202 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_InternalOp()
extern void AsyncOperationHandle_get_InternalOp_m09CAB010CFD3205ADCB1128F3046132C538B8EFE_AdjustorThunk (void);
// 0x00000203 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_IsDone()
extern void AsyncOperationHandle_get_IsDone_m7F8233755EDB537A312B42BFAC1B4E95FDEB0B62_AdjustorThunk (void);
// 0x00000204 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::IsValid()
extern void AsyncOperationHandle_IsValid_mDDEEC690F9BFA3CFA09B8344947F1405EECA22F1_AdjustorThunk (void);
// 0x00000205 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_OperationException()
extern void AsyncOperationHandle_get_OperationException_mE66E9FDDCF99EF8B6D706A05F3156514D552C259_AdjustorThunk (void);
// 0x00000206 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_PercentComplete()
extern void AsyncOperationHandle_get_PercentComplete_m56F5B0016C1AE1681A5670F8F50F716F94C4B9C1_AdjustorThunk (void);
// 0x00000207 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_ReferenceCount()
extern void AsyncOperationHandle_get_ReferenceCount_mC0B0856194FE7E39B581A60345938FCBF6FD8BA6_AdjustorThunk (void);
// 0x00000208 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Release()
extern void AsyncOperationHandle_Release_m75B6AFC0710FF804ECD0CBC6E7EFB85EEF095F67_AdjustorThunk (void);
// 0x00000209 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Result()
extern void AsyncOperationHandle_get_Result_m692BEC3C25DA9A5BFCD041C069C693B25F783DED_AdjustorThunk (void);
// 0x0000020A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Status()
extern void AsyncOperationHandle_get_Status_m541A1E32905CA1F7092BFBB1299F939271FA57E7_AdjustorThunk (void);
// 0x0000020B System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Task()
extern void AsyncOperationHandle_get_Task_mE97DD2E15087A3EEAEA60F51C555BD7BC18C6F9F_AdjustorThunk (void);
// 0x0000020C System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.get_Current()
extern void AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m902A963CD3B19733FB41AA0282DA4807D22668C8_AdjustorThunk (void);
// 0x0000020D System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.MoveNext()
extern void AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m49963C8BC42694191D9AF69FB63C1DEB8472BCB7_AdjustorThunk (void);
// 0x0000020E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.Reset()
extern void AsyncOperationHandle_System_Collections_IEnumerator_Reset_mC3C2DA4955C33CDC282A3155FC1619EE5DE50810_AdjustorThunk (void);
// 0x0000020F System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::.ctor()
extern void GroupOperation__ctor_m53C7DABD998F336EF512B5A7939EBCEFC35453EE (void);
// 0x00000210 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::UnityEngine.ResourceManagement.AsyncOperations.ICachable.get_Hash()
extern void GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_get_Hash_m255C41D287D9D2F81B8CD36C843E072677509658 (void);
// 0x00000211 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::UnityEngine.ResourceManagement.AsyncOperations.ICachable.set_Hash(System.Int32)
extern void GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_set_Hash_m8BC643CCFAFFF998702423EC9EE41C29F01C48F9 (void);
// 0x00000212 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDependentOps()
extern void GroupOperation_GetDependentOps_m65A13CA9F754AB7B61EDE32ACD44E96EF66A2EB8 (void);
// 0x00000213 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void GroupOperation_GetDependencies_m63B2EBBAE700A12C192B6624E18B5AECA68BAE54 (void);
// 0x00000214 System.String UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::get_DebugName()
extern void GroupOperation_get_DebugName_m10F52CC27B3E235135A71BA5BE8FB11B78DC603F (void);
// 0x00000215 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Execute()
extern void GroupOperation_Execute_mF71134E1DCBA889EF1ADB17EC1E16A6DAEEF4A1F (void);
// 0x00000216 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::CompleteIfDependenciesComplete()
extern void GroupOperation_CompleteIfDependenciesComplete_m81CE72A80A868B8825BFBEC959976741EC5C399E (void);
// 0x00000217 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Destroy()
extern void GroupOperation_Destroy_m5B8C2CA59E96C598672FA4FEF25D20B2CFA74BFC (void);
// 0x00000218 System.Single UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::get_Progress()
extern void GroupOperation_get_Progress_m330078EFB12B5B49E59B66D34CF8D619A23553E2 (void);
// 0x00000219 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Init(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void GroupOperation_Init_m29EC3388CA40669CB8D9E6CF7B06E1C35DCD2458 (void);
// 0x0000021A System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::OnOperationCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void GroupOperation_OnOperationCompleted_mCEAB8FA43C2DBEB9C72FD6A2A6412A76AD46DD58 (void);
// 0x0000021B System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x0000021C System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_ProvideHandleVersion()
// 0x0000021D UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_Location()
// 0x0000021E System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_DependencyCount()
// 0x0000021F System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
// 0x00000220 TDepObject UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependency(System.Int32)
// 0x00000221 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::SetProgressCallback(System.Func`1<System.Single>)
// 0x00000222 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x00000223 System.Type UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_RequestedType()
// 0x00000224 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::UnityEngine.ResourceManagement.AsyncOperations.ICachable.get_Hash()
// 0x00000225 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::UnityEngine.ResourceManagement.AsyncOperations.ICachable.set_Hash(System.Int32)
// 0x00000226 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_ProvideHandleVersion()
// 0x00000227 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_Location()
// 0x00000228 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::.ctor()
// 0x00000229 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000022A System.String UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_DebugName()
// 0x0000022B System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
// 0x0000022C System.Type UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_RequestedType()
// 0x0000022D System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_DependencyCount()
// 0x0000022E TDepObject UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependency(System.Int32)
// 0x0000022F System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::SetProgressCallback(System.Func`1<System.Single>)
// 0x00000230 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x00000231 System.Single UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_Progress()
// 0x00000232 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Execute()
// 0x00000233 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x00000234 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Destroy()
static Il2CppMethodPointer s_methodPointers[564] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoBehaviourCallbackHooks_add_OnUpdateDelegate_m71F16EDF13A73B69505988BF80F3BD3C15D657B6,
	MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m5A983C728E2EECA2E549DD44203A30BC9EA4488F,
	MonoBehaviourCallbackHooks_Awake_m380AFA73E5C2B03FB2DC04F469CEC765362F3AE1,
	MonoBehaviourCallbackHooks_Update_mF0F182D68DFD4098DEB677350B6C87935C23D9E2,
	MonoBehaviourCallbackHooks__ctor_m6DA4F0AF02FEBFF30AA23A97945582D2884EB1C1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceManager_get_ExceptionHandler_m7AA5E78ACAE04A2406EBA923EE0CBC2BFE39EB01,
	ResourceManager_set_ExceptionHandler_mA6F5EBE9E65F68C8A8610638985E34304FE15BA7,
	ResourceManager_get_InternalIdTransformFunc_mE09C9ABCDE9E35C821FFBEEF42FCE9FDA8FEB308,
	ResourceManager_set_InternalIdTransformFunc_m0287E07471C3786B84A9A320065C8E2B782E31C1,
	ResourceManager_TransformInternalId_m99309A473F0354A9A003C8269967FB49300DC06C,
	ResourceManager_get_OperationCacheCount_m088CAEA949AC929E003D39339F9A6CD77EEC9253,
	ResourceManager_get_InstanceOperationCount_m2A10D47B814899A0C7E5880E37DA48DE883D3ECC,
	ResourceManager_AddUpdateReceiver_m8E85F81F33E25C178B60C465322AADE11C03AA1F,
	ResourceManager_RemoveUpdateReciever_mE9CC3445844CC1E22DDD4815184FD27412E07925,
	ResourceManager_get_Allocator_mEEC5A006F0FDDB3771E377F1A40C9AE319B5A474,
	ResourceManager_set_Allocator_m5B678A15912CC5908DB81FFA8F49C625E7E14377,
	ResourceManager_get_ResourceProviders_m7B8444862B525921DCA90FFF0F89BAE11825677A,
	ResourceManager_get_CertificateHandlerInstance_m12F938F5FEF5B4853F73BBF21EEE51C73716F9B9,
	ResourceManager_set_CertificateHandlerInstance_m0DED0F9FE32208F8C3F3A995E93DE2AC7CFF2F9D,
	ResourceManager__ctor_m528366A74390F0F37D4F8D327B311E131484895F,
	ResourceManager_OnObjectAdded_m0A54698AC9BA86E496F1E3BA0DFF1A8DDB00600E,
	ResourceManager_OnObjectRemoved_mFB8B23D34D88A56665DD72B6487456D7B0F73294,
	ResourceManager_RegisterForCallbacks_mDFD4D66966A06B27431E6CFA21C62653613C5395,
	ResourceManager_ClearDiagnosticsCallback_m21D48E1CD446FADEB667D05256F3B165FCC6B97C,
	ResourceManager_ClearDiagnosticCallbacks_mC2ACDDB95C7291580AB454B5FEA5D76F5C953A5F,
	ResourceManager_UnregisterDiagnosticCallback_m94060E96C7D51B594484713FB4DE87A9B0A13454,
	ResourceManager_RegisterDiagnosticCallback_m80A28924C61813B6E8E547E85B6F009514BD6F29,
	ResourceManager_RegisterDiagnosticCallback_m7C36E880E0E8C7924E0B74A6769D3BFBD76BD65C,
	ResourceManager_PostDiagnosticEvent_m774EFC2B6BC9364BCF3B242FD3115BD3F94EA6BE,
	ResourceManager_GetResourceProvider_m38873363AA6A66617312E8F2ECE4277BA17B24A3,
	ResourceManager_GetDefaultTypeForLocation_mEB119E0ACD2ECD658112DCE606E3BD8B74B8C3A2,
	ResourceManager_CalculateLocationsHash_m9BE52AD42B5E6FB2082A455391477A87AF4A18AE,
	ResourceManager_ProvideResource_m594F8B2A6122F7744DBCCE7316450F302D6AB7C2,
	NULL,
	NULL,
	ResourceManager_StartOperation_mF350C51B1D49B0A1900C7B955595E50A560DE47A,
	ResourceManager_OnInstanceOperationDestroy_m0519061477A75DB2BF0F3F8A18CB34F25D908E9F,
	ResourceManager_OnOperationDestroyNonCached_m98C9B9996EE251C99419A7C6D4AC674C41B487E7,
	ResourceManager_OnOperationDestroyCached_mB4AFABDBC148F73495D45930E03AE05055ACABCC,
	NULL,
	ResourceManager_AddOperationToCache_mF821E49A9CAE2772E817ECB95460480AC32AFC23,
	ResourceManager_RemoveOperationFromCache_m42791732D849ED8CB5B858AF11CA1A0D3ABA6590,
	ResourceManager_IsOperationCached_m83330F9DD03DE37659319CC3EE6B607B49EEBE63,
	ResourceManager_CachedOperationCount_mA1EB8E0A0D61A773132C5BE029CAFC7A0C67C19A,
	NULL,
	NULL,
	ResourceManager_Release_mD67A0632D3D7D0B7F97841BC1E4C7CF2FC191C57,
	ResourceManager_Acquire_m88572CDE3229742F390437973CDAFEC0C4B1074C,
	ResourceManager_AcquireGroupOpFromCache_m5782A7CFB719A51E06134911422E3C5F9567B355,
	NULL,
	ResourceManager_CreateGenericGroupOperation_m148289B454ABDC6D18F9BA386EFEE2C4FFA640F4,
	ResourceManager_ProvideResourceGroupCached_mD1115A6CB5B547E6767CE7DC3289BB2C64D9E545,
	NULL,
	NULL,
	NULL,
	ResourceManager_ProvideScene_m003D41F2CBA62B7D1A53CED8E312CB9E5DC1F055,
	ResourceManager_ReleaseScene_mBD5161D48EFD7ACAD02CB9A12378CAE0EA8A828E,
	ResourceManager_ProvideInstance_mAE65646633A09A4E51654766498DAA9D9EE3C951,
	ResourceManager_CleanupSceneInstances_m2BBE6BCBFBA7DC2CEA5D38DE680FA6A52B0EA528,
	ResourceManager_ExecuteDeferredCallbacks_m75C200EC542CDBCEB07DBD4720835323A34D144F,
	ResourceManager_RegisterForDeferredCallback_m0436E85EDF6A341FEE22C1C7A360EC0643042272,
	ResourceManager_Update_m1038C44C2B864801F1D40DC503AA7F1029AAAF70,
	ResourceManager_Dispose_m90EE4D872BF46DE7D69ED70BD540A2B46064D0B8,
	ResourceManager__cctor_m9E01A63275781A7065E780992DFD1A0269EE76CB,
	ResourceManager_U3C_ctorU3Eb__45_0_mCECD009288795FC98DBCE498BA37927C1AD49273,
	DiagnosticEventContext_get_OperationHandle_m6884404709298E7C22B1F7F8205C1B7E8796EE05_AdjustorThunk,
	DiagnosticEventContext_get_Type_mC90B555707F6837DA6A22722EF4353065334C905_AdjustorThunk,
	DiagnosticEventContext_get_EventValue_mCD0F3458A7BCBE06550729AC816B34E5E19E14EF_AdjustorThunk,
	DiagnosticEventContext_get_Location_mF350B540A75C4C08BA750678E3827EADBE2FDF75_AdjustorThunk,
	DiagnosticEventContext_get_Context_m4B8F30E7A090EB33FE0C1F8720C40273B3859C04_AdjustorThunk,
	DiagnosticEventContext_get_Error_m1CF5543DF39540144965910BC6FFF71862753E60_AdjustorThunk,
	DiagnosticEventContext__ctor_mC21627E8722C2E92B119E731AA890319A131FECA_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	InstanceOperation_Init_mFEA262E8A7D70C9FFADB5F4076C5B9C7341026C9,
	InstanceOperation_GetDependencies_m76864181C3947B5A9C885BC53983F10B45D29741,
	InstanceOperation_get_DebugName_m2163DD9A80CE393626ECC58E61AE24BB697DD053,
	InstanceOperation_InstanceScene_mAC44625C564DE76E956CA8BCF4EC2C9F6178D9DE,
	InstanceOperation_Destroy_mE795720CCC23566F64105C160595FE0D634D57FA,
	InstanceOperation_get_Progress_mED88CD3EA094C0A946CF1B31074C9C8D3D3E9268,
	InstanceOperation_Execute_m8792B88CB3F74349E1FD1D35E9426D58894F19D3,
	InstanceOperation__ctor_m491135C07279DE258B3D9EE81BC28527EEF1BA21,
	NULL,
	NULL,
	NULL,
	NULL,
	WebRequestQueueOperation_get_IsDone_mD60F4EB09496B7A2F86C696815E34491375CC9D0,
	WebRequestQueueOperation__ctor_m68CB315ACE251B662B8A4A603751432C8048F38A,
	WebRequestQueueOperation_Complete_m045307756A8EB68F5ADC3E6FF182361C60AABBF8,
	WebRequestQueue_QueueRequest_m9E882E5334EC6CB6FB517C1A0DF985B83779B78F,
	WebRequestQueue_OnWebAsyncOpComplete_mF0B3325CA9B1386C3DAB80E1D87C4C5DB16F624C,
	WebRequestQueue__cctor_m387136624EFB374B41B0EEE1A63506F97F28D9B6,
	ResourceManagerException__ctor_mAEDC8C69DA13800EC322613C6829018DBB1E07CB,
	ResourceManagerException__ctor_mDBACFBD3B06C3A8F3436E8A78F8B10E6ED6759A1,
	ResourceManagerException__ctor_mA9EAADAF71BABEB9D6EB3063410E4245186ADD3B,
	ResourceManagerException__ctor_mEDDA961AA897100A189BB8E88B3FD87FBA614544,
	UnknownResourceProviderException_get_Location_mB274BAF53E19FE874A24CC67C522D494CCC8F4F0,
	UnknownResourceProviderException_set_Location_mF93EB4AAA41E957EFFACCEA94DFF45AEC67EB619,
	UnknownResourceProviderException__ctor_mB1E23AB4989DE1993682D165DD042F52E042AAE8,
	UnknownResourceProviderException__ctor_mCB059067E6AF43FD4715E52CDA42C6D95901D796,
	UnknownResourceProviderException__ctor_mB8D96934D5DEE32F14F4AE30438961E70B361EE7,
	UnknownResourceProviderException__ctor_m9D021A7F0FE0B700DD55ED1060274872AA899098,
	UnknownResourceProviderException__ctor_m70991EE39B986E427F59C87D1D52DB7885C03546,
	UnknownResourceProviderException_get_Message_mC59322E40728F5119EB36A38F38BA109AFA55E28,
	UnknownResourceProviderException_ToString_mE320AC807B6D64A5AAD350EB35DFB6781B571648,
	DelayedActionManager_GetNode_m4B1CA12E533503A5C4D48BB1C80F10F62565E489,
	DelayedActionManager_Clear_m74693E78A10AB5620D5A91374EE31AAE959064FB,
	DelayedActionManager_DestroyWhenComplete_mB9B1B0A8DE58B2DBCACE37C5846FBFCAA6E18BCA,
	DelayedActionManager_AddAction_m59A84189B25DF74719B2D26B7F50E9AD275F1742,
	DelayedActionManager_AddActionInternal_m570A8E6E3923163BA9811F5569516807717659E0,
	DelayedActionManager_Awake_m693AC7F17BF8583DBBAED409AB03408F0E02C964,
	DelayedActionManager_get_IsActive_m4E01D61FD7704242F5B12498F318DA7B79C43133,
	DelayedActionManager_Wait_m42346772DD5598A885F50832E40928B438663245,
	DelayedActionManager_LateUpdate_m5DA30ECF97E705B5C199CB708EE73B39E676EDE7,
	DelayedActionManager_InternalLateUpdate_m0BDB5B3466E86462F1F6059ABAF70A9FCD7F7059,
	DelayedActionManager_OnApplicationQuit_m50C8D15F97A47AFFB397A02E762E1328AF784979,
	DelayedActionManager__ctor_mF1F8365F8BCEE13A73CD17B17927DB087F9F70EF,
	DelegateInfo__ctor_m5F9781FFA3BB8947EC7DDE4D0E927D6C56AAED56_AdjustorThunk,
	DelegateInfo_get_InvocationTime_mE58AD92E984581E6CF13DB1BB8E74A9E1993674C_AdjustorThunk,
	DelegateInfo_ToString_m5D5AD0956A9A4B7FC994E4E37B5A5E0E8831C4E1_AdjustorThunk,
	DelegateInfo_Invoke_m6E94807D8DD79D90FEC652EA7AB2450CA3A2428B_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DefaultAllocationStrategy_New_mB78CC5AFBF1B07035814191AB926E4ACBD41A6CE,
	DefaultAllocationStrategy_Release_mD958EA6925889742484435B1CD1AAF4087F60E4A,
	DefaultAllocationStrategy__ctor_mC395409A503C0199396C35AA3B939CE682AB3C8A,
	LRUCacheAllocationStrategy__ctor_m1DE9B78742C8D5457040C65AFA15854983E33FC6,
	LRUCacheAllocationStrategy_GetPool_mFC7368E7F5D84E6CFDCC693520C2B2B6965627A8,
	LRUCacheAllocationStrategy_ReleasePool_m72D0EB4F039EE69BDB8BBB68AFB8BA7D62B92BCF,
	LRUCacheAllocationStrategy_New_m9A93EA16BC5C9C0F1933D6A19C8C19F854F9D2E3,
	LRUCacheAllocationStrategy_Release_m42CAEAF3890309E4396717B9CE28D17082BA6300,
	SerializedTypeRestrictionAttribute__ctor_m73D9657C618F99685B17D67B3C0E10CE9C16007A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializedType_get_AssemblyName_m26AAB5BD9E0D42C3748D63FFFF829035428DF953_AdjustorThunk,
	SerializedType_get_ClassName_mB9E5EB03E83676EECFA405A1A1369B87D9B69C61_AdjustorThunk,
	SerializedType_ToString_m1148F8BA12B705068191BD22F924E4570E0CF2BF_AdjustorThunk,
	SerializedType_get_Value_m1BC4883C0CA3F911AC298F99BA3820D28B116AE4_AdjustorThunk,
	SerializedType_set_Value_m926E07D1B3ADDEC9AC75DC7EABCDF9C1D8B9FCFF_AdjustorThunk,
	SerializedType_get_ValueChanged_m3B4DCF3F7B8B10EBCBB1CC9F51BDF9B90810022E_AdjustorThunk,
	SerializedType_set_ValueChanged_m237555FB2018F1106F72F91325662ED6779CCFF2_AdjustorThunk,
	ObjectInitializationData_get_Id_mEC5373B1DDFE78FB72F291806ED9C869DA2B27B5_AdjustorThunk,
	ObjectInitializationData_get_ObjectType_m14F9142B5C61D1F8285BA84C34B2747EBDE59DD0_AdjustorThunk,
	ObjectInitializationData_get_Data_m0A9B73EED42A63E5DAF79EFD978AC64E38B23D71_AdjustorThunk,
	ObjectInitializationData_ToString_mA1CB0E03EDC5E862A576AC96DF6E768E018BE8EF_AdjustorThunk,
	NULL,
	ObjectInitializationData_GetAsyncInitHandle_m1A3A5DA8AD774DC20CC561528A6305CC4F5999A6_AdjustorThunk,
	ResourceManagerConfig_ExtractKeyAndSubKey_m10E4749CC55275EB25BB3341FF729050BEFCABB0,
	ResourceManagerConfig_IsPathRemote_m2626B711D462C076C9028B01395AFB5D905C58CB,
	ResourceManagerConfig_ShouldPathUseWebRequest_m20ED59B08CD29D311DE947B60B3020B6447EA19A,
	ResourceManagerConfig_CreateArrayResult_mC0878410CBD7950D2E123DD3C8592AFF4CAD0426,
	NULL,
	ResourceManagerConfig_CreateListResult_m5A686FF2B8B99AC174723DA1AF7DDAE9D04D5ADF,
	NULL,
	NULL,
	NULL,
	AssetBundleRequestOptions_get_Hash_m7E8FFDF35E4F8490371194922EE7EA74D02060EA,
	AssetBundleRequestOptions_set_Hash_m7139F049F7A9D7C605C213FA8183B6BBFE20427A,
	AssetBundleRequestOptions_get_Crc_m5F918FDCC93369AA688F6EAC9FF56D1C901EB9E3,
	AssetBundleRequestOptions_set_Crc_mB615B2A91B6C4EC004B9AD0290916A17CBC2ECF0,
	AssetBundleRequestOptions_get_Timeout_m0EC9538E2205B9E1313753385847B90A69D6DF6A,
	AssetBundleRequestOptions_set_Timeout_m5853BF9CE70EDD668859EF45CDEF31F365B710B5,
	AssetBundleRequestOptions_get_ChunkedTransfer_m7FACD52559F14DEA310A70D42FF4065AE8A63E47,
	AssetBundleRequestOptions_set_ChunkedTransfer_m3EF05C676B21C63FA0904876A13CABF1EB0DFE3D,
	AssetBundleRequestOptions_get_RedirectLimit_m95B063F1734F63EFA277FAFF37904FB986BD091B,
	AssetBundleRequestOptions_set_RedirectLimit_m73E2D9E93B3EBA1702DA6F982861F71FB0FA0ABE,
	AssetBundleRequestOptions_get_RetryCount_m7998E1CF424014231B8181583BE8DE1DFFBFF79C,
	AssetBundleRequestOptions_set_RetryCount_mD353E67BE0A34AE43D8D5BAF8611C02998820358,
	AssetBundleRequestOptions_get_BundleName_m03D99F2383AD124A7346CFD8BEAA3F803362982D,
	AssetBundleRequestOptions_set_BundleName_m64C87ED93FAD47B98555EFAA5C2AF50CC425B93E,
	AssetBundleRequestOptions_get_BundleSize_m447FF9C828884DE530C97D4F8EF98B02C57E1D95,
	AssetBundleRequestOptions_set_BundleSize_m3B30445CFA5ABFBBFE1BC62AAB53BCD7AE93A118,
	AssetBundleRequestOptions_ComputeSize_m5E3CDC20AE5F1970B47BF96397A239005DD67161,
	AssetBundleRequestOptions__ctor_m75BC7C2F98DE1E2A506BDB5DFA1FF2E93ABF1284,
	AssetBundleResource_CreateWebRequest_m6DDCA06801DAD8223CF1A68974B1C85D315E518A,
	AssetBundleResource_PercentComplete_m379EC2172D7AFCC9E9026847389969EF1AEB0913,
	AssetBundleResource_GetAssetBundle_m7EAF635CD73ECAD7119B84CA933EB8A3D5E64A7E,
	AssetBundleResource_Start_m40703643636E0DC5A289132532F961A8106AF675,
	AssetBundleResource_BeginOperation_m069554D609B10EB9691072B6334D4C31F4A14266,
	AssetBundleResource_LocalRequestOperationCompleted_mF195784E9CBE63D363B00629E44459AABA215477,
	AssetBundleResource_WebRequestOperationCompleted_m8729ED7DC01163D46A6E486AE8690CECF2A7D52A,
	AssetBundleResource_Unload_mD88D286E0C628E6C5DFD34B7BCBFD58FDC6D2968,
	AssetBundleResource__ctor_m9A68288B5047A5A1D36A19E0D19C19EAAB51C591,
	AssetBundleResource_U3CBeginOperationU3Eb__11_0_mC51B5EE30BA90EFB788E2397DE3FE99070B81D21,
	AssetBundleProvider_Provide_m97B5CBAF2B5AE8DF7B37591CB1D46C8779EF9200,
	AssetBundleProvider_GetDefaultType_m66A9FDF33312277E8EEBE303539D452944901776,
	AssetBundleProvider_Release_m3291510B7E9B4F159C2E474D59D0134D1E43DAE4,
	AssetBundleProvider__ctor_m1FC8A928B824E44695F4AF75A2CCD61602320831,
	AtlasSpriteProvider_Provide_m1EEB6E65B16F76C067088BEAF5CD12D9E040BA8E,
	AtlasSpriteProvider__ctor_m7B78828B1E7EF669E8F189BED2D40FF0D3B73094,
	BundledAssetProvider_Provide_mC7B58A62D1D9778FC4A5B0EED0DA6D7D2A05649C,
	BundledAssetProvider__ctor_m5DF36E924DEBBDD1FEB2A32EDFE823478AE410EF,
	InternalOp_LoadBundleFromDependecies_m46D5AC51CD34C01580FA458D0AFA12152A23DD5E,
	InternalOp_Start_m919D6A046CAD232EFEA23C9884033249FC9B35C8,
	InternalOp_ActionComplete_m993FB0E901E8937DF0DA612852DF050C610350AF,
	InternalOp_ProgressCallback_mCC6E27B16074B9DE33ACC4676ED69F9B7EA3C784,
	InternalOp__ctor_m9842FA729C3DF56A9E53B8F9C13D90E1B964A30C,
	InstantiationParameters_get_Position_m513B350F34651CD2D5B865E9A67F3DF9299C7993_AdjustorThunk,
	InstantiationParameters_get_Rotation_mD3BCB21E3275C5BC81E27F94F0B5BD0CEC5AA2D0_AdjustorThunk,
	InstantiationParameters_get_Parent_mCEF42A2F091CDD7B7C8ECAD9FE9EA2E843515893_AdjustorThunk,
	InstantiationParameters_get_InstantiateInWorldPosition_m2F7E212FDFB4213E04066CF078A97AAA40D7BFF5_AdjustorThunk,
	InstantiationParameters_get_SetPositionRotation_m1A867DF4E1EAC95D9DFC3919CBC2E56590046EF3_AdjustorThunk,
	InstantiationParameters__ctor_mA7C5DA2011D4832617164C7B4DB467140A93014F_AdjustorThunk,
	InstantiationParameters__ctor_m5A71887F734D777681811274B19E2327CB487A01_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	ProvideHandle__ctor_m6162616DB274E0DF650DB36B4ECD1AB8FF7FF0AA_AdjustorThunk,
	ProvideHandle_get_InternalOp_mBAFE92C7761F1994A9AC5542BC7085AE67C57051_AdjustorThunk,
	ProvideHandle_get_ResourceManager_m05D4E79DEBFDD6BD466CD6A2219958B00C6C3E8E_AdjustorThunk,
	ProvideHandle_get_Type_m9569FAA40332A3EFD53227FABC324CC03B4BAD3D_AdjustorThunk,
	ProvideHandle_get_Location_m5F7540397789E7214F8B9DD88C8E9B8A7A6ABE53_AdjustorThunk,
	ProvideHandle_get_DependencyCount_m2E1FFD8268C87179D01E7E188CEEFF34B6D87DA9_AdjustorThunk,
	NULL,
	ProvideHandle_GetDependencies_m50B8C3466AED683B0F8581207B09D887350CB741_AdjustorThunk,
	ProvideHandle_SetProgressCallback_mCADBA6D92915BE6956E55E25A84F9A5E61BD289F_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SceneInstance_get_Scene_m941C7B9506F1E8C45785CD5F6AE03DF8939F3ABE_AdjustorThunk,
	SceneInstance_set_Scene_m8339D0F8872EBEAA41C8D30E6D125AF19C98C8DD_AdjustorThunk,
	SceneInstance_Activate_m530505E3BA0342410BE3523D387D5AB83B7B7971_AdjustorThunk,
	SceneInstance_ActivateAsync_m0B8BF6B8A556B71E075610FF9B47E62B9209120D_AdjustorThunk,
	SceneInstance_GetHashCode_m1D0926F7DDB54F2D00B59B84CE8E3D25BB225204_AdjustorThunk,
	SceneInstance_Equals_mD0482B6D8FF701E433A23F4047E02634128BF0AF_AdjustorThunk,
	NULL,
	NULL,
	InstanceProvider_ProvideInstance_m8AA295CB4A86AD9E5F02B4AB4FEB5F21387F7847,
	InstanceProvider_ReleaseInstance_mE265735C7B7A4D6F0C9A51574202884016499AB6,
	InstanceProvider__ctor_mA7E8EB0FA388084ABDBACA27DA2C01369E41917C,
	JsonAssetProvider_Convert_mD40A593AF7815BA0370BF56680B7BDB3244C68F8,
	JsonAssetProvider__ctor_mFAF69C1316EEF5EC237555CF441B02D5BAEA662B,
	LegacyResourcesProvider_Provide_mEF5558FFD039A3B21397A35C7E2AD125B4E703B7,
	LegacyResourcesProvider_Release_m0F75A0B59A5FD6F3E21B103E4E511BA2DBBFBA33,
	LegacyResourcesProvider__ctor_mD1A1D4C58AA8F53CEEDD79DB11FE3BEDF5331D0E,
	InternalOp_Start_m6C70A85A81F430309DB81F75DF8B73EC27723055,
	InternalOp_AsyncOperationCompleted_m42AA9B583E668AF5FE9905B5D007ABE74EB4B3A1,
	InternalOp_PercentComplete_mF3F221B34AB03C94CE359A89E5203C168D40A3BD,
	InternalOp__ctor_m93A886B5D9D47C4257A65225CC5CD7D08ACD54B6,
	ResourceProviderBase_get_ProviderId_m5860108653362B09CF615C607BF9234FD97DC11D,
	ResourceProviderBase_Initialize_m34E104D3A5D94257BE270AF01B9B58593EB2A83B,
	ResourceProviderBase_CanProvide_mD9D178400A79CEF5294BFAE7B6079BAFA3EF653A,
	ResourceProviderBase_ToString_m80D7CDE36BB26C8DE21290352CAF1A2C1D753D7C,
	ResourceProviderBase_Release_mC35EDC061332BAE4B65E04429BCD0D199D0F1A06,
	ResourceProviderBase_GetDefaultType_m2156E5A367DDA36A48496BDD01E01C4985700F57,
	NULL,
	ResourceProviderBase_InitializeAsync_m80430AFAB1D2335A508216D5A110CB736F6171EA,
	ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mD7856EDEB725FA84A89DAD3F529B9D3FC656EEC6,
	ResourceProviderBase__ctor_m7FCD7D94546A446A53C78AF135940F4F563DFAC7,
	BaseInitAsyncOp_Init_m50C6E027B3900B3DBCE301C140DE82F74CCE38D2,
	BaseInitAsyncOp_Execute_mC12E09D151DA1D4D838FCB2E38B58F824AEB0E3F,
	BaseInitAsyncOp__ctor_mE020A405B8D14A3269982E70A9C421A717A92FE3,
	U3CU3Ec__DisplayClass10_0__ctor_mB1D7DDAE2A506B4A834A35D720C18095BA4653C3,
	U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m3D6B8698FD2339D3145463DFEB3BB21BD687E747,
	SceneProvider_ProvideScene_mE435E5C564B157899B826BBB6B190EDC54C6D706,
	SceneProvider_ReleaseScene_m9BB8637EA68C3B8DB8E25CDC4852A180DCAA5C8D,
	SceneProvider__ctor_mEC43F97F97C21AF8819D707466AFC7A974213789,
	SceneOp__ctor_m6E840C0E940E2F47FCFB25E68AD7394B30A41A7A,
	SceneOp_Init_mEC11802072024619BCCBFBF331162214D6756897,
	SceneOp_GetDependencies_mDFE658D9B04123FFADA4605EB3CC1BD9808E11FE,
	SceneOp_get_DebugName_m940D8BB960C1E6DDFCC790C1517666C2065AB06D,
	SceneOp_Execute_m845979D53419C5D08F0207FC8C948D29612AA497,
	SceneOp_InternalLoadScene_m369DFDA4064B0052AAB1FEC513594712DC1E8D5B,
	SceneOp_InternalLoad_m9436104CF3623A019EB51DD4C955F2D29B4D30AB,
	SceneOp_Destroy_m636C2F910A0170927DF4A69D01D43B04AA87337C,
	SceneOp_get_Progress_mFBEE2A7096ECEDB3F0074CC45FF5DD5DF3F9C5EC,
	SceneOp_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m4F862CC6D8E5FE79BA68C842C755AEB5F3B17283,
	UnloadSceneOp_Init_mB10A921B664C5806A883BDE33E1ED18117B6150C,
	UnloadSceneOp_Execute_mA30928C0C721A4CF8F65EB4C878EE6C8B7E267EE,
	UnloadSceneOp_UnloadSceneCompleted_m30163335443361B263B8D2FA9A216CEA10962E7A,
	UnloadSceneOp_get_Progress_m764037909310241E17C9B4F82DCB28AF0ABCC1A3,
	UnloadSceneOp__ctor_mC1F956299122ACD2793E5D0A2AE8B3DD9A07383C,
	TextDataProvider_get_IgnoreFailures_m54B8CF389A2EF6D0C92E97D23541CD730CE45851,
	TextDataProvider_set_IgnoreFailures_m42340E49DD6BFBEE98EA6D24A0FB5B0673372A74,
	TextDataProvider_Convert_m2B991FF6B1CB9CD871439B6F5089BE5AE6DF2518,
	TextDataProvider_Provide_mF3742F8E9A17003FF5D555E4022419B9EE0F8989,
	TextDataProvider__ctor_m7AD4D795A1480E3CEC6DCDD478A37FD51E79812D,
	InternalOp_GetPercentComplete_m7AC5C9BEC338F6C89783E233E8C532057E62BD2A,
	InternalOp_Start_m2A6D3C16C95797FC3162CC981CFA28C4128E35E2,
	InternalOp_RequestOperation_completed_m587EACB3BF4B624F485E87D20E661A8F0192BADE,
	InternalOp__ctor_mA96AC18D8B782C4508EB024210E260A8E899753E,
	InternalOp_U3CStartU3Eb__6_0_mBDCB8E098F9284FC7C3A90C6461A8C107E159F4A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceLocationBase_get_InternalId_m446E746FAE3B40B1CDA9A588A319DDBDB1453ABB,
	ResourceLocationBase_get_ProviderId_mEE3324D2E4071C922810A050C10AFF5DDCA3EAA3,
	ResourceLocationBase_get_Dependencies_m054FEB15A19CB1591D9E11587725EAD00100F3EB,
	ResourceLocationBase_get_HasDependencies_m25C0F08130902EFEAEF8DDD99D75073BD7377272,
	ResourceLocationBase_get_Data_m97D221A2A5D2CBC3B22C0E748AFC5E0E5AE4E8F8,
	ResourceLocationBase_set_Data_m27679E141DAE40E2566189ABE8E951B9B2BB9E5C,
	ResourceLocationBase_get_PrimaryKey_mAA5E2116F41249FE94A19BFB0E102605B8941B24,
	ResourceLocationBase_set_PrimaryKey_m7EAFEAF682EE8771E9D4AB09C4C2A375A8ACEA48,
	ResourceLocationBase_get_DependencyHashCode_m1CF137823A47AE4C1EC0503AA257AB7CCDE88CEB,
	ResourceLocationBase_get_ResourceType_m8621BD5A50D22E8F3C5933811823DB63034A62FD,
	ResourceLocationBase_Hash_mF2BC6233AC5F1F90E1AC29823D2F0CFB639374D4,
	ResourceLocationBase_ToString_m828E1E56F19E2A35DF72ED842192DABE7386A9A0,
	ResourceLocationBase__ctor_m9F01B99ECB49EFF4B75BF41E169FBFEF707809AA,
	ResourceLocationBase_ComputeDependencyHash_mB8A54AB1782AFCE00F30B2C22D0048B5006DBF52,
	DiagnosticEvent_get_Graph_m21E2F78A724957B7AA6BFECBB68D2CED69881BF1_AdjustorThunk,
	DiagnosticEvent_get_ObjectId_m1CCBE7BC50003514247D99544A60BD22DF70596D_AdjustorThunk,
	DiagnosticEvent_get_DisplayName_mD05DA6038C714D3FE520B15F9278E3CF36778B97_AdjustorThunk,
	DiagnosticEvent_get_Dependencies_m5FC6794AD6E24152ABDB0506516B46C3E3538B24_AdjustorThunk,
	DiagnosticEvent_get_Stream_m2811009448086191841A0EBAC768BD8DEDCB17E7_AdjustorThunk,
	DiagnosticEvent_get_Frame_mE28A9CF94A945044C148F653F95C4142ECA221B1_AdjustorThunk,
	DiagnosticEvent_get_Value_mE9BC9F537B62C5668DEE8929569E4E46C24ACB76_AdjustorThunk,
	DiagnosticEvent__ctor_m6A631EBC9C9EA934424340F88116686A83511D9B_AdjustorThunk,
	DiagnosticEvent_Serialize_m6255901EC86D38FBAFDE9069AF340608B45205E0_AdjustorThunk,
	DiagnosticEvent_Deserialize_m1C6167E45D907FA54A3287E6791D6D10E2A6A6F6,
	DiagnosticEventCollector_FindOrCreateGlobalInstance_m5BD990B7652CB733C6A21BF644A7B81BE9E8817A,
	DiagnosticEventCollector_get_PlayerConnectionGuid_m6515FF5B59417D6493D238D80D2581C489D6C796,
	DiagnosticEventCollector_RegisterEventHandler_m198D2A3243FDE6B6C976EB504675E61A39E8654E,
	DiagnosticEventCollector_RegisterEventHandler_mD2C0D4CC5D9CD3E276309315AE6411E256EBC51C,
	DiagnosticEventCollector_UnregisterEventHandler_m9B73D49777D673D625F01FFA911F2B15316F135E,
	DiagnosticEventCollector_PostEvent_m4CFA734CD011BA23C0661A3F2BCE6FFB572C9CB3,
	DiagnosticEventCollector_Awake_mC7773B56D7B9412F5BA8905661D0A38255D75B94,
	DiagnosticEventCollector_Update_m0EA57F5DE2E327CF6682733E566A1179AFEB0FB0,
	DiagnosticEventCollector_OnApplicationQuit_m44BBC89DD2585D108F9D349A99DC1CC998B84DA1,
	DiagnosticEventCollector__ctor_m6EBDFC1BB47C875919F8A53247C417754DBFAA81,
	U3CU3Ec__cctor_m03501AE1E3ACCDF64A4518B76A3AEF49B8D6698E,
	U3CU3Ec__ctor_mBD797CCC9A8BE99DFB69CEF9DD7DDA9AB80B0D38,
	U3CU3Ec_U3CAwakeU3Eb__12_0_m20C38A15E2C4BFE5025F0D3A6E77C757EDC719F7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AsyncOperationHandle__ctor_m4A737468E34CBBCC7DDA85F5A0A5D461DB7F4913_AdjustorThunk,
	AsyncOperationHandle__ctor_m93AADC394EAFD7638E5B37B60CC52C229A8B74DC_AdjustorThunk,
	AsyncOperationHandle_Acquire_mC15C71C7BA8B95C49B8BCCD33CD2F7EB5B03BF7F_AdjustorThunk,
	AsyncOperationHandle_add_Completed_mBD4A54B7E0322F292C67A62A5461193758726B20_AdjustorThunk,
	AsyncOperationHandle_remove_Completed_mFF2A9DFC4D2E2A6D1302BFBC7EA49E01E3E14CEB_AdjustorThunk,
	NULL,
	AsyncOperationHandle_get_DebugName_m9263AE2FF7206453566D317443292E71686910FE_AdjustorThunk,
	AsyncOperationHandle_add_Destroyed_mCDA263102547EEC0D53DD5B2B7F55881EC896235_AdjustorThunk,
	AsyncOperationHandle_remove_Destroyed_m3BE893CECF7F7273A884984B6025867D7AA90FB2_AdjustorThunk,
	AsyncOperationHandle_GetDependencies_mD420A5EA523C940C0F90A216B971E8B6A060F71E_AdjustorThunk,
	AsyncOperationHandle_GetHashCode_m7764BEBFB86D7C0EB3D46D4AC116ADD56AEEAC68_AdjustorThunk,
	AsyncOperationHandle_get_InternalOp_m09CAB010CFD3205ADCB1128F3046132C538B8EFE_AdjustorThunk,
	AsyncOperationHandle_get_IsDone_m7F8233755EDB537A312B42BFAC1B4E95FDEB0B62_AdjustorThunk,
	AsyncOperationHandle_IsValid_mDDEEC690F9BFA3CFA09B8344947F1405EECA22F1_AdjustorThunk,
	AsyncOperationHandle_get_OperationException_mE66E9FDDCF99EF8B6D706A05F3156514D552C259_AdjustorThunk,
	AsyncOperationHandle_get_PercentComplete_m56F5B0016C1AE1681A5670F8F50F716F94C4B9C1_AdjustorThunk,
	AsyncOperationHandle_get_ReferenceCount_mC0B0856194FE7E39B581A60345938FCBF6FD8BA6_AdjustorThunk,
	AsyncOperationHandle_Release_m75B6AFC0710FF804ECD0CBC6E7EFB85EEF095F67_AdjustorThunk,
	AsyncOperationHandle_get_Result_m692BEC3C25DA9A5BFCD041C069C693B25F783DED_AdjustorThunk,
	AsyncOperationHandle_get_Status_m541A1E32905CA1F7092BFBB1299F939271FA57E7_AdjustorThunk,
	AsyncOperationHandle_get_Task_mE97DD2E15087A3EEAEA60F51C555BD7BC18C6F9F_AdjustorThunk,
	AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m902A963CD3B19733FB41AA0282DA4807D22668C8_AdjustorThunk,
	AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m49963C8BC42694191D9AF69FB63C1DEB8472BCB7_AdjustorThunk,
	AsyncOperationHandle_System_Collections_IEnumerator_Reset_mC3C2DA4955C33CDC282A3155FC1619EE5DE50810_AdjustorThunk,
	GroupOperation__ctor_m53C7DABD998F336EF512B5A7939EBCEFC35453EE,
	GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_get_Hash_m255C41D287D9D2F81B8CD36C843E072677509658,
	GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_set_Hash_m8BC643CCFAFFF998702423EC9EE41C29F01C48F9,
	GroupOperation_GetDependentOps_m65A13CA9F754AB7B61EDE32ACD44E96EF66A2EB8,
	GroupOperation_GetDependencies_m63B2EBBAE700A12C192B6624E18B5AECA68BAE54,
	GroupOperation_get_DebugName_m10F52CC27B3E235135A71BA5BE8FB11B78DC603F,
	GroupOperation_Execute_mF71134E1DCBA889EF1ADB17EC1E16A6DAEEF4A1F,
	GroupOperation_CompleteIfDependenciesComplete_m81CE72A80A868B8825BFBEC959976741EC5C399E,
	GroupOperation_Destroy_m5B8C2CA59E96C598672FA4FEF25D20B2CFA74BFC,
	GroupOperation_get_Progress_m330078EFB12B5B49E59B66D34CF8D619A23553E2,
	GroupOperation_Init_m29EC3388CA40669CB8D9E6CF7B06E1C35DCD2458,
	GroupOperation_OnOperationCompleted_mCEAB8FA43C2DBEB9C72FD6A2A6412A76AD46DD58,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[564] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	163,
	14,
	26,
	28,
	10,
	10,
	26,
	26,
	14,
	26,
	14,
	14,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	2272,
	105,
	28,
	41,
	2273,
	-1,
	-1,
	2274,
	26,
	26,
	26,
	-1,
	62,
	30,
	30,
	10,
	-1,
	-1,
	2275,
	2275,
	34,
	-1,
	2276,
	2277,
	-1,
	-1,
	-1,
	2278,
	2279,
	2280,
	2281,
	23,
	459,
	340,
	23,
	3,
	26,
	2282,
	10,
	10,
	14,
	14,
	14,
	2283,
	-1,
	-1,
	-1,
	2284,
	26,
	14,
	1799,
	23,
	737,
	23,
	23,
	-1,
	-1,
	-1,
	340,
	89,
	26,
	26,
	0,
	163,
	3,
	23,
	26,
	27,
	111,
	14,
	26,
	26,
	23,
	26,
	27,
	111,
	14,
	14,
	502,
	3,
	23,
	2285,
	2286,
	23,
	49,
	1772,
	23,
	340,
	23,
	23,
	2286,
	737,
	14,
	23,
	90,
	2287,
	14,
	2288,
	58,
	62,
	58,
	62,
	23,
	344,
	14,
	26,
	58,
	62,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	14,
	14,
	26,
	89,
	31,
	14,
	2289,
	14,
	14,
	-1,
	2273,
	368,
	114,
	114,
	1,
	-1,
	1,
	-1,
	-1,
	14,
	14,
	26,
	10,
	32,
	10,
	32,
	89,
	31,
	10,
	32,
	10,
	32,
	14,
	26,
	184,
	210,
	2290,
	23,
	28,
	737,
	14,
	2291,
	23,
	26,
	26,
	23,
	23,
	26,
	2291,
	28,
	27,
	23,
	2291,
	23,
	2291,
	23,
	0,
	2291,
	26,
	737,
	23,
	1552,
	1820,
	14,
	89,
	89,
	459,
	2292,
	-1,
	2293,
	27,
	27,
	14,
	14,
	14,
	14,
	10,
	-1,
	26,
	26,
	-1,
	14,
	28,
	90,
	2291,
	27,
	10,
	1799,
	2281,
	23,
	14,
	10,
	9,
	2278,
	2279,
	2293,
	27,
	23,
	105,
	23,
	2291,
	27,
	23,
	2291,
	26,
	737,
	23,
	14,
	90,
	90,
	14,
	27,
	28,
	2291,
	2287,
	10,
	23,
	26,
	23,
	23,
	23,
	89,
	2278,
	2279,
	23,
	26,
	2294,
	26,
	14,
	23,
	2295,
	1178,
	23,
	737,
	340,
	2296,
	23,
	26,
	737,
	23,
	89,
	31,
	105,
	2291,
	23,
	737,
	2297,
	26,
	23,
	26,
	2290,
	14,
	14,
	14,
	112,
	10,
	89,
	14,
	14,
	14,
	14,
	14,
	14,
	89,
	14,
	26,
	14,
	26,
	10,
	14,
	112,
	14,
	833,
	23,
	14,
	10,
	14,
	14,
	10,
	10,
	10,
	2298,
	14,
	2299,
	4,
	435,
	2300,
	26,
	26,
	2301,
	23,
	23,
	23,
	23,
	3,
	23,
	2301,
	10,
	32,
	14,
	14,
	10,
	14,
	23,
	23,
	10,
	737,
	10,
	14,
	89,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	14,
	2302,
	2282,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	130,
	2282,
	26,
	26,
	-1,
	14,
	26,
	26,
	26,
	10,
	14,
	89,
	89,
	14,
	737,
	10,
	23,
	14,
	10,
	14,
	14,
	89,
	23,
	23,
	10,
	32,
	14,
	26,
	14,
	23,
	23,
	23,
	737,
	26,
	2275,
	2303,
	10,
	14,
	10,
	26,
	-1,
	26,
	-1,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[31] = 
{
	{ 0x02000002, { 0, 20 } },
	{ 0x02000003, { 20, 23 } },
	{ 0x02000005, { 43, 23 } },
	{ 0x02000006, { 66, 15 } },
	{ 0x0200000A, { 116, 5 } },
	{ 0x0200000C, { 121, 8 } },
	{ 0x0200001A, { 129, 9 } },
	{ 0x0200001B, { 138, 5 } },
	{ 0x02000041, { 156, 50 } },
	{ 0x02000042, { 206, 2 } },
	{ 0x02000043, { 208, 22 } },
	{ 0x02000048, { 232, 10 } },
	{ 0x0600004E, { 81, 2 } },
	{ 0x0600004F, { 83, 2 } },
	{ 0x06000054, { 85, 1 } },
	{ 0x06000059, { 86, 4 } },
	{ 0x0600005A, { 90, 4 } },
	{ 0x0600005E, { 94, 3 } },
	{ 0x06000061, { 97, 9 } },
	{ 0x06000062, { 106, 6 } },
	{ 0x06000063, { 112, 4 } },
	{ 0x060000C8, { 143, 1 } },
	{ 0x060000CE, { 144, 2 } },
	{ 0x060000D0, { 146, 2 } },
	{ 0x060000D1, { 148, 2 } },
	{ 0x06000103, { 150, 4 } },
	{ 0x0600010C, { 154, 1 } },
	{ 0x0600010F, { 155, 1 } },
	{ 0x060001FC, { 230, 2 } },
	{ 0x0600022E, { 242, 1 } },
	{ 0x06000230, { 243, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[247] = 
{
	{ (Il2CppRGCTXDataType)3, 33059 },
	{ (Il2CppRGCTXDataType)3, 33060 },
	{ (Il2CppRGCTXDataType)2, 22790 },
	{ (Il2CppRGCTXDataType)3, 33061 },
	{ (Il2CppRGCTXDataType)3, 33062 },
	{ (Il2CppRGCTXDataType)3, 33063 },
	{ (Il2CppRGCTXDataType)3, 33064 },
	{ (Il2CppRGCTXDataType)3, 33065 },
	{ (Il2CppRGCTXDataType)3, 33066 },
	{ (Il2CppRGCTXDataType)3, 33067 },
	{ (Il2CppRGCTXDataType)3, 33068 },
	{ (Il2CppRGCTXDataType)3, 33069 },
	{ (Il2CppRGCTXDataType)3, 33070 },
	{ (Il2CppRGCTXDataType)2, 17513 },
	{ (Il2CppRGCTXDataType)3, 33071 },
	{ (Il2CppRGCTXDataType)3, 33072 },
	{ (Il2CppRGCTXDataType)2, 17514 },
	{ (Il2CppRGCTXDataType)3, 33073 },
	{ (Il2CppRGCTXDataType)2, 17515 },
	{ (Il2CppRGCTXDataType)3, 33074 },
	{ (Il2CppRGCTXDataType)2, 17520 },
	{ (Il2CppRGCTXDataType)3, 33075 },
	{ (Il2CppRGCTXDataType)3, 33076 },
	{ (Il2CppRGCTXDataType)3, 33077 },
	{ (Il2CppRGCTXDataType)3, 33078 },
	{ (Il2CppRGCTXDataType)3, 33079 },
	{ (Il2CppRGCTXDataType)3, 33080 },
	{ (Il2CppRGCTXDataType)3, 33081 },
	{ (Il2CppRGCTXDataType)2, 22444 },
	{ (Il2CppRGCTXDataType)3, 33082 },
	{ (Il2CppRGCTXDataType)3, 33083 },
	{ (Il2CppRGCTXDataType)3, 33084 },
	{ (Il2CppRGCTXDataType)3, 33085 },
	{ (Il2CppRGCTXDataType)2, 22791 },
	{ (Il2CppRGCTXDataType)3, 33086 },
	{ (Il2CppRGCTXDataType)3, 33087 },
	{ (Il2CppRGCTXDataType)3, 33088 },
	{ (Il2CppRGCTXDataType)3, 33089 },
	{ (Il2CppRGCTXDataType)3, 33090 },
	{ (Il2CppRGCTXDataType)3, 33091 },
	{ (Il2CppRGCTXDataType)3, 33092 },
	{ (Il2CppRGCTXDataType)2, 22792 },
	{ (Il2CppRGCTXDataType)3, 33093 },
	{ (Il2CppRGCTXDataType)3, 33094 },
	{ (Il2CppRGCTXDataType)2, 17531 },
	{ (Il2CppRGCTXDataType)3, 33095 },
	{ (Il2CppRGCTXDataType)2, 22793 },
	{ (Il2CppRGCTXDataType)3, 33096 },
	{ (Il2CppRGCTXDataType)1, 17532 },
	{ (Il2CppRGCTXDataType)1, 17534 },
	{ (Il2CppRGCTXDataType)3, 33097 },
	{ (Il2CppRGCTXDataType)3, 33098 },
	{ (Il2CppRGCTXDataType)2, 17533 },
	{ (Il2CppRGCTXDataType)3, 33099 },
	{ (Il2CppRGCTXDataType)3, 33100 },
	{ (Il2CppRGCTXDataType)3, 33101 },
	{ (Il2CppRGCTXDataType)3, 33102 },
	{ (Il2CppRGCTXDataType)3, 33103 },
	{ (Il2CppRGCTXDataType)3, 33104 },
	{ (Il2CppRGCTXDataType)3, 33105 },
	{ (Il2CppRGCTXDataType)3, 33106 },
	{ (Il2CppRGCTXDataType)3, 33107 },
	{ (Il2CppRGCTXDataType)3, 33108 },
	{ (Il2CppRGCTXDataType)3, 33109 },
	{ (Il2CppRGCTXDataType)3, 33110 },
	{ (Il2CppRGCTXDataType)3, 33111 },
	{ (Il2CppRGCTXDataType)3, 33112 },
	{ (Il2CppRGCTXDataType)2, 17542 },
	{ (Il2CppRGCTXDataType)3, 33113 },
	{ (Il2CppRGCTXDataType)2, 22794 },
	{ (Il2CppRGCTXDataType)3, 33114 },
	{ (Il2CppRGCTXDataType)1, 17543 },
	{ (Il2CppRGCTXDataType)3, 33115 },
	{ (Il2CppRGCTXDataType)3, 33116 },
	{ (Il2CppRGCTXDataType)3, 33117 },
	{ (Il2CppRGCTXDataType)3, 33118 },
	{ (Il2CppRGCTXDataType)3, 33119 },
	{ (Il2CppRGCTXDataType)3, 33120 },
	{ (Il2CppRGCTXDataType)3, 33121 },
	{ (Il2CppRGCTXDataType)3, 33122 },
	{ (Il2CppRGCTXDataType)3, 33123 },
	{ (Il2CppRGCTXDataType)1, 17557 },
	{ (Il2CppRGCTXDataType)3, 33124 },
	{ (Il2CppRGCTXDataType)3, 33125 },
	{ (Il2CppRGCTXDataType)3, 33126 },
	{ (Il2CppRGCTXDataType)2, 18 },
	{ (Il2CppRGCTXDataType)1, 19 },
	{ (Il2CppRGCTXDataType)3, 33127 },
	{ (Il2CppRGCTXDataType)3, 33128 },
	{ (Il2CppRGCTXDataType)3, 33129 },
	{ (Il2CppRGCTXDataType)1, 21 },
	{ (Il2CppRGCTXDataType)3, 33130 },
	{ (Il2CppRGCTXDataType)3, 33131 },
	{ (Il2CppRGCTXDataType)3, 33132 },
	{ (Il2CppRGCTXDataType)3, 33133 },
	{ (Il2CppRGCTXDataType)3, 33134 },
	{ (Il2CppRGCTXDataType)2, 22796 },
	{ (Il2CppRGCTXDataType)2, 25 },
	{ (Il2CppRGCTXDataType)3, 33135 },
	{ (Il2CppRGCTXDataType)3, 33136 },
	{ (Il2CppRGCTXDataType)3, 33137 },
	{ (Il2CppRGCTXDataType)1, 26 },
	{ (Il2CppRGCTXDataType)3, 33138 },
	{ (Il2CppRGCTXDataType)2, 22797 },
	{ (Il2CppRGCTXDataType)3, 33139 },
	{ (Il2CppRGCTXDataType)3, 33140 },
	{ (Il2CppRGCTXDataType)1, 30 },
	{ (Il2CppRGCTXDataType)3, 33141 },
	{ (Il2CppRGCTXDataType)3, 33142 },
	{ (Il2CppRGCTXDataType)3, 33143 },
	{ (Il2CppRGCTXDataType)2, 17566 },
	{ (Il2CppRGCTXDataType)3, 33144 },
	{ (Il2CppRGCTXDataType)2, 33 },
	{ (Il2CppRGCTXDataType)3, 33145 },
	{ (Il2CppRGCTXDataType)3, 33146 },
	{ (Il2CppRGCTXDataType)3, 33147 },
	{ (Il2CppRGCTXDataType)3, 33148 },
	{ (Il2CppRGCTXDataType)2, 17596 },
	{ (Il2CppRGCTXDataType)3, 33149 },
	{ (Il2CppRGCTXDataType)3, 33150 },
	{ (Il2CppRGCTXDataType)3, 33151 },
	{ (Il2CppRGCTXDataType)2, 41 },
	{ (Il2CppRGCTXDataType)3, 33152 },
	{ (Il2CppRGCTXDataType)3, 33153 },
	{ (Il2CppRGCTXDataType)2, 40 },
	{ (Il2CppRGCTXDataType)3, 33154 },
	{ (Il2CppRGCTXDataType)3, 33155 },
	{ (Il2CppRGCTXDataType)3, 33156 },
	{ (Il2CppRGCTXDataType)3, 33157 },
	{ (Il2CppRGCTXDataType)3, 33158 },
	{ (Il2CppRGCTXDataType)3, 33159 },
	{ (Il2CppRGCTXDataType)3, 33160 },
	{ (Il2CppRGCTXDataType)2, 50 },
	{ (Il2CppRGCTXDataType)3, 33161 },
	{ (Il2CppRGCTXDataType)2, 22798 },
	{ (Il2CppRGCTXDataType)3, 33162 },
	{ (Il2CppRGCTXDataType)3, 33163 },
	{ (Il2CppRGCTXDataType)3, 33164 },
	{ (Il2CppRGCTXDataType)2, 22799 },
	{ (Il2CppRGCTXDataType)2, 22800 },
	{ (Il2CppRGCTXDataType)3, 33165 },
	{ (Il2CppRGCTXDataType)3, 33166 },
	{ (Il2CppRGCTXDataType)3, 33167 },
	{ (Il2CppRGCTXDataType)2, 17646 },
	{ (Il2CppRGCTXDataType)1, 17650 },
	{ (Il2CppRGCTXDataType)2, 17650 },
	{ (Il2CppRGCTXDataType)1, 17651 },
	{ (Il2CppRGCTXDataType)2, 17651 },
	{ (Il2CppRGCTXDataType)1, 22801 },
	{ (Il2CppRGCTXDataType)1, 22802 },
	{ (Il2CppRGCTXDataType)3, 33168 },
	{ (Il2CppRGCTXDataType)3, 33169 },
	{ (Il2CppRGCTXDataType)3, 33170 },
	{ (Il2CppRGCTXDataType)3, 33171 },
	{ (Il2CppRGCTXDataType)3, 33172 },
	{ (Il2CppRGCTXDataType)3, 33173 },
	{ (Il2CppRGCTXDataType)3, 33174 },
	{ (Il2CppRGCTXDataType)3, 33175 },
	{ (Il2CppRGCTXDataType)3, 33176 },
	{ (Il2CppRGCTXDataType)2, 17722 },
	{ (Il2CppRGCTXDataType)3, 33177 },
	{ (Il2CppRGCTXDataType)3, 33178 },
	{ (Il2CppRGCTXDataType)2, 17722 },
	{ (Il2CppRGCTXDataType)3, 33179 },
	{ (Il2CppRGCTXDataType)3, 33180 },
	{ (Il2CppRGCTXDataType)2, 79 },
	{ (Il2CppRGCTXDataType)3, 33181 },
	{ (Il2CppRGCTXDataType)3, 33182 },
	{ (Il2CppRGCTXDataType)3, 33183 },
	{ (Il2CppRGCTXDataType)3, 33184 },
	{ (Il2CppRGCTXDataType)3, 33185 },
	{ (Il2CppRGCTXDataType)3, 33186 },
	{ (Il2CppRGCTXDataType)2, 22803 },
	{ (Il2CppRGCTXDataType)3, 33187 },
	{ (Il2CppRGCTXDataType)3, 33188 },
	{ (Il2CppRGCTXDataType)3, 33189 },
	{ (Il2CppRGCTXDataType)2, 80 },
	{ (Il2CppRGCTXDataType)3, 33190 },
	{ (Il2CppRGCTXDataType)3, 33191 },
	{ (Il2CppRGCTXDataType)2, 22804 },
	{ (Il2CppRGCTXDataType)3, 33192 },
	{ (Il2CppRGCTXDataType)3, 33193 },
	{ (Il2CppRGCTXDataType)3, 33194 },
	{ (Il2CppRGCTXDataType)3, 33195 },
	{ (Il2CppRGCTXDataType)3, 33196 },
	{ (Il2CppRGCTXDataType)3, 33197 },
	{ (Il2CppRGCTXDataType)3, 33198 },
	{ (Il2CppRGCTXDataType)3, 33199 },
	{ (Il2CppRGCTXDataType)3, 33200 },
	{ (Il2CppRGCTXDataType)3, 33201 },
	{ (Il2CppRGCTXDataType)3, 33202 },
	{ (Il2CppRGCTXDataType)3, 33203 },
	{ (Il2CppRGCTXDataType)3, 33204 },
	{ (Il2CppRGCTXDataType)3, 33205 },
	{ (Il2CppRGCTXDataType)3, 33206 },
	{ (Il2CppRGCTXDataType)3, 33207 },
	{ (Il2CppRGCTXDataType)3, 33208 },
	{ (Il2CppRGCTXDataType)3, 33209 },
	{ (Il2CppRGCTXDataType)3, 33210 },
	{ (Il2CppRGCTXDataType)3, 33211 },
	{ (Il2CppRGCTXDataType)3, 33212 },
	{ (Il2CppRGCTXDataType)3, 33213 },
	{ (Il2CppRGCTXDataType)3, 33214 },
	{ (Il2CppRGCTXDataType)1, 80 },
	{ (Il2CppRGCTXDataType)3, 33215 },
	{ (Il2CppRGCTXDataType)3, 33216 },
	{ (Il2CppRGCTXDataType)2, 81 },
	{ (Il2CppRGCTXDataType)3, 33217 },
	{ (Il2CppRGCTXDataType)3, 33218 },
	{ (Il2CppRGCTXDataType)2, 17735 },
	{ (Il2CppRGCTXDataType)3, 33219 },
	{ (Il2CppRGCTXDataType)3, 33220 },
	{ (Il2CppRGCTXDataType)3, 33221 },
	{ (Il2CppRGCTXDataType)3, 33222 },
	{ (Il2CppRGCTXDataType)3, 33223 },
	{ (Il2CppRGCTXDataType)3, 33224 },
	{ (Il2CppRGCTXDataType)3, 33225 },
	{ (Il2CppRGCTXDataType)3, 33226 },
	{ (Il2CppRGCTXDataType)3, 33227 },
	{ (Il2CppRGCTXDataType)3, 33228 },
	{ (Il2CppRGCTXDataType)3, 33229 },
	{ (Il2CppRGCTXDataType)3, 33230 },
	{ (Il2CppRGCTXDataType)3, 33231 },
	{ (Il2CppRGCTXDataType)3, 33232 },
	{ (Il2CppRGCTXDataType)3, 33233 },
	{ (Il2CppRGCTXDataType)3, 33234 },
	{ (Il2CppRGCTXDataType)3, 33235 },
	{ (Il2CppRGCTXDataType)3, 33236 },
	{ (Il2CppRGCTXDataType)2, 17734 },
	{ (Il2CppRGCTXDataType)3, 33237 },
	{ (Il2CppRGCTXDataType)2, 17741 },
	{ (Il2CppRGCTXDataType)3, 33238 },
	{ (Il2CppRGCTXDataType)3, 33239 },
	{ (Il2CppRGCTXDataType)2, 17752 },
	{ (Il2CppRGCTXDataType)1, 17753 },
	{ (Il2CppRGCTXDataType)3, 33240 },
	{ (Il2CppRGCTXDataType)2, 17753 },
	{ (Il2CppRGCTXDataType)3, 33241 },
	{ (Il2CppRGCTXDataType)3, 33242 },
	{ (Il2CppRGCTXDataType)3, 33243 },
	{ (Il2CppRGCTXDataType)3, 33244 },
	{ (Il2CppRGCTXDataType)3, 33245 },
	{ (Il2CppRGCTXDataType)2, 17754 },
	{ (Il2CppRGCTXDataType)2, 83 },
	{ (Il2CppRGCTXDataType)3, 33246 },
	{ (Il2CppRGCTXDataType)2, 84 },
	{ (Il2CppRGCTXDataType)1, 84 },
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnity_ResourceManager;
extern const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule;
const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule = 
{
	"Unity.ResourceManager.dll",
	564,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	31,
	s_rgctxIndices,
	247,
	s_rgctxValues,
	&g_DebuggerMetadataRegistrationUnity_ResourceManager,
};
