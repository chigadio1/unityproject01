﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412 (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937 (void);
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018 (void);
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E (void);
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31 (void);
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1 (void);
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6 (void);
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93 (void);
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6 (void);
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460 (void);
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B (void);
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268 (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153 (void);
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477 (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91 (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E (void);
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265 (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4 (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465 (void);
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34 (void);
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB (void);
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252 (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F (void);
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058 (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218 (void);
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D (void);
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63 (void);
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E (void);
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF (void);
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E (void);
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598 (void);
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1 (void);
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129 (void);
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD (void);
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 (void);
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000031 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000034 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000003F TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000041 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000043 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000044 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000045 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000047 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000048 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000049 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004A System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004B System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004C System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000004D System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000004E System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000004F TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000050 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000051 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000052 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000053 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000054 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000055 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000056 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000057 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000058 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000059 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000005B System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000005C System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000005D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005F System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000060 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000061 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000062 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000063 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000064 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000065 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000066 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000067 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000068 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000069 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006A System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000006B System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000006C System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000006D System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000006E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000006F System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000070 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000071 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000072 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000073 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000074 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000075 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000076 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000077 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000078 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000079 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000007A System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000007B System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000007C TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000007D System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000007E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000007F System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x00000080 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000081 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000082 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000083 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000084 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000085 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000086 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000087 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000088 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x00000089 System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x0000008A System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x0000008B TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000008C System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000008D System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x0000008E System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000008F System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000090 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x00000091 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x00000092 System.Boolean System.Linq.Enumerable_<ConcatIterator>d__59`1::MoveNext()
// 0x00000093 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally1()
// 0x00000094 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally2()
// 0x00000095 TSource System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000096 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x00000097 System.Object System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x00000098 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000099 System.Collections.IEnumerator System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009A System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x0000009B System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x0000009C System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x0000009D System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x0000009E TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000009F System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x000000A0 System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x000000A1 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A2 System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A3 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x000000A4 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000A5 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000A6 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000A7 System.Void System.Linq.Set`1::Resize()
// 0x000000A8 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000A9 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000AA System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000AB System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AC System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000AD System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000AE System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000AF System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000B0 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000B1 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000B2 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000B3 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000B4 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000B5 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000B6 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000B7 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000B8 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000B9 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000BA System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000BB System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000BC System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000BD System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000BE TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000BF System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32*,System.Int32)
extern void BitHelper__ctor_m2770BB414919277B2CF522840B54819F5082CD80 (void);
// 0x000000C0 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32[],System.Int32)
extern void BitHelper__ctor_m7A8E43BE1D2A4ED086E708B6FFE693322FC9D2EB (void);
// 0x000000C1 System.Void System.Collections.Generic.BitHelper::MarkBit(System.Int32)
extern void BitHelper_MarkBit_m1C6D787021BEA9D02DCA0762C09E5D443E04A86B (void);
// 0x000000C2 System.Boolean System.Collections.Generic.BitHelper::IsMarked(System.Int32)
extern void BitHelper_IsMarked_m6036A81F50D820045D3F62E52D57098A332AB608 (void);
// 0x000000C3 System.Int32 System.Collections.Generic.BitHelper::ToIntArrayLength(System.Int32)
extern void BitHelper_ToIntArrayLength_m32A0B1B014CB81891165AC325514784171C8E7B3 (void);
// 0x000000C4 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000C5 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000C6 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000C7 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000C8 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000C9 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000CA System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000CB System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000CC System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000CD System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000CE System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000CF System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000D0 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D1 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000D2 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000D3 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000D4 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000D5 System.Void System.Collections.Generic.HashSet`1::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000D6 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000D7 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000D8 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000D9 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000DA System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000DB System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000DC System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000DD System.Void System.Collections.Generic.HashSet`1::IntersectWithHashSetWithSameEC(System.Collections.Generic.HashSet`1<T>)
// 0x000000DE System.Void System.Collections.Generic.HashSet`1::IntersectWithEnumerable(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000DF System.Int32 System.Collections.Generic.HashSet`1::InternalIndexOf(T)
// 0x000000E0 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000E1 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000E2 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000E3 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000E4 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000E5 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000E6 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000E7 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
// 0x000000E8 System.Void System.Collections.Generic.ICollectionDebugView`1::.ctor(System.Collections.Generic.ICollection`1<T>)
// 0x000000E9 T[] System.Collections.Generic.ICollectionDebugView`1::get_Items()
static Il2CppMethodPointer s_methodPointers[233] = 
{
	SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412,
	AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57,
	AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937,
	AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018,
	AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E,
	AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31,
	AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1,
	AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6,
	AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93,
	AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6,
	AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460,
	AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45,
	AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B,
	AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C,
	AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268,
	AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF,
	AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153,
	AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477,
	AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91,
	AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E,
	AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265,
	AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4,
	AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502,
	AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1,
	AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465,
	AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34,
	AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB,
	AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252,
	AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F,
	AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058,
	AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218,
	AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D,
	AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D,
	AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63,
	AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD,
	AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E,
	AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF,
	AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB,
	AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E,
	AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598,
	AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1,
	AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679,
	AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129,
	AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A,
	AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD,
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BitHelper__ctor_m2770BB414919277B2CF522840B54819F5082CD80,
	BitHelper__ctor_m7A8E43BE1D2A4ED086E708B6FFE693322FC9D2EB,
	BitHelper_MarkBit_m1C6D787021BEA9D02DCA0762C09E5D443E04A86B,
	BitHelper_IsMarked_m6036A81F50D820045D3F62E52D57098A332AB608,
	BitHelper_ToIntArrayLength_m32A0B1B014CB81891165AC325514784171C8E7B3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[233] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	934,
	27,
	37,
	209,
	209,
	3,
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	64,
	130,
	32,
	30,
	21,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[58] = 
{
	{ 0x02000008, { 86, 4 } },
	{ 0x02000009, { 90, 9 } },
	{ 0x0200000A, { 101, 7 } },
	{ 0x0200000B, { 110, 10 } },
	{ 0x0200000C, { 122, 11 } },
	{ 0x0200000D, { 136, 9 } },
	{ 0x0200000E, { 148, 12 } },
	{ 0x0200000F, { 163, 1 } },
	{ 0x02000010, { 164, 2 } },
	{ 0x02000011, { 166, 12 } },
	{ 0x02000012, { 178, 8 } },
	{ 0x02000013, { 186, 9 } },
	{ 0x02000014, { 195, 11 } },
	{ 0x02000015, { 206, 2 } },
	{ 0x02000017, { 208, 8 } },
	{ 0x02000019, { 216, 3 } },
	{ 0x0200001A, { 219, 5 } },
	{ 0x0200001B, { 224, 7 } },
	{ 0x0200001C, { 231, 3 } },
	{ 0x0200001D, { 234, 7 } },
	{ 0x0200001E, { 241, 4 } },
	{ 0x02000020, { 245, 34 } },
	{ 0x02000022, { 279, 2 } },
	{ 0x02000023, { 281, 2 } },
	{ 0x06000032, { 0, 10 } },
	{ 0x06000033, { 10, 10 } },
	{ 0x06000034, { 20, 5 } },
	{ 0x06000035, { 25, 5 } },
	{ 0x06000036, { 30, 1 } },
	{ 0x06000037, { 31, 2 } },
	{ 0x06000038, { 33, 1 } },
	{ 0x06000039, { 34, 2 } },
	{ 0x0600003A, { 36, 2 } },
	{ 0x0600003B, { 38, 1 } },
	{ 0x0600003C, { 39, 2 } },
	{ 0x0600003D, { 41, 1 } },
	{ 0x0600003E, { 42, 2 } },
	{ 0x0600003F, { 44, 3 } },
	{ 0x06000040, { 47, 2 } },
	{ 0x06000041, { 49, 4 } },
	{ 0x06000042, { 53, 4 } },
	{ 0x06000043, { 57, 3 } },
	{ 0x06000044, { 60, 3 } },
	{ 0x06000045, { 63, 3 } },
	{ 0x06000046, { 66, 1 } },
	{ 0x06000047, { 67, 1 } },
	{ 0x06000048, { 68, 3 } },
	{ 0x06000049, { 71, 3 } },
	{ 0x0600004A, { 74, 2 } },
	{ 0x0600004B, { 76, 3 } },
	{ 0x0600004C, { 79, 2 } },
	{ 0x0600004D, { 81, 5 } },
	{ 0x0600005D, { 99, 2 } },
	{ 0x06000062, { 108, 2 } },
	{ 0x06000067, { 120, 2 } },
	{ 0x0600006D, { 133, 3 } },
	{ 0x06000072, { 145, 3 } },
	{ 0x06000077, { 160, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[283] = 
{
	{ (Il2CppRGCTXDataType)2, 22685 },
	{ (Il2CppRGCTXDataType)3, 32751 },
	{ (Il2CppRGCTXDataType)2, 22686 },
	{ (Il2CppRGCTXDataType)2, 22687 },
	{ (Il2CppRGCTXDataType)3, 32752 },
	{ (Il2CppRGCTXDataType)2, 22688 },
	{ (Il2CppRGCTXDataType)2, 22689 },
	{ (Il2CppRGCTXDataType)3, 32753 },
	{ (Il2CppRGCTXDataType)2, 22690 },
	{ (Il2CppRGCTXDataType)3, 32754 },
	{ (Il2CppRGCTXDataType)2, 22691 },
	{ (Il2CppRGCTXDataType)3, 32755 },
	{ (Il2CppRGCTXDataType)2, 22692 },
	{ (Il2CppRGCTXDataType)2, 22693 },
	{ (Il2CppRGCTXDataType)3, 32756 },
	{ (Il2CppRGCTXDataType)2, 22694 },
	{ (Il2CppRGCTXDataType)2, 22695 },
	{ (Il2CppRGCTXDataType)3, 32757 },
	{ (Il2CppRGCTXDataType)2, 22696 },
	{ (Il2CppRGCTXDataType)3, 32758 },
	{ (Il2CppRGCTXDataType)2, 22697 },
	{ (Il2CppRGCTXDataType)3, 32759 },
	{ (Il2CppRGCTXDataType)3, 32760 },
	{ (Il2CppRGCTXDataType)2, 12531 },
	{ (Il2CppRGCTXDataType)3, 32761 },
	{ (Il2CppRGCTXDataType)2, 22698 },
	{ (Il2CppRGCTXDataType)3, 32762 },
	{ (Il2CppRGCTXDataType)3, 32763 },
	{ (Il2CppRGCTXDataType)2, 12538 },
	{ (Il2CppRGCTXDataType)3, 32764 },
	{ (Il2CppRGCTXDataType)3, 32765 },
	{ (Il2CppRGCTXDataType)2, 22699 },
	{ (Il2CppRGCTXDataType)3, 32766 },
	{ (Il2CppRGCTXDataType)3, 32767 },
	{ (Il2CppRGCTXDataType)2, 22700 },
	{ (Il2CppRGCTXDataType)3, 32768 },
	{ (Il2CppRGCTXDataType)2, 22701 },
	{ (Il2CppRGCTXDataType)3, 32769 },
	{ (Il2CppRGCTXDataType)3, 32770 },
	{ (Il2CppRGCTXDataType)2, 22702 },
	{ (Il2CppRGCTXDataType)3, 32771 },
	{ (Il2CppRGCTXDataType)3, 32772 },
	{ (Il2CppRGCTXDataType)2, 22703 },
	{ (Il2CppRGCTXDataType)3, 32773 },
	{ (Il2CppRGCTXDataType)2, 22704 },
	{ (Il2CppRGCTXDataType)3, 32774 },
	{ (Il2CppRGCTXDataType)3, 32775 },
	{ (Il2CppRGCTXDataType)2, 12572 },
	{ (Il2CppRGCTXDataType)3, 32776 },
	{ (Il2CppRGCTXDataType)2, 22705 },
	{ (Il2CppRGCTXDataType)2, 22706 },
	{ (Il2CppRGCTXDataType)2, 12573 },
	{ (Il2CppRGCTXDataType)2, 22707 },
	{ (Il2CppRGCTXDataType)2, 22708 },
	{ (Il2CppRGCTXDataType)2, 22709 },
	{ (Il2CppRGCTXDataType)2, 12575 },
	{ (Il2CppRGCTXDataType)2, 22710 },
	{ (Il2CppRGCTXDataType)2, 12577 },
	{ (Il2CppRGCTXDataType)2, 22711 },
	{ (Il2CppRGCTXDataType)3, 32777 },
	{ (Il2CppRGCTXDataType)2, 12580 },
	{ (Il2CppRGCTXDataType)2, 22712 },
	{ (Il2CppRGCTXDataType)3, 32778 },
	{ (Il2CppRGCTXDataType)2, 22713 },
	{ (Il2CppRGCTXDataType)2, 12583 },
	{ (Il2CppRGCTXDataType)2, 22714 },
	{ (Il2CppRGCTXDataType)2, 22715 },
	{ (Il2CppRGCTXDataType)2, 12587 },
	{ (Il2CppRGCTXDataType)2, 12589 },
	{ (Il2CppRGCTXDataType)2, 22716 },
	{ (Il2CppRGCTXDataType)3, 32779 },
	{ (Il2CppRGCTXDataType)2, 12592 },
	{ (Il2CppRGCTXDataType)2, 22717 },
	{ (Il2CppRGCTXDataType)3, 32780 },
	{ (Il2CppRGCTXDataType)2, 22718 },
	{ (Il2CppRGCTXDataType)2, 12595 },
	{ (Il2CppRGCTXDataType)2, 12597 },
	{ (Il2CppRGCTXDataType)2, 22719 },
	{ (Il2CppRGCTXDataType)3, 32781 },
	{ (Il2CppRGCTXDataType)2, 22720 },
	{ (Il2CppRGCTXDataType)3, 32782 },
	{ (Il2CppRGCTXDataType)3, 32783 },
	{ (Il2CppRGCTXDataType)2, 22721 },
	{ (Il2CppRGCTXDataType)2, 12602 },
	{ (Il2CppRGCTXDataType)2, 22722 },
	{ (Il2CppRGCTXDataType)2, 12604 },
	{ (Il2CppRGCTXDataType)3, 32784 },
	{ (Il2CppRGCTXDataType)3, 32785 },
	{ (Il2CppRGCTXDataType)2, 12607 },
	{ (Il2CppRGCTXDataType)3, 32786 },
	{ (Il2CppRGCTXDataType)3, 32787 },
	{ (Il2CppRGCTXDataType)2, 12619 },
	{ (Il2CppRGCTXDataType)2, 22723 },
	{ (Il2CppRGCTXDataType)3, 32788 },
	{ (Il2CppRGCTXDataType)3, 32789 },
	{ (Il2CppRGCTXDataType)2, 12621 },
	{ (Il2CppRGCTXDataType)2, 22408 },
	{ (Il2CppRGCTXDataType)3, 32790 },
	{ (Il2CppRGCTXDataType)3, 32791 },
	{ (Il2CppRGCTXDataType)2, 22724 },
	{ (Il2CppRGCTXDataType)3, 32792 },
	{ (Il2CppRGCTXDataType)3, 32793 },
	{ (Il2CppRGCTXDataType)2, 12631 },
	{ (Il2CppRGCTXDataType)2, 22725 },
	{ (Il2CppRGCTXDataType)3, 32794 },
	{ (Il2CppRGCTXDataType)3, 32795 },
	{ (Il2CppRGCTXDataType)3, 31444 },
	{ (Il2CppRGCTXDataType)3, 32796 },
	{ (Il2CppRGCTXDataType)2, 22726 },
	{ (Il2CppRGCTXDataType)3, 32797 },
	{ (Il2CppRGCTXDataType)3, 32798 },
	{ (Il2CppRGCTXDataType)2, 12643 },
	{ (Il2CppRGCTXDataType)2, 22727 },
	{ (Il2CppRGCTXDataType)3, 32799 },
	{ (Il2CppRGCTXDataType)3, 32800 },
	{ (Il2CppRGCTXDataType)3, 32801 },
	{ (Il2CppRGCTXDataType)3, 32802 },
	{ (Il2CppRGCTXDataType)3, 32803 },
	{ (Il2CppRGCTXDataType)3, 31450 },
	{ (Il2CppRGCTXDataType)3, 32804 },
	{ (Il2CppRGCTXDataType)2, 22728 },
	{ (Il2CppRGCTXDataType)3, 32805 },
	{ (Il2CppRGCTXDataType)3, 32806 },
	{ (Il2CppRGCTXDataType)2, 12656 },
	{ (Il2CppRGCTXDataType)2, 22729 },
	{ (Il2CppRGCTXDataType)3, 32807 },
	{ (Il2CppRGCTXDataType)3, 32808 },
	{ (Il2CppRGCTXDataType)2, 12658 },
	{ (Il2CppRGCTXDataType)2, 22730 },
	{ (Il2CppRGCTXDataType)3, 32809 },
	{ (Il2CppRGCTXDataType)3, 32810 },
	{ (Il2CppRGCTXDataType)2, 22731 },
	{ (Il2CppRGCTXDataType)3, 32811 },
	{ (Il2CppRGCTXDataType)3, 32812 },
	{ (Il2CppRGCTXDataType)2, 22732 },
	{ (Il2CppRGCTXDataType)3, 32813 },
	{ (Il2CppRGCTXDataType)3, 32814 },
	{ (Il2CppRGCTXDataType)2, 12673 },
	{ (Il2CppRGCTXDataType)2, 22733 },
	{ (Il2CppRGCTXDataType)3, 32815 },
	{ (Il2CppRGCTXDataType)3, 32816 },
	{ (Il2CppRGCTXDataType)3, 32817 },
	{ (Il2CppRGCTXDataType)3, 31461 },
	{ (Il2CppRGCTXDataType)2, 22734 },
	{ (Il2CppRGCTXDataType)3, 32818 },
	{ (Il2CppRGCTXDataType)3, 32819 },
	{ (Il2CppRGCTXDataType)2, 22735 },
	{ (Il2CppRGCTXDataType)3, 32820 },
	{ (Il2CppRGCTXDataType)3, 32821 },
	{ (Il2CppRGCTXDataType)2, 12689 },
	{ (Il2CppRGCTXDataType)2, 22736 },
	{ (Il2CppRGCTXDataType)3, 32822 },
	{ (Il2CppRGCTXDataType)3, 32823 },
	{ (Il2CppRGCTXDataType)3, 32824 },
	{ (Il2CppRGCTXDataType)3, 32825 },
	{ (Il2CppRGCTXDataType)3, 32826 },
	{ (Il2CppRGCTXDataType)3, 32827 },
	{ (Il2CppRGCTXDataType)3, 31467 },
	{ (Il2CppRGCTXDataType)2, 22737 },
	{ (Il2CppRGCTXDataType)3, 32828 },
	{ (Il2CppRGCTXDataType)3, 32829 },
	{ (Il2CppRGCTXDataType)2, 22738 },
	{ (Il2CppRGCTXDataType)3, 32830 },
	{ (Il2CppRGCTXDataType)3, 32831 },
	{ (Il2CppRGCTXDataType)3, 32832 },
	{ (Il2CppRGCTXDataType)3, 32833 },
	{ (Il2CppRGCTXDataType)3, 32834 },
	{ (Il2CppRGCTXDataType)3, 32835 },
	{ (Il2CppRGCTXDataType)2, 22739 },
	{ (Il2CppRGCTXDataType)2, 22740 },
	{ (Il2CppRGCTXDataType)3, 32836 },
	{ (Il2CppRGCTXDataType)2, 12724 },
	{ (Il2CppRGCTXDataType)2, 12718 },
	{ (Il2CppRGCTXDataType)3, 32837 },
	{ (Il2CppRGCTXDataType)2, 12717 },
	{ (Il2CppRGCTXDataType)2, 22741 },
	{ (Il2CppRGCTXDataType)3, 32838 },
	{ (Il2CppRGCTXDataType)3, 32839 },
	{ (Il2CppRGCTXDataType)3, 32840 },
	{ (Il2CppRGCTXDataType)2, 12737 },
	{ (Il2CppRGCTXDataType)2, 12732 },
	{ (Il2CppRGCTXDataType)3, 32841 },
	{ (Il2CppRGCTXDataType)2, 12731 },
	{ (Il2CppRGCTXDataType)2, 22742 },
	{ (Il2CppRGCTXDataType)3, 32842 },
	{ (Il2CppRGCTXDataType)3, 32843 },
	{ (Il2CppRGCTXDataType)3, 32844 },
	{ (Il2CppRGCTXDataType)3, 32845 },
	{ (Il2CppRGCTXDataType)2, 12747 },
	{ (Il2CppRGCTXDataType)2, 12742 },
	{ (Il2CppRGCTXDataType)3, 32846 },
	{ (Il2CppRGCTXDataType)2, 12741 },
	{ (Il2CppRGCTXDataType)2, 22743 },
	{ (Il2CppRGCTXDataType)3, 32847 },
	{ (Il2CppRGCTXDataType)3, 32848 },
	{ (Il2CppRGCTXDataType)3, 32849 },
	{ (Il2CppRGCTXDataType)2, 22744 },
	{ (Il2CppRGCTXDataType)3, 32850 },
	{ (Il2CppRGCTXDataType)2, 12760 },
	{ (Il2CppRGCTXDataType)2, 12752 },
	{ (Il2CppRGCTXDataType)3, 32851 },
	{ (Il2CppRGCTXDataType)3, 32852 },
	{ (Il2CppRGCTXDataType)2, 12751 },
	{ (Il2CppRGCTXDataType)2, 22745 },
	{ (Il2CppRGCTXDataType)3, 32853 },
	{ (Il2CppRGCTXDataType)3, 32854 },
	{ (Il2CppRGCTXDataType)2, 22746 },
	{ (Il2CppRGCTXDataType)2, 22747 },
	{ (Il2CppRGCTXDataType)3, 32855 },
	{ (Il2CppRGCTXDataType)2, 22748 },
	{ (Il2CppRGCTXDataType)2, 22749 },
	{ (Il2CppRGCTXDataType)3, 32856 },
	{ (Il2CppRGCTXDataType)3, 32857 },
	{ (Il2CppRGCTXDataType)2, 12772 },
	{ (Il2CppRGCTXDataType)3, 32858 },
	{ (Il2CppRGCTXDataType)2, 12773 },
	{ (Il2CppRGCTXDataType)2, 22750 },
	{ (Il2CppRGCTXDataType)3, 32859 },
	{ (Il2CppRGCTXDataType)3, 32860 },
	{ (Il2CppRGCTXDataType)2, 22751 },
	{ (Il2CppRGCTXDataType)3, 32861 },
	{ (Il2CppRGCTXDataType)3, 32862 },
	{ (Il2CppRGCTXDataType)3, 32863 },
	{ (Il2CppRGCTXDataType)2, 12788 },
	{ (Il2CppRGCTXDataType)3, 32864 },
	{ (Il2CppRGCTXDataType)2, 12797 },
	{ (Il2CppRGCTXDataType)3, 32865 },
	{ (Il2CppRGCTXDataType)2, 22752 },
	{ (Il2CppRGCTXDataType)2, 22753 },
	{ (Il2CppRGCTXDataType)3, 32866 },
	{ (Il2CppRGCTXDataType)3, 32867 },
	{ (Il2CppRGCTXDataType)3, 32868 },
	{ (Il2CppRGCTXDataType)3, 32869 },
	{ (Il2CppRGCTXDataType)3, 32870 },
	{ (Il2CppRGCTXDataType)3, 32871 },
	{ (Il2CppRGCTXDataType)2, 12813 },
	{ (Il2CppRGCTXDataType)2, 22754 },
	{ (Il2CppRGCTXDataType)3, 32872 },
	{ (Il2CppRGCTXDataType)3, 32873 },
	{ (Il2CppRGCTXDataType)2, 12817 },
	{ (Il2CppRGCTXDataType)3, 32874 },
	{ (Il2CppRGCTXDataType)2, 22755 },
	{ (Il2CppRGCTXDataType)2, 12827 },
	{ (Il2CppRGCTXDataType)2, 12825 },
	{ (Il2CppRGCTXDataType)2, 22756 },
	{ (Il2CppRGCTXDataType)3, 32875 },
	{ (Il2CppRGCTXDataType)2, 22757 },
	{ (Il2CppRGCTXDataType)3, 32876 },
	{ (Il2CppRGCTXDataType)3, 32877 },
	{ (Il2CppRGCTXDataType)3, 32878 },
	{ (Il2CppRGCTXDataType)2, 12835 },
	{ (Il2CppRGCTXDataType)3, 32879 },
	{ (Il2CppRGCTXDataType)3, 32880 },
	{ (Il2CppRGCTXDataType)2, 12838 },
	{ (Il2CppRGCTXDataType)3, 32881 },
	{ (Il2CppRGCTXDataType)1, 22758 },
	{ (Il2CppRGCTXDataType)2, 12837 },
	{ (Il2CppRGCTXDataType)3, 32882 },
	{ (Il2CppRGCTXDataType)1, 12837 },
	{ (Il2CppRGCTXDataType)1, 12835 },
	{ (Il2CppRGCTXDataType)2, 22759 },
	{ (Il2CppRGCTXDataType)2, 12837 },
	{ (Il2CppRGCTXDataType)2, 12840 },
	{ (Il2CppRGCTXDataType)2, 12839 },
	{ (Il2CppRGCTXDataType)2, 12855 },
	{ (Il2CppRGCTXDataType)3, 32883 },
	{ (Il2CppRGCTXDataType)2, 12841 },
	{ (Il2CppRGCTXDataType)3, 32884 },
	{ (Il2CppRGCTXDataType)2, 12841 },
	{ (Il2CppRGCTXDataType)3, 32885 },
	{ (Il2CppRGCTXDataType)3, 32886 },
	{ (Il2CppRGCTXDataType)3, 32887 },
	{ (Il2CppRGCTXDataType)3, 32888 },
	{ (Il2CppRGCTXDataType)3, 32889 },
	{ (Il2CppRGCTXDataType)3, 32890 },
	{ (Il2CppRGCTXDataType)3, 32891 },
	{ (Il2CppRGCTXDataType)3, 32892 },
	{ (Il2CppRGCTXDataType)3, 32893 },
	{ (Il2CppRGCTXDataType)2, 12836 },
	{ (Il2CppRGCTXDataType)3, 32894 },
	{ (Il2CppRGCTXDataType)2, 12851 },
	{ (Il2CppRGCTXDataType)2, 12862 },
	{ (Il2CppRGCTXDataType)2, 12864 },
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationSystem_Core;
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	233,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	58,
	s_rgctxIndices,
	283,
	s_rgctxValues,
	&g_DebuggerMetadataRegistrationSystem_Core,
};
