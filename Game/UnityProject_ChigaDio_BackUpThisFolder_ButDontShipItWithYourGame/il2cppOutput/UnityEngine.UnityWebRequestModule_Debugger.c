﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#else
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#else
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[82] = 
{
	{ 0, 0 } /* 0x06000001 System.String UnityEngineInternal.WebRequestUtils::RedirectTo(System.String,System.String) */,
	{ 0, 0 } /* 0x06000002 System.String UnityEngineInternal.WebRequestUtils::MakeInitialUrl(System.String,System.String) */,
	{ 0, 0 } /* 0x06000003 System.String UnityEngineInternal.WebRequestUtils::MakeUriString(System.Uri,System.String,System.Boolean) */,
	{ 0, 0 } /* 0x06000004 System.String UnityEngineInternal.WebRequestUtils::URLDecode(System.String) */,
	{ 0, 0 } /* 0x06000005 System.Void UnityEngineInternal.WebRequestUtils::.cctor() */,
	{ 0, 0 } /* 0x06000006 System.Text.Encoding UnityEngine.WWWForm::get_DefaultEncoding() */,
	{ 0, 0 } /* 0x06000007 System.Byte UnityEngine.WWWTranscoder::Hex2Byte(System.Byte[],System.Int32) */,
	{ 0, 0 } /* 0x06000008 System.Byte[] UnityEngine.WWWTranscoder::URLDecode(System.Byte[]) */,
	{ 0, 0 } /* 0x06000009 System.Boolean UnityEngine.WWWTranscoder::ByteSubArrayEquals(System.Byte[],System.Int32,System.Byte[]) */,
	{ 0, 0 } /* 0x0600000A System.Byte[] UnityEngine.WWWTranscoder::Decode(System.Byte[],System.Byte,System.Byte[]) */,
	{ 0, 0 } /* 0x0600000B System.Void UnityEngine.WWWTranscoder::.cctor() */,
	{ 0, 0 } /* 0x0600000C UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::get_webRequest() */,
	{ 0, 0 } /* 0x0600000D System.Void UnityEngine.Networking.UnityWebRequestAsyncOperation::set_webRequest(UnityEngine.Networking.UnityWebRequest) */,
	{ 0, 0 } /* 0x0600000E System.Void UnityEngine.Networking.UnityWebRequestAsyncOperation::.ctor() */,
	{ 0, 0 } /* 0x0600000F System.String UnityEngine.Networking.UnityWebRequest::GetWebErrorString(UnityEngine.Networking.UnityWebRequest/UnityWebRequestError) */,
	{ 0, 0 } /* 0x06000010 System.String UnityEngine.Networking.UnityWebRequest::GetHTTPStatusString(System.Int64) */,
	{ 0, 0 } /* 0x06000011 System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeCertificateHandlerOnDispose() */,
	{ 0, 0 } /* 0x06000012 System.Void UnityEngine.Networking.UnityWebRequest::set_disposeCertificateHandlerOnDispose(System.Boolean) */,
	{ 0, 0 } /* 0x06000013 System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeDownloadHandlerOnDispose() */,
	{ 0, 0 } /* 0x06000014 System.Void UnityEngine.Networking.UnityWebRequest::set_disposeDownloadHandlerOnDispose(System.Boolean) */,
	{ 0, 0 } /* 0x06000015 System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeUploadHandlerOnDispose() */,
	{ 0, 0 } /* 0x06000016 System.Void UnityEngine.Networking.UnityWebRequest::set_disposeUploadHandlerOnDispose(System.Boolean) */,
	{ 0, 0 } /* 0x06000017 System.IntPtr UnityEngine.Networking.UnityWebRequest::Create() */,
	{ 0, 0 } /* 0x06000018 System.Void UnityEngine.Networking.UnityWebRequest::Release() */,
	{ 0, 0 } /* 0x06000019 System.Void UnityEngine.Networking.UnityWebRequest::InternalDestroy() */,
	{ 0, 0 } /* 0x0600001A System.Void UnityEngine.Networking.UnityWebRequest::InternalSetDefaults() */,
	{ 0, 0 } /* 0x0600001B System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String,UnityEngine.Networking.DownloadHandler,UnityEngine.Networking.UploadHandler) */,
	{ 0, 0 } /* 0x0600001C System.Void UnityEngine.Networking.UnityWebRequest::Finalize() */,
	{ 0, 0 } /* 0x0600001D System.Void UnityEngine.Networking.UnityWebRequest::Dispose() */,
	{ 0, 0 } /* 0x0600001E System.Void UnityEngine.Networking.UnityWebRequest::DisposeHandlers() */,
	{ 0, 0 } /* 0x0600001F UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::BeginWebRequest() */,
	{ 0, 0 } /* 0x06000020 UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest() */,
	{ 0, 0 } /* 0x06000021 System.Void UnityEngine.Networking.UnityWebRequest::Abort() */,
	{ 0, 0 } /* 0x06000022 UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod) */,
	{ 0, 0 } /* 0x06000023 System.Void UnityEngine.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod) */,
	{ 0, 0 } /* 0x06000024 UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetCustomMethod(System.String) */,
	{ 0, 0 } /* 0x06000025 System.Void UnityEngine.Networking.UnityWebRequest::InternalSetCustomMethod(System.String) */,
	{ 0, 0 } /* 0x06000026 System.Void UnityEngine.Networking.UnityWebRequest::set_method(System.String) */,
	{ 0, 0 } /* 0x06000027 UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::GetError() */,
	{ 0, 0 } /* 0x06000028 System.String UnityEngine.Networking.UnityWebRequest::get_error() */,
	{ 0, 0 } /* 0x06000029 System.String UnityEngine.Networking.UnityWebRequest::get_url() */,
	{ 0, 0 } /* 0x0600002A System.Void UnityEngine.Networking.UnityWebRequest::set_url(System.String) */,
	{ 0, 0 } /* 0x0600002B System.String UnityEngine.Networking.UnityWebRequest::GetUrl() */,
	{ 0, 0 } /* 0x0600002C UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUrl(System.String) */,
	{ 0, 0 } /* 0x0600002D System.Void UnityEngine.Networking.UnityWebRequest::InternalSetUrl(System.String) */,
	{ 0, 0 } /* 0x0600002E System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode() */,
	{ 0, 0 } /* 0x0600002F System.Boolean UnityEngine.Networking.UnityWebRequest::get_isModifiable() */,
	{ 0, 0 } /* 0x06000030 UnityEngine.Networking.UnityWebRequest/Result UnityEngine.Networking.UnityWebRequest::get_result() */,
	{ 0, 0 } /* 0x06000031 System.Void UnityEngine.Networking.UnityWebRequest::SetRedirectLimitFromScripting(System.Int32) */,
	{ 0, 0 } /* 0x06000032 System.Void UnityEngine.Networking.UnityWebRequest::set_redirectLimit(System.Int32) */,
	{ 0, 0 } /* 0x06000033 UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUploadHandler(UnityEngine.Networking.UploadHandler) */,
	{ 0, 0 } /* 0x06000034 UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::get_uploadHandler() */,
	{ 0, 0 } /* 0x06000035 System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler) */,
	{ 0, 0 } /* 0x06000036 UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetDownloadHandler(UnityEngine.Networking.DownloadHandler) */,
	{ 0, 0 } /* 0x06000037 UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler() */,
	{ 0, 0 } /* 0x06000038 System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler) */,
	{ 0, 0 } /* 0x06000039 UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetCertificateHandler(UnityEngine.Networking.CertificateHandler) */,
	{ 0, 0 } /* 0x0600003A UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::get_certificateHandler() */,
	{ 0, 0 } /* 0x0600003B System.Void UnityEngine.Networking.UnityWebRequest::set_certificateHandler(UnityEngine.Networking.CertificateHandler) */,
	{ 0, 0 } /* 0x0600003C UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetTimeoutMsec(System.Int32) */,
	{ 0, 0 } /* 0x0600003D System.Void UnityEngine.Networking.UnityWebRequest::set_timeout(System.Int32) */,
	{ 0, 0 } /* 0x0600003E System.Void UnityEngine.Networking.CertificateHandler::Release() */,
	{ 0, 0 } /* 0x0600003F System.Boolean UnityEngine.Networking.CertificateHandler::ValidateCertificate(System.Byte[]) */,
	{ 0, 0 } /* 0x06000040 System.Boolean UnityEngine.Networking.CertificateHandler::ValidateCertificateNative(System.Byte[]) */,
	{ 0, 0 } /* 0x06000041 System.Void UnityEngine.Networking.CertificateHandler::Dispose() */,
	{ 0, 0 } /* 0x06000042 System.Void UnityEngine.Networking.DownloadHandler::Release() */,
	{ 0, 0 } /* 0x06000043 System.Void UnityEngine.Networking.DownloadHandler::.ctor() */,
	{ 0, 0 } /* 0x06000044 System.Void UnityEngine.Networking.DownloadHandler::Finalize() */,
	{ 0, 0 } /* 0x06000045 System.Void UnityEngine.Networking.DownloadHandler::Dispose() */,
	{ 0, 0 } /* 0x06000046 System.String UnityEngine.Networking.DownloadHandler::get_text() */,
	{ 0, 0 } /* 0x06000047 System.Byte[] UnityEngine.Networking.DownloadHandler::GetData() */,
	{ 0, 0 } /* 0x06000048 System.String UnityEngine.Networking.DownloadHandler::GetText() */,
	{ 0, 0 } /* 0x06000049 System.Text.Encoding UnityEngine.Networking.DownloadHandler::GetTextEncoder() */,
	{ 0, 0 } /* 0x0600004A System.String UnityEngine.Networking.DownloadHandler::GetContentType() */,
	{ 0, 0 } /* 0x0600004B System.Byte[] UnityEngine.Networking.DownloadHandler::InternalGetByteArray(UnityEngine.Networking.DownloadHandler) */,
	{ 0, 0 } /* 0x0600004C System.IntPtr UnityEngine.Networking.DownloadHandlerBuffer::Create(UnityEngine.Networking.DownloadHandlerBuffer) */,
	{ 0, 0 } /* 0x0600004D System.Void UnityEngine.Networking.DownloadHandlerBuffer::InternalCreateBuffer() */,
	{ 0, 0 } /* 0x0600004E System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor() */,
	{ 0, 0 } /* 0x0600004F System.Byte[] UnityEngine.Networking.DownloadHandlerBuffer::GetData() */,
	{ 0, 0 } /* 0x06000050 System.Byte[] UnityEngine.Networking.DownloadHandlerBuffer::InternalGetData() */,
	{ 0, 0 } /* 0x06000051 System.Void UnityEngine.Networking.UploadHandler::Release() */,
	{ 0, 0 } /* 0x06000052 System.Void UnityEngine.Networking.UploadHandler::Dispose() */,
};
#else
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[1] = { { 0, 0} };
#endif
#if IL2CPP_MONO_DEBUGGER
extern Il2CppSequencePoint g_sequencePointsUnityEngine_UnityWebRequestModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_UnityWebRequestModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#else
extern Il2CppSequencePoint g_sequencePointsUnityEngine_UnityWebRequestModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_UnityWebRequestModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppCatchPoint g_catchPoints[] = {
{ 28769, 3846, 163, 0, -1 },
{ 28698, 4065, 162, 0, -1 },
{ 28698, 4065, 198, 1, -1 },
};
#else
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#else
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#else
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#else
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[82] = 
{
	{ 0, 0, 0 } /* System.String UnityEngineInternal.WebRequestUtils::RedirectTo(System.String,System.String) */,
	{ 0, 0, 0 } /* System.String UnityEngineInternal.WebRequestUtils::MakeInitialUrl(System.String,System.String) */,
	{ 0, 0, 0 } /* System.String UnityEngineInternal.WebRequestUtils::MakeUriString(System.Uri,System.String,System.Boolean) */,
	{ 0, 0, 0 } /* System.String UnityEngineInternal.WebRequestUtils::URLDecode(System.String) */,
	{ 0, 0, 0 } /* System.Void UnityEngineInternal.WebRequestUtils::.cctor() */,
	{ 0, 0, 0 } /* System.Text.Encoding UnityEngine.WWWForm::get_DefaultEncoding() */,
	{ 0, 0, 0 } /* System.Byte UnityEngine.WWWTranscoder::Hex2Byte(System.Byte[],System.Int32) */,
	{ 0, 0, 0 } /* System.Byte[] UnityEngine.WWWTranscoder::URLDecode(System.Byte[]) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.WWWTranscoder::ByteSubArrayEquals(System.Byte[],System.Int32,System.Byte[]) */,
	{ 0, 0, 0 } /* System.Byte[] UnityEngine.WWWTranscoder::Decode(System.Byte[],System.Byte,System.Byte[]) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.WWWTranscoder::.cctor() */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::get_webRequest() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequestAsyncOperation::set_webRequest(UnityEngine.Networking.UnityWebRequest) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequestAsyncOperation::.ctor() */,
	{ 0, 0, 0 } /* System.String UnityEngine.Networking.UnityWebRequest::GetWebErrorString(UnityEngine.Networking.UnityWebRequest/UnityWebRequestError) */,
	{ 0, 0, 0 } /* System.String UnityEngine.Networking.UnityWebRequest::GetHTTPStatusString(System.Int64) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeCertificateHandlerOnDispose() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_disposeCertificateHandlerOnDispose(System.Boolean) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeDownloadHandlerOnDispose() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_disposeDownloadHandlerOnDispose(System.Boolean) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeUploadHandlerOnDispose() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_disposeUploadHandlerOnDispose(System.Boolean) */,
	{ 0, 0, 0 } /* System.IntPtr UnityEngine.Networking.UnityWebRequest::Create() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::Release() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::InternalDestroy() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::InternalSetDefaults() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String,UnityEngine.Networking.DownloadHandler,UnityEngine.Networking.UploadHandler) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::Finalize() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::Dispose() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::DisposeHandlers() */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::BeginWebRequest() */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::Abort() */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod) */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetCustomMethod(System.String) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::InternalSetCustomMethod(System.String) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_method(System.String) */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::GetError() */,
	{ 0, 0, 0 } /* System.String UnityEngine.Networking.UnityWebRequest::get_error() */,
	{ 0, 0, 0 } /* System.String UnityEngine.Networking.UnityWebRequest::get_url() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_url(System.String) */,
	{ 0, 0, 0 } /* System.String UnityEngine.Networking.UnityWebRequest::GetUrl() */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUrl(System.String) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::InternalSetUrl(System.String) */,
	{ 0, 0, 0 } /* System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode() */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Networking.UnityWebRequest::get_isModifiable() */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest/Result UnityEngine.Networking.UnityWebRequest::get_result() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::SetRedirectLimitFromScripting(System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_redirectLimit(System.Int32) */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetUploadHandler(UnityEngine.Networking.UploadHandler) */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::get_uploadHandler() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler) */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetDownloadHandler(UnityEngine.Networking.DownloadHandler) */,
	{ 0, 0, 0 } /* UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler) */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetCertificateHandler(UnityEngine.Networking.CertificateHandler) */,
	{ 0, 0, 0 } /* UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::get_certificateHandler() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_certificateHandler(UnityEngine.Networking.CertificateHandler) */,
	{ 0, 0, 0 } /* UnityEngine.Networking.UnityWebRequest/UnityWebRequestError UnityEngine.Networking.UnityWebRequest::SetTimeoutMsec(System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UnityWebRequest::set_timeout(System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.CertificateHandler::Release() */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Networking.CertificateHandler::ValidateCertificate(System.Byte[]) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Networking.CertificateHandler::ValidateCertificateNative(System.Byte[]) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.CertificateHandler::Dispose() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.DownloadHandler::Release() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.DownloadHandler::.ctor() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.DownloadHandler::Finalize() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.DownloadHandler::Dispose() */,
	{ 0, 0, 0 } /* System.String UnityEngine.Networking.DownloadHandler::get_text() */,
	{ 0, 0, 0 } /* System.Byte[] UnityEngine.Networking.DownloadHandler::GetData() */,
	{ 0, 0, 0 } /* System.String UnityEngine.Networking.DownloadHandler::GetText() */,
	{ 0, 0, 0 } /* System.Text.Encoding UnityEngine.Networking.DownloadHandler::GetTextEncoder() */,
	{ 0, 0, 0 } /* System.String UnityEngine.Networking.DownloadHandler::GetContentType() */,
	{ 0, 0, 0 } /* System.Byte[] UnityEngine.Networking.DownloadHandler::InternalGetByteArray(UnityEngine.Networking.DownloadHandler) */,
	{ 0, 0, 0 } /* System.IntPtr UnityEngine.Networking.DownloadHandlerBuffer::Create(UnityEngine.Networking.DownloadHandlerBuffer) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.DownloadHandlerBuffer::InternalCreateBuffer() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor() */,
	{ 0, 0, 0 } /* System.Byte[] UnityEngine.Networking.DownloadHandlerBuffer::GetData() */,
	{ 0, 0, 0 } /* System.Byte[] UnityEngine.Networking.DownloadHandlerBuffer::InternalGetData() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UploadHandler::Release() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Networking.UploadHandler::Dispose() */,
};
#else
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[1] = { { 0, 0, 0 } };
#endif
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_UnityWebRequestModule;
const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_UnityWebRequestModule = 
{
	(Il2CppMethodExecutionContextInfo*)g_methodExecutionContextInfos,
	(Il2CppMethodExecutionContextInfoIndex*)g_methodExecutionContextInfoIndexes,
	(Il2CppMethodScope*)g_methodScopes,
	(Il2CppMethodHeaderInfo*)g_methodHeaderInfos,
	(Il2CppSequencePointSourceFile*)g_sequencePointSourceFiles,
	0,
	(Il2CppSequencePoint*)g_sequencePointsUnityEngine_UnityWebRequestModule,
	3,
	(Il2CppCatchPoint*)g_catchPoints,
	0,
	(Il2CppTypeSourceFilePair*)g_typeSourceFiles,
	(const char**)g_methodExecutionContextInfoStrings,
};
