﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Int32 NPOI.POIXMLDocumentPart::IncrementRelationCounter()
extern void POIXMLDocumentPart_IncrementRelationCounter_mAA9E19E2C03A177BEED92FC5DEE42944E489B104 (void);
// 0x00000002 System.Int32 NPOI.POIXMLDocumentPart::DecrementRelationCounter()
extern void POIXMLDocumentPart_DecrementRelationCounter_m35E96A5398A8C34D80EE72DA8E220363BC2AEF1A (void);
// 0x00000003 System.Int32 NPOI.POIXMLDocumentPart::GetRelationCounter()
extern void POIXMLDocumentPart_GetRelationCounter_mE9D85EDE265216347F097E7ADDAA6FBFF2C4C45A (void);
// 0x00000004 System.Void NPOI.POIXMLDocumentPart::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage)
extern void POIXMLDocumentPart__ctor_m9C3CA573396BCEABFEF8080CFE2D1A81FB872683 (void);
// 0x00000005 System.Void NPOI.POIXMLDocumentPart::.ctor()
extern void POIXMLDocumentPart__ctor_m83735182590B17918A24D9064401A11339487954 (void);
// 0x00000006 System.Void NPOI.POIXMLDocumentPart::.ctor(NPOI.OpenXml4Net.OPC.PackagePart,NPOI.OpenXml4Net.OPC.PackageRelationship)
extern void POIXMLDocumentPart__ctor_mB3A12EFD2DFE02765BD8694CDFADEC0A2CD92810 (void);
// 0x00000007 System.Xml.XmlNamespaceManager NPOI.POIXMLDocumentPart::get_NamespaceManager()
extern void POIXMLDocumentPart_get_NamespaceManager_m909312D7D29884BC4A4D5D7DA54BD832C23CEFC0 (void);
// 0x00000008 System.Xml.XmlNamespaceManager NPOI.POIXMLDocumentPart::CreateDefaultNSM()
extern void POIXMLDocumentPart_CreateDefaultNSM_m64F247CDF69B8C66B19DF8F5ADBCF8B591A15EF4 (void);
// 0x00000009 NPOI.OpenXml4Net.OPC.PackagePart NPOI.POIXMLDocumentPart::GetPackagePart()
extern void POIXMLDocumentPart_GetPackagePart_m97C7798B2BD5391F1B9D0837575B6C5B801E9265 (void);
// 0x0000000A System.Xml.XmlDocument NPOI.POIXMLDocumentPart::ConvertStreamToXml(System.IO.Stream)
extern void POIXMLDocumentPart_ConvertStreamToXml_m3B00F60A1178A21ED32933B5966A8C48FC15B6C5 (void);
// 0x0000000B NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.POIXMLDocumentPart::GetPackageRelationship()
extern void POIXMLDocumentPart_GetPackageRelationship_m2F5CB2352DF156A8CB48DA47BC77448EFECDAD0F (void);
// 0x0000000C System.Collections.Generic.List`1<NPOI.POIXMLDocumentPart> NPOI.POIXMLDocumentPart::GetRelations()
extern void POIXMLDocumentPart_GetRelations_m1A77728867382F10BD3292CB34B2711A50CFC453 (void);
// 0x0000000D System.String NPOI.POIXMLDocumentPart::GetRelationId(NPOI.POIXMLDocumentPart)
extern void POIXMLDocumentPart_GetRelationId_mAF9C5358C0D9D58888DDDA18D0B852B93094FC77 (void);
// 0x0000000E System.Void NPOI.POIXMLDocumentPart::AddRelation(System.String,NPOI.POIXMLDocumentPart)
extern void POIXMLDocumentPart_AddRelation_mD05953ED02696D99DD8EBD960E85C5591A0276CA (void);
// 0x0000000F System.Void NPOI.POIXMLDocumentPart::RemoveRelation(NPOI.POIXMLDocumentPart)
extern void POIXMLDocumentPart_RemoveRelation_mCC36784CAADC4C56B275CAFE8BA6A59D4775AA41 (void);
// 0x00000010 System.Boolean NPOI.POIXMLDocumentPart::RemoveRelation(NPOI.POIXMLDocumentPart,System.Boolean)
extern void POIXMLDocumentPart_RemoveRelation_mB9FA4025B65E8907FF98AEAF658068D9CBAEE86F (void);
// 0x00000011 NPOI.POIXMLDocumentPart NPOI.POIXMLDocumentPart::GetParent()
extern void POIXMLDocumentPart_GetParent_m3A223A75A629A6994B4B0F98063EF7376FABA06C (void);
// 0x00000012 System.String NPOI.POIXMLDocumentPart::ToString()
extern void POIXMLDocumentPart_ToString_mE065333E7AB6B956A2441B2705170CA978EFBC40 (void);
// 0x00000013 System.Void NPOI.POIXMLDocumentPart::Commit()
extern void POIXMLDocumentPart_Commit_m78ED5D07BA7842EB7061371F12E30417017A2D69 (void);
// 0x00000014 System.Void NPOI.POIXMLDocumentPart::OnSave(System.Collections.Generic.List`1<NPOI.OpenXml4Net.OPC.PackagePart>)
extern void POIXMLDocumentPart_OnSave_m8E70B19D45E12185AFCEE8D172636A7FD876B8A2 (void);
// 0x00000015 NPOI.POIXMLDocumentPart NPOI.POIXMLDocumentPart::CreateRelationship(NPOI.POIXMLRelation,NPOI.POIXMLFactory)
extern void POIXMLDocumentPart_CreateRelationship_mD2B8BC4EF6AB9A35A2453F0FC1B626C50C1B55CB (void);
// 0x00000016 NPOI.POIXMLDocumentPart NPOI.POIXMLDocumentPart::CreateRelationship(NPOI.POIXMLRelation,NPOI.POIXMLFactory,System.Int32,System.Boolean)
extern void POIXMLDocumentPart_CreateRelationship_mF346C61600510FCAC50F508EA6659627B99096D9 (void);
// 0x00000017 System.Void NPOI.POIXMLDocumentPart::Read(NPOI.POIXMLFactory,System.Collections.Generic.Dictionary`2<NPOI.OpenXml4Net.OPC.PackagePart,NPOI.POIXMLDocumentPart>)
extern void POIXMLDocumentPart_Read_mE1BCF560E5D3FA245514BF338E674BE5284559EF (void);
// 0x00000018 System.Void NPOI.POIXMLDocumentPart::OnDocumentCreate()
extern void POIXMLDocumentPart_OnDocumentCreate_m298234B3D9AC29D20A9194A08C513E60982790C1 (void);
// 0x00000019 System.Void NPOI.POIXMLDocumentPart::OnDocumentRead()
extern void POIXMLDocumentPart_OnDocumentRead_m314D2B5E98D230328246496AB122A3A26A409C69 (void);
// 0x0000001A System.Void NPOI.POIXMLDocumentPart::onDocumentRemove()
extern void POIXMLDocumentPart_onDocumentRemove_m3899C5FA2F151979A828149C9AB95E3518D7953B (void);
// 0x0000001B System.Void NPOI.POIXMLDocumentPart::.cctor()
extern void POIXMLDocumentPart__cctor_m3883F5D6128700A7A12FDEC904B2A984C503F7FB (void);
// 0x0000001C System.Void NPOI.POIXMLDocument::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage)
extern void POIXMLDocument__ctor_m09BFFE3F1F3097B978D510417000643CC026B15A (void);
// 0x0000001D NPOI.OpenXml4Net.OPC.OPCPackage NPOI.POIXMLDocument::get_Package()
extern void POIXMLDocument_get_Package_mE7ED9A743B48329991CF524C42E136D172925D4C (void);
// 0x0000001E NPOI.POIXMLProperties NPOI.POIXMLDocument::GetProperties()
extern void POIXMLDocument_GetProperties_mA16B43DFE84B69C1F197AB81D75CDD899F9C01AE (void);
// 0x0000001F System.Void NPOI.POIXMLDocument::Load(NPOI.POIXMLFactory)
extern void POIXMLDocument_Load_m2035AC49492A017923162D6E9DA025A563D56A81 (void);
// 0x00000020 System.Void NPOI.POIXMLDocument::Write(System.IO.Stream)
extern void POIXMLDocument_Write_m1835686382F5890721E890450CBB97165E2D6F1E (void);
// 0x00000021 System.Void NPOI.POIXMLDocument::.cctor()
extern void POIXMLDocument__cctor_m3FF3982278EA1507BB56B5BA3A4AC43D24F2EE0E (void);
// 0x00000022 System.Void NPOI.POIXMLException::.ctor(System.Exception)
extern void POIXMLException__ctor_m89160B0B53F776DE7A0ED6B05276EEA950477F85 (void);
// 0x00000023 NPOI.POIXMLDocumentPart NPOI.POIXMLFactory::CreateDocumentPart(NPOI.POIXMLDocumentPart,NPOI.OpenXml4Net.OPC.PackageRelationship,NPOI.OpenXml4Net.OPC.PackagePart)
// 0x00000024 NPOI.POIXMLDocumentPart NPOI.POIXMLFactory::CreateDocumentPart(NPOI.POIXMLRelation)
// 0x00000025 System.Void NPOI.POIXMLFactory::.ctor()
extern void POIXMLFactory__ctor_m6C94C6CFF85D6B5BEF5A2D7B11BF830930A7B63D (void);
// 0x00000026 System.Void NPOI.CoreProperties::.ctor(NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart)
extern void CoreProperties__ctor_mA9B660CBF5FD3BA3BE297EA84BFBA021DD6E5CC9 (void);
// 0x00000027 System.Void NPOI.ExtendedProperties::.ctor(NPOI.OpenXmlFormats.ExtendedPropertiesDocument)
extern void ExtendedProperties__ctor_m73464D44FBE7104CCA07782B9BAC50B0BE9FD79A (void);
// 0x00000028 System.Void NPOI.CustomProperties::.ctor(NPOI.OpenXmlFormats.CustomPropertiesDocument)
extern void CustomProperties__ctor_m2E1B8E7D40CB0E614330CF84943EEAD92248EEAE (void);
// 0x00000029 NPOI.OpenXmlFormats.CT_Property NPOI.CustomProperties::Add(System.String)
extern void CustomProperties_Add_mC74DD361DEE373BBC5EF756FE6AF4153C155D32E (void);
// 0x0000002A System.Void NPOI.CustomProperties::AddProperty(System.String,System.String)
extern void CustomProperties_AddProperty_m98AF304A32EB6B780F5809BB6F3741FB096AA42C (void);
// 0x0000002B System.Int32 NPOI.CustomProperties::NextPid()
extern void CustomProperties_NextPid_m4157397130049166F2B2F32D3D6F567CFC340F6F (void);
// 0x0000002C System.Boolean NPOI.CustomProperties::Contains(System.String)
extern void CustomProperties_Contains_m4DE6162CAF490DEF211B92232D7854ACA78BC4E3 (void);
// 0x0000002D System.Void NPOI.CustomProperties::.cctor()
extern void CustomProperties__cctor_m7CDE11D00884F7F438E1C7078CEE4B7BC455AB5C (void);
// 0x0000002E System.Void NPOI.POIXMLProperties::.cctor()
extern void POIXMLProperties__cctor_m9ACD75E00D7AC35DDCE263ABFD9B8B1822D329C2 (void);
// 0x0000002F System.Void NPOI.POIXMLProperties::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage)
extern void POIXMLProperties__ctor_m96D97978173EBC2A07B05DF0B7B2947B4712203B (void);
// 0x00000030 NPOI.CustomProperties NPOI.POIXMLProperties::get_CustomProperties()
extern void POIXMLProperties_get_CustomProperties_m5EDE13E7EEF6987005B1C8B9C0769BC4C26EB990 (void);
// 0x00000031 System.Void NPOI.POIXMLProperties::Commit()
extern void POIXMLProperties_Commit_m1758AFE3B10AD38FD9A0FD4E8214022BBFCE1B31 (void);
// 0x00000032 System.Void NPOI.POIXMLRelation::.ctor(System.String,System.String,System.String,System.Type)
extern void POIXMLRelation__ctor_mE3808C9EC1D0853DC51C23B0DD4705069F4E2BE1 (void);
// 0x00000033 System.String NPOI.POIXMLRelation::get_ContentType()
extern void POIXMLRelation_get_ContentType_mEE5BEF7BABA2E142D86E80959568B738A831920C (void);
// 0x00000034 System.String NPOI.POIXMLRelation::get_Relation()
extern void POIXMLRelation_get_Relation_mC6BFA5575CF72AF3A358F0141B76B70388FE5BFE (void);
// 0x00000035 System.String NPOI.POIXMLRelation::get_DefaultFileName()
extern void POIXMLRelation_get_DefaultFileName_mCEBB328D558C01FACF1BF1FB5B52DE12A565C296 (void);
// 0x00000036 System.String NPOI.POIXMLRelation::GetFileName(System.Int32)
extern void POIXMLRelation_GetFileName_m584CD4F9E43DAA0969CF5ACAE7324FB7FCF101DA (void);
// 0x00000037 System.Type NPOI.POIXMLRelation::get_RelationClass()
extern void POIXMLRelation_get_RelationClass_m9FD705D95CC41266F75561A544E898128126BC35 (void);
// 0x00000038 System.Void NPOI.SS.UserModel.IndexedColors::.ctor(System.Int32,NPOI.HSSF.Util.HSSFColor)
extern void IndexedColors__ctor_mFB7EEFA8D97BDC9875A4BB16057E32FB3EF90249 (void);
// 0x00000039 System.Void NPOI.SS.UserModel.IndexedColors::.cctor()
extern void IndexedColors__cctor_mFC9E4E57EF192561A21E365E2594B8D1AF8A2011 (void);
// 0x0000003A System.Int16 NPOI.SS.UserModel.IndexedColors::get_Index()
extern void IndexedColors_get_Index_m31FFD69243F0D24C1C5DF5468CA8305760E70327 (void);
// 0x0000003B NPOI.SS.UserModel.IWorkbook NPOI.SS.UserModel.WorkbookFactory::Create(System.String)
extern void WorkbookFactory_Create_m19E3BB3A72D6F4702AB405C2C1050AFFCEA5321F (void);
// 0x0000003C NPOI.OpenXml4Net.OPC.OPCPackage NPOI.Util.PackageHelper::Open(System.IO.Stream)
extern void PackageHelper_Open_m77A97328F6439A295AE16A74F0BDFB7647FF5644 (void);
// 0x0000003D NPOI.OpenXmlFormats.Spreadsheet.CT_CalcChain NPOI.XSSF.Model.CalculationChain::GetCTCalcChain()
extern void CalculationChain_GetCTCalcChain_m00935F4486051A3F7A2B6EFD72036259B54210DC (void);
// 0x0000003E System.Void NPOI.XSSF.Model.CalculationChain::RemoveItem(System.Int32,System.String)
extern void CalculationChain_RemoveItem_m540B359572EAB3CE34CC6513741CDDA1A07884A6 (void);
// 0x0000003F System.Void NPOI.XSSF.Model.IndexedUDFFinder::.ctor(NPOI.SS.Formula.Udf.UDFFinder[])
extern void IndexedUDFFinder__ctor_m9E8C23D39BBF77CC8B525AFB97A4B754CA2FC63C (void);
// 0x00000040 NPOI.SS.Formula.Functions.FreeRefFunction NPOI.XSSF.Model.IndexedUDFFinder::FindFunction(System.String)
extern void IndexedUDFFinder_FindFunction_mF3F2F4A3DA88DFA2A8818A7ADB73B8FF4FDE8E81 (void);
// 0x00000041 System.String NPOI.XSSF.Model.IndexedUDFFinder::GetFunctionName(System.Int32)
extern void IndexedUDFFinder_GetFunctionName_m57FA22FC4064247D96F0AAEA302528EFC913517F (void);
// 0x00000042 System.Int32 NPOI.XSSF.Model.IndexedUDFFinder::GetFunctionIndex(System.String)
extern void IndexedUDFFinder_GetFunctionIndex_m4C31FEB19B22AE08FF6AD89162C82CA98FA4207A (void);
// 0x00000043 System.String NPOI.XSSF.Model.SharedStringsTable::GetKey(NPOI.OpenXmlFormats.Spreadsheet.CT_Rst)
extern void SharedStringsTable_GetKey_mD2C981BD3D82B65C9580BF314DD9E89DB6E7E0E8 (void);
// 0x00000044 NPOI.OpenXmlFormats.Spreadsheet.CT_Rst NPOI.XSSF.Model.SharedStringsTable::GetEntryAt(System.Int32)
extern void SharedStringsTable_GetEntryAt_mBC5F2231C56E57531BDEBD7355AD69F626A6B8DE (void);
// 0x00000045 System.Int32 NPOI.XSSF.Model.SharedStringsTable::AddEntry(NPOI.OpenXmlFormats.Spreadsheet.CT_Rst)
extern void SharedStringsTable_AddEntry_mF62E41F2AFC07FA4991B727C0CE7B23132242ECC (void);
// 0x00000046 System.Void NPOI.XSSF.Model.StylesTable::SetTheme(NPOI.XSSF.Model.ThemesTable)
extern void StylesTable_SetTheme_m88B90E66A0F3A191EB5F8B4CA734848B25D823C5 (void);
// 0x00000047 System.String NPOI.XSSF.Model.StylesTable::GetNumberFormatAt(System.Int32)
extern void StylesTable_GetNumberFormatAt_mC6EBD133780177D0221DB7F0943FAE005FB4878C (void);
// 0x00000048 NPOI.XSSF.UserModel.XSSFFont NPOI.XSSF.Model.StylesTable::GetFontAt(System.Int32)
extern void StylesTable_GetFontAt_m654FDEA4A7807E082D4869EBC41A81A74AF98EB0 (void);
// 0x00000049 NPOI.XSSF.UserModel.XSSFCellStyle NPOI.XSSF.Model.StylesTable::GetStyleAt(System.Int32)
extern void StylesTable_GetStyleAt_m969FF4984E29EF31978E6F3DA496CF17373ABBFE (void);
// 0x0000004A NPOI.OpenXmlFormats.Spreadsheet.CT_Xf NPOI.XSSF.Model.StylesTable::GetCellXfAt(System.Int32)
extern void StylesTable_GetCellXfAt_m16617A6A54EA82A6200F4E91D12F9E29A106E68B (void);
// 0x0000004B NPOI.OpenXmlFormats.Spreadsheet.CT_Xf NPOI.XSSF.Model.StylesTable::GetCellStyleXfAt(System.Int32)
extern void StylesTable_GetCellStyleXfAt_mD5F4333BABB10EC6DAD02B09ADD5E25A30B86DC4 (void);
// 0x0000004C System.Int32 NPOI.XSSF.Model.StylesTable::get_NumCellStyles()
extern void StylesTable_get_NumCellStyles_m0661911112CEF768E1D0AF40D1AE0A70F59FF37D (void);
// 0x0000004D System.Void NPOI.XSSF.Model.StylesTable::.cctor()
extern void StylesTable__cctor_m4FE08E83CA7B1C3F3204C0383D009EF9190FF8EB (void);
// 0x0000004E System.Void NPOI.XSSF.UserModel.Extensions.XSSFCellBorder::SetThemesTable(NPOI.XSSF.Model.ThemesTable)
extern void XSSFCellBorder_SetThemesTable_m5DDA99E96EADAE63BA69776EECB84724D13D54C6 (void);
// 0x0000004F System.Void NPOI.XSSF.UserModel.Helpers.ColumnHelper::.ctor(NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet)
extern void ColumnHelper__ctor_m575531CECD15F090DC01544A7F74778762F0A4DD (void);
// 0x00000050 System.Void NPOI.XSSF.UserModel.Helpers.ColumnHelper::CleanColumns()
extern void ColumnHelper_CleanColumns_m4C47782E74C464DB35A626BC2E134A41A8F2CA68 (void);
// 0x00000051 System.Void NPOI.XSSF.UserModel.Helpers.ColumnHelper::SortColumns(NPOI.OpenXmlFormats.Spreadsheet.CT_Cols)
extern void ColumnHelper_SortColumns_m5F7CEA4E1798BC72E36B16C66D7C68DAF358A315 (void);
// 0x00000052 NPOI.OpenXmlFormats.Spreadsheet.CT_Col NPOI.XSSF.UserModel.Helpers.ColumnHelper::CloneCol(NPOI.OpenXmlFormats.Spreadsheet.CT_Cols,NPOI.OpenXmlFormats.Spreadsheet.CT_Col)
extern void ColumnHelper_CloneCol_m2F8AF5961E38327DD66BBCE492DC4A17AA89369D (void);
// 0x00000053 NPOI.OpenXmlFormats.Spreadsheet.CT_Cols NPOI.XSSF.UserModel.Helpers.ColumnHelper::AddCleanColIntoCols(NPOI.OpenXmlFormats.Spreadsheet.CT_Cols,NPOI.OpenXmlFormats.Spreadsheet.CT_Col)
extern void ColumnHelper_AddCleanColIntoCols_m10105405545C3C37A8429600F5943DB92EE5F18C (void);
// 0x00000054 NPOI.OpenXmlFormats.Spreadsheet.CT_Col NPOI.XSSF.UserModel.Helpers.ColumnHelper::insertCol(NPOI.OpenXmlFormats.Spreadsheet.CT_Cols,System.Int64,System.Int64,NPOI.OpenXmlFormats.Spreadsheet.CT_Col[])
extern void ColumnHelper_insertCol_m3DD5D9F0233D458F9389A8CDE7DDA1E7BA1F729D (void);
// 0x00000055 System.Void NPOI.XSSF.UserModel.Helpers.ColumnHelper::SetColumnAttributes(NPOI.OpenXmlFormats.Spreadsheet.CT_Col,NPOI.OpenXmlFormats.Spreadsheet.CT_Col)
extern void ColumnHelper_SetColumnAttributes_m4E0485EE0B04EF88FCBE84DF8028C644886AE03E (void);
// 0x00000056 System.Boolean NPOI.XSSF.UserModel.Helpers.ColumnHelper::columnExists(NPOI.OpenXmlFormats.Spreadsheet.CT_Cols,System.Int64,System.Int64)
extern void ColumnHelper_columnExists_m830601B52E3279BB3CC6C8C7A5A329670F2A30B1 (void);
// 0x00000057 System.Void NPOI.XSSF.UserModel.XSSFCell::.ctor(NPOI.XSSF.UserModel.XSSFRow,NPOI.OpenXmlFormats.Spreadsheet.CT_Cell)
extern void XSSFCell__ctor_mB9754C8B384EFD5C81AC019960AA276534D34F8B (void);
// 0x00000058 NPOI.SS.UserModel.ISheet NPOI.XSSF.UserModel.XSSFCell::get_Sheet()
extern void XSSFCell_get_Sheet_m8E114F3EC4E37191F45175A8DEEB5714CD748BA6 (void);
// 0x00000059 NPOI.SS.UserModel.IRow NPOI.XSSF.UserModel.XSSFCell::get_Row()
extern void XSSFCell_get_Row_m21F4DF4C54D7C2D797BE20BE6992B878714C6031 (void);
// 0x0000005A System.Boolean NPOI.XSSF.UserModel.XSSFCell::get_BooleanCellValue()
extern void XSSFCell_get_BooleanCellValue_m5346C90797E33DD645F706A6DB72B09864037B13 (void);
// 0x0000005B System.Void NPOI.XSSF.UserModel.XSSFCell::SetCellValue(System.Boolean)
extern void XSSFCell_SetCellValue_m006EC44EB4878721B53B9EDAABF35F5DFFEC648B (void);
// 0x0000005C System.Double NPOI.XSSF.UserModel.XSSFCell::get_NumericCellValue()
extern void XSSFCell_get_NumericCellValue_m09DF185692A9CE7C676DFB8B288F427FA2B6BB5D (void);
// 0x0000005D System.Void NPOI.XSSF.UserModel.XSSFCell::SetCellValue(System.Double)
extern void XSSFCell_SetCellValue_mEB01822A9EF815056AB70FAC182A9AA112BED957 (void);
// 0x0000005E System.String NPOI.XSSF.UserModel.XSSFCell::get_StringCellValue()
extern void XSSFCell_get_StringCellValue_mE0721EDE9204E51CB370DD5B3E521B368B6FFFE2 (void);
// 0x0000005F NPOI.SS.UserModel.IRichTextString NPOI.XSSF.UserModel.XSSFCell::get_RichStringCellValue()
extern void XSSFCell_get_RichStringCellValue_m11C368FCAA89A6175B421ED5B26E2956B6094CFE (void);
// 0x00000060 System.Void NPOI.XSSF.UserModel.XSSFCell::CheckFormulaCachedValueType(NPOI.SS.UserModel.CellType,NPOI.SS.UserModel.CellType)
extern void XSSFCell_CheckFormulaCachedValueType_m43383F8187190A448AE1772B29178E56BC8F6834 (void);
// 0x00000061 System.Void NPOI.XSSF.UserModel.XSSFCell::SetCellValue(System.String)
extern void XSSFCell_SetCellValue_mDEA033C99443435200607F08CAC6571B60DDB12D (void);
// 0x00000062 System.Void NPOI.XSSF.UserModel.XSSFCell::SetCellValue(NPOI.SS.UserModel.IRichTextString)
extern void XSSFCell_SetCellValue_m67E7C5073DD7407A5D96EECD5C23B91848CCADA1 (void);
// 0x00000063 System.String NPOI.XSSF.UserModel.XSSFCell::get_CellFormula()
extern void XSSFCell_get_CellFormula_mC236DD3E204EE8C0C51E25CE39D28F9A2B4CA973 (void);
// 0x00000064 System.String NPOI.XSSF.UserModel.XSSFCell::ConvertSharedFormula(System.Int32)
extern void XSSFCell_ConvertSharedFormula_mB8C2CC971FB4BD9E5B3388F85C25AF233A661F35 (void);
// 0x00000065 System.Int32 NPOI.XSSF.UserModel.XSSFCell::get_ColumnIndex()
extern void XSSFCell_get_ColumnIndex_m0D0B2115AA481C708E832335A540D9C18607833E (void);
// 0x00000066 System.Int32 NPOI.XSSF.UserModel.XSSFCell::get_RowIndex()
extern void XSSFCell_get_RowIndex_m13B7955899DEEFB3C1F78C16D1A168D4287FDFAA (void);
// 0x00000067 System.String NPOI.XSSF.UserModel.XSSFCell::GetReference()
extern void XSSFCell_GetReference_mF9420E93DE67493289B3EE64B1CD7A1C31346997 (void);
// 0x00000068 NPOI.SS.UserModel.ICellStyle NPOI.XSSF.UserModel.XSSFCell::get_CellStyle()
extern void XSSFCell_get_CellStyle_m4CE434C7212D3CDE3207B7DC4DCD0FD5F3AC4871 (void);
// 0x00000069 NPOI.SS.UserModel.CellType NPOI.XSSF.UserModel.XSSFCell::get_CellType()
extern void XSSFCell_get_CellType_m5F4ADC04AB62935E8FAC0D91317AA10F4DD1EFFE (void);
// 0x0000006A NPOI.SS.UserModel.CellType NPOI.XSSF.UserModel.XSSFCell::GetBaseCellType(System.Boolean)
extern void XSSFCell_GetBaseCellType_m7C3A07DA194C0B04074B88FAEF35129C88A9DC0A (void);
// 0x0000006B System.DateTime NPOI.XSSF.UserModel.XSSFCell::get_DateCellValue()
extern void XSSFCell_get_DateCellValue_mFE5E4B0A4361E4A322775B094B94BDB08AAE745E (void);
// 0x0000006C System.String NPOI.XSSF.UserModel.XSSFCell::get_ErrorCellString()
extern void XSSFCell_get_ErrorCellString_mBBBD06674605193C65F487ABD77BDB121688D345 (void);
// 0x0000006D System.Byte NPOI.XSSF.UserModel.XSSFCell::get_ErrorCellValue()
extern void XSSFCell_get_ErrorCellValue_mB26E951145A6BE17C9B6FFC04CADD7152D00AABD (void);
// 0x0000006E System.Void NPOI.XSSF.UserModel.XSSFCell::SetBlank()
extern void XSSFCell_SetBlank_m713FCD4B3F58768EC7C6704E2F147FE448C1B598 (void);
// 0x0000006F System.Void NPOI.XSSF.UserModel.XSSFCell::SetCellNum(System.Int32)
extern void XSSFCell_SetCellNum_m9D1850C3BBEDD76CC01089102ABA184E3436E5EF (void);
// 0x00000070 System.Void NPOI.XSSF.UserModel.XSSFCell::SetCellType(NPOI.SS.UserModel.CellType)
extern void XSSFCell_SetCellType_m77C4E947A59082428B06C6EC4AD7C0FF0FA030C2 (void);
// 0x00000071 System.String NPOI.XSSF.UserModel.XSSFCell::ToString()
extern void XSSFCell_ToString_mBA01D2E2F09AFF260888FA5138D451618E0CE8CF (void);
// 0x00000072 System.String NPOI.XSSF.UserModel.XSSFCell::GetCellTypeName(NPOI.SS.UserModel.CellType)
extern void XSSFCell_GetCellTypeName_m121DA1E47E46C2822B9135DB474A7CCEE0BEE372 (void);
// 0x00000073 System.Exception NPOI.XSSF.UserModel.XSSFCell::TypeMismatch(NPOI.SS.UserModel.CellType,NPOI.SS.UserModel.CellType,System.Boolean)
extern void XSSFCell_TypeMismatch_mF14E005E515AD3CB5B3A00AA162C2F163FCFC1CF (void);
// 0x00000074 System.Void NPOI.XSSF.UserModel.XSSFCell::CheckBounds(System.Int32)
extern void XSSFCell_CheckBounds_m941D37677696B4A6CDD1644B472B9275DA4FD7A8 (void);
// 0x00000075 NPOI.OpenXmlFormats.Spreadsheet.CT_Cell NPOI.XSSF.UserModel.XSSFCell::GetCTCell()
extern void XSSFCell_GetCTCell_m9D36BB039DF7277793A4ACDFD87EC6E0A57C5F5F (void);
// 0x00000076 System.Boolean NPOI.XSSF.UserModel.XSSFCell::ConvertCellValueToBoolean()
extern void XSSFCell_ConvertCellValueToBoolean_m7AB3788DDEF0AF16E7299A77C4E36CAA2FD5BAA1 (void);
// 0x00000077 System.String NPOI.XSSF.UserModel.XSSFCell::ConvertCellValueToString()
extern void XSSFCell_ConvertCellValueToString_mBB3D2EDC97AF12B7EDC87DED36B60901C2B0F870 (void);
// 0x00000078 NPOI.SS.Util.CellRangeAddress NPOI.XSSF.UserModel.XSSFCell::get_ArrayFormulaRange()
extern void XSSFCell_get_ArrayFormulaRange_m60695A97B2E09291A374B1015A022EAE7BEE5328 (void);
// 0x00000079 System.Boolean NPOI.XSSF.UserModel.XSSFCell::get_IsPartOfArrayFormulaGroup()
extern void XSSFCell_get_IsPartOfArrayFormulaGroup_m5880F5DE755AF995051D7BD381EFE453F83D65A2 (void);
// 0x0000007A System.Void NPOI.XSSF.UserModel.XSSFCell::NotifyArrayFormulaChanging(System.String)
extern void XSSFCell_NotifyArrayFormulaChanging_m3175A219B21BCA55B26C7F68782F2F2B4DDC15EC (void);
// 0x0000007B System.Void NPOI.XSSF.UserModel.XSSFCell::NotifyArrayFormulaChanging()
extern void XSSFCell_NotifyArrayFormulaChanging_m47B665B80BEEF4F6C8ED08B7E466AE4B29BA7D98 (void);
// 0x0000007C System.Void NPOI.XSSF.UserModel.XSSFCell::.cctor()
extern void XSSFCell__cctor_mBEB7376C222B563DC9C59A8BC315460E2B8EDCB9 (void);
// 0x0000007D System.Void NPOI.XSSF.UserModel.XSSFCellStyle::.ctor(System.Int32,System.Int32,NPOI.XSSF.Model.StylesTable,NPOI.XSSF.Model.ThemesTable)
extern void XSSFCellStyle__ctor_m6A79380655A3B78FED4059563A41082B47215652 (void);
// 0x0000007E NPOI.OpenXmlFormats.Spreadsheet.CT_Xf NPOI.XSSF.UserModel.XSSFCellStyle::GetCoreXf()
extern void XSSFCellStyle_GetCoreXf_mCA0ADE3352B55207616A851852FE15FC8F6A0BAB (void);
// 0x0000007F System.Int16 NPOI.XSSF.UserModel.XSSFCellStyle::get_DataFormat()
extern void XSSFCellStyle_get_DataFormat_m16E7ECD10346C93384CB94663B46368D6E275EB9 (void);
// 0x00000080 System.String NPOI.XSSF.UserModel.XSSFCellStyle::GetDataFormatString()
extern void XSSFCellStyle_GetDataFormatString_m7A9F45371EAB66BE9E94D584BCCDC8874A3EFB06 (void);
// 0x00000081 System.Int32 NPOI.XSSF.UserModel.XSSFCellStyle::GetHashCode()
extern void XSSFCellStyle_GetHashCode_mA80711740DF84C42387DD7B93D4AE4A86241A122 (void);
// 0x00000082 System.Boolean NPOI.XSSF.UserModel.XSSFCellStyle::Equals(System.Object)
extern void XSSFCellStyle_Equals_m74957AD38B97B98BE0218B1B968AEA39373298B5 (void);
// 0x00000083 System.Void NPOI.XSSF.UserModel.XSSFSheet::.ctor()
extern void XSSFSheet__ctor_m19FC1FF55C9165530F6143F295D571AAEBA97113 (void);
// 0x00000084 NPOI.SS.UserModel.IWorkbook NPOI.XSSF.UserModel.XSSFSheet::get_Workbook()
extern void XSSFSheet_get_Workbook_m973E4FECBB9B808687CB8BB4B2949B688CA19E9E (void);
// 0x00000085 System.Void NPOI.XSSF.UserModel.XSSFSheet::OnDocumentRead()
extern void XSSFSheet_OnDocumentRead_m1CA72ED5D6A16B3BC02D4E0AB86101E8966ED1B1 (void);
// 0x00000086 System.Void NPOI.XSSF.UserModel.XSSFSheet::Read(System.IO.Stream)
extern void XSSFSheet_Read_mC3A5B13B07103DDED1BB937AFEEAD630DA049037 (void);
// 0x00000087 System.Void NPOI.XSSF.UserModel.XSSFSheet::OnDocumentCreate()
extern void XSSFSheet_OnDocumentCreate_mBF8D38014D4037A046C83AA6D8775DBE8129F83E (void);
// 0x00000088 System.Void NPOI.XSSF.UserModel.XSSFSheet::InitRows(NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet)
extern void XSSFSheet_InitRows_m2455387DD1474516CB0204CB780B5D59E9FFEA36 (void);
// 0x00000089 System.Void NPOI.XSSF.UserModel.XSSFSheet::InitHyperlinks()
extern void XSSFSheet_InitHyperlinks_mA2D5565D9C6E2A0C7C3855907D5D8FBFA26743BF (void);
// 0x0000008A NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet NPOI.XSSF.UserModel.XSSFSheet::NewSheet()
extern void XSSFSheet_NewSheet_m377EFC50964E3A1334A409345A55FA0FB81ED907 (void);
// 0x0000008B System.String NPOI.XSSF.UserModel.XSSFSheet::get_SheetName()
extern void XSSFSheet_get_SheetName_m83A6422E3383A3082F3358C60EA199CD54F93424 (void);
// 0x0000008C System.Int32 NPOI.XSSF.UserModel.XSSFSheet::GetLastKey(System.Collections.Generic.IList`1<System.Int32>)
extern void XSSFSheet_GetLastKey_m8BD2BFE9BC108B295250A9ED6EAF71254A7C2334 (void);
// 0x0000008D System.Collections.Generic.SortedList`2<System.Int32,NPOI.XSSF.UserModel.XSSFRow> NPOI.XSSF.UserModel.XSSFSheet::HeadMap(System.Collections.Generic.SortedList`2<System.Int32,NPOI.XSSF.UserModel.XSSFRow>,System.Int32)
extern void XSSFSheet_HeadMap_m40D40CFB2C42350FB4C390FD5E8DD2628247A4CA (void);
// 0x0000008E NPOI.SS.UserModel.IRow NPOI.XSSF.UserModel.XSSFSheet::CreateRow(System.Int32)
extern void XSSFSheet_CreateRow_m4C926C49F6F700838E0E96A86C5C0102FEE38174 (void);
// 0x0000008F NPOI.SS.UserModel.IRow NPOI.XSSF.UserModel.XSSFSheet::GetRow(System.Int32)
extern void XSSFSheet_GetRow_m5C46CCFF008AC52DFC2048321DAAC191CFCB3409 (void);
// 0x00000090 NPOI.OpenXmlFormats.Spreadsheet.CT_CellFormula NPOI.XSSF.UserModel.XSSFSheet::GetSharedFormula(System.Int32)
extern void XSSFSheet_GetSharedFormula_m75B6EDEC0217054DB7CB627795BBF8F664A27245 (void);
// 0x00000091 System.Void NPOI.XSSF.UserModel.XSSFSheet::OnReadCell(NPOI.XSSF.UserModel.XSSFCell)
extern void XSSFSheet_OnReadCell_m1788436E3BBE5FB81202A93383459C6D2BFA9AF1 (void);
// 0x00000092 System.Void NPOI.XSSF.UserModel.XSSFSheet::Commit()
extern void XSSFSheet_Commit_m3208FDB01F94AEC1292D20A7DD4EFBAFA7C5F21B (void);
// 0x00000093 System.Void NPOI.XSSF.UserModel.XSSFSheet::Write(System.IO.Stream)
extern void XSSFSheet_Write_m514EE36D018C19E9CBAEA403F28B323C708EEB28 (void);
// 0x00000094 System.Boolean NPOI.XSSF.UserModel.XSSFSheet::IsCellInArrayFormulaContext(NPOI.SS.UserModel.ICell)
extern void XSSFSheet_IsCellInArrayFormulaContext_m985F457CB7E73642B7B32A0094D7D0EC721A22C5 (void);
// 0x00000095 NPOI.XSSF.UserModel.XSSFCell NPOI.XSSF.UserModel.XSSFSheet::GetFirstCellInArrayFormula(NPOI.SS.UserModel.ICell)
extern void XSSFSheet_GetFirstCellInArrayFormula_mC7FB2001F40BF6967D8A6B2BDA79649535262F44 (void);
// 0x00000096 NPOI.SS.UserModel.ICellRange`1<NPOI.SS.UserModel.ICell> NPOI.XSSF.UserModel.XSSFSheet::GetCellRange(NPOI.SS.Util.CellRangeAddress)
extern void XSSFSheet_GetCellRange_mDCD69EDF0F1A0CDAD71278CFA94F903921ED4DDB (void);
// 0x00000097 NPOI.SS.UserModel.ICellRange`1<NPOI.SS.UserModel.ICell> NPOI.XSSF.UserModel.XSSFSheet::RemoveArrayFormula(NPOI.SS.UserModel.ICell)
extern void XSSFSheet_RemoveArrayFormula_m1A522BAE8BEF1E21D36E2E7ED61B567FD4E2C0E1 (void);
// 0x00000098 System.Void NPOI.XSSF.UserModel.XSSFSheet::.cctor()
extern void XSSFSheet__cctor_m22416C4D4A777684C86C5E6CB29C2D3F1041867E (void);
// 0x00000099 System.Byte[] NPOI.XSSF.UserModel.XSSFChartSheet::blankWorksheet()
extern void XSSFChartSheet_blankWorksheet_m1D9881BDCFFF46E488E26285E312C3BCAA57ED0F (void);
// 0x0000009A System.Void NPOI.XSSF.UserModel.XSSFChartSheet::.cctor()
extern void XSSFChartSheet__cctor_mB3E13C2833741A69C245F352E2EDFDB4B4385C3C (void);
// 0x0000009B System.Void NPOI.XSSF.UserModel.XSSFDataFormat::.ctor(NPOI.XSSF.Model.StylesTable)
extern void XSSFDataFormat__ctor_m154866A4F54E64056E911B55EA25A0BBD2338F06 (void);
// 0x0000009C System.String NPOI.XSSF.UserModel.XSSFDataFormat::GetFormat(System.Int16)
extern void XSSFDataFormat_GetFormat_m7780941725BDC00498C76BF297DD1C7320512198 (void);
// 0x0000009D System.Void NPOI.XSSF.UserModel.XSSFDataValidationHelper::.ctor(NPOI.XSSF.UserModel.XSSFSheet)
extern void XSSFDataValidationHelper__ctor_mD89CE0249D3D0CD1323DC59726B3A535C6EF7EDA (void);
// 0x0000009E NPOI.XSSF.UserModel.XSSFEvaluationWorkbook NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::Create(NPOI.SS.UserModel.IWorkbook)
extern void XSSFEvaluationWorkbook_Create_m9DFA7C9209A073186D275854537F607BEDC40328 (void);
// 0x0000009F System.Void NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::.ctor(NPOI.SS.UserModel.IWorkbook)
extern void XSSFEvaluationWorkbook__ctor_mA423583159D63F594DDE62244D143CDE5EA02695 (void);
// 0x000000A0 System.Int32 NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::ConvertFromExternalSheetIndex(System.Int32)
extern void XSSFEvaluationWorkbook_ConvertFromExternalSheetIndex_mF125C442F3BFC1ED51368C16C5DF08CF2EEFFF1B (void);
// 0x000000A1 System.Int32 NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::ConvertToExternalSheetIndex(System.Int32)
extern void XSSFEvaluationWorkbook_ConvertToExternalSheetIndex_m42DE8521C35487B8F55DC0AD73A2F2457B7152A0 (void);
// 0x000000A2 System.Int32 NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::GetExternalSheetIndex(System.String)
extern void XSSFEvaluationWorkbook_GetExternalSheetIndex_mD884497C1DD7AB2A17BB035A67E3DBF446D2F3D3 (void);
// 0x000000A3 NPOI.SS.Formula.IEvaluationName NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::GetName(System.String,System.Int32)
extern void XSSFEvaluationWorkbook_GetName_m0B56ED380212B37FBCCBE0171D4F4638B041D980 (void);
// 0x000000A4 NPOI.SS.Formula.PTG.NameXPtg NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::GetNameXPtg(System.String)
extern void XSSFEvaluationWorkbook_GetNameXPtg_m93360D65BA5414140908DEDCAABD19031E71DD38 (void);
// 0x000000A5 System.String NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::ResolveNameXText(NPOI.SS.Formula.PTG.NameXPtg)
extern void XSSFEvaluationWorkbook_ResolveNameXText_mAFC3A5F204BBF1B38E98BD8EB6D56539B38AA363 (void);
// 0x000000A6 NPOI.SS.Formula.ExternalSheet NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::GetExternalSheet(System.Int32)
extern void XSSFEvaluationWorkbook_GetExternalSheet_mAAD6D62B250AFC63D4CE94490DFA6ED15DF8416D (void);
// 0x000000A7 System.Int32 NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::GetExternalSheetIndex(System.String,System.String)
extern void XSSFEvaluationWorkbook_GetExternalSheetIndex_m1A68B752712110EB32202FC2B728B9CB31DC4EAA (void);
// 0x000000A8 System.String NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::GetSheetNameByExternSheet(System.Int32)
extern void XSSFEvaluationWorkbook_GetSheetNameByExternSheet_m272011D2821193BAD078B23E5B1B70B4CDA2F331 (void);
// 0x000000A9 System.String NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::GetNameText(NPOI.SS.Formula.PTG.NamePtg)
extern void XSSFEvaluationWorkbook_GetNameText_mAE50D876161CB3C8693749EB527622A68FE55019 (void);
// 0x000000AA NPOI.SS.Formula.Udf.UDFFinder NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::GetUDFFinder()
extern void XSSFEvaluationWorkbook_GetUDFFinder_m1FE2D890342FB9B4179C0FDB9481118640BD1329 (void);
// 0x000000AB NPOI.SS.SpreadsheetVersion NPOI.XSSF.UserModel.XSSFEvaluationWorkbook::GetSpreadsheetVersion()
extern void XSSFEvaluationWorkbook_GetSpreadsheetVersion_m3CB4BF3536631E95549D7C6188B39C13ADFABA42 (void);
// 0x000000AC System.Void NPOI.XSSF.UserModel.XSSFEvaluationWorkbook_Name::.ctor(NPOI.SS.UserModel.IName,System.Int32,NPOI.SS.Formula.IFormulaParsingWorkbook)
extern void Name__ctor_mCBF61CC348DFA7776BE399D410F05D39760B3918 (void);
// 0x000000AD System.Boolean NPOI.XSSF.UserModel.XSSFEvaluationWorkbook_Name::get_HasFormula()
extern void Name_get_HasFormula_mB534597E147580BBE6FA84F436CC0B2D820ACFD5 (void);
// 0x000000AE System.Boolean NPOI.XSSF.UserModel.XSSFEvaluationWorkbook_Name::get_IsFunctionName()
extern void Name_get_IsFunctionName_mA7C9074C06644336E7E4B87A7D84A6834FCCD5B5 (void);
// 0x000000AF System.Boolean NPOI.XSSF.UserModel.XSSFEvaluationWorkbook_Name::get_IsRange()
extern void Name_get_IsRange_m32F8839CBD3026097532BF51EA03F542034F7C17 (void);
// 0x000000B0 NPOI.SS.Formula.PTG.NamePtg NPOI.XSSF.UserModel.XSSFEvaluationWorkbook_Name::CreatePtg()
extern void Name_CreatePtg_m2E598375DBAEDC923A3F424C97FCCDB2035F3D1A (void);
// 0x000000B1 System.Void NPOI.XSSF.UserModel.XSSFFactory::.ctor()
extern void XSSFFactory__ctor_m9DCA7515B99A4263366917D4C27626CDC7169EB5 (void);
// 0x000000B2 NPOI.XSSF.UserModel.XSSFFactory NPOI.XSSF.UserModel.XSSFFactory::GetInstance()
extern void XSSFFactory_GetInstance_m7EDF92040038A4F235F988A75EAD85A0B989199D (void);
// 0x000000B3 NPOI.POIXMLDocumentPart NPOI.XSSF.UserModel.XSSFFactory::CreateDocumentPart(NPOI.POIXMLDocumentPart,NPOI.OpenXml4Net.OPC.PackageRelationship,NPOI.OpenXml4Net.OPC.PackagePart)
extern void XSSFFactory_CreateDocumentPart_m34966646865CA8A02851632D7C6D82A0F8EFE550 (void);
// 0x000000B4 NPOI.POIXMLDocumentPart NPOI.XSSF.UserModel.XSSFFactory::CreateDocumentPart(NPOI.POIXMLRelation)
extern void XSSFFactory_CreateDocumentPart_mDD88E1AD3308FE1BDE82D9B5EF5FE925AEB4C31C (void);
// 0x000000B5 System.Void NPOI.XSSF.UserModel.XSSFFactory::.cctor()
extern void XSSFFactory__cctor_mDDF69C11276F8983DBBCBFB85C7613F2ADDD3D4F (void);
// 0x000000B6 NPOI.OpenXmlFormats.Spreadsheet.CT_Font NPOI.XSSF.UserModel.XSSFFont::GetCTFont()
extern void XSSFFont_GetCTFont_mF90EBCA95FB4F35BC0646E48097BFDC7053310C3 (void);
// 0x000000B7 System.Void NPOI.XSSF.UserModel.XSSFFont::SetThemesTable(NPOI.XSSF.Model.ThemesTable)
extern void XSSFFont_SetThemesTable_mA596E2035750D3A5E1E60AAB960B8C2648A7CAFC (void);
// 0x000000B8 System.Void NPOI.XSSF.UserModel.XSSFFont::.cctor()
extern void XSSFFont__cctor_mDFA948F083421924F4202C14BE157D9AFD3C82EE (void);
// 0x000000B9 System.Void NPOI.XSSF.UserModel.XSSFName::.ctor(NPOI.OpenXmlFormats.Spreadsheet.CT_DefinedName,NPOI.XSSF.UserModel.XSSFWorkbook)
extern void XSSFName__ctor_m031B6704625BE5F4E9B15697A626DADBB110121D (void);
// 0x000000BA NPOI.OpenXmlFormats.Spreadsheet.CT_DefinedName NPOI.XSSF.UserModel.XSSFName::GetCTName()
extern void XSSFName_GetCTName_m1D5F0FC22AC573340F83FFEA789FED28DF1DEC82 (void);
// 0x000000BB System.String NPOI.XSSF.UserModel.XSSFName::get_NameName()
extern void XSSFName_get_NameName_m785A63BE9FD8E8A0093D9926AFBB8CFAB845DFDF (void);
// 0x000000BC System.Int32 NPOI.XSSF.UserModel.XSSFName::get_SheetIndex()
extern void XSSFName_get_SheetIndex_m1FC17F4E3ECDA9091CB86D07870BC8DF62B87E64 (void);
// 0x000000BD System.Boolean NPOI.XSSF.UserModel.XSSFName::get_Function()
extern void XSSFName_get_Function_m8F790234A59850F00091FB22E3EBF5D11382B6C2 (void);
// 0x000000BE System.Boolean NPOI.XSSF.UserModel.XSSFName::get_IsFunctionName()
extern void XSSFName_get_IsFunctionName_mD4D4A54D3B47209167E5D1C8200B9490A41D7C5B (void);
// 0x000000BF System.Int32 NPOI.XSSF.UserModel.XSSFName::GetHashCode()
extern void XSSFName_GetHashCode_m818AF7CBD4D0FB2403A0B70A3C447AB62C13CE17 (void);
// 0x000000C0 System.Boolean NPOI.XSSF.UserModel.XSSFName::Equals(System.Object)
extern void XSSFName_Equals_mC5442C2FBD14F94367859B11DCF22635FDC140AA (void);
// 0x000000C1 System.Void NPOI.XSSF.UserModel.XSSFName::.cctor()
extern void XSSFName__cctor_m6821233721562F5AD1F579C9687101F5AA241DB1 (void);
// 0x000000C2 System.Void NPOI.XSSF.UserModel.XSSFHyperlink::.ctor(NPOI.OpenXmlFormats.Spreadsheet.CT_Hyperlink,NPOI.OpenXml4Net.OPC.PackageRelationship)
extern void XSSFHyperlink__ctor_mFA49FC8AE5AC520BCC7B08736CE66AD24FA1DC1E (void);
// 0x000000C3 NPOI.OpenXmlFormats.Spreadsheet.CT_Hyperlink NPOI.XSSF.UserModel.XSSFHyperlink::GetCTHyperlink()
extern void XSSFHyperlink_GetCTHyperlink_mDBE6CB4F438142681A26359F913A58BDBF9B22A3 (void);
// 0x000000C4 System.Boolean NPOI.XSSF.UserModel.XSSFHyperlink::NeedsRelationToo()
extern void XSSFHyperlink_NeedsRelationToo_mCDDE617A36C15AE600AB6E06A11867AEA87F973B (void);
// 0x000000C5 System.Void NPOI.XSSF.UserModel.XSSFHyperlink::GenerateRelationIfNeeded(NPOI.OpenXml4Net.OPC.PackagePart)
extern void XSSFHyperlink_GenerateRelationIfNeeded_m7DC845976AEA11A87C2719312A22017B6F759E3F (void);
// 0x000000C6 System.Void NPOI.XSSF.UserModel.XSSFPictureData::.cctor()
extern void XSSFPictureData__cctor_m886E340F971099DD211ED0F162553251920F5415 (void);
// 0x000000C7 System.Void NPOI.XSSF.UserModel.XSSFRelation::.ctor(System.String,System.String,System.String,System.Type)
extern void XSSFRelation__ctor_mE6C40378A119DD7DC5244B065C71B4F0CC3F8C65 (void);
// 0x000000C8 NPOI.XSSF.UserModel.XSSFRelation NPOI.XSSF.UserModel.XSSFRelation::GetInstance(System.String)
extern void XSSFRelation_GetInstance_m0485CCEB044F36A7870F0E0C62332D9CA455A8FF (void);
// 0x000000C9 System.Void NPOI.XSSF.UserModel.XSSFRelation::.cctor()
extern void XSSFRelation__cctor_m6592F9F94D4A324642C614F90F740D315D1EC56A (void);
// 0x000000CA System.Void NPOI.XSSF.UserModel.XSSFRichTextString::.ctor(System.String)
extern void XSSFRichTextString__ctor_mFD017350E8A2078F9C72DEF31102F162BF7261C3 (void);
// 0x000000CB System.Void NPOI.XSSF.UserModel.XSSFRichTextString::SetStylesTableReference(NPOI.XSSF.Model.StylesTable)
extern void XSSFRichTextString_SetStylesTableReference_m223D7616ADAD462F01665E42D91C12E7C176D7CF (void);
// 0x000000CC System.Void NPOI.XSSF.UserModel.XSSFRichTextString::.ctor(NPOI.OpenXmlFormats.Spreadsheet.CT_Rst)
extern void XSSFRichTextString__ctor_m441C2DF5478676568FF898F5A3542D5CFEAD1563 (void);
// 0x000000CD System.Void NPOI.XSSF.UserModel.XSSFRichTextString::SetRunAttributes(NPOI.OpenXmlFormats.Spreadsheet.CT_Font,NPOI.OpenXmlFormats.Spreadsheet.CT_RPrElt)
extern void XSSFRichTextString_SetRunAttributes_mA1051E1EA018355A22CA8301ACDC706B6C8EC618 (void);
// 0x000000CE System.Int32 NPOI.XSSF.UserModel.XSSFRichTextString::GetIndexOfFormattingRun(System.Int32)
extern void XSSFRichTextString_GetIndexOfFormattingRun_m54B529CFE3C8C339ADB848AE2B957421A8873CD8 (void);
// 0x000000CF System.String NPOI.XSSF.UserModel.XSSFRichTextString::get_String()
extern void XSSFRichTextString_get_String_mD3F8F178928319BE1F06DCDB57AC0CAFDC5EA8F8 (void);
// 0x000000D0 System.String NPOI.XSSF.UserModel.XSSFRichTextString::ToString()
extern void XSSFRichTextString_ToString_mCE6E4D7271F974E4F0B9A34D81622FF56CDDFEF0 (void);
// 0x000000D1 System.Int32 NPOI.XSSF.UserModel.XSSFRichTextString::get_Length()
extern void XSSFRichTextString_get_Length_m61AA298D58D30C083029918473616DF5A7D98421 (void);
// 0x000000D2 System.Int32 NPOI.XSSF.UserModel.XSSFRichTextString::get_NumFormattingRuns()
extern void XSSFRichTextString_get_NumFormattingRuns_mBA361C07C5482739193594F59C33F69B47CD750B (void);
// 0x000000D3 NPOI.OpenXmlFormats.Spreadsheet.CT_Rst NPOI.XSSF.UserModel.XSSFRichTextString::GetCTRst()
extern void XSSFRichTextString_GetCTRst_mA1C73F382F97769E777396DCEAEDD943349F169A (void);
// 0x000000D4 System.Void NPOI.XSSF.UserModel.XSSFRichTextString::PreserveSpaces(System.String)
extern void XSSFRichTextString_PreserveSpaces_m0F314D4318BFDE8B732B310EC197505D120A100C (void);
// 0x000000D5 System.String NPOI.XSSF.UserModel.XSSFRichTextString::UtfDecode(System.String)
extern void XSSFRichTextString_UtfDecode_mD8BD3D36199EB766BBA78A065DF9A4113975A522 (void);
// 0x000000D6 System.Void NPOI.XSSF.UserModel.XSSFRichTextString::.cctor()
extern void XSSFRichTextString__cctor_m2F289178DFF374F99FEB20148BC520C901A92F8D (void);
// 0x000000D7 System.Void NPOI.XSSF.UserModel.XSSFRow::.ctor(NPOI.OpenXmlFormats.Spreadsheet.CT_Row,NPOI.XSSF.UserModel.XSSFSheet)
extern void XSSFRow__ctor_mBCBF6DEBFD52FF5426CC5ABF8CF9C0C0535EEF99 (void);
// 0x000000D8 NPOI.SS.UserModel.ISheet NPOI.XSSF.UserModel.XSSFRow::get_Sheet()
extern void XSSFRow_get_Sheet_mA0F66B76BC8EFD591BCE29757BD26C0B21EAF8B8 (void);
// 0x000000D9 System.Int32 NPOI.XSSF.UserModel.XSSFRow::CompareTo(NPOI.XSSF.UserModel.XSSFRow)
extern void XSSFRow_CompareTo_m5A33D2DED9F7ECC970D5D6A4B96D274B9BD2313F (void);
// 0x000000DA NPOI.SS.UserModel.ICell NPOI.XSSF.UserModel.XSSFRow::CreateCell(System.Int32)
extern void XSSFRow_CreateCell_mEDC195BF78D9DCCCE28A38DFC770CA4F1D164440 (void);
// 0x000000DB NPOI.SS.UserModel.ICell NPOI.XSSF.UserModel.XSSFRow::CreateCell(System.Int32,NPOI.SS.UserModel.CellType)
extern void XSSFRow_CreateCell_m3BDA3E6F84C1F7C0CE6B07ACA0ED01491D97C34B (void);
// 0x000000DC NPOI.SS.UserModel.ICell NPOI.XSSF.UserModel.XSSFRow::GetCell(System.Int32)
extern void XSSFRow_GetCell_m811FB693E916444348B7C4341CC7B6C37E7A9616 (void);
// 0x000000DD NPOI.SS.UserModel.ICell NPOI.XSSF.UserModel.XSSFRow::RetrieveCell(System.Int32)
extern void XSSFRow_RetrieveCell_mD34782670FCA90773E901251BADBA28B26F9B0F4 (void);
// 0x000000DE NPOI.SS.UserModel.ICell NPOI.XSSF.UserModel.XSSFRow::GetCell(System.Int32,NPOI.SS.UserModel.MissingCellPolicy)
extern void XSSFRow_GetCell_mEEECD3A2DB6048D82056954B15504E50D224FCA2 (void);
// 0x000000DF System.Int32 NPOI.XSSF.UserModel.XSSFRow::GetLastKey(System.Collections.Generic.SortedDictionary`2_KeyCollection<System.Int32,NPOI.SS.UserModel.ICell>)
extern void XSSFRow_GetLastKey_m11464F715651EBE8CA7093792B8108C54AD83708 (void);
// 0x000000E0 System.Int16 NPOI.XSSF.UserModel.XSSFRow::get_LastCellNum()
extern void XSSFRow_get_LastCellNum_m595E9C09198914CC7815BDE5EF5FEF2B06E028D6 (void);
// 0x000000E1 System.Int32 NPOI.XSSF.UserModel.XSSFRow::get_RowNum()
extern void XSSFRow_get_RowNum_mC89E0D6EC45DE46C23ACB24233BE718A2A8FF458 (void);
// 0x000000E2 System.Void NPOI.XSSF.UserModel.XSSFRow::set_RowNum(System.Int32)
extern void XSSFRow_set_RowNum_mCF2D412D0B00EEDE38C48254EC1A4A10BB7C7B1A (void);
// 0x000000E3 NPOI.OpenXmlFormats.Spreadsheet.CT_Row NPOI.XSSF.UserModel.XSSFRow::GetCTRow()
extern void XSSFRow_GetCTRow_m8FB4D55D909CE4BF80C57FB52BB1C5D0BB462275 (void);
// 0x000000E4 System.Void NPOI.XSSF.UserModel.XSSFRow::OnDocumentWrite()
extern void XSSFRow_OnDocumentWrite_m97025FCC531FF687A733FA8F15F0789AD9D40ACA (void);
// 0x000000E5 System.String NPOI.XSSF.UserModel.XSSFRow::ToString()
extern void XSSFRow_ToString_m085611080127A2F4B9B71666563DC5855A76CAF9 (void);
// 0x000000E6 System.Void NPOI.XSSF.UserModel.XSSFRow::.cctor()
extern void XSSFRow__cctor_m7484C58B18F02836A2433F8F9EBD88A05C4B343C (void);
// 0x000000E7 System.Void NPOI.XSSF.UserModel.XSSFVMLDrawing::.cctor()
extern void XSSFVMLDrawing__cctor_m3A8540D81FC9E1CECCAEBF94CFF8ABC30434BEB2 (void);
// 0x000000E8 System.Void NPOI.XSSF.UserModel.XSSFWorkbook::.ctor(System.IO.Stream)
extern void XSSFWorkbook__ctor_m7942004F5D4B76DBD538CDD3487B019A831914C8 (void);
// 0x000000E9 System.Void NPOI.XSSF.UserModel.XSSFWorkbook::OnDocumentRead()
extern void XSSFWorkbook_OnDocumentRead_m580FABCD07BF17477B6A90C2382FEC19D20A526D (void);
// 0x000000EA NPOI.SS.UserModel.IName NPOI.XSSF.UserModel.XSSFWorkbook::GetNameAt(System.Int32)
extern void XSSFWorkbook_GetNameAt_m2096A82CD6F564787D89088EA7955E58594CD62A (void);
// 0x000000EB System.Int32 NPOI.XSSF.UserModel.XSSFWorkbook::get_NumberOfNames()
extern void XSSFWorkbook_get_NumberOfNames_m66702D991F59B2395ED482FEFB521D82D6A4D1D5 (void);
// 0x000000EC NPOI.SS.UserModel.ISheet NPOI.XSSF.UserModel.XSSFWorkbook::GetSheet(System.String)
extern void XSSFWorkbook_GetSheet_m1ED9132DB9AB4F086A5225279AA19681D9E8985D (void);
// 0x000000ED System.Int32 NPOI.XSSF.UserModel.XSSFWorkbook::GetSheetIndex(System.String)
extern void XSSFWorkbook_GetSheetIndex_m8B99E63F4DDF960B1C2107AEAC980DCB7276BE56 (void);
// 0x000000EE System.Int32 NPOI.XSSF.UserModel.XSSFWorkbook::GetSheetIndex(NPOI.SS.UserModel.ISheet)
extern void XSSFWorkbook_GetSheetIndex_mE0AABAFF93A1C2C6809BCD8A6929AA579C021B01 (void);
// 0x000000EF System.String NPOI.XSSF.UserModel.XSSFWorkbook::GetSheetName(System.Int32)
extern void XSSFWorkbook_GetSheetName_mECC4097663FD7FA2B543690525A6391CF290818A (void);
// 0x000000F0 NPOI.SS.UserModel.MissingCellPolicy NPOI.XSSF.UserModel.XSSFWorkbook::get_MissingCellPolicy()
extern void XSSFWorkbook_get_MissingCellPolicy_mC1E5917BAEB4ABBDD740358798478F3B15E014B6 (void);
// 0x000000F1 System.Void NPOI.XSSF.UserModel.XSSFWorkbook::ValidateSheetIndex(System.Int32)
extern void XSSFWorkbook_ValidateSheetIndex_m643605341F0C35BB093110609B4838914C80FFCF (void);
// 0x000000F2 System.Void NPOI.XSSF.UserModel.XSSFWorkbook::SaveNamedRanges()
extern void XSSFWorkbook_SaveNamedRanges_m5390D2940293EAB5452E52E7F2E335FFB8E1E8B3 (void);
// 0x000000F3 System.Void NPOI.XSSF.UserModel.XSSFWorkbook::SaveCalculationChain()
extern void XSSFWorkbook_SaveCalculationChain_m82D774BBEDC805E35C9C6118F2F32AC659CC5708 (void);
// 0x000000F4 System.Void NPOI.XSSF.UserModel.XSSFWorkbook::Commit()
extern void XSSFWorkbook_Commit_mDD81888B738F28E8680CF71717D12612B1EAD5C0 (void);
// 0x000000F5 NPOI.XSSF.Model.SharedStringsTable NPOI.XSSF.UserModel.XSSFWorkbook::GetSharedStringSource()
extern void XSSFWorkbook_GetSharedStringSource_m1CB0C23C6EC91AD02F683861177D477F243BF327 (void);
// 0x000000F6 NPOI.XSSF.Model.StylesTable NPOI.XSSF.UserModel.XSSFWorkbook::GetStylesSource()
extern void XSSFWorkbook_GetStylesSource_mA8F66FC35700E468DBB169E28F7AB0B7540B274D (void);
// 0x000000F7 System.Boolean NPOI.XSSF.UserModel.XSSFWorkbook::IsDate1904()
extern void XSSFWorkbook_IsDate1904_mE6AFEA598D6B29526CC81BFA23DAD9564E05A9C2 (void);
// 0x000000F8 System.Void NPOI.XSSF.UserModel.XSSFWorkbook::OnDeleteFormula(NPOI.XSSF.UserModel.XSSFCell)
extern void XSSFWorkbook_OnDeleteFormula_m25B8D5BE5ECA50077C1D5E437D6846E75B1BE62B (void);
// 0x000000F9 NPOI.SS.Formula.Udf.UDFFinder NPOI.XSSF.UserModel.XSSFWorkbook::GetUDFFinder()
extern void XSSFWorkbook_GetUDFFinder_m4B908DB2CEAE450872885355213070CC54D30D07 (void);
// 0x000000FA System.Void NPOI.XSSF.UserModel.XSSFWorkbook::.cctor()
extern void XSSFWorkbook__cctor_mF81EEB6D684E6AF60FAC6753A2249A4BF16F1FD1 (void);
// 0x000000FB System.Int32 NPOI.XSSF.Util.CTColComparator::Compare(NPOI.OpenXmlFormats.Spreadsheet.CT_Col,NPOI.OpenXmlFormats.Spreadsheet.CT_Col)
extern void CTColComparator_Compare_mBBAE0BF4BBF6AD5305EFADCC9F9EB32551B52431 (void);
// 0x000000FC System.Void NPOI.XSSF.Util.CTColComparator::.ctor()
extern void CTColComparator__ctor_mF51A75272E0543AC64E882D17D3D9DF4F2A601BC (void);
// 0x000000FD System.Int64[] NPOI.XSSF.Util.NumericRanges::GetOverlappingRange(System.Int64[],System.Int64[])
extern void NumericRanges_GetOverlappingRange_mF60B3C35FF742B2C9E3BFB0CBF63B8F069D87A6E (void);
// 0x000000FE System.Int32 NPOI.XSSF.Util.NumericRanges::GetOverlappingType(System.Int64[],System.Int64[])
extern void NumericRanges_GetOverlappingType_mE80223E3A313C47C51DBDB612399BC097D0DB561 (void);
static Il2CppMethodPointer s_methodPointers[254] = 
{
	POIXMLDocumentPart_IncrementRelationCounter_mAA9E19E2C03A177BEED92FC5DEE42944E489B104,
	POIXMLDocumentPart_DecrementRelationCounter_m35E96A5398A8C34D80EE72DA8E220363BC2AEF1A,
	POIXMLDocumentPart_GetRelationCounter_mE9D85EDE265216347F097E7ADDAA6FBFF2C4C45A,
	POIXMLDocumentPart__ctor_m9C3CA573396BCEABFEF8080CFE2D1A81FB872683,
	POIXMLDocumentPart__ctor_m83735182590B17918A24D9064401A11339487954,
	POIXMLDocumentPart__ctor_mB3A12EFD2DFE02765BD8694CDFADEC0A2CD92810,
	POIXMLDocumentPart_get_NamespaceManager_m909312D7D29884BC4A4D5D7DA54BD832C23CEFC0,
	POIXMLDocumentPart_CreateDefaultNSM_m64F247CDF69B8C66B19DF8F5ADBCF8B591A15EF4,
	POIXMLDocumentPart_GetPackagePart_m97C7798B2BD5391F1B9D0837575B6C5B801E9265,
	POIXMLDocumentPart_ConvertStreamToXml_m3B00F60A1178A21ED32933B5966A8C48FC15B6C5,
	POIXMLDocumentPart_GetPackageRelationship_m2F5CB2352DF156A8CB48DA47BC77448EFECDAD0F,
	POIXMLDocumentPart_GetRelations_m1A77728867382F10BD3292CB34B2711A50CFC453,
	POIXMLDocumentPart_GetRelationId_mAF9C5358C0D9D58888DDDA18D0B852B93094FC77,
	POIXMLDocumentPart_AddRelation_mD05953ED02696D99DD8EBD960E85C5591A0276CA,
	POIXMLDocumentPart_RemoveRelation_mCC36784CAADC4C56B275CAFE8BA6A59D4775AA41,
	POIXMLDocumentPart_RemoveRelation_mB9FA4025B65E8907FF98AEAF658068D9CBAEE86F,
	POIXMLDocumentPart_GetParent_m3A223A75A629A6994B4B0F98063EF7376FABA06C,
	POIXMLDocumentPart_ToString_mE065333E7AB6B956A2441B2705170CA978EFBC40,
	POIXMLDocumentPart_Commit_m78ED5D07BA7842EB7061371F12E30417017A2D69,
	POIXMLDocumentPart_OnSave_m8E70B19D45E12185AFCEE8D172636A7FD876B8A2,
	POIXMLDocumentPart_CreateRelationship_mD2B8BC4EF6AB9A35A2453F0FC1B626C50C1B55CB,
	POIXMLDocumentPart_CreateRelationship_mF346C61600510FCAC50F508EA6659627B99096D9,
	POIXMLDocumentPart_Read_mE1BCF560E5D3FA245514BF338E674BE5284559EF,
	POIXMLDocumentPart_OnDocumentCreate_m298234B3D9AC29D20A9194A08C513E60982790C1,
	POIXMLDocumentPart_OnDocumentRead_m314D2B5E98D230328246496AB122A3A26A409C69,
	POIXMLDocumentPart_onDocumentRemove_m3899C5FA2F151979A828149C9AB95E3518D7953B,
	POIXMLDocumentPart__cctor_m3883F5D6128700A7A12FDEC904B2A984C503F7FB,
	POIXMLDocument__ctor_m09BFFE3F1F3097B978D510417000643CC026B15A,
	POIXMLDocument_get_Package_mE7ED9A743B48329991CF524C42E136D172925D4C,
	POIXMLDocument_GetProperties_mA16B43DFE84B69C1F197AB81D75CDD899F9C01AE,
	POIXMLDocument_Load_m2035AC49492A017923162D6E9DA025A563D56A81,
	POIXMLDocument_Write_m1835686382F5890721E890450CBB97165E2D6F1E,
	POIXMLDocument__cctor_m3FF3982278EA1507BB56B5BA3A4AC43D24F2EE0E,
	POIXMLException__ctor_m89160B0B53F776DE7A0ED6B05276EEA950477F85,
	NULL,
	NULL,
	POIXMLFactory__ctor_m6C94C6CFF85D6B5BEF5A2D7B11BF830930A7B63D,
	CoreProperties__ctor_mA9B660CBF5FD3BA3BE297EA84BFBA021DD6E5CC9,
	ExtendedProperties__ctor_m73464D44FBE7104CCA07782B9BAC50B0BE9FD79A,
	CustomProperties__ctor_m2E1B8E7D40CB0E614330CF84943EEAD92248EEAE,
	CustomProperties_Add_mC74DD361DEE373BBC5EF756FE6AF4153C155D32E,
	CustomProperties_AddProperty_m98AF304A32EB6B780F5809BB6F3741FB096AA42C,
	CustomProperties_NextPid_m4157397130049166F2B2F32D3D6F567CFC340F6F,
	CustomProperties_Contains_m4DE6162CAF490DEF211B92232D7854ACA78BC4E3,
	CustomProperties__cctor_m7CDE11D00884F7F438E1C7078CEE4B7BC455AB5C,
	POIXMLProperties__cctor_m9ACD75E00D7AC35DDCE263ABFD9B8B1822D329C2,
	POIXMLProperties__ctor_m96D97978173EBC2A07B05DF0B7B2947B4712203B,
	POIXMLProperties_get_CustomProperties_m5EDE13E7EEF6987005B1C8B9C0769BC4C26EB990,
	POIXMLProperties_Commit_m1758AFE3B10AD38FD9A0FD4E8214022BBFCE1B31,
	POIXMLRelation__ctor_mE3808C9EC1D0853DC51C23B0DD4705069F4E2BE1,
	POIXMLRelation_get_ContentType_mEE5BEF7BABA2E142D86E80959568B738A831920C,
	POIXMLRelation_get_Relation_mC6BFA5575CF72AF3A358F0141B76B70388FE5BFE,
	POIXMLRelation_get_DefaultFileName_mCEBB328D558C01FACF1BF1FB5B52DE12A565C296,
	POIXMLRelation_GetFileName_m584CD4F9E43DAA0969CF5ACAE7324FB7FCF101DA,
	POIXMLRelation_get_RelationClass_m9FD705D95CC41266F75561A544E898128126BC35,
	IndexedColors__ctor_mFB7EEFA8D97BDC9875A4BB16057E32FB3EF90249,
	IndexedColors__cctor_mFC9E4E57EF192561A21E365E2594B8D1AF8A2011,
	IndexedColors_get_Index_m31FFD69243F0D24C1C5DF5468CA8305760E70327,
	WorkbookFactory_Create_m19E3BB3A72D6F4702AB405C2C1050AFFCEA5321F,
	PackageHelper_Open_m77A97328F6439A295AE16A74F0BDFB7647FF5644,
	CalculationChain_GetCTCalcChain_m00935F4486051A3F7A2B6EFD72036259B54210DC,
	CalculationChain_RemoveItem_m540B359572EAB3CE34CC6513741CDDA1A07884A6,
	IndexedUDFFinder__ctor_m9E8C23D39BBF77CC8B525AFB97A4B754CA2FC63C,
	IndexedUDFFinder_FindFunction_mF3F2F4A3DA88DFA2A8818A7ADB73B8FF4FDE8E81,
	IndexedUDFFinder_GetFunctionName_m57FA22FC4064247D96F0AAEA302528EFC913517F,
	IndexedUDFFinder_GetFunctionIndex_m4C31FEB19B22AE08FF6AD89162C82CA98FA4207A,
	SharedStringsTable_GetKey_mD2C981BD3D82B65C9580BF314DD9E89DB6E7E0E8,
	SharedStringsTable_GetEntryAt_mBC5F2231C56E57531BDEBD7355AD69F626A6B8DE,
	SharedStringsTable_AddEntry_mF62E41F2AFC07FA4991B727C0CE7B23132242ECC,
	StylesTable_SetTheme_m88B90E66A0F3A191EB5F8B4CA734848B25D823C5,
	StylesTable_GetNumberFormatAt_mC6EBD133780177D0221DB7F0943FAE005FB4878C,
	StylesTable_GetFontAt_m654FDEA4A7807E082D4869EBC41A81A74AF98EB0,
	StylesTable_GetStyleAt_m969FF4984E29EF31978E6F3DA496CF17373ABBFE,
	StylesTable_GetCellXfAt_m16617A6A54EA82A6200F4E91D12F9E29A106E68B,
	StylesTable_GetCellStyleXfAt_mD5F4333BABB10EC6DAD02B09ADD5E25A30B86DC4,
	StylesTable_get_NumCellStyles_m0661911112CEF768E1D0AF40D1AE0A70F59FF37D,
	StylesTable__cctor_m4FE08E83CA7B1C3F3204C0383D009EF9190FF8EB,
	XSSFCellBorder_SetThemesTable_m5DDA99E96EADAE63BA69776EECB84724D13D54C6,
	ColumnHelper__ctor_m575531CECD15F090DC01544A7F74778762F0A4DD,
	ColumnHelper_CleanColumns_m4C47782E74C464DB35A626BC2E134A41A8F2CA68,
	ColumnHelper_SortColumns_m5F7CEA4E1798BC72E36B16C66D7C68DAF358A315,
	ColumnHelper_CloneCol_m2F8AF5961E38327DD66BBCE492DC4A17AA89369D,
	ColumnHelper_AddCleanColIntoCols_m10105405545C3C37A8429600F5943DB92EE5F18C,
	ColumnHelper_insertCol_m3DD5D9F0233D458F9389A8CDE7DDA1E7BA1F729D,
	ColumnHelper_SetColumnAttributes_m4E0485EE0B04EF88FCBE84DF8028C644886AE03E,
	ColumnHelper_columnExists_m830601B52E3279BB3CC6C8C7A5A329670F2A30B1,
	XSSFCell__ctor_mB9754C8B384EFD5C81AC019960AA276534D34F8B,
	XSSFCell_get_Sheet_m8E114F3EC4E37191F45175A8DEEB5714CD748BA6,
	XSSFCell_get_Row_m21F4DF4C54D7C2D797BE20BE6992B878714C6031,
	XSSFCell_get_BooleanCellValue_m5346C90797E33DD645F706A6DB72B09864037B13,
	XSSFCell_SetCellValue_m006EC44EB4878721B53B9EDAABF35F5DFFEC648B,
	XSSFCell_get_NumericCellValue_m09DF185692A9CE7C676DFB8B288F427FA2B6BB5D,
	XSSFCell_SetCellValue_mEB01822A9EF815056AB70FAC182A9AA112BED957,
	XSSFCell_get_StringCellValue_mE0721EDE9204E51CB370DD5B3E521B368B6FFFE2,
	XSSFCell_get_RichStringCellValue_m11C368FCAA89A6175B421ED5B26E2956B6094CFE,
	XSSFCell_CheckFormulaCachedValueType_m43383F8187190A448AE1772B29178E56BC8F6834,
	XSSFCell_SetCellValue_mDEA033C99443435200607F08CAC6571B60DDB12D,
	XSSFCell_SetCellValue_m67E7C5073DD7407A5D96EECD5C23B91848CCADA1,
	XSSFCell_get_CellFormula_mC236DD3E204EE8C0C51E25CE39D28F9A2B4CA973,
	XSSFCell_ConvertSharedFormula_mB8C2CC971FB4BD9E5B3388F85C25AF233A661F35,
	XSSFCell_get_ColumnIndex_m0D0B2115AA481C708E832335A540D9C18607833E,
	XSSFCell_get_RowIndex_m13B7955899DEEFB3C1F78C16D1A168D4287FDFAA,
	XSSFCell_GetReference_mF9420E93DE67493289B3EE64B1CD7A1C31346997,
	XSSFCell_get_CellStyle_m4CE434C7212D3CDE3207B7DC4DCD0FD5F3AC4871,
	XSSFCell_get_CellType_m5F4ADC04AB62935E8FAC0D91317AA10F4DD1EFFE,
	XSSFCell_GetBaseCellType_m7C3A07DA194C0B04074B88FAEF35129C88A9DC0A,
	XSSFCell_get_DateCellValue_mFE5E4B0A4361E4A322775B094B94BDB08AAE745E,
	XSSFCell_get_ErrorCellString_mBBBD06674605193C65F487ABD77BDB121688D345,
	XSSFCell_get_ErrorCellValue_mB26E951145A6BE17C9B6FFC04CADD7152D00AABD,
	XSSFCell_SetBlank_m713FCD4B3F58768EC7C6704E2F147FE448C1B598,
	XSSFCell_SetCellNum_m9D1850C3BBEDD76CC01089102ABA184E3436E5EF,
	XSSFCell_SetCellType_m77C4E947A59082428B06C6EC4AD7C0FF0FA030C2,
	XSSFCell_ToString_mBA01D2E2F09AFF260888FA5138D451618E0CE8CF,
	XSSFCell_GetCellTypeName_m121DA1E47E46C2822B9135DB474A7CCEE0BEE372,
	XSSFCell_TypeMismatch_mF14E005E515AD3CB5B3A00AA162C2F163FCFC1CF,
	XSSFCell_CheckBounds_m941D37677696B4A6CDD1644B472B9275DA4FD7A8,
	XSSFCell_GetCTCell_m9D36BB039DF7277793A4ACDFD87EC6E0A57C5F5F,
	XSSFCell_ConvertCellValueToBoolean_m7AB3788DDEF0AF16E7299A77C4E36CAA2FD5BAA1,
	XSSFCell_ConvertCellValueToString_mBB3D2EDC97AF12B7EDC87DED36B60901C2B0F870,
	XSSFCell_get_ArrayFormulaRange_m60695A97B2E09291A374B1015A022EAE7BEE5328,
	XSSFCell_get_IsPartOfArrayFormulaGroup_m5880F5DE755AF995051D7BD381EFE453F83D65A2,
	XSSFCell_NotifyArrayFormulaChanging_m3175A219B21BCA55B26C7F68782F2F2B4DDC15EC,
	XSSFCell_NotifyArrayFormulaChanging_m47B665B80BEEF4F6C8ED08B7E466AE4B29BA7D98,
	XSSFCell__cctor_mBEB7376C222B563DC9C59A8BC315460E2B8EDCB9,
	XSSFCellStyle__ctor_m6A79380655A3B78FED4059563A41082B47215652,
	XSSFCellStyle_GetCoreXf_mCA0ADE3352B55207616A851852FE15FC8F6A0BAB,
	XSSFCellStyle_get_DataFormat_m16E7ECD10346C93384CB94663B46368D6E275EB9,
	XSSFCellStyle_GetDataFormatString_m7A9F45371EAB66BE9E94D584BCCDC8874A3EFB06,
	XSSFCellStyle_GetHashCode_mA80711740DF84C42387DD7B93D4AE4A86241A122,
	XSSFCellStyle_Equals_m74957AD38B97B98BE0218B1B968AEA39373298B5,
	XSSFSheet__ctor_m19FC1FF55C9165530F6143F295D571AAEBA97113,
	XSSFSheet_get_Workbook_m973E4FECBB9B808687CB8BB4B2949B688CA19E9E,
	XSSFSheet_OnDocumentRead_m1CA72ED5D6A16B3BC02D4E0AB86101E8966ED1B1,
	XSSFSheet_Read_mC3A5B13B07103DDED1BB937AFEEAD630DA049037,
	XSSFSheet_OnDocumentCreate_mBF8D38014D4037A046C83AA6D8775DBE8129F83E,
	XSSFSheet_InitRows_m2455387DD1474516CB0204CB780B5D59E9FFEA36,
	XSSFSheet_InitHyperlinks_mA2D5565D9C6E2A0C7C3855907D5D8FBFA26743BF,
	XSSFSheet_NewSheet_m377EFC50964E3A1334A409345A55FA0FB81ED907,
	XSSFSheet_get_SheetName_m83A6422E3383A3082F3358C60EA199CD54F93424,
	XSSFSheet_GetLastKey_m8BD2BFE9BC108B295250A9ED6EAF71254A7C2334,
	XSSFSheet_HeadMap_m40D40CFB2C42350FB4C390FD5E8DD2628247A4CA,
	XSSFSheet_CreateRow_m4C926C49F6F700838E0E96A86C5C0102FEE38174,
	XSSFSheet_GetRow_m5C46CCFF008AC52DFC2048321DAAC191CFCB3409,
	XSSFSheet_GetSharedFormula_m75B6EDEC0217054DB7CB627795BBF8F664A27245,
	XSSFSheet_OnReadCell_m1788436E3BBE5FB81202A93383459C6D2BFA9AF1,
	XSSFSheet_Commit_m3208FDB01F94AEC1292D20A7DD4EFBAFA7C5F21B,
	XSSFSheet_Write_m514EE36D018C19E9CBAEA403F28B323C708EEB28,
	XSSFSheet_IsCellInArrayFormulaContext_m985F457CB7E73642B7B32A0094D7D0EC721A22C5,
	XSSFSheet_GetFirstCellInArrayFormula_mC7FB2001F40BF6967D8A6B2BDA79649535262F44,
	XSSFSheet_GetCellRange_mDCD69EDF0F1A0CDAD71278CFA94F903921ED4DDB,
	XSSFSheet_RemoveArrayFormula_m1A522BAE8BEF1E21D36E2E7ED61B567FD4E2C0E1,
	XSSFSheet__cctor_m22416C4D4A777684C86C5E6CB29C2D3F1041867E,
	XSSFChartSheet_blankWorksheet_m1D9881BDCFFF46E488E26285E312C3BCAA57ED0F,
	XSSFChartSheet__cctor_mB3E13C2833741A69C245F352E2EDFDB4B4385C3C,
	XSSFDataFormat__ctor_m154866A4F54E64056E911B55EA25A0BBD2338F06,
	XSSFDataFormat_GetFormat_m7780941725BDC00498C76BF297DD1C7320512198,
	XSSFDataValidationHelper__ctor_mD89CE0249D3D0CD1323DC59726B3A535C6EF7EDA,
	XSSFEvaluationWorkbook_Create_m9DFA7C9209A073186D275854537F607BEDC40328,
	XSSFEvaluationWorkbook__ctor_mA423583159D63F594DDE62244D143CDE5EA02695,
	XSSFEvaluationWorkbook_ConvertFromExternalSheetIndex_mF125C442F3BFC1ED51368C16C5DF08CF2EEFFF1B,
	XSSFEvaluationWorkbook_ConvertToExternalSheetIndex_m42DE8521C35487B8F55DC0AD73A2F2457B7152A0,
	XSSFEvaluationWorkbook_GetExternalSheetIndex_mD884497C1DD7AB2A17BB035A67E3DBF446D2F3D3,
	XSSFEvaluationWorkbook_GetName_m0B56ED380212B37FBCCBE0171D4F4638B041D980,
	XSSFEvaluationWorkbook_GetNameXPtg_m93360D65BA5414140908DEDCAABD19031E71DD38,
	XSSFEvaluationWorkbook_ResolveNameXText_mAFC3A5F204BBF1B38E98BD8EB6D56539B38AA363,
	XSSFEvaluationWorkbook_GetExternalSheet_mAAD6D62B250AFC63D4CE94490DFA6ED15DF8416D,
	XSSFEvaluationWorkbook_GetExternalSheetIndex_m1A68B752712110EB32202FC2B728B9CB31DC4EAA,
	XSSFEvaluationWorkbook_GetSheetNameByExternSheet_m272011D2821193BAD078B23E5B1B70B4CDA2F331,
	XSSFEvaluationWorkbook_GetNameText_mAE50D876161CB3C8693749EB527622A68FE55019,
	XSSFEvaluationWorkbook_GetUDFFinder_m1FE2D890342FB9B4179C0FDB9481118640BD1329,
	XSSFEvaluationWorkbook_GetSpreadsheetVersion_m3CB4BF3536631E95549D7C6188B39C13ADFABA42,
	Name__ctor_mCBF61CC348DFA7776BE399D410F05D39760B3918,
	Name_get_HasFormula_mB534597E147580BBE6FA84F436CC0B2D820ACFD5,
	Name_get_IsFunctionName_mA7C9074C06644336E7E4B87A7D84A6834FCCD5B5,
	Name_get_IsRange_m32F8839CBD3026097532BF51EA03F542034F7C17,
	Name_CreatePtg_m2E598375DBAEDC923A3F424C97FCCDB2035F3D1A,
	XSSFFactory__ctor_m9DCA7515B99A4263366917D4C27626CDC7169EB5,
	XSSFFactory_GetInstance_m7EDF92040038A4F235F988A75EAD85A0B989199D,
	XSSFFactory_CreateDocumentPart_m34966646865CA8A02851632D7C6D82A0F8EFE550,
	XSSFFactory_CreateDocumentPart_mDD88E1AD3308FE1BDE82D9B5EF5FE925AEB4C31C,
	XSSFFactory__cctor_mDDF69C11276F8983DBBCBFB85C7613F2ADDD3D4F,
	XSSFFont_GetCTFont_mF90EBCA95FB4F35BC0646E48097BFDC7053310C3,
	XSSFFont_SetThemesTable_mA596E2035750D3A5E1E60AAB960B8C2648A7CAFC,
	XSSFFont__cctor_mDFA948F083421924F4202C14BE157D9AFD3C82EE,
	XSSFName__ctor_m031B6704625BE5F4E9B15697A626DADBB110121D,
	XSSFName_GetCTName_m1D5F0FC22AC573340F83FFEA789FED28DF1DEC82,
	XSSFName_get_NameName_m785A63BE9FD8E8A0093D9926AFBB8CFAB845DFDF,
	XSSFName_get_SheetIndex_m1FC17F4E3ECDA9091CB86D07870BC8DF62B87E64,
	XSSFName_get_Function_m8F790234A59850F00091FB22E3EBF5D11382B6C2,
	XSSFName_get_IsFunctionName_mD4D4A54D3B47209167E5D1C8200B9490A41D7C5B,
	XSSFName_GetHashCode_m818AF7CBD4D0FB2403A0B70A3C447AB62C13CE17,
	XSSFName_Equals_mC5442C2FBD14F94367859B11DCF22635FDC140AA,
	XSSFName__cctor_m6821233721562F5AD1F579C9687101F5AA241DB1,
	XSSFHyperlink__ctor_mFA49FC8AE5AC520BCC7B08736CE66AD24FA1DC1E,
	XSSFHyperlink_GetCTHyperlink_mDBE6CB4F438142681A26359F913A58BDBF9B22A3,
	XSSFHyperlink_NeedsRelationToo_mCDDE617A36C15AE600AB6E06A11867AEA87F973B,
	XSSFHyperlink_GenerateRelationIfNeeded_m7DC845976AEA11A87C2719312A22017B6F759E3F,
	XSSFPictureData__cctor_m886E340F971099DD211ED0F162553251920F5415,
	XSSFRelation__ctor_mE6C40378A119DD7DC5244B065C71B4F0CC3F8C65,
	XSSFRelation_GetInstance_m0485CCEB044F36A7870F0E0C62332D9CA455A8FF,
	XSSFRelation__cctor_m6592F9F94D4A324642C614F90F740D315D1EC56A,
	XSSFRichTextString__ctor_mFD017350E8A2078F9C72DEF31102F162BF7261C3,
	XSSFRichTextString_SetStylesTableReference_m223D7616ADAD462F01665E42D91C12E7C176D7CF,
	XSSFRichTextString__ctor_m441C2DF5478676568FF898F5A3542D5CFEAD1563,
	XSSFRichTextString_SetRunAttributes_mA1051E1EA018355A22CA8301ACDC706B6C8EC618,
	XSSFRichTextString_GetIndexOfFormattingRun_m54B529CFE3C8C339ADB848AE2B957421A8873CD8,
	XSSFRichTextString_get_String_mD3F8F178928319BE1F06DCDB57AC0CAFDC5EA8F8,
	XSSFRichTextString_ToString_mCE6E4D7271F974E4F0B9A34D81622FF56CDDFEF0,
	XSSFRichTextString_get_Length_m61AA298D58D30C083029918473616DF5A7D98421,
	XSSFRichTextString_get_NumFormattingRuns_mBA361C07C5482739193594F59C33F69B47CD750B,
	XSSFRichTextString_GetCTRst_mA1C73F382F97769E777396DCEAEDD943349F169A,
	XSSFRichTextString_PreserveSpaces_m0F314D4318BFDE8B732B310EC197505D120A100C,
	XSSFRichTextString_UtfDecode_mD8BD3D36199EB766BBA78A065DF9A4113975A522,
	XSSFRichTextString__cctor_m2F289178DFF374F99FEB20148BC520C901A92F8D,
	XSSFRow__ctor_mBCBF6DEBFD52FF5426CC5ABF8CF9C0C0535EEF99,
	XSSFRow_get_Sheet_mA0F66B76BC8EFD591BCE29757BD26C0B21EAF8B8,
	XSSFRow_CompareTo_m5A33D2DED9F7ECC970D5D6A4B96D274B9BD2313F,
	XSSFRow_CreateCell_mEDC195BF78D9DCCCE28A38DFC770CA4F1D164440,
	XSSFRow_CreateCell_m3BDA3E6F84C1F7C0CE6B07ACA0ED01491D97C34B,
	XSSFRow_GetCell_m811FB693E916444348B7C4341CC7B6C37E7A9616,
	XSSFRow_RetrieveCell_mD34782670FCA90773E901251BADBA28B26F9B0F4,
	XSSFRow_GetCell_mEEECD3A2DB6048D82056954B15504E50D224FCA2,
	XSSFRow_GetLastKey_m11464F715651EBE8CA7093792B8108C54AD83708,
	XSSFRow_get_LastCellNum_m595E9C09198914CC7815BDE5EF5FEF2B06E028D6,
	XSSFRow_get_RowNum_mC89E0D6EC45DE46C23ACB24233BE718A2A8FF458,
	XSSFRow_set_RowNum_mCF2D412D0B00EEDE38C48254EC1A4A10BB7C7B1A,
	XSSFRow_GetCTRow_m8FB4D55D909CE4BF80C57FB52BB1C5D0BB462275,
	XSSFRow_OnDocumentWrite_m97025FCC531FF687A733FA8F15F0789AD9D40ACA,
	XSSFRow_ToString_m085611080127A2F4B9B71666563DC5855A76CAF9,
	XSSFRow__cctor_m7484C58B18F02836A2433F8F9EBD88A05C4B343C,
	XSSFVMLDrawing__cctor_m3A8540D81FC9E1CECCAEBF94CFF8ABC30434BEB2,
	XSSFWorkbook__ctor_m7942004F5D4B76DBD538CDD3487B019A831914C8,
	XSSFWorkbook_OnDocumentRead_m580FABCD07BF17477B6A90C2382FEC19D20A526D,
	XSSFWorkbook_GetNameAt_m2096A82CD6F564787D89088EA7955E58594CD62A,
	XSSFWorkbook_get_NumberOfNames_m66702D991F59B2395ED482FEFB521D82D6A4D1D5,
	XSSFWorkbook_GetSheet_m1ED9132DB9AB4F086A5225279AA19681D9E8985D,
	XSSFWorkbook_GetSheetIndex_m8B99E63F4DDF960B1C2107AEAC980DCB7276BE56,
	XSSFWorkbook_GetSheetIndex_mE0AABAFF93A1C2C6809BCD8A6929AA579C021B01,
	XSSFWorkbook_GetSheetName_mECC4097663FD7FA2B543690525A6391CF290818A,
	XSSFWorkbook_get_MissingCellPolicy_mC1E5917BAEB4ABBDD740358798478F3B15E014B6,
	XSSFWorkbook_ValidateSheetIndex_m643605341F0C35BB093110609B4838914C80FFCF,
	XSSFWorkbook_SaveNamedRanges_m5390D2940293EAB5452E52E7F2E335FFB8E1E8B3,
	XSSFWorkbook_SaveCalculationChain_m82D774BBEDC805E35C9C6118F2F32AC659CC5708,
	XSSFWorkbook_Commit_mDD81888B738F28E8680CF71717D12612B1EAD5C0,
	XSSFWorkbook_GetSharedStringSource_m1CB0C23C6EC91AD02F683861177D477F243BF327,
	XSSFWorkbook_GetStylesSource_mA8F66FC35700E468DBB169E28F7AB0B7540B274D,
	XSSFWorkbook_IsDate1904_mE6AFEA598D6B29526CC81BFA23DAD9564E05A9C2,
	XSSFWorkbook_OnDeleteFormula_m25B8D5BE5ECA50077C1D5E437D6846E75B1BE62B,
	XSSFWorkbook_GetUDFFinder_m4B908DB2CEAE450872885355213070CC54D30D07,
	XSSFWorkbook__cctor_mF81EEB6D684E6AF60FAC6753A2249A4BF16F1FD1,
	CTColComparator_Compare_mBBAE0BF4BBF6AD5305EFADCC9F9EB32551B52431,
	CTColComparator__ctor_mF51A75272E0543AC64E882D17D3D9DF4F2A601BC,
	NumericRanges_GetOverlappingRange_mF60B3C35FF742B2C9E3BFB0CBF63B8F069D87A6E,
	NumericRanges_GetOverlappingType_mE80223E3A313C47C51DBDB612399BC097D0DB561,
};
static const int32_t s_InvokerIndices[254] = 
{
	10,
	10,
	10,
	26,
	23,
	27,
	4,
	4,
	14,
	0,
	14,
	14,
	28,
	27,
	26,
	481,
	14,
	14,
	23,
	26,
	105,
	1108,
	27,
	23,
	23,
	23,
	3,
	26,
	14,
	14,
	26,
	26,
	3,
	26,
	215,
	28,
	23,
	26,
	26,
	26,
	28,
	27,
	10,
	9,
	3,
	3,
	26,
	14,
	23,
	446,
	14,
	14,
	14,
	34,
	14,
	62,
	3,
	249,
	0,
	0,
	14,
	62,
	26,
	28,
	34,
	112,
	28,
	34,
	112,
	26,
	34,
	34,
	34,
	34,
	34,
	10,
	3,
	26,
	26,
	23,
	163,
	105,
	105,
	2407,
	27,
	2408,
	27,
	14,
	14,
	89,
	31,
	463,
	341,
	14,
	14,
	174,
	26,
	26,
	14,
	34,
	10,
	10,
	14,
	14,
	10,
	226,
	110,
	14,
	89,
	23,
	32,
	32,
	14,
	43,
	2409,
	173,
	14,
	89,
	14,
	14,
	89,
	26,
	23,
	3,
	973,
	14,
	249,
	14,
	10,
	9,
	23,
	14,
	23,
	26,
	23,
	26,
	23,
	4,
	14,
	112,
	58,
	34,
	34,
	34,
	26,
	23,
	26,
	9,
	28,
	28,
	28,
	3,
	4,
	3,
	26,
	672,
	26,
	0,
	26,
	37,
	37,
	112,
	58,
	28,
	28,
	34,
	41,
	34,
	28,
	14,
	14,
	107,
	89,
	89,
	89,
	14,
	23,
	4,
	215,
	28,
	3,
	14,
	26,
	3,
	27,
	14,
	14,
	10,
	89,
	89,
	10,
	9,
	3,
	27,
	14,
	89,
	26,
	3,
	446,
	0,
	3,
	26,
	26,
	26,
	27,
	37,
	14,
	14,
	10,
	10,
	14,
	163,
	0,
	3,
	27,
	14,
	112,
	34,
	202,
	34,
	34,
	136,
	112,
	249,
	10,
	32,
	14,
	23,
	14,
	3,
	3,
	26,
	23,
	34,
	10,
	28,
	112,
	112,
	34,
	14,
	32,
	23,
	23,
	23,
	14,
	14,
	89,
	26,
	14,
	3,
	41,
	23,
	1,
	138,
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationNPOI_OOXML;
extern const Il2CppCodeGenModule g_NPOI_OOXMLCodeGenModule;
const Il2CppCodeGenModule g_NPOI_OOXMLCodeGenModule = 
{
	"NPOI.OOXML.dll",
	254,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	&g_DebuggerMetadataRegistrationNPOI_OOXML,
};
