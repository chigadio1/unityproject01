﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Int16 NPOI.DDF.EscherRecord::get_Options()
extern void EscherRecord_get_Options_mA163509CCD7AA5DC2D6C5496E37B0CCE2CC9E5FB (void);
// 0x00000002 System.Int32 NPOI.DDF.EscherRecord::Serialize(System.Int32,System.Byte[])
extern void EscherRecord_Serialize_mC2D1E02EC8F7604A2073A8CEBB74C98BCD18846A (void);
// 0x00000003 System.Int32 NPOI.DDF.EscherRecord::Serialize(System.Int32,System.Byte[],NPOI.DDF.EscherSerializationListener)
// 0x00000004 System.Int32 NPOI.DDF.EscherRecord::get_RecordSize()
// 0x00000005 System.Int16 NPOI.DDF.EscherRecord::get_RecordId()
extern void EscherRecord_get_RecordId_m0353F2F24EB65F9A4AE940EDEF894206B83F3490 (void);
// 0x00000006 System.Void NPOI.DDF.EscherRecord::.cctor()
extern void EscherRecord__cctor_m97B2A5587845BA3CCBA86530C3D33676701FFB2C (void);
// 0x00000007 System.Void NPOI.HPSF.Array::.ctor()
extern void Array__ctor_m5FA1D02D671FD91525C9A36AAD7C8A1E8D27A31C (void);
// 0x00000008 System.Int32 NPOI.HPSF.Array::Read(System.Byte[],System.Int32)
extern void Array_Read_m81D55450BF887649C9B68C32FB87BCF0EF0129DB (void);
// 0x00000009 System.Void NPOI.HPSF.Array_ArrayDimension::.ctor(System.Byte[],System.Int32)
extern void ArrayDimension__ctor_mF4BD36AF2E10C74BAEB54ACD2137180C8B304E02 (void);
// 0x0000000A System.Void NPOI.HPSF.Array_ArrayHeader::.ctor(System.Byte[],System.Int32)
extern void ArrayHeader__ctor_m75E6FCDAE18AAEEE958C561BF1F8DB7E1C9C8E85 (void);
// 0x0000000B System.Int64 NPOI.HPSF.Array_ArrayHeader::get_NumberOfScalarValues()
extern void ArrayHeader_get_NumberOfScalarValues_m14AC92BB5A0B881C6E8CB5A55F2B872BB9B011EB (void);
// 0x0000000C System.Int32 NPOI.HPSF.Array_ArrayHeader::get_Size()
extern void ArrayHeader_get_Size_m24D274A26BD9063B548185F017193046C147FF3F (void);
// 0x0000000D System.Void NPOI.HPSF.Blob::.ctor(System.Byte[],System.Int32)
extern void Blob__ctor_m4EFCFAD64637E7E7A69663D0473A7BE9BADB907A (void);
// 0x0000000E System.Int32 NPOI.HPSF.Blob::get_Size()
extern void Blob_get_Size_m25830AC9F3337DFAF649AE7B4563440B01D8C4E7 (void);
// 0x0000000F System.Void NPOI.HPSF.ClipboardData::.ctor(System.Byte[],System.Int32)
extern void ClipboardData__ctor_m82D55811EEB7AC8DCFCCCCB72360E2898AFE2CAC (void);
// 0x00000010 System.Int32 NPOI.HPSF.ClipboardData::get_Size()
extern void ClipboardData_get_Size_mECA721C91661192B7CBB5ADC065AB40B08D6664F (void);
// 0x00000011 System.Byte[] NPOI.HPSF.ClipboardData::ToByteArray()
extern void ClipboardData_ToByteArray_mCC093C541D2571EB503791CEBBC7E20AED4EBB26 (void);
// 0x00000012 System.Void NPOI.HPSF.CodePageString::.ctor(System.Byte[],System.Int32)
extern void CodePageString__ctor_mCA99EA2601564998CBA0B8C3B7A00E300E3AA5C9 (void);
// 0x00000013 System.Void NPOI.HPSF.CodePageString::.ctor(System.String,System.Int32)
extern void CodePageString__ctor_mC0B6749A7743538A5C5166F4ED8A45FEC8712337 (void);
// 0x00000014 System.String NPOI.HPSF.CodePageString::GetJavaValue(System.Int32)
extern void CodePageString_GetJavaValue_mE7DFBA44A6ACBDC71C45EB76E0520D3A6DF063CF (void);
// 0x00000015 System.Int32 NPOI.HPSF.CodePageString::get_Size()
extern void CodePageString_get_Size_mB7F9E3C0BBCD70BEFCDD8979A275082CBA8B9AAF (void);
// 0x00000016 System.Void NPOI.HPSF.CodePageString::SetJavaValue(System.String,System.Int32)
extern void CodePageString_SetJavaValue_mE87B422643AB871E4C89F296AD0AE5B16783BAA6 (void);
// 0x00000017 System.Int32 NPOI.HPSF.CodePageString::Write(System.IO.Stream)
extern void CodePageString_Write_mEFB88FAD311F217B145BE41432C7AC7E63128B55 (void);
// 0x00000018 System.Void NPOI.HPSF.Currency::.ctor(System.Byte[],System.Int32)
extern void Currency__ctor_m4613F45CDCDCB399AECDAE4E1BBC6C97775A04A9 (void);
// 0x00000019 System.Void NPOI.HPSF.Date::.ctor(System.Byte[],System.Int32)
extern void Date__ctor_m06E3F4CB509C8BCC43681FD834FB57FAF8DC2A79 (void);
// 0x0000001A System.Void NPOI.HPSF.Decimal::.ctor(System.Byte[],System.Int32)
extern void Decimal__ctor_mD02610FFD7567B7B9AC6AB9FDE8F04186D8ADFC0 (void);
// 0x0000001B System.Void NPOI.HPSF.Filetime::.ctor(System.Byte[],System.Int32)
extern void Filetime__ctor_m70C39F1166513E9C1BC3B1EF981B0D4E30F9AA0F (void);
// 0x0000001C System.Void NPOI.HPSF.Filetime::.ctor(System.Int32,System.Int32)
extern void Filetime__ctor_m3BCFBAE817EC9994A1632ECB5E37AB714D715CF1 (void);
// 0x0000001D System.Int64 NPOI.HPSF.Filetime::get_High()
extern void Filetime_get_High_mCF3EF5D6516367333C240C48905A9D8B0B2BBD42 (void);
// 0x0000001E System.Int64 NPOI.HPSF.Filetime::get_Low()
extern void Filetime_get_Low_m2CFF481A6328BCFCD46F8843B81961DFD78DC8FA (void);
// 0x0000001F System.Int32 NPOI.HPSF.Filetime::Write(System.IO.Stream)
extern void Filetime_Write_m22D4787F8FE7950E195F4FFA8587045FD475D37C (void);
// 0x00000020 System.Void NPOI.HPSF.GUID::.ctor(System.Byte[],System.Int32)
extern void GUID__ctor_mAC428C4B9D63A0914BDD234642A8BEF860C89787 (void);
// 0x00000021 System.Void NPOI.HPSF.IndirectPropertyName::.ctor(System.Byte[],System.Int32)
extern void IndirectPropertyName__ctor_mD0E0D86775B4EFD7F1A75A4F22618798695357AA (void);
// 0x00000022 System.Int32 NPOI.HPSF.IndirectPropertyName::get_Size()
extern void IndirectPropertyName_get_Size_mE9E13DD28164AC782EE2BFC894518E720675DC8F (void);
// 0x00000023 System.Void NPOI.HPSF.TypedPropertyValue::.ctor()
extern void TypedPropertyValue__ctor_m145BDF937E96390DBEF95D83EF179353040D4239 (void);
// 0x00000024 System.Void NPOI.HPSF.TypedPropertyValue::.ctor(System.Int32,System.Object)
extern void TypedPropertyValue__ctor_m91F23C6DAB4A5559F09C64FDFA7AFE0565711D8A (void);
// 0x00000025 System.Object NPOI.HPSF.TypedPropertyValue::get_Value()
extern void TypedPropertyValue_get_Value_mD463A15EFDF9A455F0EBC09B371735206F709236 (void);
// 0x00000026 System.Int32 NPOI.HPSF.TypedPropertyValue::Read(System.Byte[],System.Int32)
extern void TypedPropertyValue_Read_mC6E77FC21B9851915E30B8C67CBE7C63A29E05AB (void);
// 0x00000027 System.Int32 NPOI.HPSF.TypedPropertyValue::ReadValue(System.Byte[],System.Int32)
extern void TypedPropertyValue_ReadValue_m3971792ECAD3D24F5DD8E0BE562C0BD720B67096 (void);
// 0x00000028 System.Int32 NPOI.HPSF.TypedPropertyValue::ReadValuePadded(System.Byte[],System.Int32)
extern void TypedPropertyValue_ReadValuePadded_m37C28AA11C3CB5E00734920AD7442B47287985AF (void);
// 0x00000029 System.Void NPOI.HPSF.TypedPropertyValue::.cctor()
extern void TypedPropertyValue__cctor_m0102420527996408C044CEADC0C795F911E66390 (void);
// 0x0000002A System.Void NPOI.HPSF.UnicodeString::.ctor(System.Byte[],System.Int32)
extern void UnicodeString__ctor_m66AA1A57E37BE146DE8510901FF0FF578FA631FC (void);
// 0x0000002B System.Int32 NPOI.HPSF.UnicodeString::get_Size()
extern void UnicodeString_get_Size_mA1153203137716CD8185FEEABC24FC311861A9E5 (void);
// 0x0000002C System.String NPOI.HPSF.UnicodeString::ToJavaString()
extern void UnicodeString_ToJavaString_m3C33D1E94EDC484866BE119D78F89EBF339E187C (void);
// 0x0000002D System.Void NPOI.HPSF.VariantBool::.ctor(System.Byte[],System.Int32)
extern void VariantBool__ctor_mBB5E67AB750BE66BABDDE5D1F94AEEA3B24AFE4D (void);
// 0x0000002E System.Boolean NPOI.HPSF.VariantBool::get_Value()
extern void VariantBool_get_Value_mD08FA60AE852D224CE80A5541CE199AFA7E760E0 (void);
// 0x0000002F System.Void NPOI.HPSF.Vector::.ctor(System.Int16)
extern void Vector__ctor_m9617BB05BDD95061A81BACEADF12202B63BDD597 (void);
// 0x00000030 System.Int32 NPOI.HPSF.Vector::Read(System.Byte[],System.Int32)
extern void Vector_Read_m990096590912504EA2848EF2158F5BFB44702A05 (void);
// 0x00000031 System.Void NPOI.HPSF.VersionedStream::.ctor(System.Byte[],System.Int32)
extern void VersionedStream__ctor_mB6C320A7C73C5C02FCF636B2C3FC6BC1669DC1C6 (void);
// 0x00000032 System.Int32 NPOI.HPSF.VersionedStream::get_Size()
extern void VersionedStream_get_Size_mCA380A2075DEAB4F2F85AE84ABFC932956BD51F7 (void);
// 0x00000033 System.Int32 NPOI.HSSF.Record.RecordBase::Serialize(System.Int32,System.Byte[])
// 0x00000034 System.Int32 NPOI.HSSF.Record.RecordBase::get_RecordSize()
// 0x00000035 System.Void NPOI.HSSF.Record.RecordBase::.ctor()
extern void RecordBase__ctor_m3AA7A080BE5A232BE2676747C159D127E6F0A381 (void);
// 0x00000036 System.Void NPOI.HSSF.Record.Aggregates.RecordAggregate::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
// 0x00000037 System.Int32 NPOI.HSSF.Record.Aggregates.RecordAggregate::Serialize(System.Int32,System.Byte[])
extern void RecordAggregate_Serialize_m5675ADB220F3D17B783E8A86419ED1686593CEA3 (void);
// 0x00000038 System.Int32 NPOI.HSSF.Record.Aggregates.RecordAggregate::get_RecordSize()
extern void RecordAggregate_get_RecordSize_m2DFC7C86CD8BAE9D6A24B25CC120B2294BC89DCC (void);
// 0x00000039 System.Void NPOI.HSSF.Record.Aggregates.RecordAggregate::.ctor()
extern void RecordAggregate__ctor_m0EF1BC067BDF9054426A8CF9D6ABCD4B00F352DD (void);
// 0x0000003A System.Void NPOI.HSSF.Record.Aggregates.RecordAggregate_SerializingRecordVisitor::.ctor(System.Byte[],System.Int32)
extern void SerializingRecordVisitor__ctor_m3235895ED1DA5959053258ACBAB542797105AC51 (void);
// 0x0000003B System.Int32 NPOI.HSSF.Record.Aggregates.RecordAggregate_SerializingRecordVisitor::CountBytesWritten()
extern void SerializingRecordVisitor_CountBytesWritten_mC4C0C4B06361AC6CDA2E6AC8227665C7076A3461 (void);
// 0x0000003C System.Void NPOI.HSSF.Record.Aggregates.RecordAggregate_SerializingRecordVisitor::VisitRecord(NPOI.HSSF.Record.Record)
extern void SerializingRecordVisitor_VisitRecord_m199D507DD8CE1504393A749400E7EE5C49DD413C (void);
// 0x0000003D System.Void NPOI.HSSF.Record.Aggregates.RecordAggregate_RecordSizingVisitor::.ctor()
extern void RecordSizingVisitor__ctor_m9E6AE76D3114A4E289D82B605BFB2A4BFA7ED708 (void);
// 0x0000003E System.Int32 NPOI.HSSF.Record.Aggregates.RecordAggregate_RecordSizingVisitor::get_TotalSize()
extern void RecordSizingVisitor_get_TotalSize_mA914C58EB358659B34781970E247021DD629E2E2 (void);
// 0x0000003F System.Void NPOI.HSSF.Record.Aggregates.RecordAggregate_RecordSizingVisitor::VisitRecord(NPOI.HSSF.Record.Record)
extern void RecordSizingVisitor_VisitRecord_mF38613F6A0FB2EC18BF004F8914B45E1094B0926 (void);
// 0x00000040 System.Void NPOI.HSSF.Record.Aggregates.RecordVisitor::VisitRecord(NPOI.HSSF.Record.Record)
// 0x00000041 System.Void NPOI.HSSF.Record.Record::.ctor()
extern void Record__ctor_mE255BD311311814FDF6A357C7D1FDD49BBC109BA (void);
// 0x00000042 System.Int16 NPOI.HSSF.Record.Record::get_Sid()
// 0x00000043 System.Object NPOI.HSSF.Record.Record::Clone()
extern void Record_Clone_mA6A1C792A05B8FE83480931F1E11E4A1BDD964B1 (void);
// 0x00000044 System.Int32 NPOI.HSSF.Record.StandardRecord::get_DataSize()
// 0x00000045 System.Int32 NPOI.HSSF.Record.StandardRecord::get_RecordSize()
extern void StandardRecord_get_RecordSize_m782578B09E0EA111BD0B261F2E5366F60ACEFD6F (void);
// 0x00000046 System.Int32 NPOI.HSSF.Record.StandardRecord::Serialize(System.Int32,System.Byte[])
extern void StandardRecord_Serialize_m4BFAD8E5C4A5FF56C29D4F001265304A61146051 (void);
// 0x00000047 System.Void NPOI.HSSF.Record.StandardRecord::Serialize(NPOI.Util.ILittleEndianOutput)
// 0x00000048 System.Void NPOI.HSSF.Record.StandardRecord::.ctor()
extern void StandardRecord__ctor_m37E579B198FE968565986B6EDF87C559EA58FD5A (void);
// 0x00000049 System.Void NPOI.HSSF.Record.Chart.AlRunsRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AlRunsRecord_Serialize_m8289867DAA1A698EE88C8B33A6F9A0E18680B6E6 (void);
// 0x0000004A System.Int32 NPOI.HSSF.Record.Chart.AlRunsRecord::get_DataSize()
extern void AlRunsRecord_get_DataSize_m32CCA4D9490B04832EC9F25D0F173B33228A4091 (void);
// 0x0000004B System.Int16 NPOI.HSSF.Record.Chart.AlRunsRecord::get_Sid()
extern void AlRunsRecord_get_Sid_m6D5CF829EF452C52BA99960153122549FD33FA1A (void);
// 0x0000004C System.Void NPOI.HSSF.Record.Chart.AlRunsRecord_CTFormat::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CTFormat_Serialize_m3FA70A4B746DE6278880B70B0ABA18BFE3F766D5 (void);
// 0x0000004D System.Void NPOI.HSSF.Record.Chart.AttachedLabelRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AttachedLabelRecord_Serialize_m8F39FDF28FEC34D99B8EC87E7590E4CC9D0622CA (void);
// 0x0000004E System.Int32 NPOI.HSSF.Record.Chart.AttachedLabelRecord::get_DataSize()
extern void AttachedLabelRecord_get_DataSize_mA1E7BBD825DE4A015E0261DAC7D32DEBBAF39237 (void);
// 0x0000004F System.Int16 NPOI.HSSF.Record.Chart.AttachedLabelRecord::get_Sid()
extern void AttachedLabelRecord_get_Sid_mF9FE3D7A831494A522883C9B6F4DC1C07E665060 (void);
// 0x00000050 System.Void NPOI.HSSF.Record.Chart.AxcExtRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AxcExtRecord_Serialize_m5B37B09FE98B5681E2593E8FA6BB670902D09BC8 (void);
// 0x00000051 System.Int32 NPOI.HSSF.Record.Chart.AxcExtRecord::get_DataSize()
extern void AxcExtRecord_get_DataSize_mDDDF518DB8E58FFDA52B1E55AD4A62158302F3E1 (void);
// 0x00000052 System.Int16 NPOI.HSSF.Record.Chart.AxcExtRecord::get_Sid()
extern void AxcExtRecord_get_Sid_m61F9A6D9C81407AE834954BBC4A90F7055CA853D (void);
// 0x00000053 System.Void NPOI.HSSF.Record.Chart.AxesUsedRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AxesUsedRecord_Serialize_mA3579853C1F4CB83162F6BB0DA4BD43465D0A38B (void);
// 0x00000054 System.Int32 NPOI.HSSF.Record.Chart.AxesUsedRecord::get_DataSize()
extern void AxesUsedRecord_get_DataSize_m8F403F9B5D099590F14D5A49F1978E1C541E50B1 (void);
// 0x00000055 System.Int16 NPOI.HSSF.Record.Chart.AxesUsedRecord::get_Sid()
extern void AxesUsedRecord_get_Sid_m609BDA75CD00408B6BD7E78B2A9B32A718939D61 (void);
// 0x00000056 System.Void NPOI.HSSF.Record.Chart.AxisLineRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AxisLineRecord_Serialize_m21A1D8013E6E06DE789A8DB1B118279ADB3820D1 (void);
// 0x00000057 System.Int32 NPOI.HSSF.Record.Chart.AxisLineRecord::get_DataSize()
extern void AxisLineRecord_get_DataSize_mF896B3B7D38A5554A575E3E18D973929E949C3C9 (void);
// 0x00000058 System.Int16 NPOI.HSSF.Record.Chart.AxisLineRecord::get_Sid()
extern void AxisLineRecord_get_Sid_mCCF57285A575F1C40D8B10F1B05B0456ED7DC566 (void);
// 0x00000059 System.Int32 NPOI.HSSF.Record.RowDataRecord::get_DataSize()
extern void RowDataRecord_get_DataSize_mE5FDA1D36434A3FCEF94D100A33FCC6CEC4745D9 (void);
// 0x0000005A System.Void NPOI.HSSF.Record.RowDataRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void RowDataRecord_Serialize_mE1F74FFB19986F31142E24C78D3D3C2E1BEF5D7B (void);
// 0x0000005B System.Int16 NPOI.HSSF.Record.RowDataRecord::get_Sid()
extern void RowDataRecord_get_Sid_mEDB19EB64DE32695D89ABD641E213061055C5D18 (void);
// 0x0000005C System.Void NPOI.HSSF.Record.Chart.BRAIRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BRAIRecord_Serialize_mF4890C0A1E650C7587E910666DE90DE35FDF5BB5 (void);
// 0x0000005D System.Int32 NPOI.HSSF.Record.Chart.BRAIRecord::get_DataSize()
extern void BRAIRecord_get_DataSize_m58384ABDF0C00EC1A6E0108A7DB3D0BB505506CC (void);
// 0x0000005E System.Int16 NPOI.HSSF.Record.Chart.BRAIRecord::get_Sid()
extern void BRAIRecord_get_Sid_mECB293FE25E21653E9AAB9D7BDED4066531DE9B6 (void);
// 0x0000005F System.Void NPOI.HSSF.Record.Chart.CatSerRangeRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CatSerRangeRecord_Serialize_mBBA146550EFEE2DB427F6B66F01456C0F1656284 (void);
// 0x00000060 System.Int32 NPOI.HSSF.Record.Chart.CatSerRangeRecord::get_DataSize()
extern void CatSerRangeRecord_get_DataSize_mC75318638BEFDBCB32722BEDC1AE579BD0C4D5D3 (void);
// 0x00000061 System.Int16 NPOI.HSSF.Record.Chart.CatSerRangeRecord::get_Sid()
extern void CatSerRangeRecord_get_Sid_m8B88A35B348FF0A5EEE50A1C1B3F46927987CDBF (void);
// 0x00000062 System.Void NPOI.HSSF.Record.Chart.Chart3DBarShapeRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void Chart3DBarShapeRecord_Serialize_m08D607F2A28A0E0C4641DAE04FB83EFDCF587342 (void);
// 0x00000063 System.Int32 NPOI.HSSF.Record.Chart.Chart3DBarShapeRecord::get_DataSize()
extern void Chart3DBarShapeRecord_get_DataSize_m06F06835E49A53E93C83C971F053E9B510251282 (void);
// 0x00000064 System.Int16 NPOI.HSSF.Record.Chart.Chart3DBarShapeRecord::get_Sid()
extern void Chart3DBarShapeRecord_get_Sid_m6610C3EEFB6AD46B3FE7EB9D53D0FB25C8675A13 (void);
// 0x00000065 System.Int32 NPOI.HSSF.Record.Chart.DataLabExtRecord::get_DataSize()
extern void DataLabExtRecord_get_DataSize_m5F7F88B821D52E781DE56F76BF61D19CB287B8ED (void);
// 0x00000066 System.Int16 NPOI.HSSF.Record.Chart.DataLabExtRecord::get_Sid()
extern void DataLabExtRecord_get_Sid_mB70207110A9AAAE1FF466AD8C3009B5F8E0A9BF5 (void);
// 0x00000067 System.Void NPOI.HSSF.Record.Chart.DataLabExtRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DataLabExtRecord_Serialize_mC541058548CB2DC2DA983B28394D930065C44FAC (void);
// 0x00000068 System.Void NPOI.HSSF.Record.Chart.DefaultTextRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DefaultTextRecord_Serialize_m476EEA51B145315AE669D975E5BB5CF6E406C553 (void);
// 0x00000069 System.Int32 NPOI.HSSF.Record.Chart.DefaultTextRecord::get_DataSize()
extern void DefaultTextRecord_get_DataSize_m7615812A5CF9DD25995B45C962CAFBC6D0C87C04 (void);
// 0x0000006A System.Int16 NPOI.HSSF.Record.Chart.DefaultTextRecord::get_Sid()
extern void DefaultTextRecord_get_Sid_m696AA3CF10E4712DE28B9F4C77995C1B0EBBE974 (void);
// 0x0000006B NPOI.HSSF.Record.Chart.ObjectKind NPOI.HSSF.Record.Chart.EndBlockRecord::get_ObjectKind()
extern void EndBlockRecord_get_ObjectKind_m6C733A2243FCB024076530FBC6087D35E39A3302 (void);
// 0x0000006C System.Int32 NPOI.HSSF.Record.Chart.EndBlockRecord::get_DataSize()
extern void EndBlockRecord_get_DataSize_m8B876FBB6C2460127C199598131CDAF245816F6F (void);
// 0x0000006D System.Int16 NPOI.HSSF.Record.Chart.EndBlockRecord::get_Sid()
extern void EndBlockRecord_get_Sid_m7DFF34990DAB87532AA77190EB2D32687522048F (void);
// 0x0000006E System.Void NPOI.HSSF.Record.Chart.EndBlockRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void EndBlockRecord_Serialize_m87AB5D0E64CBBD6CE8E383CD559B43DA272D3C23 (void);
// 0x0000006F System.Void NPOI.HSSF.Record.Chart.FbiRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FbiRecord_Serialize_mEC6EABC8A3FD1CFBF17AE646722C026079C5D32E (void);
// 0x00000070 System.Int32 NPOI.HSSF.Record.Chart.FbiRecord::get_DataSize()
extern void FbiRecord_get_DataSize_m7F2FBEFB163995C54D40676FBD4DC3944FFD484A (void);
// 0x00000071 System.Int16 NPOI.HSSF.Record.Chart.FbiRecord::get_Sid()
extern void FbiRecord_get_Sid_m3A83A79ACFA1EC4AD45EE260F4FDFF2C6477B29C (void);
// 0x00000072 System.Void NPOI.HSSF.Record.Chart.FontXRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FontXRecord_Serialize_mFA0114B5F1673B1FAEB76AC81D8F4CA58554D65E (void);
// 0x00000073 System.Int32 NPOI.HSSF.Record.Chart.FontXRecord::get_DataSize()
extern void FontXRecord_get_DataSize_mBF042AA25E42814E13319F269783440DE4D831CC (void);
// 0x00000074 System.Int16 NPOI.HSSF.Record.Chart.FontXRecord::get_Sid()
extern void FontXRecord_get_Sid_m784AC19031C356C35B110BBB307AD2239E28D56F (void);
// 0x00000075 System.Void NPOI.HSSF.Record.Chart.IFmtRecordRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void IFmtRecordRecord_Serialize_mBA67FAD3C58802B45729EFF82D82EC05047E45B3 (void);
// 0x00000076 System.Int32 NPOI.HSSF.Record.Chart.IFmtRecordRecord::get_DataSize()
extern void IFmtRecordRecord_get_DataSize_m50BA0D4BD4DB9FD64902C5DB3B19616A7BE05407 (void);
// 0x00000077 System.Int16 NPOI.HSSF.Record.Chart.IFmtRecordRecord::get_Sid()
extern void IFmtRecordRecord_get_Sid_mF63C56134DEEA22C06D2305338642B6FE6279918 (void);
// 0x00000078 System.Void NPOI.HSSF.Record.Chart.SerToCrtRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SerToCrtRecord_Serialize_mE04918E5EA6CED3D3412EC7782B5D94067C9ABB3 (void);
// 0x00000079 System.Int32 NPOI.HSSF.Record.Chart.SerToCrtRecord::get_DataSize()
extern void SerToCrtRecord_get_DataSize_m774D582E0DD19FC51F76FA01F004D4E5BF29EDF5 (void);
// 0x0000007A System.Int16 NPOI.HSSF.Record.Chart.SerToCrtRecord::get_Sid()
extern void SerToCrtRecord_get_Sid_mE2C80B049B2E56AA82FF873E9500B61D8016F6BE (void);
// 0x0000007B System.Void NPOI.HSSF.Record.Chart.ShtPropsRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ShtPropsRecord_Serialize_m4F34F5D1AB59E660D558C118161755553D9E6CED (void);
// 0x0000007C System.Int32 NPOI.HSSF.Record.Chart.ShtPropsRecord::get_DataSize()
extern void ShtPropsRecord_get_DataSize_mFC6500B9023AB54202028AA728A963530E6DF093 (void);
// 0x0000007D System.Int16 NPOI.HSSF.Record.Chart.ShtPropsRecord::get_Sid()
extern void ShtPropsRecord_get_Sid_m7355967A6C2C54EABF2DECABD96235FC3DBEE9C7 (void);
// 0x0000007E NPOI.HSSF.Record.Chart.ObjectKind NPOI.HSSF.Record.Chart.StartBlockRecord::get_ObjectKind()
extern void StartBlockRecord_get_ObjectKind_mDC04B947B8F22C79D609980BAA997EDCAD25163F (void);
// 0x0000007F System.Int16 NPOI.HSSF.Record.Chart.StartBlockRecord::get_ObjectContext()
extern void StartBlockRecord_get_ObjectContext_mF398C1F021B07BC8BB6D86181866631578DCD508 (void);
// 0x00000080 System.Int16 NPOI.HSSF.Record.Chart.StartBlockRecord::get_ObjectInstance1()
extern void StartBlockRecord_get_ObjectInstance1_mAB442696983C9A4BF33B04FA8BDDBA8536180AFE (void);
// 0x00000081 System.Int16 NPOI.HSSF.Record.Chart.StartBlockRecord::get_ObjectInstance2()
extern void StartBlockRecord_get_ObjectInstance2_m291ED108E735DAA9BB604AA8AEB334AC0D00D75F (void);
// 0x00000082 System.Int32 NPOI.HSSF.Record.Chart.StartBlockRecord::get_DataSize()
extern void StartBlockRecord_get_DataSize_m5D17CEC95AE3EB9AFDFBABBAE2EABDA4499734D7 (void);
// 0x00000083 System.Int16 NPOI.HSSF.Record.Chart.StartBlockRecord::get_Sid()
extern void StartBlockRecord_get_Sid_mC55D83720BFC5ACDD49F9C1315C937A302972B7B (void);
// 0x00000084 System.Void NPOI.HSSF.Record.Chart.StartBlockRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void StartBlockRecord_Serialize_m4644D2EBC2CE6ED5AF374F934FF7BCA575945281 (void);
// 0x00000085 System.Void NPOI.HSSF.Record.Chart.StartBlockRecord::.cctor()
extern void StartBlockRecord__cctor_mD70C67543624C2B3E384470CBA17C33094CC908F (void);
// 0x00000086 System.Int32 NPOI.HSSF.Record.DConRefRecord::get_DataSize()
extern void DConRefRecord_get_DataSize_m9A33DDF46FB99C25CA754DFAB2C323C764881631 (void);
// 0x00000087 System.Void NPOI.HSSF.Record.DConRefRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DConRefRecord_Serialize_mCD2898F64B0B51B3CA695EC0044CA8083E8B82B3 (void);
// 0x00000088 System.Int16 NPOI.HSSF.Record.DConRefRecord::get_Sid()
extern void DConRefRecord_get_Sid_m3A98530F5638613685CFA71976B2D67CE6615C83 (void);
// 0x00000089 System.String NPOI.POIFS.FileSystem.Entry::get_Name()
// 0x0000008A System.Boolean NPOI.POIFS.FileSystem.Entry::get_IsDirectoryEntry()
// 0x0000008B System.Boolean NPOI.POIFS.FileSystem.Entry::get_IsDocumentEntry()
// 0x0000008C NPOI.POIFS.FileSystem.DirectoryEntry NPOI.POIFS.FileSystem.Entry::get_Parent()
// 0x0000008D System.Collections.Generic.IEnumerator`1<NPOI.POIFS.FileSystem.Entry> NPOI.POIFS.FileSystem.DirectoryEntry::get_Entries()
// 0x0000008E NPOI.POIFS.FileSystem.DocumentEntry NPOI.POIFS.FileSystem.DirectoryEntry::CreateDocument(System.String,System.IO.Stream)
// 0x0000008F NPOI.POIFS.FileSystem.DirectoryEntry NPOI.POIFS.FileSystem.DirectoryEntry::CreateDirectory(System.String)
// 0x00000090 System.Void NPOI.Util.POIUtils::CopyNodeRecursively(NPOI.POIFS.FileSystem.Entry,NPOI.POIFS.FileSystem.DirectoryEntry)
extern void POIUtils_CopyNodeRecursively_mA13261BAA8481E2EF35F1BB3E3B5D786144C6DFE (void);
// 0x00000091 System.Void NPOI.Util.POIUtils::CopyNodes(NPOI.POIFS.FileSystem.DirectoryEntry,NPOI.POIFS.FileSystem.DirectoryEntry,System.Collections.Generic.List`1<System.String>)
extern void POIUtils_CopyNodes_m06F8C1BDAE0A94038A565FD727B7A4FF878E56F2 (void);
// 0x00000092 System.Void NPOI.SS.Formula.Atp.ArgumentsEvaluator::.ctor()
extern void ArgumentsEvaluator__ctor_mAECBB577F562C40B9216161EDA7A9B127BFB5218 (void);
// 0x00000093 System.Void NPOI.SS.Formula.Atp.ArgumentsEvaluator::.cctor()
extern void ArgumentsEvaluator__cctor_mB6F1DB9C925CCC93BFD5ECB0AE2E4070DF0BEE88 (void);
// 0x00000094 System.Void NPOI.SS.Formula.Atp.IfError::.ctor()
extern void IfError__ctor_mC34802D5C4B3144707AEB225B352532C5A288B52 (void);
// 0x00000095 System.Void NPOI.SS.Formula.Atp.IfError::.cctor()
extern void IfError__cctor_mA9FAA16DAFDAAB7FB68E0033F15C4967A03A3081 (void);
// 0x00000096 System.Void NPOI.SS.Formula.Atp.NetworkdaysFunction::.ctor(NPOI.SS.Formula.Atp.ArgumentsEvaluator)
extern void NetworkdaysFunction__ctor_m4AE1F8C052FA78E76F1110091D5ADC9B6965561B (void);
// 0x00000097 System.Void NPOI.SS.Formula.Atp.NetworkdaysFunction::.cctor()
extern void NetworkdaysFunction__cctor_mDA2B3CD3A3952E1F585328A402C1892A80231D17 (void);
// 0x00000098 System.Void NPOI.SS.Formula.Atp.WorkdayFunction::.ctor(NPOI.SS.Formula.Atp.ArgumentsEvaluator)
extern void WorkdayFunction__ctor_mB730411AD0E7F5E235201882B59BCA348A6A8679 (void);
// 0x00000099 System.Void NPOI.SS.Formula.Atp.WorkdayFunction::.cctor()
extern void WorkdayFunction__cctor_m60479CA836F45DF5FB0836FAF9412E0891A06291 (void);
// 0x0000009A System.Void NPOI.SS.Formula.Functions.Var2or3ArgFunction::.ctor()
extern void Var2or3ArgFunction__ctor_m74EA429B91DB4FBE5C098FB3EFE580D83D8F3B79 (void);
// 0x0000009B System.Void NPOI.SS.Formula.Functions.Complex::.ctor()
extern void Complex__ctor_m4A9FCC73E094239B3C29608FC6975C9D683B5894 (void);
// 0x0000009C System.Void NPOI.SS.Formula.Functions.Complex::.cctor()
extern void Complex__cctor_m72F6E712D57C3EE9AE36592C3D1E2C9389BD86DF (void);
// 0x0000009D System.Void NPOI.SS.Formula.Functions.Sumifs::.ctor()
extern void Sumifs__ctor_mC85204038F2718D3817AB992D350DF28A04753F9 (void);
// 0x0000009E System.Void NPOI.SS.Formula.Functions.Sumifs::.cctor()
extern void Sumifs__cctor_m1F9119E0240A4859393E497C2199424FB7381DE6 (void);
// 0x0000009F System.Void NPOI.HSSF.Record.Aggregates.ChartSubstreamRecordAggregate::.ctor(NPOI.HSSF.Model.RecordStream)
extern void ChartSubstreamRecordAggregate__ctor_m2BF7CDF90AEE650123BA091A58B7082E591B52DF (void);
// 0x000000A0 System.Void NPOI.HSSF.Record.Aggregates.ChartSubstreamRecordAggregate::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void ChartSubstreamRecordAggregate_VisitContainedRecords_mC221545C39F114E0D79C597C60C2B6C4AEF4FD01 (void);
// 0x000000A1 System.Void NPOI.HSSF.Record.Aggregates.WorksheetProtectionBlock::.ctor()
extern void WorksheetProtectionBlock__ctor_m446303FB4E3CFE3F05A51930771563A46FFAEE83 (void);
// 0x000000A2 System.Boolean NPOI.HSSF.Record.Aggregates.WorksheetProtectionBlock::IsComponentRecord(System.Int32)
extern void WorksheetProtectionBlock_IsComponentRecord_m73773C1CB029D6A163C0ACE680B38E8324F9A070 (void);
// 0x000000A3 System.Boolean NPOI.HSSF.Record.Aggregates.WorksheetProtectionBlock::ReadARecord(NPOI.HSSF.Model.RecordStream)
extern void WorksheetProtectionBlock_ReadARecord_mF524D9648A236A5AEB88D14CFD5817CC7C7D0E0C (void);
// 0x000000A4 System.Void NPOI.HSSF.Record.Aggregates.WorksheetProtectionBlock::CheckNotPresent(NPOI.HSSF.Record.Record)
extern void WorksheetProtectionBlock_CheckNotPresent_mCDDA8F0F67129CEB03968490808127E3E31B1EE2 (void);
// 0x000000A5 System.Void NPOI.HSSF.Record.Aggregates.WorksheetProtectionBlock::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void WorksheetProtectionBlock_VisitContainedRecords_m650D7FC03FE358156CA2A3D5EE1141339A6864F3 (void);
// 0x000000A6 System.Void NPOI.HSSF.Record.Aggregates.WorksheetProtectionBlock::VisitIfPresent(NPOI.HSSF.Record.Record,NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void WorksheetProtectionBlock_VisitIfPresent_m15DB497CE56AC5AA3975949993BCEEB81D860D74 (void);
// 0x000000A7 System.Void NPOI.HSSF.Record.Aggregates.WorksheetProtectionBlock::AddRecords(NPOI.HSSF.Model.RecordStream)
extern void WorksheetProtectionBlock_AddRecords_mF0BA084A87098D3B1C5569F53090874BF68F8736 (void);
// 0x000000A8 System.Int16 NPOI.HSSF.Record.AutoFilter.AutoFilterRecord::get_Sid()
extern void AutoFilterRecord_get_Sid_m3B49DC4FB0F7ABD68A390CFC3C3EC2DF39402621 (void);
// 0x000000A9 System.Int32 NPOI.HSSF.Record.AutoFilter.AutoFilterRecord::get_DataSize()
extern void AutoFilterRecord_get_DataSize_mF98C17ACB40D40484E0A1FF40449F391DBC4D170 (void);
// 0x000000AA System.Void NPOI.HSSF.Record.AutoFilter.AutoFilterRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AutoFilterRecord_Serialize_m191202A4D1BEA2C4DF15F5DA0486324F06AD46EE (void);
// 0x000000AB System.Void NPOI.HSSF.Record.AutoFilter.AutoFilterRecord::.cctor()
extern void AutoFilterRecord__cctor_m13BBF0C329D9D4FF2CCADAB2847106352A693D53 (void);
// 0x000000AC System.Int16 NPOI.HSSF.Record.AutoFilter.FilterModeRecord::get_Sid()
extern void FilterModeRecord_get_Sid_m2D95C92B23EFA4D6F0AB9B8CE8FE03D5246273B5 (void);
// 0x000000AD System.Int32 NPOI.HSSF.Record.AutoFilter.FilterModeRecord::get_DataSize()
extern void FilterModeRecord_get_DataSize_mE13295C37DAB4BC560DDFDCA732D4D60844F61AD (void);
// 0x000000AE System.Void NPOI.HSSF.Record.AutoFilter.FilterModeRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FilterModeRecord_Serialize_mC24DDD268C5DFF15B05D2BA3DBF6CB87857A1082 (void);
// 0x000000AF System.Int32 NPOI.HSSF.Record.Chart.Chart3dRecord::get_DataSize()
extern void Chart3dRecord_get_DataSize_m112B107F670FD2FE865A4F0059F2A6673F57CADD (void);
// 0x000000B0 System.Void NPOI.HSSF.Record.Chart.Chart3dRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void Chart3dRecord_Serialize_m654B0079E52CB207A7321C6190CAB7ED46AAE9E9 (void);
// 0x000000B1 System.Int16 NPOI.HSSF.Record.Chart.Chart3dRecord::get_Sid()
extern void Chart3dRecord_get_Sid_mC9D6056453557C4494167F6020FE33605B5D98AB (void);
// 0x000000B2 System.Int32 NPOI.HSSF.Record.Chart.CrtLayout12ARecord::get_DataSize()
extern void CrtLayout12ARecord_get_DataSize_m72A3D3EDD5893F0500CE344A9265169C2F3FB827 (void);
// 0x000000B3 System.Void NPOI.HSSF.Record.Chart.CrtLayout12ARecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CrtLayout12ARecord_Serialize_m3F6CD4855DA026F9F391C91A3CCD7B594BD2A8AA (void);
// 0x000000B4 System.Int16 NPOI.HSSF.Record.Chart.CrtLayout12ARecord::get_Sid()
extern void CrtLayout12ARecord_get_Sid_m7A0FB550DB22A96298A87B94FC85EE805C699CE1 (void);
// 0x000000B5 System.Int32 NPOI.HSSF.Record.Chart.CrtLayout12Record::get_DataSize()
extern void CrtLayout12Record_get_DataSize_m50C0EECFA325A9B76D15C3059AC43C6B3F022592 (void);
// 0x000000B6 System.Void NPOI.HSSF.Record.Chart.CrtLayout12Record::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CrtLayout12Record_Serialize_m1145B3BD4392B2BF9B1C5DD1C32E3DD1C0C30296 (void);
// 0x000000B7 System.Int16 NPOI.HSSF.Record.Chart.CrtLayout12Record::get_Sid()
extern void CrtLayout12Record_get_Sid_m567DBCD333712C66ECCDE50535C69D0639E617F9 (void);
// 0x000000B8 System.Void NPOI.HSSF.Record.Chart.CrtLayout12Record::.cctor()
extern void CrtLayout12Record__cctor_mF28AC53759218E47A980F01BBC373C3BC202220C (void);
// 0x000000B9 System.Int32 NPOI.HSSF.Record.Chart.MarkerFormatRecord::get_DataSize()
extern void MarkerFormatRecord_get_DataSize_mA9B30DB1EF73E51588620C961547132213FEA12E (void);
// 0x000000BA System.Void NPOI.HSSF.Record.Chart.MarkerFormatRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void MarkerFormatRecord_Serialize_m0EE1C0851E1CC753166DE554F3D850C3E18C2607 (void);
// 0x000000BB System.Int16 NPOI.HSSF.Record.Chart.MarkerFormatRecord::get_Sid()
extern void MarkerFormatRecord_get_Sid_m8019543B349A83DA70843934DD6259BC31059076 (void);
// 0x000000BC System.Int32 NPOI.HSSF.Record.Chart.PieFormatRecord::get_DataSize()
extern void PieFormatRecord_get_DataSize_m7800EF2A841E53591BFFC5A246588729453A3CB2 (void);
// 0x000000BD System.Void NPOI.HSSF.Record.Chart.PieFormatRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PieFormatRecord_Serialize_m53A173584057F06F35E5E0C1CF9088F24E688F5A (void);
// 0x000000BE System.Int16 NPOI.HSSF.Record.Chart.PieFormatRecord::get_Sid()
extern void PieFormatRecord_get_Sid_mF8BFC3A91C98C63E1902B54E0BCC013429FCB15D (void);
// 0x000000BF System.Int32 NPOI.HSSF.Record.Chart.PieRecord::get_DataSize()
extern void PieRecord_get_DataSize_m497E097C4387930291E217E35127F476A3EB51BC (void);
// 0x000000C0 System.Void NPOI.HSSF.Record.Chart.PieRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PieRecord_Serialize_m0E3D87665651FFC0376DFA000FC48779B98611AD (void);
// 0x000000C1 System.Int16 NPOI.HSSF.Record.Chart.PieRecord::get_Sid()
extern void PieRecord_get_Sid_m24D371C9C1D443C394A326509E700C8E456D8880 (void);
// 0x000000C2 System.Void NPOI.HSSF.Record.UnicodeString::.ctor()
extern void UnicodeString__ctor_mD57F53DB35A89F924647958BD23F7295F7294DB1 (void);
// 0x000000C3 System.Void NPOI.HSSF.Record.UnicodeString::.ctor(System.String)
extern void UnicodeString__ctor_m6EAEA26F4A00BDBC65611232311C50B17016D9F9 (void);
// 0x000000C4 System.Int32 NPOI.HSSF.Record.UnicodeString::GetHashCode()
extern void UnicodeString_GetHashCode_m9018E49602B987AFC9477C6F98CC8D51B965C855 (void);
// 0x000000C5 System.Boolean NPOI.HSSF.Record.UnicodeString::Equals(System.Object)
extern void UnicodeString_Equals_m5AE91ED767950342A2F6C87E14A0E84BAA1FF076 (void);
// 0x000000C6 System.Int32 NPOI.HSSF.Record.UnicodeString::get_CharCount()
extern void UnicodeString_get_CharCount_m4F57EE74A8631C6F626C15E197A7FD7B05373D55 (void);
// 0x000000C7 System.Void NPOI.HSSF.Record.UnicodeString::set_CharCount(System.Int32)
extern void UnicodeString_set_CharCount_m72E71DFED0FB7B7E9ECDA8F0A27276E0C411AEF9 (void);
// 0x000000C8 System.Byte NPOI.HSSF.Record.UnicodeString::get_OptionFlags()
extern void UnicodeString_get_OptionFlags_mBDC6CF391CE2D1D08143B618E11D1F566B6C2C4D (void);
// 0x000000C9 System.String NPOI.HSSF.Record.UnicodeString::get_String()
extern void UnicodeString_get_String_mDD16701C32D3A9A19AAF698243F63628ECE5DBDB (void);
// 0x000000CA System.Void NPOI.HSSF.Record.UnicodeString::set_String(System.String)
extern void UnicodeString_set_String_mE999817575FAD3A570C04B871A2DAA5203D6F401 (void);
// 0x000000CB System.Int32 NPOI.HSSF.Record.UnicodeString::get_FormatRunCount()
extern void UnicodeString_get_FormatRunCount_mC3F8FC97485F693D5D3567746A317581596CC60F (void);
// 0x000000CC NPOI.HSSF.Record.UnicodeString_FormatRun NPOI.HSSF.Record.UnicodeString::GetFormatRun(System.Int32)
extern void UnicodeString_GetFormatRun_m1D22120AB55E2BCA6F9614CE287B75E9B21E0408 (void);
// 0x000000CD System.String NPOI.HSSF.Record.UnicodeString::ToString()
extern void UnicodeString_ToString_m99AC54F22F25E5A64FFE9F93C42DBDC60A85D7A9 (void);
// 0x000000CE System.String NPOI.HSSF.Record.UnicodeString::GetDebugInfo()
extern void UnicodeString_GetDebugInfo_m6608325A4D38BEB0F88E14E6D0FC6E318B717D02 (void);
// 0x000000CF System.Void NPOI.HSSF.Record.UnicodeString::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void UnicodeString_Serialize_m8F22917CF43D03E63806FE276B3B2C78EFE031EC (void);
// 0x000000D0 System.Int32 NPOI.HSSF.Record.UnicodeString::CompareTo(NPOI.HSSF.Record.UnicodeString)
extern void UnicodeString_CompareTo_m0A0DB29AB7E155C85E4D406EFB28EFC53A9BE6E0 (void);
// 0x000000D1 System.Boolean NPOI.HSSF.Record.UnicodeString::get_IsRichText()
extern void UnicodeString_get_IsRichText_m910CE1DA2AB7334EBEAC7CEA712615E4C60BC490 (void);
// 0x000000D2 System.Boolean NPOI.HSSF.Record.UnicodeString::get_IsExtendedText()
extern void UnicodeString_get_IsExtendedText_mD871CB72878D588C59E40D02A209B61A8C17B384 (void);
// 0x000000D3 System.Object NPOI.HSSF.Record.UnicodeString::Clone()
extern void UnicodeString_Clone_m662F1C04C85085582B78720BDC6F54A3C21C4576 (void);
// 0x000000D4 System.Void NPOI.HSSF.Record.UnicodeString::.cctor()
extern void UnicodeString__cctor_mB768C2FC3B11CA40B0D7DD3BB55F771F6D05BC8D (void);
// 0x000000D5 System.Void NPOI.HSSF.Record.UnicodeString_FormatRun::.ctor(System.Int16,System.Int16)
extern void FormatRun__ctor_mA3F844FB5D6684413611005C87AB2EF6EAEA9CB7 (void);
// 0x000000D6 System.Int16 NPOI.HSSF.Record.UnicodeString_FormatRun::get_CharacterPos()
extern void FormatRun_get_CharacterPos_m65D1731ACDA2C019887D25E75B26DED3FF8AD12B (void);
// 0x000000D7 System.Int16 NPOI.HSSF.Record.UnicodeString_FormatRun::get_FontIndex()
extern void FormatRun_get_FontIndex_mE63AD5A9962B1C3A96B73F1E33BBC311C88DD0B5 (void);
// 0x000000D8 System.Boolean NPOI.HSSF.Record.UnicodeString_FormatRun::Equals(System.Object)
extern void FormatRun_Equals_mE7A1E715C5D9FDCAF956C080B59A4F53567A15E6 (void);
// 0x000000D9 System.Int32 NPOI.HSSF.Record.UnicodeString_FormatRun::GetHashCode()
extern void FormatRun_GetHashCode_mF0E37DD324BD23B59C3D45DAF6EE3067BBA45EAC (void);
// 0x000000DA System.Int32 NPOI.HSSF.Record.UnicodeString_FormatRun::CompareTo(NPOI.HSSF.Record.UnicodeString_FormatRun)
extern void FormatRun_CompareTo_m96087FAF85EB87ECF9AF11BE39669D1106B5E3A9 (void);
// 0x000000DB System.String NPOI.HSSF.Record.UnicodeString_FormatRun::ToString()
extern void FormatRun_ToString_mDEF533A49D54588E5AFFEC7476C8F1967A69A7F5 (void);
// 0x000000DC System.Void NPOI.HSSF.Record.UnicodeString_FormatRun::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FormatRun_Serialize_m750F7D39F72CC87BEBD7B87AEF8062766F581CF3 (void);
// 0x000000DD System.Void NPOI.HSSF.Record.UnicodeString_ExtRst::populateEmpty()
extern void ExtRst_populateEmpty_m5B8AFEEFBC0431EE13E0194C43DDE9D516B1185B (void);
// 0x000000DE System.Int32 NPOI.HSSF.Record.UnicodeString_ExtRst::GetHashCode()
extern void ExtRst_GetHashCode_mAC891B884BE774B09B5030E57863BF99B1744936 (void);
// 0x000000DF System.Void NPOI.HSSF.Record.UnicodeString_ExtRst::.ctor()
extern void ExtRst__ctor_m67D05977368149ABDA8F52FFD8C3D1BD3A3E63CB (void);
// 0x000000E0 System.Int32 NPOI.HSSF.Record.UnicodeString_ExtRst::get_DataSize()
extern void ExtRst_get_DataSize_m5EEA62D427EE5F940301FAF03D9D31A4E3E50547 (void);
// 0x000000E1 System.Void NPOI.HSSF.Record.UnicodeString_ExtRst::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void ExtRst_Serialize_m42EADEFE7B8CB5C4D8F7D9BD5C3DC762F93A0371 (void);
// 0x000000E2 System.Boolean NPOI.HSSF.Record.UnicodeString_ExtRst::Equals(System.Object)
extern void ExtRst_Equals_m6150886A00FEAC78E78FC6CFF63E049079FD8EFD (void);
// 0x000000E3 System.Int32 NPOI.HSSF.Record.UnicodeString_ExtRst::CompareTo(NPOI.HSSF.Record.UnicodeString_ExtRst)
extern void ExtRst_CompareTo_mEB977CC0FB4F232D18C596F62CDDEE02A9FE5C03 (void);
// 0x000000E4 NPOI.HSSF.Record.UnicodeString_ExtRst NPOI.HSSF.Record.UnicodeString_ExtRst::Clone()
extern void ExtRst_Clone_m2166D89B2F7D7BDF841ED1439CDD81D683126533 (void);
// 0x000000E5 System.Void NPOI.HSSF.Record.UnicodeString_PhRun::.ctor(System.Int32,System.Int32,System.Int32)
extern void PhRun__ctor_m1005C3636B609556A53C1053CE5AA1065DBCACAE (void);
// 0x000000E6 System.Void NPOI.HSSF.Record.UnicodeString_PhRun::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void PhRun_Serialize_mA6FB104097A8A89FCEE15D9CE12CB3B027C2FCA6 (void);
// 0x000000E7 System.Int32 NPOI.Util.ILittleEndianInput::Available()
// 0x000000E8 System.Int32 NPOI.Util.ILittleEndianInput::ReadByte()
// 0x000000E9 System.Int32 NPOI.Util.ILittleEndianInput::ReadUByte()
// 0x000000EA System.Int16 NPOI.Util.ILittleEndianInput::ReadShort()
// 0x000000EB System.Int32 NPOI.Util.ILittleEndianInput::ReadUShort()
// 0x000000EC System.Int32 NPOI.Util.ILittleEndianInput::ReadInt()
// 0x000000ED System.Int64 NPOI.Util.ILittleEndianInput::ReadLong()
// 0x000000EE System.Double NPOI.Util.ILittleEndianInput::ReadDouble()
// 0x000000EF System.Void NPOI.Util.ILittleEndianInput::ReadFully(System.Byte[],System.Int32,System.Int32)
// 0x000000F0 System.Int32 NPOI.HSSF.Record.SubRecord::get_DataSize()
// 0x000000F1 System.Void NPOI.HSSF.Record.SubRecord::Serialize(NPOI.Util.ILittleEndianOutput)
// 0x000000F2 System.Void NPOI.HSSF.Record.SheetExtRecord::.ctor()
extern void SheetExtRecord__ctor_mCE91BBC5D3519246CF62344D2DF57E7680E178E6 (void);
// 0x000000F3 System.Void NPOI.HSSF.Record.SheetExtRecord::set_TabColorIndex(System.Int16)
extern void SheetExtRecord_set_TabColorIndex_m02FDEFFB13531466871B941971701CE9324C8DAE (void);
// 0x000000F4 System.Void NPOI.HSSF.Record.SheetExtRecord::set_IsAutoColor(System.Boolean)
extern void SheetExtRecord_set_IsAutoColor_mDC4E4DD672DD1A2AB2A7A9C52FA4C62B0C914E9B (void);
// 0x000000F5 System.Int32 NPOI.HSSF.Record.SheetExtRecord::get_DataSize()
extern void SheetExtRecord_get_DataSize_mCF39C7C72353A16A67BFC18357661495859437AF (void);
// 0x000000F6 System.Int16 NPOI.HSSF.Record.SheetExtRecord::get_Sid()
extern void SheetExtRecord_get_Sid_m1FCA3E7DC3C56E403ABB632F91D1B5A4909113E0 (void);
// 0x000000F7 System.Void NPOI.HSSF.Record.SheetExtRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SheetExtRecord_Serialize_m3A7C2A7EF2C762B2F8DBA1B08BEE517E36C1E9DC (void);
// 0x000000F8 System.String NPOI.HSSF.Record.SheetExtRecord::ToString()
extern void SheetExtRecord_ToString_mA5C1B69B63BB16F9E7F7993183A2CEA726A9B2BF (void);
// 0x000000F9 System.Object NPOI.HSSF.Record.SheetExtRecord::Clone()
extern void SheetExtRecord_Clone_m4A3587705C08305638C3D984D9B444875371CCC8 (void);
// 0x000000FA System.Void NPOI.SS.UserModel.DataFormatter::.cctor()
extern void DataFormatter__cctor_m4A5BE9132428C255697F9ED85631144814442E19 (void);
// 0x000000FB System.Void NPOI.SS.UserModel.DataFormatter::.ctor()
extern void DataFormatter__ctor_m5BC226324CCE8309926C5B0425C1FE43E28C35A7 (void);
// 0x000000FC System.Void NPOI.SS.UserModel.DataFormatter::.ctor(System.Globalization.CultureInfo)
extern void DataFormatter__ctor_m3FE570E146EFCBC29B841948F8F19C029E6D04D0 (void);
// 0x000000FD System.Void NPOI.SS.UserModel.DataFormatter::.ctor(System.Boolean)
extern void DataFormatter__ctor_m4D9E8BF6C4F7F5891F8F6E6E9CA8E6FBF81ACD41 (void);
// 0x000000FE NPOI.SS.Util.FormatBase NPOI.SS.UserModel.DataFormatter::GetFormat(NPOI.SS.UserModel.ICell)
extern void DataFormatter_GetFormat_m8689333B68E45530B2046ACBAB888E6FA0C6DB7F (void);
// 0x000000FF NPOI.SS.Util.FormatBase NPOI.SS.UserModel.DataFormatter::GetFormat(System.Double,System.Int32,System.String)
extern void DataFormatter_GetFormat_mF0E2ACB129B1E40808EA5CD5840847E1096927E9 (void);
// 0x00000100 NPOI.SS.Util.FormatBase NPOI.SS.UserModel.DataFormatter::CreateFormat(System.Double,System.Int32,System.String)
extern void DataFormatter_CreateFormat_m3D4DF976D4054B61BF03E1D298C26F2FDE46D924 (void);
// 0x00000101 System.Int32 NPOI.SS.UserModel.DataFormatter::IndexOfFraction(System.String)
extern void DataFormatter_IndexOfFraction_mF6A31616CCD87C298DC5A851B5C3F028EF22DA33 (void);
// 0x00000102 System.Int32 NPOI.SS.UserModel.DataFormatter::LastIndexOfFraction(System.String)
extern void DataFormatter_LastIndexOfFraction_m7ED16AD2B5D54D931C49A28D38E3A0F395E831CD (void);
// 0x00000103 NPOI.SS.Util.FormatBase NPOI.SS.UserModel.DataFormatter::CreateDateFormat(System.String,System.Double)
extern void DataFormatter_CreateDateFormat_m7AA0B47893D33CD77956C81610A77326419BC617 (void);
// 0x00000104 System.String NPOI.SS.UserModel.DataFormatter::cleanFormatForNumber(System.String)
extern void DataFormatter_cleanFormatForNumber_m51ABF94EB321B3AEA7002D976B0C7E51E6DDB4B7 (void);
// 0x00000105 NPOI.SS.Util.FormatBase NPOI.SS.UserModel.DataFormatter::CreateNumberFormat(System.String,System.Double)
extern void DataFormatter_CreateNumberFormat_m6E51C9D95EB31E918E6F4FF282AFBBD0867C1715 (void);
// 0x00000106 System.Boolean NPOI.SS.UserModel.DataFormatter::IsWholeNumber(System.Double)
extern void DataFormatter_IsWholeNumber_mBE32AD16CF28006AFC610844CCC6347BC12EEFA4 (void);
// 0x00000107 NPOI.SS.Util.FormatBase NPOI.SS.UserModel.DataFormatter::GetDefaultFormat(System.Double)
extern void DataFormatter_GetDefaultFormat_m5B5C1919E54C507EA9D9F9C5473508A2E5CA4D37 (void);
// 0x00000108 System.String NPOI.SS.UserModel.DataFormatter::GetFormattedDateString(NPOI.SS.UserModel.ICell)
extern void DataFormatter_GetFormattedDateString_mE055D37F09052AA5BBDC2C378C2D432D81196E6E (void);
// 0x00000109 System.String NPOI.SS.UserModel.DataFormatter::GetFormattedNumberString(NPOI.SS.UserModel.ICell)
extern void DataFormatter_GetFormattedNumberString_m45BBCDD48DC75734A70F2DA68C6E91C74D837CDC (void);
// 0x0000010A System.String NPOI.SS.UserModel.DataFormatter::FormatCellValue(NPOI.SS.UserModel.ICell)
extern void DataFormatter_FormatCellValue_mD22498139AE747C65F3704DB60A096049E8ABD08 (void);
// 0x0000010B System.String NPOI.SS.UserModel.DataFormatter::FormatCellValue(NPOI.SS.UserModel.ICell,NPOI.SS.UserModel.IFormulaEvaluator)
extern void DataFormatter_FormatCellValue_m02660243A830BB4D171F3C107407F25DFDB52E3F (void);
// 0x0000010C System.Void NPOI.SS.UserModel.DataFormatter::AddFormat(System.String,NPOI.SS.Util.FormatBase)
extern void DataFormatter_AddFormat_m7302FA2FC8FDD4E92EC7AF5BF0CD45C2032F7C1C (void);
// 0x0000010D System.DateTime NPOI.SS.UserModel.DateUtil::GetJavaDate(System.Double,System.Boolean)
extern void DateUtil_GetJavaDate_mFDAD3DD5F0079219F2024A6EB85C0E7B6059C784 (void);
// 0x0000010E System.Void NPOI.SS.UserModel.DateUtil::SetCalendar(System.DateTime&,System.Int32,System.Int32,System.Boolean)
extern void DateUtil_SetCalendar_mA095BD9ABA6D346012AF48A6407AD4EFF1647C25 (void);
// 0x0000010F System.DateTime NPOI.SS.UserModel.DateUtil::GetJavaCalendar(System.Double,System.Boolean)
extern void DateUtil_GetJavaCalendar_m881EE417B868ED7A363C5F5410ECE178117DDEB1 (void);
// 0x00000110 System.Boolean NPOI.SS.UserModel.DateUtil::IsADateFormat(System.Int32,System.String)
extern void DateUtil_IsADateFormat_mC201C001651BA76E56779408BD2B762D8C680978 (void);
// 0x00000111 System.Boolean NPOI.SS.UserModel.DateUtil::IsInternalDateFormat(System.Int32)
extern void DateUtil_IsInternalDateFormat_mD9146A6D7F59C492779D0A295BC49C6EFBAD1F63 (void);
// 0x00000112 System.Boolean NPOI.SS.UserModel.DateUtil::IsCellDateFormatted(NPOI.SS.UserModel.ICell)
extern void DateUtil_IsCellDateFormatted_m5CEF3DC94B5CF3354129B7AE2D667F18F16233C2 (void);
// 0x00000113 System.Boolean NPOI.SS.UserModel.DateUtil::IsValidExcelDate(System.Double)
extern void DateUtil_IsValidExcelDate_m7F5CBF587C9D66A373815ED23556FFF1D4F4E506 (void);
// 0x00000114 System.Void NPOI.SS.UserModel.DateUtil::.cctor()
extern void DateUtil__cctor_mA9807E884FBBA4BDCE01296C84D7BF736F28032E (void);
// 0x00000115 System.Void NPOI.HSSF.Util.LazilyConcatenatedByteArray::Concatenate(System.Byte[])
extern void LazilyConcatenatedByteArray_Concatenate_m8460B4046DF405D8FAB2CB43E9167AA592D2B71B (void);
// 0x00000116 System.Byte[] NPOI.HSSF.Util.LazilyConcatenatedByteArray::ToArray()
extern void LazilyConcatenatedByteArray_ToArray_mF341742704D4138CED6EEADDAEA22E42AC1080B2 (void);
// 0x00000117 System.Int32 NPOI.DDF.EscherBlipRecord::Serialize(System.Int32,System.Byte[],NPOI.DDF.EscherSerializationListener)
extern void EscherBlipRecord_Serialize_m2E71FFC0D913AC02A804970E641891BD39B27E37 (void);
// 0x00000118 System.Int32 NPOI.DDF.EscherBlipRecord::get_RecordSize()
extern void EscherBlipRecord_get_RecordSize_m7B921AF1B3663E79292BAE24F741FC181174492F (void);
// 0x00000119 System.Int32 NPOI.DDF.EscherBSERecord::Serialize(System.Int32,System.Byte[],NPOI.DDF.EscherSerializationListener)
extern void EscherBSERecord_Serialize_m8C91A0259FD221EBC2E8FDAE8C30E6BAF93B269E (void);
// 0x0000011A System.Int32 NPOI.DDF.EscherBSERecord::get_RecordSize()
extern void EscherBSERecord_get_RecordSize_m13D35BE3A350FDAB7EF5C7319B6789801C50F4C0 (void);
// 0x0000011B System.Void NPOI.DDF.EscherSerializationListener::BeforeRecordSerialize(System.Int32,System.Int16,NPOI.DDF.EscherRecord)
// 0x0000011C System.Void NPOI.DDF.EscherSerializationListener::AfterRecordSerialize(System.Int32,System.Int16,System.Int32,NPOI.DDF.EscherRecord)
// 0x0000011D System.Void NPOI.DDF.NullEscherSerializationListener::BeforeRecordSerialize(System.Int32,System.Int16,NPOI.DDF.EscherRecord)
extern void NullEscherSerializationListener_BeforeRecordSerialize_m1C86AC386A3993B1CC318692844682118E507EE3 (void);
// 0x0000011E System.Void NPOI.DDF.NullEscherSerializationListener::AfterRecordSerialize(System.Int32,System.Int16,System.Int32,NPOI.DDF.EscherRecord)
extern void NullEscherSerializationListener_AfterRecordSerialize_mAD89EBCD059270F39B71FA5DFD905A024EC3D48F (void);
// 0x0000011F System.Void NPOI.DDF.NullEscherSerializationListener::.ctor()
extern void NullEscherSerializationListener__ctor_m074A0C73CB4E0AD8154AEBD31EE1A6E0980CD933 (void);
// 0x00000120 NPOI.HPSF.CustomProperty NPOI.HPSF.CustomProperties::Put(System.String,NPOI.HPSF.CustomProperty)
extern void CustomProperties_Put_m551BDE5284AFAD7D96FB1A4658A3FFD04E089AF9 (void);
// 0x00000121 System.Object NPOI.HPSF.CustomProperties::Put(NPOI.HPSF.CustomProperty)
extern void CustomProperties_Put_m9F10B2909E3EAC55B13EDCBD1C79CD7CA519EFC7 (void);
// 0x00000122 System.Object NPOI.HPSF.CustomProperties::Put(System.String,System.String)
extern void CustomProperties_Put_mF667DB7DA2C1CE06A1264799E68F0C511376CCDB (void);
// 0x00000123 System.Boolean NPOI.HPSF.CustomProperties::ContainsKey(System.Object)
extern void CustomProperties_ContainsKey_mF42E15E71E762BA1A0C5A505E64F0965BD83BDC4 (void);
// 0x00000124 System.Collections.IDictionary NPOI.HPSF.CustomProperties::get_Dictionary()
extern void CustomProperties_get_Dictionary_m3933700517372F39A61CAF2BA39DC22F2C269CC4 (void);
// 0x00000125 System.Int32 NPOI.HPSF.CustomProperties::get_Codepage()
extern void CustomProperties_get_Codepage_m7BFCBE25CF49077D981FDBC300A9470B3FB141DA (void);
// 0x00000126 System.Void NPOI.HPSF.CustomProperties::set_Codepage(System.Int32)
extern void CustomProperties_set_Codepage_mA6F0475FF36A57B0E1196C327EC5C15A393384A2 (void);
// 0x00000127 System.Void NPOI.HPSF.CustomProperties::set_IsPure(System.Boolean)
extern void CustomProperties_set_IsPure_mC142BF56B82A63C044C280CBD6284A16E8AC9C13 (void);
// 0x00000128 System.Void NPOI.HPSF.CustomProperties::.ctor()
extern void CustomProperties__ctor_m02FCF47208B32F48303665BB9D68C8F3158B75E6 (void);
// 0x00000129 System.Int64 NPOI.HPSF.Property::get_ID()
extern void Property_get_ID_mC73CA23AD32EC231806B371083C5C3E334650FEC (void);
// 0x0000012A System.Void NPOI.HPSF.Property::set_ID(System.Int64)
extern void Property_set_ID_m48D18618F5A1F3C08BD4E21808EF8337CE912D42 (void);
// 0x0000012B System.Int64 NPOI.HPSF.Property::get_Type()
extern void Property_get_Type_m8AF3DCBF65D897C046E2E45AFC686CC2452A8B76 (void);
// 0x0000012C System.Void NPOI.HPSF.Property::set_Type(System.Int64)
extern void Property_set_Type_m6357AB9A12F41527FFA4A8062F44BE0D3AC5B729 (void);
// 0x0000012D System.Object NPOI.HPSF.Property::get_Value()
extern void Property_get_Value_mF8A3E52E5C741754DA83AF6820C10C5E3C95532E (void);
// 0x0000012E System.Void NPOI.HPSF.Property::set_Value(System.Object)
extern void Property_set_Value_mD7747B095EE0E3561EB82A9EFF50EFC5D4107B09 (void);
// 0x0000012F System.Void NPOI.HPSF.Property::.ctor(System.Int64,System.Int64,System.Object)
extern void Property__ctor_mD3D80D8C8ECD8375483EA175EA14DCA1504B68CA (void);
// 0x00000130 System.Void NPOI.HPSF.Property::.ctor(System.Int64,System.Byte[],System.Int64,System.Int32,System.Int32)
extern void Property__ctor_mB9EFCEF8AECAFF6A40A44EBCD9F928D661330B5E (void);
// 0x00000131 System.Void NPOI.HPSF.Property::.ctor()
extern void Property__ctor_m23B1AD3E354689BB7FCA2618B31431AD2FA1F66A (void);
// 0x00000132 System.Collections.IDictionary NPOI.HPSF.Property::ReadDictionary(System.Byte[],System.Int64,System.Int32,System.Int32)
extern void Property_ReadDictionary_m3DB1C6CDCE76E703C4FE0F0E49F769ABC58CAFB4 (void);
// 0x00000133 System.Boolean NPOI.HPSF.Property::Equals(System.Object)
extern void Property_Equals_m94BFADCEB9B1C59FF181BE94B22B20E0628343C4 (void);
// 0x00000134 System.Boolean NPOI.HPSF.Property::TypesAreEqual(System.Int64,System.Int64)
extern void Property_TypesAreEqual_m096CA025FBB02853A78523751ED3085B257E3D5B (void);
// 0x00000135 System.Int32 NPOI.HPSF.Property::GetHashCode()
extern void Property_GetHashCode_mA51024E537544C645069309FD81248335C4D19A4 (void);
// 0x00000136 System.String NPOI.HPSF.Property::ToString()
extern void Property_ToString_m73BADC96B4603F2FCC5125988192D293B5567390 (void);
// 0x00000137 System.Void NPOI.HPSF.MutableProperty::.ctor()
extern void MutableProperty__ctor_m175879B971041603D976D702D492D7FBA1B4587A (void);
// 0x00000138 System.Void NPOI.HPSF.MutableProperty::.ctor(NPOI.HPSF.Property)
extern void MutableProperty__ctor_mDDB6B09A9BF02A4A0A16EA9535C2E6D5EC4F5DB5 (void);
// 0x00000139 System.Int32 NPOI.HPSF.MutableProperty::Write(System.IO.Stream,System.Int32)
extern void MutableProperty_Write_m57893FE433E09F117FBE6604F55ECF2CEA9E0A1B (void);
// 0x0000013A System.Void NPOI.HPSF.CustomProperty::.ctor(NPOI.HPSF.Property)
extern void CustomProperty__ctor_mC5B74D769C0D0FDDEE1323C672F0D08A701B999B (void);
// 0x0000013B System.Void NPOI.HPSF.CustomProperty::.ctor(NPOI.HPSF.Property,System.String)
extern void CustomProperty__ctor_mC4CD6454F4CE5C86D3CBE756C7535F4C26537908 (void);
// 0x0000013C System.String NPOI.HPSF.CustomProperty::get_Name()
extern void CustomProperty_get_Name_m6F5484E61B6055A7306032218464242E6FFFF04A (void);
// 0x0000013D System.Int32 NPOI.HPSF.CustomProperty::GetHashCode()
extern void CustomProperty_GetHashCode_mA5CCB16A3E915A2D4B4F74A6C0F9BFECDB1D762B (void);
// 0x0000013E System.Int32 NPOI.HPSF.PropertySet::get_ByteOrder()
extern void PropertySet_get_ByteOrder_m7FEFBC8085D76A6A6B62A2522717470DEF6D44C4 (void);
// 0x0000013F System.Int32 NPOI.HPSF.PropertySet::get_Format()
extern void PropertySet_get_Format_m0CC74C1598679E74BAE1690682EB1DBAD49D05C3 (void);
// 0x00000140 System.Int32 NPOI.HPSF.PropertySet::get_OSVersion()
extern void PropertySet_get_OSVersion_m0E5965049F570384B4AB0CAADA989EF42D2ED78C (void);
// 0x00000141 NPOI.Util.ClassID NPOI.HPSF.PropertySet::get_ClassID()
extern void PropertySet_get_ClassID_m6CA27561056D19AF99738DCD4D66C3A6B36F956F (void);
// 0x00000142 System.Void NPOI.HPSF.PropertySet::set_ClassID(NPOI.Util.ClassID)
extern void PropertySet_set_ClassID_mAB24F894C77127864B993835FE311C6AB6DBA74A (void);
// 0x00000143 System.Int32 NPOI.HPSF.PropertySet::get_SectionCount()
extern void PropertySet_get_SectionCount_mB85DA5473696ADBD99ACCC0B5F5FB50CD2668F77 (void);
// 0x00000144 System.Collections.Generic.List`1<NPOI.HPSF.Section> NPOI.HPSF.PropertySet::get_Sections()
extern void PropertySet_get_Sections_m2E2D89C4A26A61256019773FFD633D6535FB8AE1 (void);
// 0x00000145 System.Void NPOI.HPSF.PropertySet::.ctor()
extern void PropertySet__ctor_m051F8ABB53B1B8B4A1B687FB1B663BDA3823CD37 (void);
// 0x00000146 System.Void NPOI.HPSF.PropertySet::.ctor(System.IO.Stream)
extern void PropertySet__ctor_mF55D3F3CF0D2DBC2999D7A62BB4B33B358AFFD0F (void);
// 0x00000147 System.Boolean NPOI.HPSF.PropertySet::IsPropertySetStream(System.IO.Stream)
extern void PropertySet_IsPropertySetStream_m3E5FDF8597504E6E98A45F91DFB35CD36880FE4D (void);
// 0x00000148 System.Boolean NPOI.HPSF.PropertySet::IsPropertySetStream(System.Byte[],System.Int32,System.Int32)
extern void PropertySet_IsPropertySetStream_m349DC2D18910E2726D603E78270569EAB39A5620 (void);
// 0x00000149 System.Void NPOI.HPSF.PropertySet::init(System.Byte[],System.Int32,System.Int32)
extern void PropertySet_init_mD1EB354F0FF6290632C04D71D1563E514C2E5230 (void);
// 0x0000014A System.Boolean NPOI.HPSF.PropertySet::get_IsSummaryInformation()
extern void PropertySet_get_IsSummaryInformation_mD2870D07516CA4569EECC031356262104B0AC110 (void);
// 0x0000014B System.Boolean NPOI.HPSF.PropertySet::get_IsDocumentSummaryInformation()
extern void PropertySet_get_IsDocumentSummaryInformation_m8C40EE6442B59D27C64D689E5C0251B064CA29FD (void);
// 0x0000014C NPOI.HPSF.Section NPOI.HPSF.PropertySet::get_FirstSection()
extern void PropertySet_get_FirstSection_m1F23B2E815821C0E1E0B550FC86187D1B31681D6 (void);
// 0x0000014D System.Boolean NPOI.HPSF.PropertySet::Equals(System.Object)
extern void PropertySet_Equals_m4777DCAEF4587D0B4E3A5D0B37C684805E78F6AC (void);
// 0x0000014E System.Int32 NPOI.HPSF.PropertySet::GetHashCode()
extern void PropertySet_GetHashCode_m819E5EFA9EBB95AC2773C3FED4E6DE42E5049952 (void);
// 0x0000014F System.String NPOI.HPSF.PropertySet::ToString()
extern void PropertySet_ToString_m2E73FE35E9960B00B960AB4D3CC39BD3DDCD68D1 (void);
// 0x00000150 System.Void NPOI.HPSF.PropertySet::.cctor()
extern void PropertySet__cctor_m8D3B96C170FC2F17C0FA39CD1E88E0D648ABE8B4 (void);
// 0x00000151 System.Void NPOI.HPSF.MutablePropertySet::.ctor()
extern void MutablePropertySet__ctor_mA582321633DF273A8670D994308486DB4C00614F (void);
// 0x00000152 System.Void NPOI.HPSF.MutablePropertySet::.ctor(NPOI.HPSF.PropertySet)
extern void MutablePropertySet__ctor_m73E15CC350962489AE38ACB92A0F28A2D956D301 (void);
// 0x00000153 System.Int32 NPOI.HPSF.MutablePropertySet::get_ByteOrder()
extern void MutablePropertySet_get_ByteOrder_m605C3E7BD7E963FB913A14AA571C7D9EDDA82C2D (void);
// 0x00000154 System.Int32 NPOI.HPSF.MutablePropertySet::get_Format()
extern void MutablePropertySet_get_Format_m87C4AD396891CC1388C16A8FC184576C76F283D2 (void);
// 0x00000155 System.Int32 NPOI.HPSF.MutablePropertySet::get_OSVersion()
extern void MutablePropertySet_get_OSVersion_m74328B369B84A0A11F873A12DFED29F1D2116B26 (void);
// 0x00000156 System.Void NPOI.HPSF.MutablePropertySet::set_ClassID(NPOI.Util.ClassID)
extern void MutablePropertySet_set_ClassID_m3B250CDE89658D8C3569A44C6932E11A61B65F63 (void);
// 0x00000157 NPOI.Util.ClassID NPOI.HPSF.MutablePropertySet::get_ClassID()
extern void MutablePropertySet_get_ClassID_m2C69D6D362503A6878C52EFBB707EA28673F9675 (void);
// 0x00000158 System.Void NPOI.HPSF.MutablePropertySet::ClearSections()
extern void MutablePropertySet_ClearSections_mD017B46854E9F35FD08CAC12113F99D3F0290FCD (void);
// 0x00000159 System.Void NPOI.HPSF.MutablePropertySet::AddSection(NPOI.HPSF.Section)
extern void MutablePropertySet_AddSection_m027B793884E1E7060CBA36A64FED8B8664B754BB (void);
// 0x0000015A System.Void NPOI.HPSF.MutablePropertySet::Write(System.IO.Stream)
extern void MutablePropertySet_Write_m87775F7C91E90A88AB4AC437A97E7125DEE1C2AF (void);
// 0x0000015B System.Void NPOI.HPSF.SpecialPropertySet::.ctor(NPOI.HPSF.PropertySet)
extern void SpecialPropertySet__ctor_m108A78B355EC4227BAB38B7A9907397AA06957F6 (void);
// 0x0000015C System.Int32 NPOI.HPSF.SpecialPropertySet::get_ByteOrder()
extern void SpecialPropertySet_get_ByteOrder_mE41882839A68F5263791AAB8ACAFB8BA9AAB98EA (void);
// 0x0000015D System.Int32 NPOI.HPSF.SpecialPropertySet::get_Format()
extern void SpecialPropertySet_get_Format_m8E68D2FF4D56558AC87F72B7BC82E5BA155EE46B (void);
// 0x0000015E NPOI.Util.ClassID NPOI.HPSF.SpecialPropertySet::get_ClassID()
extern void SpecialPropertySet_get_ClassID_mA6A748188F915679C91DBBBBD3558CA996470A96 (void);
// 0x0000015F System.Void NPOI.HPSF.SpecialPropertySet::set_ClassID(NPOI.Util.ClassID)
extern void SpecialPropertySet_set_ClassID_m2E09FDDCB97ACF15F41D8430B4AD94AFA82DEF6B (void);
// 0x00000160 System.Int32 NPOI.HPSF.SpecialPropertySet::get_SectionCount()
extern void SpecialPropertySet_get_SectionCount_mAFC24A8D159BCB9A894E34A4CC0464FB15F44FF6 (void);
// 0x00000161 System.Collections.Generic.List`1<NPOI.HPSF.Section> NPOI.HPSF.SpecialPropertySet::get_Sections()
extern void SpecialPropertySet_get_Sections_m13ADCFCA6AFADB5E6C4DE4B492FE38271DFBBA55 (void);
// 0x00000162 System.Boolean NPOI.HPSF.SpecialPropertySet::get_IsSummaryInformation()
extern void SpecialPropertySet_get_IsSummaryInformation_m3D4421C4F09632D20154801080FAF4E0E9F9C8F6 (void);
// 0x00000163 System.Boolean NPOI.HPSF.SpecialPropertySet::get_IsDocumentSummaryInformation()
extern void SpecialPropertySet_get_IsDocumentSummaryInformation_m59B47B49F3C874E061D9AC088C647AE8B8474DC5 (void);
// 0x00000164 NPOI.HPSF.Section NPOI.HPSF.SpecialPropertySet::get_FirstSection()
extern void SpecialPropertySet_get_FirstSection_m338D687F09884CDC688DD417D4BEAC17DAF7C2B2 (void);
// 0x00000165 System.Void NPOI.HPSF.SpecialPropertySet::AddSection(NPOI.HPSF.Section)
extern void SpecialPropertySet_AddSection_m7CC80DCA670DDAF296B28861BD531CA18E08382E (void);
// 0x00000166 System.Void NPOI.HPSF.SpecialPropertySet::ClearSections()
extern void SpecialPropertySet_ClearSections_m1A79B2C1CF2FD7032B0AAFE86FDFD59C5971D858 (void);
// 0x00000167 System.Int32 NPOI.HPSF.SpecialPropertySet::get_OSVersion()
extern void SpecialPropertySet_get_OSVersion_m47B7A78D029CF75B790D59787E6F30EE18FE5852 (void);
// 0x00000168 System.Void NPOI.HPSF.SpecialPropertySet::Write(System.IO.Stream)
extern void SpecialPropertySet_Write_m78ECDBDEF2A2F342BFE7D53A334865037488A052 (void);
// 0x00000169 System.Boolean NPOI.HPSF.SpecialPropertySet::Equals(System.Object)
extern void SpecialPropertySet_Equals_m4118ADBDED2E73A531992B4446FC2065E6C46FE0 (void);
// 0x0000016A System.Int32 NPOI.HPSF.SpecialPropertySet::GetHashCode()
extern void SpecialPropertySet_GetHashCode_mA3891CECACC7151C090EA7EBE540EBDF202817B6 (void);
// 0x0000016B System.String NPOI.HPSF.SpecialPropertySet::ToString()
extern void SpecialPropertySet_ToString_mE5D42A2AE038A658A6A1FF3CE94C38D4F767E9E3 (void);
// 0x0000016C System.Void NPOI.HPSF.DocumentSummaryInformation::.ctor(NPOI.HPSF.PropertySet)
extern void DocumentSummaryInformation__ctor_mF728E91657AB8F788958F2CF0D4E6B9A72DC65FB (void);
// 0x0000016D NPOI.HPSF.CustomProperties NPOI.HPSF.DocumentSummaryInformation::get_CustomProperties()
extern void DocumentSummaryInformation_get_CustomProperties_m4DD3B52E09835A24DDEB5E41567D1876E8F41EA4 (void);
// 0x0000016E System.Void NPOI.HPSF.DocumentSummaryInformation::set_CustomProperties(NPOI.HPSF.CustomProperties)
extern void DocumentSummaryInformation_set_CustomProperties_mFAB08CE65590219FEDAEE1FFD5D87DB7A325841E (void);
// 0x0000016F System.Void NPOI.HPSF.DocumentSummaryInformation::EnsureSection2()
extern void DocumentSummaryInformation_EnsureSection2_m8EE456086E39A2FF9F1EA44B39FBBBA6800E3047 (void);
// 0x00000170 System.Void NPOI.POIDocument::.ctor(NPOI.POIFS.FileSystem.DirectoryNode)
extern void POIDocument__ctor_m174D56D1A3FAC2341440A52A67D35AFE2578035B (void);
// 0x00000171 NPOI.HPSF.DocumentSummaryInformation NPOI.POIDocument::get_DocumentSummaryInformation()
extern void POIDocument_get_DocumentSummaryInformation_m96CB5C743A3870FB59727C880ADD6DF55B48553F (void);
// 0x00000172 System.Void NPOI.POIDocument::set_DocumentSummaryInformation(NPOI.HPSF.DocumentSummaryInformation)
extern void POIDocument_set_DocumentSummaryInformation_m6A28E6674569B1E9080D712F5ABF867419B4C632 (void);
// 0x00000173 NPOI.HPSF.SummaryInformation NPOI.POIDocument::get_SummaryInformation()
extern void POIDocument_get_SummaryInformation_m489C696308EEDCDD421FB8E97FA35BF2894149CE (void);
// 0x00000174 System.Void NPOI.POIDocument::set_SummaryInformation(NPOI.HPSF.SummaryInformation)
extern void POIDocument_set_SummaryInformation_mB0A6537493E48B2B33FC6CB6C85533063E0C79B1 (void);
// 0x00000175 System.Void NPOI.POIDocument::ReadProperties()
extern void POIDocument_ReadProperties_m4E3F17AC0E55E5CFB625BE05733C96B3BF8DB85D (void);
// 0x00000176 NPOI.HPSF.PropertySet NPOI.POIDocument::GetPropertySet(System.String)
extern void POIDocument_GetPropertySet_m269B8F797F39B621A5CA2904842F7DF4749B2F04 (void);
// 0x00000177 System.Void NPOI.POIDocument::WriteProperties(NPOI.POIFS.FileSystem.POIFSFileSystem,System.Collections.IList)
extern void POIDocument_WriteProperties_m5B4FB8B721B48BEA9F5FD38F7186092EE385BC32 (void);
// 0x00000178 System.Void NPOI.POIDocument::WritePropertySet(System.String,NPOI.HPSF.PropertySet,NPOI.POIFS.FileSystem.POIFSFileSystem)
extern void POIDocument_WritePropertySet_m54FF0CB543E3DF14BF974C0D8ED848CE44661DE5 (void);
// 0x00000179 System.Void NPOI.POIDocument::Write(System.IO.Stream)
// 0x0000017A System.Void NPOI.HPSF.HPSFException::.ctor()
extern void HPSFException__ctor_mAD2E4E6BF7D4DC0E3A765A14AEC750F49B60A9A1 (void);
// 0x0000017B System.Void NPOI.HPSF.HPSFException::.ctor(System.String)
extern void HPSFException__ctor_mADF9FBB44DF699130D42F1FA1708D260925B4F38 (void);
// 0x0000017C System.Void NPOI.Util.RuntimeException::.ctor()
extern void RuntimeException__ctor_mC1A67654CA09AA9AC0B66FB39251DD332D9554BB (void);
// 0x0000017D System.Void NPOI.Util.RuntimeException::.ctor(System.String)
extern void RuntimeException__ctor_m017A8DCA7CA74FD5269ECC970DE9C978D208D3D3 (void);
// 0x0000017E System.Void NPOI.Util.RuntimeException::.ctor(System.Exception)
extern void RuntimeException__ctor_m6A252DEF610F9E66328331E4338BEA31593317FA (void);
// 0x0000017F System.Void NPOI.Util.RuntimeException::.ctor(System.String,System.Exception)
extern void RuntimeException__ctor_mF4B80298200C5B587887FFC4723455C5DF886C76 (void);
// 0x00000180 System.Void NPOI.HPSF.HPSFRuntimeException::.ctor()
extern void HPSFRuntimeException__ctor_mCACA3439E8E1B0542AE07153586DC865E8836530 (void);
// 0x00000181 System.Void NPOI.HPSF.HPSFRuntimeException::.ctor(System.String)
extern void HPSFRuntimeException__ctor_m0A83BAA8D828753F054658A900F69CB36EF8F6C9 (void);
// 0x00000182 System.Void NPOI.HPSF.HPSFRuntimeException::.ctor(System.Exception)
extern void HPSFRuntimeException__ctor_m24319AE236E204FC886B2DF25328E42D4497F55F (void);
// 0x00000183 System.Void NPOI.HPSF.IllegalPropertySetDataException::.ctor()
extern void IllegalPropertySetDataException__ctor_mBE57FA14D3442B47A68BEEBCA44814E8F9E81BD8 (void);
// 0x00000184 System.Void NPOI.HPSF.IllegalPropertySetDataException::.ctor(System.String)
extern void IllegalPropertySetDataException__ctor_m15180A5F6A75E3493CBFF2DDB4AF5FD95C3086D3 (void);
// 0x00000185 System.Void NPOI.HPSF.IllegalPropertySetDataException::.ctor(System.Exception)
extern void IllegalPropertySetDataException__ctor_m0F24F41C8D150D6578AE8AEA6AF02A110CC79372 (void);
// 0x00000186 System.Void NPOI.HPSF.VariantTypeException::.ctor(System.Int64,System.Object,System.String)
extern void VariantTypeException__ctor_m27FAD44BF410B94A081971E1D54A685180F06CE0 (void);
// 0x00000187 System.Int64 NPOI.HPSF.VariantTypeException::get_VariantType()
extern void VariantTypeException_get_VariantType_m6BD6D387D0ACDFE4B3BAABC10551D56D374380FC (void);
// 0x00000188 System.Object NPOI.HPSF.VariantTypeException::get_Value()
extern void VariantTypeException_get_Value_m626860CD9849BDBA6161E69674F4C7C6D90F5888 (void);
// 0x00000189 System.Void NPOI.HPSF.MarkUnsupportedException::.ctor()
extern void MarkUnsupportedException__ctor_m4336781DD4D6CEB1EF355AD112AA1B1EB51BCFE5 (void);
// 0x0000018A System.Void NPOI.HPSF.MarkUnsupportedException::.ctor(System.String)
extern void MarkUnsupportedException__ctor_m657825340776F7EA98C43FF59981D834F3BAE38C (void);
// 0x0000018B System.Void NPOI.HPSF.MissingSectionException::.ctor()
extern void MissingSectionException__ctor_m199181D8EE1F66794A0036FF3DCE44C47E485964 (void);
// 0x0000018C System.Void NPOI.HPSF.MissingSectionException::.ctor(System.String)
extern void MissingSectionException__ctor_m11F813845A12DB414172BC9DE39DEE2D37A23800 (void);
// 0x0000018D NPOI.Util.ClassID NPOI.HPSF.Section::get_FormatID()
extern void Section_get_FormatID_mDE17CB4EA309F8976FDF481C1AA3FED746ACDFFD (void);
// 0x0000018E System.Int64 NPOI.HPSF.Section::get_OffSet()
extern void Section_get_OffSet_m370D8A687080A68BAB2B8114ADCE48386D25906C (void);
// 0x0000018F System.Int32 NPOI.HPSF.Section::get_Size()
extern void Section_get_Size_mA824AF372B635DDCDB94A3F9958F9F3DB9B984D5 (void);
// 0x00000190 System.Int32 NPOI.HPSF.Section::get_PropertyCount()
extern void Section_get_PropertyCount_m3A7448DC12A350A0C8CCD397A3E99A282977FA98 (void);
// 0x00000191 NPOI.HPSF.Property[] NPOI.HPSF.Section::get_Properties()
extern void Section_get_Properties_m8BFA36160F7F12A917E98FB0D947B879CA42FC57 (void);
// 0x00000192 System.Void NPOI.HPSF.Section::.ctor()
extern void Section__ctor_m6D3919E5748F527B7460F6E9444FA1CDC2D06134 (void);
// 0x00000193 System.Void NPOI.HPSF.Section::.ctor(System.Byte[],System.Int32)
extern void Section__ctor_m2788472DFA395D3D2DE365EDD0F12AE404836D0E (void);
// 0x00000194 System.Object NPOI.HPSF.Section::GetProperty(System.Int64)
extern void Section_GetProperty_mD09A0A104CC0B692CA797363AEB503D892734F97 (void);
// 0x00000195 System.Boolean NPOI.HPSF.Section::Equals(System.Object)
extern void Section_Equals_m9B2CD11FC74FF866F6969A6B544DE81A7F722486 (void);
// 0x00000196 NPOI.HPSF.Property[] NPOI.HPSF.Section::Remove(NPOI.HPSF.Property[],System.Int32)
extern void Section_Remove_mE3FB96B3D8A32EEB65450EAFA8241736F0FECE87 (void);
// 0x00000197 System.Int32 NPOI.HPSF.Section::GetHashCode()
extern void Section_GetHashCode_m7851C51892A3371B05AB2ABF24CD6A943492A5E6 (void);
// 0x00000198 System.String NPOI.HPSF.Section::ToString()
extern void Section_ToString_m9ED199998FEBCCBA69765331719D9135DFD0C4B6 (void);
// 0x00000199 System.Collections.IDictionary NPOI.HPSF.Section::get_Dictionary()
extern void Section_get_Dictionary_mEC0F2F61D31B5FE9340AE7CD949435E77320C8EF (void);
// 0x0000019A System.Void NPOI.HPSF.Section::set_Dictionary(System.Collections.IDictionary)
extern void Section_set_Dictionary_m885165EDE9D281B396D76CA6EC17738540E49355 (void);
// 0x0000019B System.Int32 NPOI.HPSF.Section::get_Codepage()
extern void Section_get_Codepage_m37B5B0279A4CD9FB5C9C6D54DF33E4B5F0972ED4 (void);
// 0x0000019C System.Int32 NPOI.HPSF.Section_PropertyListEntry::CompareTo(System.Object)
extern void PropertyListEntry_CompareTo_m0AACE0F11EA910BCCEBD444DF1375A7CA4BC79A2 (void);
// 0x0000019D System.String NPOI.HPSF.Section_PropertyListEntry::ToString()
extern void PropertyListEntry_ToString_m6E5728904F2F5997A9752CCAB80527183D9F5997 (void);
// 0x0000019E System.Void NPOI.HPSF.Section_PropertyListEntry::.ctor()
extern void PropertyListEntry__ctor_m9658BB7B4DF5FD2AC5E2C4468A0FC3E5B2787DEF (void);
// 0x0000019F System.Void NPOI.HPSF.MutableSection::.ctor()
extern void MutableSection__ctor_mCDCF5F719AC63E2F6DE5341C7557F737358FD078 (void);
// 0x000001A0 System.Void NPOI.HPSF.MutableSection::.ctor(NPOI.HPSF.Section)
extern void MutableSection__ctor_mB2A15FF4ECE116C67B5ED365245FD7AC6D172CA6 (void);
// 0x000001A1 System.Void NPOI.HPSF.MutableSection::SetFormatID(NPOI.Util.ClassID)
extern void MutableSection_SetFormatID_mFB33131AAD90A64806096A950765A2E43453EE21 (void);
// 0x000001A2 System.Void NPOI.HPSF.MutableSection::SetFormatID(System.Byte[])
extern void MutableSection_SetFormatID_mB2B389139E34E310AE24DEAEEF4C3E2860C69F11 (void);
// 0x000001A3 System.Void NPOI.HPSF.MutableSection::SetProperties(NPOI.HPSF.Property[])
extern void MutableSection_SetProperties_mB0D9EC3402913E6AEB0C4784F2F571E46339793A (void);
// 0x000001A4 System.Void NPOI.HPSF.MutableSection::SetProperty(System.Int32,System.String)
extern void MutableSection_SetProperty_mC3A61578912F3A644329AE9DA8CD849F2BC95AB4 (void);
// 0x000001A5 System.Void NPOI.HPSF.MutableSection::SetProperty(System.Int32,System.Int64,System.Object)
extern void MutableSection_SetProperty_m55F0C7F6435DB400812000FB434301515BD18F94 (void);
// 0x000001A6 System.Void NPOI.HPSF.MutableSection::SetProperty(NPOI.HPSF.Property)
extern void MutableSection_SetProperty_mDD09B81EB1D3979375497BB313CDF16BEBD14118 (void);
// 0x000001A7 System.Void NPOI.HPSF.MutableSection::RemoveProperty(System.Int64)
extern void MutableSection_RemoveProperty_m9F4338E44684CBC04AFB852DBF86ACAED6529208 (void);
// 0x000001A8 System.Int32 NPOI.HPSF.MutableSection::get_Size()
extern void MutableSection_get_Size_mB83BD12D9F683CC41DD0D99BF829B2B7EBEB8B8B (void);
// 0x000001A9 System.Int32 NPOI.HPSF.MutableSection::CalcSize()
extern void MutableSection_CalcSize_m5957F5EBDE1989CA3B0DD9EBA2BDE1379812520D (void);
// 0x000001AA System.Int32 NPOI.HPSF.MutableSection::Write(System.IO.Stream)
extern void MutableSection_Write_m2825F40F95085EDCC557AC41CE37A2C408F1EE77 (void);
// 0x000001AB System.Int32 NPOI.HPSF.MutableSection::WriteDictionary(System.IO.Stream,System.Collections.IDictionary,System.Int32)
extern void MutableSection_WriteDictionary_m2A045AE0229B2DD313ECAF3127CE34B7C6AC639F (void);
// 0x000001AC System.Int32 NPOI.HPSF.MutableSection::get_PropertyCount()
extern void MutableSection_get_PropertyCount_mC0FDD37DFFD5B08E5C9D6B21F4FC5FB50189DF25 (void);
// 0x000001AD NPOI.HPSF.Property[] NPOI.HPSF.MutableSection::get_Properties()
extern void MutableSection_get_Properties_m73AD8CBB87887F88C24580FD481AB8AE6B6ED18A (void);
// 0x000001AE System.Void NPOI.HPSF.MutableSection::EnsureProperties()
extern void MutableSection_EnsureProperties_mEC0E5F516EB275DA394C59462B2BBFA65522C213 (void);
// 0x000001AF System.Object NPOI.HPSF.MutableSection::GetProperty(System.Int64)
extern void MutableSection_GetProperty_m126FFE0E5B8881CDB4D6C4D91EC478EF45DCD9E8 (void);
// 0x000001B0 System.Collections.IDictionary NPOI.HPSF.MutableSection::get_Dictionary()
extern void MutableSection_get_Dictionary_m6AB6666D36EB1DCFACD88B9C0F9E170D80A11AC1 (void);
// 0x000001B1 System.Void NPOI.HPSF.MutableSection::set_Dictionary(System.Collections.IDictionary)
extern void MutableSection_set_Dictionary_m351C67BC193160AB76B0D7AE1DD21EFB61264B06 (void);
// 0x000001B2 System.Void NPOI.HPSF.MutableSection::Clear()
extern void MutableSection_Clear_m276E94339FD2E65A879F6E8FD0DC44E4D376176E (void);
// 0x000001B3 System.Int32 NPOI.HPSF.MutableSection::get_Codepage()
extern void MutableSection_get_Codepage_mCD22A0BDA946AB0A7160D82951CF8CAD980B6224 (void);
// 0x000001B4 System.Void NPOI.HPSF.MutableSection::set_Codepage(System.Int32)
extern void MutableSection_set_Codepage_m2B51B928B49616F7434E9AE8202F99A198F48533 (void);
// 0x000001B5 System.Int32 NPOI.HPSF.MutableSection_PropertyComparer::System.Collections.IComparer.Compare(System.Object,System.Object)
extern void PropertyComparer_System_Collections_IComparer_Compare_m1064CBB1330A87E71CCEF8432D911ADE75345FD9 (void);
// 0x000001B6 System.Void NPOI.HPSF.MutableSection_PropertyComparer::.ctor()
extern void PropertyComparer__ctor_m70778C1A661DB42F23573A77D72560780B944D32 (void);
// 0x000001B7 System.Void NPOI.HPSF.NoFormatIDException::.ctor()
extern void NoFormatIDException__ctor_mC50F1E0D99F5585DF8DFE874F7C3F32D557261A2 (void);
// 0x000001B8 System.Void NPOI.HPSF.NoPropertySetStreamException::.ctor()
extern void NoPropertySetStreamException__ctor_m981FBC9655C2731C2BE8C133E30988F044229237 (void);
// 0x000001B9 System.Void NPOI.HPSF.NoPropertySetStreamException::.ctor(System.String)
extern void NoPropertySetStreamException__ctor_mF8057FBDAD7EC4526D87DE62FDA97DC94F621097 (void);
// 0x000001BA NPOI.HPSF.PropertySet NPOI.HPSF.PropertySetFactory::Create(System.IO.Stream)
extern void PropertySetFactory_Create_mD154B17C0F6D6EBE637BFF8C42849695F61EB1A7 (void);
// 0x000001BB NPOI.HPSF.SummaryInformation NPOI.HPSF.PropertySetFactory::CreateSummaryInformation()
extern void PropertySetFactory_CreateSummaryInformation_mDA74CD70DFB40E68D98C7FFF20310351632CFAC1 (void);
// 0x000001BC NPOI.HPSF.DocumentSummaryInformation NPOI.HPSF.PropertySetFactory::CreateDocumentSummaryInformation()
extern void PropertySetFactory_CreateDocumentSummaryInformation_m5F364E1DBC343E39C97F93BF30558F431FFBE8C7 (void);
// 0x000001BD System.Void NPOI.HPSF.UnsupportedVariantTypeException::.ctor(System.Int64,System.Object)
extern void UnsupportedVariantTypeException__ctor_mB9EA81D63C1C304D9980F0E22097C4C210CF2EC6 (void);
// 0x000001BE System.Void NPOI.HPSF.ReadingNotSupportedException::.ctor(System.Int64,System.Object)
extern void ReadingNotSupportedException__ctor_m60FE7CFBE7A77FE657687F54DA0074C0ED4E6880 (void);
// 0x000001BF System.Void NPOI.HPSF.SummaryInformation::.ctor(NPOI.HPSF.PropertySet)
extern void SummaryInformation__ctor_m72FEB06864BF68DEF283631148DF990B611A2748 (void);
// 0x000001C0 System.Void NPOI.HPSF.SummaryInformation::set_ApplicationName(System.String)
extern void SummaryInformation_set_ApplicationName_m5006BF847A8AAB81B2A3F0B3C922638E1C332553 (void);
// 0x000001C1 System.Int32 NPOI.HPSF.TypeWriter::WriteToStream(System.IO.Stream,System.Int16)
extern void TypeWriter_WriteToStream_m4C3E50B38616222CD32E1DBCA1C4B88C898DFC75 (void);
// 0x000001C2 System.Int32 NPOI.HPSF.TypeWriter::WriteToStream(System.IO.Stream,System.Int32)
extern void TypeWriter_WriteToStream_m1A43C8EED310D190736D21751A2CDE4CE47A1E33 (void);
// 0x000001C3 System.Int32 NPOI.HPSF.TypeWriter::WriteToStream(System.IO.Stream,System.Int64)
extern void TypeWriter_WriteToStream_mBF65BD1E72B8A871248AF03C4702304AB3AC26F1 (void);
// 0x000001C4 System.Int32 NPOI.HPSF.TypeWriter::WriteUIntToStream(System.IO.Stream,System.UInt32)
extern void TypeWriter_WriteUIntToStream_mBBA17D23B19E65E0358BA632350433DE967FDAE0 (void);
// 0x000001C5 System.Int32 NPOI.HPSF.TypeWriter::WriteToStream(System.IO.Stream,NPOI.Util.ClassID)
extern void TypeWriter_WriteToStream_mEE2F995A405A8F8B8448C076A87C61DAB7BE9478 (void);
// 0x000001C6 System.Int32 NPOI.HPSF.TypeWriter::WriteToStream(System.IO.Stream,System.Double)
extern void TypeWriter_WriteToStream_mBE855396535327AEE1C9C10DD0DF844260EC915D (void);
// 0x000001C7 System.Void NPOI.HPSF.UnexpectedPropertySetTypeException::.ctor()
extern void UnexpectedPropertySetTypeException__ctor_m67735FE30ABBA602E6FD0B07C611EF1904D4878E (void);
// 0x000001C8 System.Void NPOI.HPSF.UnexpectedPropertySetTypeException::.ctor(System.String)
extern void UnexpectedPropertySetTypeException__ctor_m451CD805492095D06941EEBB8A4266D39B83CBA5 (void);
// 0x000001C9 System.DateTime NPOI.HPSF.Util::FiletimeToDate(System.Int32,System.Int32)
extern void Util_FiletimeToDate_mC5AF04D973C110D52BD43166EF09BFBAD692FDD3 (void);
// 0x000001CA System.DateTime NPOI.HPSF.Util::FiletimeToDate(System.Int64)
extern void Util_FiletimeToDate_m287FD9C1209627B8E99CFE751D692722B5C8EDA8 (void);
// 0x000001CB System.Int64 NPOI.HPSF.Util::DateToFileTime(System.DateTime)
extern void Util_DateToFileTime_m65DE6C5110E169BB53E54EAEE03DC0EA075D6683 (void);
// 0x000001CC System.Boolean NPOI.HPSF.Util::AreEqual(System.Collections.IList,System.Collections.IList)
extern void Util_AreEqual_mEE1805DCBAED01091D12FB6ECEE81279D6AD517A (void);
// 0x000001CD System.Boolean NPOI.HPSF.Util::internalEquals(System.Collections.IList,System.Collections.IList)
extern void Util_internalEquals_m574FDDA5F2DF61F32A47ADC267EE770D91749DDA (void);
// 0x000001CE System.Byte[] NPOI.HPSF.Util::Pad4(System.Byte[])
extern void Util_Pad4_mF85793D6A0378A0D8AFBB5C33EA15BC5275563F4 (void);
// 0x000001CF System.Void NPOI.HPSF.Util::.cctor()
extern void Util__cctor_mC304A0D091B37E87A79F84161E5362D929D646F3 (void);
// 0x000001D0 System.Void NPOI.HPSF.Variant::.cctor()
extern void Variant__cctor_m67C4D1DAD6D3FCE482CEC662C34E29C966D2FF73 (void);
// 0x000001D1 System.String NPOI.HPSF.Variant::GetVariantName(System.Int64)
extern void Variant_GetVariantName_mEBAA0EA8B1B87C193BBBC152453161EE029C2E68 (void);
// 0x000001D2 System.Boolean NPOI.HPSF.VariantSupport::get_IsLogUnsupportedTypes()
extern void VariantSupport_get_IsLogUnsupportedTypes_m4BEB68F828AC672CD59F3B1F23251AF4DAC82563 (void);
// 0x000001D3 System.Void NPOI.HPSF.VariantSupport::WriteUnsupportedTypeMessage(NPOI.HPSF.UnsupportedVariantTypeException)
extern void VariantSupport_WriteUnsupportedTypeMessage_m74E1820CE7C7A44501570B166C2912D89FF75BFE (void);
// 0x000001D4 System.Object NPOI.HPSF.VariantSupport::Read(System.Byte[],System.Int32,System.Int32,System.Int64,System.Int32)
extern void VariantSupport_Read_m3A3C4ED81D3C2A33A68ED962387D3DE54C1575F3 (void);
// 0x000001D5 System.Int32 NPOI.HPSF.VariantSupport::Write(System.IO.Stream,System.Int64,System.Object,System.Int32)
extern void VariantSupport_Write_m3B154078C66A81327D26F89BE57041BDA6CDDDD0 (void);
// 0x000001D6 System.Void NPOI.HPSF.VariantSupport::.cctor()
extern void VariantSupport__cctor_mAF18827372D9C404F798215BA172EFEA05120FBB (void);
// 0x000001D7 System.Void NPOI.HPSF.Wellknown.SectionIDMap::.cctor()
extern void SectionIDMap__cctor_m3D12E5EF35621E7E780D6D6026F0B84C84AD321A (void);
// 0x000001D8 System.Void NPOI.HPSF.WritingNotSupportedException::.ctor(System.Int64,System.Object)
extern void WritingNotSupportedException__ctor_m170EF0B205D82B436D9C17DFBED50243640E3485 (void);
// 0x000001D9 System.Int32 NPOI.SS.UserModel.IWorkbook::GetSheetIndex(NPOI.SS.UserModel.ISheet)
// 0x000001DA NPOI.SS.UserModel.ISheet NPOI.SS.UserModel.IWorkbook::GetSheet(System.String)
// 0x000001DB System.Void NPOI.SS.UserModel.IWorkbook::Write(System.IO.Stream)
// 0x000001DC NPOI.SS.UserModel.MissingCellPolicy NPOI.SS.UserModel.IWorkbook::get_MissingCellPolicy()
// 0x000001DD System.Void NPOI.HSSF.UserModel.HSSFWorkbook::.ctor()
extern void HSSFWorkbook__ctor_mBEBA83322F999D7662C6B763CF6109271DCE97DE (void);
// 0x000001DE System.Void NPOI.HSSF.UserModel.HSSFWorkbook::.ctor(NPOI.HSSF.Model.InternalWorkbook)
extern void HSSFWorkbook__ctor_m48F58A7B9D403375FCD149718D7F1ACA1398EC3E (void);
// 0x000001DF System.Void NPOI.HSSF.UserModel.HSSFWorkbook::.ctor(NPOI.POIFS.FileSystem.POIFSFileSystem,System.Boolean)
extern void HSSFWorkbook__ctor_mE25CEF368A75748FF1D75DA2E6C09F263D15F3A7 (void);
// 0x000001E0 System.String NPOI.HSSF.UserModel.HSSFWorkbook::GetWorkbookDirEntryName(NPOI.POIFS.FileSystem.DirectoryNode)
extern void HSSFWorkbook_GetWorkbookDirEntryName_m11F0872A9A89D8FF41606E8C34AEDF34E95A4613 (void);
// 0x000001E1 System.Void NPOI.HSSF.UserModel.HSSFWorkbook::.ctor(NPOI.POIFS.FileSystem.DirectoryNode,NPOI.POIFS.FileSystem.POIFSFileSystem,System.Boolean)
extern void HSSFWorkbook__ctor_mCF632E6FF65D9B711F86A4CBDBBBAA6ABF41E6DC (void);
// 0x000001E2 System.Void NPOI.HSSF.UserModel.HSSFWorkbook::.ctor(NPOI.POIFS.FileSystem.DirectoryNode,System.Boolean)
extern void HSSFWorkbook__ctor_m167A74167EAE2886C33194EFFFD6DB85AD512598 (void);
// 0x000001E3 System.Void NPOI.HSSF.UserModel.HSSFWorkbook::.ctor(System.IO.Stream)
extern void HSSFWorkbook__ctor_m489DE323AEA1F41A8F243C7D9477B599F37E9FD2 (void);
// 0x000001E4 System.Void NPOI.HSSF.UserModel.HSSFWorkbook::.ctor(System.IO.Stream,System.Boolean)
extern void HSSFWorkbook__ctor_mBFF5A532DBF7F5F6364DBBEDDB118D33DF584887 (void);
// 0x000001E5 System.Void NPOI.HSSF.UserModel.HSSFWorkbook::SetPropertiesFromWorkbook(NPOI.HSSF.Model.InternalWorkbook)
extern void HSSFWorkbook_SetPropertiesFromWorkbook_m8BD0479F9D2BDBEE01E7219F77F48822A7BD69E1 (void);
// 0x000001E6 System.Void NPOI.HSSF.UserModel.HSSFWorkbook::ConvertLabelRecords(System.Collections.IList,System.Int32)
extern void HSSFWorkbook_ConvertLabelRecords_m79D74DA382BA643DA4586964A7E2B9BD98B249DB (void);
// 0x000001E7 NPOI.SS.UserModel.MissingCellPolicy NPOI.HSSF.UserModel.HSSFWorkbook::get_MissingCellPolicy()
extern void HSSFWorkbook_get_MissingCellPolicy_mA7B5ECD76A7870F3EDD1F6E58F47D1D0B990D69A (void);
// 0x000001E8 System.Int32 NPOI.HSSF.UserModel.HSSFWorkbook::GetSheetIndex(System.String)
extern void HSSFWorkbook_GetSheetIndex_m518B888995E54A5D6A06C497743966A1309C4DAB (void);
// 0x000001E9 System.Int32 NPOI.HSSF.UserModel.HSSFWorkbook::GetSheetIndex(NPOI.SS.UserModel.ISheet)
extern void HSSFWorkbook_GetSheetIndex_m84618793ECE9540F1ADC893F59F7F88AD167E3A3 (void);
// 0x000001EA System.Collections.Generic.List`1<NPOI.HSSF.UserModel.HSSFSheet> NPOI.HSSF.UserModel.HSSFWorkbook::GetSheets()
extern void HSSFWorkbook_GetSheets_m1066778BEC6716A0E5F4DEF17EFBAABF0630EC32 (void);
// 0x000001EB NPOI.SS.UserModel.ISheet NPOI.HSSF.UserModel.HSSFWorkbook::GetSheet(System.String)
extern void HSSFWorkbook_GetSheet_mEA1380A7755A929519B0C2FAAC5C14FF2CBD31D0 (void);
// 0x000001EC System.Void NPOI.HSSF.UserModel.HSSFWorkbook::Write(System.IO.Stream)
extern void HSSFWorkbook_Write_m93F974AF13AD14EDB17278C378DAAE6A8DABB08E (void);
// 0x000001ED System.Byte[] NPOI.HSSF.UserModel.HSSFWorkbook::GetBytes()
extern void HSSFWorkbook_GetBytes_m4BE08C298BF64168C03C3D406283F113498AD4F6 (void);
// 0x000001EE NPOI.SS.Formula.Udf.UDFFinder NPOI.HSSF.UserModel.HSSFWorkbook::GetUDFFinder()
extern void HSSFWorkbook_GetUDFFinder_mEC2904FBAD32482E5441DBC7C6BC0B60A9C39B99 (void);
// 0x000001EF NPOI.HSSF.Model.InternalWorkbook NPOI.HSSF.UserModel.HSSFWorkbook::get_Workbook()
extern void HSSFWorkbook_get_Workbook_m8B74C6B603C81DE7617F63113C705CC2470ABC5E (void);
// 0x000001F0 System.Void NPOI.HSSF.UserModel.HSSFWorkbook::.cctor()
extern void HSSFWorkbook__cctor_m5167972BB2433C52C816F0CF8E8117E21746F367 (void);
// 0x000001F1 System.Void NPOI.HSSF.UserModel.HSSFWorkbook_SheetRecordCollector::.ctor()
extern void SheetRecordCollector__ctor_mD3D41EBCE91BD08AABAEAEEFEBDCCD983DFB1148 (void);
// 0x000001F2 System.Int32 NPOI.HSSF.UserModel.HSSFWorkbook_SheetRecordCollector::get_TotalSize()
extern void SheetRecordCollector_get_TotalSize_mAAFE0DA09DF4297877C0C258E9A22D00584EC49C (void);
// 0x000001F3 System.Void NPOI.HSSF.UserModel.HSSFWorkbook_SheetRecordCollector::VisitRecord(NPOI.HSSF.Record.Record)
extern void SheetRecordCollector_VisitRecord_mEC9973C8BE7F8A3CE95546B223C4132089468F74 (void);
// 0x000001F4 System.Int32 NPOI.HSSF.UserModel.HSSFWorkbook_SheetRecordCollector::Serialize(System.Int32,System.Byte[])
extern void SheetRecordCollector_Serialize_m240773C8CAECDA24D0157FD3044EC64A9A516E62 (void);
// 0x000001F5 System.Void NPOI.HSSF.UserModel.HSSFWorkbook_SheetRecordCollector::Dispose()
extern void SheetRecordCollector_Dispose_mCF0551B87B4A181777C3AA10DD9C8C61F24BF79B (void);
// 0x000001F6 System.String NPOI.HSSF.Model.HSSFFormulaParser::ToFormulaString(NPOI.HSSF.UserModel.HSSFWorkbook,NPOI.SS.Formula.PTG.Ptg[])
extern void HSSFFormulaParser_ToFormulaString_m6EDD35A01845772B47F6F6152A957AD07D84277B (void);
// 0x000001F7 NPOI.HSSF.Record.ExternSheetRecord NPOI.HSSF.Model.LinkTable::ReadExtSheetRecord(NPOI.HSSF.Model.RecordStream)
extern void LinkTable_ReadExtSheetRecord_m49480F55436F61B937F28C454CE03E39DB641187 (void);
// 0x000001F8 System.Void NPOI.HSSF.Model.LinkTable::.ctor(System.Collections.Generic.List`1<NPOI.HSSF.Record.Record>,System.Int32,NPOI.HSSF.Model.WorkbookRecordList,System.Collections.Generic.Dictionary`2<System.String,NPOI.HSSF.Record.NameCommentRecord>)
extern void LinkTable__ctor_m466E52ECA84DF92BA634B803FF0F49D47F88CF11 (void);
// 0x000001F9 System.Void NPOI.HSSF.Model.LinkTable::.ctor(System.Int32,NPOI.HSSF.Model.WorkbookRecordList)
extern void LinkTable__ctor_m2566BA77B671BABAD44BB0A75C01512F0DA819F2 (void);
// 0x000001FA System.Int32 NPOI.HSSF.Model.LinkTable::get_RecordCount()
extern void LinkTable_get_RecordCount_m40E3F59714C394DC0C5EDE93F0B9E7C832CFDDC8 (void);
// 0x000001FB System.Int32 NPOI.HSSF.Model.LinkTable::GetIndexToInternalSheet(System.Int32)
extern void LinkTable_GetIndexToInternalSheet_m74A8466E6179845A8A375EEFE7C029BF49CB0089 (void);
// 0x000001FC System.Int32 NPOI.HSSF.Model.LinkTable::get_NumNames()
extern void LinkTable_get_NumNames_mFBF81401C00851FE0170B8A80380B680EF74556C (void);
// 0x000001FD System.Int32 NPOI.HSSF.Model.LinkTable::FindRefIndexFromExtBookIndex(System.Int32)
extern void LinkTable_FindRefIndexFromExtBookIndex_m6F1A8D7263B71FA88D09917A7887CB87DA9E5FB2 (void);
// 0x000001FE NPOI.SS.Formula.PTG.NameXPtg NPOI.HSSF.Model.LinkTable::GetNameXPtg(System.String)
extern void LinkTable_GetNameXPtg_m537343CA8BD866263C0FCDDB23A5CD814FD9545A (void);
// 0x000001FF NPOI.HSSF.Record.NameRecord NPOI.HSSF.Model.LinkTable::GetNameRecord(System.Int32)
extern void LinkTable_GetNameRecord_m905F99EEC0E24F28DE9B3F2C4FD9E94A38B626F3 (void);
// 0x00000200 NPOI.SS.Formula.PTG.NameXPtg NPOI.HSSF.Model.LinkTable::AddNameXPtg(System.String)
extern void LinkTable_AddNameXPtg_mEBFAA347D93566A4A7FC1C834718CA87E5A16281 (void);
// 0x00000201 System.Int32 NPOI.HSSF.Model.LinkTable::GetSheetIndex(System.String[],System.String)
extern void LinkTable_GetSheetIndex_mFFFE41A46D4DC9F5A491C901A62DAD4B40F850F6 (void);
// 0x00000202 System.Int32 NPOI.HSSF.Model.LinkTable::GetExternalSheetIndex(System.String,System.String)
extern void LinkTable_GetExternalSheetIndex_mA29F99E8D91B752E7181A0AF79AAB1ECF31F4EDE (void);
// 0x00000203 System.String[] NPOI.HSSF.Model.LinkTable::GetExternalBookAndSheetName(System.Int32)
extern void LinkTable_GetExternalBookAndSheetName_m110202F4CCC79A3E38D3AC5C9E54E324EEDD8309 (void);
// 0x00000204 System.Int32 NPOI.HSSF.Model.LinkTable::CheckExternSheet(System.Int32)
extern void LinkTable_CheckExternSheet_m66E890116CAF8A4A9FC247BB9AB8DB04333AC165 (void);
// 0x00000205 System.Int32 NPOI.HSSF.Model.LinkTable::FindFirstRecordLocBySid(System.Int16)
extern void LinkTable_FindFirstRecordLocBySid_m27A64E0FA24888C28978513A6E5B04161702A692 (void);
// 0x00000206 System.String NPOI.HSSF.Model.LinkTable::ResolveNameXText(System.Int32,System.Int32)
extern void LinkTable_ResolveNameXText_m12807E1BFD85D458FD8183C1750FED831AFE89DC (void);
// 0x00000207 System.Void NPOI.HSSF.Model.LinkTable_CRNBlock::.ctor(NPOI.HSSF.Model.RecordStream)
extern void CRNBlock__ctor_m963E6EE0852D8719D242026FE75661651EC3197F (void);
// 0x00000208 System.Void NPOI.HSSF.Model.LinkTable_ExternalBookBlock::.ctor()
extern void ExternalBookBlock__ctor_m7F4A3466918969B2868F27C6977D90FC8726A1AC (void);
// 0x00000209 System.Void NPOI.HSSF.Model.LinkTable_ExternalBookBlock::.ctor(NPOI.HSSF.Model.RecordStream)
extern void ExternalBookBlock__ctor_mAE4B594B07FEA6ADDE9FD87EF58943B689C023E7 (void);
// 0x0000020A System.Int32 NPOI.HSSF.Model.LinkTable_ExternalBookBlock::get_NumberOfNames()
extern void ExternalBookBlock_get_NumberOfNames_m17F6E73824F0917BF7FF8598F2D746BD9462A2A2 (void);
// 0x0000020B System.Int32 NPOI.HSSF.Model.LinkTable_ExternalBookBlock::AddExternalName(NPOI.HSSF.Record.ExternalNameRecord)
extern void ExternalBookBlock_AddExternalName_m3B72095B696383492184004FFB3C9DF69263AA11 (void);
// 0x0000020C System.Void NPOI.HSSF.Model.LinkTable_ExternalBookBlock::.ctor(System.Int32)
extern void ExternalBookBlock__ctor_m1B59E9FBABED42774979D4F9E909D85BEECA2942 (void);
// 0x0000020D NPOI.HSSF.Record.SupBookRecord NPOI.HSSF.Model.LinkTable_ExternalBookBlock::GetExternalBookRecord()
extern void ExternalBookBlock_GetExternalBookRecord_m2D8B59E37DB815ADC5A9F62A0C7FE989B93FD3EB (void);
// 0x0000020E System.String NPOI.HSSF.Model.LinkTable_ExternalBookBlock::GetNameText(System.Int32)
extern void ExternalBookBlock_GetNameText_m5EB61322FA47488A08ABCD4F73C23391C811620D (void);
// 0x0000020F System.Int32 NPOI.HSSF.Model.LinkTable_ExternalBookBlock::GetIndexOfName(System.String)
extern void ExternalBookBlock_GetIndexOfName_m33448F05E061FDEED5F94EC6B26F2A6EABF2B64F (void);
// 0x00000210 System.Void NPOI.HSSF.Model.RecordOrderer::AddNewSheetRecord(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>,NPOI.HSSF.Record.RecordBase)
extern void RecordOrderer_AddNewSheetRecord_m2F8D0AC266ADEB5E599795124AF9D5F83C1CF8C0 (void);
// 0x00000211 System.Int32 NPOI.HSSF.Model.RecordOrderer::FindSheetInsertPos(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>,System.Type)
extern void RecordOrderer_FindSheetInsertPos_mEAC59A5C7FECE74427AB0F7974DC702F597060A9 (void);
// 0x00000212 System.Int32 NPOI.HSSF.Model.RecordOrderer::GetWorksheetProtectionBlockInsertPos(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void RecordOrderer_GetWorksheetProtectionBlockInsertPos_mE44F6251567D333CF252AB22301B75042C98F95D (void);
// 0x00000213 System.Boolean NPOI.HSSF.Model.RecordOrderer::IsProtectionSubsequentRecord(System.Object)
extern void RecordOrderer_IsProtectionSubsequentRecord_m701F64AE0C30A1666094B85B3E50838B79D2FFFD (void);
// 0x00000214 System.Int32 NPOI.HSSF.Model.RecordOrderer::GetPageBreakRecordInsertPos(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void RecordOrderer_GetPageBreakRecordInsertPos_mCEF0477F437AB07C82240D69C10D20D684FCC486 (void);
// 0x00000215 System.Boolean NPOI.HSSF.Model.RecordOrderer::IsPageBreakPriorRecord(NPOI.HSSF.Record.RecordBase)
extern void RecordOrderer_IsPageBreakPriorRecord_m43CB8F6DEE21C0D81B19C53D1A44EC5195083303 (void);
// 0x00000216 System.Int32 NPOI.HSSF.Model.RecordOrderer::FindInsertPosForNewCondFormatTable(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void RecordOrderer_FindInsertPosForNewCondFormatTable_m37477085A9F801CB02A6DCD6876E994AEE8007C2 (void);
// 0x00000217 System.Int32 NPOI.HSSF.Model.RecordOrderer::FindInsertPosForNewMergedRecordTable(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void RecordOrderer_FindInsertPosForNewMergedRecordTable_mF330F73FBA88830A5E88115A7906B1FAF2B47846 (void);
// 0x00000218 System.Int32 NPOI.HSSF.Model.RecordOrderer::FindDataValidationTableInsertPos(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void RecordOrderer_FindDataValidationTableInsertPos_mF35480E67592B9E5100354732B9911ABB7E8CE43 (void);
// 0x00000219 System.Boolean NPOI.HSSF.Model.RecordOrderer::IsDVTPriorRecord(NPOI.HSSF.Record.RecordBase)
extern void RecordOrderer_IsDVTPriorRecord_m7F75E333EF0BE4B96E723DF6AB89DDA169C8EAF9 (void);
// 0x0000021A System.Boolean NPOI.HSSF.Model.RecordOrderer::IsDVTSubsequentRecord(System.Int16)
extern void RecordOrderer_IsDVTSubsequentRecord_mCCDDAB4D6789AC8375362D0E1F6EBCEE880FD266 (void);
// 0x0000021B System.Int32 NPOI.HSSF.Model.RecordOrderer::GetDimensionsIndex(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void RecordOrderer_GetDimensionsIndex_mA283D0A7C2D122E9005D86340E922CD6A2784D91 (void);
// 0x0000021C System.Int32 NPOI.HSSF.Model.RecordOrderer::GetGutsRecordInsertPos(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void RecordOrderer_GetGutsRecordInsertPos_mA26627C3AD4F56F4A6F352C3215EB6A7A9B58D04 (void);
// 0x0000021D System.Boolean NPOI.HSSF.Model.RecordOrderer::IsGutsPriorRecord(NPOI.HSSF.Record.RecordBase)
extern void RecordOrderer_IsGutsPriorRecord_m6F884DCA68223CA871B6D56A18B5B95DDA548C14 (void);
// 0x0000021E System.Boolean NPOI.HSSF.Model.RecordOrderer::IsEndOfRowBlock(System.Int32)
extern void RecordOrderer_IsEndOfRowBlock_m00F24F4DEFD3387E8548BAB91B1F9FE6DA83EA72 (void);
// 0x0000021F System.Boolean NPOI.HSSF.Model.RecordOrderer::IsRowBlockRecord(System.Int32)
extern void RecordOrderer_IsRowBlockRecord_m3F30AA0969BE855299CEE06CF96C372477A2CE1E (void);
// 0x00000220 System.Void NPOI.HSSF.Model.RecordStream::.ctor(System.Collections.IList,System.Int32,System.Int32)
extern void RecordStream__ctor_m117AB967713C99CF31C6619ECDA31C86D87D1C6A (void);
// 0x00000221 System.Void NPOI.HSSF.Model.RecordStream::.ctor(System.Collections.IList,System.Int32)
extern void RecordStream__ctor_mC946AE164AF2C37293AE9A57F82B2F0C09E71998 (void);
// 0x00000222 System.Boolean NPOI.HSSF.Model.RecordStream::HasNext()
extern void RecordStream_HasNext_mE9B5B319CBA986ABF92FD0315F189F9D80D9C415 (void);
// 0x00000223 NPOI.HSSF.Record.Record NPOI.HSSF.Model.RecordStream::GetNext()
extern void RecordStream_GetNext_mF746E2A54A9D51803BE3D8E3051A3C28B0FF0486 (void);
// 0x00000224 System.Int32 NPOI.HSSF.Model.RecordStream::PeekNextSid()
extern void RecordStream_PeekNextSid_mC862FDC3398596567EA5E76876C642B71176EDF6 (void);
// 0x00000225 System.Type NPOI.HSSF.Model.RecordStream::PeekNextClass()
extern void RecordStream_PeekNextClass_m722E758B3A1C92337BCCF867576412CDF83119F8 (void);
// 0x00000226 System.Int32 NPOI.HSSF.Model.RecordStream::GetCountRead()
extern void RecordStream_GetCountRead_m1E90E6037CD6AAEA6D76E57598D851A62AC69F36 (void);
// 0x00000227 System.Void NPOI.HSSF.Model.RowBlocksReader::.ctor(NPOI.HSSF.Model.RecordStream)
extern void RowBlocksReader__ctor_mD4E75DCDFFDCA34A91E6A7A1832088EA6363CB43 (void);
// 0x00000228 NPOI.HSSF.Record.MergeCellsRecord[] NPOI.HSSF.Model.RowBlocksReader::get_LooseMergedCells()
extern void RowBlocksReader_get_LooseMergedCells_mC5B01FB3A0DBB7DC7183C9A6CF119A079EFDEE20 (void);
// 0x00000229 NPOI.HSSF.Record.Aggregates.SharedValueManager NPOI.HSSF.Model.RowBlocksReader::get_SharedFormulaManager()
extern void RowBlocksReader_get_SharedFormulaManager_m2D54D3DFD7C0A797224FCEBEBE2DC5EE1E5F713E (void);
// 0x0000022A NPOI.HSSF.Model.RecordStream NPOI.HSSF.Model.RowBlocksReader::get_PlainRecordStream()
extern void RowBlocksReader_get_PlainRecordStream_m054246CCAEC2A7E8388058D0DDBA678A3CDA9578 (void);
// 0x0000022B NPOI.HSSF.Record.CellValueRecordInterface[] NPOI.HSSF.Model.InternalSheet::GetValueRecords()
extern void InternalSheet_GetValueRecords_mD4CEC44EF00C8624FC4FDB9EA4FCCC38498E1B02 (void);
// 0x0000022C NPOI.HSSF.Model.InternalSheet NPOI.HSSF.Model.InternalSheet::CreateSheet(NPOI.HSSF.Model.RecordStream)
extern void InternalSheet_CreateSheet_m64648BF2CA14E6C19F1FC71682E0F24CDF283538 (void);
// 0x0000022D System.Void NPOI.HSSF.Model.InternalSheet::.ctor(NPOI.HSSF.Model.RecordStream)
extern void InternalSheet__ctor_m3729E092C4D0BF2A3024D9D8E14866D033AF77A7 (void);
// 0x0000022E System.Void NPOI.HSSF.Model.InternalSheet::SpillAggregate(NPOI.HSSF.Record.Aggregates.RecordAggregate,System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void InternalSheet_SpillAggregate_m0CC3318ABCE36FCE4B5364B56C5E172995D51ECD (void);
// 0x0000022F System.Void NPOI.HSSF.Model.InternalSheet::.ctor()
extern void InternalSheet__ctor_m85D5353F85ABD48396200CAC71FC19F55E6DF405 (void);
// 0x00000230 System.Void NPOI.HSSF.Model.InternalSheet::AddValueRecord(System.Int32,NPOI.HSSF.Record.CellValueRecordInterface)
extern void InternalSheet_AddValueRecord_m4BC68BBCCB369207007B1829FE626FCDB323110D (void);
// 0x00000231 System.Void NPOI.HSSF.Model.InternalSheet::ReplaceValueRecord(NPOI.HSSF.Record.CellValueRecordInterface)
extern void InternalSheet_ReplaceValueRecord_mC2DFA70FAB0ACC88F09C12CA0DA3EE1C904EF36C (void);
// 0x00000232 System.Void NPOI.HSSF.Model.InternalSheet::AddRow(NPOI.HSSF.Record.RowRecord)
extern void InternalSheet_AddRow_mFF7CD52820A331D9E0CD1A69514145EBA97D2998 (void);
// 0x00000233 NPOI.HSSF.Record.RowRecord NPOI.HSSF.Model.InternalSheet::get_NextRow()
extern void InternalSheet_get_NextRow_m1647CB0209D73F6AF9B0821BC7EAA6CB7232D317 (void);
// 0x00000234 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalSheet::CreateBOF()
extern void InternalSheet_CreateBOF_mA53E13372DDAD102E65858CDBA94D7568C3043C9 (void);
// 0x00000235 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalSheet::CreateCalcMode()
extern void InternalSheet_CreateCalcMode_mC1AAED521855153C76C6B145650EC4F15AE50352 (void);
// 0x00000236 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalSheet::CreateCalcCount()
extern void InternalSheet_CreateCalcCount_mE45DE1F14DD7594DC94A0985446876EF1625F501 (void);
// 0x00000237 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalSheet::CreateRefMode()
extern void InternalSheet_CreateRefMode_mC26E1A2090D99584E3A06294CE56BE4D995C2A94 (void);
// 0x00000238 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalSheet::CreateIteration()
extern void InternalSheet_CreateIteration_mEE610D4F037C8BE6510122F4328CA3E2F9F5BDEE (void);
// 0x00000239 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalSheet::CreateDelta()
extern void InternalSheet_CreateDelta_mC6AF04A33C915BD38A3AF0CE97F39CA7CF976B3B (void);
// 0x0000023A NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalSheet::CreateSaveRecalc()
extern void InternalSheet_CreateSaveRecalc_mBF992F3525255049AA62C3CF5B6A5795AFB65EDC (void);
// 0x0000023B NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalSheet::CreatePrintHeaders()
extern void InternalSheet_CreatePrintHeaders_mD233BE8C6FF47D0ECFCF7DFD860B515B2708B338 (void);
// 0x0000023C NPOI.HSSF.Record.PrintGridlinesRecord NPOI.HSSF.Model.InternalSheet::CreatePrintGridlines()
extern void InternalSheet_CreatePrintGridlines_mF9F6ED42EC5F040C42E56DE3C44160B263756F4A (void);
// 0x0000023D NPOI.HSSF.Record.GridsetRecord NPOI.HSSF.Model.InternalSheet::CreateGridset()
extern void InternalSheet_CreateGridset_m1E33D62C9ADFBB7D094BA744E67E462C0448120F (void);
// 0x0000023E NPOI.HSSF.Record.GutsRecord NPOI.HSSF.Model.InternalSheet::CreateGuts()
extern void InternalSheet_CreateGuts_m4690EFAD825C229231EEE63EB49FF40FFDD06779 (void);
// 0x0000023F NPOI.HSSF.Record.DefaultRowHeightRecord NPOI.HSSF.Model.InternalSheet::CreateDefaultRowHeight()
extern void InternalSheet_CreateDefaultRowHeight_m811C7B7AA38F5B86B1989B6A8C3EF38EEBD110DF (void);
// 0x00000240 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalSheet::CreateWSBool()
extern void InternalSheet_CreateWSBool_m80BBD9F93F1939A136E27D34BD554CB29EDE0BC5 (void);
// 0x00000241 NPOI.HSSF.Record.DefaultColWidthRecord NPOI.HSSF.Model.InternalSheet::CreateDefaultColWidth()
extern void InternalSheet_CreateDefaultColWidth_m2F0C6E4E192AD253E9152D4206EE56B13D4E86F9 (void);
// 0x00000242 System.Int16 NPOI.HSSF.Model.InternalSheet::get_DefaultRowHeight()
extern void InternalSheet_get_DefaultRowHeight_m72EEDE9AF8C2CB774EF276BE53204F2577D2AFFF (void);
// 0x00000243 System.Int16 NPOI.HSSF.Model.InternalSheet::GetXFIndexForColAt(System.Int16)
extern void InternalSheet_GetXFIndexForColAt_m0CDC322BB5D69CE8605B3EC20531D609051372FE (void);
// 0x00000244 NPOI.HSSF.Record.DimensionsRecord NPOI.HSSF.Model.InternalSheet::CreateDimensions()
extern void InternalSheet_CreateDimensions_mAA78D7D11EB7B6ED11494148C75FE5EBA493B40A (void);
// 0x00000245 NPOI.HSSF.Record.WindowTwoRecord NPOI.HSSF.Model.InternalSheet::CreateWindowTwo()
extern void InternalSheet_CreateWindowTwo_m12837D4602C1EC3F83D536B516086D6FC34E3466 (void);
// 0x00000246 NPOI.HSSF.Record.SelectionRecord NPOI.HSSF.Model.InternalSheet::CreateSelection()
extern void InternalSheet_CreateSelection_mD188BFB78F3F32CE8185354F8B0E897FB752EAB8 (void);
// 0x00000247 System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase> NPOI.HSSF.Model.InternalSheet::get_Records()
extern void InternalSheet_get_Records_m9A06C4B54724DEE9BAE99E0FE74B46A0F7222546 (void);
// 0x00000248 System.Int32 NPOI.HSSF.Model.InternalSheet::FindFirstRecordLocBySid(System.Int16)
extern void InternalSheet_FindFirstRecordLocBySid_mE865E5054194A6AA5C77CDEBE939C1C5CCEA6DCB (void);
// 0x00000249 System.Void NPOI.HSSF.Model.InternalSheet::Preserialize()
extern void InternalSheet_Preserialize_mCD84CB5C2563D02C1C249911AEDA8BB8D3415AAD (void);
// 0x0000024A NPOI.HSSF.Record.Aggregates.RowRecordsAggregate NPOI.HSSF.Model.InternalSheet::get_RowsAggregate()
extern void InternalSheet_get_RowsAggregate_mEDA8E232849E75FF9978928DBA544A17235F1B06 (void);
// 0x0000024B System.Void NPOI.HSSF.Model.InternalSheet::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor,System.Int32)
extern void InternalSheet_VisitContainedRecords_mDC72D134F5F6A4FDAB540D5109BBEE0A63EE2002 (void);
// 0x0000024C System.Int32 NPOI.HSSF.Model.InternalSheet::GetSizeOfInitialSheetRecords(System.Int32)
extern void InternalSheet_GetSizeOfInitialSheetRecords_m35A9D7733F4AAE4458C4DBBC155F85E4F09A4E59 (void);
// 0x0000024D System.Void NPOI.HSSF.Model.InternalSheet_RecordVisitor1::.ctor(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void RecordVisitor1__ctor_m4DC1907DA0642E3AC520C629946C716389227152 (void);
// 0x0000024E System.Void NPOI.HSSF.Model.InternalSheet_RecordVisitor1::VisitRecord(NPOI.HSSF.Record.Record)
extern void RecordVisitor1_VisitRecord_m73F18A779576848E5154DC6B75A4672BCB5704EE (void);
// 0x0000024F System.Void NPOI.HSSF.Model.InternalWorkbook::.ctor()
extern void InternalWorkbook__ctor_m4742DA02A956302D95282D296E422B73587CC7BE (void);
// 0x00000250 NPOI.HSSF.Model.InternalWorkbook NPOI.HSSF.Model.InternalWorkbook::CreateWorkbook(System.Collections.Generic.List`1<NPOI.HSSF.Record.Record>)
extern void InternalWorkbook_CreateWorkbook_mEE43BBF3820442412817422AC902BC7BF2C14F9F (void);
// 0x00000251 NPOI.HSSF.Record.NameCommentRecord NPOI.HSSF.Model.InternalWorkbook::GetNameCommentRecord(NPOI.HSSF.Record.NameRecord)
extern void InternalWorkbook_GetNameCommentRecord_m75B14021A35CBACA143B63157775D031D20AE0E9 (void);
// 0x00000252 NPOI.HSSF.Model.InternalWorkbook NPOI.HSSF.Model.InternalWorkbook::CreateWorkbook()
extern void InternalWorkbook_CreateWorkbook_mA74EE35DFCB6236F2ED63CCF5937EE26C58B31C0 (void);
// 0x00000253 NPOI.SS.Formula.ExternalSheet NPOI.HSSF.Model.InternalWorkbook::GetExternalSheet(System.Int32)
extern void InternalWorkbook_GetExternalSheet_mE682896BBC1DD0611F0D8B4933BEB8B64BD8CCBD (void);
// 0x00000254 System.Int32 NPOI.HSSF.Model.InternalWorkbook::get_NumRecords()
extern void InternalWorkbook_get_NumRecords_m4E768375113A58CDBAAA8606F47A2CC872586D5A (void);
// 0x00000255 System.Void NPOI.HSSF.Model.InternalWorkbook::SetSheetBof(System.Int32,System.Int32)
extern void InternalWorkbook_SetSheetBof_mB32626D753B251BA7841ECD5417F113F88066DFD (void);
// 0x00000256 NPOI.HSSF.Record.BoundSheetRecord NPOI.HSSF.Model.InternalWorkbook::GetBoundSheetRec(System.Int32)
extern void InternalWorkbook_GetBoundSheetRec_m1B1E0838FC38163144AC926C5D22B2C5EC36D0E4 (void);
// 0x00000257 System.String NPOI.HSSF.Model.InternalWorkbook::GetSheetName(System.Int32)
extern void InternalWorkbook_GetSheetName_m198D1A75213A8635F793B4D1D6D0C1DBBBFCB03F (void);
// 0x00000258 System.Int32 NPOI.HSSF.Model.InternalWorkbook::GetSheetIndex(System.String)
extern void InternalWorkbook_GetSheetIndex_m301048FC40152A38BCDEA82BA7F8F875CA893997 (void);
// 0x00000259 System.Void NPOI.HSSF.Model.InternalWorkbook::CheckSheets(System.Int32)
extern void InternalWorkbook_CheckSheets_m59ADE30F97265A83EC0701993AF02AA52AE9CA13 (void);
// 0x0000025A System.Int32 NPOI.HSSF.Model.InternalWorkbook::FixTabIdRecord()
extern void InternalWorkbook_FixTabIdRecord_m09E64B7D5F7013B5FE9CDFC4F190B8102605FFB8 (void);
// 0x0000025B System.Int32 NPOI.HSSF.Model.InternalWorkbook::get_NumSheets()
extern void InternalWorkbook_get_NumSheets_m9B37EF3BD59BBF945526DC5EB0DB129598814BF9 (void);
// 0x0000025C NPOI.HSSF.Record.ExtendedFormatRecord NPOI.HSSF.Model.InternalWorkbook::GetExFormatAt(System.Int32)
extern void InternalWorkbook_GetExFormatAt_m738D463D1311B3796A946B56A200329E91C58C82 (void);
// 0x0000025D System.Int32 NPOI.HSSF.Model.InternalWorkbook::AddSSTString(NPOI.HSSF.Record.UnicodeString)
extern void InternalWorkbook_AddSSTString_m9C45FC9F79157DDC1BA7DD742A519CCA360AF303 (void);
// 0x0000025E NPOI.HSSF.Record.UnicodeString NPOI.HSSF.Model.InternalWorkbook::GetSSTString(System.Int32)
extern void InternalWorkbook_GetSSTString_m34F7143963A7898590FBF724664E58A4296510B7 (void);
// 0x0000025F System.Void NPOI.HSSF.Model.InternalWorkbook::InsertSST()
extern void InternalWorkbook_InsertSST_m0027CEF188B3A5AF6C7A1CCBB3AA318E2006FF4C (void);
// 0x00000260 System.Int32 NPOI.HSSF.Model.InternalWorkbook::Serialize(System.Int32,System.Byte[])
extern void InternalWorkbook_Serialize_m421FF7E153F341090F75A6B6101CC2BFBF0D5651 (void);
// 0x00000261 System.Void NPOI.HSSF.Model.InternalWorkbook::PreSerialize()
extern void InternalWorkbook_PreSerialize_m7BF0411B4982E8619B3FA34EEC3889E7B212EE8C (void);
// 0x00000262 System.Int32 NPOI.HSSF.Model.InternalWorkbook::get_Size()
extern void InternalWorkbook_get_Size_mEB4A06BF87B36AC2B5E4309B54C6B167D7668F41 (void);
// 0x00000263 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateBOF()
extern void InternalWorkbook_CreateBOF_mEABF08927E3F2C4EF3323161EB8B3E7D44C6B6D1 (void);
// 0x00000264 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateMMS()
extern void InternalWorkbook_CreateMMS_mBF7A35DB81A7183BE6B251BA684A249A892E3BEA (void);
// 0x00000265 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateWriteAccess()
extern void InternalWorkbook_CreateWriteAccess_mF525B7F27407ECAD3EE9CAD148597C4ACB3013D7 (void);
// 0x00000266 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateCodepage()
extern void InternalWorkbook_CreateCodepage_m1A537C2C30E2463E96AE311ADFEF855CEC2DE1CB (void);
// 0x00000267 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateDSF()
extern void InternalWorkbook_CreateDSF_m08D2824AA694DF15BCC71DC20E2346ADD0F2B19B (void);
// 0x00000268 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateTabId()
extern void InternalWorkbook_CreateTabId_m4A2A3B046113C665C88947A5AAE8BC6D658F562C (void);
// 0x00000269 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateFnGroupCount()
extern void InternalWorkbook_CreateFnGroupCount_mF5325F7101295B0EFA66D3A95419D6EACC8BC8AF (void);
// 0x0000026A NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateWindowProtect()
extern void InternalWorkbook_CreateWindowProtect_m0B708EB532833C33F985F598A0ACECDACDB4CF4F (void);
// 0x0000026B NPOI.HSSF.Record.ProtectRecord NPOI.HSSF.Model.InternalWorkbook::CreateProtect()
extern void InternalWorkbook_CreateProtect_mA4AF57A79EC84083A0F0FB14124E1B79A61EED8C (void);
// 0x0000026C NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreatePassword()
extern void InternalWorkbook_CreatePassword_mD8E05D0BB41EC6FCA26F57B2D801D42D9EE1989F (void);
// 0x0000026D NPOI.HSSF.Record.ProtectionRev4Record NPOI.HSSF.Model.InternalWorkbook::CreateProtectionRev4()
extern void InternalWorkbook_CreateProtectionRev4_mFCA56839B2E6379508AC651F7777BED7E51C24A3 (void);
// 0x0000026E NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreatePasswordRev4()
extern void InternalWorkbook_CreatePasswordRev4_m590EADDA6991DD280ED8E9BE7ED60A77E5E26D5F (void);
// 0x0000026F NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateWindowOne()
extern void InternalWorkbook_CreateWindowOne_m31181DBF14CCC391F988BEEDF124BAD0591CD1E3 (void);
// 0x00000270 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateBackup()
extern void InternalWorkbook_CreateBackup_mB1D5835FF0B095BE4EC690FCDB9F34347F864A8A (void);
// 0x00000271 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateHideObj()
extern void InternalWorkbook_CreateHideObj_m9C0C022832B82CF05EA2F87368B7492C37AD0ABE (void);
// 0x00000272 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateDateWindow1904()
extern void InternalWorkbook_CreateDateWindow1904_mCA97BCF4001FEE7BE79CF11182F756CBB3337F93 (void);
// 0x00000273 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreatePrecision()
extern void InternalWorkbook_CreatePrecision_m3EE62D32059D0B71BCD1FE9F14A01825BAD6CB37 (void);
// 0x00000274 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateRefreshAll()
extern void InternalWorkbook_CreateRefreshAll_mCE9A33866DF9003A55799024B7FB2C90F2B18CE5 (void);
// 0x00000275 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateBookBool()
extern void InternalWorkbook_CreateBookBool_m31E5E0988430A9EABE5C18B7F533165129589C0E (void);
// 0x00000276 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateFont()
extern void InternalWorkbook_CreateFont_m774346DD541B9ABF9289BF2F637C8A2E0BD23323 (void);
// 0x00000277 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateExtendedFormat(System.Int32)
extern void InternalWorkbook_CreateExtendedFormat_m7817F1254E670245525B759E769E7F36E39354C6 (void);
// 0x00000278 NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateStyle(System.Int32)
extern void InternalWorkbook_CreateStyle_mEB7F7371745B91EC42BB72648FD654B899C44AD0 (void);
// 0x00000279 NPOI.HSSF.Record.UseSelFSRecord NPOI.HSSF.Model.InternalWorkbook::CreateUseSelFS()
extern void InternalWorkbook_CreateUseSelFS_mDBD6AC42C858972DF3E16377610CC7860FEBEC6E (void);
// 0x0000027A NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateBoundSheet(System.Int32)
extern void InternalWorkbook_CreateBoundSheet_m4B14B3B454BB87459185B207087B35D9B273C558 (void);
// 0x0000027B NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateCountry()
extern void InternalWorkbook_CreateCountry_mA0C29834B02836F1DD78813E088560F202F696FD (void);
// 0x0000027C NPOI.HSSF.Record.Record NPOI.HSSF.Model.InternalWorkbook::CreateExtendedSST()
extern void InternalWorkbook_CreateExtendedSST_m6CFDBA6ECBED877C2B7A9C826B12FEF3031182E3 (void);
// 0x0000027D NPOI.HSSF.Model.LinkTable NPOI.HSSF.Model.InternalWorkbook::get_OrCreateLinkTable()
extern void InternalWorkbook_get_OrCreateLinkTable_m6E3B561B184C2AC37BA7C29CF1059EF23B6B1449 (void);
// 0x0000027E System.String NPOI.HSSF.Model.InternalWorkbook::FindSheetNameFromExternSheet(System.Int32)
extern void InternalWorkbook_FindSheetNameFromExternSheet_m07F8627501D00FC28DCF52F627765E8402E85B1F (void);
// 0x0000027F System.Int32 NPOI.HSSF.Model.InternalWorkbook::CheckExternSheet(System.Int32)
extern void InternalWorkbook_CheckExternSheet_m388D310FDBD4F4C0856078ACFDE1E55FEB4AFFCF (void);
// 0x00000280 System.Int32 NPOI.HSSF.Model.InternalWorkbook::GetExternalSheetIndex(System.String,System.String)
extern void InternalWorkbook_GetExternalSheetIndex_mC0C1B81F0A5DACBEA4FC9DFD199CFA80B4790D85 (void);
// 0x00000281 System.Int32 NPOI.HSSF.Model.InternalWorkbook::get_NumNames()
extern void InternalWorkbook_get_NumNames_m16E0C82C90234C339A8A9DDDEB33D28543088A34 (void);
// 0x00000282 NPOI.SS.Formula.PTG.NameXPtg NPOI.HSSF.Model.InternalWorkbook::GetNameXPtg(System.String,NPOI.SS.Formula.Udf.UDFFinder)
extern void InternalWorkbook_GetNameXPtg_mAF85063F84EEDA7E8ECC31646D2F977A48B36F4A (void);
// 0x00000283 NPOI.HSSF.Record.NameRecord NPOI.HSSF.Model.InternalWorkbook::GetNameRecord(System.Int32)
extern void InternalWorkbook_GetNameRecord_mD38044D8C7A63B22E480E26AD031057CDECF68A7 (void);
// 0x00000284 System.Collections.Generic.List`1<NPOI.HSSF.Record.FormatRecord> NPOI.HSSF.Model.InternalWorkbook::get_Formats()
extern void InternalWorkbook_get_Formats_m11C1F4D4F02C6180767D61145FB01CEBDB0A8E43 (void);
// 0x00000285 NPOI.HSSF.Record.FormatRecord NPOI.HSSF.Model.InternalWorkbook::CreateFormat(System.Int32)
extern void InternalWorkbook_CreateFormat_mA3D3C480687565F2DEEC5B84810D5DE11567EA47 (void);
// 0x00000286 System.Boolean NPOI.HSSF.Model.InternalWorkbook::get_IsUsing1904DateWindowing()
extern void InternalWorkbook_get_IsUsing1904DateWindowing_m22836F2CA0CDF777B78B99B8103D9ED4E4089A25 (void);
// 0x00000287 System.String NPOI.HSSF.Model.InternalWorkbook::ResolveNameXText(System.Int32,System.Int32)
extern void InternalWorkbook_ResolveNameXText_m81830126ACBC6154DA2CFADA8082ED1188654340 (void);
// 0x00000288 System.Collections.Generic.List`1<NPOI.HSSF.Record.Record> NPOI.HSSF.Model.WorkbookRecordList::get_Records()
extern void WorkbookRecordList_get_Records_mE89F6DC492CD3D1C3CEFE865A932D8BF3BA2490E (void);
// 0x00000289 System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Records(System.Collections.Generic.List`1<NPOI.HSSF.Record.Record>)
extern void WorkbookRecordList_set_Records_m0655B584FA875F3EA9F08EA5DD43A86F5333F739 (void);
// 0x0000028A System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Count()
extern void WorkbookRecordList_get_Count_m0A3DB0D19B6F7C8C8BD9FDFA36CD0DD784D05F5D (void);
// 0x0000028B NPOI.HSSF.Record.Record NPOI.HSSF.Model.WorkbookRecordList::get_Item(System.Int32)
extern void WorkbookRecordList_get_Item_m5F91449AF833534300FFC93C5903B7211F7592A0 (void);
// 0x0000028C System.Void NPOI.HSSF.Model.WorkbookRecordList::Add(System.Int32,NPOI.HSSF.Record.Record)
extern void WorkbookRecordList_Add_m43ADD93156D5A565E11943CE58D4BB3C8A329275 (void);
// 0x0000028D System.Collections.IEnumerator NPOI.HSSF.Model.WorkbookRecordList::GetEnumerator()
extern void WorkbookRecordList_GetEnumerator_mB5652616072DEF6D7E78B76A6776B43C0B2AEF92 (void);
// 0x0000028E System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Protpos()
extern void WorkbookRecordList_get_Protpos_m37F88B5C41357C7E55ABFCD0C609D82EC74C4C76 (void);
// 0x0000028F System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Protpos(System.Int32)
extern void WorkbookRecordList_set_Protpos_m5F894AF0BF584A11B307F84F4B354DE00FEC75B3 (void);
// 0x00000290 System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Bspos()
extern void WorkbookRecordList_get_Bspos_mF082654D2B6FC73F90F7029473170DA688CC236F (void);
// 0x00000291 System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Bspos(System.Int32)
extern void WorkbookRecordList_set_Bspos_mD7F24C04DEFDF9FAB1C6187AEA5BC7EBD8E5D9E1 (void);
// 0x00000292 System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Tabpos()
extern void WorkbookRecordList_get_Tabpos_mA1E74C84E23C979502A287D9CCC00460F9FB85B0 (void);
// 0x00000293 System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Tabpos(System.Int32)
extern void WorkbookRecordList_set_Tabpos_m4D34260E87BAEB5E0A9B3457ED9E70B6D6BAD2D2 (void);
// 0x00000294 System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Fontpos()
extern void WorkbookRecordList_get_Fontpos_m3E8018546AB4CE689E0E0FC06EE4468FF27065C0 (void);
// 0x00000295 System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Fontpos(System.Int32)
extern void WorkbookRecordList_set_Fontpos_m92893074603DD1F575071B839C646D5DFDEC445D (void);
// 0x00000296 System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Xfpos()
extern void WorkbookRecordList_get_Xfpos_mEEAD10E3CB777B2C7E3993B49477B8E384A0B0B7 (void);
// 0x00000297 System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Xfpos(System.Int32)
extern void WorkbookRecordList_set_Xfpos_mCF5FD3A401C71C70B95F0DE567747DC192CB1D93 (void);
// 0x00000298 System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Backuppos()
extern void WorkbookRecordList_get_Backuppos_m2DDDC6AD8367FB5E0CB13A3A5F1D530F702BA858 (void);
// 0x00000299 System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Backuppos(System.Int32)
extern void WorkbookRecordList_set_Backuppos_m98685CE4CEDB486CEA07BD4EF66433D76C707F53 (void);
// 0x0000029A System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Palettepos()
extern void WorkbookRecordList_get_Palettepos_m17A8DB680519127BD08B582B16925022FF373010 (void);
// 0x0000029B System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Palettepos(System.Int32)
extern void WorkbookRecordList_set_Palettepos_m99319D9CF724B28EA7E8B6B097FF77E46F640F10 (void);
// 0x0000029C System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Namepos()
extern void WorkbookRecordList_get_Namepos_m6D0292EA108FF4C6118C657C772E07FEC9EC4DA4 (void);
// 0x0000029D System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Namepos(System.Int32)
extern void WorkbookRecordList_set_Namepos_m24597AAB041D1458188E58033B225753FF2B3DE3 (void);
// 0x0000029E System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_Supbookpos()
extern void WorkbookRecordList_get_Supbookpos_m2C6D388C0FEA69DDDEB469B29D97D0F42DCDE1D9 (void);
// 0x0000029F System.Void NPOI.HSSF.Model.WorkbookRecordList::set_Supbookpos(System.Int32)
extern void WorkbookRecordList_set_Supbookpos_m778E4255BD42B86A00003A7EEB2C4E7D1783045D (void);
// 0x000002A0 System.Int32 NPOI.HSSF.Model.WorkbookRecordList::get_ExternsheetPos()
extern void WorkbookRecordList_get_ExternsheetPos_m76C218A02FB903A4B084332C7C20E76C16D9EBAA (void);
// 0x000002A1 System.Void NPOI.HSSF.Model.WorkbookRecordList::set_ExternsheetPos(System.Int32)
extern void WorkbookRecordList_set_ExternsheetPos_mD7E93B3B1A9C32DFE8EFB2CD4D06BE66EAC5F54A (void);
// 0x000002A2 System.Void NPOI.HSSF.Model.WorkbookRecordList::.ctor()
extern void WorkbookRecordList__ctor_mADFAC13DF9FB3EB6C05FD371EFA20D2BB1310E8A (void);
// 0x000002A3 System.Void NPOI.HSSF.OldExcelFormatException::.ctor(System.String)
extern void OldExcelFormatException__ctor_m81D658F813B8D491FE0705004CD464CD4BAF0F31 (void);
// 0x000002A4 System.Void NPOI.HSSF.Record.AbstractEscherHolderRecord::.cctor()
extern void AbstractEscherHolderRecord__cctor_m017B7704E917C4339B487008D68DE697D074D3FB (void);
// 0x000002A5 System.Int32 NPOI.HSSF.Record.AbstractEscherHolderRecord::Serialize(System.Int32,System.Byte[])
extern void AbstractEscherHolderRecord_Serialize_m9C1533E2963FE4CE4EEFCBCB512A5EAFB1662EC0 (void);
// 0x000002A6 System.Int32 NPOI.HSSF.Record.AbstractEscherHolderRecord::get_RecordSize()
extern void AbstractEscherHolderRecord_get_RecordSize_m26DA84D2447EFBC3778042B5F06AAB9ADAFF5F30 (void);
// 0x000002A7 System.Void NPOI.HSSF.Record.AbstractEscherHolderRecord::Join(NPOI.HSSF.Record.AbstractEscherHolderRecord)
extern void AbstractEscherHolderRecord_Join_mBDA4AABAA46E7B8DD9E38D06181347952FAEE7DF (void);
// 0x000002A8 System.Void NPOI.HSSF.Record.AbstractEscherHolderRecord::ProcessContinueRecord(System.Byte[])
extern void AbstractEscherHolderRecord_ProcessContinueRecord_mE1E57AEE075E677FD83DBA5E1595B4561663D2FA (void);
// 0x000002A9 System.Byte[] NPOI.HSSF.Record.AbstractEscherHolderRecord::get_RawData()
extern void AbstractEscherHolderRecord_get_RawData_m3CF9FF62453E44356BA40735ECA2EC1A0400BB19 (void);
// 0x000002AA System.Void NPOI.HSSF.Record.Aggregates.CFRecordsAggregate::.ctor(NPOI.HSSF.Record.CFHeaderRecord,NPOI.HSSF.Record.CFRuleRecord[])
extern void CFRecordsAggregate__ctor_mC364273D3F58D662864308F664C029E8A6B79440 (void);
// 0x000002AB NPOI.HSSF.Record.Aggregates.CFRecordsAggregate NPOI.HSSF.Record.Aggregates.CFRecordsAggregate::CreateCFAggregate(NPOI.HSSF.Model.RecordStream)
extern void CFRecordsAggregate_CreateCFAggregate_mD3E96F7A6B0B726DF9D5FA232183BCF9F0754AFA (void);
// 0x000002AC System.Void NPOI.HSSF.Record.Aggregates.CFRecordsAggregate::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void CFRecordsAggregate_VisitContainedRecords_m35130CBE7E3C1BAE73D4D2B89CD6382DDAECA998 (void);
// 0x000002AD System.Int32 NPOI.HSSF.Record.Aggregates.CFRecordsAggregate::Serialize(System.Int32,System.Byte[])
extern void CFRecordsAggregate_Serialize_m037BBC0E2534BD725C7A93D7915934AA001206D4 (void);
// 0x000002AE System.Void NPOI.HSSF.Record.Aggregates.CFRecordsAggregate::CheckRuleIndex(System.Int32)
extern void CFRecordsAggregate_CheckRuleIndex_mE684FB67649071615D116F814642848174389E7E (void);
// 0x000002AF NPOI.HSSF.Record.CFRuleRecord NPOI.HSSF.Record.Aggregates.CFRecordsAggregate::GetRule(System.Int32)
extern void CFRecordsAggregate_GetRule_mC7242676FC917408A5C73108B446E81249CF9ACC (void);
// 0x000002B0 System.Int32 NPOI.HSSF.Record.Aggregates.CFRecordsAggregate::get_RecordSize()
extern void CFRecordsAggregate_get_RecordSize_m5BBAD5F6C86B4355227C6318E9831F035C3C7F9E (void);
// 0x000002B1 System.String NPOI.HSSF.Record.Aggregates.CFRecordsAggregate::ToString()
extern void CFRecordsAggregate_ToString_m8E4C8A1137A3690A87B9D4303DFE9F3DF94CB363 (void);
// 0x000002B2 System.Void NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate::.ctor()
extern void ColumnInfoRecordsAggregate__ctor_m27647626478945607AE06213B5E971987FEFBEC2 (void);
// 0x000002B3 System.Void NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate::.ctor(NPOI.HSSF.Model.RecordStream)
extern void ColumnInfoRecordsAggregate__ctor_mB6D873861E200C0BEB9E97F5FC7B19A2935702FC (void);
// 0x000002B4 System.Int32 NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate::get_RecordSize()
extern void ColumnInfoRecordsAggregate_get_RecordSize_m0CE8CE66BC7540FB81B8417137409F77B81CC6F0 (void);
// 0x000002B5 System.Int32 NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate::Serialize(System.Int32,System.Byte[])
extern void ColumnInfoRecordsAggregate_Serialize_m36C1A6FC718387CA73F64ADBA93EAE4B864F9B2A (void);
// 0x000002B6 System.Void NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void ColumnInfoRecordsAggregate_VisitContainedRecords_m2411EDB61BFC53A07CDA3C28E452E7EF7E732A6B (void);
// 0x000002B7 NPOI.HSSF.Record.ColumnInfoRecord NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate::GetColInfo(System.Int32)
extern void ColumnInfoRecordsAggregate_GetColInfo_mD3D7BED03B498E53244E96C4A7BCF59B9C5DFC62 (void);
// 0x000002B8 NPOI.HSSF.Record.ColumnInfoRecord NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate::FindColumnInfo(System.Int32)
extern void ColumnInfoRecordsAggregate_FindColumnInfo_mA849FAC4B4A143A48D2396FAA820B09CDB1589C5 (void);
// 0x000002B9 System.Void NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate_CIRComparator::.ctor()
extern void CIRComparator__ctor_mF21E5FAEDDD9DFCDB4AED8448B8B3DB0D0BCA481 (void);
// 0x000002BA System.Int32 NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate_CIRComparator::Compare(System.Object,System.Object)
extern void CIRComparator_Compare_m3BAA89779072ABE0A285DBBA25483DB69F0F832A (void);
// 0x000002BB System.Int32 NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate_CIRComparator::CompareColInfos(NPOI.HSSF.Record.ColumnInfoRecord,NPOI.HSSF.Record.ColumnInfoRecord)
extern void CIRComparator_CompareColInfos_m8DE6DD6369722F5143D017AD35E28D691146865A (void);
// 0x000002BC System.Void NPOI.HSSF.Record.Aggregates.ColumnInfoRecordsAggregate_CIRComparator::.cctor()
extern void CIRComparator__cctor_mE9DF81FC39F8D642690866DCA99E478E67D9AE61 (void);
// 0x000002BD System.Void NPOI.HSSF.Record.Aggregates.ConditionalFormattingTable::.ctor(NPOI.HSSF.Model.RecordStream)
extern void ConditionalFormattingTable__ctor_m5405FB6A6573B1B8E14079E2D191A6EF93B3E350 (void);
// 0x000002BE System.Void NPOI.HSSF.Record.Aggregates.ConditionalFormattingTable::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void ConditionalFormattingTable_VisitContainedRecords_mA3708520C41FEAEBB9A1766D6E2396C1F1E3CCCA (void);
// 0x000002BF System.Void NPOI.HSSF.Record.Aggregates.CustomViewSettingsRecordAggregate::.ctor(NPOI.HSSF.Model.RecordStream)
extern void CustomViewSettingsRecordAggregate__ctor_m6435990EA90075BB5C544C3661F90E45572011CB (void);
// 0x000002C0 System.Void NPOI.HSSF.Record.Aggregates.CustomViewSettingsRecordAggregate::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void CustomViewSettingsRecordAggregate_VisitContainedRecords_m26A3B8BB8E094E3850C44A3E3B2C4271C844542D (void);
// 0x000002C1 System.Boolean NPOI.HSSF.Record.Aggregates.CustomViewSettingsRecordAggregate::IsBeginRecord(System.Int32)
extern void CustomViewSettingsRecordAggregate_IsBeginRecord_mD437DE1A5972C386ADBAC88FD40321FD3111575B (void);
// 0x000002C2 System.Void NPOI.HSSF.Record.Aggregates.CustomViewSettingsRecordAggregate::Append(NPOI.HSSF.Record.RecordBase)
extern void CustomViewSettingsRecordAggregate_Append_m22951626B90089A1A2F16E78E7AE22A0C32BE4A8 (void);
// 0x000002C3 System.Void NPOI.HSSF.Record.Aggregates.DataValidityTable::.ctor(NPOI.HSSF.Model.RecordStream)
extern void DataValidityTable__ctor_mC996F5A70A03FFAA8DBEEF9D12D67ABA27B59164 (void);
// 0x000002C4 System.Void NPOI.HSSF.Record.Aggregates.DataValidityTable::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void DataValidityTable_VisitContainedRecords_mEE0D017F45A206BEF64A1418089BE580AC5CF531 (void);
// 0x000002C5 System.Int32 NPOI.HSSF.Record.CellValueRecordInterface::get_Row()
// 0x000002C6 System.Int32 NPOI.HSSF.Record.CellValueRecordInterface::get_Column()
// 0x000002C7 System.Int16 NPOI.HSSF.Record.CellValueRecordInterface::get_XFIndex()
// 0x000002C8 System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::.ctor(NPOI.HSSF.Record.FormulaRecord,NPOI.HSSF.Record.StringRecord,NPOI.HSSF.Record.Aggregates.SharedValueManager)
extern void FormulaRecordAggregate__ctor_mB9D631E5ACA6C578E70913C42F4727D15AF9FBFA (void);
// 0x000002C9 System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::NotifyFormulaChanging()
extern void FormulaRecordAggregate_NotifyFormulaChanging_mFA2ECF7A27E9EC4F5203B249D8605996FFD02E2F (void);
// 0x000002CA System.Boolean NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::get_IsPartOfArrayFormula()
extern void FormulaRecordAggregate_get_IsPartOfArrayFormula_m0615E83CE4E493C42D4DB88ECC4470F41D9194DB (void);
// 0x000002CB System.Int32 NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::Serialize(System.Int32,System.Byte[])
extern void FormulaRecordAggregate_Serialize_m6088943A40CB25FAC45D1F5A170A1C74DF369C99 (void);
// 0x000002CC System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void FormulaRecordAggregate_VisitContainedRecords_mB5E7BC3AED168E18849031F5823975AF696C61AA (void);
// 0x000002CD System.Int32 NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::get_RecordSize()
extern void FormulaRecordAggregate_get_RecordSize_m89CBCDBBA46BCC9F14B646226BE2A65670569CFE (void);
// 0x000002CE System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::HandleMissingSharedFormulaRecord(NPOI.HSSF.Record.FormulaRecord)
extern void FormulaRecordAggregate_HandleMissingSharedFormulaRecord_m6B8E375163F9BA29D6ED842FA392E84A947F1F35 (void);
// 0x000002CF NPOI.HSSF.Record.FormulaRecord NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::get_FormulaRecord()
extern void FormulaRecordAggregate_get_FormulaRecord_mE33C347FD968811B6D14EA5872C1B5A470596B21 (void);
// 0x000002D0 System.Int16 NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::get_XFIndex()
extern void FormulaRecordAggregate_get_XFIndex_m69725CB594956F0CEF3F208D37DA6E0908F6D20E (void);
// 0x000002D1 System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::set_XFIndex(System.Int16)
extern void FormulaRecordAggregate_set_XFIndex_m929EDACCD3ACA19A3D26A3C1E9725953646CF173 (void);
// 0x000002D2 System.Int32 NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::get_Column()
extern void FormulaRecordAggregate_get_Column_m4142983F214CF7A48A74AFE989F88EA68E9EC829 (void);
// 0x000002D3 System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::set_Column(System.Int32)
extern void FormulaRecordAggregate_set_Column_m6FE3C945F4F85496CE9B6B9C58BC9979B889C4F4 (void);
// 0x000002D4 System.Int32 NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::get_Row()
extern void FormulaRecordAggregate_get_Row_mA651F61CA132508D1C450A576B7AC3E04ED400BE (void);
// 0x000002D5 System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::set_Row(System.Int32)
extern void FormulaRecordAggregate_set_Row_m189338813F12DCE945988769E4D188F8EECEE83E (void);
// 0x000002D6 System.Int32 NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::CompareTo(System.Object)
extern void FormulaRecordAggregate_CompareTo_m6222CE8B18F4BB24A4205913B25D494F995BBC09 (void);
// 0x000002D7 System.Boolean NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::Equals(System.Object)
extern void FormulaRecordAggregate_Equals_m37F13B632CE527BD3A0DC2982B23231B7EE5DDFC (void);
// 0x000002D8 System.Int32 NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::GetHashCode()
extern void FormulaRecordAggregate_GetHashCode_m2DD1EC6CD7C19CB35F4BEA5B985BCD0627B6CE1A (void);
// 0x000002D9 System.String NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::ToString()
extern void FormulaRecordAggregate_ToString_m16ECE89FAB9465BDF50C2DA2411E1C303AFAD3F4 (void);
// 0x000002DA System.String NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::get_StringValue()
extern void FormulaRecordAggregate_get_StringValue_m7B20A542031363C8CECF0E0037BF5AC20D93B573 (void);
// 0x000002DB System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::SetCachedDoubleResult(System.Double)
extern void FormulaRecordAggregate_SetCachedDoubleResult_m3D5FBE5143FD14B233BBB4D2AB50E2F427966C11 (void);
// 0x000002DC System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::SetCachedStringResult(System.String)
extern void FormulaRecordAggregate_SetCachedStringResult_m87A80BC44BCD7999AEDB8C898ACE0BE562C4C6DA (void);
// 0x000002DD System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::SetCachedBooleanResult(System.Boolean)
extern void FormulaRecordAggregate_SetCachedBooleanResult_m2A58DF3ACCD8990AC5C5F050BE1AB6B445D900B0 (void);
// 0x000002DE System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::SetCachedErrorResult(System.Int32)
extern void FormulaRecordAggregate_SetCachedErrorResult_m57E0FE8BD6430E1636C61C24B4783EE4DB16F295 (void);
// 0x000002DF System.Object NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::Clone()
extern void FormulaRecordAggregate_Clone_m6BAF30ED571E57DA1600CA98A2FB9268FF60A547 (void);
// 0x000002E0 NPOI.SS.Formula.PTG.Ptg[] NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::get_FormulaTokens()
extern void FormulaRecordAggregate_get_FormulaTokens_m40BBC3F0CF25A7E6335B526C71C37A028A5ECC46 (void);
// 0x000002E1 System.Void NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::UnlinkSharedFormula()
extern void FormulaRecordAggregate_UnlinkSharedFormula_m9E51F67CC2BF45E6044477D39D2DF78613E4346F (void);
// 0x000002E2 NPOI.SS.Util.CellRangeAddress NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::GetArrayFormulaRange()
extern void FormulaRecordAggregate_GetArrayFormulaRange_m79079431CF13294B5A57399094AA72A1AA7EAD41 (void);
// 0x000002E3 NPOI.SS.Util.CellRangeAddress NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate::RemoveArrayFormula(System.Int32,System.Int32)
extern void FormulaRecordAggregate_RemoveArrayFormula_mBCAEA618395F7215C2F15195BABAE5C30E3A0C0D (void);
// 0x000002E4 System.Void NPOI.HSSF.Record.Aggregates.MergedCellsTable::.ctor()
extern void MergedCellsTable__ctor_mF9A745C310E576D4358C7EC8FC6A7C8C00277A1D (void);
// 0x000002E5 System.Void NPOI.HSSF.Record.Aggregates.MergedCellsTable::Read(NPOI.HSSF.Model.RecordStream)
extern void MergedCellsTable_Read_mE5F302186D972296D07F60267A8520C2B3E1565D (void);
// 0x000002E6 System.Int32 NPOI.HSSF.Record.Aggregates.MergedCellsTable::get_RecordSize()
extern void MergedCellsTable_get_RecordSize_m872F87904143058542B90AB3D4ED694DBF578302 (void);
// 0x000002E7 System.Void NPOI.HSSF.Record.Aggregates.MergedCellsTable::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void MergedCellsTable_VisitContainedRecords_m07CE63498D48C10C49A42BC5F524F7D0885E91E9 (void);
// 0x000002E8 System.Void NPOI.HSSF.Record.Aggregates.MergedCellsTable::AddRecords(NPOI.HSSF.Record.MergeCellsRecord[])
extern void MergedCellsTable_AddRecords_m34BF79D7645D3F41E2491AEB75B0CA27A36BD031 (void);
// 0x000002E9 System.Void NPOI.HSSF.Record.Aggregates.MergedCellsTable::AddMergeCellsRecord(NPOI.HSSF.Record.MergeCellsRecord)
extern void MergedCellsTable_AddMergeCellsRecord_mD482F4B8E7605D2F8DA0C86E464A8C7025C04254 (void);
// 0x000002EA System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock::.ctor(NPOI.HSSF.Model.RecordStream)
extern void PageSettingsBlock__ctor_m8E6813A93422DEC4AE96CA5A50B75D18CAD3E17A (void);
// 0x000002EB System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock::.ctor()
extern void PageSettingsBlock__ctor_m2E3A1B8A82A6AE4F2DB61918B83EC353106276D1 (void);
// 0x000002EC System.Boolean NPOI.HSSF.Record.Aggregates.PageSettingsBlock::IsComponentRecord(System.Int32)
extern void PageSettingsBlock_IsComponentRecord_m82476E2FE26BF3BFD8E31817852763600ABDFF3C (void);
// 0x000002ED System.Boolean NPOI.HSSF.Record.Aggregates.PageSettingsBlock::ReadARecord(NPOI.HSSF.Model.RecordStream)
extern void PageSettingsBlock_ReadARecord_mD097342646961CD67191B8CD8D213F9B9C69B1A0 (void);
// 0x000002EE System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock::CheckNotPresent(NPOI.HSSF.Record.Record)
extern void PageSettingsBlock_CheckNotPresent_m4D41B4FC865DD806C63403B99C4DCABC36992930 (void);
// 0x000002EF System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void PageSettingsBlock_VisitContainedRecords_m55FBBB11CACE63F9CD6487337699A178C5B6894F (void);
// 0x000002F0 System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock::VisitIfPresent(NPOI.HSSF.Record.Record,NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void PageSettingsBlock_VisitIfPresent_mA7F5AF761A33C7A2C813F4E5666EDD98EC2C385A (void);
// 0x000002F1 System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock::VisitIfPresent(NPOI.HSSF.Record.PageBreakRecord,NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void PageSettingsBlock_VisitIfPresent_m17CAA165A1DA92E715D2E06D1C745CD1D6D633AB (void);
// 0x000002F2 NPOI.HSSF.Record.HCenterRecord NPOI.HSSF.Record.Aggregates.PageSettingsBlock::CreateHCenter()
extern void PageSettingsBlock_CreateHCenter_m40E4A0E85309AD3087127267124DB869AF8AFEE7 (void);
// 0x000002F3 NPOI.HSSF.Record.VCenterRecord NPOI.HSSF.Record.Aggregates.PageSettingsBlock::CreateVCenter()
extern void PageSettingsBlock_CreateVCenter_m4635A5A77E5178A45CB5ECB69ED799ECD2531357 (void);
// 0x000002F4 NPOI.HSSF.Record.PrintSetupRecord NPOI.HSSF.Record.Aggregates.PageSettingsBlock::CreatePrintSetup()
extern void PageSettingsBlock_CreatePrintSetup_mF8C5FCBD27C3F2C3C00553291542E5A03C24FAAC (void);
// 0x000002F5 System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock::AddLateHeaderFooter(NPOI.HSSF.Record.HeaderFooterRecord)
extern void PageSettingsBlock_AddLateHeaderFooter_m6FBF17FD92C402BE24EC00B9754E166F7E3A0B35 (void);
// 0x000002F6 System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock::AddLateRecords(NPOI.HSSF.Model.RecordStream)
extern void PageSettingsBlock_AddLateRecords_m6FFA04BD54706BE808AF3528312F9D99E59E8B12 (void);
// 0x000002F7 System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock::PositionRecords(System.Collections.Generic.List`1<NPOI.HSSF.Record.RecordBase>)
extern void PageSettingsBlock_PositionRecords_mAFB2EFEAAB9F7BC7DFFEF29A4D600177D9FF407E (void);
// 0x000002F8 System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock_CustomRecordVisitor1::.ctor(NPOI.HSSF.Record.Aggregates.CustomViewSettingsRecordAggregate,NPOI.HSSF.Record.HeaderFooterRecord,System.Collections.Generic.List`1<NPOI.HSSF.Record.HeaderFooterRecord>,System.Collections.Generic.Dictionary`2<System.String,NPOI.HSSF.Record.HeaderFooterRecord>)
extern void CustomRecordVisitor1__ctor_m053515A95DEACAC8E7E20A24F656B775037B78BE (void);
// 0x000002F9 System.Void NPOI.HSSF.Record.Aggregates.PageSettingsBlock_CustomRecordVisitor1::VisitRecord(NPOI.HSSF.Record.Record)
extern void CustomRecordVisitor1_VisitRecord_mA18902558B7C1A0C190CBE6E105BFFF03B5B7DE5 (void);
// 0x000002FA System.Void NPOI.HSSF.Record.Aggregates.PLSAggregate::.ctor(NPOI.HSSF.Model.RecordStream)
extern void PLSAggregate__ctor_mDE489484D25EF0513E9C79A309804714DC4E35B1 (void);
// 0x000002FB System.Void NPOI.HSSF.Record.Aggregates.PLSAggregate::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void PLSAggregate_VisitContainedRecords_m46635C8D338C92DAC12C000DF147C5A32DD8B86A (void);
// 0x000002FC System.Void NPOI.HSSF.Record.Aggregates.PLSAggregate::.cctor()
extern void PLSAggregate__cctor_mBF14FE2830F54B9C7476C7B99561114D95A41AC7 (void);
// 0x000002FD System.Void NPOI.HSSF.Record.Aggregates.PositionTrackingVisitor::.ctor(NPOI.HSSF.Record.Aggregates.RecordVisitor,System.Int32)
extern void PositionTrackingVisitor__ctor_m319B490A3C2C6AF71401A32135A2FF2D29C04A4D (void);
// 0x000002FE System.Void NPOI.HSSF.Record.Aggregates.PositionTrackingVisitor::VisitRecord(NPOI.HSSF.Record.Record)
extern void PositionTrackingVisitor_VisitRecord_mC9AF3B585B583E7A3ADCCDA5C96A27DBFA803D09 (void);
// 0x000002FF System.Int32 NPOI.HSSF.Record.Aggregates.PositionTrackingVisitor::get_Position()
extern void PositionTrackingVisitor_get_Position_mA5FFBCD741E6CF87477AE6C8B03A7CC93309138C (void);
// 0x00000300 System.Void NPOI.HSSF.Record.Aggregates.PositionTrackingVisitor::set_Position(System.Int32)
extern void PositionTrackingVisitor_set_Position_m75569638CBF6BB41AFD18025DCA274B2F5B4D8E7 (void);
// 0x00000301 System.Void NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::.ctor()
extern void RowRecordsAggregate__ctor_mA6490AA48D62F0A61559EB7DB269F532993FB4FE (void);
// 0x00000302 NPOI.HSSF.Record.CellValueRecordInterface[] NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::GetValueRecords()
extern void RowRecordsAggregate_GetValueRecords_mEFC4BB9EDDFE1390D344EE8C2D2B0D6C6D2FD6CB (void);
// 0x00000303 System.Void NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::.ctor(NPOI.HSSF.Record.Aggregates.SharedValueManager)
extern void RowRecordsAggregate__ctor_mBE13EA56E859C522043C3F5A60505446727313B4 (void);
// 0x00000304 System.Int32 NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::VisitRowRecordsForBlock(System.Int32,NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void RowRecordsAggregate_VisitRowRecordsForBlock_mB786FD64C4A3A9C989DE00AC41A3E7D181AE2EA1 (void);
// 0x00000305 System.Void NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::VisitContainedRecords(NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void RowRecordsAggregate_VisitContainedRecords_mAD7D29128B29621AF46295511CDF4DF8A75DD8DD (void);
// 0x00000306 System.Void NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::.ctor(NPOI.HSSF.Model.RecordStream,NPOI.HSSF.Record.Aggregates.SharedValueManager)
extern void RowRecordsAggregate__ctor_mD9812990501AABF3A95ED646BEE8D0B60CFC252D (void);
// 0x00000307 System.Void NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::AddUnknownRecord(NPOI.HSSF.Record.Record)
extern void RowRecordsAggregate_AddUnknownRecord_m6A4D6C461DD21136BF9E95E013008507F3A06721 (void);
// 0x00000308 System.Void NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::InsertRow(NPOI.HSSF.Record.RowRecord)
extern void RowRecordsAggregate_InsertRow_m0B054F626CBAFDC07E455F36FB60A5E2A69F95BD (void);
// 0x00000309 System.Void NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::RemoveRow(NPOI.HSSF.Record.RowRecord)
extern void RowRecordsAggregate_RemoveRow_m45CF8093262956BB85B2B9A5BBDB9AE82397950F (void);
// 0x0000030A System.Void NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::InsertCell(NPOI.HSSF.Record.CellValueRecordInterface)
extern void RowRecordsAggregate_InsertCell_m1E5028EE28912751DB9041277E0F4D3F20DFBA22 (void);
// 0x0000030B System.Void NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::RemoveCell(NPOI.HSSF.Record.CellValueRecordInterface)
extern void RowRecordsAggregate_RemoveCell_mD969CAA8114A1FE8F4F3C371C39AE8DC4634CB1A (void);
// 0x0000030C NPOI.HSSF.Record.RowRecord NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::GetRow(System.Int32)
extern void RowRecordsAggregate_GetRow_m518F3828D47CE9FC3BF3C6AA644C88B23B0A82A4 (void);
// 0x0000030D NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::CreateFormula(System.Int32,System.Int32)
extern void RowRecordsAggregate_CreateFormula_m3BE3518E1924A206538945588BD2FB009080E01F (void);
// 0x0000030E System.Int32 NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::get_RowBlockCount()
extern void RowRecordsAggregate_get_RowBlockCount_m3C6B2E19F7F39D17F4C3033463DDBB4DBCF2264E (void);
// 0x0000030F System.Int32 NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::GetRowBlockSize(System.Int32)
extern void RowRecordsAggregate_GetRowBlockSize_m9F72DD19C7A6A455360CB00846DD8DFB6E5EAC54 (void);
// 0x00000310 System.Int32 NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::GetRowCountForBlock(System.Int32)
extern void RowRecordsAggregate_GetRowCountForBlock_mE58E9891CAF2566E649A87BEA666BF0948D3F11E (void);
// 0x00000311 System.Int32 NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::GetStartRowNumberForBlock(System.Int32)
extern void RowRecordsAggregate_GetStartRowNumberForBlock_mD3DD9BAD566EC12164A91A035D92F0809D70C401 (void);
// 0x00000312 System.Int32 NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::GetEndRowNumberForBlock(System.Int32)
extern void RowRecordsAggregate_GetEndRowNumberForBlock_m0F3E9CB9FD6D7873C53042AF7C7D125A7D5AD0AE (void);
// 0x00000313 System.Collections.IEnumerator NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::GetEnumerator()
extern void RowRecordsAggregate_GetEnumerator_m662D7E84C2D17E4CE69222361ED0921A0C383F4C (void);
// 0x00000314 NPOI.HSSF.Record.DimensionsRecord NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::CreateDimensions()
extern void RowRecordsAggregate_CreateDimensions_m26772D633C8FD80582D527889F129E6E79C58E5B (void);
// 0x00000315 NPOI.HSSF.Record.IndexRecord NPOI.HSSF.Record.Aggregates.RowRecordsAggregate::CreateIndexRecord(System.Int32,System.Int32,System.Int32)
extern void RowRecordsAggregate_CreateIndexRecord_m075FB813908111B1718FB3B12D2A4E6827061114 (void);
// 0x00000316 System.Void NPOI.HSSF.Record.Aggregates.SharedValueManager::.ctor(NPOI.HSSF.Record.SharedFormulaRecord[],NPOI.SS.Util.CellReference[],System.Collections.Generic.List`1<NPOI.HSSF.Record.ArrayRecord>,System.Collections.Generic.List`1<NPOI.HSSF.Record.TableRecord>)
extern void SharedValueManager__ctor_m1974DE9509A7493E7899D8DD998A052D6D8CF25F (void);
// 0x00000317 NPOI.HSSF.Record.Aggregates.SharedValueManager NPOI.HSSF.Record.Aggregates.SharedValueManager::CreateEmpty()
extern void SharedValueManager_CreateEmpty_mB7B670210892B75DBA35B091554E0232A7BE65C2 (void);
// 0x00000318 NPOI.HSSF.Record.Aggregates.SharedValueManager NPOI.HSSF.Record.Aggregates.SharedValueManager::Create(NPOI.HSSF.Record.SharedFormulaRecord[],NPOI.SS.Util.CellReference[],System.Collections.Generic.List`1<NPOI.HSSF.Record.ArrayRecord>,System.Collections.Generic.List`1<NPOI.HSSF.Record.TableRecord>)
extern void SharedValueManager_Create_m9AA85A919294C1A1B55647B32CF488DEB04720A3 (void);
// 0x00000319 NPOI.HSSF.Record.SharedFormulaRecord NPOI.HSSF.Record.Aggregates.SharedValueManager::LinkSharedFormulaRecord(NPOI.SS.Util.CellReference,NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate)
extern void SharedValueManager_LinkSharedFormulaRecord_mC87DC76C4A84A81D79534312788EC1FCB94FA3B0 (void);
// 0x0000031A NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroup NPOI.HSSF.Record.Aggregates.SharedValueManager::FindFormulaGroupForCell(NPOI.SS.Util.CellReference)
extern void SharedValueManager_FindFormulaGroupForCell_mC83BC39640C3AB72FDE0258BF15FF94B7F4380AB (void);
// 0x0000031B System.Int32 NPOI.HSSF.Record.Aggregates.SharedValueManager::GetKeyForCache(NPOI.SS.Util.CellReference)
extern void SharedValueManager_GetKeyForCache_m08FA885EBD2E06E710EEB3AC6335E22B1EC8E0D9 (void);
// 0x0000031C NPOI.HSSF.Record.SharedValueRecordBase NPOI.HSSF.Record.Aggregates.SharedValueManager::GetRecordForFirstCell(NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate)
extern void SharedValueManager_GetRecordForFirstCell_mD3A91FE534630D95E67B031C270BC57946CFA5D9 (void);
// 0x0000031D System.Void NPOI.HSSF.Record.Aggregates.SharedValueManager::Unlink(NPOI.HSSF.Record.SharedFormulaRecord)
extern void SharedValueManager_Unlink_m8DFB9FECFFB5449D5B335CACEA09B6345332A333 (void);
// 0x0000031E NPOI.SS.Util.CellRangeAddress8Bit NPOI.HSSF.Record.Aggregates.SharedValueManager::RemoveArrayFormula(System.Int32,System.Int32)
extern void SharedValueManager_RemoveArrayFormula_mB3DC3E413FCCEFBCED10A23BD9AD7546BF973822 (void);
// 0x0000031F NPOI.HSSF.Record.ArrayRecord NPOI.HSSF.Record.Aggregates.SharedValueManager::GetArrayRecord(System.Int32,System.Int32)
extern void SharedValueManager_GetArrayRecord_m20B9FFF800CC0268F340AB0F4096980E816907B8 (void);
// 0x00000320 System.Void NPOI.HSSF.Record.Aggregates.SharedValueManager::.cctor()
extern void SharedValueManager__cctor_mE637D67A9D38E8724C1302810AE3548A55523C18 (void);
// 0x00000321 NPOI.SS.Util.CellReference NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroup::get_FirstCell()
extern void SharedFormulaGroup_get_FirstCell_mB56AC2FABB527AF3959827EED275D5B1981BF379 (void);
// 0x00000322 System.Void NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroup::.ctor(NPOI.HSSF.Record.SharedFormulaRecord,NPOI.SS.Util.CellReference)
extern void SharedFormulaGroup__ctor_mAE412200241228B144EB5E66E6030F5FE3AF9525 (void);
// 0x00000323 System.Void NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroup::Add(NPOI.HSSF.Record.Aggregates.FormulaRecordAggregate)
extern void SharedFormulaGroup_Add_m4457B77E4C1EC53C16B3FE08E75FAE0AFE3F091E (void);
// 0x00000324 System.Void NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroup::UnlinkSharedFormulas()
extern void SharedFormulaGroup_UnlinkSharedFormulas_m5D53AB8F73FCF46725A3B5300C45FCF13A09A47D (void);
// 0x00000325 NPOI.HSSF.Record.SharedFormulaRecord NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroup::get_SFR()
extern void SharedFormulaGroup_get_SFR_m95ADDDC472C523C5142C0E22628A92063274E06C (void);
// 0x00000326 System.String NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroup::ToString()
extern void SharedFormulaGroup_ToString_m0BA31A5FC63B6EBE35569A27C1A0B4B59DFF52E9 (void);
// 0x00000327 System.Int32 NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroupComparator::Compare(NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroup,NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroup)
extern void SharedFormulaGroupComparator_Compare_mFA3439A781C710586BD8E4A04566147028B62E7C (void);
// 0x00000328 System.Void NPOI.HSSF.Record.Aggregates.SharedValueManager_SharedFormulaGroupComparator::.ctor()
extern void SharedFormulaGroupComparator__ctor_mCA2B5956D60B011B0BBC5D4F7D8CDB66C371059D (void);
// 0x00000329 System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::.ctor()
extern void ValueRecordsAggregate__ctor_m4A6E50DCF119EFB4316F8A5D28EBBA2C867C4E08 (void);
// 0x0000032A System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::.ctor(System.Int32,System.Int32,NPOI.HSSF.Record.CellValueRecordInterface[][])
extern void ValueRecordsAggregate__ctor_m23EF022BCC8BC55BAD0156907B635F46A5E25E8D (void);
// 0x0000032B System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::InsertCell(NPOI.HSSF.Record.CellValueRecordInterface)
extern void ValueRecordsAggregate_InsertCell_mB46C0B4DC6BEF6A09913AFFACB92060F31308B7D (void);
// 0x0000032C System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::RemoveCell(NPOI.HSSF.Record.CellValueRecordInterface)
extern void ValueRecordsAggregate_RemoveCell_mF2FA8760BDBF9EE9F50DB3553FF6215F62015F57 (void);
// 0x0000032D System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::RemoveAllCellsValuesForRow(System.Int32)
extern void ValueRecordsAggregate_RemoveAllCellsValuesForRow_mCED3E92368EE96A0E6C8A516DE11B2E17E0CC85C (void);
// 0x0000032E NPOI.HSSF.Record.CellValueRecordInterface[] NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::GetValueRecords()
extern void ValueRecordsAggregate_GetValueRecords_m9860BA578F7408938506725B221D44A75FA77CDA (void);
// 0x0000032F System.Int32 NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::get_FirstCellNum()
extern void ValueRecordsAggregate_get_FirstCellNum_m50BCF2F466E183DAC78296DD9273267F12B474AC (void);
// 0x00000330 System.Int32 NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::get_LastCellNum()
extern void ValueRecordsAggregate_get_LastCellNum_mA99F71375380DFF3B60AB3156F79D757254551F8 (void);
// 0x00000331 System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::AddMultipleBlanks(NPOI.HSSF.Record.MulBlankRecord)
extern void ValueRecordsAggregate_AddMultipleBlanks_mBC6A16D7D1DEB428D1C51DB2B2C20F27AB0915D6 (void);
// 0x00000332 NPOI.HSSF.Record.MulBlankRecord NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::CreateMBR(NPOI.HSSF.Record.CellValueRecordInterface[],System.Int32,System.Int32)
extern void ValueRecordsAggregate_CreateMBR_mDD9AA044A376F26C6F33D7078C3541BC479DEEED (void);
// 0x00000333 System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::Construct(NPOI.HSSF.Record.CellValueRecordInterface,NPOI.HSSF.Model.RecordStream,NPOI.HSSF.Record.Aggregates.SharedValueManager)
extern void ValueRecordsAggregate_Construct_m1F7CE0702E3B58A704B9C790FBE28BAEF6EAB86A (void);
// 0x00000334 System.Int32 NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::GetRowCellBlockSize(System.Int32,System.Int32)
extern void ValueRecordsAggregate_GetRowCellBlockSize_m4F5AC60AB88007E49D85BD1B4D7949C19877ADDA (void);
// 0x00000335 System.Boolean NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::RowHasCells(System.Int32)
extern void ValueRecordsAggregate_RowHasCells_mA6780B5A5A8D4783FC5164F1519EB49F8F3477BF (void);
// 0x00000336 System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::VisitCellsForRow(System.Int32,NPOI.HSSF.Record.Aggregates.RecordVisitor)
extern void ValueRecordsAggregate_VisitCellsForRow_m147787A6A280BB3261D1B5520F98BEDEE387F847 (void);
// 0x00000337 System.Int32 NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate::CountBlanks(NPOI.HSSF.Record.CellValueRecordInterface[],System.Int32)
extern void ValueRecordsAggregate_CountBlanks_m89CA8A2BAB20966E37029F99F49361AFABEC1FBC (void);
// 0x00000338 System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate_MyEnumerator::.ctor(NPOI.HSSF.Record.CellValueRecordInterface[][]&,System.Int32,System.Int32)
extern void MyEnumerator__ctor_m82FB1D28FDE504FB67D3AAF7042053B4D9A1D8D8 (void);
// 0x00000339 System.Boolean NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate_MyEnumerator::MoveNext()
extern void MyEnumerator_MoveNext_mF19A2735748CE8EF0F01F42C2CB3DD697EB0BB0A (void);
// 0x0000033A System.Object NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate_MyEnumerator::get_Current()
extern void MyEnumerator_get_Current_mEB10713DDFCC2D24644BD4D2B0AFB0796705BB97 (void);
// 0x0000033B System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate_MyEnumerator::FindNext()
extern void MyEnumerator_FindNext_m1F30F719E0CE513219F88CA79E76834621292CA1 (void);
// 0x0000033C System.Void NPOI.HSSF.Record.Aggregates.ValueRecordsAggregate_MyEnumerator::Reset()
extern void MyEnumerator_Reset_mFD84D7E712CA8B6E549D5A6DBB50344CBD3A2C2C (void);
// 0x0000033D NPOI.SS.Util.CellRangeAddress8Bit NPOI.HSSF.Record.SharedValueRecordBase::get_Range()
extern void SharedValueRecordBase_get_Range_mF46DAE7D4FA2F1B3723FD71635C9BCD35D91DC19 (void);
// 0x0000033E System.Int32 NPOI.HSSF.Record.SharedValueRecordBase::get_FirstRow()
extern void SharedValueRecordBase_get_FirstRow_m2C5046AA7BB4E296230D707663FB6E2E6433C814 (void);
// 0x0000033F System.Int32 NPOI.HSSF.Record.SharedValueRecordBase::get_LastRow()
extern void SharedValueRecordBase_get_LastRow_m907CE817167D90288FF97373EE2C6CF5696BE306 (void);
// 0x00000340 System.Int32 NPOI.HSSF.Record.SharedValueRecordBase::get_FirstColumn()
extern void SharedValueRecordBase_get_FirstColumn_m3E272FEA900C2C0D2FC17E7FD59A4D42E2DEEEE6 (void);
// 0x00000341 System.Int32 NPOI.HSSF.Record.SharedValueRecordBase::get_LastColumn()
extern void SharedValueRecordBase_get_LastColumn_m6EB6C9CEDE811566FA40F86549C50CBEF44BC252 (void);
// 0x00000342 System.Int32 NPOI.HSSF.Record.SharedValueRecordBase::get_DataSize()
extern void SharedValueRecordBase_get_DataSize_m248736F8D2E30B59DF8D276DBBAA5E6FA937CA98 (void);
// 0x00000343 System.Int32 NPOI.HSSF.Record.SharedValueRecordBase::get_ExtraDataSize()
// 0x00000344 System.Void NPOI.HSSF.Record.SharedValueRecordBase::SerializeExtraData(NPOI.Util.ILittleEndianOutput)
// 0x00000345 System.Void NPOI.HSSF.Record.SharedValueRecordBase::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SharedValueRecordBase_Serialize_mE3BE67ECF38BB542F9DCCC46059AC60F95C6413B (void);
// 0x00000346 System.Boolean NPOI.HSSF.Record.SharedValueRecordBase::IsInRange(System.Int32,System.Int32)
extern void SharedValueRecordBase_IsInRange_m57D088A6634C704A820D64304E553A4817064C5D (void);
// 0x00000347 System.Boolean NPOI.HSSF.Record.SharedValueRecordBase::IsFirstCell(System.Int32,System.Int32)
extern void SharedValueRecordBase_IsFirstCell_mE1552663312516A04540FD184DB2D1023AE52C57 (void);
// 0x00000348 NPOI.SS.Formula.PTG.Ptg[] NPOI.HSSF.Record.ArrayRecord::get_FormulaTokens()
extern void ArrayRecord_get_FormulaTokens_mAF016A6AAFFBCA2DC9D193F8E92C5613F5A38837 (void);
// 0x00000349 System.Int32 NPOI.HSSF.Record.ArrayRecord::get_ExtraDataSize()
extern void ArrayRecord_get_ExtraDataSize_m5D44BCC89B6AC88D52C7C815CA7F2CB18CCABF4D (void);
// 0x0000034A System.Void NPOI.HSSF.Record.ArrayRecord::SerializeExtraData(NPOI.Util.ILittleEndianOutput)
extern void ArrayRecord_SerializeExtraData_m68D8C07CFC57EF69EFBD724067809478235632A9 (void);
// 0x0000034B System.Int16 NPOI.HSSF.Record.ArrayRecord::get_Sid()
extern void ArrayRecord_get_Sid_m18A24AA0860D66C67A3971EE9BA164276A7927F2 (void);
// 0x0000034C System.Int16 NPOI.HSSF.Record.AutoFilter.AutoFilterInfoRecord::get_Sid()
extern void AutoFilterInfoRecord_get_Sid_m8713E726C540BF0B414E9056B49DA42BA8EDC052 (void);
// 0x0000034D System.Int32 NPOI.HSSF.Record.AutoFilter.AutoFilterInfoRecord::get_DataSize()
extern void AutoFilterInfoRecord_get_DataSize_m411F54EB20918C87388388DFA6B5F3AD4F3330BD (void);
// 0x0000034E System.Void NPOI.HSSF.Record.AutoFilter.AutoFilterInfoRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AutoFilterInfoRecord_Serialize_mE3C53A5AFA1C390D645909C8F7EC5B4551335554 (void);
// 0x0000034F System.Int32 NPOI.HSSF.Record.AutoFilter.DOPERRecord::get_RecordSize()
extern void DOPERRecord_get_RecordSize_m8238250FC5C414B6590C5389C1A7A17F30E21360 (void);
// 0x00000350 System.Int32 NPOI.HSSF.Record.AutoFilter.DOPERRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DOPERRecord_Serialize_mC55E70F9EA6728AE956D7243C115A508916ACF4A (void);
// 0x00000351 System.Int32 NPOI.HSSF.Record.AutoFilter.DOPERRecord::Serialize(System.Int32,System.Byte[])
extern void DOPERRecord_Serialize_m4E516D30D8E208A2EF47620166829C7CFD311DF6 (void);
// 0x00000352 System.Byte NPOI.HSSF.Record.AutoFilter.DOPERRecord::get_LengthOfString()
extern void DOPERRecord_get_LengthOfString_m38B9B0B3FCBC5E83374A8BCFCE4C30F8208B3AB2 (void);
// 0x00000353 System.Void NPOI.HSSF.Record.BackupRecord::.ctor()
extern void BackupRecord__ctor_m7252B3BBE153130131717864795AF206F0A7CC70 (void);
// 0x00000354 System.Int16 NPOI.HSSF.Record.BackupRecord::get_Backup()
extern void BackupRecord_get_Backup_mFEE323D63CF4846084E06CAA1C0BCBADE5B7A471 (void);
// 0x00000355 System.Void NPOI.HSSF.Record.BackupRecord::set_Backup(System.Int16)
extern void BackupRecord_set_Backup_m349C09B6B64C5DF92B6E5389E115F5071901DBAE (void);
// 0x00000356 System.String NPOI.HSSF.Record.BackupRecord::ToString()
extern void BackupRecord_ToString_m8461B405D9B98CD1D0E2822DFE7CA1E04A1143AF (void);
// 0x00000357 System.Void NPOI.HSSF.Record.BackupRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BackupRecord_Serialize_mDD40A8A1122308FC61CF68CD73C895097A81D87E (void);
// 0x00000358 System.Int32 NPOI.HSSF.Record.BackupRecord::get_DataSize()
extern void BackupRecord_get_DataSize_m3F575A3011C7ACF60D6AD0779832E9DEF958B1CA (void);
// 0x00000359 System.Int16 NPOI.HSSF.Record.BackupRecord::get_Sid()
extern void BackupRecord_get_Sid_m42A175EC4C08349BB2A6211EE314DFE7179BB277 (void);
// 0x0000035A System.Int32 NPOI.HSSF.Record.BiffHeaderInput::ReadRecordSID()
// 0x0000035B System.Int32 NPOI.HSSF.Record.BiffHeaderInput::ReadDataSize()
// 0x0000035C System.Int32 NPOI.HSSF.Record.BiffHeaderInput::Available()
// 0x0000035D System.Void NPOI.HSSF.Record.BlankRecord::.ctor()
extern void BlankRecord__ctor_m7FB8C49E697380477AED4E53414AE6E2818FDF34 (void);
// 0x0000035E System.Int32 NPOI.HSSF.Record.BlankRecord::get_Row()
extern void BlankRecord_get_Row_m61506427DD019A2873ADF6A0D96151EDE8D40F78 (void);
// 0x0000035F System.Void NPOI.HSSF.Record.BlankRecord::set_Row(System.Int32)
extern void BlankRecord_set_Row_m1571613A46177E9CCC22638BA5D63FCFBB146EB6 (void);
// 0x00000360 System.Int32 NPOI.HSSF.Record.BlankRecord::get_Column()
extern void BlankRecord_get_Column_m58C279D78D58EC3CDCF87FC37072BF4ED69E66B3 (void);
// 0x00000361 System.Void NPOI.HSSF.Record.BlankRecord::set_Column(System.Int32)
extern void BlankRecord_set_Column_mAA43FFC11E5A462699BDB68CE4F0901878E2F1BE (void);
// 0x00000362 System.Void NPOI.HSSF.Record.BlankRecord::set_XFIndex(System.Int16)
extern void BlankRecord_set_XFIndex_m4303E8568BCAD37A2D6C8E2C68105C1088E8CF05 (void);
// 0x00000363 System.Int16 NPOI.HSSF.Record.BlankRecord::get_XFIndex()
extern void BlankRecord_get_XFIndex_m59412B3F7721C2B73B34CC872CB4AC2710276F99 (void);
// 0x00000364 System.Int16 NPOI.HSSF.Record.BlankRecord::get_Sid()
extern void BlankRecord_get_Sid_m902598D4BB9AC0594DDA5DB2D6F2B3D02F6A5166 (void);
// 0x00000365 System.String NPOI.HSSF.Record.BlankRecord::ToString()
extern void BlankRecord_ToString_m76927070847FD5FD9DDF1F5FF472C3CA7F88B8E0 (void);
// 0x00000366 System.Int32 NPOI.HSSF.Record.BlankRecord::get_DataSize()
extern void BlankRecord_get_DataSize_m6C462051526EF9A06BBDF1FDE249F737CA229012 (void);
// 0x00000367 System.Void NPOI.HSSF.Record.BlankRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BlankRecord_Serialize_m761584383E7A4444613095B8A0899DB0763C04AF (void);
// 0x00000368 System.Int32 NPOI.HSSF.Record.BlankRecord::CompareTo(System.Object)
extern void BlankRecord_CompareTo_mF2E6130D55F2E54287D235C08183F75F2795972D (void);
// 0x00000369 System.Object NPOI.HSSF.Record.BlankRecord::Clone()
extern void BlankRecord_Clone_m8BC5EF827D712CE7E26DEEFC57583CC585638DC6 (void);
// 0x0000036A System.Void NPOI.HSSF.Record.BOFRecord::.ctor()
extern void BOFRecord__ctor_m8D6F00C6E53BE4F6405F20592ACD168E49E66FB2 (void);
// 0x0000036B System.Void NPOI.HSSF.Record.BOFRecord::set_Version(System.Int32)
extern void BOFRecord_set_Version_mEF8B530F1FDECC980332ED3BD717B853A5507359 (void);
// 0x0000036C System.Int32 NPOI.HSSF.Record.BOFRecord::get_Version()
extern void BOFRecord_get_Version_m2871AAC4493A07534997E1EDC4C19B197847284F (void);
// 0x0000036D System.Void NPOI.HSSF.Record.BOFRecord::set_HistoryBitMask(System.Int32)
extern void BOFRecord_set_HistoryBitMask_m562BA7155D37F4EDEA343F501AEEC769B05E6602 (void);
// 0x0000036E System.Int32 NPOI.HSSF.Record.BOFRecord::get_HistoryBitMask()
extern void BOFRecord_get_HistoryBitMask_m27A6D9F3C7E00D0FC05282B82A7763D4D7AE55D1 (void);
// 0x0000036F System.Void NPOI.HSSF.Record.BOFRecord::set_RequiredVersion(System.Int32)
extern void BOFRecord_set_RequiredVersion_m8014E704F27493A107C47CA01BCFF1D6FC321105 (void);
// 0x00000370 System.Int32 NPOI.HSSF.Record.BOFRecord::get_RequiredVersion()
extern void BOFRecord_get_RequiredVersion_mBA104D778F80BB9C9ACDC18F7E13A6389157CA54 (void);
// 0x00000371 NPOI.HSSF.Record.BOFRecordType NPOI.HSSF.Record.BOFRecord::get_Type()
extern void BOFRecord_get_Type_m098CEBE167D29BFEE2C976EE102EE4465A6A4497 (void);
// 0x00000372 System.Void NPOI.HSSF.Record.BOFRecord::set_Type(NPOI.HSSF.Record.BOFRecordType)
extern void BOFRecord_set_Type_m00979233FDC297894AF7CCCCB3B8B2B2C40319DA (void);
// 0x00000373 System.String NPOI.HSSF.Record.BOFRecord::get_TypeName()
extern void BOFRecord_get_TypeName_m24D7ED37FD52E9C2EF7D5BE830B985889D721CDB (void);
// 0x00000374 System.Int32 NPOI.HSSF.Record.BOFRecord::get_Build()
extern void BOFRecord_get_Build_mDFCD87A5F0F85D145631D666CFBB72A1BBA31269 (void);
// 0x00000375 System.Void NPOI.HSSF.Record.BOFRecord::set_Build(System.Int32)
extern void BOFRecord_set_Build_m012B3D526A36C24FB1340DA63FDB3C013369E278 (void);
// 0x00000376 System.Int32 NPOI.HSSF.Record.BOFRecord::get_BuildYear()
extern void BOFRecord_get_BuildYear_m04E739CD78C516102E1C01DDB3D91D97D1496BF4 (void);
// 0x00000377 System.Void NPOI.HSSF.Record.BOFRecord::set_BuildYear(System.Int32)
extern void BOFRecord_set_BuildYear_mC4DBF9D3BAD80DC8C7A2BC99C422D9094FD9C2E9 (void);
// 0x00000378 System.String NPOI.HSSF.Record.BOFRecord::ToString()
extern void BOFRecord_ToString_m712544A602A66D6460FA5FABF732BD28561B3FA3 (void);
// 0x00000379 System.Void NPOI.HSSF.Record.BOFRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BOFRecord_Serialize_mBFA380558E921BFBB864679FE0877D5B6EFEB364 (void);
// 0x0000037A System.Int32 NPOI.HSSF.Record.BOFRecord::get_DataSize()
extern void BOFRecord_get_DataSize_mE346A37F67D14B3C4C2DC45A31E28F97C9720C54 (void);
// 0x0000037B System.Int16 NPOI.HSSF.Record.BOFRecord::get_Sid()
extern void BOFRecord_get_Sid_mAC962195037C12E80DC3260E40E6A7C184506F4B (void);
// 0x0000037C System.Object NPOI.HSSF.Record.BOFRecord::Clone()
extern void BOFRecord_Clone_m5174590C1C390FF812171B625C0075B60EC49371 (void);
// 0x0000037D System.Void NPOI.HSSF.Record.BookBoolRecord::.ctor()
extern void BookBoolRecord__ctor_m0764C28450E135B952B670B652818D8ED1C1928C (void);
// 0x0000037E System.Int16 NPOI.HSSF.Record.BookBoolRecord::get_SaveLinkValues()
extern void BookBoolRecord_get_SaveLinkValues_m43EA7A93D99DDFAB67EA8738A0467A722422524F (void);
// 0x0000037F System.Void NPOI.HSSF.Record.BookBoolRecord::set_SaveLinkValues(System.Int16)
extern void BookBoolRecord_set_SaveLinkValues_mA5AAA76293ECF343F0EA0BBEB9596F50E6E90434 (void);
// 0x00000380 System.String NPOI.HSSF.Record.BookBoolRecord::ToString()
extern void BookBoolRecord_ToString_m239435F13779504E773A9C67313A0BB844C0644D (void);
// 0x00000381 System.Void NPOI.HSSF.Record.BookBoolRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BookBoolRecord_Serialize_mF19C15635CAB01BB05846BCBD7BB3E5ACE2B8BCB (void);
// 0x00000382 System.Int32 NPOI.HSSF.Record.BookBoolRecord::get_DataSize()
extern void BookBoolRecord_get_DataSize_m2B1D74F92B5B2BB6473BF9D0744B47CB775470CF (void);
// 0x00000383 System.Int16 NPOI.HSSF.Record.BookBoolRecord::get_Sid()
extern void BookBoolRecord_get_Sid_m914CA8D15F34A6E469DFDB06041BD9FF7E245794 (void);
// 0x00000384 System.Void NPOI.HSSF.Record.CellRecord::.ctor()
extern void CellRecord__ctor_m8C991268792B29DCBEDA5FCF8900D34DC397D5B4 (void);
// 0x00000385 System.Int32 NPOI.HSSF.Record.CellRecord::get_Row()
extern void CellRecord_get_Row_m7DCA253871EA235DCD1C06234634C895F8D52155 (void);
// 0x00000386 System.Void NPOI.HSSF.Record.CellRecord::set_Row(System.Int32)
extern void CellRecord_set_Row_mA32BC49742240FF58C7279BCBB903B33BF364503 (void);
// 0x00000387 System.Int32 NPOI.HSSF.Record.CellRecord::get_Column()
extern void CellRecord_get_Column_m87EB8724FCC7E346B7D84C3810BC6C6F775278C9 (void);
// 0x00000388 System.Void NPOI.HSSF.Record.CellRecord::set_Column(System.Int32)
extern void CellRecord_set_Column_m5A2BF9213747C07519F1144D2F64921CD3A69E36 (void);
// 0x00000389 System.Int16 NPOI.HSSF.Record.CellRecord::get_XFIndex()
extern void CellRecord_get_XFIndex_mE32C7C444FE2C857F634B5E3144DF70160E2BEE7 (void);
// 0x0000038A System.Void NPOI.HSSF.Record.CellRecord::set_XFIndex(System.Int16)
extern void CellRecord_set_XFIndex_m8323E6461C004832E1C3B855DE5FC675CD37B16D (void);
// 0x0000038B System.Int32 NPOI.HSSF.Record.CellRecord::CompareTo(System.Object)
extern void CellRecord_CompareTo_m43C7BE5A165CC80AAEB6741260B301438DF6EF01 (void);
// 0x0000038C System.String NPOI.HSSF.Record.CellRecord::ToString()
extern void CellRecord_ToString_m12B64CA72167B58DF8F23BBD6D5910E30EFC7495 (void);
// 0x0000038D System.Void NPOI.HSSF.Record.CellRecord::AppendValueText(System.Text.StringBuilder)
// 0x0000038E System.String NPOI.HSSF.Record.CellRecord::get_RecordName()
// 0x0000038F System.Void NPOI.HSSF.Record.CellRecord::SerializeValue(NPOI.Util.ILittleEndianOutput)
// 0x00000390 System.Int32 NPOI.HSSF.Record.CellRecord::get_ValueDataSize()
// 0x00000391 System.Void NPOI.HSSF.Record.CellRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CellRecord_Serialize_mCC3D7826924D9F63BBF22E2AE5E34CCD27925514 (void);
// 0x00000392 System.Int32 NPOI.HSSF.Record.CellRecord::get_DataSize()
extern void CellRecord_get_DataSize_mA44A3521D0AEAC94EE07A12F1B2DBD6ED38DFCE3 (void);
// 0x00000393 System.Void NPOI.HSSF.Record.CellRecord::CopyBaseFields(NPOI.HSSF.Record.CellRecord)
extern void CellRecord_CopyBaseFields_m9687C55AC1E173C6BB8F41AA392D61A802E42D5C (void);
// 0x00000394 System.Boolean NPOI.HSSF.Record.CellRecord::Equals(System.Object)
extern void CellRecord_Equals_m77E15913EC33CD06655F1AFAE389C7E5F70319FB (void);
// 0x00000395 System.Int32 NPOI.HSSF.Record.CellRecord::GetHashCode()
extern void CellRecord_GetHashCode_mEE5AC1DB7C9A239DC0FA528FDE845A5DCC8388E4 (void);
// 0x00000396 System.Void NPOI.HSSF.Record.BoolErrRecord::.ctor()
extern void BoolErrRecord__ctor_m2708A67D20122551E49CBCF1085E5BE7D289F429 (void);
// 0x00000397 System.Void NPOI.HSSF.Record.BoolErrRecord::SetValue(System.Boolean)
extern void BoolErrRecord_SetValue_m5C980A3F3660DEF1E60C1ABF535D9C3DDC45ACFB (void);
// 0x00000398 System.Void NPOI.HSSF.Record.BoolErrRecord::SetValue(System.Byte)
extern void BoolErrRecord_SetValue_mE074F541F3E39B153C5532171276B5FFCF56EC6C (void);
// 0x00000399 System.Boolean NPOI.HSSF.Record.BoolErrRecord::get_BooleanValue()
extern void BoolErrRecord_get_BooleanValue_m5063548A5632D9D9E0E3696C15181D38F942AC88 (void);
// 0x0000039A System.Byte NPOI.HSSF.Record.BoolErrRecord::get_ErrorValue()
extern void BoolErrRecord_get_ErrorValue_m6E3BFEC17BC2E69E587FF009CC3564EB2102A1BD (void);
// 0x0000039B System.Boolean NPOI.HSSF.Record.BoolErrRecord::get_IsBoolean()
extern void BoolErrRecord_get_IsBoolean_m84580A8B0001A7850543E172B5C48EB6B445EDB3 (void);
// 0x0000039C System.String NPOI.HSSF.Record.BoolErrRecord::get_RecordName()
extern void BoolErrRecord_get_RecordName_mB1B12870D7AE500A472195EF59147F45AC42CB90 (void);
// 0x0000039D System.Void NPOI.HSSF.Record.BoolErrRecord::AppendValueText(System.Text.StringBuilder)
extern void BoolErrRecord_AppendValueText_mB0F36E3982E1B7A5545F44F3B12C647786A41410 (void);
// 0x0000039E System.Void NPOI.HSSF.Record.BoolErrRecord::SerializeValue(NPOI.Util.ILittleEndianOutput)
extern void BoolErrRecord_SerializeValue_m7B8C8FCA16A764900F1F43EB20EB5A7B9CA3AF0B (void);
// 0x0000039F System.Int32 NPOI.HSSF.Record.BoolErrRecord::get_ValueDataSize()
extern void BoolErrRecord_get_ValueDataSize_m6520157E15C77A7F163A562BE8654491F3F21B04 (void);
// 0x000003A0 System.Int16 NPOI.HSSF.Record.BoolErrRecord::get_Sid()
extern void BoolErrRecord_get_Sid_mA844C81C406AE5D894C515271E39796D3AA0C57A (void);
// 0x000003A1 System.Object NPOI.HSSF.Record.BoolErrRecord::Clone()
extern void BoolErrRecord_Clone_m5B417E025AF1B56ADB91E17F607140021A091692 (void);
// 0x000003A2 System.Void NPOI.HSSF.Record.BottomMarginRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BottomMarginRecord_Serialize_m910B0F5E98171112D63581778AE705A26841555C (void);
// 0x000003A3 System.Int32 NPOI.HSSF.Record.BottomMarginRecord::get_DataSize()
extern void BottomMarginRecord_get_DataSize_mC05D22C10F075BBBCBC81ADFEAF1B3C11EBCBB27 (void);
// 0x000003A4 System.Int16 NPOI.HSSF.Record.BottomMarginRecord::get_Sid()
extern void BottomMarginRecord_get_Sid_mFC7814B561263E049EE84A938D8CFAC909B7B8E4 (void);
// 0x000003A5 System.Void NPOI.HSSF.Record.BoundSheetRecord::.ctor(System.String)
extern void BoundSheetRecord__ctor_m653B89501204A16618F813ADA3D2BD88743D439A (void);
// 0x000003A6 System.Int32 NPOI.HSSF.Record.BoundSheetRecord::get_PositionOfBof()
extern void BoundSheetRecord_get_PositionOfBof_mCEE1DF4347454AE590DF3E0DF0C0F42FFD286C73 (void);
// 0x000003A7 System.Void NPOI.HSSF.Record.BoundSheetRecord::set_PositionOfBof(System.Int32)
extern void BoundSheetRecord_set_PositionOfBof_m6DDD4966C7C763D2EA63A79A06DBEACB20A218E9 (void);
// 0x000003A8 System.String NPOI.HSSF.Record.BoundSheetRecord::get_Sheetname()
extern void BoundSheetRecord_get_Sheetname_m8E52BE9A21EDAF2FEC81BF5AB2B2623EF0C7A767 (void);
// 0x000003A9 System.Void NPOI.HSSF.Record.BoundSheetRecord::set_Sheetname(System.String)
extern void BoundSheetRecord_set_Sheetname_m696A7C93E443168ABDF676E3E347746E0DD791A0 (void);
// 0x000003AA System.Boolean NPOI.HSSF.Record.BoundSheetRecord::get_IsMultibyte()
extern void BoundSheetRecord_get_IsMultibyte_mFE11E2CE5A25F27A321E2580683C1987048B4176 (void);
// 0x000003AB System.String NPOI.HSSF.Record.BoundSheetRecord::ToString()
extern void BoundSheetRecord_ToString_m63DB42B1950ECBB6EE2AAF1E99120D0C964138AA (void);
// 0x000003AC System.Int32 NPOI.HSSF.Record.BoundSheetRecord::get_DataSize()
extern void BoundSheetRecord_get_DataSize_m5974530D02066BC8760AF549ECE1C8B2201B5E09 (void);
// 0x000003AD System.Void NPOI.HSSF.Record.BoundSheetRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BoundSheetRecord_Serialize_mAF05F517CA9F3A4E234B76E1D7793DDBD404B001 (void);
// 0x000003AE System.Int16 NPOI.HSSF.Record.BoundSheetRecord::get_Sid()
extern void BoundSheetRecord_get_Sid_m2F9903E6902C969C5D90834B02784B46C7570E51 (void);
// 0x000003AF System.Void NPOI.HSSF.Record.BoundSheetRecord::.cctor()
extern void BoundSheetRecord__cctor_m4B4F13BCEBA38A8A7D66E226348FB1663A0CCA82 (void);
// 0x000003B0 System.Void NPOI.HSSF.Record.CalcCountRecord::.ctor()
extern void CalcCountRecord__ctor_m78C6FB4504B70C37197DB7BCDEE0DE2AA4DC762B (void);
// 0x000003B1 System.Int16 NPOI.HSSF.Record.CalcCountRecord::get_Iterations()
extern void CalcCountRecord_get_Iterations_mD26327DBE1081A331833ACC67094EF9885084688 (void);
// 0x000003B2 System.Void NPOI.HSSF.Record.CalcCountRecord::set_Iterations(System.Int16)
extern void CalcCountRecord_set_Iterations_m4461B6D7C862AB67E03A3CABB39344C7DAADAE4B (void);
// 0x000003B3 System.String NPOI.HSSF.Record.CalcCountRecord::ToString()
extern void CalcCountRecord_ToString_m90C0E4C5771EB2B30237BD8440B074FEAE489272 (void);
// 0x000003B4 System.Void NPOI.HSSF.Record.CalcCountRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CalcCountRecord_Serialize_mAD02BD1F9F6159D5052ABCED03FBDBA51FA374D1 (void);
// 0x000003B5 System.Int32 NPOI.HSSF.Record.CalcCountRecord::get_DataSize()
extern void CalcCountRecord_get_DataSize_mBE42BF2B98D6678E5FD6DCF6C48D7A426CF17902 (void);
// 0x000003B6 System.Int16 NPOI.HSSF.Record.CalcCountRecord::get_Sid()
extern void CalcCountRecord_get_Sid_m9DE8A940DEE2BD233E360F102C4C1FF293146D6D (void);
// 0x000003B7 System.Object NPOI.HSSF.Record.CalcCountRecord::Clone()
extern void CalcCountRecord_Clone_m3DEB0F8EF5CBA8E0D60541370D50FC64392A0383 (void);
// 0x000003B8 System.Void NPOI.HSSF.Record.CalcModeRecord::.ctor()
extern void CalcModeRecord__ctor_m5DD9BF57300E1A53966BE5F6BF5CD42A6ADA60BA (void);
// 0x000003B9 System.Void NPOI.HSSF.Record.CalcModeRecord::SetCalcMode(System.Int16)
extern void CalcModeRecord_SetCalcMode_m848896B5D402B9582F44F92E176F4957D61860E1 (void);
// 0x000003BA System.Int16 NPOI.HSSF.Record.CalcModeRecord::GetCalcMode()
extern void CalcModeRecord_GetCalcMode_m559787B5B123A08AA3EF277969F955B9409CCEFD (void);
// 0x000003BB System.String NPOI.HSSF.Record.CalcModeRecord::ToString()
extern void CalcModeRecord_ToString_mE6639A76E3F4962E550573985F1EA6B0F9194CE4 (void);
// 0x000003BC System.Void NPOI.HSSF.Record.CalcModeRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CalcModeRecord_Serialize_mD9500B523BCA152512E2D2D8761C2AD8E2E32131 (void);
// 0x000003BD System.Int32 NPOI.HSSF.Record.CalcModeRecord::get_DataSize()
extern void CalcModeRecord_get_DataSize_mDA3F6465CF390831DB27A66B691038F272EB8036 (void);
// 0x000003BE System.Int16 NPOI.HSSF.Record.CalcModeRecord::get_Sid()
extern void CalcModeRecord_get_Sid_mF6E1D1EF9BD3319497AFF71565942E28E18A8BD9 (void);
// 0x000003BF System.Object NPOI.HSSF.Record.CalcModeRecord::Clone()
extern void CalcModeRecord_Clone_m241DF179E860669F8BF703B1FBE7D0FF0935B7B1 (void);
// 0x000003C0 System.Int32 NPOI.HSSF.Record.CFHeaderRecord::get_NumberOfConditionalFormats()
extern void CFHeaderRecord_get_NumberOfConditionalFormats_m52C0C53B8033D9DC4A2ABE3BD334E47995E2EB2C (void);
// 0x000003C1 System.Void NPOI.HSSF.Record.CFHeaderRecord::set_NumberOfConditionalFormats(System.Int32)
extern void CFHeaderRecord_set_NumberOfConditionalFormats_mF00ACEA4DAA21EAC539DBBB4FCF450161E2AE49C (void);
// 0x000003C2 System.Int32 NPOI.HSSF.Record.CFHeaderRecord::get_DataSize()
extern void CFHeaderRecord_get_DataSize_m10EEB66527F6D19240BCF53E6F1D27E8F8CE8A0A (void);
// 0x000003C3 System.Void NPOI.HSSF.Record.CFHeaderRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CFHeaderRecord_Serialize_m5CBF83445594314268D8042F97975F72A6E77CFA (void);
// 0x000003C4 System.Int16 NPOI.HSSF.Record.CFHeaderRecord::get_Sid()
extern void CFHeaderRecord_get_Sid_m31309EBA41B3B334E116B726451A20F7CE0C2988 (void);
// 0x000003C5 NPOI.Util.BitField NPOI.HSSF.Record.CFRuleRecord::bf(System.Int32)
extern void CFRuleRecord_bf_m2195DED64814F0725B546E463D109505EED2CF25 (void);
// 0x000003C6 System.Boolean NPOI.HSSF.Record.CFRuleRecord::get_ContainsFontFormattingBlock()
extern void CFRuleRecord_get_ContainsFontFormattingBlock_mB5BBB760E00FA05C2B135638B307C569B84FAE38 (void);
// 0x000003C7 System.Boolean NPOI.HSSF.Record.CFRuleRecord::get_ContainsBorderFormattingBlock()
extern void CFRuleRecord_get_ContainsBorderFormattingBlock_m13B3E486D15FDD4FB0216BB690BFD624337BD592 (void);
// 0x000003C8 System.Boolean NPOI.HSSF.Record.CFRuleRecord::get_ContainsPatternFormattingBlock()
extern void CFRuleRecord_get_ContainsPatternFormattingBlock_mEFF9D4053245509E71C213011A6B8F500F7DBB08 (void);
// 0x000003C9 System.Boolean NPOI.HSSF.Record.CFRuleRecord::GetOptionFlag(NPOI.Util.BitField)
extern void CFRuleRecord_GetOptionFlag_m73429DC8C54024B82EC2C1B3D8EA42A4BFF697C8 (void);
// 0x000003CA System.Int16 NPOI.HSSF.Record.CFRuleRecord::get_Sid()
extern void CFRuleRecord_get_Sid_mF5550C8FFCC632CEA66A111E52A0C9A4FB7F0FE0 (void);
// 0x000003CB System.Int32 NPOI.HSSF.Record.CFRuleRecord::GetFormulaSize(NPOI.SS.Formula.Formula)
extern void CFRuleRecord_GetFormulaSize_mCB7B7FAA363C19A4040F51DA1E2AF5D838B97F9D (void);
// 0x000003CC System.Void NPOI.HSSF.Record.CFRuleRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CFRuleRecord_Serialize_mE317818A9DB4EA3BFE3771B3A2DD32F4F424D302 (void);
// 0x000003CD System.Int32 NPOI.HSSF.Record.CFRuleRecord::get_DataSize()
extern void CFRuleRecord_get_DataSize_m901FB5CE6AB827E6CC1A4F1ADFFEEE45F91975F6 (void);
// 0x000003CE System.Void NPOI.HSSF.Record.CFRuleRecord::.cctor()
extern void CFRuleRecord__cctor_m3B2F684EAE1D5D66D01DF9B81DE44C10872E616C (void);
// 0x000003CF System.Void NPOI.HSSF.Record.CF.BorderFormatting::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BorderFormatting_Serialize_m7FF7C52620C767CDBBC06C1A0AEA46CD0895F178 (void);
// 0x000003D0 System.Void NPOI.HSSF.Record.CF.BorderFormatting::.cctor()
extern void BorderFormatting__cctor_mD9081A5CB50E3B2C9755503F4CBBA9EC09C038EE (void);
// 0x000003D1 System.Byte[] NPOI.HSSF.Record.CF.FontFormatting::GetRawRecord()
extern void FontFormatting_GetRawRecord_m7D85C819AD2346741ED1936B3B2268C433C50536 (void);
// 0x000003D2 System.Void NPOI.HSSF.Record.CF.FontFormatting::.cctor()
extern void FontFormatting__cctor_m207722B9B3314F124C877D240B9F5DB6ABD70A95 (void);
// 0x000003D3 System.Void NPOI.HSSF.Record.CF.PatternFormatting::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PatternFormatting_Serialize_mB921A1CC93764EBCD94C2467C4232FB588A10190 (void);
// 0x000003D4 System.Void NPOI.HSSF.Record.CF.PatternFormatting::.cctor()
extern void PatternFormatting__cctor_mF0A14A3188996E3778237DAA71791274BF38692D (void);
// 0x000003D5 System.Void NPOI.HSSF.Record.Chart.AreaFormatRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AreaFormatRecord_Serialize_m2DA17FB5236EB68C5D74698629EFC66088EC579E (void);
// 0x000003D6 System.Int32 NPOI.HSSF.Record.Chart.AreaFormatRecord::get_DataSize()
extern void AreaFormatRecord_get_DataSize_mF898E10353694CB2741C4F02E9CA32E48F824CA2 (void);
// 0x000003D7 System.Int16 NPOI.HSSF.Record.Chart.AreaFormatRecord::get_Sid()
extern void AreaFormatRecord_get_Sid_mC9591E15E1564769169995AE719C176EDFFA495A (void);
// 0x000003D8 System.Void NPOI.HSSF.Record.Chart.AreaRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AreaRecord_Serialize_mE9F16F31DCD7D0768E6B9CB1866CA92C3C4C256E (void);
// 0x000003D9 System.Int32 NPOI.HSSF.Record.Chart.AreaRecord::get_DataSize()
extern void AreaRecord_get_DataSize_m0A4D181F7A1BE2CE2F09E5A09DB67515400ABD66 (void);
// 0x000003DA System.Int16 NPOI.HSSF.Record.Chart.AreaRecord::get_Sid()
extern void AreaRecord_get_Sid_m48530A82FA9A208815819EBE23529550E755AC3E (void);
// 0x000003DB System.Void NPOI.HSSF.Record.Chart.AxisParentRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AxisParentRecord_Serialize_mAD8D449370A398D50E7A113C2786E7CD8E46F44B (void);
// 0x000003DC System.Int32 NPOI.HSSF.Record.Chart.AxisParentRecord::get_DataSize()
extern void AxisParentRecord_get_DataSize_mEBAB4FA463F17282ABE99FFB5AD4BB4F2FA120FF (void);
// 0x000003DD System.Int16 NPOI.HSSF.Record.Chart.AxisParentRecord::get_Sid()
extern void AxisParentRecord_get_Sid_mBBC51138331974B9EE04871BF613D920C81673BB (void);
// 0x000003DE System.Void NPOI.HSSF.Record.Chart.AxisRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void AxisRecord_Serialize_mB9CA5052A143FFACE94BDCF80095E80E74A97ED1 (void);
// 0x000003DF System.Int32 NPOI.HSSF.Record.Chart.AxisRecord::get_DataSize()
extern void AxisRecord_get_DataSize_m370BFAE0769DD79F77ADF0420C9067ABADC100AD (void);
// 0x000003E0 System.Int16 NPOI.HSSF.Record.Chart.AxisRecord::get_Sid()
extern void AxisRecord_get_Sid_m23333D6A907C0E1741FA7B57959732D7E56DF6A4 (void);
// 0x000003E1 System.Void NPOI.HSSF.Record.Chart.BarRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BarRecord_Serialize_mA4F9C054787CEF35F62A9F9B47784C2630F2AFBC (void);
// 0x000003E2 System.Int32 NPOI.HSSF.Record.Chart.BarRecord::get_DataSize()
extern void BarRecord_get_DataSize_m0F4DBDC0CE4C1555820900AC404CA04C3BC80F6D (void);
// 0x000003E3 System.Int16 NPOI.HSSF.Record.Chart.BarRecord::get_Sid()
extern void BarRecord_get_Sid_m8B947528FF405C820C1628BD75E5624071AE4253 (void);
// 0x000003E4 System.Void NPOI.HSSF.Record.Chart.BeginRecord::.ctor()
extern void BeginRecord__ctor_m479DB3665602805FC5B7E92D69EA47FDA32EC9BD (void);
// 0x000003E5 System.String NPOI.HSSF.Record.Chart.BeginRecord::ToString()
extern void BeginRecord_ToString_m8E92CACCF7C61FA2233B42358A287EF1A2E13CD1 (void);
// 0x000003E6 System.Void NPOI.HSSF.Record.Chart.BeginRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void BeginRecord_Serialize_mFACC74F6D79663E33A88F311DF0F4FD714B08796 (void);
// 0x000003E7 System.Int32 NPOI.HSSF.Record.Chart.BeginRecord::get_DataSize()
extern void BeginRecord_get_DataSize_m29BA06405C86CF8C0149CA521283B8CD45950522 (void);
// 0x000003E8 System.Int16 NPOI.HSSF.Record.Chart.BeginRecord::get_Sid()
extern void BeginRecord_get_Sid_mBBE87AE98C11D13B7FA44BCE088057ECC515B502 (void);
// 0x000003E9 System.Object NPOI.HSSF.Record.Chart.BeginRecord::Clone()
extern void BeginRecord_Clone_mCB5A27C12B5FDA4319DC06D271B6145375E84307 (void);
// 0x000003EA System.Void NPOI.HSSF.Record.Chart.BeginRecord::.cctor()
extern void BeginRecord__cctor_mA5CC23B6FFE7208797B4D7D030252756962E6863 (void);
// 0x000003EB System.Int32 NPOI.HSSF.Record.Chart.CatLabRecord::get_DataSize()
extern void CatLabRecord_get_DataSize_mCFA431DA21092E47709706399D149D121FE5AEEF (void);
// 0x000003EC System.Int16 NPOI.HSSF.Record.Chart.CatLabRecord::get_Sid()
extern void CatLabRecord_get_Sid_m70DDC32B1E539181A82CEEC5C1B6B8C237A4C4FC (void);
// 0x000003ED System.Void NPOI.HSSF.Record.Chart.CatLabRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CatLabRecord_Serialize_m625DCE8078EFFF4033DC3DD020C10A5B0AC9491A (void);
// 0x000003EE System.Int32 NPOI.HSSF.Record.Chart.ChartEndObjectRecord::get_DataSize()
extern void ChartEndObjectRecord_get_DataSize_m6BD6B223A4675867137905C722D5F14654B3345F (void);
// 0x000003EF System.Int16 NPOI.HSSF.Record.Chart.ChartEndObjectRecord::get_Sid()
extern void ChartEndObjectRecord_get_Sid_m2E60798900C9339C1CD767DB89264218ACCD0222 (void);
// 0x000003F0 System.Void NPOI.HSSF.Record.Chart.ChartEndObjectRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ChartEndObjectRecord_Serialize_m6AB217B1530CF541D37474EAD03ADA534BBA0493 (void);
// 0x000003F1 System.Void NPOI.HSSF.Record.ChartFormatRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ChartFormatRecord_Serialize_m00FB610F1645B4159CD9A74C26DCF6B3DB00AA31 (void);
// 0x000003F2 System.Int32 NPOI.HSSF.Record.ChartFormatRecord::get_DataSize()
extern void ChartFormatRecord_get_DataSize_m63264F91A87E104DE791817E90B4C3166CE4DD58 (void);
// 0x000003F3 System.Int16 NPOI.HSSF.Record.ChartFormatRecord::get_Sid()
extern void ChartFormatRecord_get_Sid_mBCD15EE5C7B1B16AC1D86AC682DA1E2103ECF4A2 (void);
// 0x000003F4 System.Int32 NPOI.HSSF.Record.ChartFormatRecord::get_XPosition()
extern void ChartFormatRecord_get_XPosition_m28C1CA5B1CDA2E158E54FCB62DCF4F7F2B308E1E (void);
// 0x000003F5 System.Int32 NPOI.HSSF.Record.ChartFormatRecord::get_YPosition()
extern void ChartFormatRecord_get_YPosition_m784F9496C6A436DF9D8F4C1EFB7FF4F6A44ED3E7 (void);
// 0x000003F6 System.Int32 NPOI.HSSF.Record.ChartFormatRecord::get_Width()
extern void ChartFormatRecord_get_Width_m3A11BC87E943531E2F5DC21F84B5AE8CC3DF5CF4 (void);
// 0x000003F7 System.Int32 NPOI.HSSF.Record.ChartFormatRecord::get_Height()
extern void ChartFormatRecord_get_Height_mC5096C4547F1AAAA28BFB51306F7F9A594A92FF8 (void);
// 0x000003F8 System.Int32 NPOI.HSSF.Record.Chart.ChartFRTInfoRecord::get_DataSize()
extern void ChartFRTInfoRecord_get_DataSize_m639384421D0C8483B35B5E27F225586001BF2113 (void);
// 0x000003F9 System.Int16 NPOI.HSSF.Record.Chart.ChartFRTInfoRecord::get_Sid()
extern void ChartFRTInfoRecord_get_Sid_m5CFBA19FEC86629F89FCF26012F0A8F7798808E9 (void);
// 0x000003FA System.Void NPOI.HSSF.Record.Chart.ChartFRTInfoRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ChartFRTInfoRecord_Serialize_m95D1EC48A55E9C4407CFEE9977E298F894A7BDD6 (void);
// 0x000003FB System.Void NPOI.HSSF.Record.Chart.ChartFRTInfoRecord_CFRTID::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CFRTID_Serialize_m8AB7EA7C636B30A3C637AB93F629A0AD83D76EFC (void);
// 0x000003FC System.Void NPOI.HSSF.Record.Chart.ChartRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ChartRecord_Serialize_m27257DA75F014BD6BDF124E0DD15948C33CF7A13 (void);
// 0x000003FD System.Int32 NPOI.HSSF.Record.Chart.ChartRecord::get_DataSize()
extern void ChartRecord_get_DataSize_m398CCB879A71F52A24E7567FE751723A51F29B8E (void);
// 0x000003FE System.Int16 NPOI.HSSF.Record.Chart.ChartRecord::get_Sid()
extern void ChartRecord_get_Sid_mBDC2C067B4FA360069805C0E8E1B89798081305C (void);
// 0x000003FF System.Int32 NPOI.HSSF.Record.Chart.ChartStartObjectRecord::get_DataSize()
extern void ChartStartObjectRecord_get_DataSize_m58ED76E776622C9959527D2FDB3311F0BFDD11D9 (void);
// 0x00000400 System.Int16 NPOI.HSSF.Record.Chart.ChartStartObjectRecord::get_Sid()
extern void ChartStartObjectRecord_get_Sid_mF8BF0404772A9E76D69D1B8244C80E1C204B82DC (void);
// 0x00000401 System.Void NPOI.HSSF.Record.Chart.ChartStartObjectRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ChartStartObjectRecord_Serialize_mDC6EC1BA7322516ECBCC8D8C69771ED94ADBB824 (void);
// 0x00000402 System.Int32 NPOI.HSSF.Record.Chart.CrtLinkRecord::get_DataSize()
extern void CrtLinkRecord_get_DataSize_m5C65C3B1C8019AB596B361C9DFA917A8135FE333 (void);
// 0x00000403 System.Void NPOI.HSSF.Record.Chart.CrtLinkRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CrtLinkRecord_Serialize_m6272106C5D30F110659040A27CD474736E446676 (void);
// 0x00000404 System.Int16 NPOI.HSSF.Record.Chart.CrtLinkRecord::get_Sid()
extern void CrtLinkRecord_get_Sid_mF362D48A854BDAABCB1CB8321B4D09CEE0A32791 (void);
// 0x00000405 System.Void NPOI.HSSF.Record.Chart.DataFormatRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DataFormatRecord_Serialize_m71A73C959B11C52E7173269844670CD2222427D7 (void);
// 0x00000406 System.Int32 NPOI.HSSF.Record.Chart.DataFormatRecord::get_DataSize()
extern void DataFormatRecord_get_DataSize_m6213A365CF5EB6314FD5E34053C4B392C4695BA3 (void);
// 0x00000407 System.Int16 NPOI.HSSF.Record.Chart.DataFormatRecord::get_Sid()
extern void DataFormatRecord_get_Sid_mB50C95E7638BDD0414BD5B4EE672611C3E7B4ED2 (void);
// 0x00000408 System.Void NPOI.HSSF.Record.Chart.DatRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DatRecord_Serialize_m7D41CF5572F7B1D2199D0BAAAC82F16A5C90EE23 (void);
// 0x00000409 System.Int32 NPOI.HSSF.Record.Chart.DatRecord::get_DataSize()
extern void DatRecord_get_DataSize_m6CC92D77FD9F4A89F562F6C50436BD7E213CDE55 (void);
// 0x0000040A System.Int16 NPOI.HSSF.Record.Chart.DatRecord::get_Sid()
extern void DatRecord_get_Sid_m3BB6E82D7AF9F11F131CA8CD1372922C4A602A9F (void);
// 0x0000040B System.Void NPOI.HSSF.Record.Chart.EndRecord::.ctor()
extern void EndRecord__ctor_m16DB750F1C363543F5A3B892329AAE79D699D361 (void);
// 0x0000040C System.String NPOI.HSSF.Record.Chart.EndRecord::ToString()
extern void EndRecord_ToString_m5389D156DC61908D6A19CBF96A433381968D0373 (void);
// 0x0000040D System.Void NPOI.HSSF.Record.Chart.EndRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void EndRecord_Serialize_mEFE9D1EC17E7184EE8096EDA633DB82DD162CC2B (void);
// 0x0000040E System.Int32 NPOI.HSSF.Record.Chart.EndRecord::get_DataSize()
extern void EndRecord_get_DataSize_m496EC9E608512EF0034C1504BF58C82EF11CB3D9 (void);
// 0x0000040F System.Int16 NPOI.HSSF.Record.Chart.EndRecord::get_Sid()
extern void EndRecord_get_Sid_m5CF07A607503860199C9CF75FD05FB86CE58B28D (void);
// 0x00000410 System.Object NPOI.HSSF.Record.Chart.EndRecord::Clone()
extern void EndRecord_Clone_m9490D30FE1E400BABEE1143BFFBDEBE61FCF3BDC (void);
// 0x00000411 System.Void NPOI.HSSF.Record.Chart.EndRecord::.cctor()
extern void EndRecord__cctor_m9A814F5E3BB6441A1148E953D563896E4C7B085E (void);
// 0x00000412 System.Void NPOI.HSSF.Record.Chart.FrameRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FrameRecord_Serialize_m9D4D24E33D117DA856B317BECE72D459D3EADE38 (void);
// 0x00000413 System.Int32 NPOI.HSSF.Record.Chart.FrameRecord::get_DataSize()
extern void FrameRecord_get_DataSize_mC391BAFC9F148762FD875B340E775BBB0E15F09E (void);
// 0x00000414 System.Int16 NPOI.HSSF.Record.Chart.FrameRecord::get_Sid()
extern void FrameRecord_get_Sid_m515266862ED6A3D16945A8C1E40A6AA942E26630 (void);
// 0x00000415 System.Void NPOI.HSSF.Record.Chart.LegendRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void LegendRecord_Serialize_mDDA8AADB6CA9FAE66B18ABACCD917F64A7B4E2FC (void);
// 0x00000416 System.Int32 NPOI.HSSF.Record.Chart.LegendRecord::get_DataSize()
extern void LegendRecord_get_DataSize_mFAA1EB8C47EC8961F300267051474FD6B0FF8AE0 (void);
// 0x00000417 System.Int16 NPOI.HSSF.Record.Chart.LegendRecord::get_Sid()
extern void LegendRecord_get_Sid_mA87A8E29975E7ECD9CEAEAA11BDACD0D935271E5 (void);
// 0x00000418 System.Void NPOI.HSSF.Record.Chart.LineFormatRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void LineFormatRecord_Serialize_m7165A7171515C1A3FB60725D7353B079C5B66BB3 (void);
// 0x00000419 System.Int32 NPOI.HSSF.Record.Chart.LineFormatRecord::get_DataSize()
extern void LineFormatRecord_get_DataSize_mD6D3C32288DC51B060CCF607788B74619930EB16 (void);
// 0x0000041A System.Int16 NPOI.HSSF.Record.Chart.LineFormatRecord::get_Sid()
extern void LineFormatRecord_get_Sid_m4368DCEA704227E0F4FD019A8A159F0380289275 (void);
// 0x0000041B System.Void NPOI.HSSF.Record.Chart.ObjectLinkRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ObjectLinkRecord_Serialize_mB118F2DE6B6FEF9842A3E7B0D7BC529C2CB031B2 (void);
// 0x0000041C System.Int32 NPOI.HSSF.Record.Chart.ObjectLinkRecord::get_DataSize()
extern void ObjectLinkRecord_get_DataSize_m68B6E2D0129FD40D07B676DD3F99E65787328A52 (void);
// 0x0000041D System.Int16 NPOI.HSSF.Record.Chart.ObjectLinkRecord::get_Sid()
extern void ObjectLinkRecord_get_Sid_m2992E49BAFA1033C49C2119F2B78C89497848FDE (void);
// 0x0000041E System.Void NPOI.HSSF.Record.Chart.PlotAreaRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PlotAreaRecord_Serialize_m53568210AEC99025B65E5C2D9E288AF3188804B6 (void);
// 0x0000041F System.Int32 NPOI.HSSF.Record.Chart.PlotAreaRecord::get_DataSize()
extern void PlotAreaRecord_get_DataSize_m4CD5D0C00AFB030D8948B316785D294092FFAE83 (void);
// 0x00000420 System.Int16 NPOI.HSSF.Record.Chart.PlotAreaRecord::get_Sid()
extern void PlotAreaRecord_get_Sid_mDE6D81218C56DE3CB55C96ADF9238D708EAEAC3A (void);
// 0x00000421 System.Void NPOI.HSSF.Record.Chart.PlotGrowthRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PlotGrowthRecord_Serialize_m4769D809D43B9C440397ECBF59D09913D59DC8B6 (void);
// 0x00000422 System.Int32 NPOI.HSSF.Record.Chart.PlotGrowthRecord::get_DataSize()
extern void PlotGrowthRecord_get_DataSize_m72B89381926EDE9E1C517D29E35DA01C566981B7 (void);
// 0x00000423 System.Int16 NPOI.HSSF.Record.Chart.PlotGrowthRecord::get_Sid()
extern void PlotGrowthRecord_get_Sid_mA023A85EC0E415FF080D1FC6EE2668D12D4C8071 (void);
// 0x00000424 System.Int32 NPOI.HSSF.Record.Chart.PosRecord::get_DataSize()
extern void PosRecord_get_DataSize_mD1022F3F9A982AA039772FCCCC7485A638F445C7 (void);
// 0x00000425 System.Void NPOI.HSSF.Record.Chart.PosRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PosRecord_Serialize_m95CE4202F64107AB3E8D634C914416A9891626D9 (void);
// 0x00000426 System.Int16 NPOI.HSSF.Record.Chart.PosRecord::get_Sid()
extern void PosRecord_get_Sid_m1E73FA0248C34D9A1315F7EA46B4BA876DC5A199 (void);
// 0x00000427 System.Void NPOI.HSSF.Record.Chart.SeriesIndexRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SeriesIndexRecord_Serialize_mA524752857140E8AD8398E0C5B0A79F36CC276A2 (void);
// 0x00000428 System.Int32 NPOI.HSSF.Record.Chart.SeriesIndexRecord::get_DataSize()
extern void SeriesIndexRecord_get_DataSize_m6090A6AA9E32E132ED2271E5029BD031DB8B72F3 (void);
// 0x00000429 System.Int16 NPOI.HSSF.Record.Chart.SeriesIndexRecord::get_Sid()
extern void SeriesIndexRecord_get_Sid_m60419460BD87D73B5557A4A02E1BE7ED8858B92F (void);
// 0x0000042A System.Void NPOI.HSSF.Record.Chart.SeriesListRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SeriesListRecord_Serialize_mF935FF7E23F8C1872FEC4055623845F8A566BF2D (void);
// 0x0000042B System.Int32 NPOI.HSSF.Record.Chart.SeriesListRecord::get_DataSize()
extern void SeriesListRecord_get_DataSize_m1169203800FF61332E95391BB69579F8FFCEBAD9 (void);
// 0x0000042C System.Int16 NPOI.HSSF.Record.Chart.SeriesListRecord::get_Sid()
extern void SeriesListRecord_get_Sid_m14547E7C3249153613B5FDF8ABC90B7D2E57DEEF (void);
// 0x0000042D System.Void NPOI.HSSF.Record.Chart.SeriesRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SeriesRecord_Serialize_m7BA0AA547C1CFA260EB63AEA0AD9F34793C8D010 (void);
// 0x0000042E System.Int32 NPOI.HSSF.Record.Chart.SeriesRecord::get_DataSize()
extern void SeriesRecord_get_DataSize_m65B865BFD971786084D21747BD60CC87BC200DEC (void);
// 0x0000042F System.Int16 NPOI.HSSF.Record.Chart.SeriesRecord::get_Sid()
extern void SeriesRecord_get_Sid_m19EEB104E33EC60E00C7465458B0E2A19DC0E6F5 (void);
// 0x00000430 System.Void NPOI.HSSF.Record.Chart.SeriesTextRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SeriesTextRecord_Serialize_m512ADF17D2694FAD5432C7BF6738991CF91A0CD2 (void);
// 0x00000431 System.Int32 NPOI.HSSF.Record.Chart.SeriesTextRecord::get_DataSize()
extern void SeriesTextRecord_get_DataSize_mD9979FAB0E19CBE2C55ED41869DC0839F99805CF (void);
// 0x00000432 System.Int16 NPOI.HSSF.Record.Chart.SeriesTextRecord::get_Sid()
extern void SeriesTextRecord_get_Sid_m3F628CE47DEA6AFC86842D8BE4CE958BD2F2666C (void);
// 0x00000433 System.Void NPOI.HSSF.Record.TextRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void TextRecord_Serialize_m47882C6CD87BAF72630A6974DBFE59B263C6734B (void);
// 0x00000434 System.Int32 NPOI.HSSF.Record.TextRecord::get_DataSize()
extern void TextRecord_get_DataSize_m05C3CCEC8735F0B1C5AD2E8FC4582345824F48F3 (void);
// 0x00000435 System.Int16 NPOI.HSSF.Record.TextRecord::get_Sid()
extern void TextRecord_get_Sid_mCD14A2737DBD0716C7C31F16A61F5C1896C5F66C (void);
// 0x00000436 System.Void NPOI.HSSF.Record.TickRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void TickRecord_Serialize_m8984967C3514E96A13D1C824AA9ABA720819F0FF (void);
// 0x00000437 System.Int32 NPOI.HSSF.Record.TickRecord::get_DataSize()
extern void TickRecord_get_DataSize_m812019C6C3D82F6608B60BC3A2CA78C5F61D55EC (void);
// 0x00000438 System.Int16 NPOI.HSSF.Record.TickRecord::get_Sid()
extern void TickRecord_get_Sid_m23528F7C2B283F74A261553AAC74F4DAE78ECCBC (void);
// 0x00000439 System.Void NPOI.HSSF.Record.UnitsRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void UnitsRecord_Serialize_m921D7E7D9013D428B749BB2134CE17BBE55693DE (void);
// 0x0000043A System.Int32 NPOI.HSSF.Record.UnitsRecord::get_DataSize()
extern void UnitsRecord_get_DataSize_m1ABD1F325FB9EF399186744A14F88CF8A7B2097B (void);
// 0x0000043B System.Int16 NPOI.HSSF.Record.UnitsRecord::get_Sid()
extern void UnitsRecord_get_Sid_m52F2630A585AF5D47918F6034177A5D608215C89 (void);
// 0x0000043C System.Void NPOI.HSSF.Record.Chart.ValueRangeRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ValueRangeRecord_Serialize_m575A4F51699F64AAB33355CDF72F5427F70D9CE0 (void);
// 0x0000043D System.Int32 NPOI.HSSF.Record.Chart.ValueRangeRecord::get_DataSize()
extern void ValueRangeRecord_get_DataSize_mB0771FEE2A22528CE737228E3E614CD363CF041C (void);
// 0x0000043E System.Int16 NPOI.HSSF.Record.Chart.ValueRangeRecord::get_Sid()
extern void ValueRangeRecord_get_Sid_m98C13824D2CD3C3907DB9D908A65F89DCD234403 (void);
// 0x0000043F System.Void NPOI.HSSF.Record.CodepageRecord::.ctor()
extern void CodepageRecord__ctor_m1575B497F60F721F25685346E6F22D3FB71AA49B (void);
// 0x00000440 System.Int16 NPOI.HSSF.Record.CodepageRecord::get_Codepage()
extern void CodepageRecord_get_Codepage_mBD535ED88C19E38066252547B165C49E0A55136A (void);
// 0x00000441 System.Void NPOI.HSSF.Record.CodepageRecord::set_Codepage(System.Int16)
extern void CodepageRecord_set_Codepage_mB0F9B4F797D5E0F66AD99141F9819773936329C6 (void);
// 0x00000442 System.String NPOI.HSSF.Record.CodepageRecord::ToString()
extern void CodepageRecord_ToString_mCA8C15A1606D8DD68DDF306865E6EA5D31DC28A2 (void);
// 0x00000443 System.Void NPOI.HSSF.Record.CodepageRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CodepageRecord_Serialize_m1C863C7DD30D604ED416CF7405E9D941C4D9CD47 (void);
// 0x00000444 System.Int32 NPOI.HSSF.Record.CodepageRecord::get_DataSize()
extern void CodepageRecord_get_DataSize_mAB6B32379FFF42A3B9B70EB10126354543B17F08 (void);
// 0x00000445 System.Int16 NPOI.HSSF.Record.CodepageRecord::get_Sid()
extern void CodepageRecord_get_Sid_m1DF97C6BC01B05029F894534B587F9621A4937C6 (void);
// 0x00000446 System.Int32 NPOI.HSSF.Record.ColumnInfoRecord::get_FirstColumn()
extern void ColumnInfoRecord_get_FirstColumn_m1BAFAB09DD3AB3C8A8B57B59A5144B744E78C84B (void);
// 0x00000447 System.Int32 NPOI.HSSF.Record.ColumnInfoRecord::get_LastColumn()
extern void ColumnInfoRecord_get_LastColumn_m965173C7A2C44CF9F7588CB19F2DDD474198337D (void);
// 0x00000448 System.Int32 NPOI.HSSF.Record.ColumnInfoRecord::get_ColumnWidth()
extern void ColumnInfoRecord_get_ColumnWidth_m78BFF73129BB3BA10ED8FA4885A108D8873485CA (void);
// 0x00000449 System.Int32 NPOI.HSSF.Record.ColumnInfoRecord::get_XFIndex()
extern void ColumnInfoRecord_get_XFIndex_mDA4B9919937F0FAFBD9E829CEA5FDDC03E305A31 (void);
// 0x0000044A System.Int16 NPOI.HSSF.Record.ColumnInfoRecord::get_Sid()
extern void ColumnInfoRecord_get_Sid_m82E43E47AF488B70FA0C3A20863E96F365E6A365 (void);
// 0x0000044B System.Boolean NPOI.HSSF.Record.ColumnInfoRecord::ContainsColumn(System.Int32)
extern void ColumnInfoRecord_ContainsColumn_m5D0E86BF8868A447C3E4F625C9EB1B98950960C0 (void);
// 0x0000044C System.Int32 NPOI.HSSF.Record.ColumnInfoRecord::get_DataSize()
extern void ColumnInfoRecord_get_DataSize_m9FB584AAE96AF3B30B157CC44210C86ECE9D9EA1 (void);
// 0x0000044D System.Void NPOI.HSSF.Record.ColumnInfoRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ColumnInfoRecord_Serialize_m8490A3162BFB03C5FA1E6795BA6F781F09EA64BB (void);
// 0x0000044E System.Void NPOI.HSSF.Record.ColumnInfoRecord::.cctor()
extern void ColumnInfoRecord__cctor_m486282E84DC8AAB11D50DDA758706C1CD9B25BD9 (void);
// 0x0000044F System.Void NPOI.POIFS.EventFileSystem.POIFSWriterEvent::.ctor(NPOI.POIFS.FileSystem.DocumentOutputStream,NPOI.POIFS.FileSystem.POIFSDocumentPath,System.String,System.Int32)
extern void POIFSWriterEvent__ctor_m26DCC8C2342FA6BB2EA431DE616E1B6D1A4C098F (void);
// 0x00000450 System.Void NPOI.POIFS.EventFileSystem.POIFSWriterListener::ProcessPOIFSWriterEvent(NPOI.POIFS.EventFileSystem.POIFSWriterEvent)
// 0x00000451 System.Int32 NPOI.POIFS.FileSystem.BlockStore::GetBlockStoreBlockSize()
// 0x00000452 NPOI.Util.ByteBuffer NPOI.POIFS.FileSystem.BlockStore::GetBlockAt(System.Int32)
// 0x00000453 NPOI.Util.ByteBuffer NPOI.POIFS.FileSystem.BlockStore::CreateBlockIfNeeded(System.Int32)
// 0x00000454 NPOI.POIFS.Storage.BATBlockAndIndex NPOI.POIFS.FileSystem.BlockStore::GetBATBlockAndIndex(System.Int32)
// 0x00000455 System.Int32 NPOI.POIFS.FileSystem.BlockStore::GetNextBlock(System.Int32)
// 0x00000456 System.Void NPOI.POIFS.FileSystem.BlockStore::SetNextBlock(System.Int32,System.Int32)
// 0x00000457 System.Int32 NPOI.POIFS.FileSystem.BlockStore::GetFreeBlock()
// 0x00000458 NPOI.POIFS.FileSystem.ChainLoopDetector NPOI.POIFS.FileSystem.BlockStore::GetChainLoopDetector()
// 0x00000459 System.Void NPOI.POIFS.FileSystem.ChainLoopDetector::.ctor(System.Int64,NPOI.POIFS.FileSystem.BlockStore)
extern void ChainLoopDetector__ctor_m11B00B3CFFDED002AF089E745AECD8B6B6A5DD8A (void);
// 0x0000045A System.Void NPOI.POIFS.FileSystem.ChainLoopDetector::Claim(System.Int32)
extern void ChainLoopDetector_Claim_mCD49B10FECDA2CA18C0ABDA315DD063456255718 (void);
// 0x0000045B System.Void NPOI.Util.ByteArrayInputStream::.ctor()
extern void ByteArrayInputStream__ctor_m781A46CD2EE7EF10D9D7AF64C5AF382E43B23E06 (void);
// 0x0000045C System.Int32 NPOI.Util.ByteArrayInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ByteArrayInputStream_Read_mC1D8DBE30DE0ADC1F92619809D14B70B071351FC (void);
// 0x0000045D System.Int32 NPOI.Util.ByteArrayInputStream::Available()
extern void ByteArrayInputStream_Available_mE83757F0936EB1628CC813D6FA87AC5439FA14CC (void);
// 0x0000045E System.Boolean NPOI.Util.ByteArrayInputStream::MarkSupported()
extern void ByteArrayInputStream_MarkSupported_m8D80D4012DF40ACD0535E76226EB35826D3B3FCF (void);
// 0x0000045F System.Void NPOI.Util.ByteArrayInputStream::Mark(System.Int32)
extern void ByteArrayInputStream_Mark_m7BEFF39BA54F08B20F163D4E6AA9595A3096C25D (void);
// 0x00000460 System.Void NPOI.Util.ByteArrayInputStream::Reset()
extern void ByteArrayInputStream_Reset_m736B9A9237B69FFC46110A0A682EC371CD80B4A0 (void);
// 0x00000461 System.Void NPOI.Util.ByteArrayInputStream::Close()
extern void ByteArrayInputStream_Close_m67B612C1F73E27E8E03F0946AFCE5A324718B141 (void);
// 0x00000462 System.Boolean NPOI.Util.ByteArrayInputStream::get_CanRead()
extern void ByteArrayInputStream_get_CanRead_mBCB989183C004DF9847B535E80AA8D7D171CD1EB (void);
// 0x00000463 System.Boolean NPOI.Util.ByteArrayInputStream::get_CanWrite()
extern void ByteArrayInputStream_get_CanWrite_mF0905F1673F49D668644DD338E8FD22347CB9641 (void);
// 0x00000464 System.Boolean NPOI.Util.ByteArrayInputStream::get_CanSeek()
extern void ByteArrayInputStream_get_CanSeek_m61376FE796A73BAE23838B4E16CEA21216D3CA87 (void);
// 0x00000465 System.Void NPOI.Util.ByteArrayInputStream::Flush()
extern void ByteArrayInputStream_Flush_mF94FE387B40E1705973C4B3E7C10A63BC89248E2 (void);
// 0x00000466 System.Int64 NPOI.Util.ByteArrayInputStream::get_Length()
extern void ByteArrayInputStream_get_Length_m60A9C8DC9463FB7F060E78D2B312CA79D2C09917 (void);
// 0x00000467 System.Int64 NPOI.Util.ByteArrayInputStream::get_Position()
extern void ByteArrayInputStream_get_Position_m9728D9828E4B5E5C857DEBE0E923ACB3105464E5 (void);
// 0x00000468 System.Void NPOI.Util.ByteArrayInputStream::set_Position(System.Int64)
extern void ByteArrayInputStream_set_Position_m0092148347650B403E4224E6064E49ECF35B0B02 (void);
// 0x00000469 System.Int64 NPOI.Util.ByteArrayInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void ByteArrayInputStream_Seek_m36EC1C9061EC85C1CCE7A7D7D55AE3CD11E8E62A (void);
// 0x0000046A System.Void NPOI.Util.ByteArrayInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void ByteArrayInputStream_Write_mB83130169E52EA258711DCC3D80601CC699A2331 (void);
// 0x0000046B System.Void NPOI.POIFS.FileSystem.DocumentInputStream::.ctor()
extern void DocumentInputStream__ctor_m581AFB79AC512FAB567BE6EAF7E2F06F441A385B (void);
// 0x0000046C System.Void NPOI.POIFS.FileSystem.DocumentInputStream::.ctor(NPOI.POIFS.FileSystem.DocumentEntry)
extern void DocumentInputStream__ctor_m8516103ECB990B6AEE75F5C2E3DA55857108297C (void);
// 0x0000046D System.Int64 NPOI.POIFS.FileSystem.DocumentInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void DocumentInputStream_Seek_mB3B1295C11554C68859E82B1997EB37F8B9B957F (void);
// 0x0000046E System.Int64 NPOI.POIFS.FileSystem.DocumentInputStream::get_Length()
extern void DocumentInputStream_get_Length_mFA7938BCA02BDB1001961A19F7EC4A515C11DB3A (void);
// 0x0000046F System.Int64 NPOI.POIFS.FileSystem.DocumentInputStream::get_Position()
extern void DocumentInputStream_get_Position_m622A15DB862A3DCEB28DCADA100D88F0E97CFF36 (void);
// 0x00000470 System.Void NPOI.POIFS.FileSystem.DocumentInputStream::set_Position(System.Int64)
extern void DocumentInputStream_set_Position_m467ACB3B77940FBBCC8ED86B0AF8A0644E38DBB1 (void);
// 0x00000471 System.Int32 NPOI.POIFS.FileSystem.DocumentInputStream::Available()
extern void DocumentInputStream_Available_mF0A119D6E65E96876F61F9BCA2C2CBD93F68242F (void);
// 0x00000472 System.Void NPOI.POIFS.FileSystem.DocumentInputStream::Close()
extern void DocumentInputStream_Close_m2A96E72D87D3E50220BCCFBD5CB687C9C3D26553 (void);
// 0x00000473 System.Void NPOI.POIFS.FileSystem.DocumentInputStream::Mark(System.Int32)
extern void DocumentInputStream_Mark_mB6FEEC2BF386900D56A9DD2E80E16519925D694A (void);
// 0x00000474 System.Boolean NPOI.POIFS.FileSystem.DocumentInputStream::MarkSupported()
extern void DocumentInputStream_MarkSupported_m27B7BCCBA3810F48C9539DB5CB0D505E3C606619 (void);
// 0x00000475 System.Int32 NPOI.POIFS.FileSystem.DocumentInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void DocumentInputStream_Read_m869CE88788AE9C3BF1DF5981039AE274C9C79413 (void);
// 0x00000476 System.Void NPOI.POIFS.FileSystem.DocumentInputStream::Reset()
extern void DocumentInputStream_Reset_mEE67E42FA7023AEB8CAF3774505DFD550F9E61AB (void);
// 0x00000477 System.Int32 NPOI.POIFS.FileSystem.DocumentInputStream::ReadByte()
extern void DocumentInputStream_ReadByte_mDE5DEF78109C29620C644085A9B9ED3D761639A7 (void);
// 0x00000478 System.Double NPOI.POIFS.FileSystem.DocumentInputStream::ReadDouble()
extern void DocumentInputStream_ReadDouble_m0C72A8B7C95B21D93FA3B490DB44B7E5FE75D7D5 (void);
// 0x00000479 System.Int16 NPOI.POIFS.FileSystem.DocumentInputStream::ReadShort()
extern void DocumentInputStream_ReadShort_m28AF0BB6C7CABDB87B974FFA8B8CB0A6D1A2E7C8 (void);
// 0x0000047A System.Void NPOI.POIFS.FileSystem.DocumentInputStream::ReadFully(System.Byte[],System.Int32,System.Int32)
extern void DocumentInputStream_ReadFully_m1FE8460029D63012E8564284C15E31D59287FA4A (void);
// 0x0000047B System.Int64 NPOI.POIFS.FileSystem.DocumentInputStream::ReadLong()
extern void DocumentInputStream_ReadLong_mA7EA10E68CABF451F2F4B668E8D263741922691E (void);
// 0x0000047C System.Int32 NPOI.POIFS.FileSystem.DocumentInputStream::ReadInt()
extern void DocumentInputStream_ReadInt_m04504D79977195297342C1C896504C8A29A625C5 (void);
// 0x0000047D System.Int32 NPOI.POIFS.FileSystem.DocumentInputStream::ReadUShort()
extern void DocumentInputStream_ReadUShort_m891329AE8094A7E52EEFAE30DD5AD068E05B6E81 (void);
// 0x0000047E System.Int32 NPOI.POIFS.FileSystem.DocumentInputStream::ReadUByte()
extern void DocumentInputStream_ReadUByte_m8D23582251EDA54F2CBE49A578BE70754437CAC1 (void);
// 0x0000047F System.Void NPOI.POIFS.FileSystem.DocumentInputStream::.cctor()
extern void DocumentInputStream__cctor_mDE1AF6EF92D507F856A25CCBA2DE93D03C09B117 (void);
// 0x00000480 System.Void NPOI.POIFS.FileSystem.DocumentOutputStream::.ctor(System.IO.Stream,System.Int32)
extern void DocumentOutputStream__ctor_mC9B17E9D90F564C809B326B4FAB6BEB98C451A57 (void);
// 0x00000481 System.Void NPOI.POIFS.FileSystem.DocumentOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void DocumentOutputStream_Write_mBBBCB56CC1BE751979C076E87D0637ECC7A2D4E3 (void);
// 0x00000482 System.Void NPOI.POIFS.FileSystem.DocumentOutputStream::Flush()
extern void DocumentOutputStream_Flush_m4616FA99D7F6910F3D7211A2B8C40915D71FCF42 (void);
// 0x00000483 System.Void NPOI.POIFS.FileSystem.DocumentOutputStream::Close()
extern void DocumentOutputStream_Close_m554845D1C103C72922BAD41B87D0BF3809ED96EE (void);
// 0x00000484 System.Void NPOI.POIFS.FileSystem.DocumentOutputStream::WriteFiller(System.Int32,System.Byte)
extern void DocumentOutputStream_WriteFiller_m01DD916CFE05ECEB8CCF9F6D4ED16BB7CBE5102A (void);
// 0x00000485 System.Void NPOI.POIFS.FileSystem.DocumentOutputStream::LimitCheck(System.Int32)
extern void DocumentOutputStream_LimitCheck_m4C2C878BD8069D22E6E58273E89B7AF118FF215F (void);
// 0x00000486 System.Void NPOI.POIFS.FileSystem.NDocumentInputStream::.ctor(NPOI.POIFS.FileSystem.DocumentEntry)
extern void NDocumentInputStream__ctor_mD930B91F94BA6B0931FF45CC275A138D9092DF4A (void);
// 0x00000487 System.Int32 NPOI.POIFS.FileSystem.NDocumentInputStream::Available()
extern void NDocumentInputStream_Available_m466149E75B81061AFABB7DC74B8592FA61D87DA2 (void);
// 0x00000488 System.Void NPOI.POIFS.FileSystem.NDocumentInputStream::Close()
extern void NDocumentInputStream_Close_m9C65EE804B43521EC3E1B410E167457A8B41DFCC (void);
// 0x00000489 System.Void NPOI.POIFS.FileSystem.NDocumentInputStream::Mark(System.Int32)
extern void NDocumentInputStream_Mark_mB0E7B5D30C579BAC7ACBA48CC9DF720571F603ED (void);
// 0x0000048A System.Int32 NPOI.POIFS.FileSystem.NDocumentInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void NDocumentInputStream_Read_m34266C2AD96B546FBDA8D3BC45B7269DBC06D1F0 (void);
// 0x0000048B System.Void NPOI.POIFS.FileSystem.NDocumentInputStream::Reset()
extern void NDocumentInputStream_Reset_mFBE7679F7B7472F9D9A9EA84B3E10742A66F2652 (void);
// 0x0000048C System.Void NPOI.POIFS.FileSystem.NDocumentInputStream::DieIfClosed()
extern void NDocumentInputStream_DieIfClosed_mA9E001A28EC2F1DF44C0DEDCCA45456EFCE4E42E (void);
// 0x0000048D System.Boolean NPOI.POIFS.FileSystem.NDocumentInputStream::atEOD()
extern void NDocumentInputStream_atEOD_m83D52AE747A3A50CB87975CE68E17B68DAF99E09 (void);
// 0x0000048E System.Void NPOI.POIFS.FileSystem.NDocumentInputStream::CheckAvaliable(System.Int32)
extern void NDocumentInputStream_CheckAvaliable_m4733CC6CBE7DDF2B13D8FFD8E720F8888CF18016 (void);
// 0x0000048F System.Void NPOI.POIFS.FileSystem.NDocumentInputStream::ReadFully(System.Byte[],System.Int32,System.Int32)
extern void NDocumentInputStream_ReadFully_m0A45E4B04CE467527836B2BE8F83981EB504090A (void);
// 0x00000490 System.Int32 NPOI.POIFS.FileSystem.NDocumentInputStream::ReadByte()
extern void NDocumentInputStream_ReadByte_m40378B371B5AF9FA3120DB5A2A9B3166C96AC136 (void);
// 0x00000491 System.Double NPOI.POIFS.FileSystem.NDocumentInputStream::ReadDouble()
extern void NDocumentInputStream_ReadDouble_mC74D301C95AF3A338FD9757FE80980E46DCE0079 (void);
// 0x00000492 System.Int64 NPOI.POIFS.FileSystem.NDocumentInputStream::ReadLong()
extern void NDocumentInputStream_ReadLong_m4E9B101CF2C06174A6BF2226F4D1A8A06FBD7594 (void);
// 0x00000493 System.Int16 NPOI.POIFS.FileSystem.NDocumentInputStream::ReadShort()
extern void NDocumentInputStream_ReadShort_mBA9BA3C9EF3D62320779BA235D975A2DC4BB6D62 (void);
// 0x00000494 System.Int32 NPOI.POIFS.FileSystem.NDocumentInputStream::ReadInt()
extern void NDocumentInputStream_ReadInt_mBFC23B0CCB1505FCF5EE20A4FC8569B6CED122C3 (void);
// 0x00000495 System.Int32 NPOI.POIFS.FileSystem.NDocumentInputStream::ReadUShort()
extern void NDocumentInputStream_ReadUShort_mFCC0D76A454E150A4F1D971100A7253404FC2F66 (void);
// 0x00000496 System.Int32 NPOI.POIFS.FileSystem.NDocumentInputStream::ReadUByte()
extern void NDocumentInputStream_ReadUByte_m9C6467313382CA9BFFD0957A3205BF5627A22856 (void);
// 0x00000497 System.Int64 NPOI.POIFS.FileSystem.NDocumentInputStream::get_Length()
extern void NDocumentInputStream_get_Length_mAAE7911E61AE8F62476492F1EDE57FAB8DD26778 (void);
// 0x00000498 System.Int64 NPOI.POIFS.FileSystem.NDocumentInputStream::get_Position()
extern void NDocumentInputStream_get_Position_mEC21710C2C086CF4D7116902DD3FF65934183B9F (void);
// 0x00000499 System.Void NPOI.POIFS.FileSystem.NDocumentInputStream::set_Position(System.Int64)
extern void NDocumentInputStream_set_Position_mBAE2A5DCA8605E8079B35030A0E5BC4DA0729F3B (void);
// 0x0000049A System.Int64 NPOI.POIFS.FileSystem.NDocumentInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void NDocumentInputStream_Seek_mD38A2453A10E3741A6312B1D17FF6493D7A5D3BC (void);
// 0x0000049B System.Void NPOI.POIFS.FileSystem.NPOIFSDocument::.ctor(NPOI.POIFS.Properties.DocumentProperty,NPOI.POIFS.FileSystem.NPOIFSFileSystem)
extern void NPOIFSDocument__ctor_mC4CDF25553CA767DAD2F4FDD419886A187247415 (void);
// 0x0000049C System.Void NPOI.POIFS.FileSystem.NPOIFSDocument::.ctor(System.String,NPOI.POIFS.FileSystem.NPOIFSFileSystem,System.IO.Stream)
extern void NPOIFSDocument__ctor_mF12A8D0D91CFAF87B39DB71121738A6B6BC88065 (void);
// 0x0000049D System.Collections.Generic.IEnumerator`1<NPOI.Util.ByteBuffer> NPOI.POIFS.FileSystem.NPOIFSDocument::GetBlockIterator()
extern void NPOIFSDocument_GetBlockIterator_m020D9CFB482D3B92B8CF24BC295E338116038658 (void);
// 0x0000049E System.Int32 NPOI.POIFS.FileSystem.NPOIFSDocument::get_Size()
extern void NPOIFSDocument_get_Size_mFC6E8DA64ABD3E773AB63E10A43957870FA31AEF (void);
// 0x0000049F NPOI.POIFS.Properties.DocumentProperty NPOI.POIFS.FileSystem.NPOIFSDocument::get_DocumentProperty()
extern void NPOIFSDocument_get_DocumentProperty_m4B3A6DAA7B928783B9E62F8B6A4195A7D06EDB20 (void);
// 0x000004A0 NPOI.POIFS.Storage.BATBlock NPOI.POIFS.FileSystem.NPOIFSFileSystem::CreateBAT(System.Int32,System.Boolean)
extern void NPOIFSFileSystem_CreateBAT_mEA951D91FEB9350347A6A9C499FD20C4CDCD37A6 (void);
// 0x000004A1 NPOI.Util.ByteBuffer NPOI.POIFS.FileSystem.NPOIFSFileSystem::GetBlockAt(System.Int32)
extern void NPOIFSFileSystem_GetBlockAt_m906D632FC22EE42218DEBCADEF84FFAE71C67AF9 (void);
// 0x000004A2 NPOI.Util.ByteBuffer NPOI.POIFS.FileSystem.NPOIFSFileSystem::CreateBlockIfNeeded(System.Int32)
extern void NPOIFSFileSystem_CreateBlockIfNeeded_m14ADB99F37D0A6C4D53F0F91913348BE309199EF (void);
// 0x000004A3 NPOI.POIFS.Storage.BATBlockAndIndex NPOI.POIFS.FileSystem.NPOIFSFileSystem::GetBATBlockAndIndex(System.Int32)
extern void NPOIFSFileSystem_GetBATBlockAndIndex_mBB626F7760054F8D6109FF92FCD01D1E5FBF205B (void);
// 0x000004A4 System.Int32 NPOI.POIFS.FileSystem.NPOIFSFileSystem::GetNextBlock(System.Int32)
extern void NPOIFSFileSystem_GetNextBlock_mC45736455D02E5F31298CE71FC66603B63BBB9E3 (void);
// 0x000004A5 System.Void NPOI.POIFS.FileSystem.NPOIFSFileSystem::SetNextBlock(System.Int32,System.Int32)
extern void NPOIFSFileSystem_SetNextBlock_m1B36283D62438382D2B8E778A27BB7E10FDE15E1 (void);
// 0x000004A6 System.Int32 NPOI.POIFS.FileSystem.NPOIFSFileSystem::GetFreeBlock()
extern void NPOIFSFileSystem_GetFreeBlock_m2EF303EFF29B05E53F32CDA1A9FA7CF198399D7A (void);
// 0x000004A7 NPOI.POIFS.FileSystem.ChainLoopDetector NPOI.POIFS.FileSystem.NPOIFSFileSystem::GetChainLoopDetector()
extern void NPOIFSFileSystem_GetChainLoopDetector_mF4BF053127B83A7E3EF4CE35EACDC676E1CF7CA5 (void);
// 0x000004A8 NPOI.POIFS.FileSystem.NPOIFSMiniStore NPOI.POIFS.FileSystem.NPOIFSFileSystem::GetMiniStore()
extern void NPOIFSFileSystem_GetMiniStore_m1AC5A9705D8EA7AA26A6F44A283B2312CBD348A5 (void);
// 0x000004A9 System.Void NPOI.POIFS.FileSystem.NPOIFSFileSystem::AddDocument(NPOI.POIFS.FileSystem.NPOIFSDocument)
extern void NPOIFSFileSystem_AddDocument_m8E4E0E4EE5F992EE0D0E2FE46F84C1D6B894460F (void);
// 0x000004AA System.Void NPOI.POIFS.FileSystem.NPOIFSFileSystem::AddDirectory(NPOI.POIFS.Properties.DirectoryProperty)
extern void NPOIFSFileSystem_AddDirectory_m51E61F4B964F76E815A38F6965EBD69F0F807D2B (void);
// 0x000004AB System.Int32 NPOI.POIFS.FileSystem.NPOIFSFileSystem::GetBigBlockSize()
extern void NPOIFSFileSystem_GetBigBlockSize_mD08447B635BB5893F3D81551C3BE7D8820422A48 (void);
// 0x000004AC NPOI.POIFS.Common.POIFSBigBlockSize NPOI.POIFS.FileSystem.NPOIFSFileSystem::GetBigBlockSizeDetails()
extern void NPOIFSFileSystem_GetBigBlockSizeDetails_mEBBFC89F674391478B6EFD543945382E60296ED9 (void);
// 0x000004AD System.Int32 NPOI.POIFS.FileSystem.NPOIFSFileSystem::GetBlockStoreBlockSize()
extern void NPOIFSFileSystem_GetBlockStoreBlockSize_mD7BDEE71E44A6D77D8396DC06D5C57D2BA7F15E5 (void);
// 0x000004AE System.Void NPOI.POIFS.FileSystem.NPOIFSFileSystem::.cctor()
extern void NPOIFSFileSystem__cctor_mC5E7822A18E3CFAF34C743F985AABAFC300D7A9E (void);
// 0x000004AF NPOI.Util.ByteBuffer NPOI.POIFS.FileSystem.NPOIFSMiniStore::GetBlockAt(System.Int32)
extern void NPOIFSMiniStore_GetBlockAt_m9FEB3F785023B16A8FC35AC72940F73AF4F2522D (void);
// 0x000004B0 NPOI.Util.ByteBuffer NPOI.POIFS.FileSystem.NPOIFSMiniStore::CreateBlockIfNeeded(System.Int32)
extern void NPOIFSMiniStore_CreateBlockIfNeeded_m5A23A6F2A83CC8BF0C91A3F6B80C6BE5907298F4 (void);
// 0x000004B1 NPOI.POIFS.Storage.BATBlockAndIndex NPOI.POIFS.FileSystem.NPOIFSMiniStore::GetBATBlockAndIndex(System.Int32)
extern void NPOIFSMiniStore_GetBATBlockAndIndex_mC1BF8070E20A9D10990C4FB8FEA000B50CD6699E (void);
// 0x000004B2 System.Int32 NPOI.POIFS.FileSystem.NPOIFSMiniStore::GetNextBlock(System.Int32)
extern void NPOIFSMiniStore_GetNextBlock_m1D27DDA4B85CBF1BFD96F6A36D46AC63674D59F6 (void);
// 0x000004B3 System.Void NPOI.POIFS.FileSystem.NPOIFSMiniStore::SetNextBlock(System.Int32,System.Int32)
extern void NPOIFSMiniStore_SetNextBlock_m458C294C5E4BC2419944FF30A4A45D8FE9D9F360 (void);
// 0x000004B4 System.Int32 NPOI.POIFS.FileSystem.NPOIFSMiniStore::GetFreeBlock()
extern void NPOIFSMiniStore_GetFreeBlock_m2F830BD5A4BCC3730A5B20087980DFAD4D40863D (void);
// 0x000004B5 NPOI.POIFS.FileSystem.ChainLoopDetector NPOI.POIFS.FileSystem.NPOIFSMiniStore::GetChainLoopDetector()
extern void NPOIFSMiniStore_GetChainLoopDetector_mF05A281B62FAA9E1D510C2D21FABC6FEF3A27FA5 (void);
// 0x000004B6 System.Int32 NPOI.POIFS.FileSystem.NPOIFSMiniStore::GetBlockStoreBlockSize()
extern void NPOIFSMiniStore_GetBlockStoreBlockSize_m9DA270C1C8A2E1B8F4DE7F7E1CA4E6DC63AC887C (void);
// 0x000004B7 System.Void NPOI.POIFS.FileSystem.NPOIFSStream::.ctor(NPOI.POIFS.FileSystem.BlockStore,System.Int32)
extern void NPOIFSStream__ctor_m6203A4E216389C4E68B63508A66040BE898BEE2C (void);
// 0x000004B8 System.Void NPOI.POIFS.FileSystem.NPOIFSStream::.ctor(NPOI.POIFS.FileSystem.BlockStore)
extern void NPOIFSStream__ctor_mEA077DE2F07571DAF08199360CAD21D73C170BCE (void);
// 0x000004B9 System.Int32 NPOI.POIFS.FileSystem.NPOIFSStream::GetStartBlock()
extern void NPOIFSStream_GetStartBlock_m65B7C7316CB80AF643C75CB9A1C83FC98B6254E9 (void);
// 0x000004BA System.Collections.Generic.IEnumerator`1<NPOI.Util.ByteBuffer> NPOI.POIFS.FileSystem.NPOIFSStream::GetEnumerator()
extern void NPOIFSStream_GetEnumerator_mF64A3E18CF2778875FC41513B8C7CE1319A2132D (void);
// 0x000004BB System.Collections.IEnumerator NPOI.POIFS.FileSystem.NPOIFSStream::System.Collections.IEnumerable.GetEnumerator()
extern void NPOIFSStream_System_Collections_IEnumerable_GetEnumerator_m9D8717BFC3E8139C92489D47F2D95FDCBDF5AB23 (void);
// 0x000004BC System.Collections.Generic.IEnumerator`1<NPOI.Util.ByteBuffer> NPOI.POIFS.FileSystem.NPOIFSStream::GetBlockIterator()
extern void NPOIFSStream_GetBlockIterator_m2AA6A61485AE2C80919F5153C4A17E3DED2FB1D9 (void);
// 0x000004BD System.Void NPOI.POIFS.FileSystem.NPOIFSStream::UpdateContents(System.Byte[])
extern void NPOIFSStream_UpdateContents_m49EA6FB1EA93DF22AB0D82C32EF1EB287831E4A2 (void);
// 0x000004BE System.Void NPOI.POIFS.FileSystem.NPOIFSStream::free(NPOI.POIFS.FileSystem.ChainLoopDetector)
extern void NPOIFSStream_free_m8FF35A9917C2623DF36195C34F943823BB892625 (void);
// 0x000004BF System.Void NPOI.POIFS.FileSystem.StreamBlockByteBufferIterator::.ctor(NPOI.POIFS.FileSystem.BlockStore,System.Int32)
extern void StreamBlockByteBufferIterator__ctor_m3E063ADCE634B149E2FFA12D82E27E09E3E5ECC7 (void);
// 0x000004C0 NPOI.Util.ByteBuffer NPOI.POIFS.FileSystem.StreamBlockByteBufferIterator::Next()
extern void StreamBlockByteBufferIterator_Next_m360AFDCE367D117657F30CC1A4F2594609DBA9D7 (void);
// 0x000004C1 NPOI.Util.ByteBuffer NPOI.POIFS.FileSystem.StreamBlockByteBufferIterator::get_Current()
extern void StreamBlockByteBufferIterator_get_Current_m6CDA5AD1019A0E4F6882EC7CFED819A0ECF36615 (void);
// 0x000004C2 System.Object NPOI.POIFS.FileSystem.StreamBlockByteBufferIterator::System.Collections.IEnumerator.get_Current()
extern void StreamBlockByteBufferIterator_System_Collections_IEnumerator_get_Current_mB1CC8494AA1B1B7F446A9C0B9166CA01913A5953 (void);
// 0x000004C3 System.Void NPOI.POIFS.FileSystem.StreamBlockByteBufferIterator::System.Collections.IEnumerator.Reset()
extern void StreamBlockByteBufferIterator_System_Collections_IEnumerator_Reset_mB74AB3BF4A107C521B2AEDCE6E9351FC7FCC50BB (void);
// 0x000004C4 System.Boolean NPOI.POIFS.FileSystem.StreamBlockByteBufferIterator::System.Collections.IEnumerator.MoveNext()
extern void StreamBlockByteBufferIterator_System_Collections_IEnumerator_MoveNext_m4EEFF6093A0D9879B2D3778FFE0EED68380349BC (void);
// 0x000004C5 System.Void NPOI.POIFS.FileSystem.StreamBlockByteBufferIterator::Dispose()
extern void StreamBlockByteBufferIterator_Dispose_mF0A7AC010DC06D6DBFDD85E7CB3961AE2F9B8051 (void);
// 0x000004C6 System.Void NPOI.POIFS.FileSystem.ODocumentInputStream::.ctor(NPOI.POIFS.FileSystem.DocumentEntry)
extern void ODocumentInputStream__ctor_m7F8EE603D2B86F3CB365D8BEFD117D320E13EDD1 (void);
// 0x000004C7 System.Int64 NPOI.POIFS.FileSystem.ODocumentInputStream::get_Length()
extern void ODocumentInputStream_get_Length_m7AFFF6904FA4FE13CF9D0CB0C2FC18DD5B802621 (void);
// 0x000004C8 System.Int32 NPOI.POIFS.FileSystem.ODocumentInputStream::Available()
extern void ODocumentInputStream_Available_m28746DFC50345A3078D8F253C9D0E68E5E903AC5 (void);
// 0x000004C9 System.Void NPOI.POIFS.FileSystem.ODocumentInputStream::Close()
extern void ODocumentInputStream_Close_m0D336A1D7E53AAAB4988C5E7EE1EFEE4719770E6 (void);
// 0x000004CA System.Void NPOI.POIFS.FileSystem.ODocumentInputStream::Mark(System.Int32)
extern void ODocumentInputStream_Mark_m214714CA8673FA68E4FDCBD2E1AFA9A0A10FD3C5 (void);
// 0x000004CB NPOI.POIFS.Storage.DataInputBlock NPOI.POIFS.FileSystem.ODocumentInputStream::GetDataInputBlock(System.Int64)
extern void ODocumentInputStream_GetDataInputBlock_mE74FDCC7FA25673ACC0813AAB68556F12762EF8D (void);
// 0x000004CC System.Int32 NPOI.POIFS.FileSystem.ODocumentInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ODocumentInputStream_Read_m2A813C4BE6D8B749FAFC71F3DC0177C8E21C8875 (void);
// 0x000004CD System.Void NPOI.POIFS.FileSystem.ODocumentInputStream::Reset()
extern void ODocumentInputStream_Reset_mA313C42CB01A417CF266F8FDDD61CC796683BA43 (void);
// 0x000004CE System.Void NPOI.POIFS.FileSystem.ODocumentInputStream::dieIfClosed()
extern void ODocumentInputStream_dieIfClosed_mEA3A5E18DA073ADF90130C4F541718C412327485 (void);
// 0x000004CF System.Boolean NPOI.POIFS.FileSystem.ODocumentInputStream::atEOD()
extern void ODocumentInputStream_atEOD_mAC3ACB85D779796DDC3FC5DB75D2F8244CA5E59E (void);
// 0x000004D0 System.Void NPOI.POIFS.FileSystem.ODocumentInputStream::CheckAvaliable(System.Int32)
extern void ODocumentInputStream_CheckAvaliable_m09B3558D162BDE0CCC3E003132459BD43D45E8E0 (void);
// 0x000004D1 System.Int32 NPOI.POIFS.FileSystem.ODocumentInputStream::ReadByte()
extern void ODocumentInputStream_ReadByte_m8D78A79D9BDA6618D526F5CCE3D02EF5EA6F8391 (void);
// 0x000004D2 System.Double NPOI.POIFS.FileSystem.ODocumentInputStream::ReadDouble()
extern void ODocumentInputStream_ReadDouble_m708DBA4648ACA8DF67F75532A237F0BB2A0F458E (void);
// 0x000004D3 System.Int16 NPOI.POIFS.FileSystem.ODocumentInputStream::ReadShort()
extern void ODocumentInputStream_ReadShort_mE36C3EDFED6167357EB9583433571714649D9B78 (void);
// 0x000004D4 System.Void NPOI.POIFS.FileSystem.ODocumentInputStream::ReadFully(System.Byte[],System.Int32,System.Int32)
extern void ODocumentInputStream_ReadFully_m992306317AEC211BDBA3C8C177BA686CE89EE37C (void);
// 0x000004D5 System.Int64 NPOI.POIFS.FileSystem.ODocumentInputStream::ReadLong()
extern void ODocumentInputStream_ReadLong_m84184D21A992A9C8EC82E5B45B50BF6F38869655 (void);
// 0x000004D6 System.Int32 NPOI.POIFS.FileSystem.ODocumentInputStream::ReadInt()
extern void ODocumentInputStream_ReadInt_m6F55983AA39E97001718A085D7A122C530300C57 (void);
// 0x000004D7 System.Int32 NPOI.POIFS.FileSystem.ODocumentInputStream::ReadUShort()
extern void ODocumentInputStream_ReadUShort_mC292175597CC831EDAC60204D9CC1DB088D789A0 (void);
// 0x000004D8 System.Int32 NPOI.POIFS.FileSystem.ODocumentInputStream::ReadUByte()
extern void ODocumentInputStream_ReadUByte_m4836A1929EC1C61EC2434FBA0EDBCCA50B225714 (void);
// 0x000004D9 System.Int64 NPOI.POIFS.FileSystem.ODocumentInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void ODocumentInputStream_Seek_m87539D94588EFD19FF34F0B82724F2C5BBD4EC45 (void);
// 0x000004DA System.Int64 NPOI.POIFS.FileSystem.ODocumentInputStream::get_Position()
extern void ODocumentInputStream_get_Position_mC4F4410A79302D2373AF54124B1C863D1C2D0F21 (void);
// 0x000004DB System.Void NPOI.POIFS.FileSystem.ODocumentInputStream::set_Position(System.Int64)
extern void ODocumentInputStream_set_Position_m906F4804F4C01D538CD3686EC9111B081E075E6D (void);
// 0x000004DC System.Int32 NPOI.POIFS.FileSystem.BATManaged::get_CountBlocks()
// 0x000004DD System.Void NPOI.POIFS.FileSystem.BATManaged::set_StartBlock(System.Int32)
// 0x000004DE System.Void NPOI.POIFS.Storage.BlockWritable::WriteBlocks(System.IO.Stream)
// 0x000004DF NPOI.POIFS.Storage.DocumentBlock[] NPOI.POIFS.FileSystem.POIFSDocument::ConvertRawBlocksToBigBlocks(NPOI.POIFS.Storage.ListManagedBlock[])
extern void POIFSDocument_ConvertRawBlocksToBigBlocks_mC96C8293971B6C7A04BFE6257ABCA8383F75B5D8 (void);
// 0x000004E0 NPOI.POIFS.Storage.SmallDocumentBlock[] NPOI.POIFS.FileSystem.POIFSDocument::ConvertRawBlocksToSmallBlocks(NPOI.POIFS.Storage.ListManagedBlock[])
extern void POIFSDocument_ConvertRawBlocksToSmallBlocks_m80BA5B84A9AB102EC826BAA21A739EB102C704C5 (void);
// 0x000004E1 System.Void NPOI.POIFS.FileSystem.POIFSDocument::.ctor(System.String,NPOI.POIFS.Common.POIFSBigBlockSize,NPOI.POIFS.Storage.ListManagedBlock[],System.Int32)
extern void POIFSDocument__ctor_m4EA2E513F08971EC3A497BE6D96C7EF7AFE6FA39 (void);
// 0x000004E2 System.Void NPOI.POIFS.FileSystem.POIFSDocument::.ctor(System.String,NPOI.POIFS.Common.POIFSBigBlockSize,System.IO.Stream)
extern void POIFSDocument__ctor_mC5747A121BDA029F82D356030493D97D018E55B0 (void);
// 0x000004E3 System.Void NPOI.POIFS.FileSystem.POIFSDocument::.ctor(System.String,System.IO.Stream)
extern void POIFSDocument__ctor_mD1BC63A2BA2D3E6E8378EB1350CBA2C472825CBE (void);
// 0x000004E4 System.Void NPOI.POIFS.FileSystem.POIFSDocument::.ctor(System.String,NPOI.POIFS.Storage.ListManagedBlock[],System.Int32)
extern void POIFSDocument__ctor_m59968A98E86D3775899B54293857CE3801272491 (void);
// 0x000004E5 System.Void NPOI.POIFS.FileSystem.POIFSDocument::WriteBlocks(System.IO.Stream)
extern void POIFSDocument_WriteBlocks_m43AF2E33252322BCFAB3C5BD182EF4F842F4F785 (void);
// 0x000004E6 NPOI.POIFS.Storage.DataInputBlock NPOI.POIFS.FileSystem.POIFSDocument::GetDataInputBlock(System.Int32)
extern void POIFSDocument_GetDataInputBlock_mFEA18D83D23EB395A297B534E03D91CB76A06F46 (void);
// 0x000004E7 System.Int32 NPOI.POIFS.FileSystem.POIFSDocument::get_CountBlocks()
extern void POIFSDocument_get_CountBlocks_m5650544605E7F120C5A95798C4C0292A970493FD (void);
// 0x000004E8 NPOI.POIFS.Properties.DocumentProperty NPOI.POIFS.FileSystem.POIFSDocument::get_DocumentProperty()
extern void POIFSDocument_get_DocumentProperty_m0F91BA9737F75966EEB2BC5B9AA64C41FFFC73AD (void);
// 0x000004E9 NPOI.POIFS.Storage.BlockWritable[] NPOI.POIFS.FileSystem.POIFSDocument::get_SmallBlocks()
extern void POIFSDocument_get_SmallBlocks_mFF12629B1D25DFB977E5AC2CC7DE663DDA6EFB3A (void);
// 0x000004EA System.Void NPOI.POIFS.FileSystem.POIFSDocument::set_StartBlock(System.Int32)
extern void POIFSDocument_set_StartBlock_mB06CAA221CCF8198BF85C84A57354519379B967C (void);
// 0x000004EB System.Void NPOI.POIFS.FileSystem.POIFSDocument::.cctor()
extern void POIFSDocument__cctor_m93D43A0A29FE63B10126B15EA2CEEC7EDE9B91C7 (void);
// 0x000004EC System.Void NPOI.POIFS.FileSystem.POIFSDocument_SmallBlockStore::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize,NPOI.POIFS.Storage.SmallDocumentBlock[])
extern void SmallBlockStore__ctor_mD6787E079DCC55ED46F0E79974D51C7AE9AE39BD (void);
// 0x000004ED NPOI.POIFS.Storage.SmallDocumentBlock[] NPOI.POIFS.FileSystem.POIFSDocument_SmallBlockStore::get_Blocks()
extern void SmallBlockStore_get_Blocks_mA07A9D91B3FC2408F966BFFD75CB8BFD8481F37A (void);
// 0x000004EE System.Boolean NPOI.POIFS.FileSystem.POIFSDocument_SmallBlockStore::get_Valid()
extern void SmallBlockStore_get_Valid_m746A122B4186121E122D8A966721952947C16ACC (void);
// 0x000004EF System.Void NPOI.POIFS.FileSystem.POIFSDocument_BigBlockStore::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize,NPOI.POIFS.Storage.DocumentBlock[])
extern void BigBlockStore__ctor_m3F361AA1BE0562B3E6FBD8A6666772ADDA0D9ED4 (void);
// 0x000004F0 System.Boolean NPOI.POIFS.FileSystem.POIFSDocument_BigBlockStore::get_Valid()
extern void BigBlockStore_get_Valid_m304787C131816D773515546D679219D5E574DCBD (void);
// 0x000004F1 NPOI.POIFS.Storage.DocumentBlock[] NPOI.POIFS.FileSystem.POIFSDocument_BigBlockStore::get_Blocks()
extern void BigBlockStore_get_Blocks_m0C8A848C28751BB4B77569F6C064A73DE9DA2DA3 (void);
// 0x000004F2 System.Void NPOI.POIFS.FileSystem.POIFSDocument_BigBlockStore::WriteBlocks(System.IO.Stream)
extern void BigBlockStore_WriteBlocks_m01B944F8E44DA91345577D47EE0E4493B9880531 (void);
// 0x000004F3 System.Int32 NPOI.POIFS.FileSystem.POIFSDocument_BigBlockStore::get_CountBlocks()
extern void BigBlockStore_get_CountBlocks_m8C4E756AF92BEC30A932AA2A745FA0ACF2520EE8 (void);
// 0x000004F4 NPOI.Util.ByteBuffer NPOI.POIFS.NIO.DataSource::Read(System.Int32,System.Int64)
// 0x000004F5 System.Void NPOI.POIFS.NIO.DataSource::Write(NPOI.Util.ByteBuffer,System.Int64)
// 0x000004F6 System.Int64 NPOI.POIFS.NIO.DataSource::get_Size()
// 0x000004F7 System.Void NPOI.POIFS.Properties.PropertyTableBase::.ctor(NPOI.POIFS.Storage.HeaderBlock)
extern void PropertyTableBase__ctor_m3FF29D3A2A56699A2A690CDFD67D0ADCD290E7BA (void);
// 0x000004F8 System.Void NPOI.POIFS.Properties.PropertyTableBase::.ctor(NPOI.POIFS.Storage.HeaderBlock,System.Collections.Generic.List`1<NPOI.POIFS.Properties.Property>)
extern void PropertyTableBase__ctor_m9562FEB6F7624D6105367FCDC46B212E768CBC86 (void);
// 0x000004F9 System.Void NPOI.POIFS.Properties.PropertyTableBase::AddProperty(NPOI.POIFS.Properties.Property)
extern void PropertyTableBase_AddProperty_m1C88A6F3EE4A2852694635552A0B12D05557CFA7 (void);
// 0x000004FA NPOI.POIFS.Properties.RootProperty NPOI.POIFS.Properties.PropertyTableBase::get_Root()
extern void PropertyTableBase_get_Root_m1C717F2FA191A8FBA520F22094C10DAB8FF05835 (void);
// 0x000004FB System.Void NPOI.POIFS.Properties.PropertyTableBase::PopulatePropertyTree(NPOI.POIFS.Properties.DirectoryProperty)
extern void PropertyTableBase_PopulatePropertyTree_m3EBEB2DE28227538C8D9C43E0D06A2480C1C22E1 (void);
// 0x000004FC System.Int32 NPOI.POIFS.Properties.PropertyTableBase::get_StartBlock()
extern void PropertyTableBase_get_StartBlock_mA8DCBCC7C4855B369709DAEC91FCD3588443F1C6 (void);
// 0x000004FD System.Void NPOI.POIFS.Properties.PropertyTableBase::set_StartBlock(System.Int32)
extern void PropertyTableBase_set_StartBlock_m2564B407A052502F18A1CEC381AAAA2BB6F534DE (void);
// 0x000004FE System.Int32 NPOI.POIFS.Properties.PropertyTableBase::get_CountBlocks()
extern void PropertyTableBase_get_CountBlocks_m3128A5634D616322B40337B36B7AF7C20F06890E (void);
// 0x000004FF System.Void NPOI.POIFS.Storage.HeaderBlockConstants::.ctor()
extern void HeaderBlockConstants__ctor_m5ECDDFA5506029EE654219E8D68049B8FE4C8209 (void);
// 0x00000500 System.Void NPOI.POIFS.Storage.HeaderBlock::.ctor(System.IO.Stream)
extern void HeaderBlock__ctor_m43A67C965840C82C0FC764572329712F5860BBF4 (void);
// 0x00000501 System.Void NPOI.POIFS.Storage.HeaderBlock::PrivateHeaderBlock(System.Byte[])
extern void HeaderBlock_PrivateHeaderBlock_mA86CB1769AEE8C40004F9016369B8DAF7567506B (void);
// 0x00000502 System.Void NPOI.POIFS.Storage.HeaderBlock::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void HeaderBlock__ctor_m81B08B00A1A4A91E3BB783C4341E8DB5D57139BF (void);
// 0x00000503 System.Byte[] NPOI.POIFS.Storage.HeaderBlock::ReadFirst512(System.IO.Stream)
extern void HeaderBlock_ReadFirst512_m5ACEEAE70FF93EC536CFD7CFDF2AAB8B51A7EE16 (void);
// 0x00000504 System.String NPOI.POIFS.Storage.HeaderBlock::LongToHex(System.Int64)
extern void HeaderBlock_LongToHex_m692CFF2DE3769E906BE748D21AF31EC82AF30D13 (void);
// 0x00000505 System.IO.IOException NPOI.POIFS.Storage.HeaderBlock::AlertShortRead(System.Int32,System.Int32)
extern void HeaderBlock_AlertShortRead_m38853947E94DEDA1578B4DE2D8A28E12D7A44170 (void);
// 0x00000506 System.Int32 NPOI.POIFS.Storage.HeaderBlock::get_PropertyStart()
extern void HeaderBlock_get_PropertyStart_m944CD586A853916358C5E25248443E31E6A4FED4 (void);
// 0x00000507 System.Void NPOI.POIFS.Storage.HeaderBlock::set_PropertyStart(System.Int32)
extern void HeaderBlock_set_PropertyStart_m9CEE42F905FB86C14A9D84C095E5E6BD68C94369 (void);
// 0x00000508 System.Int32 NPOI.POIFS.Storage.HeaderBlock::get_SBATStart()
extern void HeaderBlock_get_SBATStart_m27F4084017117007B24A217F95BCEA679C7B05AF (void);
// 0x00000509 System.Void NPOI.POIFS.Storage.HeaderBlock::set_SBATStart(System.Int32)
extern void HeaderBlock_set_SBATStart_m7712BEBB016F39C72EFA48F67770938E9235EB2A (void);
// 0x0000050A System.Int32 NPOI.POIFS.Storage.HeaderBlock::get_SBATCount()
extern void HeaderBlock_get_SBATCount_m6E9686FDD3D2AE5D2D4C4197D96E653D65E4F154 (void);
// 0x0000050B System.Void NPOI.POIFS.Storage.HeaderBlock::set_SBATBlockCount(System.Int32)
extern void HeaderBlock_set_SBATBlockCount_m5E9FEEBFA2E7EF05AA79122A80B0B65C34B73A1E (void);
// 0x0000050C System.Int32 NPOI.POIFS.Storage.HeaderBlock::get_BATCount()
extern void HeaderBlock_get_BATCount_m07EC0BDA0D964ED344FD9E61391CFBB5641BD084 (void);
// 0x0000050D System.Void NPOI.POIFS.Storage.HeaderBlock::set_BATCount(System.Int32)
extern void HeaderBlock_set_BATCount_m70A793FD88E4097A552DDE4D14BC1F9360D4D192 (void);
// 0x0000050E System.Int32[] NPOI.POIFS.Storage.HeaderBlock::get_BATArray()
extern void HeaderBlock_get_BATArray_mF8B72A22FDE5EDB26C2B4CE7104BAB1A19AB7688 (void);
// 0x0000050F System.Void NPOI.POIFS.Storage.HeaderBlock::set_BATArray(System.Int32[])
extern void HeaderBlock_set_BATArray_m8267969EBC57AD335905747C97F406D82734AC69 (void);
// 0x00000510 System.Int32 NPOI.POIFS.Storage.HeaderBlock::get_XBATCount()
extern void HeaderBlock_get_XBATCount_m54A170A09494C874CDAE488AE59D50BAAEC83219 (void);
// 0x00000511 System.Void NPOI.POIFS.Storage.HeaderBlock::set_XBATCount(System.Int32)
extern void HeaderBlock_set_XBATCount_m6AC26A5197EB9B846A71E750BC44E349119E7268 (void);
// 0x00000512 System.Int32 NPOI.POIFS.Storage.HeaderBlock::get_XBATIndex()
extern void HeaderBlock_get_XBATIndex_m7A302D1726F9D44B7077FE38FDA4F43941D74B84 (void);
// 0x00000513 System.Void NPOI.POIFS.Storage.HeaderBlock::set_XBATStart(System.Int32)
extern void HeaderBlock_set_XBATStart_mD326A9A99D271253B8DF901D4A827D971FDC2011 (void);
// 0x00000514 NPOI.POIFS.Common.POIFSBigBlockSize NPOI.POIFS.Storage.HeaderBlock::get_BigBlockSize()
extern void HeaderBlock_get_BigBlockSize_m0FE57511C9E809B6D4DA3DDECA0499AFF152E223 (void);
// 0x00000515 System.Void NPOI.POIFS.Storage.HeaderBlock::WriteData(System.IO.Stream)
extern void HeaderBlock_WriteData_m8F94D4FDDCEAAD1D0E15BC6036FF6D19274CE7A7 (void);
// 0x00000516 System.Void NPOI.POIFS.Storage.HeaderBlock::.cctor()
extern void HeaderBlock__cctor_m83876AED66CC563B21CCB772D464D7AF312889A3 (void);
// 0x00000517 System.Void NPOI.SS.Formula.Atp.EDate::.ctor()
extern void EDate__ctor_mB728A6887153E24AA349CDB5D35617E8B1CCB0B1 (void);
// 0x00000518 System.Void NPOI.SS.Formula.Atp.EDate::.cctor()
extern void EDate__cctor_m0D215CDEF96D307AA4EE8EC8ACFEB6F2B44B5647 (void);
// 0x00000519 System.Object[] NPOI.SS.Formula.Constant.ConstantValueParser::Parse(NPOI.Util.ILittleEndianInput,System.Int32)
extern void ConstantValueParser_Parse_mF55A638A80B16327024CBC90FFA85D0220C03611 (void);
// 0x0000051A System.Object NPOI.SS.Formula.Constant.ConstantValueParser::ReadAConstantValue(NPOI.Util.ILittleEndianInput)
extern void ConstantValueParser_ReadAConstantValue_mDD59FF89F3896C5EE734CCDBCC403848C09AD198 (void);
// 0x0000051B System.Object NPOI.SS.Formula.Constant.ConstantValueParser::ReadBoolean(NPOI.Util.ILittleEndianInput)
extern void ConstantValueParser_ReadBoolean_m139E67FC0E16D9D434AC54F0FD8B00D2B68DA24D (void);
// 0x0000051C System.Int32 NPOI.SS.Formula.Constant.ConstantValueParser::GetEncodedSize(System.Array)
extern void ConstantValueParser_GetEncodedSize_mC9895521560E86A9451A9AD7D0E218012CEF4A63 (void);
// 0x0000051D System.Int32 NPOI.SS.Formula.Constant.ConstantValueParser::GetEncodedSize(System.Object)
extern void ConstantValueParser_GetEncodedSize_m71F71406172D6194330563054C83100811ED3521 (void);
// 0x0000051E System.Void NPOI.SS.Formula.Constant.ConstantValueParser::Encode(NPOI.Util.ILittleEndianOutput,System.Array)
extern void ConstantValueParser_Encode_m8A42AF6A12316F6B7D7909E8347E1514DEA8A884 (void);
// 0x0000051F System.Void NPOI.SS.Formula.Constant.ConstantValueParser::EncodeSingleValue(NPOI.Util.ILittleEndianOutput,System.Object)
extern void ConstantValueParser_EncodeSingleValue_mC877F6067D955D2B24F8FF94CBF64108795079BD (void);
// 0x00000520 System.Void NPOI.SS.Formula.Constant.ErrorConstant::.ctor(System.Int32)
extern void ErrorConstant__ctor_m18398953EF90BAF2C911EF4B5CC4779D4A125CD0 (void);
// 0x00000521 System.Int32 NPOI.SS.Formula.Constant.ErrorConstant::get_ErrorCode()
extern void ErrorConstant_get_ErrorCode_mB3361A6A76DF28E76F3C4391162EE68514ED67B6 (void);
// 0x00000522 System.String NPOI.SS.Formula.Constant.ErrorConstant::get_Text()
extern void ErrorConstant_get_Text_m091E4FCBA40253BB0D45AC66911144CDD7C66F9A (void);
// 0x00000523 NPOI.SS.Formula.Constant.ErrorConstant NPOI.SS.Formula.Constant.ErrorConstant::ValueOf(System.Int32)
extern void ErrorConstant_ValueOf_m42D6805E17A080EC1702CF4AB19242BD80C8990D (void);
// 0x00000524 System.String NPOI.SS.Formula.Constant.ErrorConstant::ToString()
extern void ErrorConstant_ToString_m0D5D6E5C32662AEF40D573D0837F15A6301EFADD (void);
// 0x00000525 System.Void NPOI.SS.Formula.Constant.ErrorConstant::.cctor()
extern void ErrorConstant__cctor_mAA810FF12A047800D899DE36147CDD6268621CBF (void);
// 0x00000526 System.Int32 NPOI.HSSF.Record.ContinueRecord::get_DataSize()
extern void ContinueRecord_get_DataSize_m59B2975EC823281AA4E7DE55A70BFBA76B2D3ECF (void);
// 0x00000527 System.Void NPOI.HSSF.Record.ContinueRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ContinueRecord_Serialize_m62209FC4358C953A097F249C9BF2DA5F3F01F594 (void);
// 0x00000528 System.Byte[] NPOI.HSSF.Record.ContinueRecord::get_Data()
extern void ContinueRecord_get_Data_m379FB669D7D88291D2168D838CCC17CF855218A2 (void);
// 0x00000529 System.Int16 NPOI.HSSF.Record.ContinueRecord::get_Sid()
extern void ContinueRecord_get_Sid_m7E05B1C4FE6CEB4492425B6AB5013F468EDF2051 (void);
// 0x0000052A System.Void NPOI.HSSF.Record.Cont.ContinuableRecord::.ctor()
extern void ContinuableRecord__ctor_mA00415E16B267D054266C00D47A6D3AD1405AB31 (void);
// 0x0000052B System.Void NPOI.HSSF.Record.Cont.ContinuableRecord::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
// 0x0000052C System.Int32 NPOI.HSSF.Record.Cont.ContinuableRecord::get_RecordSize()
extern void ContinuableRecord_get_RecordSize_mFDA76EFE371C6C900879180C47390777D6BA306B (void);
// 0x0000052D System.Int32 NPOI.HSSF.Record.Cont.ContinuableRecord::Serialize(System.Int32,System.Byte[])
extern void ContinuableRecord_Serialize_m18B8B695E57E089EE923628FC9D8D3EF08A67F8E (void);
// 0x0000052E System.Void NPOI.Util.ILittleEndianOutput::WriteByte(System.Int32)
// 0x0000052F System.Void NPOI.Util.ILittleEndianOutput::WriteShort(System.Int32)
// 0x00000530 System.Void NPOI.Util.ILittleEndianOutput::WriteInt(System.Int32)
// 0x00000531 System.Void NPOI.Util.ILittleEndianOutput::WriteLong(System.Int64)
// 0x00000532 System.Void NPOI.Util.ILittleEndianOutput::WriteDouble(System.Double)
// 0x00000533 System.Void NPOI.Util.ILittleEndianOutput::Write(System.Byte[])
// 0x00000534 System.Void NPOI.Util.ILittleEndianOutput::Write(System.Byte[],System.Int32,System.Int32)
// 0x00000535 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::.ctor(NPOI.Util.ILittleEndianOutput,System.Int32)
extern void ContinuableRecordOutput__ctor_mA97B3281697F60CB938B5A9E6A32FE09D8CED45C (void);
// 0x00000536 NPOI.HSSF.Record.Cont.ContinuableRecordOutput NPOI.HSSF.Record.Cont.ContinuableRecordOutput::CreateForCountingOnly()
extern void ContinuableRecordOutput_CreateForCountingOnly_m6139DB524E9935B3AE7B74536D43B9683E11605B (void);
// 0x00000537 System.Int32 NPOI.HSSF.Record.Cont.ContinuableRecordOutput::get_TotalSize()
extern void ContinuableRecordOutput_get_TotalSize_mD3A8E710C5F8AD83250C063284A9D17F22757C3A (void);
// 0x00000538 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::Terminate()
extern void ContinuableRecordOutput_Terminate_m19D738E5DFB6B271400731C8D3420839F0973501 (void);
// 0x00000539 System.Int32 NPOI.HSSF.Record.Cont.ContinuableRecordOutput::get_AvailableSpace()
extern void ContinuableRecordOutput_get_AvailableSpace_m86560DDDEAE62D08500AEC3DC7B621450D497600 (void);
// 0x0000053A System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteContinue()
extern void ContinuableRecordOutput_WriteContinue_mF4DAE96762338EE1328BF167FFBB87A078666D96 (void);
// 0x0000053B System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteContinueIfRequired(System.Int32)
extern void ContinuableRecordOutput_WriteContinueIfRequired_mA5C94A1769394BF5A158D298390C751BC33E39C7 (void);
// 0x0000053C System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteStringData(System.String)
extern void ContinuableRecordOutput_WriteStringData_mFF458433247A9D2C10A51C7BA54E4749D4FCAFE0 (void);
// 0x0000053D System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteString(System.String,System.Int32,System.Int32)
extern void ContinuableRecordOutput_WriteString_m722522D735F62E2FBE526906A0450E66C1792E8D (void);
// 0x0000053E System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteCharacterData(System.String,System.Boolean)
extern void ContinuableRecordOutput_WriteCharacterData_mBF9C576CFB857845D6D20D550F40A881D0D31B75 (void);
// 0x0000053F System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::Write(System.Byte[])
extern void ContinuableRecordOutput_Write_m584D574C222D5ABAD25C58A47F40A12160626E29 (void);
// 0x00000540 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::Write(System.Byte[],System.Int32,System.Int32)
extern void ContinuableRecordOutput_Write_m5C5A39A45C5C8BC3074D9418A7956A5CC68A512E (void);
// 0x00000541 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteByte(System.Int32)
extern void ContinuableRecordOutput_WriteByte_mEE8CDE4B854B64161A294410A582CBA1B705374D (void);
// 0x00000542 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteDouble(System.Double)
extern void ContinuableRecordOutput_WriteDouble_mE922C1745F9C5095A4DBA3C6E35F04A21195530E (void);
// 0x00000543 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteInt(System.Int32)
extern void ContinuableRecordOutput_WriteInt_mEB0ACD4DBDA5B2B62D69CD7CE1897ED921BE0E49 (void);
// 0x00000544 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteLong(System.Int64)
extern void ContinuableRecordOutput_WriteLong_m87EC7CDC4C944BE852B0647E60AAD18BC1AE19EC (void);
// 0x00000545 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::WriteShort(System.Int32)
extern void ContinuableRecordOutput_WriteShort_mE1393EEB6DFD906953DC5A3E67763BABAAFCFBE1 (void);
// 0x00000546 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput::.cctor()
extern void ContinuableRecordOutput__cctor_mF4A642CF72A2F730406186F9422AE2A87AD008F3 (void);
// 0x00000547 NPOI.Util.ILittleEndianOutput NPOI.HSSF.Record.Cont.ContinuableRecordOutput_DelayableLittleEndianOutput1::CreateDelayedOutput(System.Int32)
extern void DelayableLittleEndianOutput1_CreateDelayedOutput_m0C585EF87073C039EEF4129C7D314F60F98FFE95 (void);
// 0x00000548 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput_DelayableLittleEndianOutput1::Write(System.Byte[])
extern void DelayableLittleEndianOutput1_Write_m82D99A1C34ABA4773563D94FABE3689481951E6A (void);
// 0x00000549 System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput_DelayableLittleEndianOutput1::Write(System.Byte[],System.Int32,System.Int32)
extern void DelayableLittleEndianOutput1_Write_mE8217516251CB4846BBD32E7900456714D9A7561 (void);
// 0x0000054A System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput_DelayableLittleEndianOutput1::WriteByte(System.Int32)
extern void DelayableLittleEndianOutput1_WriteByte_mA617208DE2FDA4EAEE9E35939FBE87AC18CDB428 (void);
// 0x0000054B System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput_DelayableLittleEndianOutput1::WriteDouble(System.Double)
extern void DelayableLittleEndianOutput1_WriteDouble_mEEB3418ADE24B2ED4E045AC5861FD10C3520EAFB (void);
// 0x0000054C System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput_DelayableLittleEndianOutput1::WriteInt(System.Int32)
extern void DelayableLittleEndianOutput1_WriteInt_mB026D8EF70ADF6AA992AC2440A576AD4774F59B2 (void);
// 0x0000054D System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput_DelayableLittleEndianOutput1::WriteLong(System.Int64)
extern void DelayableLittleEndianOutput1_WriteLong_mC77A160CE12EEB78D1F61AC2AC2C7A2C00EF0FA9 (void);
// 0x0000054E System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput_DelayableLittleEndianOutput1::WriteShort(System.Int32)
extern void DelayableLittleEndianOutput1_WriteShort_mB8C1974CD7A658CBC66DAA1DF561CC8FC4D424D1 (void);
// 0x0000054F System.Void NPOI.HSSF.Record.Cont.ContinuableRecordOutput_DelayableLittleEndianOutput1::.ctor()
extern void DelayableLittleEndianOutput1__ctor_m23C0B976C5F0C7A4488788E591F50181EEE39D27 (void);
// 0x00000550 NPOI.Util.ILittleEndianOutput NPOI.Util.IDelayableLittleEndianOutput::CreateDelayedOutput(System.Int32)
// 0x00000551 System.Void NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::.ctor(NPOI.Util.ILittleEndianOutput,System.Int32)
extern void UnknownLengthRecordOutput__ctor_m90059E77A9260AF43E040AB4BEE405F43D3D8566 (void);
// 0x00000552 System.Int32 NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::get_TotalSize()
extern void UnknownLengthRecordOutput_get_TotalSize_mB47657A700D4A67DF31D37EBC5450DF070171169 (void);
// 0x00000553 System.Int32 NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::get_AvailableSpace()
extern void UnknownLengthRecordOutput_get_AvailableSpace_m0DD6C13CBB633197EF122404969D16301A7228D7 (void);
// 0x00000554 System.Void NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::Terminate()
extern void UnknownLengthRecordOutput_Terminate_mFFF22B18914AE5B8586FF954386D90612A7BD9A2 (void);
// 0x00000555 System.Void NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::Write(System.Byte[])
extern void UnknownLengthRecordOutput_Write_m728808C88787D379294D8B390982877489BA2238 (void);
// 0x00000556 System.Void NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::Write(System.Byte[],System.Int32,System.Int32)
extern void UnknownLengthRecordOutput_Write_m84B6E37D8656FD8811B1B13E3C9E48332BC2D710 (void);
// 0x00000557 System.Void NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::WriteByte(System.Int32)
extern void UnknownLengthRecordOutput_WriteByte_mBCB62F6D14466C1EEFCF25BF154A26216CF1D7FB (void);
// 0x00000558 System.Void NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::WriteDouble(System.Double)
extern void UnknownLengthRecordOutput_WriteDouble_mD0122D1C5A762E0AA45CD0A572B98D1A72E2A9BA (void);
// 0x00000559 System.Void NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::WriteInt(System.Int32)
extern void UnknownLengthRecordOutput_WriteInt_m51185957523E40116522EA90C4C0F7DFF54E0261 (void);
// 0x0000055A System.Void NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::WriteLong(System.Int64)
extern void UnknownLengthRecordOutput_WriteLong_m455827770B5B8620ED46C7343EED29378CE7C6C2 (void);
// 0x0000055B System.Void NPOI.HSSF.Record.Cont.UnknownLengthRecordOutput::WriteShort(System.Int32)
extern void UnknownLengthRecordOutput_WriteShort_mA76412480179FF853FCF7B04FB8F2C4DA19CEF2B (void);
// 0x0000055C System.Void NPOI.HSSF.Record.CountryRecord::.ctor()
extern void CountryRecord__ctor_mFDC3615D925A450C68EFE74210E019BBBA1A2C3B (void);
// 0x0000055D System.Int16 NPOI.HSSF.Record.CountryRecord::get_DefaultCountry()
extern void CountryRecord_get_DefaultCountry_m2805267E5ED2DEBA5E5CEA6D4F84C13F582CDB87 (void);
// 0x0000055E System.Void NPOI.HSSF.Record.CountryRecord::set_DefaultCountry(System.Int16)
extern void CountryRecord_set_DefaultCountry_mF55FFDB314ADD7B070F1F470DBE24B0AB3E61D86 (void);
// 0x0000055F System.Int16 NPOI.HSSF.Record.CountryRecord::get_CurrentCountry()
extern void CountryRecord_get_CurrentCountry_m0EBDC6DB792C67B430F44E223100C1D36B40A4E3 (void);
// 0x00000560 System.Void NPOI.HSSF.Record.CountryRecord::set_CurrentCountry(System.Int16)
extern void CountryRecord_set_CurrentCountry_m52BF7A88917FE88881BA1EDBDFCBF6309AB14A44 (void);
// 0x00000561 System.String NPOI.HSSF.Record.CountryRecord::ToString()
extern void CountryRecord_ToString_mE769CDDD0DCD26BCE9D6F7D904811E6074C45F9A (void);
// 0x00000562 System.Void NPOI.HSSF.Record.CountryRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CountryRecord_Serialize_mF087C629D137A8963C24CD542EA9AEE6EC7DD56F (void);
// 0x00000563 System.Int32 NPOI.HSSF.Record.CountryRecord::get_DataSize()
extern void CountryRecord_get_DataSize_m86DC3A8449AD028E7B35CFE9B1D37347F7EAA82C (void);
// 0x00000564 System.Int16 NPOI.HSSF.Record.CountryRecord::get_Sid()
extern void CountryRecord_get_Sid_mF7B2629686282771A7FB863F8D7C0711F26B7DF7 (void);
// 0x00000565 System.Int32 NPOI.HSSF.Record.CRNCountRecord::get_NumberOfCRNs()
extern void CRNCountRecord_get_NumberOfCRNs_m549277B63A8C9883174DC3A9AE6E40850562FD53 (void);
// 0x00000566 System.Void NPOI.HSSF.Record.CRNCountRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CRNCountRecord_Serialize_mC16900C789B6BA059D977F7B87934FE1ABA62B5E (void);
// 0x00000567 System.Int32 NPOI.HSSF.Record.CRNCountRecord::get_DataSize()
extern void CRNCountRecord_get_DataSize_m5E37FBB210AE773A65C0A7E4686AABBED9CA2529 (void);
// 0x00000568 System.Int16 NPOI.HSSF.Record.CRNCountRecord::get_Sid()
extern void CRNCountRecord_get_Sid_mC9E6D52B76B2C67CDD1312F023D33863078EC4D3 (void);
// 0x00000569 System.Int32 NPOI.HSSF.Record.CRNRecord::get_DataSize()
extern void CRNRecord_get_DataSize_m4DD61C649B5EF60169AED2B526C17C7DF303224B (void);
// 0x0000056A System.Void NPOI.HSSF.Record.CRNRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CRNRecord_Serialize_m50724060ABE236C6BE654377234CA7F2A02763A5 (void);
// 0x0000056B System.Int16 NPOI.HSSF.Record.CRNRecord::get_Sid()
extern void CRNRecord_get_Sid_mED7983413D6423520A4AE98F21E0693AD7701E41 (void);
// 0x0000056C System.Void NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::.ctor(System.IO.Stream,System.Int32,NPOI.HSSF.Record.Crypto.Biff8EncryptionKey)
extern void Biff8DecryptingStream__ctor_m4BD586446360284C860BB78218A2DC24EB3C26D0 (void);
// 0x0000056D System.Int32 NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::Available()
extern void Biff8DecryptingStream_Available_m7CBD84BDE22555D32DA452909C7D71665809FFEC (void);
// 0x0000056E System.Int32 NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadRecordSID()
extern void Biff8DecryptingStream_ReadRecordSID_mEB6E54079E9AD71B4D288BDB3766F7F3872DAB1A (void);
// 0x0000056F System.Int32 NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadDataSize()
extern void Biff8DecryptingStream_ReadDataSize_mB71A837C7474BF4407D96F06960B9A4E798C7CB0 (void);
// 0x00000570 System.Double NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadDouble()
extern void Biff8DecryptingStream_ReadDouble_m2F365B7705EF1D7AB2A4DA9FB7074A09AC822447 (void);
// 0x00000571 System.Void NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadFully(System.Byte[],System.Int32,System.Int32)
extern void Biff8DecryptingStream_ReadFully_m1C5CDA36182F3FD5DB2D1081C9841AEF675E5472 (void);
// 0x00000572 System.Int32 NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadUByte()
extern void Biff8DecryptingStream_ReadUByte_m309746701DB93EAD7A3AB69A9236DE86ED96FC5D (void);
// 0x00000573 System.Int32 NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadByte()
extern void Biff8DecryptingStream_ReadByte_m0678712C04F5B3D07DB7D011357EC43181E9A652 (void);
// 0x00000574 System.Int32 NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadUShort()
extern void Biff8DecryptingStream_ReadUShort_m162CF922C516C11C575A053F5A9B6B1A7B447971 (void);
// 0x00000575 System.Int16 NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadShort()
extern void Biff8DecryptingStream_ReadShort_m54D393BE68A40AAA518B978E5D4C9F25F8945C72 (void);
// 0x00000576 System.Int32 NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadInt()
extern void Biff8DecryptingStream_ReadInt_mFFD0D3615D17800BCB71D6D5E8EB20B3BF8E0A89 (void);
// 0x00000577 System.Int64 NPOI.HSSF.Record.Crypto.Biff8DecryptingStream::ReadLong()
extern void Biff8DecryptingStream_ReadLong_m8246D497510F0A8AA1BC1A1BA678F4FED3DC3C6D (void);
// 0x00000578 NPOI.HSSF.Record.Crypto.Biff8EncryptionKey NPOI.HSSF.Record.Crypto.Biff8EncryptionKey::Create(System.Byte[])
extern void Biff8EncryptionKey_Create_m57071FAC1CABEAAD93F6F7AEE7D47126087246B1 (void);
// 0x00000579 NPOI.HSSF.Record.Crypto.Biff8EncryptionKey NPOI.HSSF.Record.Crypto.Biff8EncryptionKey::Create(System.String,System.Byte[])
extern void Biff8EncryptionKey_Create_mCA7A044135D73E9775892925BBB8EE389650A6D9 (void);
// 0x0000057A System.Void NPOI.HSSF.Record.Crypto.Biff8EncryptionKey::.ctor(System.Byte[])
extern void Biff8EncryptionKey__ctor_mEDF30CC4E55C943C53669C23584C1170C102620A (void);
// 0x0000057B System.Byte[] NPOI.HSSF.Record.Crypto.Biff8EncryptionKey::CreateKeyDigest(System.String,System.Byte[])
extern void Biff8EncryptionKey_CreateKeyDigest_mFF844B30F11461BA60A80A53C7C031F2DE13F0DF (void);
// 0x0000057C System.Boolean NPOI.HSSF.Record.Crypto.Biff8EncryptionKey::Validate(System.Byte[],System.Byte[])
extern void Biff8EncryptionKey_Validate_mA2FB35C207DEFE42E7433702464178AA0189A182 (void);
// 0x0000057D System.Void NPOI.HSSF.Record.Crypto.Biff8EncryptionKey::Check16Bytes(System.Byte[],System.String)
extern void Biff8EncryptionKey_Check16Bytes_m89DA20BAE4B5D8559A68E23C64D41BEEAC99FB03 (void);
// 0x0000057E NPOI.HSSF.Record.Crypto.RC4 NPOI.HSSF.Record.Crypto.Biff8EncryptionKey::CreateRC4(System.Int32)
extern void Biff8EncryptionKey_CreateRC4_m8EA5477B8F800B165745768C64C07AFDF8D7CF50 (void);
// 0x0000057F System.String NPOI.HSSF.Record.Crypto.Biff8EncryptionKey::get_CurrentUserPassword()
extern void Biff8EncryptionKey_get_CurrentUserPassword_m7469D0E0316599FFE50275F7B5E2B8A204E147E7 (void);
// 0x00000580 System.Void NPOI.HSSF.Record.Crypto.Biff8RC4::.ctor(System.Int32,NPOI.HSSF.Record.Crypto.Biff8EncryptionKey)
extern void Biff8RC4__ctor_mAC14D7D28C16B1083AB55D6F4785263F62064C36 (void);
// 0x00000581 System.Void NPOI.HSSF.Record.Crypto.Biff8RC4::RekeyForNextBlock()
extern void Biff8RC4_RekeyForNextBlock_m86FF0C41041ED51950F5C034B41E801482A31886 (void);
// 0x00000582 System.Int32 NPOI.HSSF.Record.Crypto.Biff8RC4::GetNextRC4Byte()
extern void Biff8RC4_GetNextRC4Byte_mEB293C26978AB6F44AAC01E65577E8165F9B71E1 (void);
// 0x00000583 System.Void NPOI.HSSF.Record.Crypto.Biff8RC4::StartRecord(System.Int32)
extern void Biff8RC4_StartRecord_mBF93F59B09325FD8719FE2FAE96FAF38638DDD06 (void);
// 0x00000584 System.Boolean NPOI.HSSF.Record.Crypto.Biff8RC4::IsNeverEncryptedRecord(System.Int32)
extern void Biff8RC4_IsNeverEncryptedRecord_mFA5D9EE0AF094535E3CCD4EA12148B5B9C21C870 (void);
// 0x00000585 System.Void NPOI.HSSF.Record.Crypto.Biff8RC4::SkipTwoBytes()
extern void Biff8RC4_SkipTwoBytes_m9F55E617A7599141E2E9F378D6835A4C3EC72075 (void);
// 0x00000586 System.Void NPOI.HSSF.Record.Crypto.Biff8RC4::Xor(System.Byte[],System.Int32,System.Int32)
extern void Biff8RC4_Xor_m29E517ADA732510F0895004A391860F995EE24EA (void);
// 0x00000587 System.Int32 NPOI.HSSF.Record.Crypto.Biff8RC4::XorByte(System.Int32)
extern void Biff8RC4_XorByte_mCB84CA640843FD7E3DA866EB5E670D9BE36EC090 (void);
// 0x00000588 System.Int32 NPOI.HSSF.Record.Crypto.Biff8RC4::Xorshort(System.Int32)
extern void Biff8RC4_Xorshort_m54CD3C94E6C1166647F7730C1CD6A80C3FF97388 (void);
// 0x00000589 System.Int32 NPOI.HSSF.Record.Crypto.Biff8RC4::XorInt(System.Int32)
extern void Biff8RC4_XorInt_mC4E0605295F9AED68CF2EF9C019231E5B40756A6 (void);
// 0x0000058A System.Int64 NPOI.HSSF.Record.Crypto.Biff8RC4::XorLong(System.Int64)
extern void Biff8RC4_XorLong_m66EC46DE2E93F1B419756343B85826420C493851 (void);
// 0x0000058B System.Void NPOI.HSSF.Record.Crypto.RC4::.ctor(System.Byte[])
extern void RC4__ctor_mA5331E6CFB9017A8F85CB170051D79F4B6310049 (void);
// 0x0000058C System.Byte NPOI.HSSF.Record.Crypto.RC4::Output()
extern void RC4_Output_mE3BD6657FEA74CB5C03E6D7DFE68F7C642E46088 (void);
// 0x0000058D System.Void NPOI.HSSF.Record.Crypto.RC4::Encrypt(System.Byte[])
extern void RC4_Encrypt_m9005830D7E8DB1D9A6C0AA9C566124D6CA9E8DF3 (void);
// 0x0000058E System.Void NPOI.HSSF.Record.Crypto.RC4::Encrypt(System.Byte[],System.Int32,System.Int32)
extern void RC4_Encrypt_m4348DB7DA70D0EF3BBF37EFB209EB249ECAEBC56 (void);
// 0x0000058F System.String NPOI.HSSF.Record.Crypto.RC4::ToString()
extern void RC4_ToString_mD2DD2E895695AE80B90BFE074FBAF7B429D5F853 (void);
// 0x00000590 System.Void NPOI.HSSF.Record.DateWindow1904Record::.ctor()
extern void DateWindow1904Record__ctor_m56626A46BD7A7FDC42406795699307AEEA262DCB (void);
// 0x00000591 System.Int16 NPOI.HSSF.Record.DateWindow1904Record::get_Windowing()
extern void DateWindow1904Record_get_Windowing_m6E8155C096B0B45713D236E355B058189FB590B0 (void);
// 0x00000592 System.Void NPOI.HSSF.Record.DateWindow1904Record::set_Windowing(System.Int16)
extern void DateWindow1904Record_set_Windowing_mC065FA0595787314951A80DA959A80E36CAEABEE (void);
// 0x00000593 System.String NPOI.HSSF.Record.DateWindow1904Record::ToString()
extern void DateWindow1904Record_ToString_mB7A7778C5F120EEE211FC427EDB5B4282E9612BA (void);
// 0x00000594 System.Void NPOI.HSSF.Record.DateWindow1904Record::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DateWindow1904Record_Serialize_m8E11F9511E4B8BAED68C8FB4898A3F6FB0FDF098 (void);
// 0x00000595 System.Int32 NPOI.HSSF.Record.DateWindow1904Record::get_DataSize()
extern void DateWindow1904Record_get_DataSize_mEA6046F2BCDBB5C8E8D2FA9EE41EA4785DE60831 (void);
// 0x00000596 System.Int16 NPOI.HSSF.Record.DateWindow1904Record::get_Sid()
extern void DateWindow1904Record_get_Sid_m08C598E64D56DD2D9F8AB2BDE1D6954612600115 (void);
// 0x00000597 System.Void NPOI.HSSF.Record.DBCellRecord::.ctor()
extern void DBCellRecord__ctor_mF62F11F36F3FDDFA553E05A3B384441EC8AD0139 (void);
// 0x00000598 System.Void NPOI.HSSF.Record.DBCellRecord::AddCellOffset(System.Int16)
extern void DBCellRecord_AddCellOffset_m3CBAD8DEE857365B3F28E9C040CCF03E5A2C343E (void);
// 0x00000599 System.Int32 NPOI.HSSF.Record.DBCellRecord::get_RowOffset()
extern void DBCellRecord_get_RowOffset_m0724E9BB16AAE9B0B278B3212067713926625A65 (void);
// 0x0000059A System.Void NPOI.HSSF.Record.DBCellRecord::set_RowOffset(System.Int32)
extern void DBCellRecord_set_RowOffset_mA7A28FF8BBD9625EF6A96D8F431D7E0B94E71371 (void);
// 0x0000059B System.String NPOI.HSSF.Record.DBCellRecord::ToString()
extern void DBCellRecord_ToString_mE8EAC50277C8FC5F9A6BDC297FFB6863C564E618 (void);
// 0x0000059C System.Void NPOI.HSSF.Record.DBCellRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DBCellRecord_Serialize_m816981347B0A0FCC693FBBFAA8EDC63ACEF9F1C5 (void);
// 0x0000059D System.Int32 NPOI.HSSF.Record.DBCellRecord::get_DataSize()
extern void DBCellRecord_get_DataSize_m3948DDCA413F41260A7F33025FC7750149A9BFA0 (void);
// 0x0000059E System.Int16 NPOI.HSSF.Record.DBCellRecord::get_Sid()
extern void DBCellRecord_get_Sid_mC72AE6287B686DA542A243AA36D0880AEBE9DCEC (void);
// 0x0000059F System.Object NPOI.HSSF.Record.DBCellRecord::Clone()
extern void DBCellRecord_Clone_m0292DE29BFA03563EFEBF2FFE84219A8FA3DE11D (void);
// 0x000005A0 System.Void NPOI.HSSF.Record.DefaultColWidthRecord::.ctor()
extern void DefaultColWidthRecord__ctor_m0AA364C93E64DF0FBD22A9D828EFFB9A845596D0 (void);
// 0x000005A1 System.Int32 NPOI.HSSF.Record.DefaultColWidthRecord::get_ColWidth()
extern void DefaultColWidthRecord_get_ColWidth_m3E3DA225C126FAF8A31B7B7E30017EC0261C2B9A (void);
// 0x000005A2 System.Void NPOI.HSSF.Record.DefaultColWidthRecord::set_ColWidth(System.Int32)
extern void DefaultColWidthRecord_set_ColWidth_m879E1E29FA65A7E8F8EEDF301C2D4989DAF35580 (void);
// 0x000005A3 System.String NPOI.HSSF.Record.DefaultColWidthRecord::ToString()
extern void DefaultColWidthRecord_ToString_m17AF7FB3FDFC40B19BC051C9403A782256C3A0D5 (void);
// 0x000005A4 System.Void NPOI.HSSF.Record.DefaultColWidthRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DefaultColWidthRecord_Serialize_mC7765D57BE9EDE34BBCA8EFACD8141FD024B7E49 (void);
// 0x000005A5 System.Int32 NPOI.HSSF.Record.DefaultColWidthRecord::get_DataSize()
extern void DefaultColWidthRecord_get_DataSize_m7188383F7EDF1DCFA252A42259EFCCE808BA9D81 (void);
// 0x000005A6 System.Int16 NPOI.HSSF.Record.DefaultColWidthRecord::get_Sid()
extern void DefaultColWidthRecord_get_Sid_m3878DDBAE3F316125A8779D0842631FA3D074ECD (void);
// 0x000005A7 System.Object NPOI.HSSF.Record.DefaultColWidthRecord::Clone()
extern void DefaultColWidthRecord_Clone_m4AC9466CE1631C68D2CF6A35254C7C0B131EBA18 (void);
// 0x000005A8 System.Void NPOI.HSSF.Record.DefaultRowHeightRecord::.ctor()
extern void DefaultRowHeightRecord__ctor_m5262EE87AE3A966348839352939AAEFD4DA002F5 (void);
// 0x000005A9 System.Int16 NPOI.HSSF.Record.DefaultRowHeightRecord::get_OptionFlags()
extern void DefaultRowHeightRecord_get_OptionFlags_mF171F64AF6778D8F8792DB75DB4E33A82D2A4935 (void);
// 0x000005AA System.Void NPOI.HSSF.Record.DefaultRowHeightRecord::set_OptionFlags(System.Int16)
extern void DefaultRowHeightRecord_set_OptionFlags_mA89F823AE164550DB0008A29DCBA65E09004C897 (void);
// 0x000005AB System.Int16 NPOI.HSSF.Record.DefaultRowHeightRecord::get_RowHeight()
extern void DefaultRowHeightRecord_get_RowHeight_m0E1C728301891B5F85EBBF35B90F08376DA6ECA3 (void);
// 0x000005AC System.Void NPOI.HSSF.Record.DefaultRowHeightRecord::set_RowHeight(System.Int16)
extern void DefaultRowHeightRecord_set_RowHeight_m33AC7F93E3C9F1F98DBF8D1193FBF2B9309933D1 (void);
// 0x000005AD System.String NPOI.HSSF.Record.DefaultRowHeightRecord::ToString()
extern void DefaultRowHeightRecord_ToString_mD49C663778B130766EAB1A7B9BA5DC141202510F (void);
// 0x000005AE System.Void NPOI.HSSF.Record.DefaultRowHeightRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DefaultRowHeightRecord_Serialize_mD1263D404C1ED1932C06D430C24996950FC550FE (void);
// 0x000005AF System.Int32 NPOI.HSSF.Record.DefaultRowHeightRecord::get_DataSize()
extern void DefaultRowHeightRecord_get_DataSize_m3A3FF1F6C6FDB37ADE540F7F0DAA88E09D64D75A (void);
// 0x000005B0 System.Int16 NPOI.HSSF.Record.DefaultRowHeightRecord::get_Sid()
extern void DefaultRowHeightRecord_get_Sid_m9A9897CA128F9106565437E03EF6318B578B328A (void);
// 0x000005B1 System.Object NPOI.HSSF.Record.DefaultRowHeightRecord::Clone()
extern void DefaultRowHeightRecord_Clone_m3E97E49C807D6462CC5226556B9A7084E930A317 (void);
// 0x000005B2 System.Void NPOI.HSSF.Record.DeltaRecord::.ctor(System.Double)
extern void DeltaRecord__ctor_m257F8B47A22F6B70DF6CEE5B92E10D1F0BD25E5D (void);
// 0x000005B3 System.Double NPOI.HSSF.Record.DeltaRecord::get_MaxChange()
extern void DeltaRecord_get_MaxChange_mE7EBD159EB38B310A7F41759C2916E525BE707EE (void);
// 0x000005B4 System.String NPOI.HSSF.Record.DeltaRecord::ToString()
extern void DeltaRecord_ToString_m9AC7BA3B7DC204CD5A01B0F44AF0621E7004E953 (void);
// 0x000005B5 System.Void NPOI.HSSF.Record.DeltaRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DeltaRecord_Serialize_m8CEC7B1123174140E23223B1E2734F7644ADD7D0 (void);
// 0x000005B6 System.Int32 NPOI.HSSF.Record.DeltaRecord::get_DataSize()
extern void DeltaRecord_get_DataSize_mEB6E36059600BE8C80F51FF6423914E724A51434 (void);
// 0x000005B7 System.Int16 NPOI.HSSF.Record.DeltaRecord::get_Sid()
extern void DeltaRecord_get_Sid_m3B57AF0C06D08EBAF6B3BE38E81F40D25E016E90 (void);
// 0x000005B8 System.Object NPOI.HSSF.Record.DeltaRecord::Clone()
extern void DeltaRecord_Clone_m1493A12DCDCF250CAA965581127A949660C3FBF9 (void);
// 0x000005B9 System.Void NPOI.HSSF.Record.DimensionsRecord::.ctor()
extern void DimensionsRecord__ctor_m55B27B81FD6E6F6CEE1668C004BBE16D5C621FA5 (void);
// 0x000005BA System.Int32 NPOI.HSSF.Record.DimensionsRecord::get_FirstRow()
extern void DimensionsRecord_get_FirstRow_mF065A9C854266500E5B8414CA059FC601B4AAFD5 (void);
// 0x000005BB System.Void NPOI.HSSF.Record.DimensionsRecord::set_FirstRow(System.Int32)
extern void DimensionsRecord_set_FirstRow_mEB9576556DE8D56EC062D12A682A9D700C69B87D (void);
// 0x000005BC System.Int32 NPOI.HSSF.Record.DimensionsRecord::get_LastRow()
extern void DimensionsRecord_get_LastRow_mB406DC18EF325F1E8F225B47137D30D0BFD29921 (void);
// 0x000005BD System.Void NPOI.HSSF.Record.DimensionsRecord::set_LastRow(System.Int32)
extern void DimensionsRecord_set_LastRow_m9DAC7C66E2C72F187F1FD2B483DA68421E13DE5D (void);
// 0x000005BE System.Int32 NPOI.HSSF.Record.DimensionsRecord::get_FirstCol()
extern void DimensionsRecord_get_FirstCol_m6077753F174AC1EB81A84DC1C701BFE0F9DD4487 (void);
// 0x000005BF System.Void NPOI.HSSF.Record.DimensionsRecord::set_FirstCol(System.Int32)
extern void DimensionsRecord_set_FirstCol_m62A7EE8178B817F6B286AF0C456A9AB73F45CC40 (void);
// 0x000005C0 System.Int32 NPOI.HSSF.Record.DimensionsRecord::get_LastCol()
extern void DimensionsRecord_get_LastCol_mBE15DC7E28054FEE05F7AE8483D2C6542E169EEE (void);
// 0x000005C1 System.Void NPOI.HSSF.Record.DimensionsRecord::set_LastCol(System.Int32)
extern void DimensionsRecord_set_LastCol_m923A20E111FFE82C8C5A58F233DE1FEAC47EC3EE (void);
// 0x000005C2 System.String NPOI.HSSF.Record.DimensionsRecord::ToString()
extern void DimensionsRecord_ToString_mCC8040312464EC5D2B6B18061B27D7B8059AA658 (void);
// 0x000005C3 System.Void NPOI.HSSF.Record.DimensionsRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DimensionsRecord_Serialize_m57D554ECDF13EDB856E9C1B6CD699A22844E6D5F (void);
// 0x000005C4 System.Int32 NPOI.HSSF.Record.DimensionsRecord::get_DataSize()
extern void DimensionsRecord_get_DataSize_mD9A81B94AF4DD278EB556745E062E588423975B7 (void);
// 0x000005C5 System.Int16 NPOI.HSSF.Record.DimensionsRecord::get_Sid()
extern void DimensionsRecord_get_Sid_m303FF0D738A13DBD600C5AA66EA9304DC21E174C (void);
// 0x000005C6 System.Object NPOI.HSSF.Record.DimensionsRecord::Clone()
extern void DimensionsRecord_Clone_m5380EF17977928A137842DAF491AD3D5D27E1F18 (void);
// 0x000005C7 System.Int16 NPOI.HSSF.Record.DrawingGroupRecord::get_Sid()
extern void DrawingGroupRecord_get_Sid_m2EEF7CCD893D2E19B338D30259AE0C563D75A41D (void);
// 0x000005C8 System.Void NPOI.HSSF.Record.DrawingRecord::.ctor()
extern void DrawingRecord__ctor_m16E20A9B427192F5277D12968C56137AC03D7BD4 (void);
// 0x000005C9 System.Void NPOI.HSSF.Record.DrawingRecord::ProcessContinueRecord(System.Byte[])
extern void DrawingRecord_ProcessContinueRecord_m87DC3ABB73B38636C4AEA8E1C505194B620F7A57 (void);
// 0x000005CA System.Void NPOI.HSSF.Record.DrawingRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DrawingRecord_Serialize_mFEF341873EED72DE3272AF54CC646CA8340F6850 (void);
// 0x000005CB System.Int32 NPOI.HSSF.Record.DrawingRecord::get_DataSize()
extern void DrawingRecord_get_DataSize_mDCEF5182AD0EB1007AB2273ADAA9F644DDCA2BBC (void);
// 0x000005CC System.Int16 NPOI.HSSF.Record.DrawingRecord::get_Sid()
extern void DrawingRecord_get_Sid_m808E2972C92A0EEFA8CC522C42AEC5908F3C71BA (void);
// 0x000005CD System.Object NPOI.HSSF.Record.DrawingRecord::Clone()
extern void DrawingRecord_Clone_mCF8710EA4531BCC5573460BDD89B7C90B8554C64 (void);
// 0x000005CE System.String NPOI.HSSF.Record.DrawingRecord::ToString()
extern void DrawingRecord_ToString_mA396790FFB50638F06C168F4CC49F9748E8B84C2 (void);
// 0x000005CF System.Void NPOI.HSSF.Record.DrawingRecord::.cctor()
extern void DrawingRecord__cctor_m237B0A150B4B0EC999C97B06521E414C1C544A61 (void);
// 0x000005D0 System.Int16 NPOI.HSSF.Record.DrawingSelectionRecord::get_Sid()
extern void DrawingSelectionRecord_get_Sid_m6FAA5063C8F3B2FC759F8925BDDFAF79F26C948C (void);
// 0x000005D1 System.Void NPOI.HSSF.Record.DSFRecord::.ctor(System.Int32)
extern void DSFRecord__ctor_m1E837A0FAEC4228A7861D5A33BEF3EC199C2382E (void);
// 0x000005D2 System.Void NPOI.HSSF.Record.DSFRecord::.ctor(System.Boolean)
extern void DSFRecord__ctor_mC6BB7427907839E90A848F6B96E733EFA1F4305B (void);
// 0x000005D3 System.String NPOI.HSSF.Record.DSFRecord::ToString()
extern void DSFRecord_ToString_m03B2E411AACEB2065515E381FDFE704D4553C939 (void);
// 0x000005D4 System.Void NPOI.HSSF.Record.DSFRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DSFRecord_Serialize_m751E75AF051269954B2EC282635D9CAD4D234067 (void);
// 0x000005D5 System.Int32 NPOI.HSSF.Record.DSFRecord::get_DataSize()
extern void DSFRecord_get_DataSize_m4C9434638C619DF0F31716DA136E164C59124D7D (void);
// 0x000005D6 System.Int16 NPOI.HSSF.Record.DSFRecord::get_Sid()
extern void DSFRecord_get_Sid_mBDC284B692C9B829DF96BDA14C8D774BC22C6CB9 (void);
// 0x000005D7 System.Void NPOI.HSSF.Record.DSFRecord::.cctor()
extern void DSFRecord__cctor_mF5CC112537E240E77885DEAB0AB239CE6B938BEB (void);
// 0x000005D8 System.Int16 NPOI.HSSF.Record.DVALRecord::get_Options()
extern void DVALRecord_get_Options_m62A63332204835758A9CBCB3CBD7940D41E4C1F8 (void);
// 0x000005D9 System.Int32 NPOI.HSSF.Record.DVALRecord::get_HorizontalPos()
extern void DVALRecord_get_HorizontalPos_m2A2683B44A6DF562B079AC88A8C95F43ED2AA6DF (void);
// 0x000005DA System.Int32 NPOI.HSSF.Record.DVALRecord::get_VerticalPos()
extern void DVALRecord_get_VerticalPos_mAEE635D27C56E677E5A361476F053B8A61535682 (void);
// 0x000005DB System.Int32 NPOI.HSSF.Record.DVALRecord::get_ObjectID()
extern void DVALRecord_get_ObjectID_m9AC6DF7AC67BFB3C6389B1EF833A4CFE4A25FF49 (void);
// 0x000005DC System.Int32 NPOI.HSSF.Record.DVALRecord::get_DVRecNo()
extern void DVALRecord_get_DVRecNo_mC76A2D61B3F10E925647F1BF0173E99F5739C072 (void);
// 0x000005DD System.Void NPOI.HSSF.Record.DVALRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DVALRecord_Serialize_m5FB0E1140DB615F00026FF5E84CFA023C7C3C8E6 (void);
// 0x000005DE System.Int32 NPOI.HSSF.Record.DVALRecord::get_DataSize()
extern void DVALRecord_get_DataSize_m9C15DADDEA37374CBBAC2CEE88272EE5E08132E7 (void);
// 0x000005DF System.Int16 NPOI.HSSF.Record.DVALRecord::get_Sid()
extern void DVALRecord_get_Sid_m058DED2375EF7CB3707983D32C71C2C997B14B83 (void);
// 0x000005E0 System.Void NPOI.HSSF.Record.DVRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DVRecord_Serialize_m54138FD224E1028EB4B1F9C19261A7353D6138AC (void);
// 0x000005E1 System.Void NPOI.HSSF.Record.DVRecord::SerializeUnicodeString(NPOI.HSSF.Record.UnicodeString,NPOI.Util.ILittleEndianOutput)
extern void DVRecord_SerializeUnicodeString_m8CB4962F738E11D9D9B2B591D5498B03EBC9751E (void);
// 0x000005E2 System.Int32 NPOI.HSSF.Record.DVRecord::GetUnicodeStringSize(NPOI.HSSF.Record.UnicodeString)
extern void DVRecord_GetUnicodeStringSize_mEB7957AA472765BB3AD1A7F12060A67A06203950 (void);
// 0x000005E3 System.Int32 NPOI.HSSF.Record.DVRecord::get_DataSize()
extern void DVRecord_get_DataSize_mDD5681640757EAD7E681C625D468232B200FFE8B (void);
// 0x000005E4 System.Int16 NPOI.HSSF.Record.DVRecord::get_Sid()
extern void DVRecord_get_Sid_mE2B92EDC836642A96C073E120AE7E128C52A0559 (void);
// 0x000005E5 System.Void NPOI.HSSF.Record.DVRecord::.cctor()
extern void DVRecord__cctor_m7307229CF1C528897D40652B434094FF3C5A1269 (void);
// 0x000005E6 System.Void NPOI.HSSF.Record.EOFRecord::.ctor()
extern void EOFRecord__ctor_m7E9BEA0908118BA8271AB18DB216E5E17695EC7F (void);
// 0x000005E7 System.String NPOI.HSSF.Record.EOFRecord::ToString()
extern void EOFRecord_ToString_m67D6F4BE683B1098A11FB966EBDF303C6C7CE1B1 (void);
// 0x000005E8 System.Void NPOI.HSSF.Record.EOFRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void EOFRecord_Serialize_m4B1A93C04424474BCAA8F579E33D25ADDC378799 (void);
// 0x000005E9 System.Int32 NPOI.HSSF.Record.EOFRecord::get_DataSize()
extern void EOFRecord_get_DataSize_mE21B4835D6FFFECB5BE57674253720E79DBA1AB2 (void);
// 0x000005EA System.Int16 NPOI.HSSF.Record.EOFRecord::get_Sid()
extern void EOFRecord_get_Sid_mB7A8CE02E3F0A8EA317DA7617C79392B6CC2A222 (void);
// 0x000005EB System.Object NPOI.HSSF.Record.EOFRecord::Clone()
extern void EOFRecord_Clone_m9750FE4792F39027CCB88D979F3C3A0A1BA7A0FB (void);
// 0x000005EC System.Void NPOI.HSSF.Record.EOFRecord::.cctor()
extern void EOFRecord__cctor_mE8AC4C4E505ECC4231856967953D6F8B34F21521 (void);
// 0x000005ED System.Int16 NPOI.HSSF.Record.EscherAggregate::get_Sid()
extern void EscherAggregate_get_Sid_m7638B3FE9D50FA6E776E55A40FB212634C46B5A4 (void);
// 0x000005EE System.Collections.Generic.Dictionary`2<System.Int32,NPOI.HSSF.Record.NoteRecord> NPOI.HSSF.Record.EscherAggregate::get_TailRecords()
extern void EscherAggregate_get_TailRecords_m7F652DB64D0B1C87E4E02F60816AA8F1D870E3D9 (void);
// 0x000005EF System.Void NPOI.HSSF.Record.EscherAggregate::.cctor()
extern void EscherAggregate__cctor_m916577A58D299DD66BC1F0FF1E38E7D2B0F5DC84 (void);
// 0x000005F0 System.Int16 NPOI.HSSF.Record.Excel9FileRecord::get_Sid()
extern void Excel9FileRecord_get_Sid_m13BBF02EC79AA3752592BA989BAD07584146CD71 (void);
// 0x000005F1 System.Int32 NPOI.HSSF.Record.Excel9FileRecord::get_DataSize()
extern void Excel9FileRecord_get_DataSize_mDC831EF33C558B5C05378039B1DDBB950403F2F3 (void);
// 0x000005F2 System.Void NPOI.HSSF.Record.Excel9FileRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void Excel9FileRecord_Serialize_m8DCF8F31CFDEA640C8FD1F6AE7E6419E41A740E3 (void);
// 0x000005F3 System.Void NPOI.HSSF.Record.ExtendedFormatRecord::.ctor()
extern void ExtendedFormatRecord__ctor_mB5F17730743A9CC4202B15BD651D9B220853D2DF (void);
// 0x000005F4 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_FontIndex()
extern void ExtendedFormatRecord_get_FontIndex_mF6A7988D85ACFB66664C0BA8961E60528764C1B5 (void);
// 0x000005F5 System.Void NPOI.HSSF.Record.ExtendedFormatRecord::set_FontIndex(System.Int16)
extern void ExtendedFormatRecord_set_FontIndex_m79209570BC86596E9E86CBF268FC5A8A80152583 (void);
// 0x000005F6 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_FormatIndex()
extern void ExtendedFormatRecord_get_FormatIndex_m0D8AE8CADCA73EE596476D805918ED6EB148145B (void);
// 0x000005F7 System.Void NPOI.HSSF.Record.ExtendedFormatRecord::set_FormatIndex(System.Int16)
extern void ExtendedFormatRecord_set_FormatIndex_m95D3F7AA3CE0ECC678DD21948537E3B0241AFA90 (void);
// 0x000005F8 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_CellOptions()
extern void ExtendedFormatRecord_get_CellOptions_mDFECBB909CCE54172CB21C77BBA14F15A11EB0FE (void);
// 0x000005F9 System.Void NPOI.HSSF.Record.ExtendedFormatRecord::set_CellOptions(System.Int16)
extern void ExtendedFormatRecord_set_CellOptions_m8ED7912A6DE846BCCE88E60844325F9898C5D2F9 (void);
// 0x000005FA System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_IsLocked()
extern void ExtendedFormatRecord_get_IsLocked_m714835D0023DFE97032CEAF99523824E3DF20B25 (void);
// 0x000005FB System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_IsHidden()
extern void ExtendedFormatRecord_get_IsHidden_mB389AE3480C537484EFDA217BCDD5719804A9D29 (void);
// 0x000005FC System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_XFType()
extern void ExtendedFormatRecord_get_XFType_m8CC9AB69DB5FF3755D2ED11BA2F9FAF4F523A24B (void);
// 0x000005FD System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_ParentIndex()
extern void ExtendedFormatRecord_get_ParentIndex_m15B965D5C4F9BF0E8CC4B372CD180B11F262A059 (void);
// 0x000005FE System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_AlignmentOptions()
extern void ExtendedFormatRecord_get_AlignmentOptions_mAB61554927D2EAB9CCD4C1C05A0FAE854EA720DE (void);
// 0x000005FF System.Void NPOI.HSSF.Record.ExtendedFormatRecord::set_AlignmentOptions(System.Int16)
extern void ExtendedFormatRecord_set_AlignmentOptions_mE920C132A10E4E578A8F90DD216AD9859108EDFA (void);
// 0x00000600 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_Alignment()
extern void ExtendedFormatRecord_get_Alignment_m408DD577BFE397C1F0CA54EF28F20807E6CCCE08 (void);
// 0x00000601 System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_WrapText()
extern void ExtendedFormatRecord_get_WrapText_mFB16D3C4A8774ABF7921B9A1D56C605DA6DB646C (void);
// 0x00000602 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_VerticalAlignment()
extern void ExtendedFormatRecord_get_VerticalAlignment_m14BCCD2DCE19ABAE8006C7CC396B40C12420ED56 (void);
// 0x00000603 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_JustifyLast()
extern void ExtendedFormatRecord_get_JustifyLast_m22010DC9F24695E55E9AA574963E32A2E2FE4394 (void);
// 0x00000604 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_Rotation()
extern void ExtendedFormatRecord_get_Rotation_mD2C2CCA83569B8B5654FF86BCDF602CB8BDE1B8A (void);
// 0x00000605 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_IndentionOptions()
extern void ExtendedFormatRecord_get_IndentionOptions_m0A67A648825F97BB721C660EBB744A0C21C17144 (void);
// 0x00000606 System.Void NPOI.HSSF.Record.ExtendedFormatRecord::set_IndentionOptions(System.Int16)
extern void ExtendedFormatRecord_set_IndentionOptions_m78A754F7C864DD8C9E7B6DD1FF6D8A016B8130D6 (void);
// 0x00000607 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_Indent()
extern void ExtendedFormatRecord_get_Indent_m47BF4131487657092F66EF72C7B437F50617D00E (void);
// 0x00000608 System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_ShrinkToFit()
extern void ExtendedFormatRecord_get_ShrinkToFit_m6B0C8C214A7ECE95FACD575F9E8204E6A0BA8CCF (void);
// 0x00000609 System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_MergeCells()
extern void ExtendedFormatRecord_get_MergeCells_m2DD944C1C83509507427415B0885BC73BDBB6560 (void);
// 0x0000060A System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_ReadingOrder()
extern void ExtendedFormatRecord_get_ReadingOrder_mC1A794335731265C76373FBAC8984671B9415624 (void);
// 0x0000060B System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_IsIndentNotParentFormat()
extern void ExtendedFormatRecord_get_IsIndentNotParentFormat_m8AE2E678151772B8582CBF20D194797AD10EF76A (void);
// 0x0000060C System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_IsIndentNotParentFont()
extern void ExtendedFormatRecord_get_IsIndentNotParentFont_mB4739AC997951FB3FF7CCF51FB91CCF8BFD95FDE (void);
// 0x0000060D System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_IsIndentNotParentAlignment()
extern void ExtendedFormatRecord_get_IsIndentNotParentAlignment_m2845122D857AE3124EF87462E827F89335D3612A (void);
// 0x0000060E System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_IsIndentNotParentBorder()
extern void ExtendedFormatRecord_get_IsIndentNotParentBorder_m35E05A297D6A1A74128EEE12E01ACD8174D2CC8F (void);
// 0x0000060F System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_IsIndentNotParentPattern()
extern void ExtendedFormatRecord_get_IsIndentNotParentPattern_m6FDA485A42E76480718DC8D4686B9AAF67FC3560 (void);
// 0x00000610 System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::get_IsIndentNotParentCellOptions()
extern void ExtendedFormatRecord_get_IsIndentNotParentCellOptions_m5C7E9E987BA2AC798725F9D8BB0189E1C12AF713 (void);
// 0x00000611 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_BorderOptions()
extern void ExtendedFormatRecord_get_BorderOptions_mC8587B8108EAC4CD3D626DA7F1242B793AD26339 (void);
// 0x00000612 System.Void NPOI.HSSF.Record.ExtendedFormatRecord::set_BorderOptions(System.Int16)
extern void ExtendedFormatRecord_set_BorderOptions_mD66571F8D390901E294E93D0ADF58920EA5ACD6B (void);
// 0x00000613 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_BorderLeft()
extern void ExtendedFormatRecord_get_BorderLeft_mE1581D450371753F7EFDAD923CEA78B228DE1684 (void);
// 0x00000614 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_BorderRight()
extern void ExtendedFormatRecord_get_BorderRight_m1B63E7DA9FEC715F432A183EA904AFF71EA16044 (void);
// 0x00000615 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_BorderTop()
extern void ExtendedFormatRecord_get_BorderTop_m2FC09BA9846AAA1F124973D9B2E45F07615F444D (void);
// 0x00000616 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_BorderBottom()
extern void ExtendedFormatRecord_get_BorderBottom_m94ECDC371014A9C6DC5AB3D624EBD2423CF90C26 (void);
// 0x00000617 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_PaletteOptions()
extern void ExtendedFormatRecord_get_PaletteOptions_mD962C0BB31C2F47D848707C33212FFD4CE8A2A7E (void);
// 0x00000618 System.Void NPOI.HSSF.Record.ExtendedFormatRecord::set_PaletteOptions(System.Int16)
extern void ExtendedFormatRecord_set_PaletteOptions_mE2C48C6210CF63D98C1B853F94DDD44537B28FB0 (void);
// 0x00000619 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_LeftBorderPaletteIdx()
extern void ExtendedFormatRecord_get_LeftBorderPaletteIdx_m23874E8AECA8C970CCDC72878128A6F4D518FCBA (void);
// 0x0000061A System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_RightBorderPaletteIdx()
extern void ExtendedFormatRecord_get_RightBorderPaletteIdx_mAA2A116746B7C93A051A29DEA14EB584A1EC5FF2 (void);
// 0x0000061B System.Int32 NPOI.HSSF.Record.ExtendedFormatRecord::get_AdtlPaletteOptions()
extern void ExtendedFormatRecord_get_AdtlPaletteOptions_mE779A0C00DDF0BC6D3B53CE73899BFB41736113F (void);
// 0x0000061C System.Void NPOI.HSSF.Record.ExtendedFormatRecord::set_AdtlPaletteOptions(System.Int32)
extern void ExtendedFormatRecord_set_AdtlPaletteOptions_mA2D5AB24CDB3F9280658B4A9CB9793702691F477 (void);
// 0x0000061D System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_TopBorderPaletteIdx()
extern void ExtendedFormatRecord_get_TopBorderPaletteIdx_mF4F52BB25689805BE538D626673527B7047470D2 (void);
// 0x0000061E System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_BottomBorderPaletteIdx()
extern void ExtendedFormatRecord_get_BottomBorderPaletteIdx_m1A8FFDD425513FBF44627C56A8835F5C9FC6E4B0 (void);
// 0x0000061F System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_AdtlDiagBorderPaletteIdx()
extern void ExtendedFormatRecord_get_AdtlDiagBorderPaletteIdx_m693FEC40305F43175EA93D988E7062F3436821DE (void);
// 0x00000620 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_AdtlDiagLineStyle()
extern void ExtendedFormatRecord_get_AdtlDiagLineStyle_m177493DC0595600859E3B6288718D3C66C31BF9D (void);
// 0x00000621 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_Diagonal()
extern void ExtendedFormatRecord_get_Diagonal_m55025B14AE2A515201ACCC782B817A6C9FDBF5C5 (void);
// 0x00000622 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_AdtlFillPattern()
extern void ExtendedFormatRecord_get_AdtlFillPattern_mE4EBE67FCEFA87A9FC9107DE5229C50FFC760A4A (void);
// 0x00000623 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_FillPaletteOptions()
extern void ExtendedFormatRecord_get_FillPaletteOptions_mDBCDA362AED751E008FE047AC345A057383BEE16 (void);
// 0x00000624 System.Void NPOI.HSSF.Record.ExtendedFormatRecord::set_FillPaletteOptions(System.Int16)
extern void ExtendedFormatRecord_set_FillPaletteOptions_m3BFC29858984083D9E3684C074D297A24DA635E3 (void);
// 0x00000625 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_FillForeground()
extern void ExtendedFormatRecord_get_FillForeground_mAA5A7517B0F47AE6FF7B0E44EAEFDD8D8E3D11C3 (void);
// 0x00000626 System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_FillBackground()
extern void ExtendedFormatRecord_get_FillBackground_m248B706AC0D6346DD48D8978F8B6A6DD8C81583C (void);
// 0x00000627 System.String NPOI.HSSF.Record.ExtendedFormatRecord::ToString()
extern void ExtendedFormatRecord_ToString_mAC0D98078AC68F5067998B3133EE326D36F46AD4 (void);
// 0x00000628 System.Void NPOI.HSSF.Record.ExtendedFormatRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ExtendedFormatRecord_Serialize_m3769E09500258F6F983606F180C5E4E1F34251A2 (void);
// 0x00000629 System.Int32 NPOI.HSSF.Record.ExtendedFormatRecord::get_DataSize()
extern void ExtendedFormatRecord_get_DataSize_mB13E203C64BDAA03740D01666230681FBFD1D20A (void);
// 0x0000062A System.Int16 NPOI.HSSF.Record.ExtendedFormatRecord::get_Sid()
extern void ExtendedFormatRecord_get_Sid_m534740EC90A8527468414852D74729680C27BDE7 (void);
// 0x0000062B System.Int32 NPOI.HSSF.Record.ExtendedFormatRecord::GetHashCode()
extern void ExtendedFormatRecord_GetHashCode_mD6E3CC3C02C70B6858F352E0C350A2CF29A1A5A4 (void);
// 0x0000062C System.Boolean NPOI.HSSF.Record.ExtendedFormatRecord::Equals(System.Object)
extern void ExtendedFormatRecord_Equals_m133487B0C1A885D057773E9AEFBD7ED9888BDA1E (void);
// 0x0000062D System.Void NPOI.HSSF.Record.ExtendedFormatRecord::.cctor()
extern void ExtendedFormatRecord__cctor_mC02A4BAF99272AA4612DBA8790407ECDECEB1319 (void);
// 0x0000062E System.Void NPOI.HSSF.Record.ExternalNameRecord::.ctor()
extern void ExternalNameRecord__ctor_mA08F4771A5CDBC88540B51B338382C8698132FD6 (void);
// 0x0000062F System.Boolean NPOI.HSSF.Record.ExternalNameRecord::get_IsAutomaticLink()
extern void ExternalNameRecord_get_IsAutomaticLink_mF2AE75D6374EE53DE5859A6C6F71F40036C551AB (void);
// 0x00000630 System.Boolean NPOI.HSSF.Record.ExternalNameRecord::get_IsStdDocumentNameIdentifier()
extern void ExternalNameRecord_get_IsStdDocumentNameIdentifier_m4F64D24333568269DE9F5506BAAE2CF9526263F0 (void);
// 0x00000631 System.Boolean NPOI.HSSF.Record.ExternalNameRecord::get_IsOLELink()
extern void ExternalNameRecord_get_IsOLELink_mBADDCD59777CC0E25D9A5FA723B26857D35E03EF (void);
// 0x00000632 System.String NPOI.HSSF.Record.ExternalNameRecord::get_Text()
extern void ExternalNameRecord_get_Text_m4C9BC3AE71CB052FE557126C22E7658A94A0F6F8 (void);
// 0x00000633 System.Void NPOI.HSSF.Record.ExternalNameRecord::set_Text(System.String)
extern void ExternalNameRecord_set_Text_mB77EA01F511CF5D384358D0BB535F6AAEFC4BCC1 (void);
// 0x00000634 System.Void NPOI.HSSF.Record.ExternalNameRecord::SetParsedExpression(NPOI.SS.Formula.PTG.Ptg[])
extern void ExternalNameRecord_SetParsedExpression_m450F9EE6E6ADA2FB88BB0D0DC61BAAEF1C3EC082 (void);
// 0x00000635 System.Int32 NPOI.HSSF.Record.ExternalNameRecord::get_DataSize()
extern void ExternalNameRecord_get_DataSize_m4F11FE1981E2E3A5E0AB82BE28AF41EFDBC72756 (void);
// 0x00000636 System.Void NPOI.HSSF.Record.ExternalNameRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ExternalNameRecord_Serialize_mE001C36A8E79AEEC97F3C3B3D6A2891A583E6F3D (void);
// 0x00000637 System.Int16 NPOI.HSSF.Record.ExternalNameRecord::get_Sid()
extern void ExternalNameRecord_get_Sid_mC67D24AFF956A2CA3F5AD1282A32E6C839D828EF (void);
// 0x00000638 System.String NPOI.HSSF.Record.ExternalNameRecord::ToString()
extern void ExternalNameRecord_ToString_m9B36882BAA91E46B2BA279619B1FD20B5C7FA1DA (void);
// 0x00000639 System.Void NPOI.HSSF.Record.RefSubRecord::.ctor(System.Int32,System.Int32,System.Int32)
extern void RefSubRecord__ctor_m298CE0B55AADCB6666D35FFBF75FD5C0E309E215 (void);
// 0x0000063A System.Int32 NPOI.HSSF.Record.RefSubRecord::get_ExtBookIndex()
extern void RefSubRecord_get_ExtBookIndex_m8D2A29319000B59CA8F32C9F94C4A4716540E576 (void);
// 0x0000063B System.Int32 NPOI.HSSF.Record.RefSubRecord::get_FirstSheetIndex()
extern void RefSubRecord_get_FirstSheetIndex_m29ECB38AF49F94F921130B76ACF1764E69F6DAEA (void);
// 0x0000063C System.Int32 NPOI.HSSF.Record.RefSubRecord::get_LastSheetIndex()
extern void RefSubRecord_get_LastSheetIndex_m5B676F858ABA668B3476E65F18994C4DEE2133C9 (void);
// 0x0000063D System.String NPOI.HSSF.Record.RefSubRecord::ToString()
extern void RefSubRecord_ToString_m714D33B1C537FE094A8747027DCC6F1D64134804 (void);
// 0x0000063E System.Void NPOI.HSSF.Record.RefSubRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void RefSubRecord_Serialize_mEFB1E5213C8E56F4E8B3A6C55A3D788F92B69348 (void);
// 0x0000063F System.Void NPOI.HSSF.Record.ExternSheetRecord::.ctor()
extern void ExternSheetRecord__ctor_m93BF5FEFFBCFE66A2B07F17706B10495FEDFDD46 (void);
// 0x00000640 System.Int32 NPOI.HSSF.Record.ExternSheetRecord::AddRef(System.Int32,System.Int32,System.Int32)
extern void ExternSheetRecord_AddRef_m16A63EA71899F956A5ABC40E9F80E37E8970F5FB (void);
// 0x00000641 System.Int32 NPOI.HSSF.Record.ExternSheetRecord::GetRefIxForSheet(System.Int32,System.Int32)
extern void ExternSheetRecord_GetRefIxForSheet_m9CA8C02CD0B1AB70F85AF9CA787C0666689B9AE9 (void);
// 0x00000642 System.Int32 NPOI.HSSF.Record.ExternSheetRecord::get_NumOfREFRecords()
extern void ExternSheetRecord_get_NumOfREFRecords_m7B5E842F344839A916488A55AAFD1270948B83DF (void);
// 0x00000643 System.Void NPOI.HSSF.Record.ExternSheetRecord::AddREFRecord(NPOI.HSSF.Record.RefSubRecord)
extern void ExternSheetRecord_AddREFRecord_mE93A4D61874F29D6EBE116579FA8EF7995E8E2E6 (void);
// 0x00000644 NPOI.HSSF.Record.RefSubRecord NPOI.HSSF.Record.ExternSheetRecord::GetRef(System.Int32)
extern void ExternSheetRecord_GetRef_m82313526D07ADE3C26DA6E87388D39E36D5F1EBE (void);
// 0x00000645 System.Int32 NPOI.HSSF.Record.ExternSheetRecord::GetExtbookIndexFromRefIndex(System.Int32)
extern void ExternSheetRecord_GetExtbookIndexFromRefIndex_m4207CA3ABA879DED341BB58D0E99B849AD8FEC2B (void);
// 0x00000646 System.Int32 NPOI.HSSF.Record.ExternSheetRecord::FindRefIndexFromExtBookIndex(System.Int32)
extern void ExternSheetRecord_FindRefIndexFromExtBookIndex_m1069F3260288DB9D9E78609F1ACDD9BE8F51AF8F (void);
// 0x00000647 NPOI.HSSF.Record.ExternSheetRecord NPOI.HSSF.Record.ExternSheetRecord::Combine(NPOI.HSSF.Record.ExternSheetRecord[])
extern void ExternSheetRecord_Combine_mE8BEF4FF3163972218F3E016C70F6FEE5D009B4D (void);
// 0x00000648 System.Int32 NPOI.HSSF.Record.ExternSheetRecord::GetFirstSheetIndexFromRefIndex(System.Int32)
extern void ExternSheetRecord_GetFirstSheetIndexFromRefIndex_mEC33E57D7F9B63F54B79B836285316A663D21596 (void);
// 0x00000649 System.String NPOI.HSSF.Record.ExternSheetRecord::ToString()
extern void ExternSheetRecord_ToString_m17FD01785FCACA0BDFC7B74D5CC4BFFCE312E54B (void);
// 0x0000064A System.Void NPOI.HSSF.Record.ExternSheetRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ExternSheetRecord_Serialize_m4CE17E7805786730F815925816FB522887282C21 (void);
// 0x0000064B System.Int32 NPOI.HSSF.Record.ExternSheetRecord::get_DataSize()
extern void ExternSheetRecord_get_DataSize_mAA5B42E649DD96A3A17D4A189DEF507561A17377 (void);
// 0x0000064C System.Int16 NPOI.HSSF.Record.ExternSheetRecord::get_Sid()
extern void ExternSheetRecord_get_Sid_m9620774FAE5D4E85608CE978DC349C7C7A3652A3 (void);
// 0x0000064D System.Void NPOI.HSSF.Record.InfoSubRecord::.ctor(System.Int32,System.Int32)
extern void InfoSubRecord__ctor_mC9D032BB1EFD44288A1A9D5E7D8196546414AD1E (void);
// 0x0000064E System.Int32 NPOI.HSSF.Record.InfoSubRecord::get_StreamPos()
extern void InfoSubRecord_get_StreamPos_m4095800DD9E662871A910070301635A85737A980 (void);
// 0x0000064F System.Int32 NPOI.HSSF.Record.InfoSubRecord::get_BucketSSTOffset()
extern void InfoSubRecord_get_BucketSSTOffset_mC6CFACDBB9C40485F40FA5D7487749F7BF1814F1 (void);
// 0x00000650 System.String NPOI.HSSF.Record.InfoSubRecord::ToString()
extern void InfoSubRecord_ToString_mFFAD461E9512C8299FC7FACC7650992659BF1F99 (void);
// 0x00000651 System.Void NPOI.HSSF.Record.InfoSubRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void InfoSubRecord_Serialize_m04A98BD625BF7D0AC466D7DFC14C6A56ABAEFA5A (void);
// 0x00000652 System.Void NPOI.HSSF.Record.ExtSSTRecord::.ctor()
extern void ExtSSTRecord__ctor_m96646D84DC69B11ADF2DBC43539D510D3B959BB2 (void);
// 0x00000653 System.Int16 NPOI.HSSF.Record.ExtSSTRecord::get_NumStringsPerBucket()
extern void ExtSSTRecord_get_NumStringsPerBucket_m8769C5DFA460C39F9C2D2A5C6032F52A60BC8D13 (void);
// 0x00000654 System.Void NPOI.HSSF.Record.ExtSSTRecord::set_NumStringsPerBucket(System.Int16)
extern void ExtSSTRecord_set_NumStringsPerBucket_mA2D566FDFD11C98DCF2655E7599B0E7861513C7F (void);
// 0x00000655 System.String NPOI.HSSF.Record.ExtSSTRecord::ToString()
extern void ExtSSTRecord_ToString_m8DECCAE19C6B99701780C8561C18F6FD8784C105 (void);
// 0x00000656 System.Void NPOI.HSSF.Record.ExtSSTRecord::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void ExtSSTRecord_Serialize_mCEDDA02DDF270CFFE0C4B2D8717835D74A3A40CE (void);
// 0x00000657 System.Int32 NPOI.HSSF.Record.ExtSSTRecord::GetNumberOfInfoRecsForStrings(System.Int32)
extern void ExtSSTRecord_GetNumberOfInfoRecsForStrings_m4E90946085491598338EACA47A3C3D024071CCF0 (void);
// 0x00000658 System.Int32 NPOI.HSSF.Record.ExtSSTRecord::GetRecordSizeForStrings(System.Int32)
extern void ExtSSTRecord_GetRecordSizeForStrings_m9E56C3A12C9B727C10BBA129CC7DB7AC28A4945F (void);
// 0x00000659 System.Int16 NPOI.HSSF.Record.ExtSSTRecord::get_Sid()
extern void ExtSSTRecord_get_Sid_m1016E9DF1DCB74FB1EC08406768B3A52AA036219 (void);
// 0x0000065A System.Void NPOI.HSSF.Record.ExtSSTRecord::SetBucketOffsets(System.Int32[],System.Int32[])
extern void ExtSSTRecord_SetBucketOffsets_mDEA8DA87B5DC589DC4E663CD8EC58DF057DCB4A7 (void);
// 0x0000065B System.Void NPOI.HSSF.Record.FilePassRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FilePassRecord_Serialize_m3051CE15C86D51FD60ED4F185A20E3673487BD39 (void);
// 0x0000065C System.Int32 NPOI.HSSF.Record.FilePassRecord::get_DataSize()
extern void FilePassRecord_get_DataSize_m576D28BC3EA6DF34483CB9DC897F18716D77BD9D (void);
// 0x0000065D System.Byte[] NPOI.HSSF.Record.FilePassRecord::get_DocId()
extern void FilePassRecord_get_DocId_m3780F9E63F1AB0D8CFA7594308227E38B520F8C9 (void);
// 0x0000065E System.Byte[] NPOI.HSSF.Record.FilePassRecord::get_SaltData()
extern void FilePassRecord_get_SaltData_m72419A244257CF207EE1A5597A915A805C4F3D40 (void);
// 0x0000065F System.Byte[] NPOI.HSSF.Record.FilePassRecord::get_SaltHash()
extern void FilePassRecord_get_SaltHash_m896D3947A7525B6F551CE4F2496C18C107079B2D (void);
// 0x00000660 System.Int16 NPOI.HSSF.Record.FilePassRecord::get_Sid()
extern void FilePassRecord_get_Sid_mBCB2F9B2FC4AE61D1F645CFE8B062E2EDAF95AA1 (void);
// 0x00000661 System.Int16 NPOI.HSSF.Record.FileSharingRecord::get_ReadOnly()
extern void FileSharingRecord_get_ReadOnly_m91E552D494CBE4D675A6E15D0167E21ED6C61B41 (void);
// 0x00000662 System.Int16 NPOI.HSSF.Record.FileSharingRecord::get_Password()
extern void FileSharingRecord_get_Password_mFE888D24EF9E959F9ADEF73157955FBEE268321A (void);
// 0x00000663 System.String NPOI.HSSF.Record.FileSharingRecord::get_Username()
extern void FileSharingRecord_get_Username_m5D7D953C7EE92F0FD134C43311C3F827AD7CD4CA (void);
// 0x00000664 System.Void NPOI.HSSF.Record.FileSharingRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FileSharingRecord_Serialize_mC3D2BC459061352EB29AA59A641C382232CF7735 (void);
// 0x00000665 System.Int32 NPOI.HSSF.Record.FileSharingRecord::get_DataSize()
extern void FileSharingRecord_get_DataSize_m5AE7B2D1EA501E472C30D6EA8EA5A866C562FAD1 (void);
// 0x00000666 System.Int16 NPOI.HSSF.Record.FileSharingRecord::get_Sid()
extern void FileSharingRecord_get_Sid_mF470085217314C9F6B07817B892F9E1A47466530 (void);
// 0x00000667 System.Void NPOI.HSSF.Record.FnGroupCountRecord::.ctor()
extern void FnGroupCountRecord__ctor_m1735BD5F568B4E23B327D6BFA96C55060E2A880A (void);
// 0x00000668 System.Int16 NPOI.HSSF.Record.FnGroupCountRecord::get_Count()
extern void FnGroupCountRecord_get_Count_m4F9A0AA6E4253364E256AF6CB322FCFEAF2E0103 (void);
// 0x00000669 System.Void NPOI.HSSF.Record.FnGroupCountRecord::set_Count(System.Int16)
extern void FnGroupCountRecord_set_Count_mF12ADA329E1C4F0168836D24872B4FC50B30B93E (void);
// 0x0000066A System.String NPOI.HSSF.Record.FnGroupCountRecord::ToString()
extern void FnGroupCountRecord_ToString_m1DC31C876F60694CCBF723382D53DF2832F6C2C3 (void);
// 0x0000066B System.Void NPOI.HSSF.Record.FnGroupCountRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FnGroupCountRecord_Serialize_mE4711235EFCBE41DB6BA3C7C5207D4394E1ED7EC (void);
// 0x0000066C System.Int32 NPOI.HSSF.Record.FnGroupCountRecord::get_DataSize()
extern void FnGroupCountRecord_get_DataSize_mF40614533D8DC41441E1344147790E5510BA311D (void);
// 0x0000066D System.Int16 NPOI.HSSF.Record.FnGroupCountRecord::get_Sid()
extern void FnGroupCountRecord_get_Sid_mBF42D71D7691A7458AAA6497D8DB2333AD485DE7 (void);
// 0x0000066E System.Void NPOI.HSSF.Record.FontRecord::.ctor()
extern void FontRecord__ctor_m8E897F3F601E95D9D2A3B03CFFAA96CEFF472107 (void);
// 0x0000066F System.Boolean NPOI.HSSF.Record.FontRecord::get_IsItalic()
extern void FontRecord_get_IsItalic_m202C14A6344B77A7E471D69BEC2ED38000511981 (void);
// 0x00000670 System.Boolean NPOI.HSSF.Record.FontRecord::get_IsStrikeout()
extern void FontRecord_get_IsStrikeout_mD61C402955F710D6C1C8F04AF6ED98A2FF161C0B (void);
// 0x00000671 System.Boolean NPOI.HSSF.Record.FontRecord::get_IsMacoutlined()
extern void FontRecord_get_IsMacoutlined_mA1CE8433E9C529DFB0A8FD9AEC83DD8321289BB9 (void);
// 0x00000672 System.Boolean NPOI.HSSF.Record.FontRecord::get_IsMacshadowed()
extern void FontRecord_get_IsMacshadowed_mE4C7BBFD9AB05FF471B5D8BA11C4610C365C3905 (void);
// 0x00000673 NPOI.SS.UserModel.FontUnderlineType NPOI.HSSF.Record.FontRecord::get_Underline()
extern void FontRecord_get_Underline_m0F543BBD7D500D4BE73D60C9DABD684830585BCA (void);
// 0x00000674 System.Byte NPOI.HSSF.Record.FontRecord::get_Family()
extern void FontRecord_get_Family_m744B91D419746A4C4F63B67539741644DB71A263 (void);
// 0x00000675 System.Byte NPOI.HSSF.Record.FontRecord::get_Charset()
extern void FontRecord_get_Charset_m44A0444C2E57EB7CD68CB98FE8F19A13AA907705 (void);
// 0x00000676 System.Void NPOI.HSSF.Record.FontRecord::set_FontName(System.String)
extern void FontRecord_set_FontName_m2C297BA4D4DF011090745B627A2B0D0EEF30FA8D (void);
// 0x00000677 System.String NPOI.HSSF.Record.FontRecord::get_FontName()
extern void FontRecord_get_FontName_mD55244AE14FD2CF032AE8B56440F2CAF27278939 (void);
// 0x00000678 System.Int16 NPOI.HSSF.Record.FontRecord::get_FontHeight()
extern void FontRecord_get_FontHeight_m1B9DC127AEED82205D063D38AE0A9274ECD0E167 (void);
// 0x00000679 System.Void NPOI.HSSF.Record.FontRecord::set_FontHeight(System.Int16)
extern void FontRecord_set_FontHeight_m7A01E11F32B36D296A793B992FFBC40A6B71AE4C (void);
// 0x0000067A System.Int16 NPOI.HSSF.Record.FontRecord::get_Attributes()
extern void FontRecord_get_Attributes_mDC98F6F0F40E771D1C8000D93CC2E9096B9777A0 (void);
// 0x0000067B System.Void NPOI.HSSF.Record.FontRecord::set_Attributes(System.Int16)
extern void FontRecord_set_Attributes_m02054F6403ADBBFF78DFA0EB486098B3F930EA91 (void);
// 0x0000067C System.Int16 NPOI.HSSF.Record.FontRecord::get_ColorPaletteIndex()
extern void FontRecord_get_ColorPaletteIndex_mE992DA3A9A9ECE7514CA79188723A418D8FCFB32 (void);
// 0x0000067D System.Void NPOI.HSSF.Record.FontRecord::set_ColorPaletteIndex(System.Int16)
extern void FontRecord_set_ColorPaletteIndex_mE87BE4F0191D3269CC8A80CAD0231AA6BD5F1C68 (void);
// 0x0000067E System.Int16 NPOI.HSSF.Record.FontRecord::get_BoldWeight()
extern void FontRecord_get_BoldWeight_m2F4C74684E3E810C063619E1E953B00575ADC258 (void);
// 0x0000067F System.Void NPOI.HSSF.Record.FontRecord::set_BoldWeight(System.Int16)
extern void FontRecord_set_BoldWeight_mDB162383D0D707C021878DEB3EA46739D4E4B3B3 (void);
// 0x00000680 NPOI.SS.UserModel.FontSuperScript NPOI.HSSF.Record.FontRecord::get_SuperSubScript()
extern void FontRecord_get_SuperSubScript_m10DA594482D4C5D1E87D95E823A35806E4A3C232 (void);
// 0x00000681 System.String NPOI.HSSF.Record.FontRecord::ToString()
extern void FontRecord_ToString_m6FFDCA956431C7E4F4ACDEACA5706B8B8AAC7F6F (void);
// 0x00000682 System.Void NPOI.HSSF.Record.FontRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FontRecord_Serialize_mC0DE23A58286627EDD87B492637D7F1166CC6632 (void);
// 0x00000683 System.Int32 NPOI.HSSF.Record.FontRecord::get_DataSize()
extern void FontRecord_get_DataSize_m759F02B007E8F31EF0C2934676C83B6EB9EA05C3 (void);
// 0x00000684 System.Int16 NPOI.HSSF.Record.FontRecord::get_Sid()
extern void FontRecord_get_Sid_m66AB9B9321BCB866F86C11BD96AC08EEC832256D (void);
// 0x00000685 System.Int32 NPOI.HSSF.Record.FontRecord::GetHashCode()
extern void FontRecord_GetHashCode_m8F62D3767D790D89F64FC77425A1D44EC439E780 (void);
// 0x00000686 System.Boolean NPOI.HSSF.Record.FontRecord::Equals(System.Object)
extern void FontRecord_Equals_m619EE54A6D7533674EA52784EE5746D72744CFEA (void);
// 0x00000687 System.Void NPOI.HSSF.Record.FontRecord::.cctor()
extern void FontRecord__cctor_mA09719A0A23C0BD9DBC543E3D68CDDA006631E9C (void);
// 0x00000688 System.Void NPOI.HSSF.Record.HeaderFooterBase::.ctor(System.String)
extern void HeaderFooterBase__ctor_m7BE8491789EC9131D5F9973D9D7AAECC3AF179B5 (void);
// 0x00000689 System.Int32 NPOI.HSSF.Record.HeaderFooterBase::get_TextLength()
extern void HeaderFooterBase_get_TextLength_mC7E24E6891166B59E6076280E9537CF48A796AEB (void);
// 0x0000068A System.String NPOI.HSSF.Record.HeaderFooterBase::get_Text()
extern void HeaderFooterBase_get_Text_m17137CB328E57BEC6B9DB2B5C78B412B3937051E (void);
// 0x0000068B System.Void NPOI.HSSF.Record.HeaderFooterBase::set_Text(System.String)
extern void HeaderFooterBase_set_Text_mC64ED03A90167998BC6AF290DE39E30E3AAB01BF (void);
// 0x0000068C System.Void NPOI.HSSF.Record.HeaderFooterBase::Serialize(NPOI.Util.ILittleEndianOutput)
extern void HeaderFooterBase_Serialize_m35A50553047596CDA37F913EF34DE1D8842683C3 (void);
// 0x0000068D System.Int32 NPOI.HSSF.Record.HeaderFooterBase::get_DataSize()
extern void HeaderFooterBase_get_DataSize_m8FBC3FFC6512AFC4FD2C2206605AAA337CFC8E44 (void);
// 0x0000068E System.Void NPOI.HSSF.Record.FooterRecord::.ctor(System.String)
extern void FooterRecord__ctor_m4AAF3DCF1C51208DA0A450E53C4085BCA96D61B4 (void);
// 0x0000068F System.String NPOI.HSSF.Record.FooterRecord::ToString()
extern void FooterRecord_ToString_m29E377CA4BD0F68D197DAB317273A7F37C8D888C (void);
// 0x00000690 System.Int16 NPOI.HSSF.Record.FooterRecord::get_Sid()
extern void FooterRecord_get_Sid_mDCB33B5C9CF66D555758551901B665AB055E0B83 (void);
// 0x00000691 System.Object NPOI.HSSF.Record.FooterRecord::Clone()
extern void FooterRecord_Clone_m27383BE767A8DBA803BDD4E584AF0F9D53FC3276 (void);
// 0x00000692 System.Void NPOI.HSSF.Record.FormatRecord::.ctor(System.Int32,System.String)
extern void FormatRecord__ctor_m94CAD2512D7B725019634FC9AAD67B2658740DA7 (void);
// 0x00000693 System.Int32 NPOI.HSSF.Record.FormatRecord::get_IndexCode()
extern void FormatRecord_get_IndexCode_m72F486E28462D0E7904BB786275BD51E2EB09505 (void);
// 0x00000694 System.String NPOI.HSSF.Record.FormatRecord::get_FormatString()
extern void FormatRecord_get_FormatString_m1091C9B9EED04FD6DFF6D9145E9F3EF75E2313D6 (void);
// 0x00000695 System.String NPOI.HSSF.Record.FormatRecord::ToString()
extern void FormatRecord_ToString_mE8EBFA8B575A6CFFF3EDDA4CB8EF18298C5DBFF5 (void);
// 0x00000696 System.Void NPOI.HSSF.Record.FormatRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FormatRecord_Serialize_mFAD5D3B5CD0C803F43D9472A1DC936FF04F37397 (void);
// 0x00000697 System.Int32 NPOI.HSSF.Record.FormatRecord::get_DataSize()
extern void FormatRecord_get_DataSize_m9AAA35E72BF58FE83DEFC486F74B1CA07D821ABF (void);
// 0x00000698 System.Int16 NPOI.HSSF.Record.FormatRecord::get_Sid()
extern void FormatRecord_get_Sid_mF251093D932E49790EE8ACA455A484EA03B22376 (void);
// 0x00000699 System.Object NPOI.HSSF.Record.FormatRecord::Clone()
extern void FormatRecord_Clone_mDF25BEF3F0F5F9C7CB62AC1E86E4F1823D818CB9 (void);
// 0x0000069A System.Void NPOI.HSSF.Record.SpecialCachedValue::.ctor(System.Byte[])
extern void SpecialCachedValue__ctor_mF301DF49A8E18803C8FC2ADC2D03B262CDCB4637 (void);
// 0x0000069B System.Int32 NPOI.HSSF.Record.SpecialCachedValue::GetTypeCode()
extern void SpecialCachedValue_GetTypeCode_m0D7605C12D5119BA22F2A8472453B73213AE3CEF (void);
// 0x0000069C System.Void NPOI.HSSF.Record.SpecialCachedValue::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SpecialCachedValue_Serialize_mA17E9C114D6CB1612D4A2FD22687DC31BF8CFEAD (void);
// 0x0000069D System.String NPOI.HSSF.Record.SpecialCachedValue::get_FormatDebugString()
extern void SpecialCachedValue_get_FormatDebugString_mFD5305446A25B9BCDCC59DC15F329E6EC199C9BA (void);
// 0x0000069E System.String NPOI.HSSF.Record.SpecialCachedValue::get_FormatValue()
extern void SpecialCachedValue_get_FormatValue_mF7206E18CBE0A99AD168C3F2193326E8B9AA752D (void);
// 0x0000069F System.Int32 NPOI.HSSF.Record.SpecialCachedValue::get_DataValue()
extern void SpecialCachedValue_get_DataValue_m4D68C71CCD41843CA1CB9540A8BEB7420BAFD091 (void);
// 0x000006A0 NPOI.HSSF.Record.SpecialCachedValue NPOI.HSSF.Record.SpecialCachedValue::CreateCachedEmptyValue()
extern void SpecialCachedValue_CreateCachedEmptyValue_m3F9813DFAC2435B2F185930138FD293E23F954DB (void);
// 0x000006A1 NPOI.HSSF.Record.SpecialCachedValue NPOI.HSSF.Record.SpecialCachedValue::CreateForString()
extern void SpecialCachedValue_CreateForString_m1A8B0FE9CF8F949D9422822D89D4D0F0B218F51C (void);
// 0x000006A2 NPOI.HSSF.Record.SpecialCachedValue NPOI.HSSF.Record.SpecialCachedValue::CreateCachedBoolean(System.Boolean)
extern void SpecialCachedValue_CreateCachedBoolean_m1CFC66E74081E472350977449BBC1F01E7526123 (void);
// 0x000006A3 NPOI.HSSF.Record.SpecialCachedValue NPOI.HSSF.Record.SpecialCachedValue::CreateCachedErrorCode(System.Int32)
extern void SpecialCachedValue_CreateCachedErrorCode_m8E904E577E0662AF098F60A478E7BFA190A9941E (void);
// 0x000006A4 NPOI.HSSF.Record.SpecialCachedValue NPOI.HSSF.Record.SpecialCachedValue::Create(System.Int32,System.Int32)
extern void SpecialCachedValue_Create_mDCD0F037E38447328C10B28D4309D11AAE664C5A (void);
// 0x000006A5 System.String NPOI.HSSF.Record.SpecialCachedValue::ToString()
extern void SpecialCachedValue_ToString_m911568568225C9C378A610C4CCEC505AFFC9E0EE (void);
// 0x000006A6 NPOI.SS.UserModel.CellType NPOI.HSSF.Record.SpecialCachedValue::GetValueType()
extern void SpecialCachedValue_GetValueType_mEBCC10DAB1AD877084DD63096E6341E847760AE9 (void);
// 0x000006A7 System.Boolean NPOI.HSSF.Record.SpecialCachedValue::GetBooleanValue()
extern void SpecialCachedValue_GetBooleanValue_mA7E99C446DA240CC327799F532B13A1B6E9D227E (void);
// 0x000006A8 System.Int32 NPOI.HSSF.Record.SpecialCachedValue::GetErrorValue()
extern void SpecialCachedValue_GetErrorValue_mC95AA902EA71CC75EDBDE893B6107534183F99FA (void);
// 0x000006A9 System.Void NPOI.HSSF.Record.FormulaRecord::.ctor()
extern void FormulaRecord__ctor_m5C2A29C86D01F1986003FC2F56BB5046C9854F62 (void);
// 0x000006AA System.Boolean NPOI.HSSF.Record.FormulaRecord::get_HasCachedResultString()
extern void FormulaRecord_get_HasCachedResultString_mA98433BB779FEFBF88BCFBB02F7162D0BCBE0D26 (void);
// 0x000006AB System.Void NPOI.HSSF.Record.FormulaRecord::SetParsedExpression(NPOI.SS.Formula.PTG.Ptg[])
extern void FormulaRecord_SetParsedExpression_m3E3F9E3A88F7DCF13D203BF96F9042C8A21548C6 (void);
// 0x000006AC System.Void NPOI.HSSF.Record.FormulaRecord::SetSharedFormula(System.Boolean)
extern void FormulaRecord_SetSharedFormula_mA21166DA33CE38F691BF93E600801090B25D9F63 (void);
// 0x000006AD System.Double NPOI.HSSF.Record.FormulaRecord::get_Value()
extern void FormulaRecord_get_Value_mE6CE2ABC2AF206B68388EB888E5AFE1AD26E2D21 (void);
// 0x000006AE System.Void NPOI.HSSF.Record.FormulaRecord::set_Value(System.Double)
extern void FormulaRecord_set_Value_m082EFE2633A41A6B50A46787CBA8B79BB4A3B2AD (void);
// 0x000006AF System.Int16 NPOI.HSSF.Record.FormulaRecord::get_Options()
extern void FormulaRecord_get_Options_mDB8CE731FC6032126970152730624813EDB6FB23 (void);
// 0x000006B0 System.Boolean NPOI.HSSF.Record.FormulaRecord::get_IsSharedFormula()
extern void FormulaRecord_get_IsSharedFormula_mE9DB60987C8FD24C2D83FF7800C28BC6280BA6AF (void);
// 0x000006B1 System.Void NPOI.HSSF.Record.FormulaRecord::set_IsSharedFormula(System.Boolean)
extern void FormulaRecord_set_IsSharedFormula_mD232AA9EAE5460F38265920E1B2B39AFC5A8CCF7 (void);
// 0x000006B2 NPOI.SS.Formula.PTG.Ptg[] NPOI.HSSF.Record.FormulaRecord::get_ParsedExpression()
extern void FormulaRecord_get_ParsedExpression_m8C0654D07CAEA7E1CA2528C9AB2378878F6EEF06 (void);
// 0x000006B3 System.Void NPOI.HSSF.Record.FormulaRecord::set_ParsedExpression(NPOI.SS.Formula.PTG.Ptg[])
extern void FormulaRecord_set_ParsedExpression_mF73C12785331D98EB37871A1A17DE3A6E0DAD9C5 (void);
// 0x000006B4 NPOI.SS.Formula.Formula NPOI.HSSF.Record.FormulaRecord::get_Formula()
extern void FormulaRecord_get_Formula_mC39E382E20C0D15D01DB060534668061C281538D (void);
// 0x000006B5 System.String NPOI.HSSF.Record.FormulaRecord::get_RecordName()
extern void FormulaRecord_get_RecordName_m0744C9E987B8EE0030B4A96550AA2FFFF513F3A9 (void);
// 0x000006B6 System.Int32 NPOI.HSSF.Record.FormulaRecord::get_ValueDataSize()
extern void FormulaRecord_get_ValueDataSize_mBB34A61781ACA17CF5BC36A83BF96BDCAE0386A2 (void);
// 0x000006B7 System.Int16 NPOI.HSSF.Record.FormulaRecord::get_Sid()
extern void FormulaRecord_get_Sid_m887046467DB2B0E13B4AE66A9E5C3A83FF4F5F5E (void);
// 0x000006B8 System.Void NPOI.HSSF.Record.FormulaRecord::SetCachedResultTypeEmptyString()
extern void FormulaRecord_SetCachedResultTypeEmptyString_m4B81E02C64E47D3555708332FE6BC7666E1B3A9A (void);
// 0x000006B9 System.Void NPOI.HSSF.Record.FormulaRecord::SetCachedResultTypeString()
extern void FormulaRecord_SetCachedResultTypeString_m5D6D157D0084818D3199C38A7912FD63C67EA913 (void);
// 0x000006BA System.Void NPOI.HSSF.Record.FormulaRecord::SetCachedResultErrorCode(System.Int32)
extern void FormulaRecord_SetCachedResultErrorCode_mDD33611E77DFA7F1A48E5EAD7784283588621629 (void);
// 0x000006BB System.Void NPOI.HSSF.Record.FormulaRecord::SetCachedResultBoolean(System.Boolean)
extern void FormulaRecord_SetCachedResultBoolean_mA856B8B893DFF2A028CF23C4162E97EF26B7F198 (void);
// 0x000006BC System.Boolean NPOI.HSSF.Record.FormulaRecord::get_CachedBooleanValue()
extern void FormulaRecord_get_CachedBooleanValue_m5491F69242C66A2BFC389E9E85B6EF5AD43E8ADC (void);
// 0x000006BD System.Int32 NPOI.HSSF.Record.FormulaRecord::get_CachedErrorValue()
extern void FormulaRecord_get_CachedErrorValue_m29E6203F45D5928201259D77D9EB8622EF9E373B (void);
// 0x000006BE NPOI.SS.UserModel.CellType NPOI.HSSF.Record.FormulaRecord::get_CachedResultType()
extern void FormulaRecord_get_CachedResultType_mC2B994A1931D72F1766DDBE35DEBCA4D01B4F98F (void);
// 0x000006BF System.Boolean NPOI.HSSF.Record.FormulaRecord::Equals(System.Object)
extern void FormulaRecord_Equals_m94ABE4BDB6689E9DD1976E4F810A302207A14644 (void);
// 0x000006C0 System.Int32 NPOI.HSSF.Record.FormulaRecord::GetHashCode()
extern void FormulaRecord_GetHashCode_m1EC604FC85411040363E51DB95E6D26447AE1867 (void);
// 0x000006C1 System.Void NPOI.HSSF.Record.FormulaRecord::SerializeValue(NPOI.Util.ILittleEndianOutput)
extern void FormulaRecord_SerializeValue_m8D99A0B51AFDE90C22872A279E0C3205BE9E64BC (void);
// 0x000006C2 System.Void NPOI.HSSF.Record.FormulaRecord::AppendValueText(System.Text.StringBuilder)
extern void FormulaRecord_AppendValueText_mE04A65EF0E68A75D5D2D04319D030F8FD91F1405 (void);
// 0x000006C3 System.Object NPOI.HSSF.Record.FormulaRecord::Clone()
extern void FormulaRecord_Clone_mEB2AF381588E3C9747C2E4E3B6FC54728DED5E17 (void);
// 0x000006C4 System.Void NPOI.HSSF.Record.GridsetRecord::.ctor()
extern void GridsetRecord__ctor_m9BE3B4CA2A97F0E1CBA3DAB9FF0CCF380EF620AF (void);
// 0x000006C5 System.Boolean NPOI.HSSF.Record.GridsetRecord::get_Gridset()
extern void GridsetRecord_get_Gridset_mC2CA024CBF3B70AA56017A677AF56888D76A5B89 (void);
// 0x000006C6 System.Void NPOI.HSSF.Record.GridsetRecord::set_Gridset(System.Boolean)
extern void GridsetRecord_set_Gridset_m10B38A81BB1521A28F9BBEF31AE05D89C29C0FBD (void);
// 0x000006C7 System.String NPOI.HSSF.Record.GridsetRecord::ToString()
extern void GridsetRecord_ToString_m405059A4B2F4FB4ED9D470E225CA535D8A277FD4 (void);
// 0x000006C8 System.Void NPOI.HSSF.Record.GridsetRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void GridsetRecord_Serialize_m583512A2CF2AC2B86428FA6B06C56FDE2385AD73 (void);
// 0x000006C9 System.Int32 NPOI.HSSF.Record.GridsetRecord::get_DataSize()
extern void GridsetRecord_get_DataSize_m9DB9B173700AD4CE50F99021A5DD567B0E7CF94D (void);
// 0x000006CA System.Int16 NPOI.HSSF.Record.GridsetRecord::get_Sid()
extern void GridsetRecord_get_Sid_m3CB886308C6EA879FBFB33AE1E1453D07021042D (void);
// 0x000006CB System.Object NPOI.HSSF.Record.GridsetRecord::Clone()
extern void GridsetRecord_Clone_m3B42BE966C6BD5BF2BBE5E67B711B57AC0092B30 (void);
// 0x000006CC System.Void NPOI.HSSF.Record.GutsRecord::.ctor()
extern void GutsRecord__ctor_m283BE4B3A3C78B777106BD4FC95A77E821364D88 (void);
// 0x000006CD System.Int16 NPOI.HSSF.Record.GutsRecord::get_LeftRowGutter()
extern void GutsRecord_get_LeftRowGutter_mC4594ED0798787507A3F8658073044815BB88E28 (void);
// 0x000006CE System.Void NPOI.HSSF.Record.GutsRecord::set_LeftRowGutter(System.Int16)
extern void GutsRecord_set_LeftRowGutter_mB56D9D916BCAD4502FD8C63C3ED7EB9378D0ACB7 (void);
// 0x000006CF System.Int16 NPOI.HSSF.Record.GutsRecord::get_TopColGutter()
extern void GutsRecord_get_TopColGutter_m3CC58A148D2B3388E48705E29A5C4FB4C6437FA7 (void);
// 0x000006D0 System.Void NPOI.HSSF.Record.GutsRecord::set_TopColGutter(System.Int16)
extern void GutsRecord_set_TopColGutter_m5CF648E6A42EBD37C92CA669C2C7F363DE2AD684 (void);
// 0x000006D1 System.Int16 NPOI.HSSF.Record.GutsRecord::get_RowLevelMax()
extern void GutsRecord_get_RowLevelMax_m5065F8F2B7AED9746E7A2E1BA06C1CE372C2EC81 (void);
// 0x000006D2 System.Void NPOI.HSSF.Record.GutsRecord::set_RowLevelMax(System.Int16)
extern void GutsRecord_set_RowLevelMax_m0F1A05C45F546C36FB391AF129E4089FFEBE4CF8 (void);
// 0x000006D3 System.Int16 NPOI.HSSF.Record.GutsRecord::get_ColLevelMax()
extern void GutsRecord_get_ColLevelMax_m4D421AAFDF9D50069AC011BAEF3CF0E721C8D409 (void);
// 0x000006D4 System.Void NPOI.HSSF.Record.GutsRecord::set_ColLevelMax(System.Int16)
extern void GutsRecord_set_ColLevelMax_mD7672729BC6D8EAC53A534BEAF841B1BCC4FD808 (void);
// 0x000006D5 System.String NPOI.HSSF.Record.GutsRecord::ToString()
extern void GutsRecord_ToString_mE743AB6312BD6EF07AF1CD9ED8065497FCA2D503 (void);
// 0x000006D6 System.Void NPOI.HSSF.Record.GutsRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void GutsRecord_Serialize_m902C98C01E9D15460A77B0789C5F2804D7A4703B (void);
// 0x000006D7 System.Int32 NPOI.HSSF.Record.GutsRecord::get_DataSize()
extern void GutsRecord_get_DataSize_m93FE0C3F24E2C14870743ED2372096B31409759A (void);
// 0x000006D8 System.Int16 NPOI.HSSF.Record.GutsRecord::get_Sid()
extern void GutsRecord_get_Sid_m90F9F4291B243A04BDDD05B7D83F120D73FFFAA9 (void);
// 0x000006D9 System.Object NPOI.HSSF.Record.GutsRecord::Clone()
extern void GutsRecord_Clone_mE107EC3B68618CE8FCD1086E16762D3C0D25EE7E (void);
// 0x000006DA System.Void NPOI.HSSF.Record.HCenterRecord::.ctor()
extern void HCenterRecord__ctor_mC997B94DDF25AE36A1105D24AD0A8DA8D73FF428 (void);
// 0x000006DB System.Boolean NPOI.HSSF.Record.HCenterRecord::get_HCenter()
extern void HCenterRecord_get_HCenter_mD09172D70818A9BD983A3D8B4BD5664BA8853DBC (void);
// 0x000006DC System.Void NPOI.HSSF.Record.HCenterRecord::set_HCenter(System.Boolean)
extern void HCenterRecord_set_HCenter_m31D5146A74D80C2BF41530726AAB5213DB91FBE5 (void);
// 0x000006DD System.String NPOI.HSSF.Record.HCenterRecord::ToString()
extern void HCenterRecord_ToString_m3C68CDBB32414E9E9DB8EFFAF92041A6EAAFFC2E (void);
// 0x000006DE System.Void NPOI.HSSF.Record.HCenterRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void HCenterRecord_Serialize_m017E6F4E42E7B0088261B6DCDB5DD1975F91CAA4 (void);
// 0x000006DF System.Int32 NPOI.HSSF.Record.HCenterRecord::get_DataSize()
extern void HCenterRecord_get_DataSize_mDE4D9453FB93BC5EF53441488EA38111C1ECA140 (void);
// 0x000006E0 System.Int16 NPOI.HSSF.Record.HCenterRecord::get_Sid()
extern void HCenterRecord_get_Sid_m74476FB8D7A93C6E04F26F33B97FFD15B08D9427 (void);
// 0x000006E1 System.Object NPOI.HSSF.Record.HCenterRecord::Clone()
extern void HCenterRecord_Clone_m763171C33B8D76BAD001E7EDC54BBDB598DDE9E9 (void);
// 0x000006E2 System.Void NPOI.HSSF.Record.HeaderFooterRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void HeaderFooterRecord_Serialize_m89B34CB9AB63D14736C4774B66AD09F7B3C3983A (void);
// 0x000006E3 System.Int32 NPOI.HSSF.Record.HeaderFooterRecord::get_DataSize()
extern void HeaderFooterRecord_get_DataSize_m0FE07D35799F6997CC67B9F757E2D3F6818C84AC (void);
// 0x000006E4 System.Int16 NPOI.HSSF.Record.HeaderFooterRecord::get_Sid()
extern void HeaderFooterRecord_get_Sid_m02962AD1405A01525049C9EC504192508D4AEC27 (void);
// 0x000006E5 System.Byte[] NPOI.HSSF.Record.HeaderFooterRecord::get_Guid()
extern void HeaderFooterRecord_get_Guid_mFE497704AFD6F12052B9D017C7ABD32F1A128EA5 (void);
// 0x000006E6 System.Boolean NPOI.HSSF.Record.HeaderFooterRecord::get_IsCurrentSheet()
extern void HeaderFooterRecord_get_IsCurrentSheet_m2853BB981105BE5CDDAC2FCAA7665B93D1A5AEDF (void);
// 0x000006E7 System.Void NPOI.HSSF.Record.HeaderFooterRecord::.cctor()
extern void HeaderFooterRecord__cctor_m4126B9EB86EAC92F9D30F1F0C61BF6A694E3E9B1 (void);
// 0x000006E8 System.Void NPOI.HSSF.Record.HeaderRecord::.ctor(System.String)
extern void HeaderRecord__ctor_m2314F9600D2E1AE0FC8C20DBB455A4A261BDF707 (void);
// 0x000006E9 System.String NPOI.HSSF.Record.HeaderRecord::ToString()
extern void HeaderRecord_ToString_mDF7E915442326C8C7518FB29D271480A0DF85954 (void);
// 0x000006EA System.Int16 NPOI.HSSF.Record.HeaderRecord::get_Sid()
extern void HeaderRecord_get_Sid_m9C1EEA11F242246A8EF4C69FF56277DAED4DBD2D (void);
// 0x000006EB System.Object NPOI.HSSF.Record.HeaderRecord::Clone()
extern void HeaderRecord_Clone_m542C0D503120A49FA58BB30B4E0B3FDCA06D09C2 (void);
// 0x000006EC System.Void NPOI.HSSF.Record.HideObjRecord::.ctor()
extern void HideObjRecord__ctor_mB4B7A499C4C5261B76D64E1D8D6214C4E952EF35 (void);
// 0x000006ED System.Void NPOI.HSSF.Record.HideObjRecord::SetHideObj(System.Int16)
extern void HideObjRecord_SetHideObj_m22974B736285877CDBFA910CD0CC36A791389492 (void);
// 0x000006EE System.Int16 NPOI.HSSF.Record.HideObjRecord::GetHideObj()
extern void HideObjRecord_GetHideObj_mC332A1EF339CC54E7F65ECFCA35E2CEBF7C97FEA (void);
// 0x000006EF System.String NPOI.HSSF.Record.HideObjRecord::ToString()
extern void HideObjRecord_ToString_mFF6A211514FBF6C5D53AF789F7C94164A1AD3745 (void);
// 0x000006F0 System.Void NPOI.HSSF.Record.HideObjRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void HideObjRecord_Serialize_mB16D5E59FD3F5189B3319E40CC63F340F54EBC34 (void);
// 0x000006F1 System.Int32 NPOI.HSSF.Record.HideObjRecord::get_DataSize()
extern void HideObjRecord_get_DataSize_mF2E67E0F6B4090E8AB3AEAE3561FDA1E767BF5A6 (void);
// 0x000006F2 System.Int16 NPOI.HSSF.Record.HideObjRecord::get_Sid()
extern void HideObjRecord_get_Sid_m5929C5B2A0ADA9402959FB4054D521B9B0A4A0BF (void);
// 0x000006F3 System.Void NPOI.HSSF.Record.PageBreakRecord::.ctor()
extern void PageBreakRecord__ctor_mEF5CB00E289D7D6632B5BE368C66EF9221D72052 (void);
// 0x000006F4 System.Int16 NPOI.HSSF.Record.PageBreakRecord::get_Sid()
extern void PageBreakRecord_get_Sid_mBD45FD985E020BC24023D8C22AA41A793D58C338 (void);
// 0x000006F5 System.Void NPOI.HSSF.Record.PageBreakRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PageBreakRecord_Serialize_m3B67CA2ACDF646C93BE40D3C371591B02BAE656E (void);
// 0x000006F6 System.Int32 NPOI.HSSF.Record.PageBreakRecord::get_DataSize()
extern void PageBreakRecord_get_DataSize_mF0CF35230A6061507ECA29D31363A1AC6AD4E358 (void);
// 0x000006F7 System.Collections.Generic.IEnumerator`1<NPOI.HSSF.Record.PageBreakRecord_Break> NPOI.HSSF.Record.PageBreakRecord::GetBreaksEnumerator()
extern void PageBreakRecord_GetBreaksEnumerator_mC31716AA43621697F16AA6031586BF14F58052EF (void);
// 0x000006F8 System.String NPOI.HSSF.Record.PageBreakRecord::ToString()
extern void PageBreakRecord_ToString_mE780C793A8F4C392C61B1C479B6C6BF2633C7DA6 (void);
// 0x000006F9 System.Void NPOI.HSSF.Record.PageBreakRecord::AddBreak(System.Int32,System.Int32,System.Int32)
extern void PageBreakRecord_AddBreak_m07CDD493A1C310C1E7DC0E72F034561D6CAE86BA (void);
// 0x000006FA System.Int32 NPOI.HSSF.Record.PageBreakRecord::get_RecordSize()
extern void PageBreakRecord_get_RecordSize_m84A2A2CE1EF97FEC438D36D4BB6259F4C9E317EB (void);
// 0x000006FB System.Int32 NPOI.HSSF.Record.PageBreakRecord::get_NumBreaks()
extern void PageBreakRecord_get_NumBreaks_m460017B97011F03B3711104224A2EA5088C4DE56 (void);
// 0x000006FC System.Boolean NPOI.HSSF.Record.PageBreakRecord::get_IsEmpty()
extern void PageBreakRecord_get_IsEmpty_m0291889761A8E7A064C3266A50DC44BF5B5BB9E7 (void);
// 0x000006FD System.Void NPOI.HSSF.Record.PageBreakRecord::.cctor()
extern void PageBreakRecord__cctor_m7EA7239A05A486C28AA10C42A30F751E9794759B (void);
// 0x000006FE System.Void NPOI.HSSF.Record.PageBreakRecord_Break::.ctor(System.Int32,System.Int32,System.Int32)
extern void Break__ctor_mAAA7A0A09C3D2659D307E0FFB1BEB4804899AC45 (void);
// 0x000006FF System.Void NPOI.HSSF.Record.PageBreakRecord_Break::Serialize(NPOI.Util.ILittleEndianOutput)
extern void Break_Serialize_mCAD988EB19C710BBCE9F8F8E2ABC79F123511EC1 (void);
// 0x00000700 System.Void NPOI.HSSF.Record.HorizontalPageBreakRecord::.ctor()
extern void HorizontalPageBreakRecord__ctor_mDAC7BA8620BC495D719AF608CBE0F2F1F824C52A (void);
// 0x00000701 System.Int16 NPOI.HSSF.Record.HorizontalPageBreakRecord::get_Sid()
extern void HorizontalPageBreakRecord_get_Sid_m2DFF3C7AF7EE75B3CB1316BF8DAEBAFD4407A13F (void);
// 0x00000702 System.Object NPOI.HSSF.Record.HorizontalPageBreakRecord::Clone()
extern void HorizontalPageBreakRecord_Clone_m573B8BBAB4EFA0A9F03AB208F2E0AA30E160D01B (void);
// 0x00000703 System.Void NPOI.HSSF.Record.HyperlinkRecord::WriteTail(System.Byte[],NPOI.Util.ILittleEndianOutput)
extern void HyperlinkRecord_WriteTail_m6E0E315D2CAE85098ED69470879A1212639EFCAA (void);
// 0x00000704 System.Int16 NPOI.HSSF.Record.HyperlinkRecord::get_Sid()
extern void HyperlinkRecord_get_Sid_m624833D9F19E059D974CDC7FB4603E49F4F18411 (void);
// 0x00000705 System.Void NPOI.HSSF.Record.HyperlinkRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void HyperlinkRecord_Serialize_mC658A15F607ED147D745DF14262C8B2F1FF6C2FE (void);
// 0x00000706 System.Int32 NPOI.HSSF.Record.HyperlinkRecord::get_DataSize()
extern void HyperlinkRecord_get_DataSize_m77895E326BD07027889F38966E363D6C2F0BAEFD (void);
// 0x00000707 System.Void NPOI.HSSF.Record.HyperlinkRecord::.cctor()
extern void HyperlinkRecord__cctor_mC409C924FD19A5103EEFF6B981366E3242D2201C (void);
// 0x00000708 System.Void NPOI.HSSF.Record.IndexRecord::.ctor()
extern void IndexRecord__ctor_m2FE0CB7C5C1DFACB3412C4938226D8BF20C9ABEE (void);
// 0x00000709 System.Void NPOI.HSSF.Record.IndexRecord::AddDbcell(System.Int32)
extern void IndexRecord_AddDbcell_m2FFE1847DBA8658A86925DD008E7674660F5F9FA (void);
// 0x0000070A System.Int32 NPOI.HSSF.Record.IndexRecord::get_FirstRow()
extern void IndexRecord_get_FirstRow_mAD9CDC0F5C040879E868E40EC7CC143AA67B0311 (void);
// 0x0000070B System.Void NPOI.HSSF.Record.IndexRecord::set_FirstRow(System.Int32)
extern void IndexRecord_set_FirstRow_m6CECCAA98B8E398AEDE7A7E333C1AF9776EA3350 (void);
// 0x0000070C System.Int32 NPOI.HSSF.Record.IndexRecord::get_LastRowAdd1()
extern void IndexRecord_get_LastRowAdd1_mCE6165328AB4864A07B214E3B0C551178242EE27 (void);
// 0x0000070D System.Void NPOI.HSSF.Record.IndexRecord::set_LastRowAdd1(System.Int32)
extern void IndexRecord_set_LastRowAdd1_m468F97D5EAC9C85A4F0392FCA419AFC07728C56E (void);
// 0x0000070E System.Int32 NPOI.HSSF.Record.IndexRecord::get_NumDbcells()
extern void IndexRecord_get_NumDbcells_m96D833732B816E16B685741FF3A3680CE1C79E60 (void);
// 0x0000070F System.Int32 NPOI.HSSF.Record.IndexRecord::GetDbcellAt(System.Int32)
extern void IndexRecord_GetDbcellAt_m0A0014B813083D2D9A87F2D11C68BA82E548DACF (void);
// 0x00000710 System.String NPOI.HSSF.Record.IndexRecord::ToString()
extern void IndexRecord_ToString_mDBA7EFFB7AE47A5F88BF41B37EF69CEDB5600197 (void);
// 0x00000711 System.Void NPOI.HSSF.Record.IndexRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void IndexRecord_Serialize_m2F56E7CF3DFA811628161F28DCF91B8E858E3875 (void);
// 0x00000712 System.Int32 NPOI.HSSF.Record.IndexRecord::get_DataSize()
extern void IndexRecord_get_DataSize_m1929AA47BA0B8A740D6B772390FDB3578BE83ABF (void);
// 0x00000713 System.Int32 NPOI.HSSF.Record.IndexRecord::GetRecordSizeForBlockCount(System.Int32)
extern void IndexRecord_GetRecordSizeForBlockCount_m40FE317E1C9F07D95E0D296BDA9663AEF6728E0B (void);
// 0x00000714 System.Int16 NPOI.HSSF.Record.IndexRecord::get_Sid()
extern void IndexRecord_get_Sid_m505DB6D9F7D869BCAE7DE7D0C461BA063AA59D86 (void);
// 0x00000715 System.Object NPOI.HSSF.Record.IndexRecord::Clone()
extern void IndexRecord_Clone_m5479BAA644C1832E131ACA1AD36BF138E5BFAF79 (void);
// 0x00000716 System.Void NPOI.HSSF.Record.InterfaceEndRecord::.ctor()
extern void InterfaceEndRecord__ctor_mF8A068B91D53FEAE24FD6F81DA97C33C31A80D32 (void);
// 0x00000717 System.String NPOI.HSSF.Record.InterfaceEndRecord::ToString()
extern void InterfaceEndRecord_ToString_mD50D291DEFE450C52C83105B1C9C4016E020EB35 (void);
// 0x00000718 System.Void NPOI.HSSF.Record.InterfaceEndRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void InterfaceEndRecord_Serialize_m04DCF5BB22882184B47A551710ECEC7121DBCBFC (void);
// 0x00000719 System.Int32 NPOI.HSSF.Record.InterfaceEndRecord::get_DataSize()
extern void InterfaceEndRecord_get_DataSize_m6ECA7CA6410AA83D394F1C8B9BAC839AC9E63E11 (void);
// 0x0000071A System.Int16 NPOI.HSSF.Record.InterfaceEndRecord::get_Sid()
extern void InterfaceEndRecord_get_Sid_mA9F80EA75117698F3F77F8E57621BD1881797E9B (void);
// 0x0000071B System.Void NPOI.HSSF.Record.InterfaceEndRecord::.cctor()
extern void InterfaceEndRecord__cctor_m039348391E1EE6DDA4AB9BD505103E7C5FBF5129 (void);
// 0x0000071C System.Void NPOI.HSSF.Record.InterfaceHdrRecord::.ctor(System.Int32)
extern void InterfaceHdrRecord__ctor_m40BEAF753B54EEE58F6E95E8A64D7362DCCEC036 (void);
// 0x0000071D System.String NPOI.HSSF.Record.InterfaceHdrRecord::ToString()
extern void InterfaceHdrRecord_ToString_m4FBD9A5E96FAF5B9F668CC6D5AB5151E4682BBB5 (void);
// 0x0000071E System.Void NPOI.HSSF.Record.InterfaceHdrRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void InterfaceHdrRecord_Serialize_mE33E5290476E4C0F99657FAD5CA75C2AED3439CF (void);
// 0x0000071F System.Int32 NPOI.HSSF.Record.InterfaceHdrRecord::get_DataSize()
extern void InterfaceHdrRecord_get_DataSize_m8760D68017A4AEF91CB47FAA7A991D5734101739 (void);
// 0x00000720 System.Int16 NPOI.HSSF.Record.InterfaceHdrRecord::get_Sid()
extern void InterfaceHdrRecord_get_Sid_m5C314A8D5B496FE14AB48F37600A895BE7E1318A (void);
// 0x00000721 System.Void NPOI.HSSF.Record.IterationRecord::.ctor(System.Boolean)
extern void IterationRecord__ctor_m2A311968360C8562A57FF2CC92DF5D970C4AAF36 (void);
// 0x00000722 System.Boolean NPOI.HSSF.Record.IterationRecord::get_Iteration()
extern void IterationRecord_get_Iteration_mE2546EF1F6178355C8813CAB9FEC0DFBB16C76B0 (void);
// 0x00000723 System.String NPOI.HSSF.Record.IterationRecord::ToString()
extern void IterationRecord_ToString_m545BD561C28B7434159B43F1A022E21DFE1E418D (void);
// 0x00000724 System.Void NPOI.HSSF.Record.IterationRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void IterationRecord_Serialize_m3F9D523766160CD693AF95CBF0265B3275C7B1AD (void);
// 0x00000725 System.Int32 NPOI.HSSF.Record.IterationRecord::get_DataSize()
extern void IterationRecord_get_DataSize_m128581089D06F16AB6D22F665CC1946FE67822D9 (void);
// 0x00000726 System.Int16 NPOI.HSSF.Record.IterationRecord::get_Sid()
extern void IterationRecord_get_Sid_m30A6F37CC909D28B50AB1382D9C411BE7EDD8740 (void);
// 0x00000727 System.Object NPOI.HSSF.Record.IterationRecord::Clone()
extern void IterationRecord_Clone_m500EA6DD1746A0F7E739A32E585F104CF341E8DB (void);
// 0x00000728 System.Void NPOI.HSSF.Record.IterationRecord::.cctor()
extern void IterationRecord__cctor_m4C9C9C78BB396F97DCD654B0040207E2CD67E302 (void);
// 0x00000729 System.Int32 NPOI.HSSF.Record.LabelRecord::get_Row()
extern void LabelRecord_get_Row_m8993870F3CF6C408487E03E5494DA6ED0573273C (void);
// 0x0000072A System.Int32 NPOI.HSSF.Record.LabelRecord::get_Column()
extern void LabelRecord_get_Column_m0B59B8E81F8DC489DEFD4A355AC0D113E04DBA2F (void);
// 0x0000072B System.Int16 NPOI.HSSF.Record.LabelRecord::get_XFIndex()
extern void LabelRecord_get_XFIndex_m9478F8540B91AB0A8A0F4F369818C4D637D4B0CB (void);
// 0x0000072C System.String NPOI.HSSF.Record.LabelRecord::get_Value()
extern void LabelRecord_get_Value_m3A45054A9E87BCF1132E7380043718D0856141EC (void);
// 0x0000072D System.Int32 NPOI.HSSF.Record.LabelRecord::Serialize(System.Int32,System.Byte[])
extern void LabelRecord_Serialize_m1BE41D69D306593288CAE3260B3BAB01F0ABC9C3 (void);
// 0x0000072E System.Int32 NPOI.HSSF.Record.LabelRecord::get_RecordSize()
extern void LabelRecord_get_RecordSize_m0A32A1C23BE0856B574BD7269B1541AA970CF97F (void);
// 0x0000072F System.Int16 NPOI.HSSF.Record.LabelRecord::get_Sid()
extern void LabelRecord_get_Sid_m2238CD373A3C4C50729C2AD4EA89AC8AC44ED29F (void);
// 0x00000730 System.Void NPOI.HSSF.Record.LabelRecord::.cctor()
extern void LabelRecord__cctor_mDBB7805387F9A7CC14FF6E1D9879FC21EF53D3E3 (void);
// 0x00000731 System.Void NPOI.HSSF.Record.LabelSSTRecord::.ctor()
extern void LabelSSTRecord__ctor_m4FAA2F12C1E7AB5F845202CED41060E2AF2F4B24 (void);
// 0x00000732 System.String NPOI.HSSF.Record.LabelSSTRecord::get_RecordName()
extern void LabelSSTRecord_get_RecordName_m50DCB94A8296B7ECFF420E9AFEC75D8CD5E9C13E (void);
// 0x00000733 System.Int32 NPOI.HSSF.Record.LabelSSTRecord::get_SSTIndex()
extern void LabelSSTRecord_get_SSTIndex_mFD9BE1B192AA9D64267F2A122F9DC794EDA9CEA4 (void);
// 0x00000734 System.Void NPOI.HSSF.Record.LabelSSTRecord::set_SSTIndex(System.Int32)
extern void LabelSSTRecord_set_SSTIndex_mE06E0474755BD2B87755C7E556B138DE758AF1F5 (void);
// 0x00000735 System.Void NPOI.HSSF.Record.LabelSSTRecord::AppendValueText(System.Text.StringBuilder)
extern void LabelSSTRecord_AppendValueText_mB4880A8B4C176BACD10E9119E81ED214AD562DAA (void);
// 0x00000736 System.Void NPOI.HSSF.Record.LabelSSTRecord::SerializeValue(NPOI.Util.ILittleEndianOutput)
extern void LabelSSTRecord_SerializeValue_m9175FA82C94C3E4F8B47F29B3EB08AA5A77199B8 (void);
// 0x00000737 System.Int32 NPOI.HSSF.Record.LabelSSTRecord::get_ValueDataSize()
extern void LabelSSTRecord_get_ValueDataSize_m625CC3632523E99C0851F67B9F0325A70FED5643 (void);
// 0x00000738 System.Int16 NPOI.HSSF.Record.LabelSSTRecord::get_Sid()
extern void LabelSSTRecord_get_Sid_m587AEB7BCB5D8EE64AD1596BB15DE66CBD4AFCBD (void);
// 0x00000739 System.Object NPOI.HSSF.Record.LabelSSTRecord::Clone()
extern void LabelSSTRecord_Clone_m7C085FA80F51D4EE44B8B4C821D5B59CFC1ADDA8 (void);
// 0x0000073A System.Void NPOI.HSSF.Record.LeftMarginRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void LeftMarginRecord_Serialize_m64BC620A52442D624142688575D39D768F20C05E (void);
// 0x0000073B System.Int32 NPOI.HSSF.Record.LeftMarginRecord::get_DataSize()
extern void LeftMarginRecord_get_DataSize_m5A1A9C371D3302CE4EF499E13290B721E11FD373 (void);
// 0x0000073C System.Int16 NPOI.HSSF.Record.LeftMarginRecord::get_Sid()
extern void LeftMarginRecord_get_Sid_m4A2BDFDCDD8BDF0696902EA0FF96B21BF9423AEA (void);
// 0x0000073D System.Void NPOI.HSSF.Record.MergeCellsRecord::.ctor(NPOI.SS.Util.CellRangeAddress[],System.Int32,System.Int32)
extern void MergeCellsRecord__ctor_mEDA9851ADCA47B4744EA7E72DA12119CC76AD416 (void);
// 0x0000073E System.Int16 NPOI.HSSF.Record.MergeCellsRecord::get_NumAreas()
extern void MergeCellsRecord_get_NumAreas_m6F5E5D9ACE04645E256AD945E34DF7073D30AABA (void);
// 0x0000073F NPOI.SS.Util.CellRangeAddress NPOI.HSSF.Record.MergeCellsRecord::GetAreaAt(System.Int32)
extern void MergeCellsRecord_GetAreaAt_mEAF1D57015C664B13C40B6509F5E8A5538179833 (void);
// 0x00000740 System.Int32 NPOI.HSSF.Record.MergeCellsRecord::get_DataSize()
extern void MergeCellsRecord_get_DataSize_m6709F11C3B8EB209670F7800FE80030C1ABE6184 (void);
// 0x00000741 System.Int16 NPOI.HSSF.Record.MergeCellsRecord::get_Sid()
extern void MergeCellsRecord_get_Sid_m0634D8C75767D9DCF7A236DB181605525F46D704 (void);
// 0x00000742 System.Void NPOI.HSSF.Record.MergeCellsRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void MergeCellsRecord_Serialize_m2A1A1072386BB3418EE2143DBBB9EE16736E1352 (void);
// 0x00000743 System.String NPOI.HSSF.Record.MergeCellsRecord::ToString()
extern void MergeCellsRecord_ToString_m9353C222073F665C878072D1B55C397B1A696A98 (void);
// 0x00000744 System.Object NPOI.HSSF.Record.MergeCellsRecord::Clone()
extern void MergeCellsRecord_Clone_mEE5F50D8400734A1D56363E18E14CC78C65C7217 (void);
// 0x00000745 System.Void NPOI.HSSF.Record.MMSRecord::.ctor()
extern void MMSRecord__ctor_m8E9816D1049BB92F8BF09213C98F26100337935A (void);
// 0x00000746 System.Byte NPOI.HSSF.Record.MMSRecord::get_AddMenuCount()
extern void MMSRecord_get_AddMenuCount_mD185C38DE20E45E418A47584F29C9E8CE04B823D (void);
// 0x00000747 System.Void NPOI.HSSF.Record.MMSRecord::set_AddMenuCount(System.Byte)
extern void MMSRecord_set_AddMenuCount_m4F6A71D07C3DE87E53BC020106D2767C311BC030 (void);
// 0x00000748 System.Byte NPOI.HSSF.Record.MMSRecord::get_DelMenuCount()
extern void MMSRecord_get_DelMenuCount_m0BB56027DCE3B2FDC4BDB154641A39AA0DBF0FC8 (void);
// 0x00000749 System.Void NPOI.HSSF.Record.MMSRecord::set_DelMenuCount(System.Byte)
extern void MMSRecord_set_DelMenuCount_mE6C2BE9F75CA39348661E17593DE1A05D6AFD2D9 (void);
// 0x0000074A System.String NPOI.HSSF.Record.MMSRecord::ToString()
extern void MMSRecord_ToString_mC138A2D406588C0E81399C5D824CB75190C0EE76 (void);
// 0x0000074B System.Void NPOI.HSSF.Record.MMSRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void MMSRecord_Serialize_m618772964BBBD984DF276EDE170776A01ADF0BB0 (void);
// 0x0000074C System.Int32 NPOI.HSSF.Record.MMSRecord::get_DataSize()
extern void MMSRecord_get_DataSize_m19633ED01820E07EAB81E8E59FC87EB00FB7FDE4 (void);
// 0x0000074D System.Int16 NPOI.HSSF.Record.MMSRecord::get_Sid()
extern void MMSRecord_get_Sid_m1D4470918CF8FC1E98588ABC949A3D51B8B14891 (void);
// 0x0000074E System.Void NPOI.HSSF.Record.MulBlankRecord::.ctor(System.Int32,System.Int32,System.Int16[])
extern void MulBlankRecord__ctor_mE2C9B1DD3C109C1E4F3720BCA41D130BC625A764 (void);
// 0x0000074F System.Int32 NPOI.HSSF.Record.MulBlankRecord::get_Row()
extern void MulBlankRecord_get_Row_mB574A1D6701D965889C560124B02D0A577F0BD61 (void);
// 0x00000750 System.Int32 NPOI.HSSF.Record.MulBlankRecord::get_FirstColumn()
extern void MulBlankRecord_get_FirstColumn_mD933B953D2C5A849C49CE91AE0ED18210E2E2A26 (void);
// 0x00000751 System.Int32 NPOI.HSSF.Record.MulBlankRecord::get_LastColumn()
extern void MulBlankRecord_get_LastColumn_mF980F4D887CAD25423C9CD9B330020D017CCDD24 (void);
// 0x00000752 System.Int32 NPOI.HSSF.Record.MulBlankRecord::get_NumColumns()
extern void MulBlankRecord_get_NumColumns_mD347F2A1527E621F0753EEFA161F44D4ECCC82F2 (void);
// 0x00000753 System.Int16 NPOI.HSSF.Record.MulBlankRecord::GetXFAt(System.Int32)
extern void MulBlankRecord_GetXFAt_m86E1E9EBAE0CC7AD2A302DE1390E5AF8AFD8EA32 (void);
// 0x00000754 System.String NPOI.HSSF.Record.MulBlankRecord::ToString()
extern void MulBlankRecord_ToString_m9013DF4826C1EDD4B836DD06071E1497791B8418 (void);
// 0x00000755 System.Int16 NPOI.HSSF.Record.MulBlankRecord::get_Sid()
extern void MulBlankRecord_get_Sid_m782AE5CA3E4F4C5EB6AB06B7453EB8727AAE0940 (void);
// 0x00000756 System.Int32 NPOI.HSSF.Record.MulBlankRecord::get_DataSize()
extern void MulBlankRecord_get_DataSize_m7C1B2DDF83A0B04820E1C2C247884578CD817A7B (void);
// 0x00000757 System.Void NPOI.HSSF.Record.MulBlankRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void MulBlankRecord_Serialize_m3982B94834BF855AD52F9301F1A6750B467ED43E (void);
// 0x00000758 System.Object NPOI.HSSF.Record.MulBlankRecord::Clone()
extern void MulBlankRecord_Clone_m2DC2E274F0D4716707030E2BB661526376A51B7B (void);
// 0x00000759 System.Int32 NPOI.HSSF.Record.MulRKRecord::get_Row()
extern void MulRKRecord_get_Row_m22C0E8B19034E85EDD3B7BABE7BDE8B235C02212 (void);
// 0x0000075A System.Int16 NPOI.HSSF.Record.MulRKRecord::get_FirstColumn()
extern void MulRKRecord_get_FirstColumn_m176099C0F7B078A40C6CAB0A806F221E4568AE70 (void);
// 0x0000075B System.Int32 NPOI.HSSF.Record.MulRKRecord::get_NumColumns()
extern void MulRKRecord_get_NumColumns_m214186765FF7BA63E8339312CA37ACD46576EFDE (void);
// 0x0000075C System.Int16 NPOI.HSSF.Record.MulRKRecord::GetXFAt(System.Int32)
extern void MulRKRecord_GetXFAt_mE5DED909C8FDF825ECDE4FB504B1EF98EDD1A74B (void);
// 0x0000075D System.Double NPOI.HSSF.Record.MulRKRecord::GetRKNumberAt(System.Int32)
extern void MulRKRecord_GetRKNumberAt_m8D7C1AB014F1942D842A9AE0198E87A9AA7ABCB4 (void);
// 0x0000075E System.Int16 NPOI.HSSF.Record.MulRKRecord::get_Sid()
extern void MulRKRecord_get_Sid_m3D105D590AF1E497C2511052E04CF241E7B62DAA (void);
// 0x0000075F System.Void NPOI.HSSF.Record.MulRKRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void MulRKRecord_Serialize_m9C7B6FBF42881DFD34948977C718ED12A442D83F (void);
// 0x00000760 System.Int32 NPOI.HSSF.Record.MulRKRecord::get_DataSize()
extern void MulRKRecord_get_DataSize_m96CCB1525F66D7DB78D02AB1555F0A6B7142DBD8 (void);
// 0x00000761 System.Void NPOI.HSSF.Record.NameCommentRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void NameCommentRecord_Serialize_mDDAF9FAA509042ED38E31EABF4455A90B0B15452 (void);
// 0x00000762 System.Int32 NPOI.HSSF.Record.NameCommentRecord::get_DataSize()
extern void NameCommentRecord_get_DataSize_m60030D81292C74EF43FDA5EE4F0F74A9EFAFD70F (void);
// 0x00000763 System.Int16 NPOI.HSSF.Record.NameCommentRecord::get_Sid()
extern void NameCommentRecord_get_Sid_m7BDDA57A59EB2402F922D1272C390710F6D65910 (void);
// 0x00000764 System.String NPOI.HSSF.Record.NameCommentRecord::get_NameText()
extern void NameCommentRecord_get_NameText_m7E3D04909AE58EF37D6CBF5C5A98562300B12EE0 (void);
// 0x00000765 System.Boolean NPOI.HSSF.Record.NameRecord::IsFormula(System.Int32)
extern void NameRecord_IsFormula_m4079C6BEDAE2B7211B597A25EF99956B55F03CB1 (void);
// 0x00000766 System.Int16 NPOI.HSSF.Record.NameRecord::get_OptionFlag()
extern void NameRecord_get_OptionFlag_m86C7B60291CED339C918FC21F68C4F444AA991E8 (void);
// 0x00000767 System.Byte NPOI.HSSF.Record.NameRecord::get_KeyboardShortcut()
extern void NameRecord_get_KeyboardShortcut_mF0D17F7E82639F056022A6CF0EF2A92B6C3BD45E (void);
// 0x00000768 System.Byte NPOI.HSSF.Record.NameRecord::get_NameTextLength()
extern void NameRecord_get_NameTextLength_m036F76CAAD5AD112CE69E31C7C7916C1790C2AD8 (void);
// 0x00000769 System.Boolean NPOI.HSSF.Record.NameRecord::get_HasFormula()
extern void NameRecord_get_HasFormula_mBDFCB1F31F26056AF0F445651D3134B473E0BD8A (void);
// 0x0000076A System.Boolean NPOI.HSSF.Record.NameRecord::get_IsFunctionName()
extern void NameRecord_get_IsFunctionName_m836BCB101A079F879E85401C624DE709B8C8320C (void);
// 0x0000076B System.Boolean NPOI.HSSF.Record.NameRecord::get_IsBuiltInName()
extern void NameRecord_get_IsBuiltInName_m2D1762C322FEF25ACE798129BE47A61E34DD28DA (void);
// 0x0000076C System.String NPOI.HSSF.Record.NameRecord::get_NameText()
extern void NameRecord_get_NameText_m0B0C1C3A4AEDD3939DF122BA3952BE5D7AD73875 (void);
// 0x0000076D System.Byte NPOI.HSSF.Record.NameRecord::get_BuiltInName()
extern void NameRecord_get_BuiltInName_m92D83A3DA1BB82DA83225369F76578BFA268F1B9 (void);
// 0x0000076E System.String NPOI.HSSF.Record.NameRecord::get_CustomMenuText()
extern void NameRecord_get_CustomMenuText_m3DB71BACD0F509B6B92FCE489A705BBA2609542B (void);
// 0x0000076F System.String NPOI.HSSF.Record.NameRecord::get_DescriptionText()
extern void NameRecord_get_DescriptionText_m7B05BEC2FC1E2B24AC23E6B52330A9A55580CD10 (void);
// 0x00000770 System.String NPOI.HSSF.Record.NameRecord::get_HelpTopicText()
extern void NameRecord_get_HelpTopicText_m42D2B5DAE791ACFBA165C339A8A335E5F3736A6E (void);
// 0x00000771 System.String NPOI.HSSF.Record.NameRecord::get_StatusBarText()
extern void NameRecord_get_StatusBarText_m8A8E7061F37809E12F44C77B6D74BC633B1217CF (void);
// 0x00000772 System.Int32 NPOI.HSSF.Record.NameRecord::get_SheetNumber()
extern void NameRecord_get_SheetNumber_mDD9C10FB488E7705C16DB364A2A2D2AD33779222 (void);
// 0x00000773 System.Void NPOI.HSSF.Record.NameRecord::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void NameRecord_Serialize_mFF052F390252613F5294C3E95C392DFF1691AC5E (void);
// 0x00000774 System.Int16 NPOI.HSSF.Record.NameRecord::get_Sid()
extern void NameRecord_get_Sid_mE442A6DA1ADB7561D14FC8B05500D2D2EE7C4DC2 (void);
// 0x00000775 System.String NPOI.HSSF.Record.NameRecord::TranslateBuiltInName(System.Byte)
extern void NameRecord_TranslateBuiltInName_m40484A913AFC39600B0FAF285DCC9FEBA2269A99 (void);
// 0x00000776 System.Int16 NPOI.HSSF.Record.NoteRecord::get_Sid()
extern void NoteRecord_get_Sid_m42AE01A08643C120B3BBC9AD424E7D05A6BE72A2 (void);
// 0x00000777 System.Void NPOI.HSSF.Record.NoteRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void NoteRecord_Serialize_m3887CD92EDEC88FAD1F7D752CEB5ED297ACBB77E (void);
// 0x00000778 System.Int32 NPOI.HSSF.Record.NoteRecord::get_DataSize()
extern void NoteRecord_get_DataSize_mC0F7CDB519B778BDF39E2F76ABE1201132DB6359 (void);
// 0x00000779 System.Int32 NPOI.HSSF.Record.NoteRecord::get_Row()
extern void NoteRecord_get_Row_mCBD34FDC6B12BC14F1E08C4691011B35EB8F3F5C (void);
// 0x0000077A System.Int32 NPOI.HSSF.Record.NoteRecord::get_Column()
extern void NoteRecord_get_Column_m6D42D869C8DBAA47CBEAE9B3F51090BBB036CA24 (void);
// 0x0000077B System.Void NPOI.HSSF.Record.NoteRecord::.cctor()
extern void NoteRecord__cctor_m5E58F9A6E13BFA005CE844B503F671A25B602EDB (void);
// 0x0000077C System.Void NPOI.HSSF.Record.NumberRecord::.ctor()
extern void NumberRecord__ctor_mC954D969DBEE80DC4C796C03646FF4556E4DDE80 (void);
// 0x0000077D System.String NPOI.HSSF.Record.NumberRecord::get_RecordName()
extern void NumberRecord_get_RecordName_mCE6891EB4AEAA06137FF6C7EF698036BFA662E78 (void);
// 0x0000077E System.Void NPOI.HSSF.Record.NumberRecord::AppendValueText(System.Text.StringBuilder)
extern void NumberRecord_AppendValueText_m9449A5DD5523C2EC21F9D4F520FFEDCDD443D60C (void);
// 0x0000077F System.Void NPOI.HSSF.Record.NumberRecord::SerializeValue(NPOI.Util.ILittleEndianOutput)
extern void NumberRecord_SerializeValue_mD5752D39181A99103D670A8398828C4043D98EC4 (void);
// 0x00000780 System.Int32 NPOI.HSSF.Record.NumberRecord::get_ValueDataSize()
extern void NumberRecord_get_ValueDataSize_m80CCA9F55E27A0A0BDC06FD346FD672BCD151CA2 (void);
// 0x00000781 System.Double NPOI.HSSF.Record.NumberRecord::get_Value()
extern void NumberRecord_get_Value_m9FB71FB95D4940EE7DE9AC245F69D8D6E27F8ADD (void);
// 0x00000782 System.Void NPOI.HSSF.Record.NumberRecord::set_Value(System.Double)
extern void NumberRecord_set_Value_mC532184E67BDBFA87319C18C14A6FEA9BEB6284D (void);
// 0x00000783 System.Int16 NPOI.HSSF.Record.NumberRecord::get_Sid()
extern void NumberRecord_get_Sid_m6C8FEDB49569865F1EAA884377C6DD58CD78C516 (void);
// 0x00000784 System.Object NPOI.HSSF.Record.NumberRecord::Clone()
extern void NumberRecord_Clone_mDBE17D48825DB27CD5A701054C1928E745FF1250 (void);
// 0x00000785 System.Void NPOI.HSSF.Record.ObjectProtectRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ObjectProtectRecord_Serialize_m682DFDAF9E95E9CA85435D33993E0062F7E6E08C (void);
// 0x00000786 System.Int32 NPOI.HSSF.Record.ObjectProtectRecord::get_DataSize()
extern void ObjectProtectRecord_get_DataSize_m96555AEB820E62F73BFCF289997403C79057F4FD (void);
// 0x00000787 System.Int16 NPOI.HSSF.Record.ObjectProtectRecord::get_Sid()
extern void ObjectProtectRecord_get_Sid_mE1E0116924F33BB8503ECD4DBCAEBF3F86A92E8B (void);
// 0x00000788 System.Int32 NPOI.HSSF.Record.ObjRecord::Serialize(System.Int32,System.Byte[])
extern void ObjRecord_Serialize_m7033733E1C5EC6B4454ECB931E79FBBDB9524233 (void);
// 0x00000789 System.Int32 NPOI.HSSF.Record.ObjRecord::get_RecordSize()
extern void ObjRecord_get_RecordSize_m36DC6527EDB0E5F9BDD522D8A8618BDDC2869212 (void);
// 0x0000078A System.Int16 NPOI.HSSF.Record.ObjRecord::get_Sid()
extern void ObjRecord_get_Sid_mA76617D818FC15EFE23C03486C7DF136B077BD13 (void);
// 0x0000078B System.Void NPOI.HSSF.Record.PaletteRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PaletteRecord_Serialize_m0A3C315330C0AD04B6589B0BCBB7D3799ECB648C (void);
// 0x0000078C System.Int32 NPOI.HSSF.Record.PaletteRecord::get_DataSize()
extern void PaletteRecord_get_DataSize_m346BE74649D16271F1415CDCC3ED6B194DBEE92C (void);
// 0x0000078D System.Int16 NPOI.HSSF.Record.PaletteRecord::get_Sid()
extern void PaletteRecord_get_Sid_m38EC4BF9795DF8B0B757C485529A989B16F6FC73 (void);
// 0x0000078E System.Void NPOI.HSSF.Record.PColor::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PColor_Serialize_mD6688BB2CC6427672F9A3211158529D450AD9E12 (void);
// 0x0000078F System.Void NPOI.HSSF.Record.PaneRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PaneRecord_Serialize_mD4C57650BDA324A28587B4C71ACA488FC8A787BA (void);
// 0x00000790 System.Int32 NPOI.HSSF.Record.PaneRecord::get_DataSize()
extern void PaneRecord_get_DataSize_m50981724FDD93019385B814559E89C1161B812E6 (void);
// 0x00000791 System.Int16 NPOI.HSSF.Record.PaneRecord::get_Sid()
extern void PaneRecord_get_Sid_mE2EAB2A0AAC1B15F78BD31AFBA93CC7E6EA4DC9F (void);
// 0x00000792 System.Void NPOI.HSSF.Record.PasswordRecord::.ctor(System.Int32)
extern void PasswordRecord__ctor_mAAA2BACC73A15B443E44F49A0DBD6FD05EC1DFE1 (void);
// 0x00000793 System.Int32 NPOI.HSSF.Record.PasswordRecord::get_Password()
extern void PasswordRecord_get_Password_m1D5CC388F2A21F671D0857A92FACC8EE4C303606 (void);
// 0x00000794 System.String NPOI.HSSF.Record.PasswordRecord::ToString()
extern void PasswordRecord_ToString_m52857690F0A02BEECBA5FFB22EE0986CC3905C7F (void);
// 0x00000795 System.Void NPOI.HSSF.Record.PasswordRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PasswordRecord_Serialize_m08BA7B78D821730B0279DB27B0ED924209EAB136 (void);
// 0x00000796 System.Int32 NPOI.HSSF.Record.PasswordRecord::get_DataSize()
extern void PasswordRecord_get_DataSize_m2D776B4222F58C03093A908C790E4F0A5D2D9307 (void);
// 0x00000797 System.Int16 NPOI.HSSF.Record.PasswordRecord::get_Sid()
extern void PasswordRecord_get_Sid_m52F8B71B2045FE6D098A58E41E3D3D45BD89BC0F (void);
// 0x00000798 System.Object NPOI.HSSF.Record.PasswordRecord::Clone()
extern void PasswordRecord_Clone_m8161391E6FD3B96C6F3622345CD680963684D5F1 (void);
// 0x00000799 System.Void NPOI.HSSF.Record.PasswordRev4Record::.ctor(System.Int32)
extern void PasswordRev4Record__ctor_mB53B5225A4F76064FAA9790661AE1D00A93F0D51 (void);
// 0x0000079A System.String NPOI.HSSF.Record.PasswordRev4Record::ToString()
extern void PasswordRev4Record_ToString_mBFBBAA2B02936D10F6E23BFBEF5A5099B0AF983C (void);
// 0x0000079B System.Void NPOI.HSSF.Record.PasswordRev4Record::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PasswordRev4Record_Serialize_m07E4A87A961AC0DF57A8D47970F7379F57DB7A48 (void);
// 0x0000079C System.Int32 NPOI.HSSF.Record.PasswordRev4Record::get_DataSize()
extern void PasswordRev4Record_get_DataSize_m4254AD603ED62894CE2BE92EB95983657BCA3F93 (void);
// 0x0000079D System.Int16 NPOI.HSSF.Record.PasswordRev4Record::get_Sid()
extern void PasswordRev4Record_get_Sid_mABFD2F4DF161FE3755DEF6F5C39AFDB04A1EE65D (void);
// 0x0000079E System.Void NPOI.HSSF.Record.PivotTable.DataItemRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void DataItemRecord_Serialize_m2D7602B458ED94215A7C12066C57A4CACE2639EB (void);
// 0x0000079F System.Int32 NPOI.HSSF.Record.PivotTable.DataItemRecord::get_DataSize()
extern void DataItemRecord_get_DataSize_m7C1E08A6DB5B2893F35859D509E08C8CA0940183 (void);
// 0x000007A0 System.Int16 NPOI.HSSF.Record.PivotTable.DataItemRecord::get_Sid()
extern void DataItemRecord_get_Sid_mB5643295063F46978C4B87B19CEA59931C7C4902 (void);
// 0x000007A1 System.Void NPOI.HSSF.Record.PivotTable.ExtendedPivotTableViewFieldsRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ExtendedPivotTableViewFieldsRecord_Serialize_mC2F73B2A79D3F9C128841AB56565308F5D302498 (void);
// 0x000007A2 System.Int32 NPOI.HSSF.Record.PivotTable.ExtendedPivotTableViewFieldsRecord::get_DataSize()
extern void ExtendedPivotTableViewFieldsRecord_get_DataSize_mEAEA8384D5479DEA29B42502DCA0711970F5A193 (void);
// 0x000007A3 System.Int16 NPOI.HSSF.Record.PivotTable.ExtendedPivotTableViewFieldsRecord::get_Sid()
extern void ExtendedPivotTableViewFieldsRecord_get_Sid_mEBE5D290E99D0A868E6E0F55F8F44798BE3FFE65 (void);
// 0x000007A4 System.Void NPOI.HSSF.Record.PivotTable.PageItemRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PageItemRecord_Serialize_mDD6E4CE43F5F0FF8A93B14711B50FACA7FE623AD (void);
// 0x000007A5 System.Int32 NPOI.HSSF.Record.PivotTable.PageItemRecord::get_DataSize()
extern void PageItemRecord_get_DataSize_m1A9F5D68EAD75312FFB0BEE43F0D2CD97A90F333 (void);
// 0x000007A6 System.Int16 NPOI.HSSF.Record.PivotTable.PageItemRecord::get_Sid()
extern void PageItemRecord_get_Sid_mB333E09BF9490102C992DC984DD19162CDE71C66 (void);
// 0x000007A7 System.Void NPOI.HSSF.Record.PivotTable.PageItemRecord_FieldInfo::Serialize(NPOI.Util.ILittleEndianOutput)
extern void FieldInfo_Serialize_mD9FA292F54F8448B8ADC4A6B83DAF4CF118BED40 (void);
// 0x000007A8 System.Void NPOI.HSSF.Record.PivotTable.StreamIDRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void StreamIDRecord_Serialize_mFEF4D815574EF057E0E91DE9DAE1BB1F7715D8D6 (void);
// 0x000007A9 System.Int32 NPOI.HSSF.Record.PivotTable.StreamIDRecord::get_DataSize()
extern void StreamIDRecord_get_DataSize_m3BDC0C7F79CADDCCADD740E37D44FE375CF61429 (void);
// 0x000007AA System.Int16 NPOI.HSSF.Record.PivotTable.StreamIDRecord::get_Sid()
extern void StreamIDRecord_get_Sid_m39658F31DAFEC0D633D6909D7A4B8B8F74EB59E5 (void);
// 0x000007AB System.Void NPOI.HSSF.Record.PivotTable.ViewDefinitionRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ViewDefinitionRecord_Serialize_mF71127D7D10F3B1A05B6413C7E5823FAA63ABECE (void);
// 0x000007AC System.Int32 NPOI.HSSF.Record.PivotTable.ViewDefinitionRecord::get_DataSize()
extern void ViewDefinitionRecord_get_DataSize_m0A63F68F198D7BD42FE00F0971F163DF23661187 (void);
// 0x000007AD System.Int16 NPOI.HSSF.Record.PivotTable.ViewDefinitionRecord::get_Sid()
extern void ViewDefinitionRecord_get_Sid_mB74B2D0334CFA8B9B3AB10C5EFBCBD6CCF68E54B (void);
// 0x000007AE System.Void NPOI.HSSF.Record.PivotTable.ViewFieldsRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ViewFieldsRecord_Serialize_m9F9943A772A2C3FDB758C7891F27DBDFF1F5FD2E (void);
// 0x000007AF System.Int32 NPOI.HSSF.Record.PivotTable.ViewFieldsRecord::get_DataSize()
extern void ViewFieldsRecord_get_DataSize_m32CD4C24D56DAC0474D4FB9A1DD944D6A08987EA (void);
// 0x000007B0 System.Int16 NPOI.HSSF.Record.PivotTable.ViewFieldsRecord::get_Sid()
extern void ViewFieldsRecord_get_Sid_m1817980F3C6F4C2813BCF1E495741ECF79E1B5F4 (void);
// 0x000007B1 System.Void NPOI.HSSF.Record.PivotTable.ViewSourceRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ViewSourceRecord_Serialize_m9428C1D76C179FEC4B10093B7D6852F1B29B5EF3 (void);
// 0x000007B2 System.Int32 NPOI.HSSF.Record.PivotTable.ViewSourceRecord::get_DataSize()
extern void ViewSourceRecord_get_DataSize_m07C3D61EA19AFA730E79F6DA844DD5611F0BBFEE (void);
// 0x000007B3 System.Int16 NPOI.HSSF.Record.PivotTable.ViewSourceRecord::get_Sid()
extern void ViewSourceRecord_get_Sid_m036BD59B35CC7A4EDD1433FB7B18D34B20415FD4 (void);
// 0x000007B4 System.Void NPOI.HSSF.Record.PrecisionRecord::.ctor()
extern void PrecisionRecord__ctor_m915CDA695D284ED0FDF92FBB92926FF8070E4A5E (void);
// 0x000007B5 System.Boolean NPOI.HSSF.Record.PrecisionRecord::get_FullPrecision()
extern void PrecisionRecord_get_FullPrecision_mD0BECD2B12B0EDDDF9ED57CBBF1766AC835435AF (void);
// 0x000007B6 System.Void NPOI.HSSF.Record.PrecisionRecord::set_FullPrecision(System.Boolean)
extern void PrecisionRecord_set_FullPrecision_m791FCCB85DDFE912835E50A6CA76115109768F46 (void);
// 0x000007B7 System.String NPOI.HSSF.Record.PrecisionRecord::ToString()
extern void PrecisionRecord_ToString_mBBFA7D88675CC37ECC04DA90AB41080FD0F3B817 (void);
// 0x000007B8 System.Void NPOI.HSSF.Record.PrecisionRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PrecisionRecord_Serialize_m2F734CC7C666395345FAD453C72D898662D596E8 (void);
// 0x000007B9 System.Int32 NPOI.HSSF.Record.PrecisionRecord::get_DataSize()
extern void PrecisionRecord_get_DataSize_m5830BC5A0B38C985393142AEDC19B39C906DCC4C (void);
// 0x000007BA System.Int16 NPOI.HSSF.Record.PrecisionRecord::get_Sid()
extern void PrecisionRecord_get_Sid_m98C7695328E9D3A669E437F2CEDD98020FA20F89 (void);
// 0x000007BB System.Void NPOI.HSSF.Record.PrintGridlinesRecord::.ctor()
extern void PrintGridlinesRecord__ctor_mD79DA42C40924B7B79EE9B47B43E469C1CA84188 (void);
// 0x000007BC System.Boolean NPOI.HSSF.Record.PrintGridlinesRecord::get_PrintGridlines()
extern void PrintGridlinesRecord_get_PrintGridlines_m0DC5BE1E6D552DEEF7A815A862541909244222F7 (void);
// 0x000007BD System.Void NPOI.HSSF.Record.PrintGridlinesRecord::set_PrintGridlines(System.Boolean)
extern void PrintGridlinesRecord_set_PrintGridlines_m23C45FE550D38CB1D6044AC143BC1F6C192A9E7B (void);
// 0x000007BE System.String NPOI.HSSF.Record.PrintGridlinesRecord::ToString()
extern void PrintGridlinesRecord_ToString_mB4F996D83795886E3546609880B22996F070C9AA (void);
// 0x000007BF System.Void NPOI.HSSF.Record.PrintGridlinesRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PrintGridlinesRecord_Serialize_m3E9CE8DFDAFB72DE30829F7D6F24C2F9F8DB4DB4 (void);
// 0x000007C0 System.Int32 NPOI.HSSF.Record.PrintGridlinesRecord::get_DataSize()
extern void PrintGridlinesRecord_get_DataSize_mD7F7D8E24FD95B36960BF74FCBE739608FFEF490 (void);
// 0x000007C1 System.Int16 NPOI.HSSF.Record.PrintGridlinesRecord::get_Sid()
extern void PrintGridlinesRecord_get_Sid_mF24A418E2CB8C8D3409AF0D17068A20960449DFA (void);
// 0x000007C2 System.Object NPOI.HSSF.Record.PrintGridlinesRecord::Clone()
extern void PrintGridlinesRecord_Clone_mB173F3B03CE1EDDD6F73109AB6CEFAE3A882AAE0 (void);
// 0x000007C3 System.Void NPOI.HSSF.Record.PrintHeadersRecord::.ctor()
extern void PrintHeadersRecord__ctor_mF7792CBE8E86F7016802777A130408DF3DCCB07C (void);
// 0x000007C4 System.Boolean NPOI.HSSF.Record.PrintHeadersRecord::get_PrintHeaders()
extern void PrintHeadersRecord_get_PrintHeaders_m384D8B0F4F3B4A59CE5C02509C59FBD9C8DA8D62 (void);
// 0x000007C5 System.Void NPOI.HSSF.Record.PrintHeadersRecord::set_PrintHeaders(System.Boolean)
extern void PrintHeadersRecord_set_PrintHeaders_mD7E4095A2C860965C45E17A0BCD9A30D4C0A5823 (void);
// 0x000007C6 System.String NPOI.HSSF.Record.PrintHeadersRecord::ToString()
extern void PrintHeadersRecord_ToString_m9C81694799260D0662825871764AB73F4F8B68EE (void);
// 0x000007C7 System.Void NPOI.HSSF.Record.PrintHeadersRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PrintHeadersRecord_Serialize_mEAC6374A558686A2A4E71031A8CF40829D2BB460 (void);
// 0x000007C8 System.Int32 NPOI.HSSF.Record.PrintHeadersRecord::get_DataSize()
extern void PrintHeadersRecord_get_DataSize_m07EB5FDAB1AA1153FDC81D50137672A13BEA97E3 (void);
// 0x000007C9 System.Int16 NPOI.HSSF.Record.PrintHeadersRecord::get_Sid()
extern void PrintHeadersRecord_get_Sid_m80B7ED2189C83239A727D6B0CBAB4F546A9C6CB9 (void);
// 0x000007CA System.Object NPOI.HSSF.Record.PrintHeadersRecord::Clone()
extern void PrintHeadersRecord_Clone_m50C2B3094673A06586E9F7DF831666B7C704A56E (void);
// 0x000007CB System.Void NPOI.HSSF.Record.PrintSetupRecord::.ctor()
extern void PrintSetupRecord__ctor_m2E6F4E57BB9C1B744B48CA6E2ABA41CA6A8C42C5 (void);
// 0x000007CC System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_PaperSize()
extern void PrintSetupRecord_get_PaperSize_m5266952DED2C108AA405FD092FFB6527E3F93085 (void);
// 0x000007CD System.Void NPOI.HSSF.Record.PrintSetupRecord::set_PaperSize(System.Int16)
extern void PrintSetupRecord_set_PaperSize_mEF5B76F49D2ABBA4E60698F4DF768F915CDB1C17 (void);
// 0x000007CE System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_Scale()
extern void PrintSetupRecord_get_Scale_m52E87501084B95B4A97AC5A57456C8616C38101D (void);
// 0x000007CF System.Void NPOI.HSSF.Record.PrintSetupRecord::set_Scale(System.Int16)
extern void PrintSetupRecord_set_Scale_m79E6A13D69B649A1ABA327BA9556211A16EB0CFE (void);
// 0x000007D0 System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_PageStart()
extern void PrintSetupRecord_get_PageStart_m35C61948D127C983ABF1FA8CEFDB9573B010E8E6 (void);
// 0x000007D1 System.Void NPOI.HSSF.Record.PrintSetupRecord::set_PageStart(System.Int16)
extern void PrintSetupRecord_set_PageStart_mBF22D369004D2F3B64A57E69ADBFE3CECEDD8702 (void);
// 0x000007D2 System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_FitWidth()
extern void PrintSetupRecord_get_FitWidth_mF1AD751210605E009EC5CE9E0B41B4F360BE4D41 (void);
// 0x000007D3 System.Void NPOI.HSSF.Record.PrintSetupRecord::set_FitWidth(System.Int16)
extern void PrintSetupRecord_set_FitWidth_mAD77A591855B6910BD6290F65DA73AE0F597E8DA (void);
// 0x000007D4 System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_FitHeight()
extern void PrintSetupRecord_get_FitHeight_mCE7EB4FE6F4EEBD81509900B06C2CE93B7927D6E (void);
// 0x000007D5 System.Void NPOI.HSSF.Record.PrintSetupRecord::set_FitHeight(System.Int16)
extern void PrintSetupRecord_set_FitHeight_m872B7A737AB0C7C839B01A2C8CDAD672E1826FEA (void);
// 0x000007D6 System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_Options()
extern void PrintSetupRecord_get_Options_mA663E53444DE36E7A52C952B1603C44AE8D751F5 (void);
// 0x000007D7 System.Void NPOI.HSSF.Record.PrintSetupRecord::set_Options(System.Int16)
extern void PrintSetupRecord_set_Options_mB77B33B60F3A7FFFCC9293CA9F5FFAD8146F2715 (void);
// 0x000007D8 System.Boolean NPOI.HSSF.Record.PrintSetupRecord::get_LeftToRight()
extern void PrintSetupRecord_get_LeftToRight_m7F3F67BB3405A41AE6CE5E964698ABBAAA99858F (void);
// 0x000007D9 System.Boolean NPOI.HSSF.Record.PrintSetupRecord::get_Landscape()
extern void PrintSetupRecord_get_Landscape_m88A13290C1233CAF13F0899546802EB7B7A44E43 (void);
// 0x000007DA System.Boolean NPOI.HSSF.Record.PrintSetupRecord::get_ValidSettings()
extern void PrintSetupRecord_get_ValidSettings_m6D76EEAFD4BF1870A27F287B4657C9F0D791B565 (void);
// 0x000007DB System.Boolean NPOI.HSSF.Record.PrintSetupRecord::get_NoColor()
extern void PrintSetupRecord_get_NoColor_m9957A57119AED5F4420B2A5E6DA3313046BE5697 (void);
// 0x000007DC System.Boolean NPOI.HSSF.Record.PrintSetupRecord::get_Draft()
extern void PrintSetupRecord_get_Draft_mC19C654F1F7DF840766D7BC4E1FF3A581FD1F4F5 (void);
// 0x000007DD System.Boolean NPOI.HSSF.Record.PrintSetupRecord::get_Notes()
extern void PrintSetupRecord_get_Notes_mA011C0B1F4BF7C25E7EA2AAEDE194B46FBE07379 (void);
// 0x000007DE System.Boolean NPOI.HSSF.Record.PrintSetupRecord::get_NoOrientation()
extern void PrintSetupRecord_get_NoOrientation_m78E48912DB73CAF7FEE500829374E213B75C5651 (void);
// 0x000007DF System.Boolean NPOI.HSSF.Record.PrintSetupRecord::get_UsePage()
extern void PrintSetupRecord_get_UsePage_mC8460A3965847BF0B580C7C1AFB5060BF4665D80 (void);
// 0x000007E0 System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_HResolution()
extern void PrintSetupRecord_get_HResolution_mD8855CEFD66F22034B89474ACFC4C4047BCCCA9F (void);
// 0x000007E1 System.Void NPOI.HSSF.Record.PrintSetupRecord::set_HResolution(System.Int16)
extern void PrintSetupRecord_set_HResolution_m15BE907CD9B1D2EF7FF3B86423EFC72E4678E85D (void);
// 0x000007E2 System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_VResolution()
extern void PrintSetupRecord_get_VResolution_m0EB640FC219C931090CD379B8DBA9E7AD348D3A0 (void);
// 0x000007E3 System.Void NPOI.HSSF.Record.PrintSetupRecord::set_VResolution(System.Int16)
extern void PrintSetupRecord_set_VResolution_m522AE0D98AA83FCC5E5705219C504EC3E198B35B (void);
// 0x000007E4 System.Double NPOI.HSSF.Record.PrintSetupRecord::get_HeaderMargin()
extern void PrintSetupRecord_get_HeaderMargin_m092FB57C60EFB924BAA57AE17184EFF2F224258B (void);
// 0x000007E5 System.Void NPOI.HSSF.Record.PrintSetupRecord::set_HeaderMargin(System.Double)
extern void PrintSetupRecord_set_HeaderMargin_m8870CD90AFC65C3DF4AA671518169BCB66FEA08A (void);
// 0x000007E6 System.Double NPOI.HSSF.Record.PrintSetupRecord::get_FooterMargin()
extern void PrintSetupRecord_get_FooterMargin_mBA21AE9CC3B06B8E03CD429FBF164D1F3D6FC563 (void);
// 0x000007E7 System.Void NPOI.HSSF.Record.PrintSetupRecord::set_FooterMargin(System.Double)
extern void PrintSetupRecord_set_FooterMargin_m42007AC07BD91DB2A34A7EB1BB240265553E8A03 (void);
// 0x000007E8 System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_Copies()
extern void PrintSetupRecord_get_Copies_mBC016463B0DB9AD55E209DB0970D125DCA31F54C (void);
// 0x000007E9 System.Void NPOI.HSSF.Record.PrintSetupRecord::set_Copies(System.Int16)
extern void PrintSetupRecord_set_Copies_mA3E8633F174B9DCD195D94AEC6C6F1C16F901894 (void);
// 0x000007EA System.String NPOI.HSSF.Record.PrintSetupRecord::ToString()
extern void PrintSetupRecord_ToString_m8993A29EE0F16A47F3E8DB807B5834591B9A7B3B (void);
// 0x000007EB System.Void NPOI.HSSF.Record.PrintSetupRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PrintSetupRecord_Serialize_mBDFD50B06970910194A849121189A6FD9F0AE7BB (void);
// 0x000007EC System.Int32 NPOI.HSSF.Record.PrintSetupRecord::get_DataSize()
extern void PrintSetupRecord_get_DataSize_m6B85B6621DD98EA01A7A67D745BC63E11D718CCF (void);
// 0x000007ED System.Int16 NPOI.HSSF.Record.PrintSetupRecord::get_Sid()
extern void PrintSetupRecord_get_Sid_mF30FB18376F5970E024A8FF35DE995705C5EAA25 (void);
// 0x000007EE System.Object NPOI.HSSF.Record.PrintSetupRecord::Clone()
extern void PrintSetupRecord_Clone_m88E7AA81BF98809D97AEBE3D1DB34EA1EEEF5973 (void);
// 0x000007EF System.Int32 NPOI.HSSF.Record.PrintSizeRecord::get_DataSize()
extern void PrintSizeRecord_get_DataSize_mD2CA16C2730818E7697BDB2E5874F8DF61B38672 (void);
// 0x000007F0 System.Void NPOI.HSSF.Record.PrintSizeRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void PrintSizeRecord_Serialize_m1B1B44E58A19F9C032E8A8FBC2C7A73BF44B73A8 (void);
// 0x000007F1 System.Int16 NPOI.HSSF.Record.PrintSizeRecord::get_Sid()
extern void PrintSizeRecord_get_Sid_mD89A0EAA575D03E4F4D0D89BEDBFD4D403B85896 (void);
// 0x000007F2 System.Void NPOI.HSSF.Record.ProtectionRev4Record::.ctor(System.Int16)
extern void ProtectionRev4Record__ctor_m77A35D6A2E91605F72C39E04D80730C1675D8263 (void);
// 0x000007F3 System.Void NPOI.HSSF.Record.ProtectionRev4Record::.ctor(System.Boolean)
extern void ProtectionRev4Record__ctor_m9DD0CC60DE85D565A581D7C6DC5FBF67FC05817A (void);
// 0x000007F4 System.Boolean NPOI.HSSF.Record.ProtectionRev4Record::get_Protect()
extern void ProtectionRev4Record_get_Protect_mFC522EF8825B93CDF1761862CFFC9BAD98AA07F0 (void);
// 0x000007F5 System.Void NPOI.HSSF.Record.ProtectionRev4Record::set_Protect(System.Boolean)
extern void ProtectionRev4Record_set_Protect_m711302FA321114331BFFE685AB02474F497F59C5 (void);
// 0x000007F6 System.String NPOI.HSSF.Record.ProtectionRev4Record::ToString()
extern void ProtectionRev4Record_ToString_m54627711313CAA0D740D4B6D8029E0A15482271D (void);
// 0x000007F7 System.Void NPOI.HSSF.Record.ProtectionRev4Record::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ProtectionRev4Record_Serialize_m221BB85443BDDE7E53280B740BA47A23C9AE6A8E (void);
// 0x000007F8 System.Int32 NPOI.HSSF.Record.ProtectionRev4Record::get_DataSize()
extern void ProtectionRev4Record_get_DataSize_m160D5065E77FFA15A03A6A1710C2F40EDF4308C6 (void);
// 0x000007F9 System.Int16 NPOI.HSSF.Record.ProtectionRev4Record::get_Sid()
extern void ProtectionRev4Record_get_Sid_mC3902FC9BEC1A8C29AAAC30B2A47D1BAF69AB2EB (void);
// 0x000007FA System.Void NPOI.HSSF.Record.ProtectionRev4Record::.cctor()
extern void ProtectionRev4Record__cctor_m21AB2D185176AA64BB16C6D928DD3AFEE96FF33A (void);
// 0x000007FB System.Void NPOI.HSSF.Record.ProtectRecord::.ctor(System.Int16)
extern void ProtectRecord__ctor_m4EB355D565AE2BF998CABB8934776C34245CF349 (void);
// 0x000007FC System.Void NPOI.HSSF.Record.ProtectRecord::.ctor(System.Boolean)
extern void ProtectRecord__ctor_m360A2B4940ADA7C92EA5560AFDFAAB6F4C3CA5D9 (void);
// 0x000007FD System.Void NPOI.HSSF.Record.ProtectRecord::set_Protect(System.Boolean)
extern void ProtectRecord_set_Protect_m9D2D50C348A55A760225BE672350C40B1F309234 (void);
// 0x000007FE System.String NPOI.HSSF.Record.ProtectRecord::ToString()
extern void ProtectRecord_ToString_mA1F70C1AD2DD5F1E5D8D5D6790425502982FFB45 (void);
// 0x000007FF System.Void NPOI.HSSF.Record.ProtectRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ProtectRecord_Serialize_mD9465DFEC2D02406F09B800149C7CB7C119C920B (void);
// 0x00000800 System.Int32 NPOI.HSSF.Record.ProtectRecord::get_DataSize()
extern void ProtectRecord_get_DataSize_m56E828B18D91570322AA4C8341AD685AE4F24CDD (void);
// 0x00000801 System.Int16 NPOI.HSSF.Record.ProtectRecord::get_Sid()
extern void ProtectRecord_get_Sid_m56051A11E54E356D0DC85AFE9C50A989CCE96C5F (void);
// 0x00000802 System.Object NPOI.HSSF.Record.ProtectRecord::Clone()
extern void ProtectRecord_Clone_mB702AD85271C7909AB76DC8C61CF65F94A309159 (void);
// 0x00000803 System.Void NPOI.HSSF.Record.ProtectRecord::.cctor()
extern void ProtectRecord__cctor_m3B30DDE51208552D640E11852981A28983664361 (void);
// 0x00000804 System.Boolean NPOI.HSSF.Record.RecalcIdRecord::get_IsNeeded()
extern void RecalcIdRecord_get_IsNeeded_mCDC2DFF869D57A560C827584534CBE4D505D0B24 (void);
// 0x00000805 System.Void NPOI.HSSF.Record.RecalcIdRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void RecalcIdRecord_Serialize_m4460EF842CBF90C67067ADEC9504BABE439497BB (void);
// 0x00000806 System.Int32 NPOI.HSSF.Record.RecalcIdRecord::get_DataSize()
extern void RecalcIdRecord_get_DataSize_m78250B3E70A38B0C7EC5A96D7AECFA9D9A50F416 (void);
// 0x00000807 System.Int16 NPOI.HSSF.Record.RecalcIdRecord::get_Sid()
extern void RecalcIdRecord_get_Sid_mC6E06D2255755075700793ABD7CC37AA4123F1DD (void);
// 0x00000808 System.Void NPOI.HSSF.Record.RecordFactory::.cctor()
extern void RecordFactory__cctor_m53B89A6E49C2E21539BC635411704BEB15BF779E (void);
// 0x00000809 System.Collections.Generic.List`1<NPOI.HSSF.Record.Record> NPOI.HSSF.Record.RecordFactory::CreateRecords(System.IO.Stream)
extern void RecordFactory_CreateRecords_mEBFF740D8C3E6462F76C9CB2D5FFCF2E94C6B67C (void);
// 0x0000080A NPOI.HSSF.Record.Record NPOI.HSSF.Record.RecordFactory::CreateSingleRecord(NPOI.HSSF.Record.RecordInputStream)
extern void RecordFactory_CreateSingleRecord_mE6AFDA1C3E520E0C25FABBF52089DCE60E2058F9 (void);
// 0x0000080B NPOI.HSSF.Record.NumberRecord NPOI.HSSF.Record.RecordFactory::ConvertToNumberRecord(NPOI.HSSF.Record.RKRecord)
extern void RecordFactory_ConvertToNumberRecord_m88D03709F67A2DB4362FAC1EC28C8607D9F553DB (void);
// 0x0000080C NPOI.HSSF.Record.NumberRecord[] NPOI.HSSF.Record.RecordFactory::ConvertRKRecords(NPOI.HSSF.Record.MulRKRecord)
extern void RecordFactory_ConvertRKRecords_m5609A7051BA679F428090F725ACE273094844704 (void);
// 0x0000080D System.Collections.Generic.Dictionary`2<System.Int16,NPOI.HSSF.Record.RecordFactory_I_RecordCreator> NPOI.HSSF.Record.RecordFactory::RecordsToMap(System.Type[])
extern void RecordFactory_RecordsToMap_m7F351388AA82167D5C7D5D83BA34A870C694133E (void);
// 0x0000080E NPOI.HSSF.Record.RecordFactory_I_RecordCreator NPOI.HSSF.Record.RecordFactory::GetRecordCreator(System.Type)
extern void RecordFactory_GetRecordCreator_m0FDA9F1A8FA1137592F1BEBD7A1CE93C629101B1 (void);
// 0x0000080F NPOI.HSSF.Record.Record NPOI.HSSF.Record.RecordFactory_I_RecordCreator::Create(NPOI.HSSF.Record.RecordInputStream)
// 0x00000810 System.Type NPOI.HSSF.Record.RecordFactory_I_RecordCreator::GetRecordClass()
// 0x00000811 System.Void NPOI.HSSF.Record.RecordFactory_ReflectionConstructorRecordCreator::.ctor(System.Reflection.ConstructorInfo)
extern void ReflectionConstructorRecordCreator__ctor_m50CB74A2DEA44C34289B3EFBAC932E882BACFB5C (void);
// 0x00000812 NPOI.HSSF.Record.Record NPOI.HSSF.Record.RecordFactory_ReflectionConstructorRecordCreator::Create(NPOI.HSSF.Record.RecordInputStream)
extern void ReflectionConstructorRecordCreator_Create_m77FA2E7789CB73E6B23CB8275336D7B4A23547BA (void);
// 0x00000813 System.Type NPOI.HSSF.Record.RecordFactory_ReflectionConstructorRecordCreator::GetRecordClass()
extern void ReflectionConstructorRecordCreator_GetRecordClass_m6167B3F8355793FA37E9183D77BE171D8E85F9CB (void);
// 0x00000814 System.Void NPOI.HSSF.Record.RecordFactory_ReflectionMethodRecordCreator::.ctor(System.Reflection.MethodInfo)
extern void ReflectionMethodRecordCreator__ctor_m8431EF8FD447E6AC656A6D824C68E1829D233CFF (void);
// 0x00000815 NPOI.HSSF.Record.Record NPOI.HSSF.Record.RecordFactory_ReflectionMethodRecordCreator::Create(NPOI.HSSF.Record.RecordInputStream)
extern void ReflectionMethodRecordCreator_Create_m29577722763241DF383CFE1BBBC427CF43A1E749 (void);
// 0x00000816 System.Type NPOI.HSSF.Record.RecordFactory_ReflectionMethodRecordCreator::GetRecordClass()
extern void ReflectionMethodRecordCreator_GetRecordClass_m881BB83A4CF742D8D421997791E01FC15CA1C4C9 (void);
// 0x00000817 System.Void NPOI.HSSF.Record.RecordFactoryInputStream::.ctor(System.IO.Stream,System.Boolean)
extern void RecordFactoryInputStream__ctor_mBF56FB3D3179B84236B44BD3223DCADEF87AFA04 (void);
// 0x00000818 NPOI.HSSF.Record.Record NPOI.HSSF.Record.RecordFactoryInputStream::NextRecord()
extern void RecordFactoryInputStream_NextRecord_m4A7383BEE7B11FF1BFBA8AB2ED1E2476019D2BFB (void);
// 0x00000819 NPOI.HSSF.Record.Record NPOI.HSSF.Record.RecordFactoryInputStream::GetNextUnreadRecord()
extern void RecordFactoryInputStream_GetNextUnreadRecord_m6A549A564CA56588DDC2CEB5CFBB64BD969C0988 (void);
// 0x0000081A NPOI.HSSF.Record.Record NPOI.HSSF.Record.RecordFactoryInputStream::ReadNextRecord()
extern void RecordFactoryInputStream_ReadNextRecord_mB0CC037CDB5AE3A53FE4F65A218AF20F56188A19 (void);
// 0x0000081B System.Void NPOI.HSSF.Record.RecordFactoryInputStream_StreamEncryptionInfo::.ctor(NPOI.HSSF.Record.RecordInputStream,System.Collections.Generic.List`1<NPOI.HSSF.Record.Record>)
extern void StreamEncryptionInfo__ctor_m754B9CFD524DF414B1378C2480F007202CF1C313 (void);
// 0x0000081C NPOI.HSSF.Record.RecordInputStream NPOI.HSSF.Record.RecordFactoryInputStream_StreamEncryptionInfo::CreateDecryptingStream(System.IO.Stream)
extern void StreamEncryptionInfo_CreateDecryptingStream_m4D56C598134EA6D79FC9BBA8956EBC5B1A786DD8 (void);
// 0x0000081D System.Boolean NPOI.HSSF.Record.RecordFactoryInputStream_StreamEncryptionInfo::get_HasEncryption()
extern void StreamEncryptionInfo_get_HasEncryption_mCEAAA2F3BB2F4E3F17393DA72122531876BCA80F (void);
// 0x0000081E NPOI.HSSF.Record.Record NPOI.HSSF.Record.RecordFactoryInputStream_StreamEncryptionInfo::get_LastRecord()
extern void StreamEncryptionInfo_get_LastRecord_m9E2A7E5DB57DACA82098973E91F7941AE186EEB9 (void);
// 0x0000081F System.Boolean NPOI.HSSF.Record.RecordFactoryInputStream_StreamEncryptionInfo::get_HasBOFRecord()
extern void StreamEncryptionInfo_get_HasBOFRecord_m64D1ACB329416DF861E27A79C894F49AA53E0DA3 (void);
// 0x00000820 System.Void NPOI.HSSF.Record.LeftoverDataException::.ctor(System.Int32,System.Int32)
extern void LeftoverDataException__ctor_m0E9F1E5CC662CF5CB791CABFBA5200546829D106 (void);
// 0x00000821 NPOI.Util.ILittleEndianInput NPOI.HSSF.Record.SimpleHeaderInput::GetLEI(System.IO.Stream)
extern void SimpleHeaderInput_GetLEI_m561E5EC47A50D378FC0E0A99CE5497F17ACA3A4B (void);
// 0x00000822 System.Void NPOI.HSSF.Record.SimpleHeaderInput::.ctor(System.IO.Stream)
extern void SimpleHeaderInput__ctor_m5469DD62B58D79DF21DFB89B83D5CD08F39CA155 (void);
// 0x00000823 System.Int32 NPOI.HSSF.Record.SimpleHeaderInput::Available()
extern void SimpleHeaderInput_Available_m652BFAEF563D065932F425F63BCE906764EAF215 (void);
// 0x00000824 System.Int32 NPOI.HSSF.Record.SimpleHeaderInput::ReadDataSize()
extern void SimpleHeaderInput_ReadDataSize_m41580F8E84F13604187E4A2439DF3B9274388C08 (void);
// 0x00000825 System.Int32 NPOI.HSSF.Record.SimpleHeaderInput::ReadRecordSID()
extern void SimpleHeaderInput_ReadRecordSID_mB3E3E36ECD147CD589728DD59C61857B4E3861AF (void);
// 0x00000826 System.Void NPOI.HSSF.Record.RecordInputStream::.ctor(System.IO.Stream)
extern void RecordInputStream__ctor_mFD341E929CB85E0D1445F5658A591DDE15A95045 (void);
// 0x00000827 System.Void NPOI.HSSF.Record.RecordInputStream::.ctor(System.IO.Stream,NPOI.HSSF.Record.Crypto.Biff8EncryptionKey,System.Int32)
extern void RecordInputStream__ctor_m8ABA7689D8ABF810D12D8F08F2455BA32CF7472A (void);
// 0x00000828 System.Int32 NPOI.HSSF.Record.RecordInputStream::Available()
extern void RecordInputStream_Available_mCA63DF46EB20ACBD70A8DB13564034882B89BF4D (void);
// 0x00000829 System.Int32 NPOI.HSSF.Record.RecordInputStream::ReadNextSid()
extern void RecordInputStream_ReadNextSid_m7575504638C54B563C579327EC8C8A99FB9FE6EF (void);
// 0x0000082A System.Int16 NPOI.HSSF.Record.RecordInputStream::get_Sid()
extern void RecordInputStream_get_Sid_m20755873AC0545FEC5F690B73EF2ECB17BAB8B92 (void);
// 0x0000082B System.Int64 NPOI.HSSF.Record.RecordInputStream::get_Position()
extern void RecordInputStream_get_Position_m8DCD05DECCDA5F5C8663CFFCA9B1006187373B01 (void);
// 0x0000082C System.Void NPOI.HSSF.Record.RecordInputStream::set_Position(System.Int64)
extern void RecordInputStream_set_Position_mDDFECEE932D00B63459AE2FFE5821CCB9A735E7B (void);
// 0x0000082D System.Int64 NPOI.HSSF.Record.RecordInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void RecordInputStream_Seek_mA249A618F69BB2ADBC1FAD051383118272077FA7 (void);
// 0x0000082E System.Boolean NPOI.HSSF.Record.RecordInputStream::get_HasNextRecord()
extern void RecordInputStream_get_HasNextRecord_m0738131F79912DC2E2028BFCAC1EE258B4CCF4F7 (void);
// 0x0000082F System.Void NPOI.HSSF.Record.RecordInputStream::NextRecord()
extern void RecordInputStream_NextRecord_mE639B61BCBE6D82174ABBDA1B81170CCE08D97BF (void);
// 0x00000830 System.Void NPOI.HSSF.Record.RecordInputStream::CheckRecordPosition(System.Int32)
extern void RecordInputStream_CheckRecordPosition_mBD8533D26039A6745FA38E7E8BAE16141B9320FE (void);
// 0x00000831 System.Int32 NPOI.HSSF.Record.RecordInputStream::ReadByte()
extern void RecordInputStream_ReadByte_m4F63EF1BAC86659D46D92913615505AC58D610E8 (void);
// 0x00000832 System.Int16 NPOI.HSSF.Record.RecordInputStream::ReadShort()
extern void RecordInputStream_ReadShort_m1879DB5630221848FA7AD15EEB916BA4B710DA01 (void);
// 0x00000833 System.Int32 NPOI.HSSF.Record.RecordInputStream::ReadInt()
extern void RecordInputStream_ReadInt_m86790BDDD9960BB55BC4D04FF112B69A933211C3 (void);
// 0x00000834 System.Int64 NPOI.HSSF.Record.RecordInputStream::ReadLong()
extern void RecordInputStream_ReadLong_m6ADB749C4C7A4CB5633048D49E644F513527B84A (void);
// 0x00000835 System.Int32 NPOI.HSSF.Record.RecordInputStream::ReadUByte()
extern void RecordInputStream_ReadUByte_m01C8165DDD1DA4DAC33A5C58457C7A7850D3F392 (void);
// 0x00000836 System.Int32 NPOI.HSSF.Record.RecordInputStream::ReadUShort()
extern void RecordInputStream_ReadUShort_m15A4EC65BA16D9707BC42A0DA8C148E93520A33F (void);
// 0x00000837 System.Double NPOI.HSSF.Record.RecordInputStream::ReadDouble()
extern void RecordInputStream_ReadDouble_m57253C018417A021F6974C662A49E2DD4145BF54 (void);
// 0x00000838 System.Void NPOI.HSSF.Record.RecordInputStream::ReadFully(System.Byte[])
extern void RecordInputStream_ReadFully_mAC93FA0D427DA56A9AD8696E18E5E51C1FDF77F4 (void);
// 0x00000839 System.Void NPOI.HSSF.Record.RecordInputStream::ReadFully(System.Byte[],System.Int32,System.Int32)
extern void RecordInputStream_ReadFully_mB3024FA97F779CA1E508B33BF0581F38772777E3 (void);
// 0x0000083A System.Byte[] NPOI.HSSF.Record.RecordInputStream::ReadRemainder()
extern void RecordInputStream_ReadRemainder_mE68F07FB0A7CC9F3F10C8A3B20896D6D136FA9A9 (void);
// 0x0000083B System.Int32 NPOI.HSSF.Record.RecordInputStream::get_Remaining()
extern void RecordInputStream_get_Remaining_mF9C377253635FDCC7983633FE1109162614D1C3B (void);
// 0x0000083C System.Boolean NPOI.HSSF.Record.RecordInputStream::get_IsContinueNext()
extern void RecordInputStream_get_IsContinueNext_m846C00D6D5DCF252FF9E9A2797A52B31B53AFAC1 (void);
// 0x0000083D System.Int64 NPOI.HSSF.Record.RecordInputStream::get_Length()
extern void RecordInputStream_get_Length_m8919B2BED9812A55C042B6BDE7D8B911D0986F3D (void);
// 0x0000083E System.Void NPOI.HSSF.Record.RecordInputStream::Flush()
extern void RecordInputStream_Flush_m2399182A7F1D7404C5EC4F3B1E19DF2033E37097 (void);
// 0x0000083F System.Boolean NPOI.HSSF.Record.RecordInputStream::get_CanRead()
extern void RecordInputStream_get_CanRead_m070DAC024C10AAC40256ABF9EDEEE28B0B9970B8 (void);
// 0x00000840 System.Boolean NPOI.HSSF.Record.RecordInputStream::get_CanSeek()
extern void RecordInputStream_get_CanSeek_m58332DCC31CFEE58B9AD9C12F51C38EC3EF96A05 (void);
// 0x00000841 System.Boolean NPOI.HSSF.Record.RecordInputStream::get_CanWrite()
extern void RecordInputStream_get_CanWrite_mEE4DA255CD956757D9AD16B4C975A648BD904232 (void);
// 0x00000842 System.Void NPOI.HSSF.Record.RecordInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void RecordInputStream_Write_mE84515D753B76A723FCC9352984D33A11E9456E3 (void);
// 0x00000843 System.Int32 NPOI.HSSF.Record.RecordInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void RecordInputStream_Read_m76D86CAB6A255CAFE2CA9CDC9B29EFEC73E1C8A7 (void);
// 0x00000844 System.Void NPOI.HSSF.Record.RefModeRecord::.ctor()
extern void RefModeRecord__ctor_mCB1F96C36D437903F89B96255F0FD17B4C6C0D57 (void);
// 0x00000845 System.Int16 NPOI.HSSF.Record.RefModeRecord::get_Mode()
extern void RefModeRecord_get_Mode_mA8ED8CAD88B1116A8F15403A1845B6B767DC0309 (void);
// 0x00000846 System.Void NPOI.HSSF.Record.RefModeRecord::set_Mode(System.Int16)
extern void RefModeRecord_set_Mode_mF5154A84EE00CECDD7C53B3FE4AD24E314540E82 (void);
// 0x00000847 System.String NPOI.HSSF.Record.RefModeRecord::ToString()
extern void RefModeRecord_ToString_mAC9DE512B076F1B5E8B50D2837CC0E12CDD4BCAD (void);
// 0x00000848 System.Int16 NPOI.HSSF.Record.RefModeRecord::get_Sid()
extern void RefModeRecord_get_Sid_mE5F2E2102162F22AA7041E70C5CBF7BC8A49905D (void);
// 0x00000849 System.Object NPOI.HSSF.Record.RefModeRecord::Clone()
extern void RefModeRecord_Clone_m6E5453D3F8723856D4F2310BD59D6B7EB117A1CD (void);
// 0x0000084A System.Int32 NPOI.HSSF.Record.RefModeRecord::get_DataSize()
extern void RefModeRecord_get_DataSize_m5B39EEFE4D16E731E609CEB6BFB256A876F8A89D (void);
// 0x0000084B System.Void NPOI.HSSF.Record.RefModeRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void RefModeRecord_Serialize_m58FEA4888B0EA3744E62934BD6F846A143FBFFE5 (void);
// 0x0000084C System.Void NPOI.HSSF.Record.RefreshAllRecord::.ctor(System.Int32)
extern void RefreshAllRecord__ctor_mF70BD90CCB19B42929402D013C0C7E545ED6654F (void);
// 0x0000084D System.Void NPOI.HSSF.Record.RefreshAllRecord::.ctor(System.Boolean)
extern void RefreshAllRecord__ctor_mAE2596D80F1985AB83E2A6CEC6C7DCDD04EA017E (void);
// 0x0000084E System.Boolean NPOI.HSSF.Record.RefreshAllRecord::get_RefreshAll()
extern void RefreshAllRecord_get_RefreshAll_m395B110C79480B52AA65387190D1E17DC3F059B1 (void);
// 0x0000084F System.Void NPOI.HSSF.Record.RefreshAllRecord::set_RefreshAll(System.Boolean)
extern void RefreshAllRecord_set_RefreshAll_mB1C9ED266D9ADD54B8627F49982106AFE17AF116 (void);
// 0x00000850 System.String NPOI.HSSF.Record.RefreshAllRecord::ToString()
extern void RefreshAllRecord_ToString_mFF0B00CFCF36B7DC1E30C3B55CC55082E239CE81 (void);
// 0x00000851 System.Void NPOI.HSSF.Record.RefreshAllRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void RefreshAllRecord_Serialize_mB0801F7F26A1F01E9E0A764D37A00D0260BFE91E (void);
// 0x00000852 System.Int32 NPOI.HSSF.Record.RefreshAllRecord::get_DataSize()
extern void RefreshAllRecord_get_DataSize_m250079BE70840290A69D6E19FEF5ECA14ABDB58D (void);
// 0x00000853 System.Int16 NPOI.HSSF.Record.RefreshAllRecord::get_Sid()
extern void RefreshAllRecord_get_Sid_m7C9E5DA8605499478518E39D8FE9E549A4C9DA28 (void);
// 0x00000854 System.Object NPOI.HSSF.Record.RefreshAllRecord::Clone()
extern void RefreshAllRecord_Clone_m72244268FBC0A08346F515E4BDC8F3373CF7A566 (void);
// 0x00000855 System.Void NPOI.HSSF.Record.RefreshAllRecord::.cctor()
extern void RefreshAllRecord__cctor_m4A24EB452BCF2DB44BD12766A5DC5AD1411738B7 (void);
// 0x00000856 System.Int16 NPOI.HSSF.Record.RightMarginRecord::get_Sid()
extern void RightMarginRecord_get_Sid_m2F49FC6ADE01BEF4923560579326BCD03146F6B7 (void);
// 0x00000857 System.Void NPOI.HSSF.Record.RightMarginRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void RightMarginRecord_Serialize_m2037F6D07A14DFF069B0C771E77E88BD6BA72E28 (void);
// 0x00000858 System.Int32 NPOI.HSSF.Record.RightMarginRecord::get_DataSize()
extern void RightMarginRecord_get_DataSize_mDD02CCFAC970CD111B507A0A2C6B3B723B4A6F44 (void);
// 0x00000859 System.Double NPOI.HSSF.Record.RKRecord::get_RKNumber()
extern void RKRecord_get_RKNumber_m8A4BC6AA85080A5F738A5687F7B90283D181D441 (void);
// 0x0000085A System.String NPOI.HSSF.Record.RKRecord::get_RecordName()
extern void RKRecord_get_RecordName_m3120DD9BE116B1BB73BB4CE9C27C587F775A0BB1 (void);
// 0x0000085B System.Void NPOI.HSSF.Record.RKRecord::AppendValueText(System.Text.StringBuilder)
extern void RKRecord_AppendValueText_m6FF89EC7D6F1FAF2D88F82E55F620AE7491D9C80 (void);
// 0x0000085C System.Void NPOI.HSSF.Record.RKRecord::SerializeValue(NPOI.Util.ILittleEndianOutput)
extern void RKRecord_SerializeValue_m478535A8DAFBE9CE55FDA8A0D867F9A82BAD04AF (void);
// 0x0000085D System.Int32 NPOI.HSSF.Record.RKRecord::get_ValueDataSize()
extern void RKRecord_get_ValueDataSize_mBBF80778BA3E2A6C8BDA11C4301DF5D12F98C0F7 (void);
// 0x0000085E System.Int16 NPOI.HSSF.Record.RKRecord::get_Sid()
extern void RKRecord_get_Sid_m3538896B26FDFC91F076A8B39DBD460E31733689 (void);
// 0x0000085F System.Void NPOI.HSSF.Record.RowRecord::.ctor(System.Int32)
extern void RowRecord__ctor_m64447BC45A8B1CEAC6F089E71EF9DF86B1A501FE (void);
// 0x00000860 System.Void NPOI.HSSF.Record.RowRecord::SetEmpty()
extern void RowRecord_SetEmpty_m8920764924C85A59343DD5BCBB60973509CC30E6 (void);
// 0x00000861 System.Boolean NPOI.HSSF.Record.RowRecord::get_IsEmpty()
extern void RowRecord_get_IsEmpty_m851082F7DBFAD5C52981251CD61A81B17B09537E (void);
// 0x00000862 System.Int32 NPOI.HSSF.Record.RowRecord::get_RowNumber()
extern void RowRecord_get_RowNumber_mA648AC3F757988093B2CD9F60C123F908A577A7D (void);
// 0x00000863 System.Void NPOI.HSSF.Record.RowRecord::set_RowNumber(System.Int32)
extern void RowRecord_set_RowNumber_m768069F42B510C3604E88C078BB52CD770DE1C2B (void);
// 0x00000864 System.Int32 NPOI.HSSF.Record.RowRecord::get_FirstCol()
extern void RowRecord_get_FirstCol_mE304528FBA4FD174077CAEF5713280816AD70C35 (void);
// 0x00000865 System.Void NPOI.HSSF.Record.RowRecord::set_FirstCol(System.Int32)
extern void RowRecord_set_FirstCol_mF74A9A34B9622579F041D8D0A681038D93A9E9DE (void);
// 0x00000866 System.Int32 NPOI.HSSF.Record.RowRecord::get_LastCol()
extern void RowRecord_get_LastCol_mE3FF1A6560C1A37CF260F2A03D6B7843E16F8D7B (void);
// 0x00000867 System.Void NPOI.HSSF.Record.RowRecord::set_LastCol(System.Int32)
extern void RowRecord_set_LastCol_mA6B7CFB039124CC1D1BECA54BD90DFB65CBDA1D8 (void);
// 0x00000868 System.Int16 NPOI.HSSF.Record.RowRecord::get_Height()
extern void RowRecord_get_Height_m9229E558B1A38DA12E348074782CD2AFCA6BE3CC (void);
// 0x00000869 System.Void NPOI.HSSF.Record.RowRecord::set_Height(System.Int16)
extern void RowRecord_set_Height_mE813EC82E8B6D799C7C010E022DF8A52274FDF98 (void);
// 0x0000086A System.Int16 NPOI.HSSF.Record.RowRecord::get_Optimize()
extern void RowRecord_get_Optimize_mC2BA93A84FB80D2E729EE93DA587DCF7F60B2061 (void);
// 0x0000086B System.Int16 NPOI.HSSF.Record.RowRecord::get_OptionFlags()
extern void RowRecord_get_OptionFlags_mFAA2DA29B83B400244A2DA6B768B8E56D7464BD1 (void);
// 0x0000086C System.Int16 NPOI.HSSF.Record.RowRecord::get_OutlineLevel()
extern void RowRecord_get_OutlineLevel_m3FC19EABDA1FA5AC9C2AF9CBBE670857B5C727DC (void);
// 0x0000086D System.Boolean NPOI.HSSF.Record.RowRecord::get_Colapsed()
extern void RowRecord_get_Colapsed_m1C046233253CC33203D8FEBD163DAA06234AFDE7 (void);
// 0x0000086E System.Boolean NPOI.HSSF.Record.RowRecord::get_ZeroHeight()
extern void RowRecord_get_ZeroHeight_m30847EAE295F2D566AC5E5283B568F7E299BFFAC (void);
// 0x0000086F System.Boolean NPOI.HSSF.Record.RowRecord::get_BadFontHeight()
extern void RowRecord_get_BadFontHeight_m9A3FC9BFB50A654BD7A6F187B2264176DFC803E1 (void);
// 0x00000870 System.Void NPOI.HSSF.Record.RowRecord::set_BadFontHeight(System.Boolean)
extern void RowRecord_set_BadFontHeight_mC293EA09476D261F25EE0AFA6F2A9B291ED487A6 (void);
// 0x00000871 System.Boolean NPOI.HSSF.Record.RowRecord::get_Formatted()
extern void RowRecord_get_Formatted_m7BCE31099548B900855AD4857EE80AA5CA628F7B (void);
// 0x00000872 System.Int16 NPOI.HSSF.Record.RowRecord::get_OptionFlags2()
extern void RowRecord_get_OptionFlags2_m9638015715C1F2D2E2A7389126E70E62BE57CD96 (void);
// 0x00000873 System.Int16 NPOI.HSSF.Record.RowRecord::get_XFIndex()
extern void RowRecord_get_XFIndex_m9368AF6B19146326F1ED001B7522DECFC063A423 (void);
// 0x00000874 System.Boolean NPOI.HSSF.Record.RowRecord::get_TopBorder()
extern void RowRecord_get_TopBorder_m402FA13990E8D99B1FAD6718692011172E5A960B (void);
// 0x00000875 System.Boolean NPOI.HSSF.Record.RowRecord::get_BottomBorder()
extern void RowRecord_get_BottomBorder_m78A05858BEBC67B86CD965F96A24ECB6A687DF24 (void);
// 0x00000876 System.Boolean NPOI.HSSF.Record.RowRecord::get_PhoeneticGuide()
extern void RowRecord_get_PhoeneticGuide_m9F9E9496DD5BD6EBD444BFA5E0DCEF7731B3B326 (void);
// 0x00000877 System.String NPOI.HSSF.Record.RowRecord::ToString()
extern void RowRecord_ToString_m728903718B3E66361F7E186BA3900274339903DF (void);
// 0x00000878 System.Void NPOI.HSSF.Record.RowRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void RowRecord_Serialize_m9A841F60E630B17BC51C90BA5EFE55D9D1114EB1 (void);
// 0x00000879 System.Int32 NPOI.HSSF.Record.RowRecord::get_DataSize()
extern void RowRecord_get_DataSize_m0370A97B894B18CB0E489084DF7034C501296472 (void);
// 0x0000087A System.Int32 NPOI.HSSF.Record.RowRecord::get_RecordSize()
extern void RowRecord_get_RecordSize_mEDFDA60A5BDD4ADA3822997F519D9E81C64F4372 (void);
// 0x0000087B System.Int16 NPOI.HSSF.Record.RowRecord::get_Sid()
extern void RowRecord_get_Sid_m88BBA6264111DE03E070228CCA34EBB2AFDB4D45 (void);
// 0x0000087C System.Int32 NPOI.HSSF.Record.RowRecord::CompareTo(System.Object)
extern void RowRecord_CompareTo_mC3069F7587A1BDD03A93081852074DCD4D585C03 (void);
// 0x0000087D System.Boolean NPOI.HSSF.Record.RowRecord::Equals(System.Object)
extern void RowRecord_Equals_mF2707AF38AA6BCD6D56590AACDC8DAD098DDA8DE (void);
// 0x0000087E System.Int32 NPOI.HSSF.Record.RowRecord::GetHashCode()
extern void RowRecord_GetHashCode_mB8F5B1BC203DA9C174199FF822B9C298B0D1C158 (void);
// 0x0000087F System.Object NPOI.HSSF.Record.RowRecord::Clone()
extern void RowRecord_Clone_m8DB4D3B5B521A218D620D41DE1521E9587656816 (void);
// 0x00000880 System.Void NPOI.HSSF.Record.RowRecord::.cctor()
extern void RowRecord__cctor_m85A4877E8E3CB837E4BA282A85A7A05179FFEFF7 (void);
// 0x00000881 System.Void NPOI.HSSF.Record.SaveRecalcRecord::.ctor()
extern void SaveRecalcRecord__ctor_mF91141866EFCA6402B5E0BC9EA930CD4F9E86D46 (void);
// 0x00000882 System.Boolean NPOI.HSSF.Record.SaveRecalcRecord::get_Recalc()
extern void SaveRecalcRecord_get_Recalc_m9307D2A5E217A008C63F29AED78D3FB02058C790 (void);
// 0x00000883 System.Void NPOI.HSSF.Record.SaveRecalcRecord::set_Recalc(System.Boolean)
extern void SaveRecalcRecord_set_Recalc_m527F16E6A7D10626D980FFF2D6DB19DB3D94A719 (void);
// 0x00000884 System.String NPOI.HSSF.Record.SaveRecalcRecord::ToString()
extern void SaveRecalcRecord_ToString_m3199E59498688D920315AF9955A876E965D6E43F (void);
// 0x00000885 System.Void NPOI.HSSF.Record.SaveRecalcRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SaveRecalcRecord_Serialize_m3026EE4833A5BDB49C8359B4AEF848D177B79526 (void);
// 0x00000886 System.Int32 NPOI.HSSF.Record.SaveRecalcRecord::get_DataSize()
extern void SaveRecalcRecord_get_DataSize_m77233AF480A4A876B778E5EBB9DA62556E12DE47 (void);
// 0x00000887 System.Int16 NPOI.HSSF.Record.SaveRecalcRecord::get_Sid()
extern void SaveRecalcRecord_get_Sid_mD132C8F50703BBE6F422C5B3475059C7304A0F34 (void);
// 0x00000888 System.Object NPOI.HSSF.Record.SaveRecalcRecord::Clone()
extern void SaveRecalcRecord_Clone_m4E3BCC580D6BBF3136FBD1A85CF5816E6219FF12 (void);
// 0x00000889 System.Int16 NPOI.HSSF.Record.ScenarioProtectRecord::get_Sid()
extern void ScenarioProtectRecord_get_Sid_mEB8BBE033EC8B544AE1E802039123B1A82EE60CB (void);
// 0x0000088A System.Void NPOI.HSSF.Record.ScenarioProtectRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void ScenarioProtectRecord_Serialize_m2D75CEB6EF166947FD01AD011844E9212520D973 (void);
// 0x0000088B System.Int32 NPOI.HSSF.Record.ScenarioProtectRecord::get_DataSize()
extern void ScenarioProtectRecord_get_DataSize_m90CE173DA34C1E2F93910B1ABA466DB99BE51080 (void);
// 0x0000088C System.Void NPOI.HSSF.Record.SCLRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SCLRecord_Serialize_m67CA1339A3221E1CC1A9582C756D03B61DF2E233 (void);
// 0x0000088D System.Int32 NPOI.HSSF.Record.SCLRecord::get_DataSize()
extern void SCLRecord_get_DataSize_mB30EC64F67E86A555DCAE873FC5528DB127B926A (void);
// 0x0000088E System.Int16 NPOI.HSSF.Record.SCLRecord::get_Sid()
extern void SCLRecord_get_Sid_m5A96B24EF418E7359E6F3B4AA885FA3043C79DCC (void);
// 0x0000088F System.Void NPOI.HSSF.Record.SelectionRecord::.ctor(System.Int32,System.Int32)
extern void SelectionRecord__ctor_m195145E332740605A46134509EF83BBEC17133D2 (void);
// 0x00000890 System.Byte NPOI.HSSF.Record.SelectionRecord::get_Pane()
extern void SelectionRecord_get_Pane_m65414EDAE5CD0769E0626F1DC3E2C8D95AA41A39 (void);
// 0x00000891 System.Int32 NPOI.HSSF.Record.SelectionRecord::get_ActiveCellRow()
extern void SelectionRecord_get_ActiveCellRow_mAF3CF2451A8699CF26A5827178D0828BCAD51134 (void);
// 0x00000892 System.Int32 NPOI.HSSF.Record.SelectionRecord::get_ActiveCellCol()
extern void SelectionRecord_get_ActiveCellCol_mFB843BE6FF40B1BB28E809762AC1506160677781 (void);
// 0x00000893 System.Int32 NPOI.HSSF.Record.SelectionRecord::get_ActiveCellRef()
extern void SelectionRecord_get_ActiveCellRef_m5AB781F09226D96559B2C9FD3B6F0F2A87C15237 (void);
// 0x00000894 System.String NPOI.HSSF.Record.SelectionRecord::ToString()
extern void SelectionRecord_ToString_mC9E1970F3DF4994C7098FC45E11929EC1A5E3D95 (void);
// 0x00000895 System.Void NPOI.HSSF.Record.SelectionRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SelectionRecord_Serialize_m4BDBF617CD3736E379289FD29C715F3B31BDDA7B (void);
// 0x00000896 System.Int32 NPOI.HSSF.Record.SelectionRecord::get_DataSize()
extern void SelectionRecord_get_DataSize_mB9E7AFC76269B8EBB0851DBD8B689098DFF2E52F (void);
// 0x00000897 System.Int16 NPOI.HSSF.Record.SelectionRecord::get_Sid()
extern void SelectionRecord_get_Sid_m5567BA9EEE76F4B4F2EA7EE7E820A71E1715EBD8 (void);
// 0x00000898 System.Object NPOI.HSSF.Record.SelectionRecord::Clone()
extern void SelectionRecord_Clone_m21152CEF9E86011F3EBB203C762FDFF741BD1C8B (void);
// 0x00000899 System.Int32 NPOI.HSSF.Record.SharedFormulaRecord::get_ExtraDataSize()
extern void SharedFormulaRecord_get_ExtraDataSize_m6BC7A9C252B246E5E1C7B3D2B1A2FF4845F2FA1D (void);
// 0x0000089A System.Int16 NPOI.HSSF.Record.SharedFormulaRecord::get_Sid()
extern void SharedFormulaRecord_get_Sid_m9D1C282D6213948F86367CEF89C4B807C5801420 (void);
// 0x0000089B System.Void NPOI.HSSF.Record.SharedFormulaRecord::SerializeExtraData(NPOI.Util.ILittleEndianOutput)
extern void SharedFormulaRecord_SerializeExtraData_m30D906C37AC832CAC29257DE675BA7C10177CFE5 (void);
// 0x0000089C NPOI.SS.Formula.PTG.Ptg[] NPOI.HSSF.Record.SharedFormulaRecord::GetFormulaTokens(NPOI.HSSF.Record.FormulaRecord)
extern void SharedFormulaRecord_GetFormulaTokens_m83DEFD61C4CCD5328DFC27B4287B6C6C863F4D7A (void);
// 0x0000089D System.Void NPOI.HSSF.Record.SSTDeserializer::.ctor(NPOI.Util.IntMapper`1<NPOI.HSSF.Record.UnicodeString>)
extern void SSTDeserializer__ctor_m23D4D6855B13D2B96362EC12B8CA7325F70F8EE3 (void);
// 0x0000089E System.Void NPOI.HSSF.Record.SSTDeserializer::AddToStringTable(NPOI.Util.IntMapper`1<NPOI.HSSF.Record.UnicodeString>,NPOI.HSSF.Record.UnicodeString)
extern void SSTDeserializer_AddToStringTable_m5544F1786FF31C1B4A0A18131F81BC0D73E4C78C (void);
// 0x0000089F System.Void NPOI.HSSF.Record.SSTRecord::.ctor()
extern void SSTRecord__ctor_m16E36A1FD6D724F1B666DA3EF05255F0AD309A53 (void);
// 0x000008A0 System.Int32 NPOI.HSSF.Record.SSTRecord::AddString(NPOI.HSSF.Record.UnicodeString)
extern void SSTRecord_AddString_m2CEA4C37428D22640E81AC92CE07E92CBFD0B066 (void);
// 0x000008A1 System.Int32 NPOI.HSSF.Record.SSTRecord::get_NumStrings()
extern void SSTRecord_get_NumStrings_m5706A91830E050B25675CB4492A3C08FEE1B0F72 (void);
// 0x000008A2 System.Int32 NPOI.HSSF.Record.SSTRecord::get_NumUniqueStrings()
extern void SSTRecord_get_NumUniqueStrings_mD3BADD3D8F85E8427E289D936CE4470E0E18A476 (void);
// 0x000008A3 NPOI.HSSF.Record.UnicodeString NPOI.HSSF.Record.SSTRecord::GetString(System.Int32)
extern void SSTRecord_GetString_m4A94A3447156B44694D46DB7D293423B327CCA08 (void);
// 0x000008A4 System.String NPOI.HSSF.Record.SSTRecord::ToString()
extern void SSTRecord_ToString_m67F8610001F36CD03E11076518C6DD946CAF2E2E (void);
// 0x000008A5 System.Int16 NPOI.HSSF.Record.SSTRecord::get_Sid()
extern void SSTRecord_get_Sid_m5E27CEA8F35FA65D83B532C4F7817359BB9D270B (void);
// 0x000008A6 System.Int32 NPOI.HSSF.Record.SSTRecord::GetHashCode()
extern void SSTRecord_GetHashCode_m1B05942A74649CA9FB45CF4A82D69051F8157098 (void);
// 0x000008A7 System.Boolean NPOI.HSSF.Record.SSTRecord::Equals(System.Object)
extern void SSTRecord_Equals_m8CD4067D2AEB2F6B9E4709B75E8CB2FB66E55AB4 (void);
// 0x000008A8 System.Void NPOI.HSSF.Record.SSTRecord::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void SSTRecord_Serialize_m9B2B379597CEC52ED1BEFAD4C77A1209D9430339 (void);
// 0x000008A9 NPOI.HSSF.Record.ExtSSTRecord NPOI.HSSF.Record.SSTRecord::CreateExtSSTRecord(System.Int32)
extern void SSTRecord_CreateExtSSTRecord_m9F271C4C21C42B4A1F141C1C6FC5BE636F2E0356 (void);
// 0x000008AA System.Int32 NPOI.HSSF.Record.SSTRecord::CalcExtSSTRecordSize()
extern void SSTRecord_CalcExtSSTRecordSize_m01EE3F383ED470DE31AEBC2725C71736700B1AFB (void);
// 0x000008AB System.Void NPOI.HSSF.Record.SSTRecord::.cctor()
extern void SSTRecord__cctor_m5722030B9CDF941C49DBB4FE38233F862F815745 (void);
// 0x000008AC System.Void NPOI.HSSF.Record.SSTSerializer::.ctor(NPOI.Util.IntMapper`1<NPOI.HSSF.Record.UnicodeString>,System.Int32,System.Int32)
extern void SSTSerializer__ctor_m9519E155243E90CF84030CF2AEE895BB1EFB86D4 (void);
// 0x000008AD System.Void NPOI.HSSF.Record.SSTSerializer::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void SSTSerializer_Serialize_mED4EF4451A564D1E7643C5E2961618A2DC946CF4 (void);
// 0x000008AE NPOI.HSSF.Record.UnicodeString NPOI.HSSF.Record.SSTSerializer::GetUnicodeString(System.Int32)
extern void SSTSerializer_GetUnicodeString_m7F7AA5CCE7118425BB6938278F2D02BC3DDD21A0 (void);
// 0x000008AF NPOI.HSSF.Record.UnicodeString NPOI.HSSF.Record.SSTSerializer::GetUnicodeString(NPOI.Util.IntMapper`1<NPOI.HSSF.Record.UnicodeString>,System.Int32)
extern void SSTSerializer_GetUnicodeString_m7267C72068A9F1FC7FFDFADD483EB8DD76560C6B (void);
// 0x000008B0 System.Int32[] NPOI.HSSF.Record.SSTSerializer::get_BucketAbsoluteOffsets()
extern void SSTSerializer_get_BucketAbsoluteOffsets_m2DA8DF91F235063D07ABD937943CA7110A9BE14A (void);
// 0x000008B1 System.Int32[] NPOI.HSSF.Record.SSTSerializer::get_BucketRelativeOffsets()
extern void SSTSerializer_get_BucketRelativeOffsets_m1915B6CA02D34D1BFD4865CE341FBB4108AD7782 (void);
// 0x000008B2 System.Void NPOI.HSSF.Record.StringRecord::.ctor()
extern void StringRecord__ctor_m7FA3D2DB7E7B354ADE10301DA892B62C1A0331EB (void);
// 0x000008B3 System.Void NPOI.HSSF.Record.StringRecord::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void StringRecord_Serialize_mF1B4E60A643A03E3A72A162BFD3D2BD17358D5BF (void);
// 0x000008B4 System.Int16 NPOI.HSSF.Record.StringRecord::get_Sid()
extern void StringRecord_get_Sid_m8B40120F6045DF7AFE7D9FC288B0508A52840C3C (void);
// 0x000008B5 System.String NPOI.HSSF.Record.StringRecord::get_String()
extern void StringRecord_get_String_m425EBF16927CCEC85DCC3EB3C8190A3C8644FC48 (void);
// 0x000008B6 System.Void NPOI.HSSF.Record.StringRecord::set_String(System.String)
extern void StringRecord_set_String_mD4BB8FE3FD99EF96F205D9C242A5E30478020B2F (void);
// 0x000008B7 System.String NPOI.HSSF.Record.StringRecord::ToString()
extern void StringRecord_ToString_mF3F36939DEF70AF993C1ED29D994C2DBB8D8243E (void);
// 0x000008B8 System.Object NPOI.HSSF.Record.StringRecord::Clone()
extern void StringRecord_Clone_m6EB537D9C7DBBF7A29C80503AFD8E3FDEF33362D (void);
// 0x000008B9 System.Void NPOI.HSSF.Record.StyleRecord::.ctor()
extern void StyleRecord__ctor_mF3D5E85475EB0336D9A32E351B748B2A842A46C1 (void);
// 0x000008BA System.Boolean NPOI.HSSF.Record.StyleRecord::get_IsBuiltin()
extern void StyleRecord_get_IsBuiltin_mDD815FC2A966DCF32AA4BD61D2F4304FDCEE8ED7 (void);
// 0x000008BB System.Void NPOI.HSSF.Record.StyleRecord::SetBuiltinStyle(System.Int32)
extern void StyleRecord_SetBuiltinStyle_m720C42202F3868F87B402D364312FFB86E87E3AB (void);
// 0x000008BC System.Int16 NPOI.HSSF.Record.StyleRecord::get_XFIndex()
extern void StyleRecord_get_XFIndex_m5D11B6BF3D1029CE33EB279CD7B15C6CE039EF68 (void);
// 0x000008BD System.Void NPOI.HSSF.Record.StyleRecord::set_XFIndex(System.Int16)
extern void StyleRecord_set_XFIndex_m42A56088B921969A1AE2E0695E0E9FAF45A8C9DF (void);
// 0x000008BE System.String NPOI.HSSF.Record.StyleRecord::get_Name()
extern void StyleRecord_get_Name_mB71565F2051DA923E1E5360DC59EB9DD0A0E22AC (void);
// 0x000008BF System.Void NPOI.HSSF.Record.StyleRecord::set_OutlineStyleLevel(System.Int32)
extern void StyleRecord_set_OutlineStyleLevel_mF09F106E7FD36521154F6475EA830D3C6742C3F7 (void);
// 0x000008C0 System.String NPOI.HSSF.Record.StyleRecord::ToString()
extern void StyleRecord_ToString_mFB816A88104B97D23B53D4E3FDDCA70C26F26C37 (void);
// 0x000008C1 System.Int16 NPOI.HSSF.Record.StyleRecord::SetField(System.Int32,System.Int32,System.Int32,System.Int32)
extern void StyleRecord_SetField_m09980269327CF33AA08FA9B6FAFB032A786223FE (void);
// 0x000008C2 System.Void NPOI.HSSF.Record.StyleRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void StyleRecord_Serialize_mAC591FB00DBA4E8E1C7C17D440BFCD1F5C50C9D5 (void);
// 0x000008C3 System.Int32 NPOI.HSSF.Record.StyleRecord::get_DataSize()
extern void StyleRecord_get_DataSize_m6A9DCCB55C9F42590D554F58DA289F17AE83105A (void);
// 0x000008C4 System.Int16 NPOI.HSSF.Record.StyleRecord::get_Sid()
extern void StyleRecord_get_Sid_mD675A03B83D54FF68B8BF1AB88352D0154A8EE7E (void);
// 0x000008C5 System.Void NPOI.HSSF.Record.StyleRecord::.cctor()
extern void StyleRecord__cctor_mE5C077996C01E21FA928012BE865CC9B6763937C (void);
// 0x000008C6 NPOI.HSSF.Record.SupBookRecord NPOI.HSSF.Record.SupBookRecord::CreateInternalReferences(System.Int16)
extern void SupBookRecord_CreateInternalReferences_mE1D1C9207538FDAD55CAC4B9B91B144B4DF80922 (void);
// 0x000008C7 NPOI.HSSF.Record.SupBookRecord NPOI.HSSF.Record.SupBookRecord::CreateAddInFunctions()
extern void SupBookRecord_CreateAddInFunctions_mE436D378B9C588DBE90312D909A29C968AEC6393 (void);
// 0x000008C8 System.Void NPOI.HSSF.Record.SupBookRecord::.ctor(System.Boolean,System.Int16)
extern void SupBookRecord__ctor_mCD9367D74AC28A8BF6C75CDADC3A7E90CFF8C17B (void);
// 0x000008C9 System.Boolean NPOI.HSSF.Record.SupBookRecord::get_IsExternalReferences()
extern void SupBookRecord_get_IsExternalReferences_m10DC2DCC19DAB338473CD24E71FE4910F8A6E7CC (void);
// 0x000008CA System.Boolean NPOI.HSSF.Record.SupBookRecord::get_IsInternalReferences()
extern void SupBookRecord_get_IsInternalReferences_mCD9A73B2E33C1866AAB2384ED7D05DD15759ECDB (void);
// 0x000008CB System.Boolean NPOI.HSSF.Record.SupBookRecord::get_IsAddInFunctions()
extern void SupBookRecord_get_IsAddInFunctions_m82206CCD10ECD3DCD1FE030F1DA51AE2792C7D9E (void);
// 0x000008CC System.String NPOI.HSSF.Record.SupBookRecord::ToString()
extern void SupBookRecord_ToString_mE9E52418C3A11067165FDA712B16AB7FD88CB286 (void);
// 0x000008CD System.Int32 NPOI.HSSF.Record.SupBookRecord::get_DataSize()
extern void SupBookRecord_get_DataSize_m70D309DDA8E31A978D478490F18DB9041AB45E38 (void);
// 0x000008CE System.Void NPOI.HSSF.Record.SupBookRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void SupBookRecord_Serialize_mFFC74830AC6C64435FEC9548C2A2CE3FBC02A688 (void);
// 0x000008CF System.Int16 NPOI.HSSF.Record.SupBookRecord::get_Sid()
extern void SupBookRecord_get_Sid_mD7075157D4D490AB3B3C6A08F389D32435F290B9 (void);
// 0x000008D0 System.String NPOI.HSSF.Record.SupBookRecord::get_URL()
extern void SupBookRecord_get_URL_m13A855469DDAE1CD3F78428B7EDE14352D32D572 (void);
// 0x000008D1 System.String NPOI.HSSF.Record.SupBookRecord::DecodeFileName(System.String)
extern void SupBookRecord_DecodeFileName_mB2B15C027A510B7518469542F3A3804D0859D751 (void);
// 0x000008D2 System.String[] NPOI.HSSF.Record.SupBookRecord::get_SheetNames()
extern void SupBookRecord_get_SheetNames_m9596A1618289281D7CCF59408AE2FF3ECEB6CC47 (void);
// 0x000008D3 System.Void NPOI.HSSF.Record.SupBookRecord::.cctor()
extern void SupBookRecord__cctor_m1521D4BACD4F498C86B80FA67F4859120F46FBB5 (void);
// 0x000008D4 System.Void NPOI.HSSF.Record.TabIdRecord::.ctor()
extern void TabIdRecord__ctor_m86CA0ACF858380289C4F05CE1E48A6CA9B26A783 (void);
// 0x000008D5 System.Void NPOI.HSSF.Record.TabIdRecord::SetTabIdArray(System.Int16[])
extern void TabIdRecord_SetTabIdArray_mB6772DC98FAE9D7033C8FB6120D406F155648D53 (void);
// 0x000008D6 System.String NPOI.HSSF.Record.TabIdRecord::ToString()
extern void TabIdRecord_ToString_m3BE0A5359422C6787C1214EB2779344D37501616 (void);
// 0x000008D7 System.Void NPOI.HSSF.Record.TabIdRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void TabIdRecord_Serialize_mCD63B7430A66905E72B0BC605CE174B0F55134EA (void);
// 0x000008D8 System.Int32 NPOI.HSSF.Record.TabIdRecord::get_DataSize()
extern void TabIdRecord_get_DataSize_m0487C99B33D7062F82DF9AE6A040670D2E31C0E7 (void);
// 0x000008D9 System.Int16 NPOI.HSSF.Record.TabIdRecord::get_Sid()
extern void TabIdRecord_get_Sid_m0E4494940459494B92902B9959D68A98AA1EE1AA (void);
// 0x000008DA System.Void NPOI.HSSF.Record.TabIdRecord::.cctor()
extern void TabIdRecord__cctor_m16F36EC45AC2FC884C302082190DE8B5E5448E82 (void);
// 0x000008DB System.Int16 NPOI.HSSF.Record.TableRecord::get_Sid()
extern void TableRecord_get_Sid_mEB2024D21C41DCAA09998F8E0D7E6E0E9DAA2CA0 (void);
// 0x000008DC System.Int32 NPOI.HSSF.Record.TableRecord::get_ExtraDataSize()
extern void TableRecord_get_ExtraDataSize_mE47BFC867B05E658FDA125C730857CB6A345B633 (void);
// 0x000008DD System.Void NPOI.HSSF.Record.TableRecord::SerializeExtraData(NPOI.Util.ILittleEndianOutput)
extern void TableRecord_SerializeExtraData_m02D0ED231A5AA1B45A60C52DFAD76AAAF3D31234 (void);
// 0x000008DE System.Void NPOI.HSSF.Record.TableRecord::.cctor()
extern void TableRecord__cctor_m22EE4D6C9F8070FE91FC06F96ACC6D9DD56C8435 (void);
// 0x000008DF System.Void NPOI.HSSF.Record.TableStylesRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void TableStylesRecord_Serialize_mC55FC1F64AB59E3AF4CD5CE8E56754A4E5B5B1BD (void);
// 0x000008E0 System.Int32 NPOI.HSSF.Record.TableStylesRecord::get_DataSize()
extern void TableStylesRecord_get_DataSize_m493CC60BBC24A6B4DC089FC74793C5EAC6FC15D7 (void);
// 0x000008E1 System.Int16 NPOI.HSSF.Record.TableStylesRecord::get_Sid()
extern void TableStylesRecord_get_Sid_m243282FB5116A7705A620058F66A89D7E24538DB (void);
// 0x000008E2 System.Void NPOI.HSSF.Record.TextObjectRecord::SerializeTrailingRecords(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void TextObjectRecord_SerializeTrailingRecords_m7DC645337938509CEA1855E829FB1F3497050D6A (void);
// 0x000008E3 System.Void NPOI.HSSF.Record.TextObjectRecord::WriteFormatData(NPOI.HSSF.Record.Cont.ContinuableRecordOutput,NPOI.SS.UserModel.IRichTextString)
extern void TextObjectRecord_WriteFormatData_m7A66B2B0D4185C6950AC7E5EFC3E6B5E2060C8B0 (void);
// 0x000008E4 System.Int32 NPOI.HSSF.Record.TextObjectRecord::get_FormattingDataLength()
extern void TextObjectRecord_get_FormattingDataLength_m4B02F69C40173021DD918AF1247BC96D13A975AC (void);
// 0x000008E5 System.Void NPOI.HSSF.Record.TextObjectRecord::SerializeTXORecord(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void TextObjectRecord_SerializeTXORecord_mFF33259EDABFFEAEFE1D9AEEA6EE8711E759C7B3 (void);
// 0x000008E6 System.Void NPOI.HSSF.Record.TextObjectRecord::Serialize(NPOI.HSSF.Record.Cont.ContinuableRecordOutput)
extern void TextObjectRecord_Serialize_mD6BD621BC31A3182A53F31009C2BA01745283B0B (void);
// 0x000008E7 System.Int16 NPOI.HSSF.Record.TextObjectRecord::get_Sid()
extern void TextObjectRecord_get_Sid_mC3F4D5E96731BBD9CDE85CB29CAD55FA885CE930 (void);
// 0x000008E8 System.Void NPOI.HSSF.Record.TopMarginRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void TopMarginRecord_Serialize_m7C87F6F6383D3058493F37C79E9675BC45FF6F9D (void);
// 0x000008E9 System.Int32 NPOI.HSSF.Record.TopMarginRecord::get_DataSize()
extern void TopMarginRecord_get_DataSize_m16506D5E2C80DA69C9A4EF797BA6D9A8B51BB90F (void);
// 0x000008EA System.Int16 NPOI.HSSF.Record.TopMarginRecord::get_Sid()
extern void TopMarginRecord_get_Sid_m03550DD863A55529DF2D4A99DC8792BC6B32B696 (void);
// 0x000008EB System.Void NPOI.HSSF.Record.UncalcedRecord::.ctor()
extern void UncalcedRecord__ctor_mDBD38C2B1A3A2A287E0C85C1050B341B3CF58D89 (void);
// 0x000008EC System.Int16 NPOI.HSSF.Record.UncalcedRecord::get_Sid()
extern void UncalcedRecord_get_Sid_m4ACEC0F202F1DA0636355B24C3D323CE65D8736C (void);
// 0x000008ED System.String NPOI.HSSF.Record.UncalcedRecord::ToString()
extern void UncalcedRecord_ToString_m160B4DBF9BB2B092052B3F8BFC292675541F97DC (void);
// 0x000008EE System.Void NPOI.HSSF.Record.UncalcedRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void UncalcedRecord_Serialize_m0CFEE37AF03ACAAE4386BB5ECFEFFEE7A72269A3 (void);
// 0x000008EF System.Int32 NPOI.HSSF.Record.UncalcedRecord::get_DataSize()
extern void UncalcedRecord_get_DataSize_mDA4C820E27BB19D081508DB573BD8D73F8A9D7A4 (void);
// 0x000008F0 System.Int32 NPOI.HSSF.Record.UncalcedRecord::get_StaticRecordSize()
extern void UncalcedRecord_get_StaticRecordSize_m0C119B7696D8847CC8EDAA5B2615A62CCEA6C4B9 (void);
// 0x000008F1 System.Void NPOI.HSSF.Record.UnknownRecord::.ctor(NPOI.HSSF.Record.RecordInputStream)
extern void UnknownRecord__ctor_mC1AEBA1253D120FBAD09307F93039677DFCA7422 (void);
// 0x000008F2 System.Void NPOI.HSSF.Record.UnknownRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void UnknownRecord_Serialize_mEEAC7AFF3F8E70DDA272763068D061D1E7F6418F (void);
// 0x000008F3 System.Int32 NPOI.HSSF.Record.UnknownRecord::get_DataSize()
extern void UnknownRecord_get_DataSize_mA543475F50A7E0E4C5DBE2EEF54B6B5B28E31034 (void);
// 0x000008F4 System.String NPOI.HSSF.Record.UnknownRecord::ToString()
extern void UnknownRecord_ToString_m9936256AD4CDEC4CCBD3E75FDF35D7FDEB65CB97 (void);
// 0x000008F5 System.String NPOI.HSSF.Record.UnknownRecord::GetBiffName(System.Int32)
extern void UnknownRecord_GetBiffName_m7208D968F0BA424F6C448A705A572342DC2F6CB3 (void);
// 0x000008F6 System.Boolean NPOI.HSSF.Record.UnknownRecord::IsObservedButUnknown(System.Int32)
extern void UnknownRecord_IsObservedButUnknown_mC0F63D37A69AB5021BC60007378DB62A414D3D9E (void);
// 0x000008F7 System.Int16 NPOI.HSSF.Record.UnknownRecord::get_Sid()
extern void UnknownRecord_get_Sid_m9C6D7133C649CDE1B03A15C556B599B1B1ABF232 (void);
// 0x000008F8 System.Object NPOI.HSSF.Record.UnknownRecord::Clone()
extern void UnknownRecord_Clone_mDED682E1B3CC57C94499FF3DD133A4508B7834BD (void);
// 0x000008F9 System.Void NPOI.HSSF.Record.UserSViewBegin::Serialize(NPOI.Util.ILittleEndianOutput)
extern void UserSViewBegin_Serialize_mF995472A0E2B43D0AA2CF54C558D50E5C3A7D426 (void);
// 0x000008FA System.Int32 NPOI.HSSF.Record.UserSViewBegin::get_DataSize()
extern void UserSViewBegin_get_DataSize_m94B6920AA76DFC94FC167239CCC01F5F105A3AA9 (void);
// 0x000008FB System.Int16 NPOI.HSSF.Record.UserSViewBegin::get_Sid()
extern void UserSViewBegin_get_Sid_m90AB68816D708D9170B096BAC89F960A1BEA9736 (void);
// 0x000008FC System.Byte[] NPOI.HSSF.Record.UserSViewBegin::get_Guid()
extern void UserSViewBegin_get_Guid_m3F3D1F152482F1074EB21C871CECCA00242A3567 (void);
// 0x000008FD System.Void NPOI.HSSF.Record.UserSViewEnd::Serialize(NPOI.Util.ILittleEndianOutput)
extern void UserSViewEnd_Serialize_mD1D3CCD1B942B9D9D6CBAD7E0D2B9B757F9CE8FE (void);
// 0x000008FE System.Int32 NPOI.HSSF.Record.UserSViewEnd::get_DataSize()
extern void UserSViewEnd_get_DataSize_m4D9FA452DF1C33372FFADAAB4C4B07D12FCCC4FF (void);
// 0x000008FF System.Int16 NPOI.HSSF.Record.UserSViewEnd::get_Sid()
extern void UserSViewEnd_get_Sid_mC54E0376BE432613B5D2FF6C29474456F3709885 (void);
// 0x00000900 System.Void NPOI.HSSF.Record.UseSelFSRecord::.ctor(System.Int32)
extern void UseSelFSRecord__ctor_m062180E5DED5AE2B64CE0582410A9D91C67F728E (void);
// 0x00000901 System.Void NPOI.HSSF.Record.UseSelFSRecord::.ctor(System.Boolean)
extern void UseSelFSRecord__ctor_m7653FFDEC53F5E05F4F2CC4B183BEC9F4167A1D8 (void);
// 0x00000902 System.String NPOI.HSSF.Record.UseSelFSRecord::ToString()
extern void UseSelFSRecord_ToString_m0376E93F2B5D7DE7A584A2076EBE64A7F3C08A49 (void);
// 0x00000903 System.Void NPOI.HSSF.Record.UseSelFSRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void UseSelFSRecord_Serialize_m6070089CCBB6AAE62A486524DE446311FB165CC2 (void);
// 0x00000904 System.Int32 NPOI.HSSF.Record.UseSelFSRecord::get_DataSize()
extern void UseSelFSRecord_get_DataSize_m3DDAB77262CFBEE9A0BB4E5B550E099BE328F97C (void);
// 0x00000905 System.Int16 NPOI.HSSF.Record.UseSelFSRecord::get_Sid()
extern void UseSelFSRecord_get_Sid_m619AD03C0B6E1656C14D127D4D0548DE6EA2118F (void);
// 0x00000906 System.Void NPOI.HSSF.Record.UseSelFSRecord::.cctor()
extern void UseSelFSRecord__cctor_m4A87FCA2EC681EEC23E684CAA03EE8E59655770F (void);
// 0x00000907 System.Void NPOI.HSSF.Record.VCenterRecord::.ctor()
extern void VCenterRecord__ctor_mA27B0E9DC294B9215F99FA870BDFB64DBED4C3D3 (void);
// 0x00000908 System.Boolean NPOI.HSSF.Record.VCenterRecord::get_VCenter()
extern void VCenterRecord_get_VCenter_m5F4EB5D310F64E02000B7F4C45CD558B3478CF7A (void);
// 0x00000909 System.Void NPOI.HSSF.Record.VCenterRecord::set_VCenter(System.Boolean)
extern void VCenterRecord_set_VCenter_m00211E31B9F1697A3E80A977CA9C4F5F6ED625B5 (void);
// 0x0000090A System.String NPOI.HSSF.Record.VCenterRecord::ToString()
extern void VCenterRecord_ToString_mF04DAA820C6C7C6683599FD6333E4A26B823AD01 (void);
// 0x0000090B System.Void NPOI.HSSF.Record.VCenterRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void VCenterRecord_Serialize_m7B05021B44A0B1BCBA4504D41599123CF6C3AE48 (void);
// 0x0000090C System.Int32 NPOI.HSSF.Record.VCenterRecord::get_DataSize()
extern void VCenterRecord_get_DataSize_m4DC8B2E7164303711D7CDD683FFA058480A29467 (void);
// 0x0000090D System.Int16 NPOI.HSSF.Record.VCenterRecord::get_Sid()
extern void VCenterRecord_get_Sid_mDEABFD542CB62F9080E42864E767D489F33C94C4 (void);
// 0x0000090E System.Object NPOI.HSSF.Record.VCenterRecord::Clone()
extern void VCenterRecord_Clone_mEA5830243D0C6BFE18B1BFD85F757C0C7536B58A (void);
// 0x0000090F System.Void NPOI.HSSF.Record.VerticalPageBreakRecord::.ctor()
extern void VerticalPageBreakRecord__ctor_m74A845B1EFC5CAC1E0637FACF9CA8654FACC0445 (void);
// 0x00000910 System.Int16 NPOI.HSSF.Record.VerticalPageBreakRecord::get_Sid()
extern void VerticalPageBreakRecord_get_Sid_m4A5E0B55272EFF5AAA7725B17EEEC9EDA364B547 (void);
// 0x00000911 System.Object NPOI.HSSF.Record.VerticalPageBreakRecord::Clone()
extern void VerticalPageBreakRecord_Clone_mAC6958EF6D928B5C68413002F798D1DFA3F656BE (void);
// 0x00000912 System.Void NPOI.HSSF.Record.WindowOneRecord::.ctor()
extern void WindowOneRecord__ctor_m9FD43904F6705267858AC46C9136BBB6BD38BD99 (void);
// 0x00000913 System.Int16 NPOI.HSSF.Record.WindowOneRecord::get_HorizontalHold()
extern void WindowOneRecord_get_HorizontalHold_m4B887AB2E155B79E253927F89FC82C6CEE363399 (void);
// 0x00000914 System.Void NPOI.HSSF.Record.WindowOneRecord::set_HorizontalHold(System.Int16)
extern void WindowOneRecord_set_HorizontalHold_m02861F7B478F4EDF68711521D9A21847293CE843 (void);
// 0x00000915 System.Int16 NPOI.HSSF.Record.WindowOneRecord::get_VerticalHold()
extern void WindowOneRecord_get_VerticalHold_m25AA5233D03E779892424743CA553503C64B25D9 (void);
// 0x00000916 System.Void NPOI.HSSF.Record.WindowOneRecord::set_VerticalHold(System.Int16)
extern void WindowOneRecord_set_VerticalHold_mF7A919B38E683DBA8C64095040A8703BA7CCAC5B (void);
// 0x00000917 System.Int16 NPOI.HSSF.Record.WindowOneRecord::get_Width()
extern void WindowOneRecord_get_Width_m60EB2A2D963B3B4D2DD451A4DE81CD20DC50EF9E (void);
// 0x00000918 System.Void NPOI.HSSF.Record.WindowOneRecord::set_Width(System.Int16)
extern void WindowOneRecord_set_Width_m3C248EDC7943DA48722F06C9C192748CADDCF19E (void);
// 0x00000919 System.Int16 NPOI.HSSF.Record.WindowOneRecord::get_Height()
extern void WindowOneRecord_get_Height_mD19FD203A60DDBF3A83BBDCAD8059884325908D4 (void);
// 0x0000091A System.Void NPOI.HSSF.Record.WindowOneRecord::set_Height(System.Int16)
extern void WindowOneRecord_set_Height_m6F4D0DC2AA316B638CF4FE0876485E6FC797EEFB (void);
// 0x0000091B System.Int16 NPOI.HSSF.Record.WindowOneRecord::get_Options()
extern void WindowOneRecord_get_Options_m9665626BEFF57F892BAE35BB39DC362A30CADCD4 (void);
// 0x0000091C System.Void NPOI.HSSF.Record.WindowOneRecord::set_Options(System.Int16)
extern void WindowOneRecord_set_Options_m99057C6BAA284B369A9D5BFBA43388B68799C204 (void);
// 0x0000091D System.Boolean NPOI.HSSF.Record.WindowOneRecord::get_Hidden()
extern void WindowOneRecord_get_Hidden_mB40D2DDB2562946A9406220C03737FBB8F2C5FD1 (void);
// 0x0000091E System.Boolean NPOI.HSSF.Record.WindowOneRecord::get_Iconic()
extern void WindowOneRecord_get_Iconic_m157C4213AADD11D4401E9DA510153C019D2335A9 (void);
// 0x0000091F System.Boolean NPOI.HSSF.Record.WindowOneRecord::get_DisplayHorizontalScrollbar()
extern void WindowOneRecord_get_DisplayHorizontalScrollbar_mA0DFA8216A026F4EC547CE9DCD7310DB372B07DF (void);
// 0x00000920 System.Boolean NPOI.HSSF.Record.WindowOneRecord::get_DisplayVerticalScrollbar()
extern void WindowOneRecord_get_DisplayVerticalScrollbar_mDC9C76D4F2D4B1A269CBBF0CD5FCA27BA23D7541 (void);
// 0x00000921 System.Boolean NPOI.HSSF.Record.WindowOneRecord::get_DisplayTabs()
extern void WindowOneRecord_get_DisplayTabs_m1435A9014F3C786CB4ED6830EA364A927D47E2FD (void);
// 0x00000922 System.Int32 NPOI.HSSF.Record.WindowOneRecord::get_ActiveSheetIndex()
extern void WindowOneRecord_get_ActiveSheetIndex_m5977C93DD6A1700792116C86DDBE7DB4809CD58E (void);
// 0x00000923 System.Void NPOI.HSSF.Record.WindowOneRecord::set_ActiveSheetIndex(System.Int32)
extern void WindowOneRecord_set_ActiveSheetIndex_m7F28D8AE0298B8465E5D7447C99B87CA5B72673E (void);
// 0x00000924 System.Int32 NPOI.HSSF.Record.WindowOneRecord::get_FirstVisibleTab()
extern void WindowOneRecord_get_FirstVisibleTab_m8D537304676907ACA3B0C7625420C458DF916664 (void);
// 0x00000925 System.Void NPOI.HSSF.Record.WindowOneRecord::set_FirstVisibleTab(System.Int32)
extern void WindowOneRecord_set_FirstVisibleTab_mD0D172C34056D56A4F642D8F077BFD7FE0D88FF6 (void);
// 0x00000926 System.Int16 NPOI.HSSF.Record.WindowOneRecord::get_NumSelectedTabs()
extern void WindowOneRecord_get_NumSelectedTabs_m839B69AF7ED308548AC06AD6C4488FC2BEE87DD8 (void);
// 0x00000927 System.Void NPOI.HSSF.Record.WindowOneRecord::set_NumSelectedTabs(System.Int16)
extern void WindowOneRecord_set_NumSelectedTabs_mC2DCE1E7036E3E2247FF801BF4F526DC59E56F87 (void);
// 0x00000928 System.Int16 NPOI.HSSF.Record.WindowOneRecord::get_TabWidthRatio()
extern void WindowOneRecord_get_TabWidthRatio_m7799D82BA8F1AEA0AA63F06206A35E8F5F6348FE (void);
// 0x00000929 System.Void NPOI.HSSF.Record.WindowOneRecord::set_TabWidthRatio(System.Int16)
extern void WindowOneRecord_set_TabWidthRatio_mFE1937D7EECBC1EE03D5470ED72412DFC988959B (void);
// 0x0000092A System.String NPOI.HSSF.Record.WindowOneRecord::ToString()
extern void WindowOneRecord_ToString_m545B713CAD638871BE30A459A3E5ED9DEB0411AC (void);
// 0x0000092B System.Void NPOI.HSSF.Record.WindowOneRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void WindowOneRecord_Serialize_m1EEAE7DD84EC16198FD90FB0B7D5F048EF88F308 (void);
// 0x0000092C System.Int32 NPOI.HSSF.Record.WindowOneRecord::get_DataSize()
extern void WindowOneRecord_get_DataSize_m819E03954B090FC750AF8F324C07E54F8FE457CE (void);
// 0x0000092D System.Int16 NPOI.HSSF.Record.WindowOneRecord::get_Sid()
extern void WindowOneRecord_get_Sid_mFAC78172FB45323FF0F0525031C1327E40644B18 (void);
// 0x0000092E System.Void NPOI.HSSF.Record.WindowOneRecord::.cctor()
extern void WindowOneRecord__cctor_m0FC15A9BCB748EAD2A5F3AE3A314A12775E9AC8D (void);
// 0x0000092F System.Void NPOI.HSSF.Record.WindowProtectRecord::.ctor(System.Int32)
extern void WindowProtectRecord__ctor_mBA21EEA0017A2C4F71C9926FB51B38E1F172688A (void);
// 0x00000930 System.Void NPOI.HSSF.Record.WindowProtectRecord::.ctor(System.Boolean)
extern void WindowProtectRecord__ctor_m505838284CDDC340810CCB6E2280F13B05C63A50 (void);
// 0x00000931 System.Boolean NPOI.HSSF.Record.WindowProtectRecord::get_Protect()
extern void WindowProtectRecord_get_Protect_m099F28916E7C03F65871FE8CCB2347032456DD4B (void);
// 0x00000932 System.Void NPOI.HSSF.Record.WindowProtectRecord::set_Protect(System.Boolean)
extern void WindowProtectRecord_set_Protect_m261C8D9A225C85CE0107EF75BF7E987A38C8631A (void);
// 0x00000933 System.String NPOI.HSSF.Record.WindowProtectRecord::ToString()
extern void WindowProtectRecord_ToString_m213070F7BBFF6B7D7D42978CFB1918776C2F44AA (void);
// 0x00000934 System.Void NPOI.HSSF.Record.WindowProtectRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void WindowProtectRecord_Serialize_mE997690B1601B3BD5785C8CE6763EFA31FE42E75 (void);
// 0x00000935 System.Int32 NPOI.HSSF.Record.WindowProtectRecord::get_DataSize()
extern void WindowProtectRecord_get_DataSize_mE546FF7A3487D58675C63C8C67B7EFBE9AA5C269 (void);
// 0x00000936 System.Int16 NPOI.HSSF.Record.WindowProtectRecord::get_Sid()
extern void WindowProtectRecord_get_Sid_mE4EA6C98BC17083D4B2FB631367FEF86716A7F6F (void);
// 0x00000937 System.Void NPOI.HSSF.Record.WindowProtectRecord::.cctor()
extern void WindowProtectRecord__cctor_m84E1D107EA41B2CE325F3847F331508577FEA859 (void);
// 0x00000938 System.Void NPOI.HSSF.Record.WindowTwoRecord::.ctor()
extern void WindowTwoRecord__ctor_mF307C8BF23F18E7B35642DB13D9CEE3950A6F2A9 (void);
// 0x00000939 System.Int16 NPOI.HSSF.Record.WindowTwoRecord::get_Options()
extern void WindowTwoRecord_get_Options_m50E2BEFE97961752A0869563277F810713C21E4F (void);
// 0x0000093A System.Void NPOI.HSSF.Record.WindowTwoRecord::set_Options(System.Int16)
extern void WindowTwoRecord_set_Options_m03AF67CB3A84EFFA8C1CF77AC99E713723CF025E (void);
// 0x0000093B System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_DisplayFormulas()
extern void WindowTwoRecord_get_DisplayFormulas_m405A1FC3069FCF75AED96EF63E855C9ABD957806 (void);
// 0x0000093C System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_DisplayGridlines()
extern void WindowTwoRecord_get_DisplayGridlines_m87D28B47AA9D2B363ED573DB3E9762BD679BDC85 (void);
// 0x0000093D System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_DisplayRowColHeadings()
extern void WindowTwoRecord_get_DisplayRowColHeadings_m9E0B85E6EF005542ED85B69C7E0D0236AF602A97 (void);
// 0x0000093E System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_FreezePanes()
extern void WindowTwoRecord_get_FreezePanes_mB12E34C184DD22FA3DD5DE647F3CE3ED1D4DCCC5 (void);
// 0x0000093F System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_DisplayZeros()
extern void WindowTwoRecord_get_DisplayZeros_mAE698D4F191488FD2BF9DEE1F482D3AB005ED781 (void);
// 0x00000940 System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_DefaultHeader()
extern void WindowTwoRecord_get_DefaultHeader_m40DFD00D06C56581898734178353B294DF679D08 (void);
// 0x00000941 System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_Arabic()
extern void WindowTwoRecord_get_Arabic_m24983272B7A9B949BC46D3B80B0D67370B907C31 (void);
// 0x00000942 System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_DisplayGuts()
extern void WindowTwoRecord_get_DisplayGuts_m371B14F695A5863708384FE272DD1FA64EB89F46 (void);
// 0x00000943 System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_FreezePanesNoSplit()
extern void WindowTwoRecord_get_FreezePanesNoSplit_m20E7EE102563EAA78F95576D71B935EB6E6056BE (void);
// 0x00000944 System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_IsSelected()
extern void WindowTwoRecord_get_IsSelected_mAA96C37F90D63025CFC4FE7F941CC7F57F3B991E (void);
// 0x00000945 System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_IsActive()
extern void WindowTwoRecord_get_IsActive_mB38587CAC3DF3E6DE1B3691ED6E55AA26539A58A (void);
// 0x00000946 System.Boolean NPOI.HSSF.Record.WindowTwoRecord::get_SavedInPageBreakPreview()
extern void WindowTwoRecord_get_SavedInPageBreakPreview_mC5D6B966956FD98B2411F82A0B54FCDA96BCCE72 (void);
// 0x00000947 System.Int16 NPOI.HSSF.Record.WindowTwoRecord::get_TopRow()
extern void WindowTwoRecord_get_TopRow_m2358AE76E963BAAA5D6817DBDD24BF9BE38CEEB5 (void);
// 0x00000948 System.Void NPOI.HSSF.Record.WindowTwoRecord::set_TopRow(System.Int16)
extern void WindowTwoRecord_set_TopRow_m73AD89D570353ADD70AE3CF9F014F0D3714C68A0 (void);
// 0x00000949 System.Int16 NPOI.HSSF.Record.WindowTwoRecord::get_LeftCol()
extern void WindowTwoRecord_get_LeftCol_mBD84FB3893515F11D0220B4BD646C1DEDECB213B (void);
// 0x0000094A System.Void NPOI.HSSF.Record.WindowTwoRecord::set_LeftCol(System.Int16)
extern void WindowTwoRecord_set_LeftCol_m31A261D93CBA6AE9F669C29510B053D4CF47C7EA (void);
// 0x0000094B System.Int32 NPOI.HSSF.Record.WindowTwoRecord::get_HeaderColor()
extern void WindowTwoRecord_get_HeaderColor_m22D6FDEC4BDA37DFB9DDA12FB1440E15386DE68A (void);
// 0x0000094C System.Void NPOI.HSSF.Record.WindowTwoRecord::set_HeaderColor(System.Int32)
extern void WindowTwoRecord_set_HeaderColor_mAB8F660F4F49EFD20CB0148BC5C86191FCCADECA (void);
// 0x0000094D System.Int16 NPOI.HSSF.Record.WindowTwoRecord::get_PageBreakZoom()
extern void WindowTwoRecord_get_PageBreakZoom_m93C462F2E106590786CFD74F35AC4BCCDFF5CC0B (void);
// 0x0000094E System.Void NPOI.HSSF.Record.WindowTwoRecord::set_PageBreakZoom(System.Int16)
extern void WindowTwoRecord_set_PageBreakZoom_m9223B363D4F65C85589FF82377B3317442353692 (void);
// 0x0000094F System.Int16 NPOI.HSSF.Record.WindowTwoRecord::get_NormalZoom()
extern void WindowTwoRecord_get_NormalZoom_mE00C045D17926A754131F27EE16AE16BFBD6F98B (void);
// 0x00000950 System.Void NPOI.HSSF.Record.WindowTwoRecord::set_NormalZoom(System.Int16)
extern void WindowTwoRecord_set_NormalZoom_mA9194DDF56D05456D47D2DC1F5E557453599D748 (void);
// 0x00000951 System.Int32 NPOI.HSSF.Record.WindowTwoRecord::get_Reserved()
extern void WindowTwoRecord_get_Reserved_mA697746E435CEF203C7A80D8477BC71B307211F9 (void);
// 0x00000952 System.String NPOI.HSSF.Record.WindowTwoRecord::ToString()
extern void WindowTwoRecord_ToString_m13B381012570F173649A5A4C547A86CC0F62722B (void);
// 0x00000953 System.Void NPOI.HSSF.Record.WindowTwoRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void WindowTwoRecord_Serialize_mD2CEB7C5E5196D0704780FFF917F07B97ECAD49C (void);
// 0x00000954 System.Int32 NPOI.HSSF.Record.WindowTwoRecord::get_DataSize()
extern void WindowTwoRecord_get_DataSize_m923E608C332653109EA18A9EBDF8F30793510BF6 (void);
// 0x00000955 System.Int16 NPOI.HSSF.Record.WindowTwoRecord::get_Sid()
extern void WindowTwoRecord_get_Sid_m9A1111ADCE7FBB52CAF27357A91B46AD3D1DCCCB (void);
// 0x00000956 System.Object NPOI.HSSF.Record.WindowTwoRecord::Clone()
extern void WindowTwoRecord_Clone_m437622D4BE07D618C1F60430A7C32A8657B814AD (void);
// 0x00000957 System.Void NPOI.HSSF.Record.WriteAccessRecord::.cctor()
extern void WriteAccessRecord__cctor_mB1506226A35C6F91A287A61256D5308D7EFF24BC (void);
// 0x00000958 System.Void NPOI.HSSF.Record.WriteAccessRecord::.ctor()
extern void WriteAccessRecord__ctor_m5E713DEF127E3CC376BAD835AC369E8C2735F201 (void);
// 0x00000959 System.String NPOI.HSSF.Record.WriteAccessRecord::get_Username()
extern void WriteAccessRecord_get_Username_m8FD5EA280BD6746FDEEF0F14B804BB1FE9890735 (void);
// 0x0000095A System.Void NPOI.HSSF.Record.WriteAccessRecord::set_Username(System.String)
extern void WriteAccessRecord_set_Username_m5B73FC6F60FC62ED2EB366C424D5499DB9321AB3 (void);
// 0x0000095B System.String NPOI.HSSF.Record.WriteAccessRecord::ToString()
extern void WriteAccessRecord_ToString_m8D6E2BF68290426F32113C0DC4BAD34238B2F971 (void);
// 0x0000095C System.Void NPOI.HSSF.Record.WriteAccessRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void WriteAccessRecord_Serialize_m85D235C351B1310CE6360261D2663E8EC47E30FC (void);
// 0x0000095D System.Int32 NPOI.HSSF.Record.WriteAccessRecord::get_DataSize()
extern void WriteAccessRecord_get_DataSize_mC4D129D8751D323DCFB8C5CC701B830511BE547A (void);
// 0x0000095E System.Int16 NPOI.HSSF.Record.WriteAccessRecord::get_Sid()
extern void WriteAccessRecord_get_Sid_mDA4A5CCF267BE09F1CC937C680AC33C4A570D771 (void);
// 0x0000095F System.Void NPOI.HSSF.Record.WriteProtectRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void WriteProtectRecord_Serialize_m80AC514AD113688F2B22F88726791430D5BAC5FB (void);
// 0x00000960 System.Int32 NPOI.HSSF.Record.WriteProtectRecord::get_DataSize()
extern void WriteProtectRecord_get_DataSize_mCCAAA7A44995D7603021ACC456284B53DB72C9B5 (void);
// 0x00000961 System.Int16 NPOI.HSSF.Record.WriteProtectRecord::get_Sid()
extern void WriteProtectRecord_get_Sid_mF56D38B6BF711C34EA08269F869FB267F643BBEB (void);
// 0x00000962 System.Void NPOI.HSSF.Record.WSBoolRecord::.ctor()
extern void WSBoolRecord__ctor_m4AEE3B82DAB5CF62EC7BF6D2C3FC68CA24E4A4CF (void);
// 0x00000963 System.Byte NPOI.HSSF.Record.WSBoolRecord::get_WSBool1()
extern void WSBoolRecord_get_WSBool1_m643F14C06361EA86A65D0D3EC494208AFD3160A3 (void);
// 0x00000964 System.Void NPOI.HSSF.Record.WSBoolRecord::set_WSBool1(System.Byte)
extern void WSBoolRecord_set_WSBool1_m9CB4A3EA7C9C8D00EDB7BBC48A8F8655D3B52D48 (void);
// 0x00000965 System.Boolean NPOI.HSSF.Record.WSBoolRecord::get_Autobreaks()
extern void WSBoolRecord_get_Autobreaks_m8748D741738775EA853115AE872EF7DFA77F37D2 (void);
// 0x00000966 System.Boolean NPOI.HSSF.Record.WSBoolRecord::get_Dialog()
extern void WSBoolRecord_get_Dialog_m5BF0FCC6CFA3ADC74321583E826746131D056437 (void);
// 0x00000967 System.Boolean NPOI.HSSF.Record.WSBoolRecord::get_RowSumsBelow()
extern void WSBoolRecord_get_RowSumsBelow_m6F5252F847DA1BE6724D0779D8BE807BDD4F2E67 (void);
// 0x00000968 System.Boolean NPOI.HSSF.Record.WSBoolRecord::get_RowSumsRight()
extern void WSBoolRecord_get_RowSumsRight_mB89FF659909FF67756B9D31C47D4A4C274444574 (void);
// 0x00000969 System.Byte NPOI.HSSF.Record.WSBoolRecord::get_WSBool2()
extern void WSBoolRecord_get_WSBool2_mD289102389BC268ED6FC9784686655D35D02B446 (void);
// 0x0000096A System.Void NPOI.HSSF.Record.WSBoolRecord::set_WSBool2(System.Byte)
extern void WSBoolRecord_set_WSBool2_m616F6D699A6DC1B13B31F8749CF512C1EC4551EF (void);
// 0x0000096B System.Boolean NPOI.HSSF.Record.WSBoolRecord::get_FitToPage()
extern void WSBoolRecord_get_FitToPage_m25D1A56EAC89130580FD616903675261F5BF9E9A (void);
// 0x0000096C System.Boolean NPOI.HSSF.Record.WSBoolRecord::get_DisplayGuts()
extern void WSBoolRecord_get_DisplayGuts_m972E1436A569E207CF2101F3F00B0FEB772D1BA6 (void);
// 0x0000096D System.Boolean NPOI.HSSF.Record.WSBoolRecord::get_AlternateExpression()
extern void WSBoolRecord_get_AlternateExpression_mDDD87A1A79184BFEF40661972EC85C8D03E0A5C4 (void);
// 0x0000096E System.Boolean NPOI.HSSF.Record.WSBoolRecord::get_AlternateFormula()
extern void WSBoolRecord_get_AlternateFormula_m766A709539DD7AA62BC52EFFE062B0B8C6F00786 (void);
// 0x0000096F System.String NPOI.HSSF.Record.WSBoolRecord::ToString()
extern void WSBoolRecord_ToString_m7A9FBE687C5B2EBFE7A24027AC9E54424B7D61F2 (void);
// 0x00000970 System.Void NPOI.HSSF.Record.WSBoolRecord::Serialize(NPOI.Util.ILittleEndianOutput)
extern void WSBoolRecord_Serialize_mD764A178210C1C68BA3D7BF62F9F775D77B38527 (void);
// 0x00000971 System.Int32 NPOI.HSSF.Record.WSBoolRecord::get_DataSize()
extern void WSBoolRecord_get_DataSize_m147686A1DA646CC381C2AE3C7F8EA0D35A8156E2 (void);
// 0x00000972 System.Int16 NPOI.HSSF.Record.WSBoolRecord::get_Sid()
extern void WSBoolRecord_get_Sid_mEAEAB568FFA93A1B3735B91BB33270BE07EAE132 (void);
// 0x00000973 System.Object NPOI.HSSF.Record.WSBoolRecord::Clone()
extern void WSBoolRecord_Clone_mA88A50C9727BCBF57FAACD92D3F2D2B2B30937A0 (void);
// 0x00000974 System.Void NPOI.HSSF.Record.WSBoolRecord::.cctor()
extern void WSBoolRecord__cctor_mAD7A81DF4F5A8030BDD69280093D1000B74E61A6 (void);
// 0x00000975 System.String NPOI.HSSF.UserModel.HSSFErrorConstants::GetText(System.Int32)
extern void HSSFErrorConstants_GetText_m173B74E37189DB1BBEFF369F4DB084E1346DEAD0 (void);
// 0x00000976 System.Boolean NPOI.HSSF.UserModel.HSSFErrorConstants::IsValidCode(System.Int32)
extern void HSSFErrorConstants_IsValidCode_m97CEE3E59FAABD51E727A6B02211E900EACE8CCB (void);
// 0x00000977 System.Void NPOI.EncryptedDocumentException::.ctor(System.String)
extern void EncryptedDocumentException__ctor_m5ED29DC71BA3B6D3B245FFBEEDDD185252626CF5 (void);
// 0x00000978 System.Void NPOI.POIFS.Common.POIFSBigBlockSize::.ctor(System.Int32,System.Int16)
extern void POIFSBigBlockSize__ctor_m941483271C02CAF5EC7723419C0FEAC8C4A26AFA (void);
// 0x00000979 System.Int32 NPOI.POIFS.Common.POIFSBigBlockSize::GetBigBlockSize()
extern void POIFSBigBlockSize_GetBigBlockSize_m1238D54BFE455B9A814D0D51D1D58880E59DB9C4 (void);
// 0x0000097A System.Int16 NPOI.POIFS.Common.POIFSBigBlockSize::GetHeaderValue()
extern void POIFSBigBlockSize_GetHeaderValue_mD69BB61FA1154A55170C31A8B284A8F29E0C773B (void);
// 0x0000097B System.Int32 NPOI.POIFS.Common.POIFSBigBlockSize::GetPropertiesPerBlock()
extern void POIFSBigBlockSize_GetPropertiesPerBlock_m6F470C0D7C61AE19AA8CD6777E3B476A82DC642E (void);
// 0x0000097C System.Int32 NPOI.POIFS.Common.POIFSBigBlockSize::GetBATEntriesPerBlock()
extern void POIFSBigBlockSize_GetBATEntriesPerBlock_m42D4E077C529FEB8C02C93E7A9D9E7AE02DBF693 (void);
// 0x0000097D System.Int32 NPOI.POIFS.Common.POIFSBigBlockSize::GetXBATEntriesPerBlock()
extern void POIFSBigBlockSize_GetXBATEntriesPerBlock_m5425AABA29DEBD7200BBC19BA8ED32645DB1157E (void);
// 0x0000097E System.Void NPOI.POIFS.Common.POIFSConstants::.cctor()
extern void POIFSConstants__cctor_m9A3547AF887390BC5439DB7368FA9A2141FE83B2 (void);
// 0x0000097F System.Void NPOI.SS.Formula.PTG.SharedFormula::.ctor(NPOI.SS.SpreadsheetVersion)
extern void SharedFormula__ctor_m982F3B256178C2D3100F744FE5523210BF8D4A36 (void);
// 0x00000980 NPOI.SS.Formula.PTG.Ptg[] NPOI.SS.Formula.PTG.SharedFormula::ConvertSharedFormulas(NPOI.SS.Formula.PTG.Ptg[],System.Int32,System.Int32)
extern void SharedFormula_ConvertSharedFormulas_mB7424E05E61F0C01C07C90FF6A98260566F9968B (void);
// 0x00000981 System.Int32 NPOI.SS.Formula.PTG.SharedFormula::FixupRelativeColumn(System.Int32,System.Int32,System.Boolean)
extern void SharedFormula_FixupRelativeColumn_m41646512F1022D96775F4D543334827A78AF4527 (void);
// 0x00000982 System.Int32 NPOI.SS.Formula.PTG.SharedFormula::FixupRelativeRow(System.Int32,System.Int32,System.Boolean)
extern void SharedFormula_FixupRelativeRow_mE66E08FCE9B8F33FAC80A781110E8C19605AF215 (void);
// 0x00000983 System.Void NPOI.Util.ByteBuffer::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Byte[],System.Int32)
extern void ByteBuffer__ctor_mBAE7DF973D9D315EF8EE56668C968BF536979C0D (void);
// 0x00000984 System.Void NPOI.Util.ByteBuffer::.ctor(System.Int32,System.Int32)
extern void ByteBuffer__ctor_m64EA2B693DE61F693BF69927CEB4B6642D8C99A6 (void);
// 0x00000985 System.Void NPOI.Util.ByteBuffer::.ctor(System.Byte[],System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void ByteBuffer__ctor_mFF95E807620201E6EA297F95FDF9C0D4711BC890 (void);
// 0x00000986 System.Int32 NPOI.Util.ByteBuffer::get_Position()
extern void ByteBuffer_get_Position_m1EF103055C90545A15AC5E1B17F4DD02522C2A34 (void);
// 0x00000987 System.Void NPOI.Util.ByteBuffer::set_Position(System.Int32)
extern void ByteBuffer_set_Position_mACC17578EE11C5682DAA9ADB37646ADFF9FAFD80 (void);
// 0x00000988 System.Void NPOI.Util.ByteBuffer::set_Limit(System.Int32)
extern void ByteBuffer_set_Limit_mB6F25797AF55368B3632742F58DCA574B7ACD683 (void);
// 0x00000989 NPOI.Util.ByteBuffer NPOI.Util.ByteBuffer::CreateBuffer(System.Int32)
extern void ByteBuffer_CreateBuffer_mC445B1033AA61F668EFE02AE004FBCDFE74AFE66 (void);
// 0x0000098A NPOI.Util.ByteBuffer NPOI.Util.ByteBuffer::Slice()
extern void ByteBuffer_Slice_m83FE191CEDDA230F38243250D5087A53296EECAE (void);
// 0x0000098B System.Int32 NPOI.Util.ByteBuffer::Index(System.Int32)
extern void ByteBuffer_Index_m6CCE3CFE8421DF1E3DAA3924AF0B58B8D8A6B96C (void);
// 0x0000098C System.Void NPOI.Util.ByteBuffer::CheckBounds(System.Int32,System.Int32,System.Int32)
extern void ByteBuffer_CheckBounds_mAF9F72DEAAD77B21C3A7189C00D4EAC49528C787 (void);
// 0x0000098D NPOI.Util.ByteBuffer NPOI.Util.ByteBuffer::Read(System.Byte[],System.Int32,System.Int32)
extern void ByteBuffer_Read_m355BDDB37B0F610F19AD02C240203C6EF1BE5E89 (void);
// 0x0000098E NPOI.Util.ByteBuffer NPOI.Util.ByteBuffer::Write(System.Byte[],System.Int32,System.Int32)
extern void ByteBuffer_Write_mE92A3C351DB42C1F755F65F2494AADCF3D0DB873 (void);
// 0x0000098F System.Int32 NPOI.Util.ByteBuffer::get_Remain()
extern void ByteBuffer_get_Remain_mF87693658AE333D9FD36FE66E7575BFE5B3E15C0 (void);
// 0x00000990 System.Void NPOI.POIFS.FileSystem.EntryNode::.ctor()
extern void EntryNode__ctor_mB598C29F5A1EDA1D29F00F82AEEA140C1858F5C2 (void);
// 0x00000991 System.Void NPOI.POIFS.FileSystem.EntryNode::.ctor(NPOI.POIFS.Properties.Property,NPOI.POIFS.FileSystem.DirectoryNode)
extern void EntryNode__ctor_m78A992885F76BD1BBFBC1B0813C07EFE97BA3F1B (void);
// 0x00000992 NPOI.POIFS.Properties.Property NPOI.POIFS.FileSystem.EntryNode::get_Property()
extern void EntryNode_get_Property_m24E0588F36F0CDA0DD10E00657CF2735D4F52131 (void);
// 0x00000993 System.String NPOI.POIFS.FileSystem.EntryNode::get_Name()
extern void EntryNode_get_Name_mD693C74C073ED389B0AC037F2647EB0B2E7F491C (void);
// 0x00000994 System.Boolean NPOI.POIFS.FileSystem.EntryNode::get_IsDirectoryEntry()
extern void EntryNode_get_IsDirectoryEntry_mEE8E4E6FDE66E9794462490D529972F0ED7C6AA7 (void);
// 0x00000995 System.Boolean NPOI.POIFS.FileSystem.EntryNode::get_IsDocumentEntry()
extern void EntryNode_get_IsDocumentEntry_m1A72880BBD6271C5D831A83B8D404336442D0447 (void);
// 0x00000996 NPOI.POIFS.FileSystem.DirectoryEntry NPOI.POIFS.FileSystem.EntryNode::get_Parent()
extern void EntryNode_get_Parent_m4673D9C1205FCE2409B3CF16A8C251C929697B75 (void);
// 0x00000997 System.String NPOI.POIFS.FileSystem.EntryNode::ToString()
extern void EntryNode_ToString_m1AA72E8347B43DF787805AC4743F3317D474D6D5 (void);
// 0x00000998 System.Void NPOI.POIFS.FileSystem.DirectoryNode::.ctor(NPOI.POIFS.Properties.DirectoryProperty,NPOI.POIFS.FileSystem.POIFSFileSystem,NPOI.POIFS.FileSystem.DirectoryNode)
extern void DirectoryNode__ctor_m708AF1375EFA72F1AF9052C8D7984A560BF852C9 (void);
// 0x00000999 System.Void NPOI.POIFS.FileSystem.DirectoryNode::.ctor(NPOI.POIFS.Properties.DirectoryProperty,NPOI.POIFS.FileSystem.NPOIFSFileSystem,NPOI.POIFS.FileSystem.DirectoryNode)
extern void DirectoryNode__ctor_m99A5195C48220AED28690B47426027CE0CE56763 (void);
// 0x0000099A System.Void NPOI.POIFS.FileSystem.DirectoryNode::.ctor(NPOI.POIFS.Properties.DirectoryProperty,NPOI.POIFS.FileSystem.DirectoryNode,NPOI.POIFS.FileSystem.POIFSFileSystem,NPOI.POIFS.FileSystem.NPOIFSFileSystem)
extern void DirectoryNode__ctor_mB282A68FE4985297C30D6872822EB0BCEE04201E (void);
// 0x0000099B NPOI.POIFS.FileSystem.DocumentInputStream NPOI.POIFS.FileSystem.DirectoryNode::CreatePOIFSDocumentReader(System.String)
extern void DirectoryNode_CreatePOIFSDocumentReader_m628FA405B8B8401DFE4FC2D3548AF59E361AF0D2 (void);
// 0x0000099C NPOI.POIFS.FileSystem.DocumentEntry NPOI.POIFS.FileSystem.DirectoryNode::CreateDocument(NPOI.POIFS.FileSystem.POIFSDocument)
extern void DirectoryNode_CreateDocument_m80178B350D03207926E04D34B5B6DA86055AEBD2 (void);
// 0x0000099D NPOI.POIFS.FileSystem.POIFSFileSystem NPOI.POIFS.FileSystem.DirectoryNode::get_FileSystem()
extern void DirectoryNode_get_FileSystem_m3388C0146EC2F5B9E148CCC74E0EE68C3BB0BD30 (void);
// 0x0000099E NPOI.POIFS.FileSystem.NPOIFSFileSystem NPOI.POIFS.FileSystem.DirectoryNode::get_NFileSystem()
extern void DirectoryNode_get_NFileSystem_mBCA0456C5716112121B5E8D64C6CC1F1ACD6A175 (void);
// 0x0000099F System.Collections.Generic.IEnumerator`1<NPOI.POIFS.FileSystem.Entry> NPOI.POIFS.FileSystem.DirectoryNode::get_Entries()
extern void DirectoryNode_get_Entries_m1A79A038D72684FF76A792548DF69581BC8432CD (void);
// 0x000009A0 System.Boolean NPOI.POIFS.FileSystem.DirectoryNode::HasEntry(System.String)
extern void DirectoryNode_HasEntry_m29F0CCABEFC53F8AD58EBB74433E4A5FBD5C3018 (void);
// 0x000009A1 NPOI.POIFS.FileSystem.Entry NPOI.POIFS.FileSystem.DirectoryNode::GetEntry(System.String)
extern void DirectoryNode_GetEntry_m2155284DF1F9132E61FF9F574C5EA90540119CA8 (void);
// 0x000009A2 NPOI.POIFS.FileSystem.DocumentInputStream NPOI.POIFS.FileSystem.DirectoryNode::CreateDocumentInputStream(NPOI.POIFS.FileSystem.Entry)
extern void DirectoryNode_CreateDocumentInputStream_mDF3358E640B4148AA36F6092D630098CA287979D (void);
// 0x000009A3 NPOI.POIFS.FileSystem.DocumentInputStream NPOI.POIFS.FileSystem.DirectoryNode::CreateDocumentInputStream(System.String)
extern void DirectoryNode_CreateDocumentInputStream_m53A086A8E9F9CB1B4778B8A7FC2481489BE20967 (void);
// 0x000009A4 NPOI.POIFS.FileSystem.DocumentEntry NPOI.POIFS.FileSystem.DirectoryNode::CreateDocument(NPOI.POIFS.FileSystem.NPOIFSDocument)
extern void DirectoryNode_CreateDocument_mEE5A09468FE1CE6945D4D9CB3440CF2019F8D0D6 (void);
// 0x000009A5 NPOI.POIFS.FileSystem.DirectoryEntry NPOI.POIFS.FileSystem.DirectoryNode::CreateDirectory(System.String)
extern void DirectoryNode_CreateDirectory_mC8A4F9EE26A7CBD29CD236B2D231F7A3E5121710 (void);
// 0x000009A6 System.Void NPOI.POIFS.FileSystem.DirectoryNode::set_StorageClsid(NPOI.Util.ClassID)
extern void DirectoryNode_set_StorageClsid_mFA873F379B4334B5F249D2D86770959D6D64F3DD (void);
// 0x000009A7 NPOI.Util.ClassID NPOI.POIFS.FileSystem.DirectoryNode::get_StorageClsid()
extern void DirectoryNode_get_StorageClsid_mC392CA7D38A44739DD16425D77C40A8F4CA78BC6 (void);
// 0x000009A8 System.Boolean NPOI.POIFS.FileSystem.DirectoryNode::get_IsDirectoryEntry()
extern void DirectoryNode_get_IsDirectoryEntry_m71B978B987B2EE1F7AB0081D406775FCA5E989E8 (void);
// 0x000009A9 NPOI.POIFS.FileSystem.DocumentEntry NPOI.POIFS.FileSystem.DirectoryNode::CreateDocument(System.String,System.IO.Stream)
extern void DirectoryNode_CreateDocument_m1CC16312081815C345C02233CCA5E10130359311 (void);
// 0x000009AA System.Collections.Generic.IEnumerator`1<NPOI.POIFS.FileSystem.Entry> NPOI.POIFS.FileSystem.DirectoryNode::GetEnumerator()
extern void DirectoryNode_GetEnumerator_mB115C755708D2AD9B526A019A694EBC7E58CB8F6 (void);
// 0x000009AB System.Collections.IEnumerator NPOI.POIFS.FileSystem.DirectoryNode::System.Collections.IEnumerable.GetEnumerator()
extern void DirectoryNode_System_Collections_IEnumerable_GetEnumerator_m9559207752BC635B24FA7938197729DA1E820936 (void);
// 0x000009AC System.Int32 NPOI.POIFS.FileSystem.DocumentEntry::get_Size()
// 0x000009AD System.Void NPOI.POIFS.FileSystem.DocumentNode::.ctor(NPOI.POIFS.Properties.DocumentProperty,NPOI.POIFS.FileSystem.DirectoryNode)
extern void DocumentNode__ctor_m877CF47AF5E3BD72DF778DA5476D17CBC7AD3C50 (void);
// 0x000009AE NPOI.POIFS.FileSystem.POIFSDocument NPOI.POIFS.FileSystem.DocumentNode::get_Document()
extern void DocumentNode_get_Document_m604883E1298373E3BDD0336F4812ED6D7DB97609 (void);
// 0x000009AF System.Int32 NPOI.POIFS.FileSystem.DocumentNode::get_Size()
extern void DocumentNode_get_Size_mF0748B1C79811DD4CDBF268F4A1BF1B08E6052E3 (void);
// 0x000009B0 System.Boolean NPOI.POIFS.FileSystem.DocumentNode::get_IsDocumentEntry()
extern void DocumentNode_get_IsDocumentEntry_m5C0C4FA783F5D741F2CB6ECD653C51DDA3F64ACE (void);
// 0x000009B1 System.Void NPOI.POIFS.FileSystem.OfficeXmlFileException::.ctor(System.String)
extern void OfficeXmlFileException__ctor_m6C5239A7DC157B13A826848BE9B7CACDC3DCE1DB (void);
// 0x000009B2 System.Void NPOI.POIFS.FileSystem.POIFSDocumentPath::.ctor()
extern void POIFSDocumentPath__ctor_mA91A0B2B413D21705B9BA014757A0E15FF02FD41 (void);
// 0x000009B3 System.Void NPOI.POIFS.FileSystem.POIFSDocumentPath::.ctor(NPOI.POIFS.FileSystem.POIFSDocumentPath,System.String[])
extern void POIFSDocumentPath__ctor_mDB73EBFFD154EA43A63FE8C3DCDB68F937D6BEF4 (void);
// 0x000009B4 System.Boolean NPOI.POIFS.FileSystem.POIFSDocumentPath::Equals(System.Object)
extern void POIFSDocumentPath_Equals_m8359FB95C97010EA408CFC578946EFB63AB862E0 (void);
// 0x000009B5 System.String NPOI.POIFS.FileSystem.POIFSDocumentPath::GetComponent(System.Int32)
extern void POIFSDocumentPath_GetComponent_m076D1638FEB568242B7A743983094CAAFA0C3754 (void);
// 0x000009B6 System.Int32 NPOI.POIFS.FileSystem.POIFSDocumentPath::GetHashCode()
extern void POIFSDocumentPath_GetHashCode_mF410DFDD36D4F6DD1C3362E622726C0AA42A83FE (void);
// 0x000009B7 System.String NPOI.POIFS.FileSystem.POIFSDocumentPath::ToString()
extern void POIFSDocumentPath_ToString_mE9BFAF25A547CA404211C8B89B4DB4A2E35C552C (void);
// 0x000009B8 System.Int32 NPOI.POIFS.FileSystem.POIFSDocumentPath::get_Length()
extern void POIFSDocumentPath_get_Length_m3D00C8E3C1DE6BC2DCDB613F251740EAF47A14F8 (void);
// 0x000009B9 System.Void NPOI.POIFS.FileSystem.POIFSFileSystem::.ctor()
extern void POIFSFileSystem__ctor_mFAE900696D6FDEC95EDE0613C77BE938D9FF9FDD (void);
// 0x000009BA System.Void NPOI.POIFS.FileSystem.POIFSFileSystem::.ctor(System.IO.Stream)
extern void POIFSFileSystem__ctor_m6EB4266E056251BA5C8511572EE21E8544A6144B (void);
// 0x000009BB System.Void NPOI.POIFS.FileSystem.POIFSFileSystem::CloseInputStream(System.IO.Stream,System.Boolean)
extern void POIFSFileSystem_CloseInputStream_mB587B555736CBE5BCBA3DAE4A41D82D841F498D9 (void);
// 0x000009BC NPOI.POIFS.FileSystem.DocumentEntry NPOI.POIFS.FileSystem.POIFSFileSystem::CreateDocument(System.IO.Stream,System.String)
extern void POIFSFileSystem_CreateDocument_mEAC0A0C414E6786573E78FBEF6FAA794A8F66F68 (void);
// 0x000009BD System.Void NPOI.POIFS.FileSystem.POIFSFileSystem::WriteFileSystem(System.IO.Stream)
extern void POIFSFileSystem_WriteFileSystem_mF36FFED01EAD68EE785C0021F059EFD00AC3A8DC (void);
// 0x000009BE NPOI.POIFS.FileSystem.DirectoryNode NPOI.POIFS.FileSystem.POIFSFileSystem::get_Root()
extern void POIFSFileSystem_get_Root_m783D1C8275C25B99CFA8304D9D44D2CA04FAE239 (void);
// 0x000009BF System.Void NPOI.POIFS.FileSystem.POIFSFileSystem::AddDocument(NPOI.POIFS.FileSystem.POIFSDocument)
extern void POIFSFileSystem_AddDocument_m5FFA495BFC15BAE665D977E0CF18C4883D20E482 (void);
// 0x000009C0 System.Void NPOI.POIFS.FileSystem.POIFSFileSystem::AddDirectory(NPOI.POIFS.Properties.DirectoryProperty)
extern void POIFSFileSystem_AddDirectory_m5781A31B23C54108D8D270D3BBC81BE8F98BB803 (void);
// 0x000009C1 System.Void NPOI.POIFS.FileSystem.POIFSFileSystem::ProcessProperties(NPOI.POIFS.Storage.BlockList,NPOI.POIFS.Storage.BlockList,System.Collections.IEnumerator,NPOI.POIFS.FileSystem.DirectoryNode,System.Int32)
extern void POIFSFileSystem_ProcessProperties_mE32D2E36601D8B441FDDD5801ED880EFB574C3D2 (void);
// 0x000009C2 System.Void NPOI.POIFS.Properties.Property::.ctor()
extern void Property__ctor_m8657E0559C7F21E3593201E52D5CE47D248DCEF0 (void);
// 0x000009C3 System.Void NPOI.POIFS.Properties.Property::.ctor(System.Int32,System.Byte[],System.Int32)
extern void Property__ctor_m7949074CC4CC4856FC980A27184CE6D661339A66 (void);
// 0x000009C4 System.Void NPOI.POIFS.Properties.Property::WriteData(System.IO.Stream)
extern void Property_WriteData_m29E292BD481367EDFFF8D678A620E15F0602D4E0 (void);
// 0x000009C5 System.Void NPOI.POIFS.Properties.Property::set_StartBlock(System.Int32)
extern void Property_set_StartBlock_m439EE7F1E97A2103F05B320CB9ED762C7B7431AA (void);
// 0x000009C6 System.Int32 NPOI.POIFS.Properties.Property::get_StartBlock()
extern void Property_get_StartBlock_mEC80A0657D39D9839B3E424179565D124502D804 (void);
// 0x000009C7 System.Boolean NPOI.POIFS.Properties.Property::get_ShouldUseSmallBlocks()
extern void Property_get_ShouldUseSmallBlocks_m2E4402BD019A3646D8D3A4C1F124A773A2FE09CE (void);
// 0x000009C8 System.Boolean NPOI.POIFS.Properties.Property::IsSmall(System.Int32)
extern void Property_IsSmall_mA4E30AEE4EE03474D7836BBADF763217F4F202A8 (void);
// 0x000009C9 System.String NPOI.POIFS.Properties.Property::get_Name()
extern void Property_get_Name_mD7F7E9D59F76B19BD6487DF8B81F5C1650030201 (void);
// 0x000009CA System.Void NPOI.POIFS.Properties.Property::set_Name(System.String)
extern void Property_set_Name_mF3B2FD44200B19B963D14AA774933E4F19F5AD36 (void);
// 0x000009CB System.Boolean NPOI.POIFS.Properties.Property::get_IsDirectory()
extern void Property_get_IsDirectory_m87AFD49524DB209F8C493B426D0C9110A1CD8F54 (void);
// 0x000009CC NPOI.Util.ClassID NPOI.POIFS.Properties.Property::get_StorageClsid()
extern void Property_get_StorageClsid_mEA65D8DD731820FEBCF050D6C2D5841D090A4FDE (void);
// 0x000009CD System.Void NPOI.POIFS.Properties.Property::set_StorageClsid(NPOI.Util.ClassID)
extern void Property_set_StorageClsid_m018FBECCF069744EF0FCBBD758C9BAD5371F4F98 (void);
// 0x000009CE System.Void NPOI.POIFS.Properties.Property::set_PropertyType(System.Byte)
extern void Property_set_PropertyType_m10B85E586220B70789FDA33EA3E5B90297A8D561 (void);
// 0x000009CF System.Void NPOI.POIFS.Properties.Property::set_NodeColor(System.Byte)
extern void Property_set_NodeColor_mC1462B906E2AB84B44BB56675FA6E4ECB2336B07 (void);
// 0x000009D0 System.Void NPOI.POIFS.Properties.Property::set_ChildProperty(System.Int32)
extern void Property_set_ChildProperty_m42E58929ABAFA47044FEDF40205E421F93FD93DF (void);
// 0x000009D1 System.Int32 NPOI.POIFS.Properties.Property::get_ChildIndex()
extern void Property_get_ChildIndex_mA005CA5EEE877D6852FFA50FB7DAFA5F55A86B0B (void);
// 0x000009D2 System.Void NPOI.POIFS.Properties.Property::set_Size(System.Int32)
extern void Property_set_Size_m4C3ADD05CB159D466D4DA61E6C11484C718A4D44 (void);
// 0x000009D3 System.Int32 NPOI.POIFS.Properties.Property::get_Size()
extern void Property_get_Size_mE9FC6CB91CBCB1CA0A51AA050B0AA9ACE1F56876 (void);
// 0x000009D4 System.Int32 NPOI.POIFS.Properties.Property::get_Index()
extern void Property_get_Index_m4405F35D7B97C436A806D8AC34045A2715FE9765 (void);
// 0x000009D5 System.Void NPOI.POIFS.Properties.Property::set_Index(System.Int32)
extern void Property_set_Index_mE6718D7F70E12FC3FE2438031DDE8D11C590539F (void);
// 0x000009D6 System.Void NPOI.POIFS.Properties.Property::PreWrite()
// 0x000009D7 System.Int32 NPOI.POIFS.Properties.Property::get_NextChildIndex()
extern void Property_get_NextChildIndex_m6FD4DA28644C11B5A66CE3917A464C3E1C61D22C (void);
// 0x000009D8 System.Int32 NPOI.POIFS.Properties.Property::get_PreviousChildIndex()
extern void Property_get_PreviousChildIndex_m31B535A952F1E2091CB227147FFBB20B775F52FF (void);
// 0x000009D9 System.Boolean NPOI.POIFS.Properties.Property::IsValidIndex(System.Int32)
extern void Property_IsValidIndex_m641E1DC5B3A9B4CEC1E376EFE097EBE2DBF4D622 (void);
// 0x000009DA System.Void NPOI.POIFS.Properties.Property::set_PreviousChild(NPOI.POIFS.Properties.Child)
extern void Property_set_PreviousChild_m8A6E951A89DB067F236C5521B62113828A63D576 (void);
// 0x000009DB System.Void NPOI.POIFS.Properties.Property::set_NextChild(NPOI.POIFS.Properties.Child)
extern void Property_set_NextChild_mF2480C4A679D01A3AF41D6A92AF7F2633B8D7119 (void);
// 0x000009DC System.Void NPOI.POIFS.Properties.DirectoryProperty::.ctor(System.String)
extern void DirectoryProperty__ctor_mD6F9C620C163D495AFD4AB1DC8E33F63165E9A85 (void);
// 0x000009DD System.Void NPOI.POIFS.Properties.DirectoryProperty::.ctor(System.Int32,System.Byte[],System.Int32)
extern void DirectoryProperty__ctor_m600A822FFBE910581FB0A35A5A47E3107C811E85 (void);
// 0x000009DE System.Boolean NPOI.POIFS.Properties.DirectoryProperty::get_IsDirectory()
extern void DirectoryProperty_get_IsDirectory_mA43F8D714491E4A6880E69372E63F040CD8F1CB5 (void);
// 0x000009DF System.Void NPOI.POIFS.Properties.DirectoryProperty::PreWrite()
extern void DirectoryProperty_PreWrite_mF2EEB20489102682E1EF8665D7B10C1A6F62BD7D (void);
// 0x000009E0 System.Collections.Generic.IEnumerator`1<NPOI.POIFS.Properties.Property> NPOI.POIFS.Properties.DirectoryProperty::get_Children()
extern void DirectoryProperty_get_Children_m23D4FD4FA50BB5AEA091F8A5D758293E0EFDA4C7 (void);
// 0x000009E1 System.Void NPOI.POIFS.Properties.DirectoryProperty::AddChild(NPOI.POIFS.Properties.Property)
extern void DirectoryProperty_AddChild_m3146C1D71EFEBBE5D50F1FD1750CEF3CD18181B9 (void);
// 0x000009E2 System.Boolean NPOI.POIFS.Properties.DirectoryProperty_PropertyComparator::Equals(System.Object)
extern void PropertyComparator_Equals_m17AFCEE9CDCCAE7B3DE3113245A911161E962947 (void);
// 0x000009E3 System.Int32 NPOI.POIFS.Properties.DirectoryProperty_PropertyComparator::GetHashCode()
extern void PropertyComparator_GetHashCode_mD1A8F9ED9AA5EF0F26DC617AD3CA0C92BD0A0784 (void);
// 0x000009E4 System.Int32 NPOI.POIFS.Properties.DirectoryProperty_PropertyComparator::Compare(NPOI.POIFS.Properties.Property,NPOI.POIFS.Properties.Property)
extern void PropertyComparator_Compare_mA86CE137D6823E5AAF676C12F81D2401C58A0174 (void);
// 0x000009E5 System.Void NPOI.POIFS.Properties.DirectoryProperty_PropertyComparator::.ctor()
extern void PropertyComparator__ctor_m148777A490A62E18A3913546C102BD050F6CB2BF (void);
// 0x000009E6 System.Void NPOI.POIFS.Properties.DocumentProperty::.ctor(System.String,System.Int32)
extern void DocumentProperty__ctor_m510F09B94E66D8BA02303910ADA21735D8B3308E (void);
// 0x000009E7 System.Void NPOI.POIFS.Properties.DocumentProperty::.ctor(System.Int32,System.Byte[],System.Int32)
extern void DocumentProperty__ctor_m3094C2516BE0C2846C9BE39DF44ED978A5555C66 (void);
// 0x000009E8 System.Void NPOI.POIFS.Properties.DocumentProperty::set_Document(NPOI.POIFS.FileSystem.POIFSDocument)
extern void DocumentProperty_set_Document_m762713A71F0601527B74669B8A03FCB23E04032C (void);
// 0x000009E9 NPOI.POIFS.FileSystem.POIFSDocument NPOI.POIFS.Properties.DocumentProperty::get_Document()
extern void DocumentProperty_get_Document_mFD4B7BB75D5786A69B0F4B7910709E9FA10643CC (void);
// 0x000009EA System.Boolean NPOI.POIFS.Properties.DocumentProperty::get_IsDirectory()
extern void DocumentProperty_get_IsDirectory_mD5E70A2A1A595DC4B73B21CE0A9B93D724411A75 (void);
// 0x000009EB System.Void NPOI.POIFS.Properties.DocumentProperty::PreWrite()
extern void DocumentProperty_PreWrite_mD2A2413E4228011C42B0B5456047CA1C62CABEDE (void);
// 0x000009EC System.Collections.Generic.List`1<NPOI.POIFS.Properties.Property> NPOI.POIFS.Properties.PropertyFactory::ConvertToProperties(NPOI.POIFS.Storage.ListManagedBlock[])
extern void PropertyFactory_ConvertToProperties_m9602E49750159F73D8ECE2736C87F7CC2AC1851B (void);
// 0x000009ED System.Void NPOI.POIFS.Properties.PropertyFactory::ConvertToProperties(System.Byte[],System.Collections.Generic.List`1<NPOI.POIFS.Properties.Property>)
extern void PropertyFactory_ConvertToProperties_mC362ADC25BE4DCB54F923719E76644C42ABDD15E (void);
// 0x000009EE System.Void NPOI.POIFS.Properties.PropertyTable::.ctor(NPOI.POIFS.Storage.HeaderBlock)
extern void PropertyTable__ctor_m9C4D70DCFF7A923591A128ADD136B5BD8D60DF1E (void);
// 0x000009EF System.Void NPOI.POIFS.Properties.PropertyTable::.ctor(NPOI.POIFS.Storage.HeaderBlock,NPOI.POIFS.Storage.RawDataBlockList)
extern void PropertyTable__ctor_m293E09537AC1DC36E5385E2F32651BAF90E50D23 (void);
// 0x000009F0 System.Void NPOI.POIFS.Properties.PropertyTable::PreWrite()
extern void PropertyTable_PreWrite_mE48067B42EF23FEC3E29CE155F40FFD6E751183D (void);
// 0x000009F1 System.Int32 NPOI.POIFS.Properties.PropertyTable::get_CountBlocks()
extern void PropertyTable_get_CountBlocks_m5768A5D3D174F04DE111FCAE5110F0B8A8074DDC (void);
// 0x000009F2 System.Void NPOI.POIFS.Properties.PropertyTable::WriteBlocks(System.IO.Stream)
extern void PropertyTable_WriteBlocks_mC76A5A710C178A358D7E5C09FC04D15EED8D7FFC (void);
// 0x000009F3 System.Void NPOI.POIFS.Properties.RootProperty::.ctor()
extern void RootProperty__ctor_m2FB5A68B2CE7C466317C900D08FEBFC3B1AAE5A2 (void);
// 0x000009F4 System.Void NPOI.POIFS.Properties.RootProperty::.ctor(System.Int32,System.Byte[],System.Int32)
extern void RootProperty__ctor_m3502117E479CF549431CC14C18B1BF2686021F16 (void);
// 0x000009F5 System.Void NPOI.POIFS.Properties.RootProperty::set_Size(System.Int32)
extern void RootProperty_set_Size_m561DA208E7010E59B354F9BEFC64DB5363AD9485 (void);
// 0x000009F6 System.Void NPOI.POIFS.Storage.BigBlock::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void BigBlock__ctor_mC61987309864F917FCB4537A01722F365DA10404 (void);
// 0x000009F7 System.Void NPOI.POIFS.Storage.BigBlock::WriteData(System.IO.Stream,System.Byte[])
extern void BigBlock_WriteData_m5FEC7B01B448077465295A51541A9A0CDF43C435 (void);
// 0x000009F8 System.Void NPOI.POIFS.Storage.BigBlock::WriteBlocks(System.IO.Stream)
extern void BigBlock_WriteBlocks_mE689E5C36B06C81B537B7AA305F5F55DE0BB30FA (void);
// 0x000009F9 System.Void NPOI.POIFS.Storage.BigBlock::WriteData(System.IO.Stream)
// 0x000009FA System.Void NPOI.POIFS.Storage.BATBlock::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void BATBlock__ctor_mE4E2D5A81FED648232CC661FE9F8A85D2AE879CD (void);
// 0x000009FB System.Void NPOI.POIFS.Storage.BATBlock::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize,System.Int32[],System.Int32,System.Int32)
extern void BATBlock__ctor_mAF27AA8CD2600D7CB90CB29F237C8B1767FF7F45 (void);
// 0x000009FC System.Void NPOI.POIFS.Storage.BATBlock::RecomputeFree()
extern void BATBlock_RecomputeFree_m41CBB7F013C62E6C931FA895476FFA9B8F5238BE (void);
// 0x000009FD NPOI.POIFS.Storage.BATBlock NPOI.POIFS.Storage.BATBlock::CreateEmptyBATBlock(NPOI.POIFS.Common.POIFSBigBlockSize,System.Boolean)
extern void BATBlock_CreateEmptyBATBlock_m72BC5BC5CEBD9818C68D6E0DCB2DB3B29CC3738C (void);
// 0x000009FE NPOI.POIFS.Storage.BATBlock[] NPOI.POIFS.Storage.BATBlock::CreateBATBlocks(NPOI.POIFS.Common.POIFSBigBlockSize,System.Int32[])
extern void BATBlock_CreateBATBlocks_mC7049A5D268325A533FF9BA634693F5A98178C9F (void);
// 0x000009FF NPOI.POIFS.Storage.BATBlock[] NPOI.POIFS.Storage.BATBlock::CreateXBATBlocks(NPOI.POIFS.Common.POIFSBigBlockSize,System.Int32[],System.Int32)
extern void BATBlock_CreateXBATBlocks_m0B64A46AB634491A8FF9AEFA5FF0D38C7B95374C (void);
// 0x00000A00 System.Int32 NPOI.POIFS.Storage.BATBlock::CalculateStorageRequirements(System.Int32)
extern void BATBlock_CalculateStorageRequirements_m174942B14CAAE12EBB339E2EEAE91A5B4A54A3C0 (void);
// 0x00000A01 System.Int32 NPOI.POIFS.Storage.BATBlock::CalculateStorageRequirements(NPOI.POIFS.Common.POIFSBigBlockSize,System.Int32)
extern void BATBlock_CalculateStorageRequirements_m7BD10E5B3B042EE4BB27C67158D911AFF3C54CA9 (void);
// 0x00000A02 System.Int32 NPOI.POIFS.Storage.BATBlock::CalculateXBATStorageRequirements(System.Int32)
extern void BATBlock_CalculateXBATStorageRequirements_mD449F07A493FFBF422F89151AB8CB866E643BB5D (void);
// 0x00000A03 System.Int32 NPOI.POIFS.Storage.BATBlock::CalculateXBATStorageRequirements(NPOI.POIFS.Common.POIFSBigBlockSize,System.Int32)
extern void BATBlock_CalculateXBATStorageRequirements_m262E6E39F21C318842CA765FC39B7354ABC3AE0A (void);
// 0x00000A04 NPOI.POIFS.Storage.BATBlockAndIndex NPOI.POIFS.Storage.BATBlock::GetBATBlockAndIndex(System.Int32,NPOI.POIFS.Storage.HeaderBlock,System.Collections.Generic.List`1<NPOI.POIFS.Storage.BATBlock>)
extern void BATBlock_GetBATBlockAndIndex_mF8C64079874CCAEAD465EBC1187D847E1F39FA14 (void);
// 0x00000A05 NPOI.POIFS.Storage.BATBlockAndIndex NPOI.POIFS.Storage.BATBlock::GetSBATBlockAndIndex(System.Int32,NPOI.POIFS.Storage.HeaderBlock,System.Collections.Generic.List`1<NPOI.POIFS.Storage.BATBlock>)
extern void BATBlock_GetSBATBlockAndIndex_mC378B63F1FD8BA42F0926B9B1F2441870B0E795A (void);
// 0x00000A06 System.Int32 NPOI.POIFS.Storage.BATBlock::get_EntriesPerXBATBlock()
extern void BATBlock_get_EntriesPerXBATBlock_m4C88DAA546A7473F1108024C7CBC6B42D6C588A2 (void);
// 0x00000A07 System.Int32 NPOI.POIFS.Storage.BATBlock::get_XBATChainOffset()
extern void BATBlock_get_XBATChainOffset_mA827A9C48F83346EBABD9C5E85589247D100A7BC (void);
// 0x00000A08 System.Void NPOI.POIFS.Storage.BATBlock::SetXBATChain(NPOI.POIFS.Common.POIFSBigBlockSize,System.Int32)
extern void BATBlock_SetXBATChain_mBD50E79225A280C5B696AF2E67FB46C06F4B8F23 (void);
// 0x00000A09 System.Boolean NPOI.POIFS.Storage.BATBlock::get_HasFreeSectors()
extern void BATBlock_get_HasFreeSectors_mA5301DC3967DF26330836F03D8AF52223E378D9F (void);
// 0x00000A0A System.Int32 NPOI.POIFS.Storage.BATBlock::GetValueAt(System.Int32)
extern void BATBlock_GetValueAt_m0BE433CF60C17A5553FB0DF0473D740C2AD825ED (void);
// 0x00000A0B System.Void NPOI.POIFS.Storage.BATBlock::SetValueAt(System.Int32,System.Int32)
extern void BATBlock_SetValueAt_m92AE0B057F56B14654D13DF7BDA040EA87015A41 (void);
// 0x00000A0C System.Void NPOI.POIFS.Storage.BATBlock::set_OurBlockIndex(System.Int32)
extern void BATBlock_set_OurBlockIndex_m0DFD350F4507D31923591A5DC6A7B01E9E07368F (void);
// 0x00000A0D System.Void NPOI.POIFS.Storage.BATBlock::WriteData(System.IO.Stream)
extern void BATBlock_WriteData_mBF18825801725760B856609A2C1BC59FC757AECA (void);
// 0x00000A0E System.Byte[] NPOI.POIFS.Storage.BATBlock::Serialize()
extern void BATBlock_Serialize_m439CFCE7D43D411F5E7BE5F2E679CCA4DF5BF47F (void);
// 0x00000A0F System.Void NPOI.POIFS.Storage.BATBlock::.cctor()
extern void BATBlock__cctor_mDCF64B219356C06CDE791FABD593649395DC0991 (void);
// 0x00000A10 System.Void NPOI.POIFS.Storage.BATBlockAndIndex::.ctor(System.Int32,NPOI.POIFS.Storage.BATBlock)
extern void BATBlockAndIndex__ctor_mF371ADBB61FD841A39CFF01B8387AAA6A5FE8533 (void);
// 0x00000A11 System.Int32 NPOI.POIFS.Storage.BATBlockAndIndex::get_Index()
extern void BATBlockAndIndex_get_Index_m140BEB2245C36B36029EB33A93813B4580EE20FE (void);
// 0x00000A12 NPOI.POIFS.Storage.BATBlock NPOI.POIFS.Storage.BATBlockAndIndex::get_Block()
extern void BATBlockAndIndex_get_Block_mF94565940AC9329BF8AA94948CEC98BF393340FF (void);
// 0x00000A13 System.Void NPOI.POIFS.Storage.BlockAllocationTableReader::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize,System.Int32,System.Int32[],System.Int32,System.Int32,NPOI.POIFS.Storage.BlockList)
extern void BlockAllocationTableReader__ctor_mAFB85118F9ED0DE905D8A2847D315F1B6C352B7C (void);
// 0x00000A14 System.Void NPOI.POIFS.Storage.BlockAllocationTableReader::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize,NPOI.POIFS.Storage.ListManagedBlock[],NPOI.POIFS.Storage.BlockList)
extern void BlockAllocationTableReader__ctor_mF2B56AF5F54D72C16C62C3F8C6C2A7461B6BDA9E (void);
// 0x00000A15 System.Void NPOI.POIFS.Storage.BlockAllocationTableReader::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void BlockAllocationTableReader__ctor_m12EE3E7E87FE61F35DAB1C1C96118DC11ACBE7A7 (void);
// 0x00000A16 NPOI.POIFS.Storage.ListManagedBlock[] NPOI.POIFS.Storage.BlockAllocationTableReader::FetchBlocks(System.Int32,System.Int32,NPOI.POIFS.Storage.BlockList)
extern void BlockAllocationTableReader_FetchBlocks_mB7A5F1D05B7364E8748B646333A255D0429AFD03 (void);
// 0x00000A17 System.Void NPOI.POIFS.Storage.BlockAllocationTableReader::SetEntries(NPOI.POIFS.Storage.ListManagedBlock[],NPOI.POIFS.Storage.BlockList)
extern void BlockAllocationTableReader_SetEntries_m45D3EB6F2AE9C10B7F0B6C2DECD24E750B35BDE4 (void);
// 0x00000A18 System.Void NPOI.POIFS.Storage.BlockAllocationTableReader::SanityCheckBlockCount(System.Int32)
extern void BlockAllocationTableReader_SanityCheckBlockCount_m6DE2743A0606FFD3DD87E1AB579A4BF742BBFD19 (void);
// 0x00000A19 System.Void NPOI.POIFS.Storage.BlockAllocationTableReader::.cctor()
extern void BlockAllocationTableReader__cctor_m78A89416FC60DBAC24E99E4447988A5887DC09CF (void);
// 0x00000A1A System.Void NPOI.POIFS.Storage.BlockAllocationTableWriter::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void BlockAllocationTableWriter__ctor_mCCBA267CBBF9369DBD1B1A76921AD6DF46EDAF68 (void);
// 0x00000A1B System.Int32 NPOI.POIFS.Storage.BlockAllocationTableWriter::CreateBlocks()
extern void BlockAllocationTableWriter_CreateBlocks_mBC2E4208F82C51FA9648E185FA4619D2310B0BF5 (void);
// 0x00000A1C System.Int32 NPOI.POIFS.Storage.BlockAllocationTableWriter::AllocateSpace(System.Int32)
extern void BlockAllocationTableWriter_AllocateSpace_mFC4F53BC1C4A7C84C79C61D6D9E79DA7CB03EAD8 (void);
// 0x00000A1D System.Int32 NPOI.POIFS.Storage.BlockAllocationTableWriter::get_StartBlock()
extern void BlockAllocationTableWriter_get_StartBlock_m6014885C781BD4EBE912EAC5A5AA5CD6B8710F76 (void);
// 0x00000A1E System.Void NPOI.POIFS.Storage.BlockAllocationTableWriter::set_StartBlock(System.Int32)
extern void BlockAllocationTableWriter_set_StartBlock_mB894749FB0F18356160A2B68CF189803FADD5E39 (void);
// 0x00000A1F System.Void NPOI.POIFS.Storage.BlockAllocationTableWriter::SimpleCreateBlocks()
extern void BlockAllocationTableWriter_SimpleCreateBlocks_m2DB4A302398C3DC274FAB1AF562F989CEFD42DC0 (void);
// 0x00000A20 System.Void NPOI.POIFS.Storage.BlockAllocationTableWriter::WriteBlocks(System.IO.Stream)
extern void BlockAllocationTableWriter_WriteBlocks_m2DAB38FA9825B2591828FFA48DE1F6D6C4388FB7 (void);
// 0x00000A21 System.Int32 NPOI.POIFS.Storage.BlockAllocationTableWriter::get_CountBlocks()
extern void BlockAllocationTableWriter_get_CountBlocks_m2BDE57BA942C5531B1177DC524A9F6C1295A4EA3 (void);
// 0x00000A22 System.Void NPOI.POIFS.Storage.BlockAllocationTableWriter::.cctor()
extern void BlockAllocationTableWriter__cctor_m12F55A699956B62C3372D9B187603A7037ECEE74 (void);
// 0x00000A23 System.Void NPOI.POIFS.Storage.BlockList::Zap(System.Int32)
// 0x00000A24 NPOI.POIFS.Storage.ListManagedBlock NPOI.POIFS.Storage.BlockList::Remove(System.Int32)
// 0x00000A25 NPOI.POIFS.Storage.ListManagedBlock[] NPOI.POIFS.Storage.BlockList::FetchBlocks(System.Int32,System.Int32)
// 0x00000A26 System.Void NPOI.POIFS.Storage.BlockList::set_BAT(NPOI.POIFS.Storage.BlockAllocationTableReader)
// 0x00000A27 System.Int32 NPOI.POIFS.Storage.BlockList::BlockCount()
// 0x00000A28 System.Void NPOI.POIFS.Storage.BlockListImpl::.ctor()
extern void BlockListImpl__ctor_m1F827653BE601F11961575C85ED2B8442F54026A (void);
// 0x00000A29 System.Void NPOI.POIFS.Storage.BlockListImpl::SetBlocks(NPOI.POIFS.Storage.ListManagedBlock[])
extern void BlockListImpl_SetBlocks_mBFE1793728373C14C1E97821FD19C0EB55283547 (void);
// 0x00000A2A System.Void NPOI.POIFS.Storage.BlockListImpl::Zap(System.Int32)
extern void BlockListImpl_Zap_m4EC485BCDC34D76F850AD86AC711893E0DB41898 (void);
// 0x00000A2B NPOI.POIFS.Storage.ListManagedBlock NPOI.POIFS.Storage.BlockListImpl::Remove(System.Int32)
extern void BlockListImpl_Remove_mB0F6F17F2B7F4FFCD8C61392486061DE340389EA (void);
// 0x00000A2C NPOI.POIFS.Storage.ListManagedBlock[] NPOI.POIFS.Storage.BlockListImpl::FetchBlocks(System.Int32,System.Int32)
extern void BlockListImpl_FetchBlocks_mD7E26290391BFC4823E688C6A7BC183021F35AA5 (void);
// 0x00000A2D System.Void NPOI.POIFS.Storage.BlockListImpl::set_BAT(NPOI.POIFS.Storage.BlockAllocationTableReader)
extern void BlockListImpl_set_BAT_mBA1470DD23418F76DFA811C91B584BBC4C24A085 (void);
// 0x00000A2E System.Int32 NPOI.POIFS.Storage.BlockListImpl::BlockCount()
extern void BlockListImpl_BlockCount_m83969BAB5416B5AB89D9D3895461E2BBA521A5C2 (void);
// 0x00000A2F System.Void NPOI.POIFS.Storage.DataInputBlock::.ctor(System.Byte[],System.Int32)
extern void DataInputBlock__ctor_mA95EFA66395EC6944D19962516B0530982F2E270 (void);
// 0x00000A30 System.Int32 NPOI.POIFS.Storage.DataInputBlock::Available()
extern void DataInputBlock_Available_m623AD20DD1F71F65347A6071CE451806FB23A091 (void);
// 0x00000A31 System.Int32 NPOI.POIFS.Storage.DataInputBlock::ReadUByte()
extern void DataInputBlock_ReadUByte_mA63395E6EA83CA6F94DFC3C9CC4971993CE24BFA (void);
// 0x00000A32 System.Int32 NPOI.POIFS.Storage.DataInputBlock::ReadUshortLE()
extern void DataInputBlock_ReadUshortLE_m57CEBDC426DF93415323440E2DFFD8B6428F1DE2 (void);
// 0x00000A33 System.Int32 NPOI.POIFS.Storage.DataInputBlock::ReadUshortLE(NPOI.POIFS.Storage.DataInputBlock)
extern void DataInputBlock_ReadUshortLE_mA239ED7CF0FD1E901EF676836E78B05C647F5950 (void);
// 0x00000A34 System.Int32 NPOI.POIFS.Storage.DataInputBlock::ReadIntLE()
extern void DataInputBlock_ReadIntLE_mF3479462BAD83F3F8F5845024350D02C43126926 (void);
// 0x00000A35 System.Int32 NPOI.POIFS.Storage.DataInputBlock::ReadIntLE(NPOI.POIFS.Storage.DataInputBlock,System.Int32)
extern void DataInputBlock_ReadIntLE_mD75614664280F337060E80B446C819C125A8B56B (void);
// 0x00000A36 System.Int64 NPOI.POIFS.Storage.DataInputBlock::ReadLongLE()
extern void DataInputBlock_ReadLongLE_mD33843F1AA32FDFE52D361255781A74AF81A4DA0 (void);
// 0x00000A37 System.Int64 NPOI.POIFS.Storage.DataInputBlock::ReadLongLE(NPOI.POIFS.Storage.DataInputBlock,System.Int32)
extern void DataInputBlock_ReadLongLE_m7EAD4AE33FFD1A8C153BBA880DDBE500CDE03ECA (void);
// 0x00000A38 System.Void NPOI.POIFS.Storage.DataInputBlock::ReadSpanning(NPOI.POIFS.Storage.DataInputBlock,System.Int32,System.Byte[])
extern void DataInputBlock_ReadSpanning_mEEB2C7EDCC769E6C0E6B17CEF93AA662669EB9A8 (void);
// 0x00000A39 System.Void NPOI.POIFS.Storage.DataInputBlock::ReadFully(System.Byte[],System.Int32,System.Int32)
extern void DataInputBlock_ReadFully_mFC1719084A7B38DBF1F19F9963E23F9094435738 (void);
// 0x00000A3A System.Void NPOI.POIFS.Storage.DocumentBlock::.ctor(NPOI.POIFS.Storage.RawDataBlock)
extern void DocumentBlock__ctor_m9DB80B52406A4C78EB980D546A6FB72CD93AB896 (void);
// 0x00000A3B System.Void NPOI.POIFS.Storage.DocumentBlock::.ctor(System.IO.Stream,NPOI.POIFS.Common.POIFSBigBlockSize)
extern void DocumentBlock__ctor_mB9CFCF6CF2A84740E01CF02FD242265993BC52A8 (void);
// 0x00000A3C System.Void NPOI.POIFS.Storage.DocumentBlock::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void DocumentBlock__ctor_m17EDA2B0243376CDDF2E180462B845728E62DBFB (void);
// 0x00000A3D System.Int32 NPOI.POIFS.Storage.DocumentBlock::get_Size()
extern void DocumentBlock_get_Size_mAC3A9B4E3FB59D928130248D0B9712C5C956A1B6 (void);
// 0x00000A3E System.Boolean NPOI.POIFS.Storage.DocumentBlock::get_PartiallyRead()
extern void DocumentBlock_get_PartiallyRead_m253FBBF8C0F49EA1F8BDA6EE670515DC6345AC55 (void);
// 0x00000A3F System.Byte NPOI.POIFS.Storage.DocumentBlock::get_FillByte()
extern void DocumentBlock_get_FillByte_m707F7D8DAF239B79D61A420B3686680849AB6886 (void);
// 0x00000A40 NPOI.POIFS.Storage.DocumentBlock[] NPOI.POIFS.Storage.DocumentBlock::Convert(NPOI.POIFS.Common.POIFSBigBlockSize,System.Byte[],System.Int32)
extern void DocumentBlock_Convert_m1408F64A80E1EBDFF924693540F6D897D74A5239 (void);
// 0x00000A41 NPOI.POIFS.Storage.DataInputBlock NPOI.POIFS.Storage.DocumentBlock::GetDataInputBlock(NPOI.POIFS.Storage.DocumentBlock[],System.Int32)
extern void DocumentBlock_GetDataInputBlock_m615D7824EC88FCFA910D69EDBC3ACF4A669DC1CB (void);
// 0x00000A42 System.Void NPOI.POIFS.Storage.DocumentBlock::WriteData(System.IO.Stream)
extern void DocumentBlock_WriteData_m2202C07E8307343BB51E9273F32B79AADAF61FEE (void);
// 0x00000A43 System.Void NPOI.POIFS.Storage.DocumentBlock::.cctor()
extern void DocumentBlock__cctor_m0556049E6D453ED420B68E65CA9CAC1CE73568E5 (void);
// 0x00000A44 System.Void NPOI.POIFS.Storage.HeaderBlockWriter::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void HeaderBlockWriter__ctor_m29DD3AC8B1C2B51F01AB3EC643FFA8300F1EE4CB (void);
// 0x00000A45 NPOI.POIFS.Storage.BATBlock[] NPOI.POIFS.Storage.HeaderBlockWriter::SetBATBlocks(System.Int32,System.Int32)
extern void HeaderBlockWriter_SetBATBlocks_m7392DEE99595A181EC7474F40C3CC73D7DB0760C (void);
// 0x00000A46 System.Void NPOI.POIFS.Storage.HeaderBlockWriter::set_PropertyStart(System.Int32)
extern void HeaderBlockWriter_set_PropertyStart_m7924D828508D032020857AEB8FFB886BE9F04BEB (void);
// 0x00000A47 System.Void NPOI.POIFS.Storage.HeaderBlockWriter::set_SBATStart(System.Int32)
extern void HeaderBlockWriter_set_SBATStart_m6380204BF177CEB731E7E372F960A59B6184D957 (void);
// 0x00000A48 System.Void NPOI.POIFS.Storage.HeaderBlockWriter::set_SBATBlockCount(System.Int32)
extern void HeaderBlockWriter_set_SBATBlockCount_mAB04D8D8FAF965FEC060C84C7D9F6DA0517C8425 (void);
// 0x00000A49 System.Int32 NPOI.POIFS.Storage.HeaderBlockWriter::CalculateXBATStorageRequirements(NPOI.POIFS.Common.POIFSBigBlockSize,System.Int32)
extern void HeaderBlockWriter_CalculateXBATStorageRequirements_m07B82A5A91C98070DD0E09D4E09FDC5666A26C09 (void);
// 0x00000A4A System.Void NPOI.POIFS.Storage.HeaderBlockWriter::WriteBlocks(System.IO.Stream)
extern void HeaderBlockWriter_WriteBlocks_mBF0CFABB27C3E33B96ED692129EB2C2CB445A2AC (void);
// 0x00000A4B System.Byte[] NPOI.POIFS.Storage.ListManagedBlock::get_Data()
// 0x00000A4C System.Void NPOI.POIFS.Storage.PropertyBlock::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize,NPOI.POIFS.Properties.Property[],System.Int32)
extern void PropertyBlock__ctor_mA4D6B6402423E3BC8ADEECCC04E3149E0743F3BD (void);
// 0x00000A4D NPOI.POIFS.Storage.BlockWritable[] NPOI.POIFS.Storage.PropertyBlock::CreatePropertyBlockArray(NPOI.POIFS.Common.POIFSBigBlockSize,System.Collections.Generic.List`1<NPOI.POIFS.Properties.Property>)
extern void PropertyBlock_CreatePropertyBlockArray_m1182BEC209F6C0AA9BD8D6F60182792909FA03D1 (void);
// 0x00000A4E System.Void NPOI.POIFS.Storage.PropertyBlock::WriteData(System.IO.Stream)
extern void PropertyBlock_WriteData_m7261E865647A9A29B2228023BB7EE9B1EBD6B913 (void);
// 0x00000A4F System.Void NPOI.POIFS.Storage.PropertyBlock_AnonymousProperty::PreWrite()
extern void AnonymousProperty_PreWrite_mDA816F4D5A0DECA615758C206571FBB7701411F6 (void);
// 0x00000A50 System.Boolean NPOI.POIFS.Storage.PropertyBlock_AnonymousProperty::get_IsDirectory()
extern void AnonymousProperty_get_IsDirectory_m661BC0713F0ECDA001AF63C462327176D4D9E8C8 (void);
// 0x00000A51 System.Void NPOI.POIFS.Storage.PropertyBlock_AnonymousProperty::.ctor()
extern void AnonymousProperty__ctor_mF8A8D1FF59287D8742B82416208AD82F8F29C53A (void);
// 0x00000A52 System.Void NPOI.POIFS.Storage.RawDataBlock::.ctor(System.IO.Stream,System.Int32)
extern void RawDataBlock__ctor_m0F0E9D97998902B2686EC6E757404B4A4A446A4C (void);
// 0x00000A53 System.Boolean NPOI.POIFS.Storage.RawDataBlock::get_EOF()
extern void RawDataBlock_get_EOF_mCF1D79AEFDE08FFA3F4CE85F1200FB0A0A0CB7A5 (void);
// 0x00000A54 System.Boolean NPOI.POIFS.Storage.RawDataBlock::get_HasData()
extern void RawDataBlock_get_HasData_m875CA9B748A33473FC3037A992B30BCFF15EAC66 (void);
// 0x00000A55 System.Byte[] NPOI.POIFS.Storage.RawDataBlock::get_Data()
extern void RawDataBlock_get_Data_m78F0DC99E0BF7D7F1026E0BC57760C2109C40D5A (void);
// 0x00000A56 System.String NPOI.POIFS.Storage.RawDataBlock::ToString()
extern void RawDataBlock_ToString_m26E6F1579DE4E085A0D6831546A9ADBBF75A82B0 (void);
// 0x00000A57 System.Int32 NPOI.POIFS.Storage.RawDataBlock::get_BigBlockSize()
extern void RawDataBlock_get_BigBlockSize_mC6C11C7A2504D5A52E06B3FB16295410F551A8F1 (void);
// 0x00000A58 System.Void NPOI.POIFS.Storage.RawDataBlock::.cctor()
extern void RawDataBlock__cctor_m2FED7FA16EF65FDE700287BE2001FDC3CD8430E6 (void);
// 0x00000A59 System.Void NPOI.POIFS.Storage.RawDataBlockList::.ctor(System.IO.Stream,NPOI.POIFS.Common.POIFSBigBlockSize)
extern void RawDataBlockList__ctor_m47E988D6D9F5C253161D95D2B152E2905AA2FE4D (void);
// 0x00000A5A NPOI.POIFS.Storage.BlockList NPOI.POIFS.Storage.SmallBlockTableReader::GetSmallDocumentBlocks(NPOI.POIFS.Common.POIFSBigBlockSize,NPOI.POIFS.Storage.RawDataBlockList,NPOI.POIFS.Properties.RootProperty,System.Int32)
extern void SmallBlockTableReader_GetSmallDocumentBlocks_m74431943DAB02B90C5315C72B1D559698C6CC37B (void);
// 0x00000A5B System.Void NPOI.POIFS.Storage.SmallBlockTableWriter::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize,System.Collections.IList,NPOI.POIFS.Properties.RootProperty)
extern void SmallBlockTableWriter__ctor_m6AC547EA5A6D264A9FDFE6DD5AA898D3EA2D3EC9 (void);
// 0x00000A5C System.Int32 NPOI.POIFS.Storage.SmallBlockTableWriter::get_SBATBlockCount()
extern void SmallBlockTableWriter_get_SBATBlockCount_mACA3F1A8B638A0161371132464BB6C6DC81C31DE (void);
// 0x00000A5D NPOI.POIFS.Storage.BlockAllocationTableWriter NPOI.POIFS.Storage.SmallBlockTableWriter::get_SBAT()
extern void SmallBlockTableWriter_get_SBAT_mB942CA5F42D5BB334C088F5B39E28C00B58DCE39 (void);
// 0x00000A5E System.Int32 NPOI.POIFS.Storage.SmallBlockTableWriter::get_CountBlocks()
extern void SmallBlockTableWriter_get_CountBlocks_m9A4F0A96BA704B81CBB09B376DC89FE0C7CB73E4 (void);
// 0x00000A5F System.Void NPOI.POIFS.Storage.SmallBlockTableWriter::set_StartBlock(System.Int32)
extern void SmallBlockTableWriter_set_StartBlock_m3C0D9A2A98BAC6498CFACC11742755E821BB170A (void);
// 0x00000A60 System.Void NPOI.POIFS.Storage.SmallBlockTableWriter::WriteBlocks(System.IO.Stream)
extern void SmallBlockTableWriter_WriteBlocks_m4D9CC52DB516FF65210A261C2D19452B587CF100 (void);
// 0x00000A61 System.Void NPOI.POIFS.Storage.SmallDocumentBlock::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize,System.Byte[],System.Int32)
extern void SmallDocumentBlock__ctor_m624D950E81E06176D0E3F00E30BE7E678F0AB809 (void);
// 0x00000A62 System.Void NPOI.POIFS.Storage.SmallDocumentBlock::.ctor(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void SmallDocumentBlock__ctor_m87526FD90B327BA9508FD518CB88548F9D96EA4E (void);
// 0x00000A63 System.Int32 NPOI.POIFS.Storage.SmallDocumentBlock::GetBlocksPerBigBlock(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void SmallDocumentBlock_GetBlocksPerBigBlock_mA96D61377C666559E531A628A2AAC8DB83EB4886 (void);
// 0x00000A64 NPOI.POIFS.Storage.SmallDocumentBlock[] NPOI.POIFS.Storage.SmallDocumentBlock::Convert(NPOI.POIFS.Common.POIFSBigBlockSize,System.Byte[],System.Int32)
extern void SmallDocumentBlock_Convert_mD06C5384958860DE5A321E47EC894C38FEBF03EF (void);
// 0x00000A65 System.Int32 NPOI.POIFS.Storage.SmallDocumentBlock::Fill(NPOI.POIFS.Common.POIFSBigBlockSize,System.Collections.IList)
extern void SmallDocumentBlock_Fill_m6334BA3212ED704975DD8E016DE3C11F0193E13B (void);
// 0x00000A66 NPOI.POIFS.Storage.SmallDocumentBlock[] NPOI.POIFS.Storage.SmallDocumentBlock::Convert(NPOI.POIFS.Common.POIFSBigBlockSize,NPOI.POIFS.Storage.BlockWritable[],System.Int32)
extern void SmallDocumentBlock_Convert_m3C4BA518D643A2A01D1644D71AED7FB6673026D0 (void);
// 0x00000A67 System.Collections.Generic.List`1<NPOI.POIFS.Storage.SmallDocumentBlock> NPOI.POIFS.Storage.SmallDocumentBlock::Extract(NPOI.POIFS.Common.POIFSBigBlockSize,NPOI.POIFS.Storage.ListManagedBlock[])
extern void SmallDocumentBlock_Extract_m4B5247879806F696DC7AE3E24751180D98A27AE0 (void);
// 0x00000A68 NPOI.POIFS.Storage.DataInputBlock NPOI.POIFS.Storage.SmallDocumentBlock::GetDataInputBlock(NPOI.POIFS.Storage.SmallDocumentBlock[],System.Int32)
extern void SmallDocumentBlock_GetDataInputBlock_m4822583DD7A2E762E1B2BDD6931761ADF8B0D3EF (void);
// 0x00000A69 System.Int32 NPOI.POIFS.Storage.SmallDocumentBlock::CalcSize(System.Int32)
extern void SmallDocumentBlock_CalcSize_mF5BBDFBDE8C949A4EAE221D8921AB95DCA780749 (void);
// 0x00000A6A NPOI.POIFS.Storage.SmallDocumentBlock NPOI.POIFS.Storage.SmallDocumentBlock::MakeEmptySmallDocumentBlock(NPOI.POIFS.Common.POIFSBigBlockSize)
extern void SmallDocumentBlock_MakeEmptySmallDocumentBlock_m2DFA5A7470F2A77315D585E4F94C432F322020B4 (void);
// 0x00000A6B System.Int32 NPOI.POIFS.Storage.SmallDocumentBlock::ConvertToBlockCount(System.Int32)
extern void SmallDocumentBlock_ConvertToBlockCount_m36FF691977585CF8F85470517C1D0801AE6D402A (void);
// 0x00000A6C System.Void NPOI.POIFS.Storage.SmallDocumentBlock::WriteBlocks(System.IO.Stream)
extern void SmallDocumentBlock_WriteBlocks_m11ECC27AEDCB91EDDF46C6021D34C7576EEB29C5 (void);
// 0x00000A6D System.Byte[] NPOI.POIFS.Storage.SmallDocumentBlock::get_Data()
extern void SmallDocumentBlock_get_Data_m0BFDD48023B9F70BCAF729D765281291FB05421A (void);
// 0x00000A6E System.Void NPOI.POIFS.Storage.SmallDocumentBlock::.cctor()
extern void SmallDocumentBlock__cctor_m39A98D225D656CA90FF59FC763D7EC869AAD014A (void);
// 0x00000A6F System.Void NPOI.POIFS.Storage.SmallDocumentBlockList::.ctor(System.Collections.Generic.List`1<NPOI.POIFS.Storage.SmallDocumentBlock>)
extern void SmallDocumentBlockList__ctor_m7BB5D41298E5260305CE3CBA399AE0A87AE13B14 (void);
// 0x00000A70 System.Int32 NPOI.SS.UserModel.ICell::get_ColumnIndex()
// 0x00000A71 System.Int32 NPOI.SS.UserModel.ICell::get_RowIndex()
// 0x00000A72 NPOI.SS.UserModel.ISheet NPOI.SS.UserModel.ICell::get_Sheet()
// 0x00000A73 NPOI.SS.UserModel.CellType NPOI.SS.UserModel.ICell::get_CellType()
// 0x00000A74 System.Void NPOI.SS.UserModel.ICell::SetCellType(NPOI.SS.UserModel.CellType)
// 0x00000A75 System.Void NPOI.SS.UserModel.ICell::SetCellValue(System.Double)
// 0x00000A76 System.Void NPOI.SS.UserModel.ICell::SetCellValue(System.String)
// 0x00000A77 System.String NPOI.SS.UserModel.ICell::get_CellFormula()
// 0x00000A78 System.Double NPOI.SS.UserModel.ICell::get_NumericCellValue()
// 0x00000A79 System.DateTime NPOI.SS.UserModel.ICell::get_DateCellValue()
// 0x00000A7A NPOI.SS.UserModel.IRichTextString NPOI.SS.UserModel.ICell::get_RichStringCellValue()
// 0x00000A7B System.Byte NPOI.SS.UserModel.ICell::get_ErrorCellValue()
// 0x00000A7C System.String NPOI.SS.UserModel.ICell::get_StringCellValue()
// 0x00000A7D System.Void NPOI.SS.UserModel.ICell::SetCellValue(System.Boolean)
// 0x00000A7E System.Boolean NPOI.SS.UserModel.ICell::get_BooleanCellValue()
// 0x00000A7F NPOI.SS.UserModel.ICellStyle NPOI.SS.UserModel.ICell::get_CellStyle()
// 0x00000A80 System.Void NPOI.HSSF.UserModel.HSSFCell::.ctor(NPOI.HSSF.UserModel.HSSFWorkbook,NPOI.HSSF.UserModel.HSSFSheet,System.Int32,System.Int16,NPOI.SS.UserModel.CellType)
extern void HSSFCell__ctor_mBE96AB275D10A146B2AFB543CDC3D1EADDA56459 (void);
// 0x00000A81 System.Void NPOI.HSSF.UserModel.HSSFCell::.ctor(NPOI.HSSF.UserModel.HSSFWorkbook,NPOI.HSSF.UserModel.HSSFSheet,NPOI.HSSF.Record.CellValueRecordInterface)
extern void HSSFCell__ctor_m34946F8045C76CBC87EA3E53E307241406DA85F9 (void);
// 0x00000A82 System.Void NPOI.HSSF.UserModel.HSSFCell::.ctor()
extern void HSSFCell__ctor_mB4F0881EDEAF8677B4BD66E1D9FDF5D261D2D404 (void);
// 0x00000A83 NPOI.SS.UserModel.CellType NPOI.HSSF.UserModel.HSSFCell::DetermineType(NPOI.HSSF.Record.CellValueRecordInterface)
extern void HSSFCell_DetermineType_m2BC7D1EB221FC21A8DCA0994F1B3CEF42BFF2EBC (void);
// 0x00000A84 NPOI.SS.UserModel.ISheet NPOI.HSSF.UserModel.HSSFCell::get_Sheet()
extern void HSSFCell_get_Sheet_m8E834383A8CD8B5CE0C0A55FABAE978469910BF6 (void);
// 0x00000A85 NPOI.SS.UserModel.IRow NPOI.HSSF.UserModel.HSSFCell::get_Row()
extern void HSSFCell_get_Row_mD23B7F22461413B151F9EA2016FFE2AE61FE3BBB (void);
// 0x00000A86 System.Void NPOI.HSSF.UserModel.HSSFCell::SetCellType(NPOI.SS.UserModel.CellType)
extern void HSSFCell_SetCellType_mC30B4560711497C7B0FC592F3B7BF15BFD6244B6 (void);
// 0x00000A87 System.Void NPOI.HSSF.UserModel.HSSFCell::SetCellType(NPOI.SS.UserModel.CellType,System.Boolean,System.Int32,System.Int32,System.Int16)
extern void HSSFCell_SetCellType_m9BEC5675DA9B138E605BF603D1094240CE704981 (void);
// 0x00000A88 NPOI.SS.UserModel.CellType NPOI.HSSF.UserModel.HSSFCell::get_CellType()
extern void HSSFCell_get_CellType_mE403E2CAB5C4D3DF8972E25D2FADD19B136147DB (void);
// 0x00000A89 System.String NPOI.HSSF.UserModel.HSSFCell::ConvertCellValueToString()
extern void HSSFCell_ConvertCellValueToString_mCF6C54A9C168FE2D0B67751EC47EC4E7AB9F277D (void);
// 0x00000A8A System.Void NPOI.HSSF.UserModel.HSSFCell::SetCellValue(System.Double)
extern void HSSFCell_SetCellValue_m1E334F6E0DB2200CE7D3076AD9F6F616275F7622 (void);
// 0x00000A8B System.Void NPOI.HSSF.UserModel.HSSFCell::SetCellValue(System.String)
extern void HSSFCell_SetCellValue_m0BDDB85954F747FF4B004DDB82BEA08B35565A96 (void);
// 0x00000A8C System.Void NPOI.HSSF.UserModel.HSSFCell::SetCellErrorValue(System.Byte)
extern void HSSFCell_SetCellErrorValue_mBDB423E3FB809B7FDBA35E553E4A0B0CECA2F12E (void);
// 0x00000A8D System.Void NPOI.HSSF.UserModel.HSSFCell::SetCellValue(NPOI.SS.UserModel.IRichTextString)
extern void HSSFCell_SetCellValue_mA9620129A176BE7414B1E6CA56D0B78659E4F313 (void);
// 0x00000A8E System.Void NPOI.HSSF.UserModel.HSSFCell::NotifyFormulaChanging()
extern void HSSFCell_NotifyFormulaChanging_m55AC3208C81BC8C8D54296D2D5CDB3BC6F8457EA (void);
// 0x00000A8F System.String NPOI.HSSF.UserModel.HSSFCell::get_CellFormula()
extern void HSSFCell_get_CellFormula_m913F5B49131AB2384F12E8DC7609991BE38623B7 (void);
// 0x00000A90 System.Double NPOI.HSSF.UserModel.HSSFCell::get_NumericCellValue()
extern void HSSFCell_get_NumericCellValue_mC77199558ED1106F0276527D6ED6BCBB20F21D96 (void);
// 0x00000A91 System.String NPOI.HSSF.UserModel.HSSFCell::GetCellTypeName(NPOI.SS.UserModel.CellType)
extern void HSSFCell_GetCellTypeName_m9DF8171C25C9E9590A9F200FC120D0E83311E216 (void);
// 0x00000A92 System.Exception NPOI.HSSF.UserModel.HSSFCell::TypeMismatch(NPOI.SS.UserModel.CellType,NPOI.SS.UserModel.CellType,System.Boolean)
extern void HSSFCell_TypeMismatch_m3CAC109F396096A40AA0D27A9622CDA1B13228C8 (void);
// 0x00000A93 System.Void NPOI.HSSF.UserModel.HSSFCell::CheckFormulaCachedValueType(NPOI.SS.UserModel.CellType,NPOI.HSSF.Record.FormulaRecord)
extern void HSSFCell_CheckFormulaCachedValueType_mFF8F4C8582BE9099E764FA2038C934718DD61EF8 (void);
// 0x00000A94 System.DateTime NPOI.HSSF.UserModel.HSSFCell::get_DateCellValue()
extern void HSSFCell_get_DateCellValue_m568151241A852485C56E3CC70684BED67CBC602F (void);
// 0x00000A95 System.String NPOI.HSSF.UserModel.HSSFCell::get_StringCellValue()
extern void HSSFCell_get_StringCellValue_m27D1A744609102EA1FA94E9F4FEE3867AD4B133C (void);
// 0x00000A96 NPOI.SS.UserModel.IRichTextString NPOI.HSSF.UserModel.HSSFCell::get_RichStringCellValue()
extern void HSSFCell_get_RichStringCellValue_mF2071E01518B31301A02BD0EAA1BD38D03F4FB9F (void);
// 0x00000A97 System.Void NPOI.HSSF.UserModel.HSSFCell::SetCellValue(System.Boolean)
extern void HSSFCell_SetCellValue_m8E3153D7895269B8A1A8023001DBFDDB0443918D (void);
// 0x00000A98 System.Boolean NPOI.HSSF.UserModel.HSSFCell::ConvertCellValueToBoolean()
extern void HSSFCell_ConvertCellValueToBoolean_m51E88CBF78649F9E0652941CA2B3D943DC5F3CD4 (void);
// 0x00000A99 System.Boolean NPOI.HSSF.UserModel.HSSFCell::get_BooleanCellValue()
extern void HSSFCell_get_BooleanCellValue_m3F713C7B2AA1857B9F516A52F38741C86363F313 (void);
// 0x00000A9A System.Byte NPOI.HSSF.UserModel.HSSFCell::get_ErrorCellValue()
extern void HSSFCell_get_ErrorCellValue_mE6D4E7DDC208975BBD8BA20AAB6B125E26AF78B6 (void);
// 0x00000A9B NPOI.SS.UserModel.ICellStyle NPOI.HSSF.UserModel.HSSFCell::get_CellStyle()
extern void HSSFCell_get_CellStyle_m00B2C346B221631389E7E7EAB3E7E4E517DA579F (void);
// 0x00000A9C NPOI.HSSF.Record.CellValueRecordInterface NPOI.HSSF.UserModel.HSSFCell::get_CellValueRecord()
extern void HSSFCell_get_CellValueRecord_m8AFB0D4A349F135FFA4CBBC56771CC8D14C754FA (void);
// 0x00000A9D System.Void NPOI.HSSF.UserModel.HSSFCell::CheckBounds(System.Int32)
extern void HSSFCell_CheckBounds_m6163E3426B9205BD4D3B6BAF2B317E7CB222DB78 (void);
// 0x00000A9E System.String NPOI.HSSF.UserModel.HSSFCell::ToString()
extern void HSSFCell_ToString_mFAF26AAC803F2A0112856E15E9E2FE2A21D34218 (void);
// 0x00000A9F System.Int32 NPOI.HSSF.UserModel.HSSFCell::get_ColumnIndex()
extern void HSSFCell_get_ColumnIndex_m0BE9086D60532F676A805C0362586643CF21CB7B (void);
// 0x00000AA0 System.Int32 NPOI.HSSF.UserModel.HSSFCell::get_RowIndex()
extern void HSSFCell_get_RowIndex_mC7A930A49CB594ED4C31403C07A1500205A6D2EE (void);
// 0x00000AA1 System.Boolean NPOI.HSSF.UserModel.HSSFCell::get_IsPartOfArrayFormulaGroup()
extern void HSSFCell_get_IsPartOfArrayFormulaGroup_m5D20A5EC49D307784C5B7BBB7CE1F06E6029DFD1 (void);
// 0x00000AA2 NPOI.SS.Util.CellRangeAddress NPOI.HSSF.UserModel.HSSFCell::get_ArrayFormulaRange()
extern void HSSFCell_get_ArrayFormulaRange_m68817B78ED7D57ECAB027381F606297ABC9F02BE (void);
// 0x00000AA3 System.Void NPOI.HSSF.UserModel.HSSFCell::NotifyArrayFormulaChanging(System.String)
extern void HSSFCell_NotifyArrayFormulaChanging_m195070C03C71EFFDF7F09A9F9CBB14133EDAA59A (void);
// 0x00000AA4 System.Void NPOI.HSSF.UserModel.HSSFCell::NotifyArrayFormulaChanging()
extern void HSSFCell_NotifyArrayFormulaChanging_mDF76A9C3289771F66A68A215682BB08E7F6A454A (void);
// 0x00000AA5 System.Void NPOI.HSSF.UserModel.HSSFCell::.cctor()
extern void HSSFCell__cctor_m741E41780695747AE3B43B69B107125B990162B6 (void);
// 0x00000AA6 System.Int16 NPOI.SS.UserModel.ICellStyle::get_DataFormat()
// 0x00000AA7 System.String NPOI.SS.UserModel.ICellStyle::GetDataFormatString()
// 0x00000AA8 System.Void NPOI.HSSF.UserModel.HSSFCellStyle::.ctor(System.Int16,NPOI.HSSF.Record.ExtendedFormatRecord,NPOI.HSSF.UserModel.HSSFWorkbook)
extern void HSSFCellStyle__ctor_m2FE25DCAD9A643B526C68B71DD46E03F9ACFC03D (void);
// 0x00000AA9 System.Void NPOI.HSSF.UserModel.HSSFCellStyle::.ctor(System.Int16,NPOI.HSSF.Record.ExtendedFormatRecord,NPOI.HSSF.Model.InternalWorkbook)
extern void HSSFCellStyle__ctor_m7C29921F7BDF46ADD0A03A6220605FAE786DF885 (void);
// 0x00000AAA System.Int16 NPOI.HSSF.UserModel.HSSFCellStyle::get_DataFormat()
extern void HSSFCellStyle_get_DataFormat_m25EC97585A07344CE733D0F13866DADAE1E2C828 (void);
// 0x00000AAB System.String NPOI.HSSF.UserModel.HSSFCellStyle::GetDataFormatString()
extern void HSSFCellStyle_GetDataFormatString_mE9DA1FFAA7250ECB4386F33D57B547DF3967DDB6 (void);
// 0x00000AAC System.Int32 NPOI.HSSF.UserModel.HSSFCellStyle::GetHashCode()
extern void HSSFCellStyle_GetHashCode_m7368C6B3F8354FEC5AEF60CBE0A196D70AA457F8 (void);
// 0x00000AAD System.Boolean NPOI.HSSF.UserModel.HSSFCellStyle::Equals(System.Object)
extern void HSSFCellStyle_Equals_mC663069B4D7B592A88799F2FA8202498D6BF9A41 (void);
// 0x00000AAE System.Void NPOI.HSSF.UserModel.HSSFDataFormat::.ctor(NPOI.HSSF.Model.InternalWorkbook)
extern void HSSFDataFormat__ctor_m41AC3CE035FEFAF4FEEAF8FEE8C3A15969B6B6C8 (void);
// 0x00000AAF System.String NPOI.HSSF.UserModel.HSSFDataFormat::GetFormat(System.Int16)
extern void HSSFDataFormat_GetFormat_m044914E65F92E4B70AB5BCAAEE0546D7CFF72772 (void);
// 0x00000AB0 System.Void NPOI.HSSF.UserModel.HSSFDataFormat::.cctor()
extern void HSSFDataFormat__cctor_mD25BC16DA05F083D077D55BC144ABA6BAB0B5380 (void);
// 0x00000AB1 NPOI.SS.Formula.ExternalSheet NPOI.SS.Formula.IFormulaRenderingWorkbook::GetExternalSheet(System.Int32)
// 0x00000AB2 System.String NPOI.SS.Formula.IFormulaRenderingWorkbook::GetSheetNameByExternSheet(System.Int32)
// 0x00000AB3 System.String NPOI.SS.Formula.IFormulaRenderingWorkbook::ResolveNameXText(NPOI.SS.Formula.PTG.NameXPtg)
// 0x00000AB4 System.String NPOI.SS.Formula.IFormulaRenderingWorkbook::GetNameText(NPOI.SS.Formula.PTG.NamePtg)
// 0x00000AB5 NPOI.SS.Formula.IEvaluationName NPOI.SS.Formula.IFormulaParsingWorkbook::GetName(System.String,System.Int32)
// 0x00000AB6 NPOI.SS.Formula.PTG.NameXPtg NPOI.SS.Formula.IFormulaParsingWorkbook::GetNameXPtg(System.String)
// 0x00000AB7 System.Int32 NPOI.SS.Formula.IFormulaParsingWorkbook::GetExternalSheetIndex(System.String)
// 0x00000AB8 System.Int32 NPOI.SS.Formula.IFormulaParsingWorkbook::GetExternalSheetIndex(System.String,System.String)
// 0x00000AB9 NPOI.SS.SpreadsheetVersion NPOI.SS.Formula.IFormulaParsingWorkbook::GetSpreadsheetVersion()
// 0x00000ABA NPOI.HSSF.UserModel.HSSFEvaluationWorkbook NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::Create(NPOI.SS.UserModel.IWorkbook)
extern void HSSFEvaluationWorkbook_Create_m1AC732DF89755AC1BE8240BED4188E9BCC564D03 (void);
// 0x00000ABB System.Void NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::.ctor(NPOI.HSSF.UserModel.HSSFWorkbook)
extern void HSSFEvaluationWorkbook__ctor_m4ABAC2B614069039333D78AD2C099FDE535E2C7A (void);
// 0x00000ABC System.Int32 NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::GetExternalSheetIndex(System.String)
extern void HSSFEvaluationWorkbook_GetExternalSheetIndex_mADEBAA152135E762160802FF19728B3A00D8B761 (void);
// 0x00000ABD System.Int32 NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::GetExternalSheetIndex(System.String,System.String)
extern void HSSFEvaluationWorkbook_GetExternalSheetIndex_m690F9D093A3095CA2EBE90D81024757065EBB9B9 (void);
// 0x00000ABE NPOI.SS.Formula.PTG.NameXPtg NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::GetNameXPtg(System.String)
extern void HSSFEvaluationWorkbook_GetNameXPtg_mD6C9AA45F55F57744E3397D08F79FFD39F2BBD43 (void);
// 0x00000ABF NPOI.SS.Formula.IEvaluationName NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::GetName(System.String,System.Int32)
extern void HSSFEvaluationWorkbook_GetName_m88ACC4C1AF210DE3E4DE06F2E11456B984217EEC (void);
// 0x00000AC0 NPOI.SS.Formula.ExternalSheet NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::GetExternalSheet(System.Int32)
extern void HSSFEvaluationWorkbook_GetExternalSheet_m9DD101B46D3801800D5C50FBA0590EB520DABF83 (void);
// 0x00000AC1 System.String NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::ResolveNameXText(NPOI.SS.Formula.PTG.NameXPtg)
extern void HSSFEvaluationWorkbook_ResolveNameXText_m27807FC76D5B996FF354763AE38FD93964D543D2 (void);
// 0x00000AC2 System.String NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::GetSheetNameByExternSheet(System.Int32)
extern void HSSFEvaluationWorkbook_GetSheetNameByExternSheet_m71A4EE0C744C035BC89131CA7EACDBBDC0D8B75B (void);
// 0x00000AC3 System.String NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::GetNameText(NPOI.SS.Formula.PTG.NamePtg)
extern void HSSFEvaluationWorkbook_GetNameText_m3CC600914A65788F00E63560C7CD5DB66E1B5FBD (void);
// 0x00000AC4 NPOI.SS.SpreadsheetVersion NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::GetSpreadsheetVersion()
extern void HSSFEvaluationWorkbook_GetSpreadsheetVersion_mB11E0201A6B3E5DD26A131E9BC138663891347DD (void);
// 0x00000AC5 System.Void NPOI.HSSF.UserModel.HSSFEvaluationWorkbook::.cctor()
extern void HSSFEvaluationWorkbook__cctor_m2738BE1F07ECCFD5067D0FEFE2949E21245D2F12 (void);
// 0x00000AC6 System.Void NPOI.HSSF.UserModel.HSSFEvaluationWorkbook_Name::.ctor(NPOI.HSSF.Record.NameRecord,System.Int32)
extern void Name__ctor_mB7947220D3599D0CF6A640CF88B5B4D64BF3E019 (void);
// 0x00000AC7 System.Boolean NPOI.HSSF.UserModel.HSSFEvaluationWorkbook_Name::get_IsFunctionName()
extern void Name_get_IsFunctionName_m78B81A5D1343E4C44596F881D0DFA218197E673F (void);
// 0x00000AC8 System.Boolean NPOI.HSSF.UserModel.HSSFEvaluationWorkbook_Name::get_IsRange()
extern void Name_get_IsRange_mD372D65327E88FD57F1BE86E9250F82B45F62E4E (void);
// 0x00000AC9 NPOI.SS.Formula.PTG.NamePtg NPOI.HSSF.UserModel.HSSFEvaluationWorkbook_Name::CreatePtg()
extern void Name_CreatePtg_m94CAB9200C834EF517584DD13F0C0139681AF71B (void);
// 0x00000ACA System.Boolean NPOI.SS.Formula.IEvaluationName::get_IsFunctionName()
// 0x00000ACB System.Boolean NPOI.SS.Formula.IEvaluationName::get_IsRange()
// 0x00000ACC NPOI.SS.Formula.PTG.NamePtg NPOI.SS.Formula.IEvaluationName::CreatePtg()
// 0x00000ACD NPOI.SS.UserModel.CellType NPOI.SS.UserModel.IFormulaEvaluator::EvaluateFormulaCell(NPOI.SS.UserModel.ICell)
// 0x00000ACE System.String NPOI.SS.UserModel.IName::get_NameName()
// 0x00000ACF System.Int32 NPOI.SS.UserModel.IName::get_SheetIndex()
// 0x00000AD0 System.Void NPOI.HSSF.UserModel.HSSFName::.ctor(NPOI.HSSF.UserModel.HSSFWorkbook,NPOI.HSSF.Record.NameRecord,NPOI.HSSF.Record.NameCommentRecord)
extern void HSSFName__ctor_m78AFA0517882707700F91F6962F7750198392C43 (void);
// 0x00000AD1 System.String NPOI.HSSF.UserModel.HSSFName::get_NameName()
extern void HSSFName_get_NameName_m0FD1F5A903BECD8E598A1F6CEF46CF0F3E6CAFCD (void);
// 0x00000AD2 System.Int32 NPOI.HSSF.UserModel.HSSFName::get_SheetIndex()
extern void HSSFName_get_SheetIndex_m1C4DF9993BE91BA28E0EA6B4AEA69F6E626C65AA (void);
// 0x00000AD3 System.String NPOI.HSSF.UserModel.HSSFName::ToString()
extern void HSSFName_ToString_mD5AB691D49CAA0F358CE8E4491CA217502EDF9E6 (void);
// 0x00000AD4 System.Void NPOI.HSSF.Util.HSSFColor::.ctor()
extern void HSSFColor__ctor_mE548D32E94F5A2FF170B296F1BE293456D6CEDA0 (void);
// 0x00000AD5 System.Void NPOI.HSSF.Util.HSSFColor_Black::.ctor()
extern void Black__ctor_mDD512ABE1DF9BB056BD72A195DC90DF0810FB963 (void);
// 0x00000AD6 System.Void NPOI.HSSF.Util.HSSFColor_Black::.cctor()
extern void Black__cctor_m0DDF80B6D757E1AEFF4FD9A6C9689AF52475FDE1 (void);
// 0x00000AD7 System.Void NPOI.HSSF.Util.HSSFColor_Brown::.ctor()
extern void Brown__ctor_m0E80E41050A24C9FA9406EFC299B05728F33AF9D (void);
// 0x00000AD8 System.Void NPOI.HSSF.Util.HSSFColor_Brown::.cctor()
extern void Brown__cctor_mF463CFA772B94E3D35F13A9058269192E7FEAE35 (void);
// 0x00000AD9 System.Void NPOI.HSSF.Util.HSSFColor_OliveGreen::.ctor()
extern void OliveGreen__ctor_m0E43B7D05819D33711ED8EB1DB910C1852EF6039 (void);
// 0x00000ADA System.Void NPOI.HSSF.Util.HSSFColor_OliveGreen::.cctor()
extern void OliveGreen__cctor_m60083D98DACCA883A426A97622CEB7A713250B58 (void);
// 0x00000ADB System.Void NPOI.HSSF.Util.HSSFColor_DarkGreen::.ctor()
extern void DarkGreen__ctor_m5C2A8B2D6B2AF4E38A428E61A0651FF7AD34CE86 (void);
// 0x00000ADC System.Void NPOI.HSSF.Util.HSSFColor_DarkGreen::.cctor()
extern void DarkGreen__cctor_m8AE6F0D0498DD766C66418A791A8595ED40E24A3 (void);
// 0x00000ADD System.Void NPOI.HSSF.Util.HSSFColor_DarkTeal::.ctor()
extern void DarkTeal__ctor_m869EC86D21EA5D02A213A369304216AC15107F6D (void);
// 0x00000ADE System.Void NPOI.HSSF.Util.HSSFColor_DarkTeal::.cctor()
extern void DarkTeal__cctor_m522106859A6E51E81C9FCAF5ABCEC944ADFEB90A (void);
// 0x00000ADF System.Void NPOI.HSSF.Util.HSSFColor_DarkBlue::.ctor()
extern void DarkBlue__ctor_mCDF3E17EB660E4B4F9FB2390AAC81BD8EAA171EC (void);
// 0x00000AE0 System.Void NPOI.HSSF.Util.HSSFColor_DarkBlue::.cctor()
extern void DarkBlue__cctor_m2BB89C834104FAC5B76D90F6B023C4094D10655E (void);
// 0x00000AE1 System.Void NPOI.HSSF.Util.HSSFColor_Indigo::.ctor()
extern void Indigo__ctor_m189535777D7D924CD30E24AB4968574C7A78E749 (void);
// 0x00000AE2 System.Void NPOI.HSSF.Util.HSSFColor_Indigo::.cctor()
extern void Indigo__cctor_m02120AE45583B1B913016FE6A825A75BC2E35E9E (void);
// 0x00000AE3 System.Void NPOI.HSSF.Util.HSSFColor_Grey80Percent::.ctor()
extern void Grey80Percent__ctor_mBFB19CCA82A75CCDFEB7B1FA31CAEFCF34B4A00E (void);
// 0x00000AE4 System.Void NPOI.HSSF.Util.HSSFColor_Grey80Percent::.cctor()
extern void Grey80Percent__cctor_m893447BD04E619EBC73FD6BB67FAFC59DE39DD14 (void);
// 0x00000AE5 System.Void NPOI.HSSF.Util.HSSFColor_DarkRed::.ctor()
extern void DarkRed__ctor_m51C47FAC15626F8C51AAC18EA73A5F00A1C8CBB4 (void);
// 0x00000AE6 System.Void NPOI.HSSF.Util.HSSFColor_DarkRed::.cctor()
extern void DarkRed__cctor_mB41DF7EEF26ACFC9D70ECEF73F8E3EA91E0433FD (void);
// 0x00000AE7 System.Void NPOI.HSSF.Util.HSSFColor_Orange::.ctor()
extern void Orange__ctor_m55351A08AC385F6B7D4A1C56538CCDC3344F3178 (void);
// 0x00000AE8 System.Void NPOI.HSSF.Util.HSSFColor_Orange::.cctor()
extern void Orange__cctor_m3B9C6D73AD5BFBCEB2583F0AD14722FD3692ECF9 (void);
// 0x00000AE9 System.Void NPOI.HSSF.Util.HSSFColor_DarkYellow::.ctor()
extern void DarkYellow__ctor_mE7D98866483DB83A8AC5585D1B94A95AF9DE481E (void);
// 0x00000AEA System.Void NPOI.HSSF.Util.HSSFColor_DarkYellow::.cctor()
extern void DarkYellow__cctor_m85421E1DFA911EA982260CB780AD67935D76694C (void);
// 0x00000AEB System.Void NPOI.HSSF.Util.HSSFColor_Green::.ctor()
extern void Green__ctor_m425821F7CE7EF6CEE72D99548AD72F60D1EDDCCF (void);
// 0x00000AEC System.Void NPOI.HSSF.Util.HSSFColor_Green::.cctor()
extern void Green__cctor_m4A143F7672968E0942AE70BD9522747F9723C721 (void);
// 0x00000AED System.Void NPOI.HSSF.Util.HSSFColor_Teal::.ctor()
extern void Teal__ctor_m50D26DE4B307BA823BE4AA5442DDC2A69CE26296 (void);
// 0x00000AEE System.Void NPOI.HSSF.Util.HSSFColor_Teal::.cctor()
extern void Teal__cctor_m9539606D5E54C4953D23FF97A6E6D6C9F03BD9D8 (void);
// 0x00000AEF System.Void NPOI.HSSF.Util.HSSFColor_Blue::.ctor()
extern void Blue__ctor_m4F73B113B8050F8EA64BFF05F7D93DD10759EE7D (void);
// 0x00000AF0 System.Void NPOI.HSSF.Util.HSSFColor_Blue::.cctor()
extern void Blue__cctor_m1158E1C639891019AD2A70A3B2266FC39E3C7032 (void);
// 0x00000AF1 System.Void NPOI.HSSF.Util.HSSFColor_BlueGrey::.ctor()
extern void BlueGrey__ctor_m303715C4D2E3D59F227F982A3DFA0DC444080408 (void);
// 0x00000AF2 System.Void NPOI.HSSF.Util.HSSFColor_BlueGrey::.cctor()
extern void BlueGrey__cctor_mD5A1B25B45FC039D3EAF4E027C4DD9791236ACA0 (void);
// 0x00000AF3 System.Void NPOI.HSSF.Util.HSSFColor_Grey50Percent::.ctor()
extern void Grey50Percent__ctor_m96B618DF2D8F9AB964F842BB962DB2E0D062B24D (void);
// 0x00000AF4 System.Void NPOI.HSSF.Util.HSSFColor_Grey50Percent::.cctor()
extern void Grey50Percent__cctor_m9D44C37CCC03B7C5E44D3FCBA841F33239C15534 (void);
// 0x00000AF5 System.Void NPOI.HSSF.Util.HSSFColor_Red::.ctor()
extern void Red__ctor_mCB26E089499D343D656405B091BCC0279AE6B5EE (void);
// 0x00000AF6 System.Void NPOI.HSSF.Util.HSSFColor_Red::.cctor()
extern void Red__cctor_mCAE40A1A9C644BD2665C95AADD0013A0653F9027 (void);
// 0x00000AF7 System.Void NPOI.HSSF.Util.HSSFColor_LightOrange::.ctor()
extern void LightOrange__ctor_mED1C0930F7592E5D39D255A249D7070B45CB1114 (void);
// 0x00000AF8 System.Void NPOI.HSSF.Util.HSSFColor_LightOrange::.cctor()
extern void LightOrange__cctor_mD118C8ED4F78C1EF6CB72CC55730D25DC4E74A9A (void);
// 0x00000AF9 System.Void NPOI.HSSF.Util.HSSFColor_Lime::.ctor()
extern void Lime__ctor_mBF0A6EC9F661FCEDE27FCCF70ECB07E358D07A60 (void);
// 0x00000AFA System.Void NPOI.HSSF.Util.HSSFColor_Lime::.cctor()
extern void Lime__cctor_m0DEAC99DA5647600412E613C8FD66F2D58E21160 (void);
// 0x00000AFB System.Void NPOI.HSSF.Util.HSSFColor_SeaGreen::.ctor()
extern void SeaGreen__ctor_mC838FB6634715AFA25F80DC92F63B8BFB21ABFE6 (void);
// 0x00000AFC System.Void NPOI.HSSF.Util.HSSFColor_SeaGreen::.cctor()
extern void SeaGreen__cctor_m48484033F3300FC081BC99819E59DED6A1946FC8 (void);
// 0x00000AFD System.Void NPOI.HSSF.Util.HSSFColor_Aqua::.ctor()
extern void Aqua__ctor_mA77E252F3BD76E5BC8D99326640B8A4E9E051D61 (void);
// 0x00000AFE System.Void NPOI.HSSF.Util.HSSFColor_Aqua::.cctor()
extern void Aqua__cctor_m3835560656C293B6F6F784C79E23515FB4234FE6 (void);
// 0x00000AFF System.Void NPOI.HSSF.Util.HSSFColor_LightBlue::.ctor()
extern void LightBlue__ctor_m54C137C308C53DDA50F3DDC39C5E466147B0F2A6 (void);
// 0x00000B00 System.Void NPOI.HSSF.Util.HSSFColor_LightBlue::.cctor()
extern void LightBlue__cctor_mFCBCC88DE5107CCB5287DB8D827E6E479D8B0E61 (void);
// 0x00000B01 System.Void NPOI.HSSF.Util.HSSFColor_Violet::.ctor()
extern void Violet__ctor_m9F29FEF7DC3A523EC9B925047FA58B78F9ABADED (void);
// 0x00000B02 System.Void NPOI.HSSF.Util.HSSFColor_Violet::.cctor()
extern void Violet__cctor_mECD9BC37F0A360A13035E03C59E622DE60E5A21A (void);
// 0x00000B03 System.Void NPOI.HSSF.Util.HSSFColor_Grey40Percent::.ctor()
extern void Grey40Percent__ctor_m7121D52B98FD82381A06678E1D7EE11B7A869B94 (void);
// 0x00000B04 System.Void NPOI.HSSF.Util.HSSFColor_Grey40Percent::.cctor()
extern void Grey40Percent__cctor_mBE0270F9D9EA050DB86F846919A112CB023A8077 (void);
// 0x00000B05 System.Void NPOI.HSSF.Util.HSSFColor_Pink::.ctor()
extern void Pink__ctor_m7332C4C0E0FFDBBDF5DC6217B0B7DE7B41831A8D (void);
// 0x00000B06 System.Void NPOI.HSSF.Util.HSSFColor_Pink::.cctor()
extern void Pink__cctor_mD008BAFA1E0118ADD67020512AB961ED46CC0576 (void);
// 0x00000B07 System.Void NPOI.HSSF.Util.HSSFColor_Gold::.ctor()
extern void Gold__ctor_m0C9E33A40BF98F1F5C0E32465BD7FECD108DC1B5 (void);
// 0x00000B08 System.Void NPOI.HSSF.Util.HSSFColor_Gold::.cctor()
extern void Gold__cctor_m863F8EC5FF0DA0484533516053F92DED81E2EFC9 (void);
// 0x00000B09 System.Void NPOI.HSSF.Util.HSSFColor_Yellow::.ctor()
extern void Yellow__ctor_m9432629D83B482FFBD1B580098DCCE9DB927332C (void);
// 0x00000B0A System.Void NPOI.HSSF.Util.HSSFColor_Yellow::.cctor()
extern void Yellow__cctor_m12CDBB7603907802E99E8EB2350E4216A4E1BE8A (void);
// 0x00000B0B System.Void NPOI.HSSF.Util.HSSFColor_BrightGreen::.ctor()
extern void BrightGreen__ctor_m4A5175FACF2D6F7E4374BC2C0B37F5D7582C36F1 (void);
// 0x00000B0C System.Void NPOI.HSSF.Util.HSSFColor_BrightGreen::.cctor()
extern void BrightGreen__cctor_mB3F70276C338F7DBD752C14252FF0E545465A3F4 (void);
// 0x00000B0D System.Void NPOI.HSSF.Util.HSSFColor_Turquoise::.ctor()
extern void Turquoise__ctor_m9859BDCED223DF963AA53853065CC62C3CDD6787 (void);
// 0x00000B0E System.Void NPOI.HSSF.Util.HSSFColor_Turquoise::.cctor()
extern void Turquoise__cctor_mF7CED81644BC41F7FDA3855EB2E1786F1C6C59B8 (void);
// 0x00000B0F System.Void NPOI.HSSF.Util.HSSFColor_SkyBlue::.ctor()
extern void SkyBlue__ctor_m7D7402BAED2222ACCDAB95E9350C2BE8A17102CF (void);
// 0x00000B10 System.Void NPOI.HSSF.Util.HSSFColor_SkyBlue::.cctor()
extern void SkyBlue__cctor_m8830856562ACB02FD0427A07C2EE2717781DD35A (void);
// 0x00000B11 System.Void NPOI.HSSF.Util.HSSFColor_Plum::.ctor()
extern void Plum__ctor_m86D2888A5FAA1BB749F869C36E7F63B0F43E673B (void);
// 0x00000B12 System.Void NPOI.HSSF.Util.HSSFColor_Plum::.cctor()
extern void Plum__cctor_m6408C7E726EF562D5AD21912C84D8B21A283FC4B (void);
// 0x00000B13 System.Void NPOI.HSSF.Util.HSSFColor_Grey25Percent::.ctor()
extern void Grey25Percent__ctor_mC7155DD6955BEE2BC772D68FD7E08B2C1ACE9C33 (void);
// 0x00000B14 System.Void NPOI.HSSF.Util.HSSFColor_Grey25Percent::.cctor()
extern void Grey25Percent__cctor_mA637492EA2294FC7660CE12CC4994546C46D0BF9 (void);
// 0x00000B15 System.Void NPOI.HSSF.Util.HSSFColor_Rose::.ctor()
extern void Rose__ctor_m590E654772BD238DC4D6B1AFE1C8797C8578AB1E (void);
// 0x00000B16 System.Void NPOI.HSSF.Util.HSSFColor_Rose::.cctor()
extern void Rose__cctor_m1017DDF96ED78FAB1833904D0C3681C9B2F77FB9 (void);
// 0x00000B17 System.Void NPOI.HSSF.Util.HSSFColor_Tan::.ctor()
extern void Tan__ctor_m8137E897A94AF1C1B7E1B15E724B95778CB32556 (void);
// 0x00000B18 System.Void NPOI.HSSF.Util.HSSFColor_Tan::.cctor()
extern void Tan__cctor_m25493A292077F53628E65ED357CBBAABBD5BD0D8 (void);
// 0x00000B19 System.Void NPOI.HSSF.Util.HSSFColor_LightYellow::.ctor()
extern void LightYellow__ctor_m5054C67D7AB413A95F45BB6E8A8563F2632D4428 (void);
// 0x00000B1A System.Void NPOI.HSSF.Util.HSSFColor_LightYellow::.cctor()
extern void LightYellow__cctor_mF53D2A47DE9A36DEA5B61C16C637D6FB55346050 (void);
// 0x00000B1B System.Void NPOI.HSSF.Util.HSSFColor_LightGreen::.ctor()
extern void LightGreen__ctor_m0F2711909BF35AEE56E28385965181723F1644D9 (void);
// 0x00000B1C System.Void NPOI.HSSF.Util.HSSFColor_LightGreen::.cctor()
extern void LightGreen__cctor_mF2969F13D1C63D4779C5C59F5FF738D11A04BF5B (void);
// 0x00000B1D System.Void NPOI.HSSF.Util.HSSFColor_LightTurquoise::.ctor()
extern void LightTurquoise__ctor_m41A011FE8ADCD74084E2018307EEF0B2C3AE8C99 (void);
// 0x00000B1E System.Void NPOI.HSSF.Util.HSSFColor_LightTurquoise::.cctor()
extern void LightTurquoise__cctor_m7C2F990E1BCC1D3C6550699AE877B7D8C01064A6 (void);
// 0x00000B1F System.Void NPOI.HSSF.Util.HSSFColor_PaleBlue::.ctor()
extern void PaleBlue__ctor_mF9AED760C2C1390068CC62E50430066A85504FA7 (void);
// 0x00000B20 System.Void NPOI.HSSF.Util.HSSFColor_PaleBlue::.cctor()
extern void PaleBlue__cctor_mBD247D98D60511655EAE83D23ECB8A8AABAB620F (void);
// 0x00000B21 System.Void NPOI.HSSF.Util.HSSFColor_Lavender::.ctor()
extern void Lavender__ctor_m9237CEAA3A8F5D849DF35A8FF1577F7439F3FC36 (void);
// 0x00000B22 System.Void NPOI.HSSF.Util.HSSFColor_Lavender::.cctor()
extern void Lavender__cctor_mCAF182E57369046A56190A7844081FB208A299FA (void);
// 0x00000B23 System.Void NPOI.HSSF.Util.HSSFColor_White::.ctor()
extern void White__ctor_mC52C81F7A6448439687A43C57EA3D2DCD722B63B (void);
// 0x00000B24 System.Void NPOI.HSSF.Util.HSSFColor_White::.cctor()
extern void White__cctor_m9482336456A16DBA2381B6865A29D4DD996A86AB (void);
// 0x00000B25 System.Void NPOI.HSSF.Util.HSSFColor_CornflowerBlue::.ctor()
extern void CornflowerBlue__ctor_mB73855045DA56BBA94176D61451AFA66B52CBF4F (void);
// 0x00000B26 System.Void NPOI.HSSF.Util.HSSFColor_CornflowerBlue::.cctor()
extern void CornflowerBlue__cctor_m4DE055505B5AA20E950418B66F509D8DD2BF5771 (void);
// 0x00000B27 System.Void NPOI.HSSF.Util.HSSFColor_LemonChiffon::.ctor()
extern void LemonChiffon__ctor_m3D1A00D485001BB20AB984F6A73B340752873A79 (void);
// 0x00000B28 System.Void NPOI.HSSF.Util.HSSFColor_LemonChiffon::.cctor()
extern void LemonChiffon__cctor_m1A6AED69F1E0CAF685DB38B07BD36A35256A523C (void);
// 0x00000B29 System.Void NPOI.HSSF.Util.HSSFColor_Maroon::.ctor()
extern void Maroon__ctor_mAB56B99ACE15250932291862FF2BAE2E2B7B361C (void);
// 0x00000B2A System.Void NPOI.HSSF.Util.HSSFColor_Maroon::.cctor()
extern void Maroon__cctor_m70E409BF0E304D6B1E9B62F94F52E7E85358EE0F (void);
// 0x00000B2B System.Void NPOI.HSSF.Util.HSSFColor_Orchid::.ctor()
extern void Orchid__ctor_m1E8F180B5DD78F138B6A574E07D950C9B9C93F81 (void);
// 0x00000B2C System.Void NPOI.HSSF.Util.HSSFColor_Orchid::.cctor()
extern void Orchid__cctor_m1E0B1867791D66820AAA230CE662B5A372C24A9C (void);
// 0x00000B2D System.Void NPOI.HSSF.Util.HSSFColor_Coral::.ctor()
extern void Coral__ctor_m8B852E22B40F60BDD3713A77CE38B63D7932DE30 (void);
// 0x00000B2E System.Void NPOI.HSSF.Util.HSSFColor_Coral::.cctor()
extern void Coral__cctor_mDD14C3A52871DA017E6542106474F915F092207F (void);
// 0x00000B2F System.Void NPOI.HSSF.Util.HSSFColor_RoyalBlue::.ctor()
extern void RoyalBlue__ctor_m7533D174EBEEFE201060CECC9D5E6FEFE71CC352 (void);
// 0x00000B30 System.Void NPOI.HSSF.Util.HSSFColor_RoyalBlue::.cctor()
extern void RoyalBlue__cctor_m8CE982F25FDD91B3B55C25FDD07CE8AEBDCA962C (void);
// 0x00000B31 System.Void NPOI.HSSF.Util.HSSFColor_LightCornflowerBlue::.ctor()
extern void LightCornflowerBlue__ctor_m129E612D5EC328AA330E2115B2879639CFFF01DA (void);
// 0x00000B32 System.Void NPOI.HSSF.Util.HSSFColor_LightCornflowerBlue::.cctor()
extern void LightCornflowerBlue__cctor_mC43AFEC84272469E3048D95E3A76484C82DFD104 (void);
// 0x00000B33 System.Void NPOI.HSSF.Util.HSSFColor_Automatic::.ctor()
extern void Automatic__ctor_m7C94DBEDE6650C6804F561E682E02FB3B0F4CD12 (void);
// 0x00000B34 System.Void NPOI.HSSF.Util.HSSFColor_Automatic::.cctor()
extern void Automatic__cctor_mAA31F16D5EF41AD157913F2BC17B9549F497C307 (void);
// 0x00000B35 System.Void NPOI.HSSF.UserModel.HSSFPatriarch::PreSerialize()
extern void HSSFPatriarch_PreSerialize_mB489B53C42B563088E24C27182002F4A17C5F20C (void);
// 0x00000B36 System.Void NPOI.HSSF.UserModel.HSSFPatriarch::.cctor()
extern void HSSFPatriarch__cctor_m74575E7F24D70A849D2E48343B7A3312A2FFBC1F (void);
// 0x00000B37 System.String NPOI.SS.UserModel.IRichTextString::get_String()
// 0x00000B38 System.Int32 NPOI.SS.UserModel.IRichTextString::get_Length()
// 0x00000B39 System.Int32 NPOI.SS.UserModel.IRichTextString::get_NumFormattingRuns()
// 0x00000B3A System.Int32 NPOI.SS.UserModel.IRichTextString::GetIndexOfFormattingRun(System.Int32)
// 0x00000B3B System.Void NPOI.HSSF.UserModel.HSSFRichTextString::.ctor()
extern void HSSFRichTextString__ctor_m71088321C30F2A6B02DBD10E04A5FAB8CC9FB313 (void);
// 0x00000B3C System.Void NPOI.HSSF.UserModel.HSSFRichTextString::.ctor(System.String)
extern void HSSFRichTextString__ctor_mF4B27CA41A5A5010EF3ADD105A5F6EE4FCBE1A92 (void);
// 0x00000B3D System.Void NPOI.HSSF.UserModel.HSSFRichTextString::.ctor(NPOI.HSSF.Model.InternalWorkbook,NPOI.HSSF.Record.LabelSSTRecord)
extern void HSSFRichTextString__ctor_m2DDDE3EF3F0A97D49100FC43FB916883400F0FA3 (void);
// 0x00000B3E System.Void NPOI.HSSF.UserModel.HSSFRichTextString::SetWorkbookReferences(NPOI.HSSF.Model.InternalWorkbook,NPOI.HSSF.Record.LabelSSTRecord)
extern void HSSFRichTextString_SetWorkbookReferences_mCDAD5BA9648B1F0616139F2D18543D44422A708F (void);
// 0x00000B3F NPOI.HSSF.Record.UnicodeString NPOI.HSSF.UserModel.HSSFRichTextString::CloneStringIfRequired()
extern void HSSFRichTextString_CloneStringIfRequired_m64A7224527955C03191C7089CE5A47E212C6E30D (void);
// 0x00000B40 System.String NPOI.HSSF.UserModel.HSSFRichTextString::get_String()
extern void HSSFRichTextString_get_String_m8895E76FB566F7CFFF11A9BB8800A56D44C5F3C6 (void);
// 0x00000B41 NPOI.HSSF.Record.UnicodeString NPOI.HSSF.UserModel.HSSFRichTextString::get_UnicodeString()
extern void HSSFRichTextString_get_UnicodeString_m642C0FFEBB0679B545C8BDD9DA44E1994DEE4066 (void);
// 0x00000B42 System.Void NPOI.HSSF.UserModel.HSSFRichTextString::set_UnicodeString(NPOI.HSSF.Record.UnicodeString)
extern void HSSFRichTextString_set_UnicodeString_mB5D0ADE580DEC104438137E4EDDEC9B33066C1F3 (void);
// 0x00000B43 System.Int32 NPOI.HSSF.UserModel.HSSFRichTextString::get_Length()
extern void HSSFRichTextString_get_Length_m1597AE1AA654328303118D97C24CA569FEC75C50 (void);
// 0x00000B44 System.Int32 NPOI.HSSF.UserModel.HSSFRichTextString::get_NumFormattingRuns()
extern void HSSFRichTextString_get_NumFormattingRuns_mEFD1877EFAC3A6B0348DC6406B8EF71F73A35B74 (void);
// 0x00000B45 System.Int32 NPOI.HSSF.UserModel.HSSFRichTextString::GetIndexOfFormattingRun(System.Int32)
extern void HSSFRichTextString_GetIndexOfFormattingRun_mFC25F86A5B740F1A3902AEEE1115F0FCE7BEEEEF (void);
// 0x00000B46 System.Int16 NPOI.HSSF.UserModel.HSSFRichTextString::GetFontOfFormattingRun(System.Int32)
extern void HSSFRichTextString_GetFontOfFormattingRun_mB6E823B6128450B96D542A117897799F3289B6FA (void);
// 0x00000B47 System.Int32 NPOI.HSSF.UserModel.HSSFRichTextString::CompareTo(NPOI.HSSF.UserModel.HSSFRichTextString)
extern void HSSFRichTextString_CompareTo_m95E8FCC11C5CD47D3EA9CE456A33A71C53BCA53A (void);
// 0x00000B48 System.Boolean NPOI.HSSF.UserModel.HSSFRichTextString::Equals(System.Object)
extern void HSSFRichTextString_Equals_mB06AA50B99B4A629EC7DC7ACE54D3935E8C82B7B (void);
// 0x00000B49 System.Int32 NPOI.HSSF.UserModel.HSSFRichTextString::GetHashCode()
extern void HSSFRichTextString_GetHashCode_m90EB6400F31E355B74FE2338BF8224CAE9AB045F (void);
// 0x00000B4A System.String NPOI.HSSF.UserModel.HSSFRichTextString::ToString()
extern void HSSFRichTextString_ToString_mCFFDD602B119A5682549C31E3559CCBE585EBB8A (void);
// 0x00000B4B NPOI.SS.UserModel.ICell NPOI.SS.UserModel.IRow::CreateCell(System.Int32)
// 0x00000B4C NPOI.SS.UserModel.ICell NPOI.SS.UserModel.IRow::GetCell(System.Int32)
// 0x00000B4D NPOI.SS.UserModel.ISheet NPOI.SS.UserModel.IRow::get_Sheet()
// 0x00000B4E System.Void NPOI.HSSF.UserModel.HSSFRow::.ctor()
extern void HSSFRow__ctor_m0B676076B68D85DDF13618D42941CAE6CD3D9296 (void);
// 0x00000B4F System.Void NPOI.HSSF.UserModel.HSSFRow::.ctor(NPOI.HSSF.UserModel.HSSFWorkbook,NPOI.HSSF.UserModel.HSSFSheet,System.Int32)
extern void HSSFRow__ctor_m7994354160DB140555E8392B5626365249CD3379 (void);
// 0x00000B50 System.Void NPOI.HSSF.UserModel.HSSFRow::.ctor(NPOI.HSSF.UserModel.HSSFWorkbook,NPOI.HSSF.UserModel.HSSFSheet,NPOI.HSSF.Record.RowRecord)
extern void HSSFRow__ctor_m364D0A74DB207BF79BC61B4C81A070AF4E9A9E16 (void);
// 0x00000B51 NPOI.SS.UserModel.ICell NPOI.HSSF.UserModel.HSSFRow::CreateCell(System.Int32)
extern void HSSFRow_CreateCell_m46627A68A40CB4E628FDDE861484BBEA2DA45C64 (void);
// 0x00000B52 NPOI.SS.UserModel.ICell NPOI.HSSF.UserModel.HSSFRow::CreateCell(System.Int32,NPOI.SS.UserModel.CellType)
extern void HSSFRow_CreateCell_m046067534F3AE3AADE0E9A7AEEAAEAA095A48368 (void);
// 0x00000B53 NPOI.SS.UserModel.ICell NPOI.HSSF.UserModel.HSSFRow::CreateCellFromRecord(NPOI.HSSF.Record.CellValueRecordInterface)
extern void HSSFRow_CreateCellFromRecord_m861666F8B98FEC9C6F18379842E68D6C3E560E64 (void);
// 0x00000B54 System.Int32 NPOI.HSSF.UserModel.HSSFRow::get_RowNum()
extern void HSSFRow_get_RowNum_mA41D0120D7BACD29A3F4FE622C5EDB0D273EB58B (void);
// 0x00000B55 System.Void NPOI.HSSF.UserModel.HSSFRow::set_RowNum(System.Int32)
extern void HSSFRow_set_RowNum_m112F982D83AFB39A9DE645C76E26CBD1B5523278 (void);
// 0x00000B56 NPOI.SS.UserModel.ISheet NPOI.HSSF.UserModel.HSSFRow::get_Sheet()
extern void HSSFRow_get_Sheet_m4D77D7C1D71D9FB409C0FBCE138679EE59A755ED (void);
// 0x00000B57 System.Void NPOI.HSSF.UserModel.HSSFRow::AddCell(NPOI.SS.UserModel.ICell)
extern void HSSFRow_AddCell_m28BA5A7BD67CB28979743A191E6675E2A5B0A751 (void);
// 0x00000B58 NPOI.SS.UserModel.ICell NPOI.HSSF.UserModel.HSSFRow::RetrieveCell(System.Int32)
extern void HSSFRow_RetrieveCell_m22F34A94D24E4BFD5302B4B1E47DA8031B8D963B (void);
// 0x00000B59 NPOI.SS.UserModel.ICell NPOI.HSSF.UserModel.HSSFRow::GetCell(System.Int32)
extern void HSSFRow_GetCell_m63348D625B6D11D49C95CE0D801CBFE5F65FFA78 (void);
// 0x00000B5A NPOI.SS.UserModel.ICell NPOI.HSSF.UserModel.HSSFRow::GetCell(System.Int32,NPOI.SS.UserModel.MissingCellPolicy)
extern void HSSFRow_GetCell_mD19C4636BE3893593FED378AA93E791ADCC7770E (void);
// 0x00000B5B System.Void NPOI.HSSF.UserModel.HSSFRow::set_Height(System.Int16)
extern void HSSFRow_set_Height_m480C6EAD7D8E52500A6972A487C0A9EBC7CDEDBB (void);
// 0x00000B5C NPOI.HSSF.Record.RowRecord NPOI.HSSF.UserModel.HSSFRow::get_RowRecord()
extern void HSSFRow_get_RowRecord_m235649A9E68AF661A21A3EF0A582DA69B09A8FEB (void);
// 0x00000B5D System.Int32 NPOI.HSSF.UserModel.HSSFRow::CompareTo(System.Object)
extern void HSSFRow_CompareTo_m9482EE7D29A72AD4B5D9585E90FCBB518263BCFC (void);
// 0x00000B5E System.Boolean NPOI.HSSF.UserModel.HSSFRow::Equals(System.Object)
extern void HSSFRow_Equals_m432D0ED96FC1ABEE378F167AE327B2620C561330 (void);
// 0x00000B5F System.Int32 NPOI.HSSF.UserModel.HSSFRow::GetHashCode()
extern void HSSFRow_GetHashCode_mDF6F7C58476DCE598EE98C031D54E78FE453AE6F (void);
// 0x00000B60 NPOI.SS.UserModel.IRow NPOI.SS.UserModel.ISheet::GetRow(System.Int32)
// 0x00000B61 NPOI.SS.UserModel.IWorkbook NPOI.SS.UserModel.ISheet::get_Workbook()
// 0x00000B62 NPOI.SS.UserModel.ICellRange`1<NPOI.SS.UserModel.ICell> NPOI.SS.UserModel.ISheet::RemoveArrayFormula(NPOI.SS.UserModel.ICell)
// 0x00000B63 System.Void NPOI.HSSF.UserModel.HSSFSheet::.ctor(NPOI.HSSF.UserModel.HSSFWorkbook,NPOI.HSSF.Model.InternalSheet)
extern void HSSFSheet__ctor_m0AFB95C0772A8DA991304D7FCE66193BB7524E8A (void);
// 0x00000B64 System.Void NPOI.HSSF.UserModel.HSSFSheet::PreSerialize()
extern void HSSFSheet_PreSerialize_m7F9AE9DE585444CB3EF31CDA0D1B60F2FF6584A3 (void);
// 0x00000B65 System.Void NPOI.HSSF.UserModel.HSSFSheet::SetPropertiesFromSheet(NPOI.HSSF.Model.InternalSheet)
extern void HSSFSheet_SetPropertiesFromSheet_mEF4336DA61118CDF0E5D2045A8969155A103DCFE (void);
// 0x00000B66 NPOI.SS.UserModel.IRow NPOI.HSSF.UserModel.HSSFSheet::CreateRow(System.Int32)
extern void HSSFSheet_CreateRow_m9391164B1E0C568EA2D7201A6B3395B43070A5C9 (void);
// 0x00000B67 NPOI.HSSF.UserModel.HSSFRow NPOI.HSSF.UserModel.HSSFSheet::CreateRowFromRecord(NPOI.HSSF.Record.RowRecord)
extern void HSSFSheet_CreateRowFromRecord_m95393737A4982877B32D263E7707A5C32EAA5CC7 (void);
// 0x00000B68 System.Void NPOI.HSSF.UserModel.HSSFSheet::AddRow(NPOI.HSSF.UserModel.HSSFRow,System.Boolean)
extern void HSSFSheet_AddRow_m6992B0DEB336F5EA4F1CE78D1AB31767AF42BFE3 (void);
// 0x00000B69 NPOI.SS.UserModel.IRow NPOI.HSSF.UserModel.HSSFSheet::GetRow(System.Int32)
extern void HSSFSheet_GetRow_m47EBB212C8B4B769D66867260803D177774CA5DE (void);
// 0x00000B6A System.Int32 NPOI.HSSF.UserModel.HSSFSheet::get_FirstRowNum()
extern void HSSFSheet_get_FirstRowNum_mAAF5136FF708F515D74A9472F0C6F43B970B5EBC (void);
// 0x00000B6B System.Int32 NPOI.HSSF.UserModel.HSSFSheet::get_LastRowNum()
extern void HSSFSheet_get_LastRowNum_m6934E14ADD3A57F54FA2F5A63AC7581362A8EF8D (void);
// 0x00000B6C System.Int16 NPOI.HSSF.UserModel.HSSFSheet::get_DefaultRowHeight()
extern void HSSFSheet_get_DefaultRowHeight_mFFEB31E238D296C0BE43AED40655CF23749B2223 (void);
// 0x00000B6D NPOI.HSSF.Model.InternalSheet NPOI.HSSF.UserModel.HSSFSheet::get_Sheet()
extern void HSSFSheet_get_Sheet_m80231677F6B929806333500F6F520AFF273AA69A (void);
// 0x00000B6E NPOI.SS.UserModel.ICellRange`1<NPOI.SS.UserModel.ICell> NPOI.HSSF.UserModel.HSSFSheet::RemoveArrayFormula(NPOI.SS.UserModel.ICell)
extern void HSSFSheet_RemoveArrayFormula_m22627DE153AA74C73659AC553B5C503892884EF4 (void);
// 0x00000B6F NPOI.SS.UserModel.ICellRange`1<NPOI.SS.UserModel.ICell> NPOI.HSSF.UserModel.HSSFSheet::GetCellRange(NPOI.SS.Util.CellRangeAddress)
extern void HSSFSheet_GetCellRange_mD3147B9EEA09E4FCCC9B87F9BE85DD196229B4A8 (void);
// 0x00000B70 NPOI.SS.UserModel.IWorkbook NPOI.HSSF.UserModel.HSSFSheet::get_Workbook()
extern void HSSFSheet_get_Workbook_m9C48D069F02C86975FB70B7878CC70921C6A5CF6 (void);
// 0x00000B71 System.Void NPOI.HSSF.Util.GUID::.ctor(System.Int32,System.Int32,System.Int32,System.Int64)
extern void GUID__ctor_mD8794259110005CF8CF5E810DA97A01F244C7C33 (void);
// 0x00000B72 System.Void NPOI.HSSF.Util.GUID::Serialize(NPOI.Util.ILittleEndianOutput)
extern void GUID_Serialize_mC78A54D48026683221ECD7B19F4D298110D65140 (void);
// 0x00000B73 System.Boolean NPOI.HSSF.Util.GUID::Equals(System.Object)
extern void GUID_Equals_mD914C6C5EFB6AA7C119E98A69C0E16A3F7ABE532 (void);
// 0x00000B74 System.Int32 NPOI.HSSF.Util.GUID::GetHashCode()
extern void GUID_GetHashCode_m9290B718E23C76117399F2CB97A9CE576989D1F2 (void);
// 0x00000B75 System.Int64 NPOI.HSSF.Util.GUID::get_D4()
extern void GUID_get_D4_m80650DD474C19C9AC8C3032547E4C5DB268AF740 (void);
// 0x00000B76 System.String NPOI.HSSF.Util.GUID::FormatAsString()
extern void GUID_FormatAsString_mE04212B995A45FBC1B952EF4424445E47832CB75 (void);
// 0x00000B77 System.String NPOI.HSSF.Util.GUID::ToString()
extern void GUID_ToString_m7261078AD406749A876E28DDF30D61FF5A8E7A6A (void);
// 0x00000B78 NPOI.HSSF.Util.GUID NPOI.HSSF.Util.GUID::Parse(System.String)
extern void GUID_Parse_m961CCB29EAA4A49EAA446E0E606656307F5F4C39 (void);
// 0x00000B79 System.Int64 NPOI.HSSF.Util.GUID::ParseLELong(System.Char[],System.Int32)
extern void GUID_ParseLELong_m6F2CDBF227864DEF0AA64E88451A2643C7667B32 (void);
// 0x00000B7A System.Int32 NPOI.HSSF.Util.GUID::ParseShort(System.Char[],System.Int32)
extern void GUID_ParseShort_mD336D1DFC45B21A14CBC7ABA4C12E142ADF6E007 (void);
// 0x00000B7B System.Int32 NPOI.HSSF.Util.GUID::ParseHexChar(System.Char)
extern void GUID_ParseHexChar_mC153A244B167B8A1D6807D62DE66D5B0BF11AF02 (void);
// 0x00000B7C System.Double NPOI.HSSF.Util.RKUtil::DecodeNumber(System.Int32)
extern void RKUtil_DecodeNumber_mACAE8CA8D4C38C21AEDDA311B00091231EC989F8 (void);
// 0x00000B7D System.Void NPOI.SS.Formula.Atp.NotImplemented::.ctor(System.String)
extern void NotImplemented__ctor_m494F2A3B87FE13DA199789FA345AF0D00DE68B87 (void);
// 0x00000B7E NPOI.SS.Formula.Functions.FreeRefFunction NPOI.SS.Formula.Udf.UDFFinder::FindFunction(System.String)
// 0x00000B7F System.Void NPOI.SS.Formula.Udf.UDFFinder::.ctor()
extern void UDFFinder__ctor_m6B1A81999F40EB7C806C35DE4E483BD6E9924EA0 (void);
// 0x00000B80 System.Void NPOI.SS.Formula.Udf.UDFFinder::.cctor()
extern void UDFFinder__cctor_m856F2A16BAAD1E6C3CB7517B1068BED85C955793 (void);
// 0x00000B81 System.Void NPOI.SS.Formula.Atp.AnalysisToolPak::.ctor()
extern void AnalysisToolPak__ctor_mD4B3D7DB65DEC35A8E26B743B14FA19ED6D7FAE5 (void);
// 0x00000B82 NPOI.SS.Formula.Functions.FreeRefFunction NPOI.SS.Formula.Atp.AnalysisToolPak::FindFunction(System.String)
extern void AnalysisToolPak_FindFunction_m17D7CF066843CD14D7E53887133ACD816224CC75 (void);
// 0x00000B83 System.Collections.Hashtable NPOI.SS.Formula.Atp.AnalysisToolPak::CreateFunctionsMap()
extern void AnalysisToolPak_CreateFunctionsMap_mC3C10A533C6DF9CAC0320A6F4059E8C88BF00519 (void);
// 0x00000B84 System.Void NPOI.SS.Formula.Atp.AnalysisToolPak::r(System.Collections.Hashtable,System.String,NPOI.SS.Formula.Functions.FreeRefFunction)
extern void AnalysisToolPak_r_m5CDA787762F97C907B03FCC7FB0E4E0A010A6E92 (void);
// 0x00000B85 System.Void NPOI.SS.Formula.Atp.AnalysisToolPak::.cctor()
extern void AnalysisToolPak__cctor_mE939623F627F2F8A833489AA39D03A8699583791 (void);
// 0x00000B86 System.Void NPOI.SS.Formula.Atp.MRound::.ctor()
extern void MRound__ctor_mC544B878B44E3DF454C5656CCE10BF302CDE7A56 (void);
// 0x00000B87 System.Void NPOI.SS.Formula.Atp.MRound::.cctor()
extern void MRound__cctor_m0C169AD7856FF03BF948CD61355B0FEA2A8F9B66 (void);
// 0x00000B88 System.Void NPOI.SS.Formula.Atp.ParityFunction::.ctor(System.Int32)
extern void ParityFunction__ctor_m700DD79DDB8C9810E73BB5507D2B8A302A082B14 (void);
// 0x00000B89 System.Void NPOI.SS.Formula.Atp.ParityFunction::.cctor()
extern void ParityFunction__cctor_m3D69D69C57E3092D493BC19FD72B822F304AA12D (void);
// 0x00000B8A System.Void NPOI.SS.Formula.Atp.RandBetween::.ctor()
extern void RandBetween__ctor_mB0B2C846DEA4B6A890123CAABB21DFA6CADF7C0C (void);
// 0x00000B8B System.Void NPOI.SS.Formula.Atp.RandBetween::.cctor()
extern void RandBetween__cctor_m942D1940B989EFD943C5B5BCEC875215A0EBCA36 (void);
// 0x00000B8C System.Void NPOI.SS.Formula.Atp.YearFrac::.ctor()
extern void YearFrac__ctor_m3E2A445742AF7CD1F301B5FA6C4D5E09AF0B2143 (void);
// 0x00000B8D System.Void NPOI.SS.Formula.Atp.YearFrac::.cctor()
extern void YearFrac__cctor_m85E4E9C77D268B74A5572EEFB60EAF2DF1F9C111 (void);
// 0x00000B8E System.Void NPOI.SS.Formula.ExternalSheet::.ctor(System.String,System.String)
extern void ExternalSheet__ctor_m1E70E35D8E412FEC85067FC763C428B771F4035E (void);
// 0x00000B8F System.String NPOI.SS.Formula.ExternalSheet::GetWorkbookName()
extern void ExternalSheet_GetWorkbookName_m9BC3E8AF26978A59658B1AB8D3B9AD14D2DE8AE2 (void);
// 0x00000B90 System.String NPOI.SS.Formula.ExternalSheet::GetSheetName()
extern void ExternalSheet_GetSheetName_mFE68F76ADC42B35AEBE02A4B9F625E2D577CBCC7 (void);
// 0x00000B91 System.String NPOI.SS.Formula.Eval.ErrorEval::GetText(System.Int32)
extern void ErrorEval_GetText_m63C81B63A1B452E7C6A87C061DE91583F196B61A (void);
// 0x00000B92 System.Void NPOI.SS.Formula.Eval.ErrorEval::.ctor(System.Int32)
extern void ErrorEval__ctor_m08B9E060855FAD013187DD83659A8EE582522324 (void);
// 0x00000B93 System.String NPOI.SS.Formula.Eval.ErrorEval::ToString()
extern void ErrorEval_ToString_m4B1958710E095087E730C4DFB073783C8C250BCA (void);
// 0x00000B94 System.Void NPOI.SS.Formula.Eval.ErrorEval::.cctor()
extern void ErrorEval__cctor_m7E285F5ADC2EB3647E42E808FC108BC8BCF3B93D (void);
// 0x00000B95 System.Void NPOI.SS.Formula.Formula::.ctor(System.Byte[],System.Int32)
extern void Formula__ctor_m0246B5728D58DAF8226B3387225554B488732B39 (void);
// 0x00000B96 NPOI.SS.Formula.PTG.Ptg[] NPOI.SS.Formula.Formula::get_Tokens()
extern void Formula_get_Tokens_mC05CA4973D84C36873FD405BAAF829E5275A3B63 (void);
// 0x00000B97 System.Void NPOI.SS.Formula.Formula::Serialize(NPOI.Util.ILittleEndianOutput)
extern void Formula_Serialize_mD3A508B900B5DE6EBECA3086AAA96C747C34A292 (void);
// 0x00000B98 System.Void NPOI.SS.Formula.Formula::SerializeTokens(NPOI.Util.ILittleEndianOutput)
extern void Formula_SerializeTokens_m93B761AE47D8B8B5141EC694F5299C646C8F1C98 (void);
// 0x00000B99 System.Void NPOI.SS.Formula.Formula::SerializeArrayConstantData(NPOI.Util.ILittleEndianOutput)
extern void Formula_SerializeArrayConstantData_mF5C16E2AECBF411F334CE0B6DF967E8F69DD29D0 (void);
// 0x00000B9A System.Int32 NPOI.SS.Formula.Formula::get_EncodedSize()
extern void Formula_get_EncodedSize_m66258B72D883D70341F3739406B1E96D3B64FEDF (void);
// 0x00000B9B System.Int32 NPOI.SS.Formula.Formula::get_EncodedTokenSize()
extern void Formula_get_EncodedTokenSize_m65FE0DEBAB856D632212DF80037913CDBE3ADA83 (void);
// 0x00000B9C NPOI.SS.Formula.Formula NPOI.SS.Formula.Formula::Create(NPOI.SS.Formula.PTG.Ptg[])
extern void Formula_Create_m5B3B3C53127C08B08CFDE7EC60F041A91CA464D6 (void);
// 0x00000B9D NPOI.SS.Formula.Formula NPOI.SS.Formula.Formula::Copy()
extern void Formula_Copy_m0B4D06570AFB73CAEF6B79B9ED5ED019B22F4DBD (void);
// 0x00000B9E NPOI.SS.Util.CellReference NPOI.SS.Formula.Formula::get_ExpReference()
extern void Formula_get_ExpReference_mA47CA19BBA03C7CF591502CC95AE950B4653DE78 (void);
// 0x00000B9F System.Void NPOI.SS.Formula.Formula::.cctor()
extern void Formula__cctor_mEFB43DA0402613F3976F9FA3FE5DC169E67E3AC2 (void);
// 0x00000BA0 System.Void NPOI.SS.Formula.FormulaParseException::.ctor(System.String)
extern void FormulaParseException__ctor_mC48145AED34991A6705ADF32650C4042BAA1CA84 (void);
// 0x00000BA1 System.Void NPOI.SS.Formula.FormulaParser::.ctor(System.String,NPOI.SS.Formula.IFormulaParsingWorkbook,System.Int32)
extern void FormulaParser__ctor_mC585BE1D888EB7B82DA486A0E3A93C3EBC3CCFF0 (void);
// 0x00000BA2 NPOI.SS.Formula.PTG.Ptg[] NPOI.SS.Formula.FormulaParser::Parse(System.String,NPOI.SS.Formula.IFormulaParsingWorkbook,NPOI.SS.Formula.FormulaType,System.Int32)
extern void FormulaParser_Parse_m0A1C0DF9CCEE8E5FE753A0A281E411C583D9580F (void);
// 0x00000BA3 System.Void NPOI.SS.Formula.FormulaParser::GetChar()
extern void FormulaParser_GetChar_mEA6B357BDCD946FE6AB521F4D1DDAAE2F3F6233A (void);
// 0x00000BA4 System.Exception NPOI.SS.Formula.FormulaParser::expected(System.String)
extern void FormulaParser_expected_m11712A67E51FF3FA6D349837013E60B6E794BE41 (void);
// 0x00000BA5 System.Boolean NPOI.SS.Formula.FormulaParser::IsAlpha(System.Char)
extern void FormulaParser_IsAlpha_m6C11155787A5DC9036E0B51CB1C73E17A0FFF36E (void);
// 0x00000BA6 System.Boolean NPOI.SS.Formula.FormulaParser::IsDigit(System.Char)
extern void FormulaParser_IsDigit_m8B56B2E7DDEC7BA5C150E16FE826C5A662E7E442 (void);
// 0x00000BA7 System.Boolean NPOI.SS.Formula.FormulaParser::IsWhite(System.Char)
extern void FormulaParser_IsWhite_m58837A75FA520F9E98EEE7937399B64CCD73129F (void);
// 0x00000BA8 System.Void NPOI.SS.Formula.FormulaParser::SkipWhite()
extern void FormulaParser_SkipWhite_m25E7790158D711C03D5048DFCBF4EEAE4AFDC701 (void);
// 0x00000BA9 System.Void NPOI.SS.Formula.FormulaParser::Match(System.Char)
extern void FormulaParser_Match_m9BA1B07BD8CF8102A7617EE6883391D976714346 (void);
// 0x00000BAA System.String NPOI.SS.Formula.FormulaParser::ParseUnquotedIdentifier()
extern void FormulaParser_ParseUnquotedIdentifier_mC593242B5FAEC4ED7676E638BE35A995887DC494 (void);
// 0x00000BAB System.String NPOI.SS.Formula.FormulaParser::GetNum()
extern void FormulaParser_GetNum_m9AE7F13683C172A275E5DB79A30E2309A8DA0DFD (void);
// 0x00000BAC NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::ParseRangeExpression()
extern void FormulaParser_ParseRangeExpression_m0EBD053994FB6F1DDF66EE4652D30525678E4416 (void);
// 0x00000BAD NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::AugmentWithMemPtg(NPOI.SS.Formula.ParseNode)
extern void FormulaParser_AugmentWithMemPtg_mB6E528CC01900E0A4B3CC3F114A21939A6844D07 (void);
// 0x00000BAE System.Boolean NPOI.SS.Formula.FormulaParser::NeedsMemFunc(NPOI.SS.Formula.ParseNode)
extern void FormulaParser_NeedsMemFunc_m9CDFB2AC99217197351FAD9BBA7C0CC5DF13BA2A (void);
// 0x00000BAF System.Boolean NPOI.SS.Formula.FormulaParser::IsValidDefinedNameChar(System.Char)
extern void FormulaParser_IsValidDefinedNameChar_mDD15E5B8CD4E0224EEE1AAC0C5803024F38AF8A8 (void);
// 0x00000BB0 System.Void NPOI.SS.Formula.FormulaParser::CheckValidRangeOperand(System.String,System.Int32,NPOI.SS.Formula.ParseNode)
extern void FormulaParser_CheckValidRangeOperand_m7CD1F37361330D27B661C7C3AA0215ACA6624DEF (void);
// 0x00000BB1 System.Boolean NPOI.SS.Formula.FormulaParser::IsValidRangeOperand(NPOI.SS.Formula.ParseNode)
extern void FormulaParser_IsValidRangeOperand_m650F3651F3F7289B72B6BC01B42E07F6F3362929 (void);
// 0x00000BB2 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::ParseRangeable()
extern void FormulaParser_ParseRangeable_m2DB9927748CDBDDC17D6D65AEA9A569BDD6F2433 (void);
// 0x00000BB3 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::ParseNonRange(System.Int32)
extern void FormulaParser_ParseNonRange_m1255CDF4D37F8A0956A8571E0DE0264F649EC056 (void);
// 0x00000BB4 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::CreateAreaRefParseNode(NPOI.SS.Formula.FormulaParser_SheetIdentifier,NPOI.SS.Formula.FormulaParser_SimpleRangePart,NPOI.SS.Formula.FormulaParser_SimpleRangePart)
extern void FormulaParser_CreateAreaRefParseNode_m6FCB69D23B2681D810CB3BD3A176F9590A6B91EF (void);
// 0x00000BB5 NPOI.SS.Util.AreaReference NPOI.SS.Formula.FormulaParser::CreateAreaRef(NPOI.SS.Formula.FormulaParser_SimpleRangePart,NPOI.SS.Formula.FormulaParser_SimpleRangePart)
extern void FormulaParser_CreateAreaRef_mE5E64844E32339AA10E70C319A6C7BD1C0044BD4 (void);
// 0x00000BB6 NPOI.SS.Formula.FormulaParser_SimpleRangePart NPOI.SS.Formula.FormulaParser::ParseSimpleRangePart()
extern void FormulaParser_ParseSimpleRangePart_m07CD0B3694FF76552A37FF5FF7859618BAF5DC9C (void);
// 0x00000BB7 NPOI.SS.Formula.FormulaParser_SheetIdentifier NPOI.SS.Formula.FormulaParser::ParseSheetName()
extern void FormulaParser_ParseSheetName_m63A519BA57EBD6AABE4AAF11498B48D835AA20E6 (void);
// 0x00000BB8 System.Boolean NPOI.SS.Formula.FormulaParser::IsUnquotedSheetNameChar(System.Char)
extern void FormulaParser_IsUnquotedSheetNameChar_m73D9E38266E8434E669CEC7D3F16F92B4EB7DCAC (void);
// 0x00000BB9 System.Void NPOI.SS.Formula.FormulaParser::ResetPointer(System.Int32)
extern void FormulaParser_ResetPointer_m2BFEF8937FE22AF3A201D6383A5A555ACC19F9FD (void);
// 0x00000BBA System.Boolean NPOI.SS.Formula.FormulaParser::IsValidCellReference(System.String)
extern void FormulaParser_IsValidCellReference_m1894BED3E7E786490B6C40C7C5F748D687CA9665 (void);
// 0x00000BBB NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::Function(System.String)
extern void FormulaParser_Function_m44AF79BFACD6CAF75F4143E043403ADB30B65F11 (void);
// 0x00000BBC NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::GetFunction(System.String,NPOI.SS.Formula.PTG.Ptg,NPOI.SS.Formula.ParseNode[])
extern void FormulaParser_GetFunction_mDACAC6E7C983706B98CF059657441E1DCBE9ADCA (void);
// 0x00000BBD System.Void NPOI.SS.Formula.FormulaParser::ValidateNumArgs(System.Int32,NPOI.SS.Formula.Function.FunctionMetadata)
extern void FormulaParser_ValidateNumArgs_m90719D56C7EEF394C91493B3E0D8DC00430A5CFE (void);
// 0x00000BBE System.Boolean NPOI.SS.Formula.FormulaParser::IsArgumentDelimiter(System.Char)
extern void FormulaParser_IsArgumentDelimiter_m0E5F8505D033CE1D731F52905FF407BAD1F4A102 (void);
// 0x00000BBF NPOI.SS.Formula.ParseNode[] NPOI.SS.Formula.FormulaParser::Arguments()
extern void FormulaParser_Arguments_mE00CCA1690A428D119B9E74023D903256F0431D9 (void);
// 0x00000BC0 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::PowerFactor()
extern void FormulaParser_PowerFactor_mD909A608D245C8A01F9C727FA838DECB0EAD5B1D (void);
// 0x00000BC1 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::PercentFactor()
extern void FormulaParser_PercentFactor_m803D8F4C596CEA8CD2DB4B1D32185EF220920470 (void);
// 0x00000BC2 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::ParseSimpleFactor()
extern void FormulaParser_ParseSimpleFactor_mEF6DCBA91736C96DB01C6C00DAB6095FEE65218C (void);
// 0x00000BC3 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::ParseUnary(System.Boolean)
extern void FormulaParser_ParseUnary_m36BC5DB44A7822D1A0586E12FEADD8BD8478DA0D (void);
// 0x00000BC4 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::ParseArray()
extern void FormulaParser_ParseArray_mFB1A1B0D199C95D1E713B1EC186AE7B7B483DB49 (void);
// 0x00000BC5 System.Void NPOI.SS.Formula.FormulaParser::CheckRowLengths(System.Object[][],System.Int32)
extern void FormulaParser_CheckRowLengths_m2F420D6A216BEB88F16649A5A47B91708A026045 (void);
// 0x00000BC6 System.Object[] NPOI.SS.Formula.FormulaParser::ParseArrayRow()
extern void FormulaParser_ParseArrayRow_m5C162202B9F0E1EBFA3CD8EE88160D3B777D2549 (void);
// 0x00000BC7 System.Object NPOI.SS.Formula.FormulaParser::ParseArrayItem()
extern void FormulaParser_ParseArrayItem_m230C1379014A997BA30F0FD045D7209103C06CF5 (void);
// 0x00000BC8 System.Boolean NPOI.SS.Formula.FormulaParser::ParseBooleanLiteral()
extern void FormulaParser_ParseBooleanLiteral_mC0AAF0D1467A12E2B915EBA7529CA484BB1E2545 (void);
// 0x00000BC9 System.Double NPOI.SS.Formula.FormulaParser::ConvertArrayNumber(NPOI.SS.Formula.PTG.Ptg,System.Boolean)
extern void FormulaParser_ConvertArrayNumber_m0F4D483B5402FBE8C1D234C51C37143283FCC7CD (void);
// 0x00000BCA NPOI.SS.Formula.PTG.Ptg NPOI.SS.Formula.FormulaParser::ParseNumber()
extern void FormulaParser_ParseNumber_mC93706E16C3BDE6AA9A2C81D0016C9CA9B65B9B2 (void);
// 0x00000BCB System.Int32 NPOI.SS.Formula.FormulaParser::ParseErrorLiteral()
extern void FormulaParser_ParseErrorLiteral_m3B483EBFB5E822CD7E7623FAE5ED6450C8891605 (void);
// 0x00000BCC NPOI.SS.Formula.PTG.Ptg NPOI.SS.Formula.FormulaParser::GetNumberPtgFromString(System.String,System.String,System.String)
extern void FormulaParser_GetNumberPtgFromString_m2749FA3328B550F911D868965A3F0541572FD816 (void);
// 0x00000BCD System.String NPOI.SS.Formula.FormulaParser::ParseStringLiteral()
extern void FormulaParser_ParseStringLiteral_m060091DF89D33C0D4EF1F38818632A75863673CC (void);
// 0x00000BCE NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::Term()
extern void FormulaParser_Term_m5356470EE912B13A0B200033E962036C1E59B853 (void);
// 0x00000BCF NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::ComparisonExpression()
extern void FormulaParser_ComparisonExpression_mCD90EAEB2EB719CF915C7AF06A2D63E6ACC9865D (void);
// 0x00000BD0 NPOI.SS.Formula.PTG.Ptg NPOI.SS.Formula.FormulaParser::GetComparisonToken()
extern void FormulaParser_GetComparisonToken_m47DA1182ED1269125C016A3E4F705EAB2B551BB2 (void);
// 0x00000BD1 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::ConcatExpression()
extern void FormulaParser_ConcatExpression_m5DA19F60E1E064168D2D9E3B2B5BA0B2AD1137E2 (void);
// 0x00000BD2 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::AdditiveExpression()
extern void FormulaParser_AdditiveExpression_mE11C8330EE92B7BC4105D6748A2ABD69090EB177 (void);
// 0x00000BD3 System.Void NPOI.SS.Formula.FormulaParser::Parse()
extern void FormulaParser_Parse_mB525F580E43656098BA8D5FAD8E4AF8601B7A1ED (void);
// 0x00000BD4 NPOI.SS.Formula.ParseNode NPOI.SS.Formula.FormulaParser::UnionExpression()
extern void FormulaParser_UnionExpression_m5C32CB1EC71F1445C018FDA43C5FD367C517991A (void);
// 0x00000BD5 NPOI.SS.Formula.PTG.Ptg[] NPOI.SS.Formula.FormulaParser::GetRPNPtg(NPOI.SS.Formula.FormulaType)
extern void FormulaParser_GetRPNPtg_m49A3E3A462EC914B75AA09AB46E084778E988043 (void);
// 0x00000BD6 System.Void NPOI.SS.Formula.FormulaParser_Identifier::.ctor(System.String,System.Boolean)
extern void Identifier__ctor_m99CC113806A08F2822E1866DD1C59BA566043792 (void);
// 0x00000BD7 System.String NPOI.SS.Formula.FormulaParser_Identifier::get_Name()
extern void Identifier_get_Name_m87836E899CAB864F693689FABCFC74947E15A425 (void);
// 0x00000BD8 System.Boolean NPOI.SS.Formula.FormulaParser_Identifier::get_IsQuoted()
extern void Identifier_get_IsQuoted_mADBA9BE13F2EEC245A31FA4DA0B31779E87DB60D (void);
// 0x00000BD9 System.String NPOI.SS.Formula.FormulaParser_Identifier::ToString()
extern void Identifier_ToString_m47550E91627B7FA8B192A8C15082E8F66C62140B (void);
// 0x00000BDA System.Void NPOI.SS.Formula.FormulaParser_SheetIdentifier::.ctor(System.String,NPOI.SS.Formula.FormulaParser_Identifier)
extern void SheetIdentifier__ctor_m6F3C6A03911242D36A487A7C0AF53602EBDC404D (void);
// 0x00000BDB System.String NPOI.SS.Formula.FormulaParser_SheetIdentifier::get_BookName()
extern void SheetIdentifier_get_BookName_m4B1D064E20C0FE813AEAB4CD6BAEC5A9609EAA97 (void);
// 0x00000BDC NPOI.SS.Formula.FormulaParser_Identifier NPOI.SS.Formula.FormulaParser_SheetIdentifier::get_SheetID()
extern void SheetIdentifier_get_SheetID_mB6005A00E2664E8888FC3DCF020DBECE112364F6 (void);
// 0x00000BDD System.String NPOI.SS.Formula.FormulaParser_SheetIdentifier::ToString()
extern void SheetIdentifier_ToString_m568F31173DE3B9F3E859FF15117FFCAD45E2D47A (void);
// 0x00000BDE NPOI.SS.Formula.FormulaParser_SimpleRangePart_PartType NPOI.SS.Formula.FormulaParser_SimpleRangePart::Get(System.Boolean,System.Boolean)
extern void SimpleRangePart_Get_m8D794BE9CF2A6D46A008E977225F0E016AB53FFD (void);
// 0x00000BDF System.Void NPOI.SS.Formula.FormulaParser_SimpleRangePart::.ctor(System.String,System.Boolean,System.Boolean)
extern void SimpleRangePart__ctor_mB534461B8FDB2AC49C0BBB86BA019521D82E99E5 (void);
// 0x00000BE0 System.Boolean NPOI.SS.Formula.FormulaParser_SimpleRangePart::get_IsCell()
extern void SimpleRangePart_get_IsCell_m55981A1C2B907B39CA6A6D5AB53E68D38E0A7BF6 (void);
// 0x00000BE1 System.Boolean NPOI.SS.Formula.FormulaParser_SimpleRangePart::get_IsRowOrColumn()
extern void SimpleRangePart_get_IsRowOrColumn_m91DCC31D747B6D24F286F51541EFD155B3183E79 (void);
// 0x00000BE2 NPOI.SS.Util.CellReference NPOI.SS.Formula.FormulaParser_SimpleRangePart::getCellReference()
extern void SimpleRangePart_getCellReference_m17C0F79F6420611E1B0662DA10A1DEC06674F30F (void);
// 0x00000BE3 System.Boolean NPOI.SS.Formula.FormulaParser_SimpleRangePart::get_IsColumn()
extern void SimpleRangePart_get_IsColumn_mE07A44EDFD808CE117532CBFA4784E3D64AE14A8 (void);
// 0x00000BE4 System.Boolean NPOI.SS.Formula.FormulaParser_SimpleRangePart::get_IsRow()
extern void SimpleRangePart_get_IsRow_m44C09D6C82F75C35BA0FD086D5EE9A5CCB8E51E6 (void);
// 0x00000BE5 System.String NPOI.SS.Formula.FormulaParser_SimpleRangePart::get_Rep()
extern void SimpleRangePart_get_Rep_m188F00F3CF0812D91EC88D5B80D3C1E6C58C51A4 (void);
// 0x00000BE6 System.Boolean NPOI.SS.Formula.FormulaParser_SimpleRangePart::IsCompatibleForArea(NPOI.SS.Formula.FormulaParser_SimpleRangePart)
extern void SimpleRangePart_IsCompatibleForArea_mE258B11B4BC067D5945B987B8B6503D65A3010AA (void);
// 0x00000BE7 System.String NPOI.SS.Formula.FormulaParser_SimpleRangePart::ToString()
extern void SimpleRangePart_ToString_m87F45DB60D5B4BD2E412476C776030583A84E01B (void);
// 0x00000BE8 System.String NPOI.SS.Formula.FormulaRenderer::ToFormulaString(NPOI.SS.Formula.IFormulaRenderingWorkbook,NPOI.SS.Formula.PTG.Ptg[])
extern void FormulaRenderer_ToFormulaString_m7244B965BD5A1F846F0F1365F43183F9EBCDD3CB (void);
// 0x00000BE9 System.String[] NPOI.SS.Formula.FormulaRenderer::GetOperands(System.Collections.Stack,System.Int32)
extern void FormulaRenderer_GetOperands_m8C229B4066B17AC6BA44B6E1F3FCA8C20208A1B3 (void);
// 0x00000BEA System.Void NPOI.SS.Formula.Function.FunctionDataBuilder::.ctor(System.Int32)
extern void FunctionDataBuilder__ctor_m0E5AA3DF5282389E51CB537EDE7AF841BDCBAA5F (void);
// 0x00000BEB System.Void NPOI.SS.Formula.Function.FunctionDataBuilder::Add(System.Int32,System.String,System.Int32,System.Int32,System.Byte,System.Byte[],System.Boolean)
extern void FunctionDataBuilder_Add_m84967CEDA9AC8D00B3328F1E6B785ABDCD9D7199 (void);
// 0x00000BEC NPOI.SS.Formula.Function.FunctionMetadataRegistry NPOI.SS.Formula.Function.FunctionDataBuilder::Build()
extern void FunctionDataBuilder_Build_m8628A8E57C9AF0AAB7D615E4D9D93B5CBF270F50 (void);
// 0x00000BED System.Void NPOI.SS.Formula.Function.FunctionMetadata::.ctor(System.Int32,System.String,System.Int32,System.Int32,System.Byte,System.Byte[])
extern void FunctionMetadata__ctor_m4BDEFD5B55E8EDD2244D70687026F9E9161240F2 (void);
// 0x00000BEE System.Int32 NPOI.SS.Formula.Function.FunctionMetadata::get_Index()
extern void FunctionMetadata_get_Index_m48BF885652356A3BFFE54E0C54BCD75437C91757 (void);
// 0x00000BEF System.String NPOI.SS.Formula.Function.FunctionMetadata::get_Name()
extern void FunctionMetadata_get_Name_m2F7AFFD764774E4BBD0A8A29AED2252B8DFC805C (void);
// 0x00000BF0 System.Int32 NPOI.SS.Formula.Function.FunctionMetadata::get_MinParams()
extern void FunctionMetadata_get_MinParams_mD70488860BB8A41D22E46AABEFD9FFCF3588C383 (void);
// 0x00000BF1 System.Int32 NPOI.SS.Formula.Function.FunctionMetadata::get_MaxParams()
extern void FunctionMetadata_get_MaxParams_mC38217C5C4AB4C62E92C428F153284FAF0751D7D (void);
// 0x00000BF2 System.Boolean NPOI.SS.Formula.Function.FunctionMetadata::get_HasFixedArgsLength()
extern void FunctionMetadata_get_HasFixedArgsLength_m86F67ED8876FB262422ECBE1FFD97E9F9164C3CC (void);
// 0x00000BF3 System.Byte NPOI.SS.Formula.Function.FunctionMetadata::get_ReturnClassCode()
extern void FunctionMetadata_get_ReturnClassCode_mA2F23F43E91C23C4A4C0E0A7EB2B13C370E1BADA (void);
// 0x00000BF4 System.Byte[] NPOI.SS.Formula.Function.FunctionMetadata::get_ParameterClassCodes()
extern void FunctionMetadata_get_ParameterClassCodes_m15BB055D607317C4DD40D0C66313F0B20E4C57D7 (void);
// 0x00000BF5 System.Boolean NPOI.SS.Formula.Function.FunctionMetadata::get_HasUnlimitedVarags()
extern void FunctionMetadata_get_HasUnlimitedVarags_m05E4D7C9901B04F3AC1278FF9EA6D5EB32813C2C (void);
// 0x00000BF6 System.String NPOI.SS.Formula.Function.FunctionMetadata::ToString()
extern void FunctionMetadata_ToString_m4E2D8F3E54AF55EEE17E3AAB4A1A63DA1BDC1E00 (void);
// 0x00000BF7 NPOI.SS.Formula.Function.FunctionMetadataRegistry NPOI.SS.Formula.Function.FunctionMetadataReader::CreateRegistry()
extern void FunctionMetadataReader_CreateRegistry_m037C5FAB408976B0599AEBBD44C4CBD14878FEA7 (void);
// 0x00000BF8 System.Void NPOI.SS.Formula.Function.FunctionMetadataReader::ProcessLine(NPOI.SS.Formula.Function.FunctionDataBuilder,System.String)
extern void FunctionMetadataReader_ProcessLine_m09E6DC81F4C85C8F9C909369A53509B3FFBE1C7E (void);
// 0x00000BF9 System.Byte NPOI.SS.Formula.Function.FunctionMetadataReader::ParseReturnTypeCode(System.String)
extern void FunctionMetadataReader_ParseReturnTypeCode_m6AA90BB8CD35B58AF14DC710B6ED6C26123C951C (void);
// 0x00000BFA System.Byte[] NPOI.SS.Formula.Function.FunctionMetadataReader::ParseOperandTypeCodes(System.String)
extern void FunctionMetadataReader_ParseOperandTypeCodes_mBEEE4176CE9F62002A15324A70D3AB52B0972928 (void);
// 0x00000BFB System.Boolean NPOI.SS.Formula.Function.FunctionMetadataReader::IsDash(System.String)
extern void FunctionMetadataReader_IsDash_mCC80C04697BDD3BDDD0F17F5FFE87037AE382D53 (void);
// 0x00000BFC System.Byte NPOI.SS.Formula.Function.FunctionMetadataReader::ParseOperandTypeCode(System.String)
extern void FunctionMetadataReader_ParseOperandTypeCode_mFBE8BEEB9E464AE10B6604D15DA0E94C4A6FF84B (void);
// 0x00000BFD System.Void NPOI.SS.Formula.Function.FunctionMetadataReader::ValidateFunctionName(System.String)
extern void FunctionMetadataReader_ValidateFunctionName_mF6C78975BC5F4F647831047DD0A89F458B01C44F (void);
// 0x00000BFE System.Int32 NPOI.SS.Formula.Function.FunctionMetadataReader::ParseInt(System.String)
extern void FunctionMetadataReader_ParseInt_m1A5E406CA3FDA67DCE9941D7BEE0552F9D0C28A4 (void);
// 0x00000BFF System.Void NPOI.SS.Formula.Function.FunctionMetadataReader::.cctor()
extern void FunctionMetadataReader__cctor_mFBC1BFF17E8C4522A2EEF2B15CDE164E56757150 (void);
// 0x00000C00 NPOI.SS.Formula.Function.FunctionMetadataRegistry NPOI.SS.Formula.Function.FunctionMetadataRegistry::GetInstance()
extern void FunctionMetadataRegistry_GetInstance_m76847BA51659B0BB6262715FC160066BF62A043C (void);
// 0x00000C01 System.Void NPOI.SS.Formula.Function.FunctionMetadataRegistry::.ctor(NPOI.SS.Formula.Function.FunctionMetadata[],System.Collections.Hashtable)
extern void FunctionMetadataRegistry__ctor_m48BB698FEA7832FAAD751671E2A736C053581315 (void);
// 0x00000C02 NPOI.SS.Formula.Function.FunctionMetadata NPOI.SS.Formula.Function.FunctionMetadataRegistry::GetFunctionByIndex(System.Int32)
extern void FunctionMetadataRegistry_GetFunctionByIndex_mA47703D4785DF45A0A676719658387536104AABA (void);
// 0x00000C03 NPOI.SS.Formula.Function.FunctionMetadata NPOI.SS.Formula.Function.FunctionMetadataRegistry::GetFunctionByIndexInternal(System.Int32)
extern void FunctionMetadataRegistry_GetFunctionByIndexInternal_m38889774C6E7833201A1BB3DA5B0D86B396D520F (void);
// 0x00000C04 System.Int16 NPOI.SS.Formula.Function.FunctionMetadataRegistry::LookupIndexByName(System.String)
extern void FunctionMetadataRegistry_LookupIndexByName_m0B821DB40F069A2BF5400B1C9D4E3A9FC0D27DBA (void);
// 0x00000C05 NPOI.SS.Formula.Function.FunctionMetadata NPOI.SS.Formula.Function.FunctionMetadataRegistry::GetFunctionByNameInternal(System.String)
extern void FunctionMetadataRegistry_GetFunctionByNameInternal_m932CEA2AC7A49B6F3287B6A9892577619BB9D49D (void);
// 0x00000C06 NPOI.SS.Formula.Function.FunctionMetadata NPOI.SS.Formula.Function.FunctionMetadataRegistry::GetFunctionByName(System.String)
extern void FunctionMetadataRegistry_GetFunctionByName_m968D4956B6A45A68A7BEEE49D08E5692131BEDA2 (void);
// 0x00000C07 System.Void NPOI.SS.Formula.OperandClassTransformer::.ctor(NPOI.SS.Formula.FormulaType)
extern void OperandClassTransformer__ctor_mE6548A3D5C365F7FA3D177922C765ADEE10E74CC (void);
// 0x00000C08 System.Void NPOI.SS.Formula.OperandClassTransformer::TransformFormula(NPOI.SS.Formula.ParseNode)
extern void OperandClassTransformer_TransformFormula_m49F263C63E533D7D0C48FC57EEA5085FC9152218 (void);
// 0x00000C09 System.Void NPOI.SS.Formula.OperandClassTransformer::TransformNode(NPOI.SS.Formula.ParseNode,System.Byte,System.Boolean)
extern void OperandClassTransformer_TransformNode_m48C475030F55352137F3AF58E486D9B8D42E5E01 (void);
// 0x00000C0A System.Boolean NPOI.SS.Formula.OperandClassTransformer::IsSingleArgSum(NPOI.SS.Formula.PTG.Ptg)
extern void OperandClassTransformer_IsSingleArgSum_mE7930564D078BA455BD4B360E09985584EE3FB5B (void);
// 0x00000C0B System.Boolean NPOI.SS.Formula.OperandClassTransformer::IsSimpleValueFunction(NPOI.SS.Formula.PTG.Ptg)
extern void OperandClassTransformer_IsSimpleValueFunction_m0D7A35132B760A45A0C0CA1008CFCFB5825D69A4 (void);
// 0x00000C0C System.Byte NPOI.SS.Formula.OperandClassTransformer::TransformClass(System.Byte,System.Byte,System.Boolean)
extern void OperandClassTransformer_TransformClass_m454BA192BE0CA284C9921E2CDB7A9DDFF2A3591E (void);
// 0x00000C0D System.Void NPOI.SS.Formula.OperandClassTransformer::TransformFunctionNode(NPOI.SS.Formula.PTG.AbstractFunctionPtg,NPOI.SS.Formula.ParseNode[],System.Byte,System.Boolean)
extern void OperandClassTransformer_TransformFunctionNode_m7A6D9E751E88A7BD16CB209ED205954D50F63500 (void);
// 0x00000C0E System.Void NPOI.SS.Formula.OperandClassTransformer::SetSimpleValueFuncClass(NPOI.SS.Formula.PTG.AbstractFunctionPtg,System.Byte,System.Boolean)
extern void OperandClassTransformer_SetSimpleValueFuncClass_mB63DF446EF1CA8E940A39BA42F570C7DACF59B33 (void);
// 0x00000C0F System.Void NPOI.SS.Formula.ParseNode::.ctor(NPOI.SS.Formula.PTG.Ptg,NPOI.SS.Formula.ParseNode[])
extern void ParseNode__ctor_m5565C4215F77B1555A3906D22893BBD003343118 (void);
// 0x00000C10 System.Void NPOI.SS.Formula.ParseNode::.ctor(NPOI.SS.Formula.PTG.Ptg)
extern void ParseNode__ctor_m87FA0031DDD2858AC67292D61F13498BDABC675F (void);
// 0x00000C11 System.Void NPOI.SS.Formula.ParseNode::.ctor(NPOI.SS.Formula.PTG.Ptg,NPOI.SS.Formula.ParseNode)
extern void ParseNode__ctor_m0FA0A74ED2AF86C8858D2174B2FE5366760FF2CB (void);
// 0x00000C12 System.Void NPOI.SS.Formula.ParseNode::.ctor(NPOI.SS.Formula.PTG.Ptg,NPOI.SS.Formula.ParseNode,NPOI.SS.Formula.ParseNode)
extern void ParseNode__ctor_m4D800012CFFF2A3A0277764E536E9710A8F66D70 (void);
// 0x00000C13 System.Int32 NPOI.SS.Formula.ParseNode::get_TokenCount()
extern void ParseNode_get_TokenCount_m4909339096F2385DCF9E5D8DD70DD287C81202FB (void);
// 0x00000C14 System.Int32 NPOI.SS.Formula.ParseNode::get_EncodedSize()
extern void ParseNode_get_EncodedSize_m7ACD97A6F723C6E25B0BF9D527BD8DA32D072B3D (void);
// 0x00000C15 NPOI.SS.Formula.PTG.Ptg[] NPOI.SS.Formula.ParseNode::ToTokenArray(NPOI.SS.Formula.ParseNode)
extern void ParseNode_ToTokenArray_m42ECE15566C0B8B1335F5FC335BDDBFB6C85930A (void);
// 0x00000C16 System.Void NPOI.SS.Formula.ParseNode::CollectPtgs(NPOI.SS.Formula.ParseNode_TokenCollector)
extern void ParseNode_CollectPtgs_m33D532C80DA6C7D5C2F6200A85FFE9CCE2B2C3A6 (void);
// 0x00000C17 System.Void NPOI.SS.Formula.ParseNode::CollectIfPtgs(NPOI.SS.Formula.ParseNode_TokenCollector)
extern void ParseNode_CollectIfPtgs_m8D317A1EB550DBBC7689A169A1B0F67BB840DAFE (void);
// 0x00000C18 System.Boolean NPOI.SS.Formula.ParseNode::IsIf(NPOI.SS.Formula.PTG.Ptg)
extern void ParseNode_IsIf_m8599172DE8A769AD43F7A7DFDB61FE6B0D5E4A87 (void);
// 0x00000C19 NPOI.SS.Formula.PTG.Ptg NPOI.SS.Formula.ParseNode::GetToken()
extern void ParseNode_GetToken_mD3800B2BFAAE6A27E7DCD4C40EA57F1EA34AB473 (void);
// 0x00000C1A NPOI.SS.Formula.ParseNode[] NPOI.SS.Formula.ParseNode::GetChildren()
extern void ParseNode_GetChildren_mD263AC2618DFB32978AD70AD5DE1208D9AA59BC1 (void);
// 0x00000C1B System.Void NPOI.SS.Formula.ParseNode::.cctor()
extern void ParseNode__cctor_mD10A7DFCB9EAD71A3B672914148F26442C768F2A (void);
// 0x00000C1C System.Void NPOI.SS.Formula.ParseNode_TokenCollector::.ctor(System.Int32)
extern void TokenCollector__ctor_m37A149E0A2D3BA54C70ABDCA174B1ECA7227A087 (void);
// 0x00000C1D System.Int32 NPOI.SS.Formula.ParseNode_TokenCollector::sumTokenSizes(System.Int32,System.Int32)
extern void TokenCollector_sumTokenSizes_m5BC3C5D7C196F7F23A43686F13622A90134EC89C (void);
// 0x00000C1E System.Int32 NPOI.SS.Formula.ParseNode_TokenCollector::CreatePlaceholder()
extern void TokenCollector_CreatePlaceholder_m70E20568ACB06940F5166E727E139A6EB455FE22 (void);
// 0x00000C1F System.Void NPOI.SS.Formula.ParseNode_TokenCollector::Add(NPOI.SS.Formula.PTG.Ptg)
extern void TokenCollector_Add_mF782C603CCFA717FA0669320FCE8CCDCEA16AE93 (void);
// 0x00000C20 System.Void NPOI.SS.Formula.ParseNode_TokenCollector::SetPlaceholder(System.Int32,NPOI.SS.Formula.PTG.Ptg)
extern void TokenCollector_SetPlaceholder_mEB46F9A5DC8C65CF087634F2598962C9004B8AAF (void);
// 0x00000C21 NPOI.SS.Formula.PTG.Ptg[] NPOI.SS.Formula.ParseNode_TokenCollector::GetResult()
extern void TokenCollector_GetResult_m9255D5F9DA567183FF5093AB9F463BD3FEA72985 (void);
// 0x00000C22 NPOI.SS.Formula.PTG.Ptg[] NPOI.SS.Formula.PTG.Ptg::ReadTokens(System.Int32,NPOI.Util.ILittleEndianInput)
extern void Ptg_ReadTokens_m027D788025E61243FE491099B3C721A04BDE8D75 (void);
// 0x00000C23 NPOI.SS.Formula.PTG.Ptg NPOI.SS.Formula.PTG.Ptg::CreatePtg(NPOI.Util.ILittleEndianInput)
extern void Ptg_CreatePtg_mA23F361D4DFEDAC283F88D0760F9314D3E057714 (void);
// 0x00000C24 NPOI.SS.Formula.PTG.Ptg NPOI.SS.Formula.PTG.Ptg::CreateClassifiedPtg(System.Byte,NPOI.Util.ILittleEndianInput)
extern void Ptg_CreateClassifiedPtg_m103732D22241DF012AC862F2490B4042DE6252D4 (void);
// 0x00000C25 NPOI.SS.Formula.PTG.Ptg NPOI.SS.Formula.PTG.Ptg::CreateBasePtg(System.Byte,NPOI.Util.ILittleEndianInput)
extern void Ptg_CreateBasePtg_m0E733A91A7E8019AB77592BBF72121BBEA97A723 (void);
// 0x00000C26 NPOI.SS.Formula.PTG.Ptg[] NPOI.SS.Formula.PTG.Ptg::ToPtgArray(System.Collections.ArrayList)
extern void Ptg_ToPtgArray_mABDB65A07D0292FA03539311D989093BFC5533FC (void);
// 0x00000C27 System.Object NPOI.SS.Formula.PTG.Ptg::Clone()
extern void Ptg_Clone_m53C9FA081C6B4DF8B8EE12E7F049C449DA593A35 (void);
// 0x00000C28 System.Int32 NPOI.SS.Formula.PTG.Ptg::GetEncodedSize(NPOI.SS.Formula.PTG.Ptg[])
extern void Ptg_GetEncodedSize_m4837F62AD9D858CD8E56D438BA6E4A0769AE2820 (void);
// 0x00000C29 System.Int32 NPOI.SS.Formula.PTG.Ptg::GetEncodedSizeWithoutArrayData(NPOI.SS.Formula.PTG.Ptg[])
extern void Ptg_GetEncodedSizeWithoutArrayData_mDCF7A3EE7F85C2CCB06784CD15A200C2B7A9A80D (void);
// 0x00000C2A System.Int32 NPOI.SS.Formula.PTG.Ptg::SerializePtgs(NPOI.SS.Formula.PTG.Ptg[],System.Byte[],System.Int32)
extern void Ptg_SerializePtgs_mCCA9085233798F636AB38D7DCF514558436145E0 (void);
// 0x00000C2B System.Int32 NPOI.SS.Formula.PTG.Ptg::get_Size()
// 0x00000C2C System.Boolean NPOI.SS.Formula.PTG.Ptg::get_IsBaseToken()
// 0x00000C2D System.Void NPOI.SS.Formula.PTG.Ptg::Write(NPOI.Util.ILittleEndianOutput)
// 0x00000C2E System.String NPOI.SS.Formula.PTG.Ptg::ToFormulaString()
// 0x00000C2F System.String NPOI.SS.Formula.PTG.Ptg::ToString()
extern void Ptg_ToString_m8665E41D057F48DFC91CB6912F4D9E1F4CFE3916 (void);
// 0x00000C30 System.Byte NPOI.SS.Formula.PTG.Ptg::get_PtgClass()
extern void Ptg_get_PtgClass_mE87396305D94739E6735735EFAC8BA82FFC1F925 (void);
// 0x00000C31 System.Void NPOI.SS.Formula.PTG.Ptg::set_PtgClass(System.Byte)
extern void Ptg_set_PtgClass_m4994637286E7D4A3B842D6C2C9D1492AA89EDCF0 (void);
// 0x00000C32 System.Byte NPOI.SS.Formula.PTG.Ptg::get_DefaultOperandClass()
// 0x00000C33 System.Char NPOI.SS.Formula.PTG.Ptg::get_RVAType()
extern void Ptg_get_RVAType_mFCF3CE5E1BDA8B65A85A3A8DB47592C376474377 (void);
// 0x00000C34 System.Object NPOI.SS.Formula.PTG.Ptg::System.ICloneable.Clone()
extern void Ptg_System_ICloneable_Clone_mF1132B72DCC4D8E2409C8C5912AB6106223548C7 (void);
// 0x00000C35 System.Void NPOI.SS.Formula.PTG.Ptg::.ctor()
extern void Ptg__ctor_m0915325ECFFF03858A8FAD9099BAD7EE5C2F998E (void);
// 0x00000C36 System.Void NPOI.SS.Formula.PTG.Ptg::.cctor()
extern void Ptg__cctor_m835C4059A388435F9F6976D3CAC0B925F4943777 (void);
// 0x00000C37 System.String NPOI.SS.Formula.PTG.OperationPtg::ToFormulaString(System.String[])
// 0x00000C38 System.Int32 NPOI.SS.Formula.PTG.OperationPtg::get_NumberOfOperands()
// 0x00000C39 System.Byte NPOI.SS.Formula.PTG.OperationPtg::get_DefaultOperandClass()
extern void OperationPtg_get_DefaultOperandClass_m3F8D1FAE07A60AED59344A67E50DAB2D75F7AC70 (void);
// 0x00000C3A System.Void NPOI.SS.Formula.PTG.OperationPtg::.ctor()
extern void OperationPtg__ctor_m84079E2B621DE6792A0E1652209F4F292EEADAF2 (void);
// 0x00000C3B System.Void NPOI.SS.Formula.PTG.AbstractFunctionPtg::.ctor(System.Int32,System.Int32,System.Byte[],System.Int32)
extern void AbstractFunctionPtg__ctor_mA9D7865B64F7C07CE50700A41AFF27D348378454 (void);
// 0x00000C3C System.Boolean NPOI.SS.Formula.PTG.AbstractFunctionPtg::get_IsBaseToken()
extern void AbstractFunctionPtg_get_IsBaseToken_m9E12B83C2B6929E851E20010822997B2B4EC6400 (void);
// 0x00000C3D System.String NPOI.SS.Formula.PTG.AbstractFunctionPtg::ToString()
extern void AbstractFunctionPtg_ToString_m0E62590E987FA5DBBCEDBCEAFFAF7BF57AE7A619 (void);
// 0x00000C3E System.Int32 NPOI.SS.Formula.PTG.AbstractFunctionPtg::get_NumberOfOperands()
extern void AbstractFunctionPtg_get_NumberOfOperands_mFAD01192E8D715FE8D3B3394B0BD7D83C23F0ACD (void);
// 0x00000C3F System.String NPOI.SS.Formula.PTG.AbstractFunctionPtg::get_Name()
extern void AbstractFunctionPtg_get_Name_mF20C7BDCD2F3FAC0691743949B31BF088C460A15 (void);
// 0x00000C40 System.Boolean NPOI.SS.Formula.PTG.AbstractFunctionPtg::get_IsExternalFunction()
extern void AbstractFunctionPtg_get_IsExternalFunction_mE8F681EAC1E7ECB6369251FAC8830E535BD4912C (void);
// 0x00000C41 System.String NPOI.SS.Formula.PTG.AbstractFunctionPtg::ToFormulaString()
extern void AbstractFunctionPtg_ToFormulaString_m8549803B5F37CB3579EBD2F50015E0CDEAEC9460 (void);
// 0x00000C42 System.String NPOI.SS.Formula.PTG.AbstractFunctionPtg::ToFormulaString(System.String[])
extern void AbstractFunctionPtg_ToFormulaString_m53F05D484F32C8566650DC2AD674CDC89891739D (void);
// 0x00000C43 System.Void NPOI.SS.Formula.PTG.AbstractFunctionPtg::AppendArgs(System.Text.StringBuilder,System.Int32,System.String[])
extern void AbstractFunctionPtg_AppendArgs_mD7A3F52BD4A92794EA13FE6E8C5A4F6AAC7BDBF8 (void);
// 0x00000C44 System.Boolean NPOI.SS.Formula.PTG.AbstractFunctionPtg::IsBuiltInFunctionName(System.String)
extern void AbstractFunctionPtg_IsBuiltInFunctionName_m2B85DC19B9FAA6C7202D0C213F2018983FD37D36 (void);
// 0x00000C45 System.String NPOI.SS.Formula.PTG.AbstractFunctionPtg::LookupName(System.Int16)
extern void AbstractFunctionPtg_LookupName_m9E69C2E383D0EDBBE7D35B8F75B7899FC82E011D (void);
// 0x00000C46 System.Int16 NPOI.SS.Formula.PTG.AbstractFunctionPtg::LookupIndex(System.String)
extern void AbstractFunctionPtg_LookupIndex_m7B0BEE60E04C52FC6F6B3324EDF5FD6ACE715462 (void);
// 0x00000C47 System.Byte NPOI.SS.Formula.PTG.AbstractFunctionPtg::get_DefaultOperandClass()
extern void AbstractFunctionPtg_get_DefaultOperandClass_m9698CDBF1DB2C4EE551330542C912EF8D0049B34 (void);
// 0x00000C48 System.Byte NPOI.SS.Formula.PTG.AbstractFunctionPtg::GetParameterClass(System.Int32)
extern void AbstractFunctionPtg_GetParameterClass_m70E8347D2966F60896B596AF371A7C1B7FF4EAD0 (void);
// 0x00000C49 System.Boolean NPOI.SS.Formula.PTG.ValueOperatorPtg::get_IsBaseToken()
extern void ValueOperatorPtg_get_IsBaseToken_mEFDE7C9A0C2ABAF61728E8CD3E11D0984073B029 (void);
// 0x00000C4A System.Byte NPOI.SS.Formula.PTG.ValueOperatorPtg::get_DefaultOperandClass()
extern void ValueOperatorPtg_get_DefaultOperandClass_m21894AF2F09A1B8AD4ED2CB7336DB5C517F246F2 (void);
// 0x00000C4B System.Void NPOI.SS.Formula.PTG.ValueOperatorPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void ValueOperatorPtg_Write_m6D83074BF75DADBEB9D9F1CF27CFB449AD5876C3 (void);
// 0x00000C4C System.Byte NPOI.SS.Formula.PTG.ValueOperatorPtg::get_Sid()
// 0x00000C4D System.Int32 NPOI.SS.Formula.PTG.ValueOperatorPtg::get_Size()
extern void ValueOperatorPtg_get_Size_m9A33BDCC3A0C26582635D2E262E6AC6E2483CA59 (void);
// 0x00000C4E System.String NPOI.SS.Formula.PTG.ValueOperatorPtg::ToFormulaString()
extern void ValueOperatorPtg_ToFormulaString_m4A8D9363BBB896A303320EAB252F2134F12535AE (void);
// 0x00000C4F System.Void NPOI.SS.Formula.PTG.ValueOperatorPtg::.ctor()
extern void ValueOperatorPtg__ctor_m7D6F367A7F0E3E49092847AEBF259B1278EB58C2 (void);
// 0x00000C50 System.Void NPOI.SS.Formula.PTG.AddPtg::.ctor()
extern void AddPtg__ctor_m4DF480028748B84007CAD928E34245E70E2CCFC4 (void);
// 0x00000C51 System.Byte NPOI.SS.Formula.PTG.AddPtg::get_Sid()
extern void AddPtg_get_Sid_m633C0CCE749BE84D7567F3EC2A8ED42123C5BD7F (void);
// 0x00000C52 System.Int32 NPOI.SS.Formula.PTG.AddPtg::get_NumberOfOperands()
extern void AddPtg_get_NumberOfOperands_m174FACEFF3381FF2AD07192B608792804BE4C94E (void);
// 0x00000C53 System.String NPOI.SS.Formula.PTG.AddPtg::ToFormulaString(System.String[])
extern void AddPtg_ToFormulaString_m9622B436943992F22C240C9C33998CAE72C4097C (void);
// 0x00000C54 System.Void NPOI.SS.Formula.PTG.AddPtg::.cctor()
extern void AddPtg__cctor_m2914BCEC07AC06AB107D866123F2FD81C16BE0E1 (void);
// 0x00000C55 System.Boolean NPOI.SS.Formula.PTG.OperandPtg::get_IsBaseToken()
extern void OperandPtg_get_IsBaseToken_m7CECE041B6934ED688B818AA5BC03E4955AE1EE9 (void);
// 0x00000C56 NPOI.SS.Formula.PTG.OperandPtg NPOI.SS.Formula.PTG.OperandPtg::Copy()
extern void OperandPtg_Copy_m86124DA7827EBD1A76BD3932C4FEF81B8B88DE63 (void);
// 0x00000C57 System.Void NPOI.SS.Formula.PTG.OperandPtg::.ctor()
extern void OperandPtg__ctor_m417B39AD4DF7A1FC6B691A3BF015F1A3FED20718 (void);
// 0x00000C58 System.Void NPOI.SS.Formula.PTG.AreaPtgBase::.ctor()
extern void AreaPtgBase__ctor_m869233DD0E3BCF4EA9736C9F1CBDBBA131782B55 (void);
// 0x00000C59 System.Void NPOI.SS.Formula.PTG.AreaPtgBase::.ctor(NPOI.SS.Util.AreaReference)
extern void AreaPtgBase__ctor_m1CDAA664389A8C04CCC94BABB1971119195944AC (void);
// 0x00000C5A System.Void NPOI.SS.Formula.PTG.AreaPtgBase::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void AreaPtgBase__ctor_mBE221B60305160301DFB5168CC001DC79263AD7B (void);
// 0x00000C5B System.Void NPOI.SS.Formula.PTG.AreaPtgBase::ReadCoordinates(NPOI.Util.ILittleEndianInput)
extern void AreaPtgBase_ReadCoordinates_mB5BF8D2E14A51C974D529854ACA3386B04869BF2 (void);
// 0x00000C5C System.Void NPOI.SS.Formula.PTG.AreaPtgBase::WriteCoordinates(NPOI.Util.ILittleEndianOutput)
extern void AreaPtgBase_WriteCoordinates_m7176B2763BB642CCE5A99420171F27DBD46AF9DD (void);
// 0x00000C5D System.Int32 NPOI.SS.Formula.PTG.AreaPtgBase::get_FirstRow()
extern void AreaPtgBase_get_FirstRow_m44D07AD5388447B93F07127195F52739972B2DA3 (void);
// 0x00000C5E System.Void NPOI.SS.Formula.PTG.AreaPtgBase::set_FirstRow(System.Int32)
extern void AreaPtgBase_set_FirstRow_mE6707980FC6887BCE32E917DC24DC0692D07B102 (void);
// 0x00000C5F System.Int32 NPOI.SS.Formula.PTG.AreaPtgBase::get_LastRow()
extern void AreaPtgBase_get_LastRow_m3F411334B85FC1E981054D7813C9F7E00BA1AD34 (void);
// 0x00000C60 System.Void NPOI.SS.Formula.PTG.AreaPtgBase::set_LastRow(System.Int32)
extern void AreaPtgBase_set_LastRow_m0C396F10EAB203DDF15195C7728670A29EB8B0D9 (void);
// 0x00000C61 System.Int32 NPOI.SS.Formula.PTG.AreaPtgBase::get_FirstColumn()
extern void AreaPtgBase_get_FirstColumn_m73896B03BB406AD7C9A272D2B2B26570BBC8F213 (void);
// 0x00000C62 System.Void NPOI.SS.Formula.PTG.AreaPtgBase::set_FirstColumn(System.Int32)
extern void AreaPtgBase_set_FirstColumn_mB2CEE9A15E9493E8FC32F001AB86BBB2C6B211EC (void);
// 0x00000C63 System.Boolean NPOI.SS.Formula.PTG.AreaPtgBase::get_IsFirstRowRelative()
extern void AreaPtgBase_get_IsFirstRowRelative_mB3FB872B40596A5C33D44C459E9487A213EBC4D3 (void);
// 0x00000C64 System.Void NPOI.SS.Formula.PTG.AreaPtgBase::set_IsFirstRowRelative(System.Boolean)
extern void AreaPtgBase_set_IsFirstRowRelative_m977B982F6B627E5364D155F3C542E0786ABED964 (void);
// 0x00000C65 System.Boolean NPOI.SS.Formula.PTG.AreaPtgBase::get_IsFirstColRelative()
extern void AreaPtgBase_get_IsFirstColRelative_mAA9D66FCE2529D5A28BA3E5F514B36723ED910F9 (void);
// 0x00000C66 System.Void NPOI.SS.Formula.PTG.AreaPtgBase::set_IsFirstColRelative(System.Boolean)
extern void AreaPtgBase_set_IsFirstColRelative_m9AE04069219190FA64433B8ED784614CDDF92DEE (void);
// 0x00000C67 System.Int32 NPOI.SS.Formula.PTG.AreaPtgBase::get_LastColumn()
extern void AreaPtgBase_get_LastColumn_mBFBC0F0691E8ABC920F86F0F319C688CDD19573A (void);
// 0x00000C68 System.Void NPOI.SS.Formula.PTG.AreaPtgBase::set_LastColumn(System.Int32)
extern void AreaPtgBase_set_LastColumn_m6FBDBC76343730F841CE698C353828D6D27057DC (void);
// 0x00000C69 System.Boolean NPOI.SS.Formula.PTG.AreaPtgBase::get_IsLastRowRelative()
extern void AreaPtgBase_get_IsLastRowRelative_mD554BDE275050E386C91C415CEB39C07B1FD98CC (void);
// 0x00000C6A System.Void NPOI.SS.Formula.PTG.AreaPtgBase::set_IsLastRowRelative(System.Boolean)
extern void AreaPtgBase_set_IsLastRowRelative_mE36452A30FE9341C3ECF98A3FBEE2903CFBC46EA (void);
// 0x00000C6B System.Boolean NPOI.SS.Formula.PTG.AreaPtgBase::get_IsLastColRelative()
extern void AreaPtgBase_get_IsLastColRelative_mEDE524022567B6360512E592D0E94FE14D605662 (void);
// 0x00000C6C System.Void NPOI.SS.Formula.PTG.AreaPtgBase::set_IsLastColRelative(System.Boolean)
extern void AreaPtgBase_set_IsLastColRelative_m424CABDBE3A62050B7F6644684CE49C89AF910AE (void);
// 0x00000C6D System.String NPOI.SS.Formula.PTG.AreaPtgBase::ToFormulaString()
extern void AreaPtgBase_ToFormulaString_m265E05F14F615DB2C8E52770C5D2F68B2BDD30E9 (void);
// 0x00000C6E System.Byte NPOI.SS.Formula.PTG.AreaPtgBase::get_DefaultOperandClass()
extern void AreaPtgBase_get_DefaultOperandClass_m12D33C5173451EFBCD1EC0D7A9B5A4B42B1C5D5F (void);
// 0x00000C6F System.String NPOI.SS.Formula.PTG.AreaPtgBase::FormatReferenceAsString()
extern void AreaPtgBase_FormatReferenceAsString_m5E85E21CB6FED757299A4DF852931A39126D0349 (void);
// 0x00000C70 System.Void NPOI.SS.Formula.PTG.AreaPtgBase::.cctor()
extern void AreaPtgBase__cctor_mED087609FA476008D65E9C0E494A8B611F2BD98A (void);
// 0x00000C71 System.Void NPOI.SS.Formula.PTG.Area2DPtgBase::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void Area2DPtgBase__ctor_m23DB8669380073AEAE9502D7754D34C58590B3C3 (void);
// 0x00000C72 System.Void NPOI.SS.Formula.PTG.Area2DPtgBase::.ctor(NPOI.SS.Util.AreaReference)
extern void Area2DPtgBase__ctor_m32D31588FED59D14DDFBB044CF1816E16912FAA1 (void);
// 0x00000C73 System.Void NPOI.SS.Formula.PTG.Area2DPtgBase::.ctor(NPOI.Util.ILittleEndianInput)
extern void Area2DPtgBase__ctor_m6A9650BA6BEF783768286BFB79C6AC457A943DFF (void);
// 0x00000C74 System.Byte NPOI.SS.Formula.PTG.Area2DPtgBase::get_Sid()
// 0x00000C75 System.Void NPOI.SS.Formula.PTG.Area2DPtgBase::Write(NPOI.Util.ILittleEndianOutput)
extern void Area2DPtgBase_Write_m1BDF63E2DA02705175DB832632CE5C3F25F99F10 (void);
// 0x00000C76 System.Int32 NPOI.SS.Formula.PTG.Area2DPtgBase::get_Size()
extern void Area2DPtgBase_get_Size_mEDD126C99C0B83EBDAF58A0E7BD1A21D91A4308F (void);
// 0x00000C77 System.String NPOI.SS.Formula.PTG.Area2DPtgBase::ToFormulaString()
extern void Area2DPtgBase_ToFormulaString_mA324A8F005781595BDA957DDA869F874423697F0 (void);
// 0x00000C78 System.String NPOI.SS.Formula.PTG.Area2DPtgBase::ToString()
extern void Area2DPtgBase_ToString_mEFEF18ED39DAC55A8BE709332B52E1024EC5C39F (void);
// 0x00000C79 System.String NPOI.SS.Formula.WorkbookDependentFormula::ToFormulaString(NPOI.SS.Formula.IFormulaRenderingWorkbook)
// 0x00000C7A System.Void NPOI.SS.Formula.PTG.Area3DPtg::.ctor(NPOI.SS.Util.AreaReference,System.Int32)
extern void Area3DPtg__ctor_m66FC30CC6719B41ADEA958B61BCE7A8212B60FF6 (void);
// 0x00000C7B System.Void NPOI.SS.Formula.PTG.Area3DPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void Area3DPtg__ctor_m4344D85A4C5179B43EBC75D80A7E6D0FF3B93470 (void);
// 0x00000C7C System.String NPOI.SS.Formula.PTG.Area3DPtg::ToString()
extern void Area3DPtg_ToString_mA90FD5D80D87705C94178C8E0878EE5219CABFAF (void);
// 0x00000C7D System.Void NPOI.SS.Formula.PTG.Area3DPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void Area3DPtg_Write_m892A1EE0340829DE01689D4381BE94D24CD7E7EB (void);
// 0x00000C7E System.Int32 NPOI.SS.Formula.PTG.Area3DPtg::get_Size()
extern void Area3DPtg_get_Size_mE0212C5FA2C16F9D17495370CB443DDACE110DFA (void);
// 0x00000C7F System.Int32 NPOI.SS.Formula.PTG.Area3DPtg::get_ExternSheetIndex()
extern void Area3DPtg_get_ExternSheetIndex_mF6650091AA7FE96A2AC3ACEF426C09E1CC4B27A8 (void);
// 0x00000C80 System.Void NPOI.SS.Formula.PTG.Area3DPtg::set_ExternSheetIndex(System.Int32)
extern void Area3DPtg_set_ExternSheetIndex_mC723C4B353126EA7AEAE4FDF8AEC0111A1DDF2B2 (void);
// 0x00000C81 System.String NPOI.SS.Formula.PTG.Area3DPtg::ToFormulaString()
extern void Area3DPtg_ToFormulaString_m5217B11AE4155DD4A2799D0802B9A85689C64E75 (void);
// 0x00000C82 System.String NPOI.SS.Formula.PTG.Area3DPtg::ToFormulaString(NPOI.SS.Formula.IFormulaRenderingWorkbook)
extern void Area3DPtg_ToFormulaString_m63FC7CD4A38DBC318193FD099A64165B7035E27C (void);
// 0x00000C83 System.Byte NPOI.SS.Formula.PTG.Area3DPtg::get_DefaultOperandClass()
extern void Area3DPtg_get_DefaultOperandClass_mC2D623A1255D707F45816AC06152A2F3CD0232CA (void);
// 0x00000C84 System.Void NPOI.SS.Formula.PTG.AreaErrPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void AreaErrPtg__ctor_m47C598A779B2A45F63BBDADE5288A77CBDB8BD5A (void);
// 0x00000C85 System.Void NPOI.SS.Formula.PTG.AreaErrPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void AreaErrPtg_Write_mE42FA3B5C1DC2CECF9943BA9BB49FA86A6A17058 (void);
// 0x00000C86 System.String NPOI.SS.Formula.PTG.AreaErrPtg::ToFormulaString()
extern void AreaErrPtg_ToFormulaString_m5D87950804AFA3D9EA0D85C513D858EF4CC77CD8 (void);
// 0x00000C87 System.Byte NPOI.SS.Formula.PTG.AreaErrPtg::get_DefaultOperandClass()
extern void AreaErrPtg_get_DefaultOperandClass_m86BEE7EAED9E109D0D064EB9436E875BA6DEB5FA (void);
// 0x00000C88 System.Int32 NPOI.SS.Formula.PTG.AreaErrPtg::get_Size()
extern void AreaErrPtg_get_Size_mF084C2BDBF3C0998F21132852B5483344B8923C7 (void);
// 0x00000C89 System.Void NPOI.SS.Formula.PTG.AreaNPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void AreaNPtg__ctor_mFD390E29C5BC11DE0DF2484FBAD188D708379EBC (void);
// 0x00000C8A System.Byte NPOI.SS.Formula.PTG.AreaNPtg::get_Sid()
extern void AreaNPtg_get_Sid_m3A23AB53BE1476E8AE061500633F25EF51772CC0 (void);
// 0x00000C8B System.Void NPOI.SS.Formula.PTG.AreaPtg::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void AreaPtg__ctor_m990D9D141773CFD1681236E5CB01D3E1A1B29036 (void);
// 0x00000C8C System.Void NPOI.SS.Formula.PTG.AreaPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void AreaPtg__ctor_mC916875EFAE8D739033F8F2AC92C3FCC5493632C (void);
// 0x00000C8D System.Void NPOI.SS.Formula.PTG.AreaPtg::.ctor(NPOI.SS.Util.AreaReference)
extern void AreaPtg__ctor_mFB2FBD8624811E5D2BB3CDE7B0F889D4CB6665DF (void);
// 0x00000C8E System.Byte NPOI.SS.Formula.PTG.AreaPtg::get_Sid()
extern void AreaPtg_get_Sid_mBEB621AF2AD101E5E31F97EFD683AF7FCC0DC5FD (void);
// 0x00000C8F System.Void NPOI.SS.Formula.PTG.ArrayPtg::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Object[])
extern void ArrayPtg__ctor_mE69531E2414F65A383B534C27F286212761BB331 (void);
// 0x00000C90 System.Void NPOI.SS.Formula.PTG.ArrayPtg::.ctor(System.Object[][])
extern void ArrayPtg__ctor_mCFE677A8E3F24B10548743DE76EDE8A845B154CC (void);
// 0x00000C91 System.Boolean NPOI.SS.Formula.PTG.ArrayPtg::get_IsBaseToken()
extern void ArrayPtg_get_IsBaseToken_mEF57847718560DDB28ABE2B3396F4DE76E7814F9 (void);
// 0x00000C92 System.String NPOI.SS.Formula.PTG.ArrayPtg::ToString()
extern void ArrayPtg_ToString_m8A584FE709E51C91CC94E757749D1ABF2D5BFDDC (void);
// 0x00000C93 System.Int32 NPOI.SS.Formula.PTG.ArrayPtg::GetValueIndex(System.Int32,System.Int32)
extern void ArrayPtg_GetValueIndex_mABCB4A69BE737F79539C83B3A745C2B5B942760F (void);
// 0x00000C94 System.Void NPOI.SS.Formula.PTG.ArrayPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void ArrayPtg_Write_mA1B224A53C4418C31918835B168CD945C9C4E319 (void);
// 0x00000C95 System.Int32 NPOI.SS.Formula.PTG.ArrayPtg::WriteTokenValueBytes(NPOI.Util.ILittleEndianOutput)
extern void ArrayPtg_WriteTokenValueBytes_m03F1596D8A0A286CC565993C29DE0F0B92686C5C (void);
// 0x00000C96 System.Int32 NPOI.SS.Formula.PTG.ArrayPtg::get_RowCount()
extern void ArrayPtg_get_RowCount_mD5ABBCF393428E3914846159C7116843E9DF801C (void);
// 0x00000C97 System.Int32 NPOI.SS.Formula.PTG.ArrayPtg::get_ColumnCount()
extern void ArrayPtg_get_ColumnCount_m3116C129AA425617E0F4BB3947FC0D65BF4FFF97 (void);
// 0x00000C98 System.Int32 NPOI.SS.Formula.PTG.ArrayPtg::get_Size()
extern void ArrayPtg_get_Size_m9EC58D80C910087B2836DED8DB98E86D491BF7A3 (void);
// 0x00000C99 System.String NPOI.SS.Formula.PTG.ArrayPtg::ToFormulaString()
extern void ArrayPtg_ToFormulaString_mE65C6E7FBE77D98AA93E0C2E0802C95670FB57BC (void);
// 0x00000C9A System.String NPOI.SS.Formula.PTG.ArrayPtg::GetConstantText(System.Object)
extern void ArrayPtg_GetConstantText_mEB901D5BB86B57FF5F51DA46C3C0A8A7D6D8FAA9 (void);
// 0x00000C9B System.Byte NPOI.SS.Formula.PTG.ArrayPtg::get_DefaultOperandClass()
extern void ArrayPtg_get_DefaultOperandClass_m30F355EC9E7970D897D3E7A540A30DBEF55D6836 (void);
// 0x00000C9C System.Void NPOI.SS.Formula.PTG.ArrayPtg_Initial::.ctor(NPOI.Util.ILittleEndianInput)
extern void Initial__ctor_mE9F24CD3DB9CF8DCB6CE35A22641928D507DA666 (void);
// 0x00000C9D System.Exception NPOI.SS.Formula.PTG.ArrayPtg_Initial::Invalid()
extern void Initial_Invalid_m49A38081C21CBBAAE0E7801C745EBC1DF3476C48 (void);
// 0x00000C9E System.Byte NPOI.SS.Formula.PTG.ArrayPtg_Initial::get_DefaultOperandClass()
extern void Initial_get_DefaultOperandClass_m59B454FF4ADB779252B4363B40959ECFCA2C4B37 (void);
// 0x00000C9F System.Int32 NPOI.SS.Formula.PTG.ArrayPtg_Initial::get_Size()
extern void Initial_get_Size_m3B62D4C4792FB960CDF0A32587B5FF66F4136734 (void);
// 0x00000CA0 System.Boolean NPOI.SS.Formula.PTG.ArrayPtg_Initial::get_IsBaseToken()
extern void Initial_get_IsBaseToken_mFB1F8F9A3E1B2A320A24795F8108D64E951E1B26 (void);
// 0x00000CA1 System.String NPOI.SS.Formula.PTG.ArrayPtg_Initial::ToFormulaString()
extern void Initial_ToFormulaString_mDADC9DDD60F094351E9120FAECD00773BC9B2BD6 (void);
// 0x00000CA2 System.Void NPOI.SS.Formula.PTG.ArrayPtg_Initial::Write(NPOI.Util.ILittleEndianOutput)
extern void Initial_Write_m807B4A43AC02CF870D03CC66281C9EC58A4C6206 (void);
// 0x00000CA3 NPOI.SS.Formula.PTG.ArrayPtg NPOI.SS.Formula.PTG.ArrayPtg_Initial::FinishReading(NPOI.Util.ILittleEndianInput)
extern void Initial_FinishReading_mFF2F9581B23D36158EC136EA62AD1044D8B6D6FF (void);
// 0x00000CA4 System.Boolean NPOI.SS.Formula.PTG.ControlPtg::get_IsBaseToken()
extern void ControlPtg_get_IsBaseToken_m73EBDD798E110D703381F92761D8ADB57FD9674A (void);
// 0x00000CA5 System.Byte NPOI.SS.Formula.PTG.ControlPtg::get_DefaultOperandClass()
extern void ControlPtg_get_DefaultOperandClass_m9D576F60775D6A84062FED5632151E0CCE045BE4 (void);
// 0x00000CA6 System.Void NPOI.SS.Formula.PTG.ControlPtg::.ctor()
extern void ControlPtg__ctor_m5EB4B239F30F10D518C51108593445197C827BCA (void);
// 0x00000CA7 System.Void NPOI.SS.Formula.PTG.AttrPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void AttrPtg__ctor_m5B7F4EA37CC98BA3CD7F49FEF219DA6F81104B5D (void);
// 0x00000CA8 System.Void NPOI.SS.Formula.PTG.AttrPtg::.ctor(System.Int32,System.Int32,System.Int32[],System.Int32)
extern void AttrPtg__ctor_m491EC7B79BF2F75109B8CE59D71037FABA0D9434 (void);
// 0x00000CA9 NPOI.SS.Formula.PTG.AttrPtg NPOI.SS.Formula.PTG.AttrPtg::CreateIf(System.Int32)
extern void AttrPtg_CreateIf_m9B4B74726A8B0DAA732A16D5F9BF8AFC5C628C3E (void);
// 0x00000CAA NPOI.SS.Formula.PTG.AttrPtg NPOI.SS.Formula.PTG.AttrPtg::CreateSkip(System.Int32)
extern void AttrPtg_CreateSkip_mD9DD769FD8650DF1A068D5C2C4C3CC343A27BA51 (void);
// 0x00000CAB NPOI.SS.Formula.PTG.AttrPtg NPOI.SS.Formula.PTG.AttrPtg::GetSumSingle()
extern void AttrPtg_GetSumSingle_m7895B0908465DB1BC72F0425AB60CAA2F798BCD0 (void);
// 0x00000CAC System.Boolean NPOI.SS.Formula.PTG.AttrPtg::get_IsSemiVolatile()
extern void AttrPtg_get_IsSemiVolatile_mF7DDDCECAB3229D456687391F8479BA9FE0CCF35 (void);
// 0x00000CAD System.Boolean NPOI.SS.Formula.PTG.AttrPtg::get_IsOptimizedIf()
extern void AttrPtg_get_IsOptimizedIf_m561F72C33376373DCCE7AE4CBFD2CA7A344A7827 (void);
// 0x00000CAE System.Boolean NPOI.SS.Formula.PTG.AttrPtg::get_IsOptimizedChoose()
extern void AttrPtg_get_IsOptimizedChoose_m4F8F6CAD03790DD2A307A87214D41A8C73FFEEA8 (void);
// 0x00000CAF System.Boolean NPOI.SS.Formula.PTG.AttrPtg::get_IsSum()
extern void AttrPtg_get_IsSum_m7D9DF3792629BBA98EE8E297F7550B744BBAE60C (void);
// 0x00000CB0 System.Boolean NPOI.SS.Formula.PTG.AttrPtg::get_IsBaxcel()
extern void AttrPtg_get_IsBaxcel_mD84C0CF7BBE51EB45ADEB9B5A3197877385CB358 (void);
// 0x00000CB1 System.Boolean NPOI.SS.Formula.PTG.AttrPtg::get_IsSpace()
extern void AttrPtg_get_IsSpace_mD8CDBF143DDE57ADF09CAE897EBD601DA01EE29D (void);
// 0x00000CB2 System.Boolean NPOI.SS.Formula.PTG.AttrPtg::get_IsSkip()
extern void AttrPtg_get_IsSkip_m29D941D9FB92D405C9F25327C8C8CF247DFF0AFC (void);
// 0x00000CB3 System.Int16 NPOI.SS.Formula.PTG.AttrPtg::get_Data()
extern void AttrPtg_get_Data_m41CC582758AC242B741F47096E6018AEC0610BA1 (void);
// 0x00000CB4 System.String NPOI.SS.Formula.PTG.AttrPtg::ToString()
extern void AttrPtg_ToString_m33B7D5123FA21CD9A01FF2C943F0F5C3C5F51E43 (void);
// 0x00000CB5 System.Void NPOI.SS.Formula.PTG.AttrPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void AttrPtg_Write_mB438B7CF3786116EF48A12305B2CECDC139E5A74 (void);
// 0x00000CB6 System.Int32 NPOI.SS.Formula.PTG.AttrPtg::get_Size()
extern void AttrPtg_get_Size_m25372E31D02FC7FC0C9A38F1CE5BC9093260FC9C (void);
// 0x00000CB7 System.String NPOI.SS.Formula.PTG.AttrPtg::ToFormulaString(System.String[])
extern void AttrPtg_ToFormulaString_mDF8374EBF4884668A8DFC9FCE63F75E23394280E (void);
// 0x00000CB8 System.Int32 NPOI.SS.Formula.PTG.AttrPtg::get_NumberOfOperands()
extern void AttrPtg_get_NumberOfOperands_m5A4FA3B007408DF640F6F6654455D46C0BF302EB (void);
// 0x00000CB9 System.String NPOI.SS.Formula.PTG.AttrPtg::ToFormulaString()
extern void AttrPtg_ToFormulaString_m45ECEE5DB7F3E10A186A02E5298EBAAAAA77A01C (void);
// 0x00000CBA System.Object NPOI.SS.Formula.PTG.AttrPtg::Clone()
extern void AttrPtg_Clone_m51CB8CFB8BD24E49EF2EC96CDCDC70D847D6D0B2 (void);
// 0x00000CBB System.Void NPOI.SS.Formula.PTG.AttrPtg::.cctor()
extern void AttrPtg__cctor_m021FA6FE8816B1ACE9197AE108E8385F4ED9C69F (void);
// 0x00000CBC System.Boolean NPOI.SS.Formula.PTG.ScalarConstantPtg::get_IsBaseToken()
extern void ScalarConstantPtg_get_IsBaseToken_mE7EB7CB4A9DB54935EB25D6F477A73CB4BD00461 (void);
// 0x00000CBD System.Byte NPOI.SS.Formula.PTG.ScalarConstantPtg::get_DefaultOperandClass()
extern void ScalarConstantPtg_get_DefaultOperandClass_m576653EDE5527DB08C30EC47955A925CA584CC03 (void);
// 0x00000CBE System.Void NPOI.SS.Formula.PTG.ScalarConstantPtg::.ctor()
extern void ScalarConstantPtg__ctor_m9C4E5F9F50C36FE3DB2445CF2642B171AD705E53 (void);
// 0x00000CBF System.Void NPOI.SS.Formula.PTG.BoolPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void BoolPtg__ctor_mC8B69E0692AC36098F832AEE0461127624BAA1FF (void);
// 0x00000CC0 System.Void NPOI.SS.Formula.PTG.BoolPtg::.ctor(System.String)
extern void BoolPtg__ctor_m879EC0BCC321A48B26B6D10555513EB5F9D6AB71 (void);
// 0x00000CC1 System.Void NPOI.SS.Formula.PTG.BoolPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void BoolPtg_Write_m940A6A5415B16062E2AB3C353B719122B1CAB3A4 (void);
// 0x00000CC2 System.Int32 NPOI.SS.Formula.PTG.BoolPtg::get_Size()
extern void BoolPtg_get_Size_m8EEF1368E76148165E3D5813AB324B71E35C3434 (void);
// 0x00000CC3 System.String NPOI.SS.Formula.PTG.BoolPtg::ToFormulaString()
extern void BoolPtg_ToFormulaString_m9AE9265C2F253403BD40C7F0437D2E45BCB253EA (void);
// 0x00000CC4 System.Void NPOI.SS.Formula.PTG.ConcatPtg::.ctor()
extern void ConcatPtg__ctor_m2A054415BCFF9C15324EC63759FCF43279E2C547 (void);
// 0x00000CC5 System.Byte NPOI.SS.Formula.PTG.ConcatPtg::get_Sid()
extern void ConcatPtg_get_Sid_m11BCD1D1295605D8B400464B54F93AB01137B6B9 (void);
// 0x00000CC6 System.Int32 NPOI.SS.Formula.PTG.ConcatPtg::get_NumberOfOperands()
extern void ConcatPtg_get_NumberOfOperands_m7F0745E548507149897B0C5CE782F8043D5FEDF5 (void);
// 0x00000CC7 System.String NPOI.SS.Formula.PTG.ConcatPtg::ToFormulaString(System.String[])
extern void ConcatPtg_ToFormulaString_m8C89DBA2E3B2EE0A40A9431E4EF4A31FEBA1A69F (void);
// 0x00000CC8 System.Void NPOI.SS.Formula.PTG.ConcatPtg::.cctor()
extern void ConcatPtg__cctor_mB363DB620F141F98687E49C89D8D2560421DB79B (void);
// 0x00000CC9 System.Void NPOI.SS.Formula.PTG.DeletedArea3DPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void DeletedArea3DPtg__ctor_m6EF521126B01A2EDB0BC27B88E9A3F26C71F1B9D (void);
// 0x00000CCA System.String NPOI.SS.Formula.PTG.DeletedArea3DPtg::ToFormulaString(NPOI.SS.Formula.IFormulaRenderingWorkbook)
extern void DeletedArea3DPtg_ToFormulaString_m6A796166617F11B2A1B04C0C01C30154045777FD (void);
// 0x00000CCB System.String NPOI.SS.Formula.PTG.DeletedArea3DPtg::ToFormulaString()
extern void DeletedArea3DPtg_ToFormulaString_mD1B51292E22A7AD2260B1B1A376DA18B332BE7A2 (void);
// 0x00000CCC System.Byte NPOI.SS.Formula.PTG.DeletedArea3DPtg::get_DefaultOperandClass()
extern void DeletedArea3DPtg_get_DefaultOperandClass_mC2D2D3C6819A27CDD92178CD7AD7A5C3CCA34962 (void);
// 0x00000CCD System.Int32 NPOI.SS.Formula.PTG.DeletedArea3DPtg::get_Size()
extern void DeletedArea3DPtg_get_Size_m5FA0090DA3FE821438485BAD13FD2DF7F496FD81 (void);
// 0x00000CCE System.Void NPOI.SS.Formula.PTG.DeletedArea3DPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void DeletedArea3DPtg_Write_mB38DFFF5DF25132B87F149BEAF6D78D6A3EF42E4 (void);
// 0x00000CCF System.Void NPOI.SS.Formula.PTG.DeletedRef3DPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void DeletedRef3DPtg__ctor_m3E2A836F3734E1B50342019980BB26C1BD43A021 (void);
// 0x00000CD0 System.String NPOI.SS.Formula.PTG.DeletedRef3DPtg::ToFormulaString(NPOI.SS.Formula.IFormulaRenderingWorkbook)
extern void DeletedRef3DPtg_ToFormulaString_mA29F22BD5DF4E0CB4A6027F58E517761DBBB1202 (void);
// 0x00000CD1 System.String NPOI.SS.Formula.PTG.DeletedRef3DPtg::ToFormulaString()
extern void DeletedRef3DPtg_ToFormulaString_m3E9D01E4A0C11BC475AE8972EBBC1955B91E4EBD (void);
// 0x00000CD2 System.Byte NPOI.SS.Formula.PTG.DeletedRef3DPtg::get_DefaultOperandClass()
extern void DeletedRef3DPtg_get_DefaultOperandClass_m75E6EA9A314D28B6D8B511A7D4D3BC5EF59070B0 (void);
// 0x00000CD3 System.Int32 NPOI.SS.Formula.PTG.DeletedRef3DPtg::get_Size()
extern void DeletedRef3DPtg_get_Size_m9AA84BBC8343269FB3E0E0796940CA16061F8DAC (void);
// 0x00000CD4 System.Void NPOI.SS.Formula.PTG.DeletedRef3DPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void DeletedRef3DPtg_Write_m8FF623E7B078C59AC9C6C1FA68F71A0C468812E4 (void);
// 0x00000CD5 System.Void NPOI.SS.Formula.PTG.DividePtg::.ctor()
extern void DividePtg__ctor_m723933048829DC88F43796458DF4948FFD728AA5 (void);
// 0x00000CD6 System.Byte NPOI.SS.Formula.PTG.DividePtg::get_Sid()
extern void DividePtg_get_Sid_m7E4F9F8A63147AAD5AE74FB338F08C50F9642D7A (void);
// 0x00000CD7 System.Int32 NPOI.SS.Formula.PTG.DividePtg::get_NumberOfOperands()
extern void DividePtg_get_NumberOfOperands_m6A340A5FCF0A5F664565BF48C5BEDFD72744B55C (void);
// 0x00000CD8 System.String NPOI.SS.Formula.PTG.DividePtg::ToFormulaString(System.String[])
extern void DividePtg_ToFormulaString_mC7A6B33708574CD986E92F070AB6B06AD2F97FB7 (void);
// 0x00000CD9 System.Void NPOI.SS.Formula.PTG.DividePtg::.cctor()
extern void DividePtg__cctor_mB8D74935473A3FBF4F134DF08B7A95FADB563DDF (void);
// 0x00000CDA System.Void NPOI.SS.Formula.PTG.EqualPtg::.ctor()
extern void EqualPtg__ctor_mAE4EF77D3C21A632A77B4894D05B750B985B2B6E (void);
// 0x00000CDB System.Byte NPOI.SS.Formula.PTG.EqualPtg::get_Sid()
extern void EqualPtg_get_Sid_m7B694A93BDA8B3D85B10EB8425445ABCE6CC4BFD (void);
// 0x00000CDC System.Int32 NPOI.SS.Formula.PTG.EqualPtg::get_NumberOfOperands()
extern void EqualPtg_get_NumberOfOperands_m25E0F33EBBC8FD445713BC314461A4F6A55802EA (void);
// 0x00000CDD System.String NPOI.SS.Formula.PTG.EqualPtg::ToFormulaString(System.String[])
extern void EqualPtg_ToFormulaString_m67B8FF445AAD0BFB838E936DBE67C569FF7BAC84 (void);
// 0x00000CDE System.Void NPOI.SS.Formula.PTG.EqualPtg::.cctor()
extern void EqualPtg__cctor_m540E26CFB1DD96360D30945E69D0261A71F6BFBE (void);
// 0x00000CDF NPOI.SS.Formula.PTG.ErrPtg NPOI.SS.Formula.PTG.ErrPtg::ValueOf(System.Int32)
extern void ErrPtg_ValueOf_m2E63C5E71C67E94DB0458D92D2766F657B879118 (void);
// 0x00000CE0 System.Void NPOI.SS.Formula.PTG.ErrPtg::.ctor(System.Int32)
extern void ErrPtg__ctor_m4B0C7672A84BD21F1F6DD108477F1F1A3D369426 (void);
// 0x00000CE1 System.Void NPOI.SS.Formula.PTG.ErrPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void ErrPtg__ctor_m431F52E388DF57CADE747EA81EAF2147EC8D80E5 (void);
// 0x00000CE2 System.Void NPOI.SS.Formula.PTG.ErrPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void ErrPtg_Write_mF7384BB304E85907D3084EE8DB267F91A9D1C85C (void);
// 0x00000CE3 System.String NPOI.SS.Formula.PTG.ErrPtg::ToFormulaString()
extern void ErrPtg_ToFormulaString_m3ACACCCE35EB73FDBF5360C9FB852198CF75BA72 (void);
// 0x00000CE4 System.Int32 NPOI.SS.Formula.PTG.ErrPtg::get_Size()
extern void ErrPtg_get_Size_m06661FDF57D23D49DDDB783334858BBAB369587A (void);
// 0x00000CE5 System.Void NPOI.SS.Formula.PTG.ErrPtg::.cctor()
extern void ErrPtg__cctor_mF147161A942B92CA89FCDB282C19C8EB3A336DBA (void);
// 0x00000CE6 System.Void NPOI.SS.Formula.PTG.ExpPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void ExpPtg__ctor_mB8B5CDC80DE308CF91A7FB62F85265344E7A69BE (void);
// 0x00000CE7 System.Void NPOI.SS.Formula.PTG.ExpPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void ExpPtg_Write_mCA80E85815AB7223BAEC88014E77E175325221ED (void);
// 0x00000CE8 System.Int32 NPOI.SS.Formula.PTG.ExpPtg::get_Size()
extern void ExpPtg_get_Size_m829D43F0A31FF1C849C268F1A0EAAE3166195ED2 (void);
// 0x00000CE9 System.Int16 NPOI.SS.Formula.PTG.ExpPtg::get_Row()
extern void ExpPtg_get_Row_m6C078D7586A2192B89114D6441A2CD2A3BC311B4 (void);
// 0x00000CEA System.Int16 NPOI.SS.Formula.PTG.ExpPtg::get_Column()
extern void ExpPtg_get_Column_m28118A28B4539E7A7F25FA857222847784C0764D (void);
// 0x00000CEB System.String NPOI.SS.Formula.PTG.ExpPtg::ToFormulaString()
extern void ExpPtg_ToFormulaString_mB35A6E530133E13217B0B0F9372E08C30A635911 (void);
// 0x00000CEC System.String NPOI.SS.Formula.PTG.ExpPtg::ToString()
extern void ExpPtg_ToString_m685C68EDBBA3645313768EF18D17A6045A765D3D (void);
// 0x00000CED System.String NPOI.SS.Formula.PTG.ExternSheetNameResolver::PrependSheetName(NPOI.SS.Formula.IFormulaRenderingWorkbook,System.Int32,System.String)
extern void ExternSheetNameResolver_PrependSheetName_m4BDDB7A6728C7EF8A16FE78CF3C18985E43C7D7E (void);
// 0x00000CEE NPOI.SS.Formula.PTG.FuncPtg NPOI.SS.Formula.PTG.FuncPtg::Create(NPOI.Util.ILittleEndianInput)
extern void FuncPtg_Create_m4B7E645A4000FDD9C18769EFC01969C7E5CD8051 (void);
// 0x00000CEF System.Void NPOI.SS.Formula.PTG.FuncPtg::.ctor(System.Int32,NPOI.SS.Formula.Function.FunctionMetadata)
extern void FuncPtg__ctor_m07D78592E5563AFF818D2129381A87566C9CB648 (void);
// 0x00000CF0 NPOI.SS.Formula.PTG.FuncPtg NPOI.SS.Formula.PTG.FuncPtg::Create(System.Int32)
extern void FuncPtg_Create_m1E6E689736498CF9E0DB085358399E9DE0D4E7BB (void);
// 0x00000CF1 System.Void NPOI.SS.Formula.PTG.FuncPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void FuncPtg_Write_m56BC7A08A87B3F345176AD211EB0F9EFDA2FCF08 (void);
// 0x00000CF2 System.Int32 NPOI.SS.Formula.PTG.FuncPtg::get_Size()
extern void FuncPtg_get_Size_m4E0FF689DB5A3AEDD222FA91E892A7B9519AB002 (void);
// 0x00000CF3 System.Void NPOI.SS.Formula.PTG.FuncVarPtg::.ctor(System.Int32,System.Int32,System.Byte[],System.Int32)
extern void FuncVarPtg__ctor_m7B95D4B182FCBF93500D6BEF9FD1E18CA34AD11D (void);
// 0x00000CF4 NPOI.SS.Formula.PTG.FuncVarPtg NPOI.SS.Formula.PTG.FuncVarPtg::Create(NPOI.Util.ILittleEndianInput)
extern void FuncVarPtg_Create_m1DE3EBF1343594F5D749DB050154360F98459C7F (void);
// 0x00000CF5 NPOI.SS.Formula.PTG.FuncVarPtg NPOI.SS.Formula.PTG.FuncVarPtg::Create(System.String,System.Int32)
extern void FuncVarPtg_Create_mEC5C8B9FCC6D626562C862AED63B669E47E813BC (void);
// 0x00000CF6 NPOI.SS.Formula.PTG.FuncVarPtg NPOI.SS.Formula.PTG.FuncVarPtg::Create(System.Int32,System.Int32)
extern void FuncVarPtg_Create_mD000C571F3F96B5E752B2EF41C0558C0A841B2C5 (void);
// 0x00000CF7 System.Void NPOI.SS.Formula.PTG.FuncVarPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void FuncVarPtg_Write_m3CDD2E9431888795FD271AE2A847B439089F3D7C (void);
// 0x00000CF8 System.Int32 NPOI.SS.Formula.PTG.FuncVarPtg::get_Size()
extern void FuncVarPtg_get_Size_mF4BDEA807BD6F3756247ABFBBD8CEB41C01CA67E (void);
// 0x00000CF9 System.Void NPOI.SS.Formula.PTG.FuncVarPtg::.cctor()
extern void FuncVarPtg__cctor_mB4C468FAE253AED34FFB74E3C64A452D8642918F (void);
// 0x00000CFA System.Void NPOI.SS.Formula.PTG.GreaterEqualPtg::.ctor()
extern void GreaterEqualPtg__ctor_m3258051F6265721ABF3AB09456BFBF127B53D21C (void);
// 0x00000CFB System.Byte NPOI.SS.Formula.PTG.GreaterEqualPtg::get_Sid()
extern void GreaterEqualPtg_get_Sid_mE8089C3C61FE1D445C45F4E18958264B9DF49510 (void);
// 0x00000CFC System.Int32 NPOI.SS.Formula.PTG.GreaterEqualPtg::get_NumberOfOperands()
extern void GreaterEqualPtg_get_NumberOfOperands_m92703B808AA6734459AA5141BCEE093C442D2BC5 (void);
// 0x00000CFD System.String NPOI.SS.Formula.PTG.GreaterEqualPtg::ToFormulaString(System.String[])
extern void GreaterEqualPtg_ToFormulaString_mEB92A7A5060DEBFB357C168E51D167393A9BB364 (void);
// 0x00000CFE System.Void NPOI.SS.Formula.PTG.GreaterEqualPtg::.cctor()
extern void GreaterEqualPtg__cctor_m0B48C2EB5E56E7EC67AF87666E5B80D497E6031C (void);
// 0x00000CFF System.Void NPOI.SS.Formula.PTG.GreaterThanPtg::.ctor()
extern void GreaterThanPtg__ctor_mFC29B29CC16904C21730B88980A4E9650AB632CD (void);
// 0x00000D00 System.Byte NPOI.SS.Formula.PTG.GreaterThanPtg::get_Sid()
extern void GreaterThanPtg_get_Sid_m2967150BC486C429BDEF63C6D0481EDE518D54DE (void);
// 0x00000D01 System.Int32 NPOI.SS.Formula.PTG.GreaterThanPtg::get_NumberOfOperands()
extern void GreaterThanPtg_get_NumberOfOperands_m8C017120E896C8547EB83AAC9C8660AA19758BD7 (void);
// 0x00000D02 System.String NPOI.SS.Formula.PTG.GreaterThanPtg::ToFormulaString(System.String[])
extern void GreaterThanPtg_ToFormulaString_m1CCDC221154A37EAB636CADA4237BB4DF5C45A7C (void);
// 0x00000D03 System.Void NPOI.SS.Formula.PTG.GreaterThanPtg::.cctor()
extern void GreaterThanPtg__cctor_mF4BB9D836426C6B50A590D332CAA6194074DEB91 (void);
// 0x00000D04 System.Void NPOI.SS.Formula.PTG.IntersectionPtg::.ctor()
extern void IntersectionPtg__ctor_mE79F00CC93677B0DC9288A0E86E0BFE44D249049 (void);
// 0x00000D05 System.Boolean NPOI.SS.Formula.PTG.IntersectionPtg::get_IsBaseToken()
extern void IntersectionPtg_get_IsBaseToken_m616EF909E6BF642C1A70F7F63FFCF729015A80FD (void);
// 0x00000D06 System.Int32 NPOI.SS.Formula.PTG.IntersectionPtg::get_Size()
extern void IntersectionPtg_get_Size_m1FC9DACD14C989D90934958B2DC203295E3D2759 (void);
// 0x00000D07 System.Void NPOI.SS.Formula.PTG.IntersectionPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void IntersectionPtg_Write_m8CA4EEE97558C9CF9D0493F81D1C2EC3BD8D4CB3 (void);
// 0x00000D08 System.String NPOI.SS.Formula.PTG.IntersectionPtg::ToFormulaString()
extern void IntersectionPtg_ToFormulaString_m00655AA3A7F3077C9B3B4FED82ACBB1DDB454989 (void);
// 0x00000D09 System.String NPOI.SS.Formula.PTG.IntersectionPtg::ToFormulaString(System.String[])
extern void IntersectionPtg_ToFormulaString_mA1B8BD7119F6173C49E98A2F44C557B1654EA6A9 (void);
// 0x00000D0A System.Int32 NPOI.SS.Formula.PTG.IntersectionPtg::get_NumberOfOperands()
extern void IntersectionPtg_get_NumberOfOperands_m6F9BCF69EA1E572AB879223B2FF6AE8D507F7CA4 (void);
// 0x00000D0B System.Void NPOI.SS.Formula.PTG.IntersectionPtg::.cctor()
extern void IntersectionPtg__cctor_m16EA527A97AA92C261C2A14DEE83A7429988C020 (void);
// 0x00000D0C System.Boolean NPOI.SS.Formula.PTG.IntPtg::IsInRange(System.Int32)
extern void IntPtg_IsInRange_m51549EFB756D1D44F9D3637B8464ED485FFE3CF7 (void);
// 0x00000D0D System.Void NPOI.SS.Formula.PTG.IntPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void IntPtg__ctor_mE52010129A048BACBE24ED9B4D8B09BBD519F6CD (void);
// 0x00000D0E System.Void NPOI.SS.Formula.PTG.IntPtg::.ctor(System.Int32)
extern void IntPtg__ctor_m40BDAF2591BC4DE414BE6677BC6C943AE3911DEB (void);
// 0x00000D0F System.Int32 NPOI.SS.Formula.PTG.IntPtg::get_Value()
extern void IntPtg_get_Value_mE136F8201D92006780E75BCFA51E51AB5D33E38B (void);
// 0x00000D10 System.Void NPOI.SS.Formula.PTG.IntPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void IntPtg_Write_mB852D52CB4BB1EC502B9FD5C237951BC2699E98D (void);
// 0x00000D11 System.Int32 NPOI.SS.Formula.PTG.IntPtg::get_Size()
extern void IntPtg_get_Size_m8B191FCE427AB321209A9A0C9BE73BBB5397959F (void);
// 0x00000D12 System.String NPOI.SS.Formula.PTG.IntPtg::ToFormulaString()
extern void IntPtg_ToFormulaString_m65EE6394E47A690EC78015A12C7FDE9981AFC941 (void);
// 0x00000D13 System.String NPOI.SS.Formula.PTG.IntPtg::ToString()
extern void IntPtg_ToString_m72BB00CD5526778158F53FBFE0DA1C2DFDF6026B (void);
// 0x00000D14 System.Void NPOI.SS.Formula.PTG.LessEqualPtg::.ctor()
extern void LessEqualPtg__ctor_m89040A0EDBAC284404DFC0E2A8D85EA3FCEE4BA5 (void);
// 0x00000D15 System.Byte NPOI.SS.Formula.PTG.LessEqualPtg::get_Sid()
extern void LessEqualPtg_get_Sid_m86FC7BA3A0DE5CF16ED10A269981E1533B0C8E65 (void);
// 0x00000D16 System.Int32 NPOI.SS.Formula.PTG.LessEqualPtg::get_NumberOfOperands()
extern void LessEqualPtg_get_NumberOfOperands_mC91114DFF12770D334376639565F3170D2CDF160 (void);
// 0x00000D17 System.String NPOI.SS.Formula.PTG.LessEqualPtg::ToFormulaString(System.String[])
extern void LessEqualPtg_ToFormulaString_m539560CF676FA206D54A694A78061BABAE3F5114 (void);
// 0x00000D18 System.Void NPOI.SS.Formula.PTG.LessEqualPtg::.cctor()
extern void LessEqualPtg__cctor_mF51EE80975A34FB5EE7820AD60A9D78604DB1B78 (void);
// 0x00000D19 System.Void NPOI.SS.Formula.PTG.LessThanPtg::.ctor()
extern void LessThanPtg__ctor_m44A025A6E5BA5467524522BE01B1844C675B9049 (void);
// 0x00000D1A System.Byte NPOI.SS.Formula.PTG.LessThanPtg::get_Sid()
extern void LessThanPtg_get_Sid_mD13EF9A714BBBAD25EA55FB11867321D8B55C76B (void);
// 0x00000D1B System.Int32 NPOI.SS.Formula.PTG.LessThanPtg::get_NumberOfOperands()
extern void LessThanPtg_get_NumberOfOperands_mC6514C08E5D59087BC7B6D0EA622CBC0CE7D09D7 (void);
// 0x00000D1C System.String NPOI.SS.Formula.PTG.LessThanPtg::ToFormulaString(System.String[])
extern void LessThanPtg_ToFormulaString_m7B319B98F4C325793AE566A56C5DBEC48E7BC630 (void);
// 0x00000D1D System.Void NPOI.SS.Formula.PTG.LessThanPtg::.cctor()
extern void LessThanPtg__cctor_mC9ED5EE2920A82BB915C9B7CFA8A1E71961D9F5A (void);
// 0x00000D1E System.Void NPOI.SS.Formula.PTG.MemAreaPtg::.ctor(System.Int32)
extern void MemAreaPtg__ctor_m9C175ACAFA04CF0CC843ACD445310B62399E463D (void);
// 0x00000D1F System.Void NPOI.SS.Formula.PTG.MemAreaPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void MemAreaPtg__ctor_m0CE1184C37439F924088631863E95FBEEA2D0898 (void);
// 0x00000D20 System.Void NPOI.SS.Formula.PTG.MemAreaPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void MemAreaPtg_Write_m5E094F84E747BF846CAA46CD56B33D6AE3FDFB43 (void);
// 0x00000D21 System.Int32 NPOI.SS.Formula.PTG.MemAreaPtg::get_Size()
extern void MemAreaPtg_get_Size_m6491721CC421662D71447489DD5E32AB428EDDC1 (void);
// 0x00000D22 System.String NPOI.SS.Formula.PTG.MemAreaPtg::ToFormulaString()
extern void MemAreaPtg_ToFormulaString_m69DAD1E1454F086ADDA1EFC96667DBEA7B4C84BC (void);
// 0x00000D23 System.Byte NPOI.SS.Formula.PTG.MemAreaPtg::get_DefaultOperandClass()
extern void MemAreaPtg_get_DefaultOperandClass_mACE078FC7DF3AAD733BBCD62412222E5E019A000 (void);
// 0x00000D24 System.String NPOI.SS.Formula.PTG.MemAreaPtg::ToString()
extern void MemAreaPtg_ToString_m1784065EBA580F547DEE077F91F35F3C75D71EED (void);
// 0x00000D25 System.Void NPOI.SS.Formula.PTG.MemErrPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void MemErrPtg__ctor_m22BE1FC4FEE1C114150FE5FBAA10F183578482A0 (void);
// 0x00000D26 System.Void NPOI.SS.Formula.PTG.MemErrPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void MemErrPtg_Write_m2190A83AD3A9EBB5F6AD4BB0842C9C97A2DB9080 (void);
// 0x00000D27 System.String NPOI.SS.Formula.PTG.MemErrPtg::ToFormulaString()
extern void MemErrPtg_ToFormulaString_m3848CB189A2556412C2D0068DE8769C41A756A48 (void);
// 0x00000D28 System.Void NPOI.SS.Formula.PTG.MemFuncPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void MemFuncPtg__ctor_mC3967FA59060A633861E323620F4ACA5FC2BE4D7 (void);
// 0x00000D29 System.Void NPOI.SS.Formula.PTG.MemFuncPtg::.ctor(System.Int32)
extern void MemFuncPtg__ctor_m7059EBC983A69A3EC4F2CE6E71FE7D1A88D14B66 (void);
// 0x00000D2A System.Int32 NPOI.SS.Formula.PTG.MemFuncPtg::get_Size()
extern void MemFuncPtg_get_Size_m480A6367A4E15798D7CAAD7282751BD12ADF5281 (void);
// 0x00000D2B System.Void NPOI.SS.Formula.PTG.MemFuncPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void MemFuncPtg_Write_m703B662F9C2B5624815014E935937FFBC978CDDA (void);
// 0x00000D2C System.String NPOI.SS.Formula.PTG.MemFuncPtg::ToFormulaString()
extern void MemFuncPtg_ToFormulaString_mDA23E2BCA8823B07C2E56F4AE63FB8F33446F893 (void);
// 0x00000D2D System.Byte NPOI.SS.Formula.PTG.MemFuncPtg::get_DefaultOperandClass()
extern void MemFuncPtg_get_DefaultOperandClass_m000F157939BE94EDEB6AD2FE0441DCF4D8E54720 (void);
// 0x00000D2E System.Void NPOI.SS.Formula.PTG.MissingArgPtg::.ctor()
extern void MissingArgPtg__ctor_m34F7D2A481AD3E89F382F22BCF03C0ACD88DB3DF (void);
// 0x00000D2F System.Void NPOI.SS.Formula.PTG.MissingArgPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void MissingArgPtg_Write_mACD4D14E2C40A8875FE86B47DE7639D450CD0B3A (void);
// 0x00000D30 System.Int32 NPOI.SS.Formula.PTG.MissingArgPtg::get_Size()
extern void MissingArgPtg_get_Size_m6BCB0C87A86B1A34E076780B27705ED9A8593A7D (void);
// 0x00000D31 System.String NPOI.SS.Formula.PTG.MissingArgPtg::ToFormulaString()
extern void MissingArgPtg_ToFormulaString_mADCFC7A1784842F958ACA4E77348DD967F0288BB (void);
// 0x00000D32 System.Void NPOI.SS.Formula.PTG.MissingArgPtg::.cctor()
extern void MissingArgPtg__cctor_m7C5540806CCB6E5E12B07AE926B6AC4B9BB73291 (void);
// 0x00000D33 System.Void NPOI.SS.Formula.PTG.MultiplyPtg::.ctor()
extern void MultiplyPtg__ctor_mA36C2367C803C561A03D8C7C4BDB34B8E5482650 (void);
// 0x00000D34 System.Byte NPOI.SS.Formula.PTG.MultiplyPtg::get_Sid()
extern void MultiplyPtg_get_Sid_m5F0702132FF12831751A2B31799E6C0932D18A16 (void);
// 0x00000D35 System.Int32 NPOI.SS.Formula.PTG.MultiplyPtg::get_NumberOfOperands()
extern void MultiplyPtg_get_NumberOfOperands_m0F1A85E560E3D40E16D6677D1A47E1111D748D11 (void);
// 0x00000D36 System.String NPOI.SS.Formula.PTG.MultiplyPtg::ToFormulaString(System.String[])
extern void MultiplyPtg_ToFormulaString_m2650D38909757C32FEEEEAE5155FD681198549A2 (void);
// 0x00000D37 System.Void NPOI.SS.Formula.PTG.MultiplyPtg::.cctor()
extern void MultiplyPtg__cctor_m4F65A901D1010E0E41E6E83BB698560C6FCBD0F8 (void);
// 0x00000D38 System.Void NPOI.SS.Formula.PTG.NamePtg::.ctor(System.Int32)
extern void NamePtg__ctor_mED709AFD2487988D704D6CD39572FEA3D50D1B54 (void);
// 0x00000D39 System.Void NPOI.SS.Formula.PTG.NamePtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void NamePtg__ctor_m2130067E42BB01B3E1EAF6E88C4FACF1E4809941 (void);
// 0x00000D3A System.Int32 NPOI.SS.Formula.PTG.NamePtg::get_Index()
extern void NamePtg_get_Index_mBE476DBD0577173C9FCEDF17BD048911EA69821B (void);
// 0x00000D3B System.Void NPOI.SS.Formula.PTG.NamePtg::Write(NPOI.Util.ILittleEndianOutput)
extern void NamePtg_Write_m7E7D383C9A4D89D33E58EFBCD6798DB7F3E2A3EA (void);
// 0x00000D3C System.Int32 NPOI.SS.Formula.PTG.NamePtg::get_Size()
extern void NamePtg_get_Size_m36371EAB1AED55535554ADFB8655784A450233F4 (void);
// 0x00000D3D System.String NPOI.SS.Formula.PTG.NamePtg::ToFormulaString(NPOI.SS.Formula.IFormulaRenderingWorkbook)
extern void NamePtg_ToFormulaString_m24A66B05F5ECBD41E1C5214B346CFCB6737AD073 (void);
// 0x00000D3E System.String NPOI.SS.Formula.PTG.NamePtg::ToFormulaString()
extern void NamePtg_ToFormulaString_m8BE26F0AB7CC689CFE5313482494A57871B003C0 (void);
// 0x00000D3F System.Byte NPOI.SS.Formula.PTG.NamePtg::get_DefaultOperandClass()
extern void NamePtg_get_DefaultOperandClass_m3424173D4E9B8FAF416C1E6516AADF0649781970 (void);
// 0x00000D40 System.Void NPOI.SS.Formula.PTG.NameXPtg::.ctor(System.Int32,System.Int32,System.Int32)
extern void NameXPtg__ctor_m5870792E09746836417C70631481FD4DDF72C068 (void);
// 0x00000D41 System.Void NPOI.SS.Formula.PTG.NameXPtg::.ctor(System.Int32,System.Int32)
extern void NameXPtg__ctor_m10F55461B20008E835765461D92C1262D6072C3D (void);
// 0x00000D42 System.Void NPOI.SS.Formula.PTG.NameXPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void NameXPtg__ctor_mC600250C02A1108874313B7192DD41C846580BDB (void);
// 0x00000D43 System.Void NPOI.SS.Formula.PTG.NameXPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void NameXPtg_Write_mA22E3611E5E59E9EE5AA777571F5CB245A201D68 (void);
// 0x00000D44 System.Int32 NPOI.SS.Formula.PTG.NameXPtg::get_Size()
extern void NameXPtg_get_Size_mCF8A906595F9793710BE6D58BBFC4A8D2658DE32 (void);
// 0x00000D45 System.String NPOI.SS.Formula.PTG.NameXPtg::ToFormulaString(NPOI.SS.Formula.IFormulaRenderingWorkbook)
extern void NameXPtg_ToFormulaString_m3BEF5B75C31F946091E5C757CC01DE69ACB4FEFA (void);
// 0x00000D46 System.String NPOI.SS.Formula.PTG.NameXPtg::ToFormulaString()
extern void NameXPtg_ToFormulaString_mEE06F315D850D0CEBACF407F23D335E9A8D809BB (void);
// 0x00000D47 System.Byte NPOI.SS.Formula.PTG.NameXPtg::get_DefaultOperandClass()
extern void NameXPtg_get_DefaultOperandClass_mB08CB060EF09212FCC9EAC27FC493E74716B854A (void);
// 0x00000D48 System.Int32 NPOI.SS.Formula.PTG.NameXPtg::get_SheetRefIndex()
extern void NameXPtg_get_SheetRefIndex_mF5E2E65E1479D244DEA2D27657AF642714986AE3 (void);
// 0x00000D49 System.Int32 NPOI.SS.Formula.PTG.NameXPtg::get_NameIndex()
extern void NameXPtg_get_NameIndex_mDC70B5D581F996B4DC3D277FE673AD9F08B14117 (void);
// 0x00000D4A System.Void NPOI.SS.Formula.PTG.NotEqualPtg::.ctor()
extern void NotEqualPtg__ctor_mD7D87B2549CE5EB9B922CB78283D08D6BCD042A4 (void);
// 0x00000D4B System.Byte NPOI.SS.Formula.PTG.NotEqualPtg::get_Sid()
extern void NotEqualPtg_get_Sid_m56FA2F0605AC7A1A091130CD4A5985C89C6B6784 (void);
// 0x00000D4C System.Int32 NPOI.SS.Formula.PTG.NotEqualPtg::get_NumberOfOperands()
extern void NotEqualPtg_get_NumberOfOperands_mE0B7424CF9B373D9F1B5B737E1E815EFBBDD3576 (void);
// 0x00000D4D System.String NPOI.SS.Formula.PTG.NotEqualPtg::ToFormulaString(System.String[])
extern void NotEqualPtg_ToFormulaString_m42F96EAA19C27594F67740DF4DA87C2382A23C1F (void);
// 0x00000D4E System.Void NPOI.SS.Formula.PTG.NotEqualPtg::.cctor()
extern void NotEqualPtg__cctor_m597D01547D08967EAAB00308A76EE20FB200F982 (void);
// 0x00000D4F System.Void NPOI.SS.Formula.PTG.NumberPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void NumberPtg__ctor_mFE22945A475E496AD37DFBE37F75F5993B2288D1 (void);
// 0x00000D50 System.Void NPOI.SS.Formula.PTG.NumberPtg::.ctor(System.String)
extern void NumberPtg__ctor_m8518FF5D66F93674181E098B402C6D7350A151BE (void);
// 0x00000D51 System.Void NPOI.SS.Formula.PTG.NumberPtg::.ctor(System.Double)
extern void NumberPtg__ctor_m2F806DF7449717F9869A5FA0206A07B980A833DF (void);
// 0x00000D52 System.Double NPOI.SS.Formula.PTG.NumberPtg::get_Value()
extern void NumberPtg_get_Value_m414AEEA91CC67A11DEA7CFF6AAB4C076D220DB40 (void);
// 0x00000D53 System.Void NPOI.SS.Formula.PTG.NumberPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void NumberPtg_Write_m375D063F51D55183CA32F994B418E6517D887594 (void);
// 0x00000D54 System.Int32 NPOI.SS.Formula.PTG.NumberPtg::get_Size()
extern void NumberPtg_get_Size_m76BEFF98391D28F7AD8456BB65686CE35C70CB41 (void);
// 0x00000D55 System.String NPOI.SS.Formula.PTG.NumberPtg::ToFormulaString()
extern void NumberPtg_ToFormulaString_m1A161471ABA10F5C3AE994EFCFB261CDEB851DAA (void);
// 0x00000D56 System.Void NPOI.SS.Formula.PTG.ParenthesisPtg::.ctor()
extern void ParenthesisPtg__ctor_m50B47035AB95E58A9FC0BD6B1E8F945C553E0CBB (void);
// 0x00000D57 System.Void NPOI.SS.Formula.PTG.ParenthesisPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void ParenthesisPtg_Write_m13D6A5B8C3C15E629FD53876F9B2CD58CF8AD828 (void);
// 0x00000D58 System.Int32 NPOI.SS.Formula.PTG.ParenthesisPtg::get_Size()
extern void ParenthesisPtg_get_Size_mC71DB7D373F7D6103A89E75FEC45E2282E956A3F (void);
// 0x00000D59 System.String NPOI.SS.Formula.PTG.ParenthesisPtg::ToFormulaString()
extern void ParenthesisPtg_ToFormulaString_m9C959E43A6BF1A9CCD9F741D03962C5940783E72 (void);
// 0x00000D5A System.Void NPOI.SS.Formula.PTG.ParenthesisPtg::.cctor()
extern void ParenthesisPtg__cctor_m092006373D05F283E6746EEAE8D33BD833EB7C0C (void);
// 0x00000D5B System.Void NPOI.SS.Formula.PTG.PercentPtg::.ctor()
extern void PercentPtg__ctor_m63CC498D93E721AE07C0B65880861310213493C2 (void);
// 0x00000D5C System.Byte NPOI.SS.Formula.PTG.PercentPtg::get_Sid()
extern void PercentPtg_get_Sid_mA69F08BCA765F4D6EBEBB44B730FD7536B2C8183 (void);
// 0x00000D5D System.Int32 NPOI.SS.Formula.PTG.PercentPtg::get_NumberOfOperands()
extern void PercentPtg_get_NumberOfOperands_mB6B0834194453CEE3720DFB87F6298F42C2FD1C8 (void);
// 0x00000D5E System.String NPOI.SS.Formula.PTG.PercentPtg::ToFormulaString(System.String[])
extern void PercentPtg_ToFormulaString_m32DC6B78151C6E97B99FD0E013DB82340C7338C8 (void);
// 0x00000D5F System.Void NPOI.SS.Formula.PTG.PercentPtg::.cctor()
extern void PercentPtg__cctor_m1A981623A669EA3E873FB3AF8B25A2DC524EABDA (void);
// 0x00000D60 System.Void NPOI.SS.Formula.PTG.PowerPtg::.ctor()
extern void PowerPtg__ctor_mA240EE0F20EA9E93CAEE754D2E18817B7B5C0B0F (void);
// 0x00000D61 System.Byte NPOI.SS.Formula.PTG.PowerPtg::get_Sid()
extern void PowerPtg_get_Sid_m584FF7750E9DB448488D6A0B20742411B34D5BA6 (void);
// 0x00000D62 System.Int32 NPOI.SS.Formula.PTG.PowerPtg::get_NumberOfOperands()
extern void PowerPtg_get_NumberOfOperands_mE8DB482CBDF8AC649094D242A84AD95C3405244A (void);
// 0x00000D63 System.String NPOI.SS.Formula.PTG.PowerPtg::ToFormulaString(System.String[])
extern void PowerPtg_ToFormulaString_m83CC92A7866A01633C38F34DF3785638882085EB (void);
// 0x00000D64 System.Void NPOI.SS.Formula.PTG.PowerPtg::.cctor()
extern void PowerPtg__cctor_m23AF0035228428F86E8281C026B20D29CC4BE767 (void);
// 0x00000D65 System.Void NPOI.SS.Formula.PTG.RangePtg::.ctor()
extern void RangePtg__ctor_m8F907E27F0E9D87D91FF0E00ED94A9005AC945BF (void);
// 0x00000D66 System.Boolean NPOI.SS.Formula.PTG.RangePtg::get_IsBaseToken()
extern void RangePtg_get_IsBaseToken_m7DA17F241C4822CD772251D33653282572E19E7F (void);
// 0x00000D67 System.Int32 NPOI.SS.Formula.PTG.RangePtg::get_Size()
extern void RangePtg_get_Size_m127189097A7D917CB40EEE7E4100EBCD79DE550B (void);
// 0x00000D68 System.Void NPOI.SS.Formula.PTG.RangePtg::Write(NPOI.Util.ILittleEndianOutput)
extern void RangePtg_Write_mEFD7E970987B73319B23D30816D71AD0D51AD165 (void);
// 0x00000D69 System.String NPOI.SS.Formula.PTG.RangePtg::ToFormulaString()
extern void RangePtg_ToFormulaString_mE445074FFFEA672AA87B543CC17492AE618D3A80 (void);
// 0x00000D6A System.String NPOI.SS.Formula.PTG.RangePtg::ToFormulaString(System.String[])
extern void RangePtg_ToFormulaString_mF6F91372D57E9687241197528D1EA42738FAE33D (void);
// 0x00000D6B System.Int32 NPOI.SS.Formula.PTG.RangePtg::get_NumberOfOperands()
extern void RangePtg_get_NumberOfOperands_mBF6F2A52FB9D2D4EEA568769A761E463E62F3A2B (void);
// 0x00000D6C System.Void NPOI.SS.Formula.PTG.RangePtg::.cctor()
extern void RangePtg__cctor_m1566E1AF3852C2EBEF0EB71C4F3AD5B1BEE2DE37 (void);
// 0x00000D6D System.Void NPOI.SS.Formula.PTG.RefPtgBase::.ctor()
extern void RefPtgBase__ctor_mCD06C55E5EE63543FEE6D674A406C488336EB91B (void);
// 0x00000D6E System.Void NPOI.SS.Formula.PTG.RefPtgBase::.ctor(NPOI.SS.Util.CellReference)
extern void RefPtgBase__ctor_m6632163B5816E5C1C5D3E34AF5D5FE0658D2C655 (void);
// 0x00000D6F System.Void NPOI.SS.Formula.PTG.RefPtgBase::ReadCoordinates(NPOI.Util.ILittleEndianInput)
extern void RefPtgBase_ReadCoordinates_m2B7388BF9AF3086D22FC4569F8EC8FF672B7C8F2 (void);
// 0x00000D70 System.Void NPOI.SS.Formula.PTG.RefPtgBase::WriteCoordinates(NPOI.Util.ILittleEndianOutput)
extern void RefPtgBase_WriteCoordinates_m3754E96E396CCC71C97695947828ADDA93457F75 (void);
// 0x00000D71 System.Int32 NPOI.SS.Formula.PTG.RefPtgBase::get_Row()
extern void RefPtgBase_get_Row_m79F554E80DDE625AB26C7B66E194822C35F2AFC0 (void);
// 0x00000D72 System.Void NPOI.SS.Formula.PTG.RefPtgBase::set_Row(System.Int32)
extern void RefPtgBase_set_Row_mA5400CA251353461DBB300ED29C2091E94C2B181 (void);
// 0x00000D73 System.Boolean NPOI.SS.Formula.PTG.RefPtgBase::get_IsRowRelative()
extern void RefPtgBase_get_IsRowRelative_m53BCEB7F7B615CC22D9F6716E3A2EE6411BEF29A (void);
// 0x00000D74 System.Void NPOI.SS.Formula.PTG.RefPtgBase::set_IsRowRelative(System.Boolean)
extern void RefPtgBase_set_IsRowRelative_mBD4039C7F4869ABD3812CBD3B1B63B91E03B99D4 (void);
// 0x00000D75 System.Boolean NPOI.SS.Formula.PTG.RefPtgBase::get_IsColRelative()
extern void RefPtgBase_get_IsColRelative_m9392E393FDA0410B3A94C949F02B4A7DA159440B (void);
// 0x00000D76 System.Void NPOI.SS.Formula.PTG.RefPtgBase::set_IsColRelative(System.Boolean)
extern void RefPtgBase_set_IsColRelative_m9666E259DE64D68CBB7AC80E6E60E35E4B263E89 (void);
// 0x00000D77 System.Int32 NPOI.SS.Formula.PTG.RefPtgBase::get_Column()
extern void RefPtgBase_get_Column_mC596B5277A95C27BBDF14561FD163030440CFC7F (void);
// 0x00000D78 System.Void NPOI.SS.Formula.PTG.RefPtgBase::set_Column(System.Int32)
extern void RefPtgBase_set_Column_m9113F5D2A3121F8142D151E115D6D98A91ED16C5 (void);
// 0x00000D79 System.String NPOI.SS.Formula.PTG.RefPtgBase::FormatReferenceAsString()
extern void RefPtgBase_FormatReferenceAsString_m6B59D043536822913C53438BBEC14ECB2EFC74D4 (void);
// 0x00000D7A System.Byte NPOI.SS.Formula.PTG.RefPtgBase::get_DefaultOperandClass()
extern void RefPtgBase_get_DefaultOperandClass_mE327EBEA32911F4B1DA7AA077E010E9A891C3A60 (void);
// 0x00000D7B System.Void NPOI.SS.Formula.PTG.RefPtgBase::.cctor()
extern void RefPtgBase__cctor_mDF6D1BFBA36501C67D612EB77DA767F5F64A8638 (void);
// 0x00000D7C System.Void NPOI.SS.Formula.PTG.Ref2DPtgBase::.ctor(NPOI.SS.Util.CellReference)
extern void Ref2DPtgBase__ctor_m2932C99158FDBAF44362733125B5CACCC8B34C38 (void);
// 0x00000D7D System.Void NPOI.SS.Formula.PTG.Ref2DPtgBase::.ctor(System.Int32,System.Int32,System.Boolean,System.Boolean)
extern void Ref2DPtgBase__ctor_m5C92B260431471FE61A93F4EB0A9E02C3FEFA9C6 (void);
// 0x00000D7E System.Void NPOI.SS.Formula.PTG.Ref2DPtgBase::.ctor(NPOI.Util.ILittleEndianInput)
extern void Ref2DPtgBase__ctor_m97D2E61085D7C77B6EC80AAE90DA75F3F15DC2EA (void);
// 0x00000D7F System.Void NPOI.SS.Formula.PTG.Ref2DPtgBase::Write(NPOI.Util.ILittleEndianOutput)
extern void Ref2DPtgBase_Write_mF941CF2B00DB0FE1111F5D1B3238FCEB5BCD0E16 (void);
// 0x00000D80 System.String NPOI.SS.Formula.PTG.Ref2DPtgBase::ToFormulaString()
extern void Ref2DPtgBase_ToFormulaString_m11F7C20ADC8B3BAA18A80289B5CE49B1E28C60B4 (void);
// 0x00000D81 System.Byte NPOI.SS.Formula.PTG.Ref2DPtgBase::get_Sid()
// 0x00000D82 System.Int32 NPOI.SS.Formula.PTG.Ref2DPtgBase::get_Size()
extern void Ref2DPtgBase_get_Size_m55F81CD84E0B06CD89621F3D287200E9283CEB09 (void);
// 0x00000D83 System.String NPOI.SS.Formula.PTG.Ref2DPtgBase::ToString()
extern void Ref2DPtgBase_ToString_mC663DE1A98D231E42E89E5679E72C3DA308C9B3E (void);
// 0x00000D84 System.Void NPOI.SS.Formula.PTG.Ref3DPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void Ref3DPtg__ctor_mBC70058378D3C76A9EDE5F20B19567BF7084ED0F (void);
// 0x00000D85 System.Void NPOI.SS.Formula.PTG.Ref3DPtg::.ctor(NPOI.SS.Util.CellReference,System.Int32)
extern void Ref3DPtg__ctor_m786918885CDD73662311FB20A5D65FA4911DBFC0 (void);
// 0x00000D86 System.String NPOI.SS.Formula.PTG.Ref3DPtg::ToString()
extern void Ref3DPtg_ToString_mBE3466452EEB4B5C16175F10CB725567F7DA2E3B (void);
// 0x00000D87 System.Void NPOI.SS.Formula.PTG.Ref3DPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void Ref3DPtg_Write_mDF6DAD9F340BB682DB517F001B6FD3D4AF513A90 (void);
// 0x00000D88 System.Int32 NPOI.SS.Formula.PTG.Ref3DPtg::get_Size()
extern void Ref3DPtg_get_Size_m46A8837A68379E92C13A37FEFE3F18B900AC4194 (void);
// 0x00000D89 System.Int32 NPOI.SS.Formula.PTG.Ref3DPtg::get_ExternSheetIndex()
extern void Ref3DPtg_get_ExternSheetIndex_m17CFD3E5B6CB6532A67FEE671D0D9918B134492E (void);
// 0x00000D8A System.Void NPOI.SS.Formula.PTG.Ref3DPtg::set_ExternSheetIndex(System.Int32)
extern void Ref3DPtg_set_ExternSheetIndex_m6A197FDA5C280177E9937AA1EE3101A918AB1F5D (void);
// 0x00000D8B System.String NPOI.SS.Formula.PTG.Ref3DPtg::ToFormulaString(NPOI.SS.Formula.IFormulaRenderingWorkbook)
extern void Ref3DPtg_ToFormulaString_m9F8CCF0F3C2A4B8DAE7EF03C70BA748033D40933 (void);
// 0x00000D8C System.String NPOI.SS.Formula.PTG.Ref3DPtg::ToFormulaString()
extern void Ref3DPtg_ToFormulaString_mA6C95D3B8E4DAD5B2D019C084EA95C9801700793 (void);
// 0x00000D8D System.Byte NPOI.SS.Formula.PTG.Ref3DPtg::get_DefaultOperandClass()
extern void Ref3DPtg_get_DefaultOperandClass_mC67499BB2BE75935E6CD043A315EF5B0F4262B07 (void);
// 0x00000D8E System.Void NPOI.SS.Formula.PTG.RefErrorPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void RefErrorPtg__ctor_m144E0FD6A6640B67D574D5FF5126F4E58E55C21E (void);
// 0x00000D8F System.String NPOI.SS.Formula.PTG.RefErrorPtg::ToString()
extern void RefErrorPtg_ToString_mE164732DA6C458DE12A7CCACDF8BECDC58948868 (void);
// 0x00000D90 System.Void NPOI.SS.Formula.PTG.RefErrorPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void RefErrorPtg_Write_m7EE4BD023D71FFBB404A0DA3D7F8DA08362BCA00 (void);
// 0x00000D91 System.Int32 NPOI.SS.Formula.PTG.RefErrorPtg::get_Reserved()
extern void RefErrorPtg_get_Reserved_m6A37614601F4D78BCD141D0399BFB35C4C7DA8C5 (void);
// 0x00000D92 System.Int32 NPOI.SS.Formula.PTG.RefErrorPtg::get_Size()
extern void RefErrorPtg_get_Size_m67452D35CF904766CFAD2A847E7982E7B065C559 (void);
// 0x00000D93 System.String NPOI.SS.Formula.PTG.RefErrorPtg::ToFormulaString()
extern void RefErrorPtg_ToFormulaString_m6A1CA5E16DD22FC21E43DA8D28D272E2B965022A (void);
// 0x00000D94 System.Byte NPOI.SS.Formula.PTG.RefErrorPtg::get_DefaultOperandClass()
extern void RefErrorPtg_get_DefaultOperandClass_mA90C6B8646480046765CF8CB3F62A60F94EB0725 (void);
// 0x00000D95 System.Void NPOI.SS.Formula.PTG.RefNPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void RefNPtg__ctor_mD0DEBEE8D8443D9B7F911289AD9EBD62011067EC (void);
// 0x00000D96 System.Byte NPOI.SS.Formula.PTG.RefNPtg::get_Sid()
extern void RefNPtg_get_Sid_mF2C6D124747BA74F0494DF0B92D0887513DC41AF (void);
// 0x00000D97 System.Void NPOI.SS.Formula.PTG.RefPtg::.ctor(System.Int32,System.Int32,System.Boolean,System.Boolean)
extern void RefPtg__ctor_m31091C5E00A22687AB73FC7D326CC76CE4DF0BC2 (void);
// 0x00000D98 System.Void NPOI.SS.Formula.PTG.RefPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void RefPtg__ctor_m1840930156C019DCB868E02F33A713D6507A986F (void);
// 0x00000D99 System.Void NPOI.SS.Formula.PTG.RefPtg::.ctor(NPOI.SS.Util.CellReference)
extern void RefPtg__ctor_m2C17DED88D445D95BFC960DA3D0A381AC7EA5DEF (void);
// 0x00000D9A System.Byte NPOI.SS.Formula.PTG.RefPtg::get_Sid()
extern void RefPtg_get_Sid_m03B950F8BEEDE490C8B856D9B8FA6C679A9EC08D (void);
// 0x00000D9B System.Void NPOI.SS.Formula.PTG.StringPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void StringPtg__ctor_mDD2F209B9D7F5D9125A8F1F8A1DA849F903E7598 (void);
// 0x00000D9C System.Void NPOI.SS.Formula.PTG.StringPtg::.ctor(System.String)
extern void StringPtg__ctor_m69B54E1CF1C92DBEE9908430206308B6DA967F20 (void);
// 0x00000D9D System.Void NPOI.SS.Formula.PTG.StringPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void StringPtg_Write_mA9FF22E16F79FB3E8E5527661ACFA0956ED15209 (void);
// 0x00000D9E System.Int32 NPOI.SS.Formula.PTG.StringPtg::get_Size()
extern void StringPtg_get_Size_m05A38E73046BA5F00563BE913754B4840C397E28 (void);
// 0x00000D9F System.String NPOI.SS.Formula.PTG.StringPtg::ToFormulaString()
extern void StringPtg_ToFormulaString_m7E64D03CBB702FE9259ECD581770AA599B5BB7F7 (void);
// 0x00000DA0 System.String NPOI.SS.Formula.PTG.StringPtg::ToString()
extern void StringPtg_ToString_mB9E77F1BA9BE117E0B67A9473A90FC03385899B9 (void);
// 0x00000DA1 System.Void NPOI.SS.Formula.PTG.StringPtg::.cctor()
extern void StringPtg__cctor_mAB36BCE60E7B43C9E512FEB9451CD185303320AA (void);
// 0x00000DA2 System.Void NPOI.SS.Formula.PTG.SubtractPtg::.ctor()
extern void SubtractPtg__ctor_m92BD64E2292076E20DA068460C167B8A7EA17A2D (void);
// 0x00000DA3 System.Byte NPOI.SS.Formula.PTG.SubtractPtg::get_Sid()
extern void SubtractPtg_get_Sid_m632842229595B69F6A2013E3F829EC3DBE73CAA0 (void);
// 0x00000DA4 System.Int32 NPOI.SS.Formula.PTG.SubtractPtg::get_NumberOfOperands()
extern void SubtractPtg_get_NumberOfOperands_m4E5A44D8C3B76964C7E453F7A9F3A2F4256A25C8 (void);
// 0x00000DA5 System.String NPOI.SS.Formula.PTG.SubtractPtg::ToFormulaString(System.String[])
extern void SubtractPtg_ToFormulaString_m8D38A7F1C2ADDFDFD389A81277C930ACF8E33E99 (void);
// 0x00000DA6 System.Void NPOI.SS.Formula.PTG.SubtractPtg::.cctor()
extern void SubtractPtg__cctor_m5F6F5DC4720F3EBEB91D7166C315D23C2B4ACD29 (void);
// 0x00000DA7 System.Void NPOI.SS.Formula.PTG.TblPtg::.ctor(NPOI.Util.ILittleEndianInput)
extern void TblPtg__ctor_m94971FA1DB5E4D3B613785443CE2B7908DF53B7D (void);
// 0x00000DA8 System.Void NPOI.SS.Formula.PTG.TblPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void TblPtg_Write_mB5D2B2215D3ECA98EA564740F221D340EDCCDE13 (void);
// 0x00000DA9 System.Int32 NPOI.SS.Formula.PTG.TblPtg::get_Size()
extern void TblPtg_get_Size_m817AFE04B726FC96EAAB66DC8008099B532BF214 (void);
// 0x00000DAA System.Int32 NPOI.SS.Formula.PTG.TblPtg::get_Row()
extern void TblPtg_get_Row_m36CB880D3353772181AECE6AF874862F4E43D53F (void);
// 0x00000DAB System.Int32 NPOI.SS.Formula.PTG.TblPtg::get_Column()
extern void TblPtg_get_Column_mFC4FF9466B369B94C300C60B7EE6BB55A8CE4877 (void);
// 0x00000DAC System.String NPOI.SS.Formula.PTG.TblPtg::ToFormulaString()
extern void TblPtg_ToFormulaString_mE999C51986D899A9465FF1C36B90FDFD70C8EC1F (void);
// 0x00000DAD System.String NPOI.SS.Formula.PTG.TblPtg::ToString()
extern void TblPtg_ToString_m43C7FA27BCF8B227171C0EE9D5B9A6CFAED6CA2A (void);
// 0x00000DAE System.Void NPOI.SS.Formula.PTG.UnaryMinusPtg::.ctor()
extern void UnaryMinusPtg__ctor_m649DAC719E7B77897D3660BF90A65CBC073FE2A8 (void);
// 0x00000DAF System.Byte NPOI.SS.Formula.PTG.UnaryMinusPtg::get_Sid()
extern void UnaryMinusPtg_get_Sid_m6FD1089EEE7047A632AC617A28F04D1171B6F0E4 (void);
// 0x00000DB0 System.Int32 NPOI.SS.Formula.PTG.UnaryMinusPtg::get_NumberOfOperands()
extern void UnaryMinusPtg_get_NumberOfOperands_m0B3B64C5CB403B11FCC55024BA70C2BC43F93E6C (void);
// 0x00000DB1 System.String NPOI.SS.Formula.PTG.UnaryMinusPtg::ToFormulaString(System.String[])
extern void UnaryMinusPtg_ToFormulaString_m97C315B319E04FB05945F2EC13DDB8EB5A1A9E15 (void);
// 0x00000DB2 System.Void NPOI.SS.Formula.PTG.UnaryMinusPtg::.cctor()
extern void UnaryMinusPtg__cctor_mA70BAC21219765255E5E6AE21F090FAC432AC47F (void);
// 0x00000DB3 System.Void NPOI.SS.Formula.PTG.UnaryPlusPtg::.ctor()
extern void UnaryPlusPtg__ctor_mF316973EB42D5655EF4F129270DFC169444EFF38 (void);
// 0x00000DB4 System.Byte NPOI.SS.Formula.PTG.UnaryPlusPtg::get_Sid()
extern void UnaryPlusPtg_get_Sid_m2D82EAD4F5FE4C5AFDAFADCFD222BF4F372064AF (void);
// 0x00000DB5 System.Int32 NPOI.SS.Formula.PTG.UnaryPlusPtg::get_NumberOfOperands()
extern void UnaryPlusPtg_get_NumberOfOperands_mB04C50EFAB4620F6228B7ABB214D40640E998202 (void);
// 0x00000DB6 System.String NPOI.SS.Formula.PTG.UnaryPlusPtg::ToFormulaString(System.String[])
extern void UnaryPlusPtg_ToFormulaString_mC3AE2389326298BE51B1AE1B5D4A1445D1549242 (void);
// 0x00000DB7 System.Void NPOI.SS.Formula.PTG.UnaryPlusPtg::.cctor()
extern void UnaryPlusPtg__cctor_mFBAE1AD75D76CA2C247968B9624347A55F79C649 (void);
// 0x00000DB8 System.Void NPOI.SS.Formula.PTG.UnionPtg::.ctor()
extern void UnionPtg__ctor_m20D17B7700CAD1350DE2433B18DACFF9FBA6B7EF (void);
// 0x00000DB9 System.Boolean NPOI.SS.Formula.PTG.UnionPtg::get_IsBaseToken()
extern void UnionPtg_get_IsBaseToken_m551F919F4A513418A8CD771BFB475760AA299074 (void);
// 0x00000DBA System.Int32 NPOI.SS.Formula.PTG.UnionPtg::get_Size()
extern void UnionPtg_get_Size_m96B347CD302F0C1991984A2E70BDF3148BF4BE39 (void);
// 0x00000DBB System.Void NPOI.SS.Formula.PTG.UnionPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void UnionPtg_Write_m01BCD3E246C86A22E3B006A4009FE554E8C2162E (void);
// 0x00000DBC System.String NPOI.SS.Formula.PTG.UnionPtg::ToFormulaString()
extern void UnionPtg_ToFormulaString_mC7C2DC045004140D377D7CDAF6F9FD007E7BB1D4 (void);
// 0x00000DBD System.String NPOI.SS.Formula.PTG.UnionPtg::ToFormulaString(System.String[])
extern void UnionPtg_ToFormulaString_mB925AC8C527CED120FD57A6108883D1C207976F0 (void);
// 0x00000DBE System.Int32 NPOI.SS.Formula.PTG.UnionPtg::get_NumberOfOperands()
extern void UnionPtg_get_NumberOfOperands_m29882FD8570040A6B2010CFB6178C66DA5D29B9A (void);
// 0x00000DBF System.Void NPOI.SS.Formula.PTG.UnionPtg::.cctor()
extern void UnionPtg__cctor_m8E227AB82DFC6D51A75898E74950EDEA29B05498 (void);
// 0x00000DC0 System.Void NPOI.SS.Formula.PTG.UnknownPtg::.ctor()
extern void UnknownPtg__ctor_mAB8A1741074512B1B9F10F3335A6DB1ECA6E8571 (void);
// 0x00000DC1 System.Boolean NPOI.SS.Formula.PTG.UnknownPtg::get_IsBaseToken()
extern void UnknownPtg_get_IsBaseToken_mA11A38AB7C039360D041AEB065A28466BBDB9C9E (void);
// 0x00000DC2 System.Void NPOI.SS.Formula.PTG.UnknownPtg::Write(NPOI.Util.ILittleEndianOutput)
extern void UnknownPtg_Write_mFF8209D7B21AE77D72EEA488370E30B0CC03FFB2 (void);
// 0x00000DC3 System.Int32 NPOI.SS.Formula.PTG.UnknownPtg::get_Size()
extern void UnknownPtg_get_Size_m73036F7A04C8D442E3615BB5AB124D6150F22407 (void);
// 0x00000DC4 System.String NPOI.SS.Formula.PTG.UnknownPtg::ToFormulaString()
extern void UnknownPtg_ToFormulaString_m492658C08085D3A98FD1D6868EF1BCB76922D49E (void);
// 0x00000DC5 System.Byte NPOI.SS.Formula.PTG.UnknownPtg::get_DefaultOperandClass()
extern void UnknownPtg_get_DefaultOperandClass_m4050DF16BEAF7D4E8B54F04CFB70FE44ADCAAA8E (void);
// 0x00000DC6 System.Object NPOI.SS.Formula.PTG.UnknownPtg::Clone()
extern void UnknownPtg_Clone_mA7E5424A72C3E2B7F93792D50FCD4E9BD78A644F (void);
// 0x00000DC7 System.String NPOI.SS.Formula.SheetNameFormatter::Format(System.String)
extern void SheetNameFormatter_Format_m974014B99C387F91DDFEAB7F9855269B02CA4828 (void);
// 0x00000DC8 System.Void NPOI.SS.Formula.SheetNameFormatter::AppendFormat(System.Text.StringBuilder,System.String)
extern void SheetNameFormatter_AppendFormat_m9B5094F38ED1C0B0C980B294C2A14FD7AC17B945 (void);
// 0x00000DC9 System.Void NPOI.SS.Formula.SheetNameFormatter::AppendFormat(System.Text.StringBuilder,System.String,System.String)
extern void SheetNameFormatter_AppendFormat_mC60EF6F98A25779130713F9F67358C3345D00EA9 (void);
// 0x00000DCA System.Void NPOI.SS.Formula.SheetNameFormatter::AppendAndEscape(System.Text.StringBuilder,System.String)
extern void SheetNameFormatter_AppendAndEscape_m9FA933482C2E177889E1A6D650DC197D68AF91B2 (void);
// 0x00000DCB System.Boolean NPOI.SS.Formula.SheetNameFormatter::NeedsDelimiting(System.String)
extern void SheetNameFormatter_NeedsDelimiting_m343469ACFDCC92F9B9F03C506DAAE300C98910D4 (void);
// 0x00000DCC System.Boolean NPOI.SS.Formula.SheetNameFormatter::NameLooksLikeBooleanLiteral(System.String)
extern void SheetNameFormatter_NameLooksLikeBooleanLiteral_m4C70C8D0B5CF91D2C1790C9BBED3D633C95670FB (void);
// 0x00000DCD System.Boolean NPOI.SS.Formula.SheetNameFormatter::IsSpecialChar(System.Char)
extern void SheetNameFormatter_IsSpecialChar_m9E7115414B914E058603A7268F875118C1713A70 (void);
// 0x00000DCE System.Boolean NPOI.SS.Formula.SheetNameFormatter::CellReferenceIsWithinRange(System.String,System.String)
extern void SheetNameFormatter_CellReferenceIsWithinRange_mDBA2370345687AFCDF8C6230C46778EA575888B3 (void);
// 0x00000DCF System.Boolean NPOI.SS.Formula.SheetNameFormatter::NameLooksLikePlainCellReference(System.String)
extern void SheetNameFormatter_NameLooksLikePlainCellReference_m0D43511414807DCB4FE41CEC1620A2531E7BF791 (void);
// 0x00000DD0 System.Void NPOI.SS.Formula.SheetNameFormatter::.cctor()
extern void SheetNameFormatter__cctor_m2A00D244FD59F7B78DC1AA6726D59F4CB33DED4C (void);
// 0x00000DD1 System.Void NPOI.SS.Formula.Udf.AggregatingUDFFinder::.ctor(NPOI.SS.Formula.Udf.UDFFinder[])
extern void AggregatingUDFFinder__ctor_mB9C34047EFEED19AE6532DA5D909D33DAEEC31E2 (void);
// 0x00000DD2 NPOI.SS.Formula.Functions.FreeRefFunction NPOI.SS.Formula.Udf.AggregatingUDFFinder::FindFunction(System.String)
extern void AggregatingUDFFinder_FindFunction_mAE4D33E5DE614DCEB6E0D50D3FEBA61049B50946 (void);
// 0x00000DD3 System.Void NPOI.SS.SpreadsheetVersion::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void SpreadsheetVersion__ctor_m0653C59C87D6252906235511B6A5599934F2BCF6 (void);
// 0x00000DD4 System.Int32 NPOI.SS.SpreadsheetVersion::get_MaxRows()
extern void SpreadsheetVersion_get_MaxRows_m80E6CEEE7233D746914B82F98C1AB501327860DC (void);
// 0x00000DD5 System.Int32 NPOI.SS.SpreadsheetVersion::get_LastRowIndex()
extern void SpreadsheetVersion_get_LastRowIndex_m7CEC5758DB68207FC9EAF5FDB9E6D3ABBA8316C7 (void);
// 0x00000DD6 System.Int32 NPOI.SS.SpreadsheetVersion::get_LastColumnIndex()
extern void SpreadsheetVersion_get_LastColumnIndex_m83583EB13FB068B4341519E837908455EFA94B57 (void);
// 0x00000DD7 System.Int32 NPOI.SS.SpreadsheetVersion::get_MaxFunctionArgs()
extern void SpreadsheetVersion_get_MaxFunctionArgs_mD82CEDA37AC54BDA3F4BA65AF804860C3753455D (void);
// 0x00000DD8 System.String NPOI.SS.SpreadsheetVersion::get_LastColumnName()
extern void SpreadsheetVersion_get_LastColumnName_mFF12078480A2B53A68BA8315C75824B7D2DF3E7E (void);
// 0x00000DD9 System.Int32 NPOI.SS.SpreadsheetVersion::get_MaxTextLength()
extern void SpreadsheetVersion_get_MaxTextLength_m3072FBF2D7E55DE393B3A8C3E15FE63743EFDA55 (void);
// 0x00000DDA System.Void NPOI.SS.SpreadsheetVersion::.cctor()
extern void SpreadsheetVersion__cctor_mE5F1168E4CE880F0101BEC268ECFB0A12FA475B3 (void);
// 0x00000DDB System.Void NPOI.SS.UserModel.BuiltinFormats::.cctor()
extern void BuiltinFormats__cctor_mEFDFB391266E5C138A12A04994A50730252056FC (void);
// 0x00000DDC System.Void NPOI.SS.UserModel.BuiltinFormats::PutFormat(System.Collections.Generic.List`1<System.String>,System.Int32,System.String)
extern void BuiltinFormats_PutFormat_m92D6B4D4976580BB05470B2ADB9BB43194497175 (void);
// 0x00000DDD System.String[] NPOI.SS.UserModel.BuiltinFormats::GetAll()
extern void BuiltinFormats_GetAll_mE37FC454F14487C5AF1F67B1B4110F8AEB1583DC (void);
// 0x00000DDE System.String NPOI.SS.UserModel.BuiltinFormats::GetBuiltinFormat(System.Int32)
extern void BuiltinFormats_GetBuiltinFormat_m58EE2BC476E35C0114F3E07009F58512B51B3B4E (void);
// 0x00000DDF System.Void NPOI.SS.Util.FormatBase::.ctor()
extern void FormatBase__ctor_m5DDEE8973CE6327218D6FB790EFE2EF8902F02AE (void);
// 0x00000DE0 System.String NPOI.SS.Util.FormatBase::Format(System.Object,System.Globalization.CultureInfo)
extern void FormatBase_Format_m824AE30BACFB62B5BA0072E0CDA6946CEA166601 (void);
// 0x00000DE1 System.Object NPOI.SS.Util.FormatBase::ParseObject(System.String,System.Int32)
// 0x00000DE2 System.Void NPOI.SS.Util.SimpleDateFormat::.ctor(System.String)
extern void SimpleDateFormat__ctor_m55829BEB655069C2026346E3C27D57022096D764 (void);
// 0x00000DE3 System.String NPOI.SS.Util.SimpleDateFormat::Format(System.Object,System.Globalization.CultureInfo)
extern void SimpleDateFormat_Format_mEAC99B457219581C050A3FE06E23823CE1E32685 (void);
// 0x00000DE4 System.Object NPOI.SS.Util.SimpleDateFormat::ParseObject(System.String,System.Int32)
extern void SimpleDateFormat_ParseObject_m36AA6D3FFF748668549EA9360690360F29188313 (void);
// 0x00000DE5 System.Void NPOI.SS.UserModel.ExcelStyleDateFormatter::.ctor(System.String)
extern void ExcelStyleDateFormatter__ctor_m3A90CA276B2C827152A22B95D1621E16C0765B8D (void);
// 0x00000DE6 System.String NPOI.SS.UserModel.ExcelStyleDateFormatter::ProcessFormatPattern(System.String)
extern void ExcelStyleDateFormatter_ProcessFormatPattern_m9C0144A75F48F0494C8EC6A60699F94F0B13E992 (void);
// 0x00000DE7 System.String NPOI.SS.UserModel.ExcelStyleDateFormatter::Format(System.Object,System.Globalization.CultureInfo)
extern void ExcelStyleDateFormatter_Format_mF8493BC1D05A966B7AF702BD9061061AA415C4BD (void);
// 0x00000DE8 System.Text.StringBuilder NPOI.SS.UserModel.ExcelStyleDateFormatter::Format(System.DateTime,System.Text.StringBuilder,System.Globalization.CultureInfo)
extern void ExcelStyleDateFormatter_Format_mDEDA0754190EFF5F1CE3F2FF6C7A7E51C92234AA (void);
// 0x00000DE9 System.Void NPOI.SS.UserModel.FormulaError::.ctor(System.Int32,System.String)
extern void FormulaError__ctor_mDBD1FCC265496B30C9316556A2A68F45E79C343F (void);
// 0x00000DEA System.Byte NPOI.SS.UserModel.FormulaError::get_Code()
extern void FormulaError_get_Code_m7F01BFC3A5C363A133236160F2969B60D53E4661 (void);
// 0x00000DEB System.String NPOI.SS.UserModel.FormulaError::get_String()
extern void FormulaError_get_String_m3B49928B93AEFE862F07E8826788B50A4D8F90CE (void);
// 0x00000DEC NPOI.SS.UserModel.FormulaError NPOI.SS.UserModel.FormulaError::ForInt(System.Byte)
extern void FormulaError_ForInt_m5F3575271AEDDBE340BBB474710E45705EC8CEA1 (void);
// 0x00000DED NPOI.SS.UserModel.FormulaError NPOI.SS.UserModel.FormulaError::ForString(System.String)
extern void FormulaError_ForString_mF2A40B55F3712EE76D2128805BC1C3EE41FF1D87 (void);
// 0x00000DEE System.Void NPOI.SS.UserModel.FormulaError::.cctor()
extern void FormulaError__cctor_m4FC2E63B1492B9A74BB1C7AC737CD09D95005BEE (void);
// 0x00000DEF System.Void NPOI.SS.UserModel.MissingCellPolicy::.ctor()
extern void MissingCellPolicy__ctor_mA72538F03C2668812708B24F25C693F280897F9E (void);
// 0x00000DF0 System.Void NPOI.SS.UserModel.MissingCellPolicy::.cctor()
extern void MissingCellPolicy__cctor_m94A927385AD40A3884FF223C207EEF58A8DC1770 (void);
// 0x00000DF1 System.Void NPOI.SS.Util.AreaReference::.ctor(System.String)
extern void AreaReference__ctor_m6E92717D13625367576E141E5E91E8E9899CF1C2 (void);
// 0x00000DF2 System.Boolean NPOI.SS.Util.AreaReference::IsPlainColumn(System.String)
extern void AreaReference_IsPlainColumn_m9A48FCC91B459F3733DBB39342BFF6D39FD51AB8 (void);
// 0x00000DF3 NPOI.SS.Util.AreaReference NPOI.SS.Util.AreaReference::GetWholeRow(System.String,System.String)
extern void AreaReference_GetWholeRow_m10B1F69EAD5506D883B4998EB1F3E8B60FE88638 (void);
// 0x00000DF4 NPOI.SS.Util.AreaReference NPOI.SS.Util.AreaReference::GetWholeColumn(System.String,System.String)
extern void AreaReference_GetWholeColumn_m08531C4F030808A6E4151FADA9890AEF8191E35B (void);
// 0x00000DF5 System.Void NPOI.SS.Util.AreaReference::.ctor(NPOI.SS.Util.CellReference,NPOI.SS.Util.CellReference)
extern void AreaReference__ctor_mCE36D6791894DF717978DED7120A219802093B06 (void);
// 0x00000DF6 System.Boolean NPOI.SS.Util.AreaReference::IsContiguous(System.String)
extern void AreaReference_IsContiguous_m5262992DA3B343683D5E1BE1FEF65906A6DCC89D (void);
// 0x00000DF7 System.Boolean NPOI.SS.Util.AreaReference::IsWholeColumnReference(NPOI.SS.Util.CellReference,NPOI.SS.Util.CellReference)
extern void AreaReference_IsWholeColumnReference_mE1FA2CC46EF512895267795FC3F0CC5A75ABC68E (void);
// 0x00000DF8 System.Boolean NPOI.SS.Util.AreaReference::IsWholeColumnReference()
extern void AreaReference_IsWholeColumnReference_mCE3F40823D6DEC12F89B357ED8463483F09A636C (void);
// 0x00000DF9 NPOI.SS.Util.CellReference NPOI.SS.Util.AreaReference::get_FirstCell()
extern void AreaReference_get_FirstCell_m0C3F651A4243EB63F99AB44053AD6711BF5D5C25 (void);
// 0x00000DFA NPOI.SS.Util.CellReference NPOI.SS.Util.AreaReference::get_LastCell()
extern void AreaReference_get_LastCell_m7AA221E50DADBCEED52DFE5E26CC43877C22878F (void);
// 0x00000DFB System.String NPOI.SS.Util.AreaReference::FormatAsString()
extern void AreaReference_FormatAsString_m53BB787492A094E95F82E3F87D33B4657D3CEBFF (void);
// 0x00000DFC System.String NPOI.SS.Util.AreaReference::ToString()
extern void AreaReference_ToString_m960A91E8E1335B900A169EF3EE8ACF9D8B619D60 (void);
// 0x00000DFD System.String[] NPOI.SS.Util.AreaReference::SeparateAreaRefs(System.String)
extern void AreaReference_SeparateAreaRefs_m0163E2F94B30EA5B271BE77443B941FB090E92A5 (void);
// 0x00000DFE System.Void NPOI.SS.Util.CellRangeAddressBase::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void CellRangeAddressBase__ctor_m513861A46225913583543B53D4A114DD63FFD861 (void);
// 0x00000DFF System.Boolean NPOI.SS.Util.CellRangeAddressBase::IsInRange(System.Int32,System.Int32)
extern void CellRangeAddressBase_IsInRange_mE18E101F8E1C6A01F60B6207613B80EAA4517400 (void);
// 0x00000E00 System.Boolean NPOI.SS.Util.CellRangeAddressBase::get_IsFullColumnRange()
extern void CellRangeAddressBase_get_IsFullColumnRange_mA4CC545B591EB9571F0C73324F5538B9169A70FE (void);
// 0x00000E01 System.Boolean NPOI.SS.Util.CellRangeAddressBase::get_IsFullRowRange()
extern void CellRangeAddressBase_get_IsFullRowRange_m55F485829AFA876F3291DA41D163DE7E970393D8 (void);
// 0x00000E02 System.Int32 NPOI.SS.Util.CellRangeAddressBase::get_FirstColumn()
extern void CellRangeAddressBase_get_FirstColumn_m34C04F347C9C6078F89BB6038E7869D0483EDCB8 (void);
// 0x00000E03 System.Int32 NPOI.SS.Util.CellRangeAddressBase::get_FirstRow()
extern void CellRangeAddressBase_get_FirstRow_mEA1AD7011F2CD4B5D154A43A7CBEB684CF2A5E89 (void);
// 0x00000E04 System.Int32 NPOI.SS.Util.CellRangeAddressBase::get_LastColumn()
extern void CellRangeAddressBase_get_LastColumn_mC9608C98A300394E6C16C07CC14ED888D5F7519C (void);
// 0x00000E05 System.Int32 NPOI.SS.Util.CellRangeAddressBase::get_LastRow()
extern void CellRangeAddressBase_get_LastRow_m7922F88203E11F96F4AC6B096B0F243E4559A92F (void);
// 0x00000E06 System.Int32 NPOI.SS.Util.CellRangeAddressBase::get_NumberOfCells()
extern void CellRangeAddressBase_get_NumberOfCells_m2CF30919B11B5E9A513D1020038C46276F74D24A (void);
// 0x00000E07 System.String NPOI.SS.Util.CellRangeAddressBase::ToString()
extern void CellRangeAddressBase_ToString_mC091D9ED4BB5A23C4A9BCE21DDF57011739FC07B (void);
// 0x00000E08 System.Void NPOI.SS.Util.CellRangeAddress::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void CellRangeAddress__ctor_m6964CCAAD84DA087C60F64D654BFF7B28B5C5036 (void);
// 0x00000E09 System.Void NPOI.SS.Util.CellRangeAddress::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CellRangeAddress_Serialize_mCF8819993C67901705734977253DFB90B854A766 (void);
// 0x00000E0A System.String NPOI.SS.Util.CellRangeAddress::FormatAsString()
extern void CellRangeAddress_FormatAsString_m0B37D03B665C96FCF5E0F2300C2214C63392BEE6 (void);
// 0x00000E0B System.String NPOI.SS.Util.CellRangeAddress::FormatAsString(System.String,System.Boolean)
extern void CellRangeAddress_FormatAsString_m9AE730F3E8277916055AACFB4B8C2DBA9937D521 (void);
// 0x00000E0C NPOI.SS.Util.CellRangeAddress NPOI.SS.Util.CellRangeAddress::Copy()
extern void CellRangeAddress_Copy_mF94B32AEDF14370CC6B64BE92CF7F9CECA14AADC (void);
// 0x00000E0D System.Int32 NPOI.SS.Util.CellRangeAddress::GetEncodedSize(System.Int32)
extern void CellRangeAddress_GetEncodedSize_mA7B454915F76259968B978A6F53046648F12FA8E (void);
// 0x00000E0E NPOI.SS.Util.CellRangeAddress NPOI.SS.Util.CellRangeAddress::ValueOf(System.String)
extern void CellRangeAddress_ValueOf_m6A63027E25DEF53BC731AA2375FB0C9E35FB4DC1 (void);
// 0x00000E0F System.Void NPOI.SS.Util.CellRangeAddress8Bit::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void CellRangeAddress8Bit__ctor_m0EC0802DFE5E63A15D29CE3E6ABBA5E07B3BB36B (void);
// 0x00000E10 System.Void NPOI.SS.Util.CellRangeAddress8Bit::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CellRangeAddress8Bit_Serialize_m1C11DA1576DD346A0B6F2CB0E5E8E60FE27ECA7B (void);
// 0x00000E11 System.Int32 NPOI.SS.Util.CellRangeAddress8Bit::GetEncodedSize(System.Int32)
extern void CellRangeAddress8Bit_GetEncodedSize_m060AC382C78CB55A225BFB2738D439D2C7490735 (void);
// 0x00000E12 System.Void NPOI.SS.Util.CellRangeAddressList::Serialize(NPOI.Util.ILittleEndianOutput)
extern void CellRangeAddressList_Serialize_mECBC4F5FE589DB1EB820AF69451D31A7D6B2032E (void);
// 0x00000E13 System.Int32 NPOI.SS.Util.CellRangeAddressList::get_Size()
extern void CellRangeAddressList_get_Size_mF088329B2E94C74C297F563071029FEE13CFE47B (void);
// 0x00000E14 System.Int32 NPOI.SS.Util.CellRangeAddressList::GetEncodedSize(System.Int32)
extern void CellRangeAddressList_GetEncodedSize_m20DC0B9B79B822EF03BA31A03B26B3925B456D8C (void);
// 0x00000E15 System.Void NPOI.SS.Util.CellReference::.ctor(System.String)
extern void CellReference__ctor_m9DA291B78BC76586AFAE6EF254888A50C3148E12 (void);
// 0x00000E16 System.Void NPOI.SS.Util.CellReference::.ctor(NPOI.SS.UserModel.ICell)
extern void CellReference__ctor_mBF41570E5C5B9B75A97434ED22D06F546A3DC839 (void);
// 0x00000E17 System.Void NPOI.SS.Util.CellReference::.ctor(System.Int32,System.Int32)
extern void CellReference__ctor_m2AB3376F1707F71F5FCBF815EE8A09599331C818 (void);
// 0x00000E18 System.Void NPOI.SS.Util.CellReference::.ctor(System.Int32,System.Int32,System.Boolean,System.Boolean)
extern void CellReference__ctor_m53C8DA42A140B4A247C05BEC1C4019A064D1BE58 (void);
// 0x00000E19 System.Void NPOI.SS.Util.CellReference::.ctor(System.String,System.Int32,System.Int32,System.Boolean,System.Boolean)
extern void CellReference__ctor_mCFE6FF3894EC60F6577D85C1C533A839DB5B05A3 (void);
// 0x00000E1A System.Int32 NPOI.SS.Util.CellReference::get_Row()
extern void CellReference_get_Row_mF70C04A56A2499D823C247E091A0B38590E7F0FF (void);
// 0x00000E1B System.Int16 NPOI.SS.Util.CellReference::get_Col()
extern void CellReference_get_Col_mB5458650AD434748C6A3DEDA98724685BF100766 (void);
// 0x00000E1C System.Boolean NPOI.SS.Util.CellReference::get_IsRowAbsolute()
extern void CellReference_get_IsRowAbsolute_m1914748618922FAFD3DE78066807DF4794E7656F (void);
// 0x00000E1D System.Boolean NPOI.SS.Util.CellReference::get_IsColAbsolute()
extern void CellReference_get_IsColAbsolute_mCE96B8739983C8288E52D0526507AA3981566A20 (void);
// 0x00000E1E System.String NPOI.SS.Util.CellReference::get_SheetName()
extern void CellReference_get_SheetName_m753CF1BCCABD92AEC5A674BC5D4F94952F9ECF18 (void);
// 0x00000E1F System.Int32 NPOI.SS.Util.CellReference::ConvertColStringToIndex(System.String)
extern void CellReference_ConvertColStringToIndex_m21A9F81F935C26CC0A9E98AB58D9B1FF59E48791 (void);
// 0x00000E20 System.Boolean NPOI.SS.Util.CellReference::IsPartAbsolute(System.String)
extern void CellReference_IsPartAbsolute_m57636C639940E76B8C3C18EE5D5A7A3B6F488833 (void);
// 0x00000E21 NPOI.SS.Util.NameType NPOI.SS.Util.CellReference::ClassifyCellReference(System.String,NPOI.SS.SpreadsheetVersion)
extern void CellReference_ClassifyCellReference_m3CFC8B45CD37615533D5BF51A0F83D6987BBA392 (void);
// 0x00000E22 NPOI.SS.Util.NameType NPOI.SS.Util.CellReference::ValidateNamedRangeName(System.String,NPOI.SS.SpreadsheetVersion)
extern void CellReference_ValidateNamedRangeName_m5A4ADAC0BBD07E7F1883BD312680CE9F12815B1A (void);
// 0x00000E23 System.String NPOI.SS.Util.CellReference::ConvertNumToColString(System.Int32)
extern void CellReference_ConvertNumToColString_mE1D4D5C9E0EB481B3DCA1D52ADFC5E707FC1030A (void);
// 0x00000E24 System.String[] NPOI.SS.Util.CellReference::SeparateRefParts(System.String)
extern void CellReference_SeparateRefParts_m6031C4557C60EDFD36EBDE37DDE8DF368608EE2D (void);
// 0x00000E25 System.String NPOI.SS.Util.CellReference::ParseSheetName(System.String,System.Int32)
extern void CellReference_ParseSheetName_mCA06225ABB48BE5F725604CB5B9342011C129752 (void);
// 0x00000E26 System.String NPOI.SS.Util.CellReference::FormatAsString()
extern void CellReference_FormatAsString_mAD8B736FB344548352F9639D7A29617BDE7009C7 (void);
// 0x00000E27 System.String NPOI.SS.Util.CellReference::ToString()
extern void CellReference_ToString_m894B5367C085A969B9D67BFC475FFC358FAF1EF6 (void);
// 0x00000E28 System.Void NPOI.SS.Util.CellReference::AppendCellReference(System.Text.StringBuilder)
extern void CellReference_AppendCellReference_m182A795925A7FF3B14E57C452C7A64A8FDAE95BE (void);
// 0x00000E29 System.Boolean NPOI.SS.Util.CellReference::CellReferenceIsWithinRange(System.String,System.String,NPOI.SS.SpreadsheetVersion)
extern void CellReference_CellReferenceIsWithinRange_m4C778E913DEB6AE3B3042EA7586557B6634AD41A (void);
// 0x00000E2A System.Boolean NPOI.SS.Util.CellReference::IsRowWithnRange(System.String,NPOI.SS.SpreadsheetVersion)
extern void CellReference_IsRowWithnRange_m35E2BC4C4DD03935FF8DD1A08422F97B112397EA (void);
// 0x00000E2B System.Boolean NPOI.SS.Util.CellReference::IsColumnWithnRange(System.String,NPOI.SS.SpreadsheetVersion)
extern void CellReference_IsColumnWithnRange_m151A82C8A6B54097BD17086161A37DDB624B3558 (void);
// 0x00000E2C System.Boolean NPOI.SS.Util.CellReference::Equals(System.Object)
extern void CellReference_Equals_m38040E7AEA719EF7E1ACB8A5EA1609D3B7B4B002 (void);
// 0x00000E2D System.Int32 NPOI.SS.Util.CellReference::GetHashCode()
extern void CellReference_GetHashCode_mADC43CC485CBA0A4D1BFD4906517F597EE5E5242 (void);
// 0x00000E2E NPOI.Util.BigInteger NPOI.SS.Util.ExpandedDouble::GetFrac(System.Int64)
extern void ExpandedDouble_GetFrac_m6999BAFA1C80B4B0CC71B16753B460418EB0B056 (void);
// 0x00000E2F System.Void NPOI.SS.Util.ExpandedDouble::.ctor(System.Int64)
extern void ExpandedDouble__ctor_mF998D7F7DB2B942480953A1893AB601B837A74A9 (void);
// 0x00000E30 NPOI.SS.Util.NormalisedDecimal NPOI.SS.Util.ExpandedDouble::NormaliseBaseTen()
extern void ExpandedDouble_NormaliseBaseTen_mF0A26045825B1FAB9F5A7FB6C080C4DC8E2167CC (void);
// 0x00000E31 System.Int32 NPOI.SS.Util.ExpandedDouble::GetBinaryExponent()
extern void ExpandedDouble_GetBinaryExponent_m7B3AFEB8424371FC34F5C309B5231A4C85147E4D (void);
// 0x00000E32 System.Void NPOI.SS.Util.ExpandedDouble::.cctor()
extern void ExpandedDouble__cctor_m73518193D9F68E12C75D3813B85559C9F9914A6B (void);
// 0x00000E33 System.Void NPOI.SS.Util.SSNFormat::.ctor()
extern void SSNFormat__ctor_mB19E7F833F5FCAECBF2392CC3101CF9892F68ACB (void);
// 0x00000E34 System.String NPOI.SS.Util.SSNFormat::Format(System.Object,System.Globalization.CultureInfo)
extern void SSNFormat_Format_m2034159DE0DBAAFAABEE87935F5CA878C5038787 (void);
// 0x00000E35 System.Object NPOI.SS.Util.SSNFormat::ParseObject(System.String,System.Int32)
extern void SSNFormat_ParseObject_m1D867B4739DD0EE632FFDB00ACA086D889C054A2 (void);
// 0x00000E36 System.Void NPOI.SS.Util.SSNFormat::.cctor()
extern void SSNFormat__cctor_mA60858D4620CCA95F1FC62A90EF608F6C9958146 (void);
// 0x00000E37 System.Void NPOI.SS.Util.ZipPlusFourFormat::.ctor()
extern void ZipPlusFourFormat__ctor_m1CED2DEF44219F690FD7BFA4D9F9AC564A2974FD (void);
// 0x00000E38 System.String NPOI.SS.Util.ZipPlusFourFormat::Format(System.Object,System.Globalization.CultureInfo)
extern void ZipPlusFourFormat_Format_m0BE99A1CC895B752A1AC9438EFCF50DE1786B069 (void);
// 0x00000E39 System.Object NPOI.SS.Util.ZipPlusFourFormat::ParseObject(System.String,System.Int32)
extern void ZipPlusFourFormat_ParseObject_m1FDC1DCB8300220E400C42781E5F46169AF78599 (void);
// 0x00000E3A System.Void NPOI.SS.Util.ZipPlusFourFormat::.cctor()
extern void ZipPlusFourFormat__cctor_m1CF7EC852C1720F605669EDFAAC246D3697C992D (void);
// 0x00000E3B System.Void NPOI.SS.Util.PhoneFormat::.ctor()
extern void PhoneFormat__ctor_m680503A6FB8D3F18FCE047B46910F4A120548280 (void);
// 0x00000E3C System.String NPOI.SS.Util.PhoneFormat::Format(System.Object,System.Globalization.CultureInfo)
extern void PhoneFormat_Format_m9305D82B8F07B4F4B15B80A0F7E3D76827F8DF25 (void);
// 0x00000E3D System.Object NPOI.SS.Util.PhoneFormat::ParseObject(System.String,System.Int32)
extern void PhoneFormat_ParseObject_m9EFD3C1BC42506610B5A4453977ABB632EB48555 (void);
// 0x00000E3E System.Void NPOI.SS.Util.PhoneFormat::.cctor()
extern void PhoneFormat__cctor_m4E909A1CB8C30FE5C035D1D5EBA8639973A14466 (void);
// 0x00000E3F System.Void NPOI.SS.Util.DecimalFormat::.ctor(System.String)
extern void DecimalFormat__ctor_m47D37833D47C51FA092375AADCE17B2BF94DFBF0 (void);
// 0x00000E40 System.String NPOI.SS.Util.DecimalFormat::Format(System.Object,System.Globalization.CultureInfo)
extern void DecimalFormat_Format_m9BE6C3AA92F4A7B1A8EB97E07913A8EE023A7929 (void);
// 0x00000E41 System.Object NPOI.SS.Util.DecimalFormat::ParseObject(System.String,System.Int32)
extern void DecimalFormat_ParseObject_mAFD1765CEA1A9F95760AC3A6FC17DC7D64AB20C4 (void);
// 0x00000E42 System.Void NPOI.SS.Util.DecimalFormat::.cctor()
extern void DecimalFormat__cctor_mB88E3D251664044F444643AB506F11ACC800DEB8 (void);
// 0x00000E43 System.Void NPOI.SS.Util.FractionFormat::.ctor(System.String)
extern void FractionFormat__ctor_m85E713261FCC439EDC96151DF83265EA613C655D (void);
// 0x00000E44 System.String NPOI.SS.Util.FractionFormat::Format(System.Double)
extern void FractionFormat_Format_mA13ADEB3A17A6ADC134C5CEE056EE1B97A20C7D9 (void);
// 0x00000E45 System.Int32 NPOI.SS.Util.FractionFormat::CountHashes(System.String)
extern void FractionFormat_CountHashes_m6ECCF24E33E5CBEEFCC76950D04228FE97FD3298 (void);
// 0x00000E46 System.String NPOI.SS.Util.FractionFormat::Format(System.Object,System.Globalization.CultureInfo)
extern void FractionFormat_Format_mB452B53136E0752CCA1FA80688F468271EFB5F81 (void);
// 0x00000E47 System.Object NPOI.SS.Util.FractionFormat::ParseObject(System.String,System.Int32)
extern void FractionFormat_ParseObject_mCFADE1808079A86D8ED5EB141989DC29C8BC7603 (void);
// 0x00000E48 System.Void NPOI.SS.Util.FractionFormat::.cctor()
extern void FractionFormat__cctor_mCE29544FD6BCA4C9E32FC520B525CD17CD190C13 (void);
// 0x00000E49 System.Void NPOI.SS.Util.ConstantStringFormat::.ctor(System.String)
extern void ConstantStringFormat__ctor_m5A8A53684B2BBA1DFD132FD975AF7DE6F1FEC629 (void);
// 0x00000E4A System.String NPOI.SS.Util.ConstantStringFormat::Format(System.Object,System.Globalization.CultureInfo)
extern void ConstantStringFormat_Format_m78BBAE67E53CC90FC6187786D30D3AB71E37CB37 (void);
// 0x00000E4B System.Object NPOI.SS.Util.ConstantStringFormat::ParseObject(System.String,System.Int32)
extern void ConstantStringFormat_ParseObject_m8511681DF00DD22EC47ADBEBBA73D6667B1492C0 (void);
// 0x00000E4C System.Void NPOI.SS.Util.ConstantStringFormat::.cctor()
extern void ConstantStringFormat__cctor_m5F6CFC2123430DFA7C59B303E1C9F1056E661DC0 (void);
// 0x00000E4D System.Void NPOI.SS.Util.MutableFPNumber::.ctor(NPOI.Util.BigInteger,System.Int32)
extern void MutableFPNumber__ctor_mDACD7CFEFF9124A272F856A2ED851753CF580DA1 (void);
// 0x00000E4E System.Void NPOI.SS.Util.MutableFPNumber::Normalise64bit()
extern void MutableFPNumber_Normalise64bit_m6538983CCE5F3031EC51FDCFAA92ED1671D71C57 (void);
// 0x00000E4F System.Int32 NPOI.SS.Util.MutableFPNumber::Get64BitNormalisedExponent()
extern void MutableFPNumber_Get64BitNormalisedExponent_m110E36B33D253F7048128F3D4B44EF5F94BE77E8 (void);
// 0x00000E50 System.Boolean NPOI.SS.Util.MutableFPNumber::IsBelowMaxRep()
extern void MutableFPNumber_IsBelowMaxRep_mAAFDD67C80C0BE47DD47D75D5CDBC18A50DCC1BF (void);
// 0x00000E51 System.Boolean NPOI.SS.Util.MutableFPNumber::IsAboveMinRep()
extern void MutableFPNumber_IsAboveMinRep_mDC1A6D5DE76D3734BA98C645AD6182876E024471 (void);
// 0x00000E52 NPOI.SS.Util.NormalisedDecimal NPOI.SS.Util.MutableFPNumber::CreateNormalisedDecimal(System.Int32)
extern void MutableFPNumber_CreateNormalisedDecimal_m9BDCA89E28CBF75021723EBBD7939105AB4DFC05 (void);
// 0x00000E53 System.Void NPOI.SS.Util.MutableFPNumber::multiplyByPowerOfTen(System.Int32)
extern void MutableFPNumber_multiplyByPowerOfTen_mE2602533FAD47C1A353D4130962C526A3ECB0E62 (void);
// 0x00000E54 System.Void NPOI.SS.Util.MutableFPNumber::mulShift(NPOI.Util.BigInteger,System.Int32)
extern void MutableFPNumber_mulShift_m040EA6C1FD53313779AF5936B12B7AE860A476CD (void);
// 0x00000E55 System.Void NPOI.SS.Util.MutableFPNumber::.cctor()
extern void MutableFPNumber__cctor_m65D7E57B8A44EF7EDE67D9BB42E803A773240170 (void);
// 0x00000E56 System.Void NPOI.SS.Util.MutableFPNumber_Rounder::.cctor()
extern void Rounder__cctor_m6CCAF86FF450C8A5C5A73FF37B7C372046981809 (void);
// 0x00000E57 NPOI.Util.BigInteger NPOI.SS.Util.MutableFPNumber_Rounder::Round(NPOI.Util.BigInteger,System.Int32)
extern void Rounder_Round_m82490AA089A7DBD17240899E561C4508ED03C7DF (void);
// 0x00000E58 System.Void NPOI.SS.Util.MutableFPNumber_TenPower::.ctor(System.Int32)
extern void TenPower__ctor_mF58646C1A1BCA06E5B09F8B431918F50C93B88B6 (void);
// 0x00000E59 NPOI.SS.Util.MutableFPNumber_TenPower NPOI.SS.Util.MutableFPNumber_TenPower::GetInstance(System.Int32)
extern void TenPower_GetInstance_mA72AA8A3869EDEAB2C579064BC33ED13E08F1FCA (void);
// 0x00000E5A System.Void NPOI.SS.Util.MutableFPNumber_TenPower::.cctor()
extern void TenPower__cctor_m49907AA7D1C11D5AB62AB32C54F1353E96096DC5 (void);
// 0x00000E5B NPOI.SS.Util.NormalisedDecimal NPOI.SS.Util.NormalisedDecimal::Create(NPOI.Util.BigInteger,System.Int32)
extern void NormalisedDecimal_Create_mFDFC63E360B5BCE4FE3996B76A59B51FC95316AA (void);
// 0x00000E5C NPOI.SS.Util.NormalisedDecimal NPOI.SS.Util.NormalisedDecimal::RoundUnits()
extern void NormalisedDecimal_RoundUnits_mB0643CCC8B90FC92C8D989BB3A014D8A309EB28D (void);
// 0x00000E5D System.Void NPOI.SS.Util.NormalisedDecimal::.ctor(System.Int64,System.Int32,System.Int32)
extern void NormalisedDecimal__ctor_m1496EA6187A0507F661CB957D44E62BC0E7C4A7E (void);
// 0x00000E5E System.String NPOI.SS.Util.NormalisedDecimal::GetSignificantDecimalDigits()
extern void NormalisedDecimal_GetSignificantDecimalDigits_mC3EEF83247CAA8580359A3DF183D3AFA3A19C7C4 (void);
// 0x00000E5F System.String NPOI.SS.Util.NormalisedDecimal::GetSignificantDecimalDigitsLastDigitRounded()
extern void NormalisedDecimal_GetSignificantDecimalDigitsLastDigitRounded_mB17E8944C8CAC63561B0BDB37FD2A0F21276EE79 (void);
// 0x00000E60 System.Int32 NPOI.SS.Util.NormalisedDecimal::GetDecimalExponent()
extern void NormalisedDecimal_GetDecimalExponent_mA70FA799B406C260DB4A597394103A3A7112FA7A (void);
// 0x00000E61 System.Decimal NPOI.SS.Util.NormalisedDecimal::GetFractionalPart()
extern void NormalisedDecimal_GetFractionalPart_m1E891C8F4AFD5FDDB6F453B49FDFD020E13DD10E (void);
// 0x00000E62 System.String NPOI.SS.Util.NormalisedDecimal::GetFractionalDigits()
extern void NormalisedDecimal_GetFractionalDigits_m66CBD39FA6F0114C99B296503CD2173355FD2011 (void);
// 0x00000E63 System.String NPOI.SS.Util.NormalisedDecimal::ToString()
extern void NormalisedDecimal_ToString_mFA3BF2C420B8B6C42F39D03C77D1FFEECB7F5DBE (void);
// 0x00000E64 System.Void NPOI.SS.Util.NormalisedDecimal::.cctor()
extern void NormalisedDecimal__cctor_mDCF13703BE5000C4E64EF18F40B779E75E9CDC13 (void);
// 0x00000E65 System.String NPOI.SS.Util.NumberToTextConverter::ToText(System.Double)
extern void NumberToTextConverter_ToText_m80E6E05EF0B18F7C2D82C7FFED2B8B0C337C7E55 (void);
// 0x00000E66 System.String NPOI.SS.Util.NumberToTextConverter::RawDoubleBitsToText(System.Int64)
extern void NumberToTextConverter_RawDoubleBitsToText_m996C3954C99445C294202D82020A9C9AFA4E7CF9 (void);
// 0x00000E67 System.Void NPOI.SS.Util.NumberToTextConverter::ConvertToText(System.Text.StringBuilder,NPOI.SS.Util.NormalisedDecimal)
extern void NumberToTextConverter_ConvertToText_mDAAE07BA6BDCFC355E07A5C1A5691FB04B18CF40 (void);
// 0x00000E68 System.Void NPOI.SS.Util.NumberToTextConverter::FormatLessThanOne(System.Text.StringBuilder,System.String,System.Int32,System.Int32)
extern void NumberToTextConverter_FormatLessThanOne_m6383002A7A8499914F7B61C63C6A68168E5D3FE0 (void);
// 0x00000E69 System.Void NPOI.SS.Util.NumberToTextConverter::FormatGreaterThanOne(System.Text.StringBuilder,System.String,System.Int32,System.Int32)
extern void NumberToTextConverter_FormatGreaterThanOne_mA45CE88A44574380E02B85CEC994DFD42D612DF1 (void);
// 0x00000E6A System.Boolean NPOI.SS.Util.NumberToTextConverter::NeedsScientificNotation(System.Int32)
extern void NumberToTextConverter_NeedsScientificNotation_m62F24EAD17EC565DE346AE8883139E0EE7458492 (void);
// 0x00000E6B System.Int32 NPOI.SS.Util.NumberToTextConverter::CountSignifantDigits(System.String)
extern void NumberToTextConverter_CountSignifantDigits_mC9FFCF348C094972AB05E55AFAC3E86931D661F1 (void);
// 0x00000E6C System.Void NPOI.SS.Util.NumberToTextConverter::AppendExp(System.Text.StringBuilder,System.Int32)
extern void NumberToTextConverter_AppendExp_mE623BADDFBAA6F11C213EFEB45854B12D93B7689 (void);
// 0x00000E6D System.Void NPOI.SS.Util.SSCellRange`1::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,K[])
// 0x00000E6E NPOI.SS.Util.SSCellRange`1<K> NPOI.SS.Util.SSCellRange`1::Create(System.Int32,System.Int32,System.Int32,System.Int32,System.Collections.Generic.List`1<K>,System.Type)
// 0x00000E6F System.Collections.Generic.IEnumerator`1<K> NPOI.SS.Util.SSCellRange`1::GetEnumerator()
// 0x00000E70 System.Collections.IEnumerator NPOI.SS.Util.SSCellRange`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000E71 System.Void NPOI.SS.Util.SSCellRange`1_ArrayIterator`1::.ctor(D[])
// 0x00000E72 System.Boolean NPOI.SS.Util.SSCellRange`1_ArrayIterator`1::MoveNext()
// 0x00000E73 System.Void NPOI.SS.Util.SSCellRange`1_ArrayIterator`1::Reset()
// 0x00000E74 D NPOI.SS.Util.SSCellRange`1_ArrayIterator`1::get_Current()
// 0x00000E75 System.Void NPOI.SS.Util.SSCellRange`1_ArrayIterator`1::Dispose()
// 0x00000E76 System.Object NPOI.SS.Util.SSCellRange`1_ArrayIterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000E77 System.Void NPOI.SS.Util.WorkbookUtil::ValidateSheetName(System.String)
extern void WorkbookUtil_ValidateSheetName_mDED7A7A081A5EF4DC70225E15652FE18E1FCCCC0 (void);
// 0x00000E78 System.Void NPOI.Util.Arrays::Fill(System.Byte[],System.Byte)
extern void Arrays_Fill_m5A44FF369636AB4812A73107CEFCC329E580A909 (void);
// 0x00000E79 System.Boolean NPOI.Util.Arrays::Equals(System.Object,System.Object)
extern void Arrays_Equals_m6DBCBB63CF8F34776881CF03E8FF72F15B94172F (void);
// 0x00000E7A System.Int32[] NPOI.Util.Arrays::CopyOfRange(System.Int32[],System.Int32,System.Int32)
extern void Arrays_CopyOfRange_m8262C1A4B753EAA021E2AF5692575C33534D19B3 (void);
// 0x00000E7B System.Void NPOI.Util.BigInteger::.cctor()
extern void BigInteger__cctor_m03B31BB7EAF2686B75E2F8EE0E6414A9EDA4B592 (void);
// 0x00000E7C System.Void NPOI.Util.BigInteger::Init()
extern void BigInteger_Init_mA1724ACD6AD153FDEC83540FFCA17C83E9653855 (void);
// 0x00000E7D System.Void NPOI.Util.BigInteger::.ctor(System.Int32[],System.Int32)
extern void BigInteger__ctor_mAFC5FDB262A33A85F0BCC6A4C06B70AF4FCACAC1 (void);
// 0x00000E7E System.Void NPOI.Util.BigInteger::.ctor(System.Int32[])
extern void BigInteger__ctor_mE9FBA1AF5C802F74405384420DCABA8B606E9F14 (void);
// 0x00000E7F System.Void NPOI.Util.BigInteger::.ctor(System.Int64)
extern void BigInteger__ctor_m63C178579EFB35B2556E70810B2A994C1A6AF086 (void);
// 0x00000E80 System.Int32[] NPOI.Util.BigInteger::TrustedStripLeadingZeroInts(System.Int32[])
extern void BigInteger_TrustedStripLeadingZeroInts_mFC4BFBEADD08449A230930FC8E25D249315DD756 (void);
// 0x00000E81 NPOI.Util.BigInteger NPOI.Util.BigInteger::ValueOf(System.Int64)
extern void BigInteger_ValueOf_mE92722A72FD10E320409F7724C98EAF26DD0AD3A (void);
// 0x00000E82 NPOI.Util.BigInteger NPOI.Util.BigInteger::ValueOf(System.Int32[])
extern void BigInteger_ValueOf_m610ED8DCBCF6B68BBFEEB6B038733F2F150E0969 (void);
// 0x00000E83 System.Int32 NPOI.Util.BigInteger::BitLengthForInt(System.Int32)
extern void BigInteger_BitLengthForInt_mDE733E1E30CFC64FE3696241F526D017BDA30C49 (void);
// 0x00000E84 System.Int32 NPOI.Util.BigInteger::BitLength()
extern void BigInteger_BitLength_mFD945F9F5A7C7A584C953F746D6D495142AE7500 (void);
// 0x00000E85 NPOI.Util.BigInteger NPOI.Util.BigInteger::Pow(System.Int32)
extern void BigInteger_Pow_mB8ACAE706C2C89598198ABCB66956FB3D0AEF3B3 (void);
// 0x00000E86 System.Int32[] NPOI.Util.BigInteger::MultiplyToLen(System.Int32[],System.Int32,System.Int32[],System.Int32,System.Int32[])
extern void BigInteger_MultiplyToLen_m32030999786563D51D74BA6561F3926836AD2A82 (void);
// 0x00000E87 System.Int32 NPOI.Util.BigInteger::mulAdd(System.Int32[],System.Int32[],System.Int32,System.Int32,System.Int32)
extern void BigInteger_mulAdd_mCB70F353891318DBBB4508AF1D7F3843903695E5 (void);
// 0x00000E88 System.Int32[] NPOI.Util.BigInteger::squareToLen(System.Int32[],System.Int32,System.Int32[])
extern void BigInteger_squareToLen_mA2C06566CFB132DEE67B73613D168985F4122BA2 (void);
// 0x00000E89 System.Void NPOI.Util.BigInteger::PrimitiveLeftShift(System.Int32[],System.Int32,System.Int32)
extern void BigInteger_PrimitiveLeftShift_m3AE4B9F92BB0A324B89C3C54E1473EDEE1ECA5DC (void);
// 0x00000E8A System.Int32 NPOI.Util.BigInteger::addOne(System.Int32[],System.Int32,System.Int32,System.Int32)
extern void BigInteger_addOne_m26B7604EF4991B35D996395BACEE49AE7C2B460C (void);
// 0x00000E8B System.Int32 NPOI.Util.BigInteger::intLength()
extern void BigInteger_intLength_mA2A33CFF8C11754202D6F72DEDE203914DD13F3F (void);
// 0x00000E8C System.Int32 NPOI.Util.BigInteger::signInt()
extern void BigInteger_signInt_mF7ED72253FE9007C12EAF140BA3D6891CE421527 (void);
// 0x00000E8D System.Int32 NPOI.Util.BigInteger::GetInt(System.Int32)
extern void BigInteger_GetInt_m900BA5491C988D5A0EB62C244879E9890B5776F3 (void);
// 0x00000E8E System.Int32 NPOI.Util.BigInteger::FirstNonzeroIntNum()
extern void BigInteger_FirstNonzeroIntNum_mF4B813E70E36797B9D264548174B613CF8CC793E (void);
// 0x00000E8F System.Int32[] NPOI.Util.BigInteger::makePositive(System.Int32[])
extern void BigInteger_makePositive_m73E8520919F427FDD23933DD7A684336855ED02C (void);
// 0x00000E90 System.Int32 NPOI.Util.BigInteger::NumberOfLeadingZeros(System.Int32)
extern void BigInteger_NumberOfLeadingZeros_m3B514A1048D1AB141413FCF4E4BFA488A2EE06F2 (void);
// 0x00000E91 System.Int32 NPOI.Util.BigInteger::BitCountForInt(System.Int32)
extern void BigInteger_BitCountForInt_m641781FCD5DD2C22770BF2F6B9142A07FD5A0BA6 (void);
// 0x00000E92 System.Int32 NPOI.Util.BigInteger::CompareTo(NPOI.Util.BigInteger)
extern void BigInteger_CompareTo_mB81B65FDEA90C6419B468C56030BC10367107A04 (void);
// 0x00000E93 System.Int32 NPOI.Util.BigInteger::compareMagnitude(NPOI.Util.BigInteger)
extern void BigInteger_compareMagnitude_m24B0A802F50B750A24BD375CF78412D7754413AB (void);
// 0x00000E94 System.Boolean NPOI.Util.BigInteger::Equals(System.Object)
extern void BigInteger_Equals_m7B5EDB496A03526E16F79128B78C62DEB83D0FB1 (void);
// 0x00000E95 System.Int32 NPOI.Util.BigInteger::GetHashCode()
extern void BigInteger_GetHashCode_m01B2D69D364B4B3DC74BBF7B254D9FFD17DD3950 (void);
// 0x00000E96 System.Int32 NPOI.Util.BigInteger::IntValue()
extern void BigInteger_IntValue_m9006863772CA25E235BDCF308B2E7B81C3092A59 (void);
// 0x00000E97 NPOI.Util.BigInteger NPOI.Util.BigInteger::ShiftLeft(System.Int32)
extern void BigInteger_ShiftLeft_m9DA6FAE0CC9739A6125A336D4E3A7A04C7DF3B76 (void);
// 0x00000E98 System.Int64 NPOI.Util.BigInteger::LongValue()
extern void BigInteger_LongValue_mD780DE30AD906DDE0F5A521D851C3F58609D5CBC (void);
// 0x00000E99 NPOI.Util.BigInteger NPOI.Util.BigInteger::ShiftRight(System.Int32)
extern void BigInteger_ShiftRight_m121E3736131CD74242B0A2C69CACCF5ECEC4F666 (void);
// 0x00000E9A System.Int32[] NPOI.Util.BigInteger::Increment(System.Int32[])
extern void BigInteger_Increment_mB48C1D8CD90788751B8282847636234D9CEE9C94 (void);
// 0x00000E9B NPOI.Util.BigInteger NPOI.Util.BigInteger::and(NPOI.Util.BigInteger)
extern void BigInteger_and_m72FBA8800494CEB80C9297CE48FA6ECCAE7D96C0 (void);
// 0x00000E9C NPOI.Util.BigInteger NPOI.Util.BigInteger::Or(NPOI.Util.BigInteger)
extern void BigInteger_Or_mB7E0CF40CBFE3F98B1DCEE626DB51244ACB6B218 (void);
// 0x00000E9D NPOI.Util.BigInteger NPOI.Util.BigInteger::Multiply(NPOI.Util.BigInteger)
extern void BigInteger_Multiply_m7042C76B0A354ABAF8EA8B0E54A34C4FC7CFF3FF (void);
// 0x00000E9E NPOI.Util.BigInteger NPOI.Util.BigInteger::Add(NPOI.Util.BigInteger)
extern void BigInteger_Add_m6C1B39461B440662DD7263F3EAB77F279A51FC2C (void);
// 0x00000E9F System.Int32[] NPOI.Util.BigInteger::add(System.Int32[],System.Int32[])
extern void BigInteger_add_m279031CC95AB6E2275ACF76F304CB72AA61F669F (void);
// 0x00000EA0 System.Int32[] NPOI.Util.BigInteger::Subtract(System.Int32[],System.Int32[])
extern void BigInteger_Subtract_mE8128D2BDE75E57CD37437BC30B947C4EBDD0421 (void);
// 0x00000EA1 NPOI.Util.BigInteger NPOI.Util.BigInteger::Divide(NPOI.Util.BigInteger)
extern void BigInteger_Divide_mED98325A2D60E12905E2D88FF5CFDDEF355B6DBF (void);
// 0x00000EA2 NPOI.Util.BigInteger NPOI.Util.BigInteger::op_RightShift(NPOI.Util.BigInteger,System.Int32)
extern void BigInteger_op_RightShift_mB98854BF605C032DFE9ED60E34A733AE1C672F0B (void);
// 0x00000EA3 NPOI.Util.BigInteger NPOI.Util.BigInteger::op_LeftShift(NPOI.Util.BigInteger,System.Int32)
extern void BigInteger_op_LeftShift_m884739A657C91F0460175D55B179542DC71D4B60 (void);
// 0x00000EA4 NPOI.Util.BigInteger NPOI.Util.BigInteger::op_BitwiseAnd(NPOI.Util.BigInteger,NPOI.Util.BigInteger)
extern void BigInteger_op_BitwiseAnd_mDEC6AF8E34218F3198969B99908C6F4030460088 (void);
// 0x00000EA5 NPOI.Util.BigInteger NPOI.Util.BigInteger::op_BitwiseOr(NPOI.Util.BigInteger,NPOI.Util.BigInteger)
extern void BigInteger_op_BitwiseOr_mC56696B69EBD4B0A5EC61952225B983BD0E98270 (void);
// 0x00000EA6 NPOI.Util.BigInteger NPOI.Util.BigInteger::op_Multiply(NPOI.Util.BigInteger,NPOI.Util.BigInteger)
extern void BigInteger_op_Multiply_m0491622D4B2C8CC6F9F18EAE8978B28083B4DA65 (void);
// 0x00000EA7 NPOI.Util.BigInteger NPOI.Util.BigInteger::op_Addition(NPOI.Util.BigInteger,NPOI.Util.BigInteger)
extern void BigInteger_op_Addition_mE2454552AF008E85C9CFA97744BD539E4EABD6CC (void);
// 0x00000EA8 NPOI.Util.BigInteger NPOI.Util.BigInteger::op_Division(NPOI.Util.BigInteger,NPOI.Util.BigInteger)
extern void BigInteger_op_Division_m3FDE7CD999102BEB4CB3890E80CE1D9914B5065E (void);
// 0x00000EA9 System.Void NPOI.Util.MutableBigInteger::.ctor()
extern void MutableBigInteger__ctor_m1EEE90956FFB238F8BF1503C70789DD3399E0355 (void);
// 0x00000EAA System.Void NPOI.Util.MutableBigInteger::.ctor(System.Int32)
extern void MutableBigInteger__ctor_m9068565F5592C295D38D2B2A3B7638BA6C18D6B4 (void);
// 0x00000EAB System.Void NPOI.Util.MutableBigInteger::.ctor(System.Int32[])
extern void MutableBigInteger__ctor_mFAB746FBE1143BCC16BE535A832A39FCCAED1955 (void);
// 0x00000EAC System.Int32[] NPOI.Util.MutableBigInteger::ArraysCopyOfRange(System.Int32[],System.Int32,System.Int32)
extern void MutableBigInteger_ArraysCopyOfRange_mFF2781B6A042487A162D3F61C80D9EF15B6BD3A7 (void);
// 0x00000EAD System.Void NPOI.Util.MutableBigInteger::.ctor(NPOI.Util.MutableBigInteger)
extern void MutableBigInteger__ctor_m8537F6D7050FD9AE3F137EB038064DAFAB469E67 (void);
// 0x00000EAE System.Int32[] NPOI.Util.MutableBigInteger::getMagnitudeArray()
extern void MutableBigInteger_getMagnitudeArray_m90D072FA050E8CD7714496A858C67DC0BA654F3D (void);
// 0x00000EAF NPOI.Util.BigInteger NPOI.Util.MutableBigInteger::toBigInteger(System.Int32)
extern void MutableBigInteger_toBigInteger_m8634C9B2A5F5023AFA55CE381F10D423C783C666 (void);
// 0x00000EB0 System.Void NPOI.Util.MutableBigInteger::clear()
extern void MutableBigInteger_clear_m9438262C2CBA306037AD04DD7DC4B39EB91C29A0 (void);
// 0x00000EB1 System.Int32 NPOI.Util.MutableBigInteger::compare(NPOI.Util.MutableBigInteger)
extern void MutableBigInteger_compare_m130AA2FBD0523B7C9EEF093E34C79F4A05579D07 (void);
// 0x00000EB2 System.Void NPOI.Util.MutableBigInteger::normalize()
extern void MutableBigInteger_normalize_m6A829F4B80032E707BD4E0037A0E914EFB71FB31 (void);
// 0x00000EB3 System.Void NPOI.Util.MutableBigInteger::setValue(System.Int32[],System.Int32)
extern void MutableBigInteger_setValue_m9F3D725C9CCDD82140E8B7434AF186C3A71987CB (void);
// 0x00000EB4 System.Void NPOI.Util.MutableBigInteger::rightShift(System.Int32)
extern void MutableBigInteger_rightShift_mAE314FBD7996242288029D08A73CE844EACDC929 (void);
// 0x00000EB5 System.Void NPOI.Util.MutableBigInteger::leftShift(System.Int32)
extern void MutableBigInteger_leftShift_mB8A22158D4A0E5001356DA223E1981B9E5BA4A3C (void);
// 0x00000EB6 System.Int32 NPOI.Util.MutableBigInteger::divadd(System.Int32[],System.Int32[],System.Int32)
extern void MutableBigInteger_divadd_m0AA4D7F236075C35D466D5795BD7152F183D17E4 (void);
// 0x00000EB7 System.Int32 NPOI.Util.MutableBigInteger::mulsub(System.Int32[],System.Int32[],System.Int32,System.Int32,System.Int32)
extern void MutableBigInteger_mulsub_m85BAD4FBE14BE354B302E209FE63385BE105690F (void);
// 0x00000EB8 System.Void NPOI.Util.MutableBigInteger::primitiveRightShift(System.Int32)
extern void MutableBigInteger_primitiveRightShift_m4B127B7526C3D6FF51D5E60841AE0ABFDB9CF5F6 (void);
// 0x00000EB9 System.Void NPOI.Util.MutableBigInteger::primitiveLeftShift(System.Int32)
extern void MutableBigInteger_primitiveLeftShift_m20B90AB3E1B6476F63CF970160AF9650E0DDA321 (void);
// 0x00000EBA System.Int32 NPOI.Util.MutableBigInteger::divideOneWord(System.Int32,NPOI.Util.MutableBigInteger)
extern void MutableBigInteger_divideOneWord_mA07B5EB9E3A27A3EEA6D509295A6BB1C90C0D688 (void);
// 0x00000EBB NPOI.Util.MutableBigInteger NPOI.Util.MutableBigInteger::divide(NPOI.Util.MutableBigInteger,NPOI.Util.MutableBigInteger)
extern void MutableBigInteger_divide_m0ECCB9D5BE05D568676E9947483ADE8E2B4DC02E (void);
// 0x00000EBC NPOI.Util.MutableBigInteger NPOI.Util.MutableBigInteger::divideMagnitude(System.Int32[],NPOI.Util.MutableBigInteger)
extern void MutableBigInteger_divideMagnitude_mD6DE8DAED117A8DB028C9DDDE58C43135E0FB160 (void);
// 0x00000EBD System.Boolean NPOI.Util.MutableBigInteger::unsignedLongCompare(System.Int64,System.Int64)
extern void MutableBigInteger_unsignedLongCompare_m4CBC08BE1D27CCEBE328CA0DAA9F9CE697A13990 (void);
// 0x00000EBE System.Void NPOI.Util.MutableBigInteger::divWord(System.Int32[],System.Int64,System.Int32)
extern void MutableBigInteger_divWord_m381F6B63761EF53DA05AFB1008F4F9403B6B6159 (void);
// 0x00000EBF System.Void NPOI.Util.MutableBigInteger::.cctor()
extern void MutableBigInteger__cctor_mFCDB0CFA2AD1642BB367DCC1AB533BC4764BA61C (void);
// 0x00000EC0 System.Void NPOI.Util.BitField::.ctor(System.Int32)
extern void BitField__ctor_m5CD8E28AFB42A0CB6CB782BEDD6349055A44FA45 (void);
// 0x00000EC1 System.Int32 NPOI.Util.BitField::Clear(System.Int32)
extern void BitField_Clear_mDEF6676B745582552F6DCAA019D5F7296913A057 (void);
// 0x00000EC2 System.Int16 NPOI.Util.BitField::ClearShort(System.Int16)
extern void BitField_ClearShort_m87C09E296908A249F1EF37496CD65D678FB2DAF6 (void);
// 0x00000EC3 System.Int32 NPOI.Util.BitField::GetRawValue(System.Int32)
extern void BitField_GetRawValue_mF51C82E6CD24AE3A2C3D451E8EC0203F552DBB06 (void);
// 0x00000EC4 System.Int16 NPOI.Util.BitField::GetShortValue(System.Int16)
extern void BitField_GetShortValue_mFC51F5B19ED26B4F14BBF6631D1AAEAEFA38F5AB (void);
// 0x00000EC5 System.Int32 NPOI.Util.BitField::GetValue(System.Int32)
extern void BitField_GetValue_m55E378DCB7D5F0499CF3B6E550AA1B541F931A97 (void);
// 0x00000EC6 System.Boolean NPOI.Util.BitField::IsSet(System.Int32)
extern void BitField_IsSet_m1ED44B529C844D7699119715608FC0E4F338098F (void);
// 0x00000EC7 System.Int32 NPOI.Util.BitField::Set(System.Int32)
extern void BitField_Set_m12B83A2BD5F9268ADCF761E6408E377BEDEF7DCE (void);
// 0x00000EC8 System.Int32 NPOI.Util.BitField::SetBoolean(System.Int32,System.Boolean)
extern void BitField_SetBoolean_mCC228C9A0FA0EEE04F020BE092A957362694E3F0 (void);
// 0x00000EC9 System.Int16 NPOI.Util.BitField::SetShort(System.Int16)
extern void BitField_SetShort_m2E49A070F0CDA23A5402CEF35660474335D731E4 (void);
// 0x00000ECA System.Int16 NPOI.Util.BitField::SetShortBoolean(System.Int16,System.Boolean)
extern void BitField_SetShortBoolean_mEB3E65DB9EB5F6F179B249C4D954CD527F213B06 (void);
// 0x00000ECB System.Int16 NPOI.Util.BitField::SetShortValue(System.Int16,System.Int16)
extern void BitField_SetShortValue_m3EA5D9080D508710AF22FA0364CFCC4155861443 (void);
// 0x00000ECC System.Int32 NPOI.Util.BitField::SetValue(System.Int32,System.Int32)
extern void BitField_SetValue_m3B396B7F64519E079BA0703849383F6118451F6F (void);
// 0x00000ECD System.Byte NPOI.Util.BitField::ClearByte(System.Byte)
extern void BitField_ClearByte_m246B0695279569828AD8098CB553B3AA5E9211B1 (void);
// 0x00000ECE System.Byte NPOI.Util.BitField::SetByte(System.Byte)
extern void BitField_SetByte_mD89E96AEC7CF03C79E1B2036169EC87ADED64B92 (void);
// 0x00000ECF NPOI.Util.BitField NPOI.Util.BitFieldFactory::GetInstance(System.Int32)
extern void BitFieldFactory_GetInstance_m19F8E3319B9498FF8EE3066B76C7E3531762DF73 (void);
// 0x00000ED0 System.Void NPOI.Util.BitFieldFactory::.cctor()
extern void BitFieldFactory__cctor_mD5D302B6C5F60962B5F396DED2A2D83149A8AA1F (void);
// 0x00000ED1 System.Void NPOI.Util.ByteField::.ctor(System.Int32)
extern void ByteField__ctor_m55462DF74145676AC636B10648BCD7FAABFE9538 (void);
// 0x00000ED2 System.Void NPOI.Util.ByteField::.ctor(System.Int32,System.Byte)
extern void ByteField__ctor_mBF5CCEE8850EE95D4ACF08CF63EA915A43981245 (void);
// 0x00000ED3 System.Void NPOI.Util.ByteField::.ctor(System.Int32,System.Byte[])
extern void ByteField__ctor_m247A69125A8FBD4A3B8B88D166E21B4DA6D9A7AB (void);
// 0x00000ED4 System.Void NPOI.Util.ByteField::set_Value(System.Byte)
extern void ByteField_set_Value_m0FCF2117E13754B8AE600BB6CDDBD472A9103835 (void);
// 0x00000ED5 System.Void NPOI.Util.ByteField::ReadFromBytes(System.Byte[])
extern void ByteField_ReadFromBytes_m983920715CC65DF0FACD9F799816549E24E10B79 (void);
// 0x00000ED6 System.Void NPOI.Util.ByteField::Set(System.Byte,System.Byte[])
extern void ByteField_Set_m14015FDBEECCEBF94A6FD5F43D312CF9D54F0C6F (void);
// 0x00000ED7 System.String NPOI.Util.ByteField::ToString()
extern void ByteField_ToString_mDF043EF879D8D9D549CFB4E670CBA3460CD5707F (void);
// 0x00000ED8 System.Void NPOI.Util.ByteField::WriteToBytes(System.Byte[])
extern void ByteField_WriteToBytes_m6036CF46D3FB23B30BD7EC5EE4B10587418BA2F0 (void);
// 0x00000ED9 System.Int32 NPOI.Util.Character::GetNumericValue(System.Char)
extern void Character_GetNumericValue_m9C2ABA1A05E0328E2D082A13811C255B223746C4 (void);
// 0x00000EDA System.Void NPOI.Util.ClassID::.ctor(System.Byte[],System.Int32)
extern void ClassID__ctor_mCE617454E404E7D81C2137A14AFB249A93500C61 (void);
// 0x00000EDB System.Void NPOI.Util.ClassID::.ctor()
extern void ClassID__ctor_mBA7A611793EF8D17CE13B160420B14C649D29C1F (void);
// 0x00000EDC System.Int32 NPOI.Util.ClassID::get_Length()
extern void ClassID_get_Length_m9ACDC1CB2CFF7D69FE8447AEE02F29EFFF5893D8 (void);
// 0x00000EDD System.Byte[] NPOI.Util.ClassID::get_Bytes()
extern void ClassID_get_Bytes_m06F4ADEB47F992DBE040CF8564DE1ACE6F607AA9 (void);
// 0x00000EDE System.Void NPOI.Util.ClassID::set_Bytes(System.Byte[])
extern void ClassID_set_Bytes_m37490D37A942E0E7A773ADE5C86EF18F654C16B1 (void);
// 0x00000EDF System.Byte[] NPOI.Util.ClassID::Read(System.Byte[],System.Int32)
extern void ClassID_Read_mC69943882CA6FA13C394B30CE871DECC35D477EE (void);
// 0x00000EE0 System.Void NPOI.Util.ClassID::Write(System.Byte[],System.Int32)
extern void ClassID_Write_m860E4F3050B1FFCB5732F68A6E34C616D83B7B69 (void);
// 0x00000EE1 System.Boolean NPOI.Util.ClassID::Equals(System.Object)
extern void ClassID_Equals_m1B368E827BDAD3DF2C2BCAA64333274BFF9A5F7B (void);
// 0x00000EE2 System.Int32 NPOI.Util.ClassID::GetHashCode()
extern void ClassID_GetHashCode_m395B4569B464CF459D1E80AF5664810F48AFE3E2 (void);
// 0x00000EE3 System.String NPOI.Util.ClassID::ToString()
extern void ClassID_ToString_m11123D1DF018FD401277B38D16E59C556AFABE65 (void);
// 0x00000EE4 System.String NPOI.Util.HexDump::Dump(System.Byte)
extern void HexDump_Dump_mDECF260C8CD8BE236D8547DCCB3D1E34F8BDACB6 (void);
// 0x00000EE5 System.String NPOI.Util.HexDump::Dump(System.Int64)
extern void HexDump_Dump_mE42871DCD32E961EF38D5564EC1C0EEDDBFB587F (void);
// 0x00000EE6 System.String NPOI.Util.HexDump::Dump(System.Byte[],System.Int64,System.Int32)
extern void HexDump_Dump_m657308EAF6DFBCDD141CE54D7A7BE0404F0D1B2C (void);
// 0x00000EE7 System.Char[] NPOI.Util.HexDump::ShortToHex(System.Int32)
extern void HexDump_ShortToHex_mDC2CB868F01C48D553DC66206303D2EA6608A71D (void);
// 0x00000EE8 System.Char[] NPOI.Util.HexDump::ByteToHex(System.Int32)
extern void HexDump_ByteToHex_m55E85212F60C5F103F80F159ED90A77397B978E1 (void);
// 0x00000EE9 System.Char[] NPOI.Util.HexDump::IntToHex(System.Int32)
extern void HexDump_IntToHex_m13C120BBCD4138973743C0D68346D1BFC473F554 (void);
// 0x00000EEA System.Char[] NPOI.Util.HexDump::LongToHex(System.Int64)
extern void HexDump_LongToHex_m50FDEA1B06E6F211DEFA0611B302E4967DAF93E7 (void);
// 0x00000EEB System.Char[] NPOI.Util.HexDump::ToHexChars(System.Int64,System.Int32)
extern void HexDump_ToHexChars_mADDFEFCB8D0491B2107AC6DDE190B153852C8362 (void);
// 0x00000EEC System.String NPOI.Util.HexDump::ToHex(System.Byte)
extern void HexDump_ToHex_m2B0277AB0112C37AE07437CC89584885632F88BB (void);
// 0x00000EED System.String NPOI.Util.HexDump::ToHex(System.Int64)
extern void HexDump_ToHex_m48CB587CA1D1AF1B7EEC59560CC1B714CEEE175C (void);
// 0x00000EEE System.String NPOI.Util.HexDump::ToHex(System.Byte[])
extern void HexDump_ToHex_m0E639753CFAC7F574004F84F14CEFA46793AC97E (void);
// 0x00000EEF System.String NPOI.Util.HexDump::ToHex(System.Int64,System.Int32)
extern void HexDump_ToHex_m6B51733E34DB9BA4D4E652BDD0268189B5713141 (void);
// 0x00000EF0 System.Void NPOI.Util.HexDump::.cctor()
extern void HexDump__cctor_m2A369E9172F04C6DB3134732AB069C1F365D56FA (void);
// 0x00000EF1 System.Byte[] NPOI.Util.HexRead::ReadData(System.IO.Stream,System.Int32)
extern void HexRead_ReadData_m61F6AC6EA9AE20EBFD3B395FC2880093A43FC1DC (void);
// 0x00000EF2 System.Byte[] NPOI.Util.HexRead::ReadFromString(System.String)
extern void HexRead_ReadFromString_m5B5C5222DF64AC086B7CC72B40CECB2DCD3B3EEF (void);
// 0x00000EF3 System.Void NPOI.Util.HexRead::ReadToEOL(System.IO.Stream)
extern void HexRead_ReadToEOL_m0D243F2CBB5BB54D5EBB214C8487017B96370F0F (void);
// 0x00000EF4 System.Void NPOI.Util.IntegerField::.ctor(System.Int32)
extern void IntegerField__ctor_mF9A34BFA0F2D2A2A3E604A35326D56568B05BF2B (void);
// 0x00000EF5 System.Void NPOI.Util.IntegerField::.ctor(System.Int32,System.Byte[])
extern void IntegerField__ctor_m4D664EE20B2D7674F211FA341DE2404539C55DDB (void);
// 0x00000EF6 System.Void NPOI.Util.IntegerField::.ctor(System.Int32,System.Int32,System.Byte[])
extern void IntegerField__ctor_mEE69B6ED96E115BA59288AB1AE44B2697590D612 (void);
// 0x00000EF7 System.Int32 NPOI.Util.IntegerField::get_Value()
extern void IntegerField_get_Value_mB494D3E17FB3E0F52D047C848E207E38493482CE (void);
// 0x00000EF8 System.Void NPOI.Util.IntegerField::Set(System.Int32,System.Byte[])
extern void IntegerField_Set_m1DF9A74F1B72E549F8D2E79D2B8931D37BE0DE89 (void);
// 0x00000EF9 System.Void NPOI.Util.IntegerField::ReadFromBytes(System.Byte[])
extern void IntegerField_ReadFromBytes_m41554DE9454EBDE7C18EB2297CFDD82CB533068A (void);
// 0x00000EFA System.Void NPOI.Util.IntegerField::WriteToBytes(System.Byte[])
extern void IntegerField_WriteToBytes_m09148AC3B0F7EEF94F8B0626711C25DED3FC5390 (void);
// 0x00000EFB System.String NPOI.Util.IntegerField::ToString()
extern void IntegerField_ToString_mFCFF2DE39A17D47B536288D13831D23301DCE24C (void);
// 0x00000EFC System.Void NPOI.Util.IntList::.ctor()
extern void IntList__ctor_mD575312D9F4A93D75EF6BB7CCA47CBAAC530F7B4 (void);
// 0x00000EFD System.Void NPOI.Util.IntList::.ctor(System.Int32)
extern void IntList__ctor_m386B9096450A2543A4CC27DC75C080ED0DEF7CB9 (void);
// 0x00000EFE System.Void NPOI.Util.IntList::.ctor(System.Int32,System.Int32)
extern void IntList__ctor_mA9ADF880B9CEF0E58411C4F0DFD3670AE52BE7C8 (void);
// 0x00000EFF System.Void NPOI.Util.IntList::FillArray(System.Int32,System.Int32[],System.Int32)
extern void IntList_FillArray_mCBD51C0BBC58C5DB09C1866667D3FF75587D6F1A (void);
// 0x00000F00 System.Boolean NPOI.Util.IntList::Add(System.Int32)
extern void IntList_Add_m01D16FE0324D1B8E540F40BFAE37A20767113E72 (void);
// 0x00000F01 System.Boolean NPOI.Util.IntList::AddAll(NPOI.Util.IntList)
extern void IntList_AddAll_m46760A0B26979D28C5E3A72B43C948E7BE211D11 (void);
// 0x00000F02 System.Boolean NPOI.Util.IntList::Equals(System.Object)
extern void IntList_Equals_m65D4D9714B72312E23418268C9B210DC610282E6 (void);
// 0x00000F03 System.Int32 NPOI.Util.IntList::Get(System.Int32)
extern void IntList_Get_m06EB8278A0E85548F56166B213FC64CA23767484 (void);
// 0x00000F04 System.Int32 NPOI.Util.IntList::GetHashCode()
extern void IntList_GetHashCode_m8549D1008746294CD2A4A73251B6BBC8B6A9EF1F (void);
// 0x00000F05 System.Int32 NPOI.Util.IntList::get_Count()
extern void IntList_get_Count_m7E0041194791E5526FC06421E87C20BEEEC417F9 (void);
// 0x00000F06 System.Void NPOI.Util.IntList::growArray(System.Int32)
extern void IntList_growArray_m992A817C575CBFA7012C073E0FC1FB91E54EC170 (void);
// 0x00000F07 System.Void NPOI.Util.IntList::.cctor()
extern void IntList__cctor_m7D1893A70D34072ACE4984345B2F230052E826A9 (void);
// 0x00000F08 System.Void NPOI.Util.IntMapper`1::.ctor()
// 0x00000F09 System.Void NPOI.Util.IntMapper`1::.ctor(System.Int32)
// 0x00000F0A System.Boolean NPOI.Util.IntMapper`1::Add(T)
// 0x00000F0B System.Int32 NPOI.Util.IntMapper`1::get_Size()
// 0x00000F0C T NPOI.Util.IntMapper`1::get_Item(System.Int32)
// 0x00000F0D System.Int32 NPOI.Util.IntMapper`1::GetIndex(T)
// 0x00000F0E System.Void NPOI.Util.IntMapper`1::.cctor()
// 0x00000F0F System.Int32 NPOI.Util.IOUtils::ReadFully(System.IO.Stream,System.Byte[])
extern void IOUtils_ReadFully_m9FED4E4E7275BADDD475C8D9A0EDCF95AB067D42 (void);
// 0x00000F10 System.Int32 NPOI.Util.IOUtils::ReadFully(System.IO.Stream,System.Byte[],System.Int32,System.Int32)
extern void IOUtils_ReadFully_m4F19FECCB930EB5BB23A7F1389DF8C332B02F7AA (void);
// 0x00000F11 System.Void NPOI.Util.IOUtils::Copy(System.IO.Stream,System.IO.Stream)
extern void IOUtils_Copy_m7916DDB83BEC6F5E71A813F62EC57F756BB71CD7 (void);
// 0x00000F12 System.Void NPOI.Util.LittleEndianByteArrayInputStream::.ctor(System.Byte[],System.Int32,System.Int32)
extern void LittleEndianByteArrayInputStream__ctor_m86B9FA47B8F416E4A5FA648779353982A491E1E8 (void);
// 0x00000F13 System.Void NPOI.Util.LittleEndianByteArrayInputStream::.ctor(System.Byte[])
extern void LittleEndianByteArrayInputStream__ctor_mDEC236E9550884C404649C66381232BDD121BF35 (void);
// 0x00000F14 System.Int32 NPOI.Util.LittleEndianByteArrayInputStream::Available()
extern void LittleEndianByteArrayInputStream_Available_m9EFA77807A091914B520097FC1290FABAEB4D7DD (void);
// 0x00000F15 System.Void NPOI.Util.LittleEndianByteArrayInputStream::CheckPosition(System.Int32)
extern void LittleEndianByteArrayInputStream_CheckPosition_m596DB720B2DBD4040607FE92A6D68A13FE39264B (void);
// 0x00000F16 System.Int32 NPOI.Util.LittleEndianByteArrayInputStream::ReadByte()
extern void LittleEndianByteArrayInputStream_ReadByte_m646A05D9D87E8CDED7F74E0BA51EEDAC10671B2E (void);
// 0x00000F17 System.Int32 NPOI.Util.LittleEndianByteArrayInputStream::ReadInt()
extern void LittleEndianByteArrayInputStream_ReadInt_m8EE4A7FEA7E5CAF725E85D9A7876D68173340469 (void);
// 0x00000F18 System.Int64 NPOI.Util.LittleEndianByteArrayInputStream::ReadLong()
extern void LittleEndianByteArrayInputStream_ReadLong_mB414C3DB7504C6378CF1E0618FD664C3D9176545 (void);
// 0x00000F19 System.Int16 NPOI.Util.LittleEndianByteArrayInputStream::ReadShort()
extern void LittleEndianByteArrayInputStream_ReadShort_m97439305F4F80679BB98F2D3DACC5B3DDB798B5F (void);
// 0x00000F1A System.Int32 NPOI.Util.LittleEndianByteArrayInputStream::ReadUByte()
extern void LittleEndianByteArrayInputStream_ReadUByte_m129998D184ED0971FD60DA4FF89CBF24EFDE1446 (void);
// 0x00000F1B System.Int32 NPOI.Util.LittleEndianByteArrayInputStream::ReadUShort()
extern void LittleEndianByteArrayInputStream_ReadUShort_m8B3A3914A092A3485DC49EB2AEDD332D23991CD3 (void);
// 0x00000F1C System.Void NPOI.Util.LittleEndianByteArrayInputStream::ReadFully(System.Byte[],System.Int32,System.Int32)
extern void LittleEndianByteArrayInputStream_ReadFully_m3163836CB0FC75CFB7F0B7BB32D097DAE210FF77 (void);
// 0x00000F1D System.Double NPOI.Util.LittleEndianByteArrayInputStream::ReadDouble()
extern void LittleEndianByteArrayInputStream_ReadDouble_mF85301E78F4B7C58ADF562EDDEB2D3DDFEDC4847 (void);
// 0x00000F1E System.Void NPOI.Util.LittleEndianByteArrayOutputStream::.ctor(System.Byte[],System.Int32,System.Int32)
extern void LittleEndianByteArrayOutputStream__ctor_mB3E42860E8B57D76B39F37FA1F0A469CD265EAE7 (void);
// 0x00000F1F System.Void NPOI.Util.LittleEndianByteArrayOutputStream::.ctor(System.Byte[],System.Int32)
extern void LittleEndianByteArrayOutputStream__ctor_m94B9933DF04D29E0FE1A1068D51A15A1A3DE791C (void);
// 0x00000F20 System.Void NPOI.Util.LittleEndianByteArrayOutputStream::CheckPosition(System.Int32)
extern void LittleEndianByteArrayOutputStream_CheckPosition_mD62E5EEA3335A899F412BCAE8CC12EEF2320815F (void);
// 0x00000F21 System.Void NPOI.Util.LittleEndianByteArrayOutputStream::WriteByte(System.Int32)
extern void LittleEndianByteArrayOutputStream_WriteByte_m2C6724BDD03BEFFA3D9A4CDED6348BB6B30408A7 (void);
// 0x00000F22 System.Void NPOI.Util.LittleEndianByteArrayOutputStream::WriteDouble(System.Double)
extern void LittleEndianByteArrayOutputStream_WriteDouble_m7789EB501A71E68EFF1610223B061EB7F20A4EEE (void);
// 0x00000F23 System.Void NPOI.Util.LittleEndianByteArrayOutputStream::WriteInt(System.Int32)
extern void LittleEndianByteArrayOutputStream_WriteInt_m0F3FC2951321557E1D7685DD42BD6A2238359785 (void);
// 0x00000F24 System.Void NPOI.Util.LittleEndianByteArrayOutputStream::WriteLong(System.Int64)
extern void LittleEndianByteArrayOutputStream_WriteLong_m6B0973C2816F3A885DBDF0877E1ABDCDDE867C11 (void);
// 0x00000F25 System.Void NPOI.Util.LittleEndianByteArrayOutputStream::WriteShort(System.Int32)
extern void LittleEndianByteArrayOutputStream_WriteShort_m1A21E7019A87A370D8E4124902F5851E3AB5DB4B (void);
// 0x00000F26 System.Void NPOI.Util.LittleEndianByteArrayOutputStream::Write(System.Byte[])
extern void LittleEndianByteArrayOutputStream_Write_mEDB8823616A67D1EB4A987842C6CDE5EF6EF9E68 (void);
// 0x00000F27 System.Void NPOI.Util.LittleEndianByteArrayOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void LittleEndianByteArrayOutputStream_Write_m31A939E183A47291BD366CB7B3C15395E2960C2A (void);
// 0x00000F28 System.Int32 NPOI.Util.LittleEndianByteArrayOutputStream::get_WriteIndex()
extern void LittleEndianByteArrayOutputStream_get_WriteIndex_m102424BA14ADADBBEF633FCBEB50BDF26DC3A901 (void);
// 0x00000F29 NPOI.Util.ILittleEndianOutput NPOI.Util.LittleEndianByteArrayOutputStream::CreateDelayedOutput(System.Int32)
extern void LittleEndianByteArrayOutputStream_CreateDelayedOutput_m5B5F4B8255345D8C135E83A4183B79F8BE3DA556 (void);
// 0x00000F2A System.Int32 NPOI.Util.LittleEndianInputStream::Available()
extern void LittleEndianInputStream_Available_m38A54A020090C77C4375A9C2ECB7CFB5A2B9250C (void);
// 0x00000F2B System.Void NPOI.Util.LittleEndianInputStream::.ctor(System.IO.Stream)
extern void LittleEndianInputStream__ctor_m0BF3D602FD8FA6F86447D5205E132CD0D11B6568 (void);
// 0x00000F2C System.Int32 NPOI.Util.LittleEndianInputStream::ReadByte()
extern void LittleEndianInputStream_ReadByte_m8C0B8A2DB2B78034E7703BD7036528DBA4B07E92 (void);
// 0x00000F2D System.Int32 NPOI.Util.LittleEndianInputStream::ReadUByte()
extern void LittleEndianInputStream_ReadUByte_mEAFACC97FD157D04CE92BFA1D954A0F9C22523A4 (void);
// 0x00000F2E System.Double NPOI.Util.LittleEndianInputStream::ReadDouble()
extern void LittleEndianInputStream_ReadDouble_mF3C8E2A2738EDA05CAAF2220890CE92077E572E2 (void);
// 0x00000F2F System.Int32 NPOI.Util.LittleEndianInputStream::ReadInt()
extern void LittleEndianInputStream_ReadInt_mA05499D6FE05D756EE8F8D301E2B646F83AA70A1 (void);
// 0x00000F30 System.Int64 NPOI.Util.LittleEndianInputStream::ReadLong()
extern void LittleEndianInputStream_ReadLong_m4255DCDF134B959AB3B3C6272FD0FE12A147B104 (void);
// 0x00000F31 System.Int16 NPOI.Util.LittleEndianInputStream::ReadShort()
extern void LittleEndianInputStream_ReadShort_mB1E2C2858941DE2DC8B8D45A0840E6C1FD3CDB47 (void);
// 0x00000F32 System.Int32 NPOI.Util.LittleEndianInputStream::ReadUShort()
extern void LittleEndianInputStream_ReadUShort_mC10887A2DAC3738939D26C257D093276B539366F (void);
// 0x00000F33 System.Void NPOI.Util.LittleEndianInputStream::CheckEOF(System.Int32)
extern void LittleEndianInputStream_CheckEOF_m0DEECCBB5A01CD791A114EE4CC6A5DB915CFD889 (void);
// 0x00000F34 System.Void NPOI.Util.LittleEndianInputStream::ReadFully(System.Byte[],System.Int32,System.Int32)
extern void LittleEndianInputStream_ReadFully_m0617EE000A49A99D35CC7D96C68F0237D57D7E3F (void);
// 0x00000F35 System.Void NPOI.Util.LittleEndianOutputStream::Dispose()
extern void LittleEndianOutputStream_Dispose_mDF2AC7441F4583F76D77F0F4302AA765E2EBB6BF (void);
// 0x00000F36 System.Void NPOI.Util.LittleEndianOutputStream::Dispose(System.Boolean)
extern void LittleEndianOutputStream_Dispose_mF6B4D623608D962A2B900BBA2B3A5AFEE7CBD9EC (void);
// 0x00000F37 System.Void NPOI.Util.LittleEndianOutputStream::.ctor(System.IO.Stream)
extern void LittleEndianOutputStream__ctor_mDC70EABEE04BC96276C5186FD11E92429C4B5FA1 (void);
// 0x00000F38 System.Void NPOI.Util.LittleEndianOutputStream::WriteByte(System.Int32)
extern void LittleEndianOutputStream_WriteByte_mC391FB2B611D688F46DB37E8D731109A42E93329 (void);
// 0x00000F39 System.Void NPOI.Util.LittleEndianOutputStream::WriteDouble(System.Double)
extern void LittleEndianOutputStream_WriteDouble_m930B77EFE7C010A7DE07B11CB4558A8BF673C035 (void);
// 0x00000F3A System.Void NPOI.Util.LittleEndianOutputStream::WriteInt(System.Int32)
extern void LittleEndianOutputStream_WriteInt_m6DA7E55D733CF81B8DE7529CB10A9D9124163C1D (void);
// 0x00000F3B System.Void NPOI.Util.LittleEndianOutputStream::WriteLong(System.Int64)
extern void LittleEndianOutputStream_WriteLong_m23FFDCD56A541F1B2248F498A90864DF8D746FD2 (void);
// 0x00000F3C System.Void NPOI.Util.LittleEndianOutputStream::WriteShort(System.Int32)
extern void LittleEndianOutputStream_WriteShort_mB28C8E241FBDA7FC2BB20D4352AAC5929E629903 (void);
// 0x00000F3D System.Void NPOI.Util.LittleEndianOutputStream::Write(System.Byte[])
extern void LittleEndianOutputStream_Write_m774509B1FD49EEF999DB35A0237FA47327962A82 (void);
// 0x00000F3E System.Void NPOI.Util.LittleEndianOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void LittleEndianOutputStream_Write_mCE6FAF974EDB23E70A7DC35623E339A2804DD509 (void);
// 0x00000F3F System.Int16 NPOI.Util.LittleEndian::GetShort(System.Byte[],System.Int32)
extern void LittleEndian_GetShort_mF3BA89D51340D9BEB4C02510D683AB797325A9A5 (void);
// 0x00000F40 System.Int32 NPOI.Util.LittleEndian::GetUShort(System.Byte[],System.Int32)
extern void LittleEndian_GetUShort_mDEC12F6FB3B73465A03EBE6BDB2BEE7FE1E33233 (void);
// 0x00000F41 System.Int16 NPOI.Util.LittleEndian::GetShort(System.Byte[])
extern void LittleEndian_GetShort_m90910A445E3D735B3A360B021BC2301D646FC00E (void);
// 0x00000F42 System.Int32 NPOI.Util.LittleEndian::GetUShort(System.Byte[])
extern void LittleEndian_GetUShort_mC6170325385FC9D70B73F1C0580FA249123C7880 (void);
// 0x00000F43 System.Int32 NPOI.Util.LittleEndian::GetInt(System.Byte[],System.Int32)
extern void LittleEndian_GetInt_m91A1A30D11579869A0A81267BCF0C06A76DE572E (void);
// 0x00000F44 System.Int32 NPOI.Util.LittleEndian::GetInt(System.Byte[])
extern void LittleEndian_GetInt_mA6EF389995FFD75F3F94167BB0A3A23A4E904D28 (void);
// 0x00000F45 System.Int64 NPOI.Util.LittleEndian::GetUInt(System.Byte[],System.Int32)
extern void LittleEndian_GetUInt_m89B76D884B3F8EBFCE8951A8777B04CD0FE1D775 (void);
// 0x00000F46 System.Int64 NPOI.Util.LittleEndian::GetLong(System.Byte[],System.Int32)
extern void LittleEndian_GetLong_m902DF9ECD3F727BA02D6CECD8C56CF3A1442CD0C (void);
// 0x00000F47 System.Double NPOI.Util.LittleEndian::GetDouble(System.Byte[],System.Int32)
extern void LittleEndian_GetDouble_mA60858AF36F7AAB0AB242E7C1366E1EFD202A4A3 (void);
// 0x00000F48 System.Void NPOI.Util.LittleEndian::PutShort(System.Byte[],System.Int32,System.Int16)
extern void LittleEndian_PutShort_mBC5815570164ED653AFDA2E39E24B1C9716AECAB (void);
// 0x00000F49 System.Void NPOI.Util.LittleEndian::PutShort(System.IO.Stream,System.Int16)
extern void LittleEndian_PutShort_m02D659EA29583434ECCC1333BB149AA17DA2BCBD (void);
// 0x00000F4A System.Void NPOI.Util.LittleEndian::PutInt(System.Byte[],System.Int32,System.Int32)
extern void LittleEndian_PutInt_mA3B280611EED79C16825438BB1C8C74161B97B31 (void);
// 0x00000F4B System.Void NPOI.Util.LittleEndian::PutInt(System.Int32,System.IO.Stream)
extern void LittleEndian_PutInt_m05FF1C514A2E8D7D93FF3A5A3BB5E11CD0192340 (void);
// 0x00000F4C System.Void NPOI.Util.LittleEndian::PutLong(System.Byte[],System.Int32,System.Int64)
extern void LittleEndian_PutLong_mB7D2F415C4B259EB4759DEACD938F90344BE48E5 (void);
// 0x00000F4D System.Byte[] NPOI.Util.LittleEndian::GetByteArray(System.Byte[],System.Int32,System.Int32)
extern void LittleEndian_GetByteArray_mA5B5C4325F64E256BB7DB8548828D7ED369D2774 (void);
// 0x00000F4E System.Int16 NPOI.Util.LittleEndian::GetUByte(System.Byte[],System.Int32)
extern void LittleEndian_GetUByte_mEEFDC552CF0995F5CACF7C55BD0A846FA9657D7F (void);
// 0x00000F4F System.Void NPOI.Util.LittleEndian::PutDouble(System.Double,System.IO.Stream)
extern void LittleEndian_PutDouble_mE92DAEBCA183902D8C0381893477199485E00757 (void);
// 0x00000F50 System.Void NPOI.Util.LittleEndian::PutUInt(System.Int64,System.IO.Stream)
extern void LittleEndian_PutUInt_mA6A6D1EB1298FD0AC5F3CD67B774292C6AFF6EED (void);
// 0x00000F51 System.Void NPOI.Util.LittleEndian::PutLong(System.Int64,System.IO.Stream)
extern void LittleEndian_PutLong_mBAA8781144CC6032642957E4BFB32D5EFFC309E3 (void);
// 0x00000F52 System.Void NPOI.Util.LongField::.ctor(System.Int32)
extern void LongField__ctor_mF089E4684CF5D8AAD008588573C35E5756C177FE (void);
// 0x00000F53 System.Void NPOI.Util.LongField::.ctor(System.Int32,System.Int64,System.Byte[])
extern void LongField__ctor_m77B02D2C0B44F61DE01A9D552F8950CB47C89C69 (void);
// 0x00000F54 System.Void NPOI.Util.LongField::Set(System.Int64,System.Byte[])
extern void LongField_Set_m96B8152512C2CB101CC8DC71BF39FE4E248A590E (void);
// 0x00000F55 System.Void NPOI.Util.LongField::WriteToBytes(System.Byte[])
extern void LongField_WriteToBytes_mBFEB8C9EE648EE9D1F40B274304D2E79F8D0CC3D (void);
// 0x00000F56 System.String NPOI.Util.LongField::ToString()
extern void LongField_ToString_mC1A16C0985B2B0F8D8C0FDF8994D47BACD79AC46 (void);
// 0x00000F57 System.Void NPOI.Util.POILogger::.ctor()
extern void POILogger__ctor_m6A535072F52D6DA111EFAC584E31D8EF5BEDE1F4 (void);
// 0x00000F58 System.Void NPOI.Util.POILogger::Initialize(System.String)
// 0x00000F59 System.Void NPOI.Util.POILogger::Log(System.Int32,System.Object)
// 0x00000F5A System.Void NPOI.Util.POILogger::Log(System.Int32,System.Object,System.Exception)
// 0x00000F5B System.Void NPOI.Util.POILogger::Log(System.Int32,System.Exception)
extern void POILogger_Log_mFA0811BE7F90C904CA3F3506C59DA6D8A1E15AA3 (void);
// 0x00000F5C System.Void NPOI.Util.NullLogger::Initialize(System.String)
extern void NullLogger_Initialize_m1D49789A09F6E205DFCB2203597E01729163D8CF (void);
// 0x00000F5D System.Void NPOI.Util.NullLogger::Log(System.Int32,System.Object)
extern void NullLogger_Log_m969F3CD089FC86CB9D120A52A2CD8CA3308AEC52 (void);
// 0x00000F5E System.Void NPOI.Util.NullLogger::Log(System.Int32,System.Object,System.Exception)
extern void NullLogger_Log_m91AFCC9A3B1977F85E8F283FB9A75F8DFC92EAA2 (void);
// 0x00000F5F System.Void NPOI.Util.NullLogger::.ctor()
extern void NullLogger__ctor_mC9ABEF89C9C6CD05345517763DB7A86733B51B7E (void);
// 0x00000F60 System.Int32 NPOI.Util.Operator::UnsignedRightShift(System.Int32,System.Int32)
extern void Operator_UnsignedRightShift_m2BF9A4E449CF5A8C7355951488E669FA87455FEA (void);
// 0x00000F61 System.Int64 NPOI.Util.Operator::UnsignedRightShift(System.Int64,System.Int32)
extern void Operator_UnsignedRightShift_m48CEF7A353E39C6B6E173099FFFA0C15D140462F (void);
// 0x00000F62 NPOI.Util.POILogger NPOI.Util.POILogFactory::GetLogger(System.Type)
extern void POILogFactory_GetLogger_m48771ED92B379411190DB546CA7BECF6AEBAA964 (void);
// 0x00000F63 NPOI.Util.POILogger NPOI.Util.POILogFactory::GetLogger(System.String)
extern void POILogFactory_GetLogger_m5436FD72FC502BDC0FEA30A98200C9DBE41A87C5 (void);
// 0x00000F64 System.Void NPOI.Util.POILogFactory::.cctor()
extern void POILogFactory__cctor_m660C1DDED14ECAE67D36E9338A861F5C46C32862 (void);
// 0x00000F65 System.Void NPOI.Util.RecordFormatException::.ctor(System.String)
extern void RecordFormatException__ctor_mEC2C3B5D050AD3AEF8CFC9E9F783532321514633 (void);
// 0x00000F66 System.Void NPOI.Util.RecordFormatException::.ctor(System.String,System.Exception)
extern void RecordFormatException__ctor_m9DFF8F587A56A7783F445FF2A8A7F556AC9B4FCB (void);
// 0x00000F67 System.Void NPOI.Util.ShortField::.ctor(System.Int32)
extern void ShortField__ctor_m36CFD283551C4D065D6507A67FA46B38BEE64889 (void);
// 0x00000F68 System.Void NPOI.Util.ShortField::.ctor(System.Int32,System.Byte[])
extern void ShortField__ctor_mB4E4D39D3BDD179BEBC1BD8895281B42B6CB15EE (void);
// 0x00000F69 System.Void NPOI.Util.ShortField::.ctor(System.Int32,System.Int16,System.Byte[]&)
extern void ShortField__ctor_mA2F9102CDA841C14509C1F19A506E8FE3C44FB5D (void);
// 0x00000F6A System.Int16 NPOI.Util.ShortField::get_Value()
extern void ShortField_get_Value_mA0BB87F3502CC1ED87AE113610CBC9E339CF72B7 (void);
// 0x00000F6B System.Void NPOI.Util.ShortField::Set(System.Int16,System.Byte[]&)
extern void ShortField_Set_m9559CA66A9B37547A5513F7C95C0E87E31F84A8A (void);
// 0x00000F6C System.Void NPOI.Util.ShortField::ReadFromBytes(System.Byte[])
extern void ShortField_ReadFromBytes_mD082E5846BD66F03639D0D69E9027B5691C2DCB2 (void);
// 0x00000F6D System.Void NPOI.Util.ShortField::WriteToBytes(System.Byte[])
extern void ShortField_WriteToBytes_m6D2FEDFD04C4151F3BE4B7A106162FAEBF29129C (void);
// 0x00000F6E System.Void NPOI.Util.ShortField::Write(System.Int32,System.Int16,System.Byte[]&)
extern void ShortField_Write_m7F5C30B7C7FADD79CB9642BB7E35BE21AE1D1DDF (void);
// 0x00000F6F System.String NPOI.Util.ShortField::ToString()
extern void ShortField_ToString_m3A54C171D01E0426A1AB80642B36683AC2BA4063 (void);
// 0x00000F70 System.String NPOI.Util.StringUtil::GetFromUnicodeLE(System.Byte[],System.Int32,System.Int32)
extern void StringUtil_GetFromUnicodeLE_m5FD4F5569390663B7BCCEA72CCBC6AF84F793393 (void);
// 0x00000F71 System.Void NPOI.Util.StringUtil::PutCompressedUnicode(System.String,NPOI.Util.ILittleEndianOutput)
extern void StringUtil_PutCompressedUnicode_mB34DC0049DA2FCD2653119D890B3BE96372C189F (void);
// 0x00000F72 System.Void NPOI.Util.StringUtil::PutUnicodeLE(System.String,NPOI.Util.ILittleEndianOutput)
extern void StringUtil_PutUnicodeLE_m5985127E462F11622F3A1CCF817BEC7F76BB5BD5 (void);
// 0x00000F73 System.Boolean NPOI.Util.StringUtil::HasMultibyte(System.String)
extern void StringUtil_HasMultibyte_m32B525077C2CD33B5A3B98324D75C2580B564514 (void);
// 0x00000F74 System.String NPOI.Util.StringUtil::ReadCompressedUnicode(NPOI.Util.ILittleEndianInput,System.Int32)
extern void StringUtil_ReadCompressedUnicode_mB942D0AAB9655D7AF5BD6F96F6DE3BADAB34F5BD (void);
// 0x00000F75 System.String NPOI.Util.StringUtil::ReadUnicodeLE(NPOI.Util.ILittleEndianInput,System.Int32)
extern void StringUtil_ReadUnicodeLE_m635B0C77BAB7766DAA2E2E16695B8C2656854B3B (void);
// 0x00000F76 System.String NPOI.Util.StringUtil::ReadUnicodeString(NPOI.Util.ILittleEndianInput)
extern void StringUtil_ReadUnicodeString_m0C1E3EF489201FC50FE397E606C7CE1A67ADDA9F (void);
// 0x00000F77 System.Void NPOI.Util.StringUtil::WriteUnicodeString(NPOI.Util.ILittleEndianOutput,System.String)
extern void StringUtil_WriteUnicodeString_m8928B19E925B0A93FDF74CF05B441042940698DE (void);
// 0x00000F78 System.Void NPOI.Util.StringUtil::WriteUnicodeStringFlagAndData(NPOI.Util.ILittleEndianOutput,System.String)
extern void StringUtil_WriteUnicodeStringFlagAndData_m8A94DC799A5C1895DAEEEBDC77089D4A68C85E5A (void);
// 0x00000F79 System.Int32 NPOI.Util.StringUtil::GetEncodedSize(System.String)
extern void StringUtil_GetEncodedSize_mBE92CC89FBEA47CCCAC643D7A0FD025AE86F0455 (void);
// 0x00000F7A System.String NPOI.Util.StringUtil::ToHexString(System.Char)
extern void StringUtil_ToHexString_m4DEBE36008DDD972F00F3124EEAC9FFE0CDEBC2E (void);
// 0x00000F7B System.String NPOI.Util.StringUtil::ToHexString(System.Int16)
extern void StringUtil_ToHexString_mA523B43FEF94A2F5D754D41E501635A68403A85F (void);
// 0x00000F7C System.String NPOI.Util.StringUtil::ToHexString(System.Int32)
extern void StringUtil_ToHexString_m0A3DB93B261B0D1FF65E8E38AC6B233CCB91B3A4 (void);
static Il2CppMethodPointer s_methodPointers[3964] = 
{
	EscherRecord_get_Options_mA163509CCD7AA5DC2D6C5496E37B0CCE2CC9E5FB,
	EscherRecord_Serialize_mC2D1E02EC8F7604A2073A8CEBB74C98BCD18846A,
	NULL,
	NULL,
	EscherRecord_get_RecordId_m0353F2F24EB65F9A4AE940EDEF894206B83F3490,
	EscherRecord__cctor_m97B2A5587845BA3CCBA86530C3D33676701FFB2C,
	Array__ctor_m5FA1D02D671FD91525C9A36AAD7C8A1E8D27A31C,
	Array_Read_m81D55450BF887649C9B68C32FB87BCF0EF0129DB,
	ArrayDimension__ctor_mF4BD36AF2E10C74BAEB54ACD2137180C8B304E02,
	ArrayHeader__ctor_m75E6FCDAE18AAEEE958C561BF1F8DB7E1C9C8E85,
	ArrayHeader_get_NumberOfScalarValues_m14AC92BB5A0B881C6E8CB5A55F2B872BB9B011EB,
	ArrayHeader_get_Size_m24D274A26BD9063B548185F017193046C147FF3F,
	Blob__ctor_m4EFCFAD64637E7E7A69663D0473A7BE9BADB907A,
	Blob_get_Size_m25830AC9F3337DFAF649AE7B4563440B01D8C4E7,
	ClipboardData__ctor_m82D55811EEB7AC8DCFCCCCB72360E2898AFE2CAC,
	ClipboardData_get_Size_mECA721C91661192B7CBB5ADC065AB40B08D6664F,
	ClipboardData_ToByteArray_mCC093C541D2571EB503791CEBBC7E20AED4EBB26,
	CodePageString__ctor_mCA99EA2601564998CBA0B8C3B7A00E300E3AA5C9,
	CodePageString__ctor_mC0B6749A7743538A5C5166F4ED8A45FEC8712337,
	CodePageString_GetJavaValue_mE7DFBA44A6ACBDC71C45EB76E0520D3A6DF063CF,
	CodePageString_get_Size_mB7F9E3C0BBCD70BEFCDD8979A275082CBA8B9AAF,
	CodePageString_SetJavaValue_mE87B422643AB871E4C89F296AD0AE5B16783BAA6,
	CodePageString_Write_mEFB88FAD311F217B145BE41432C7AC7E63128B55,
	Currency__ctor_m4613F45CDCDCB399AECDAE4E1BBC6C97775A04A9,
	Date__ctor_m06E3F4CB509C8BCC43681FD834FB57FAF8DC2A79,
	Decimal__ctor_mD02610FFD7567B7B9AC6AB9FDE8F04186D8ADFC0,
	Filetime__ctor_m70C39F1166513E9C1BC3B1EF981B0D4E30F9AA0F,
	Filetime__ctor_m3BCFBAE817EC9994A1632ECB5E37AB714D715CF1,
	Filetime_get_High_mCF3EF5D6516367333C240C48905A9D8B0B2BBD42,
	Filetime_get_Low_m2CFF481A6328BCFCD46F8843B81961DFD78DC8FA,
	Filetime_Write_m22D4787F8FE7950E195F4FFA8587045FD475D37C,
	GUID__ctor_mAC428C4B9D63A0914BDD234642A8BEF860C89787,
	IndirectPropertyName__ctor_mD0E0D86775B4EFD7F1A75A4F22618798695357AA,
	IndirectPropertyName_get_Size_mE9E13DD28164AC782EE2BFC894518E720675DC8F,
	TypedPropertyValue__ctor_m145BDF937E96390DBEF95D83EF179353040D4239,
	TypedPropertyValue__ctor_m91F23C6DAB4A5559F09C64FDFA7AFE0565711D8A,
	TypedPropertyValue_get_Value_mD463A15EFDF9A455F0EBC09B371735206F709236,
	TypedPropertyValue_Read_mC6E77FC21B9851915E30B8C67CBE7C63A29E05AB,
	TypedPropertyValue_ReadValue_m3971792ECAD3D24F5DD8E0BE562C0BD720B67096,
	TypedPropertyValue_ReadValuePadded_m37C28AA11C3CB5E00734920AD7442B47287985AF,
	TypedPropertyValue__cctor_m0102420527996408C044CEADC0C795F911E66390,
	UnicodeString__ctor_m66AA1A57E37BE146DE8510901FF0FF578FA631FC,
	UnicodeString_get_Size_mA1153203137716CD8185FEEABC24FC311861A9E5,
	UnicodeString_ToJavaString_m3C33D1E94EDC484866BE119D78F89EBF339E187C,
	VariantBool__ctor_mBB5E67AB750BE66BABDDE5D1F94AEEA3B24AFE4D,
	VariantBool_get_Value_mD08FA60AE852D224CE80A5541CE199AFA7E760E0,
	Vector__ctor_m9617BB05BDD95061A81BACEADF12202B63BDD597,
	Vector_Read_m990096590912504EA2848EF2158F5BFB44702A05,
	VersionedStream__ctor_mB6C320A7C73C5C02FCF636B2C3FC6BC1669DC1C6,
	VersionedStream_get_Size_mCA380A2075DEAB4F2F85AE84ABFC932956BD51F7,
	NULL,
	NULL,
	RecordBase__ctor_m3AA7A080BE5A232BE2676747C159D127E6F0A381,
	NULL,
	RecordAggregate_Serialize_m5675ADB220F3D17B783E8A86419ED1686593CEA3,
	RecordAggregate_get_RecordSize_m2DFC7C86CD8BAE9D6A24B25CC120B2294BC89DCC,
	RecordAggregate__ctor_m0EF1BC067BDF9054426A8CF9D6ABCD4B00F352DD,
	SerializingRecordVisitor__ctor_m3235895ED1DA5959053258ACBAB542797105AC51,
	SerializingRecordVisitor_CountBytesWritten_mC4C0C4B06361AC6CDA2E6AC8227665C7076A3461,
	SerializingRecordVisitor_VisitRecord_m199D507DD8CE1504393A749400E7EE5C49DD413C,
	RecordSizingVisitor__ctor_m9E6AE76D3114A4E289D82B605BFB2A4BFA7ED708,
	RecordSizingVisitor_get_TotalSize_mA914C58EB358659B34781970E247021DD629E2E2,
	RecordSizingVisitor_VisitRecord_mF38613F6A0FB2EC18BF004F8914B45E1094B0926,
	NULL,
	Record__ctor_mE255BD311311814FDF6A357C7D1FDD49BBC109BA,
	NULL,
	Record_Clone_mA6A1C792A05B8FE83480931F1E11E4A1BDD964B1,
	NULL,
	StandardRecord_get_RecordSize_m782578B09E0EA111BD0B261F2E5366F60ACEFD6F,
	StandardRecord_Serialize_m4BFAD8E5C4A5FF56C29D4F001265304A61146051,
	NULL,
	StandardRecord__ctor_m37E579B198FE968565986B6EDF87C559EA58FD5A,
	AlRunsRecord_Serialize_m8289867DAA1A698EE88C8B33A6F9A0E18680B6E6,
	AlRunsRecord_get_DataSize_m32CCA4D9490B04832EC9F25D0F173B33228A4091,
	AlRunsRecord_get_Sid_m6D5CF829EF452C52BA99960153122549FD33FA1A,
	CTFormat_Serialize_m3FA70A4B746DE6278880B70B0ABA18BFE3F766D5,
	AttachedLabelRecord_Serialize_m8F39FDF28FEC34D99B8EC87E7590E4CC9D0622CA,
	AttachedLabelRecord_get_DataSize_mA1E7BBD825DE4A015E0261DAC7D32DEBBAF39237,
	AttachedLabelRecord_get_Sid_mF9FE3D7A831494A522883C9B6F4DC1C07E665060,
	AxcExtRecord_Serialize_m5B37B09FE98B5681E2593E8FA6BB670902D09BC8,
	AxcExtRecord_get_DataSize_mDDDF518DB8E58FFDA52B1E55AD4A62158302F3E1,
	AxcExtRecord_get_Sid_m61F9A6D9C81407AE834954BBC4A90F7055CA853D,
	AxesUsedRecord_Serialize_mA3579853C1F4CB83162F6BB0DA4BD43465D0A38B,
	AxesUsedRecord_get_DataSize_m8F403F9B5D099590F14D5A49F1978E1C541E50B1,
	AxesUsedRecord_get_Sid_m609BDA75CD00408B6BD7E78B2A9B32A718939D61,
	AxisLineRecord_Serialize_m21A1D8013E6E06DE789A8DB1B118279ADB3820D1,
	AxisLineRecord_get_DataSize_mF896B3B7D38A5554A575E3E18D973929E949C3C9,
	AxisLineRecord_get_Sid_mCCF57285A575F1C40D8B10F1B05B0456ED7DC566,
	RowDataRecord_get_DataSize_mE5FDA1D36434A3FCEF94D100A33FCC6CEC4745D9,
	RowDataRecord_Serialize_mE1F74FFB19986F31142E24C78D3D3C2E1BEF5D7B,
	RowDataRecord_get_Sid_mEDB19EB64DE32695D89ABD641E213061055C5D18,
	BRAIRecord_Serialize_mF4890C0A1E650C7587E910666DE90DE35FDF5BB5,
	BRAIRecord_get_DataSize_m58384ABDF0C00EC1A6E0108A7DB3D0BB505506CC,
	BRAIRecord_get_Sid_mECB293FE25E21653E9AAB9D7BDED4066531DE9B6,
	CatSerRangeRecord_Serialize_mBBA146550EFEE2DB427F6B66F01456C0F1656284,
	CatSerRangeRecord_get_DataSize_mC75318638BEFDBCB32722BEDC1AE579BD0C4D5D3,
	CatSerRangeRecord_get_Sid_m8B88A35B348FF0A5EEE50A1C1B3F46927987CDBF,
	Chart3DBarShapeRecord_Serialize_m08D607F2A28A0E0C4641DAE04FB83EFDCF587342,
	Chart3DBarShapeRecord_get_DataSize_m06F06835E49A53E93C83C971F053E9B510251282,
	Chart3DBarShapeRecord_get_Sid_m6610C3EEFB6AD46B3FE7EB9D53D0FB25C8675A13,
	DataLabExtRecord_get_DataSize_m5F7F88B821D52E781DE56F76BF61D19CB287B8ED,
	DataLabExtRecord_get_Sid_mB70207110A9AAAE1FF466AD8C3009B5F8E0A9BF5,
	DataLabExtRecord_Serialize_mC541058548CB2DC2DA983B28394D930065C44FAC,
	DefaultTextRecord_Serialize_m476EEA51B145315AE669D975E5BB5CF6E406C553,
	DefaultTextRecord_get_DataSize_m7615812A5CF9DD25995B45C962CAFBC6D0C87C04,
	DefaultTextRecord_get_Sid_m696AA3CF10E4712DE28B9F4C77995C1B0EBBE974,
	EndBlockRecord_get_ObjectKind_m6C733A2243FCB024076530FBC6087D35E39A3302,
	EndBlockRecord_get_DataSize_m8B876FBB6C2460127C199598131CDAF245816F6F,
	EndBlockRecord_get_Sid_m7DFF34990DAB87532AA77190EB2D32687522048F,
	EndBlockRecord_Serialize_m87AB5D0E64CBBD6CE8E383CD559B43DA272D3C23,
	FbiRecord_Serialize_mEC6EABC8A3FD1CFBF17AE646722C026079C5D32E,
	FbiRecord_get_DataSize_m7F2FBEFB163995C54D40676FBD4DC3944FFD484A,
	FbiRecord_get_Sid_m3A83A79ACFA1EC4AD45EE260F4FDFF2C6477B29C,
	FontXRecord_Serialize_mFA0114B5F1673B1FAEB76AC81D8F4CA58554D65E,
	FontXRecord_get_DataSize_mBF042AA25E42814E13319F269783440DE4D831CC,
	FontXRecord_get_Sid_m784AC19031C356C35B110BBB307AD2239E28D56F,
	IFmtRecordRecord_Serialize_mBA67FAD3C58802B45729EFF82D82EC05047E45B3,
	IFmtRecordRecord_get_DataSize_m50BA0D4BD4DB9FD64902C5DB3B19616A7BE05407,
	IFmtRecordRecord_get_Sid_mF63C56134DEEA22C06D2305338642B6FE6279918,
	SerToCrtRecord_Serialize_mE04918E5EA6CED3D3412EC7782B5D94067C9ABB3,
	SerToCrtRecord_get_DataSize_m774D582E0DD19FC51F76FA01F004D4E5BF29EDF5,
	SerToCrtRecord_get_Sid_mE2C80B049B2E56AA82FF873E9500B61D8016F6BE,
	ShtPropsRecord_Serialize_m4F34F5D1AB59E660D558C118161755553D9E6CED,
	ShtPropsRecord_get_DataSize_mFC6500B9023AB54202028AA728A963530E6DF093,
	ShtPropsRecord_get_Sid_m7355967A6C2C54EABF2DECABD96235FC3DBEE9C7,
	StartBlockRecord_get_ObjectKind_mDC04B947B8F22C79D609980BAA997EDCAD25163F,
	StartBlockRecord_get_ObjectContext_mF398C1F021B07BC8BB6D86181866631578DCD508,
	StartBlockRecord_get_ObjectInstance1_mAB442696983C9A4BF33B04FA8BDDBA8536180AFE,
	StartBlockRecord_get_ObjectInstance2_m291ED108E735DAA9BB604AA8AEB334AC0D00D75F,
	StartBlockRecord_get_DataSize_m5D17CEC95AE3EB9AFDFBABBAE2EABDA4499734D7,
	StartBlockRecord_get_Sid_mC55D83720BFC5ACDD49F9C1315C937A302972B7B,
	StartBlockRecord_Serialize_m4644D2EBC2CE6ED5AF374F934FF7BCA575945281,
	StartBlockRecord__cctor_mD70C67543624C2B3E384470CBA17C33094CC908F,
	DConRefRecord_get_DataSize_m9A33DDF46FB99C25CA754DFAB2C323C764881631,
	DConRefRecord_Serialize_mCD2898F64B0B51B3CA695EC0044CA8083E8B82B3,
	DConRefRecord_get_Sid_m3A98530F5638613685CFA71976B2D67CE6615C83,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	POIUtils_CopyNodeRecursively_mA13261BAA8481E2EF35F1BB3E3B5D786144C6DFE,
	POIUtils_CopyNodes_m06F8C1BDAE0A94038A565FD727B7A4FF878E56F2,
	ArgumentsEvaluator__ctor_mAECBB577F562C40B9216161EDA7A9B127BFB5218,
	ArgumentsEvaluator__cctor_mB6F1DB9C925CCC93BFD5ECB0AE2E4070DF0BEE88,
	IfError__ctor_mC34802D5C4B3144707AEB225B352532C5A288B52,
	IfError__cctor_mA9FAA16DAFDAAB7FB68E0033F15C4967A03A3081,
	NetworkdaysFunction__ctor_m4AE1F8C052FA78E76F1110091D5ADC9B6965561B,
	NetworkdaysFunction__cctor_mDA2B3CD3A3952E1F585328A402C1892A80231D17,
	WorkdayFunction__ctor_mB730411AD0E7F5E235201882B59BCA348A6A8679,
	WorkdayFunction__cctor_m60479CA836F45DF5FB0836FAF9412E0891A06291,
	Var2or3ArgFunction__ctor_m74EA429B91DB4FBE5C098FB3EFE580D83D8F3B79,
	Complex__ctor_m4A9FCC73E094239B3C29608FC6975C9D683B5894,
	Complex__cctor_m72F6E712D57C3EE9AE36592C3D1E2C9389BD86DF,
	Sumifs__ctor_mC85204038F2718D3817AB992D350DF28A04753F9,
	Sumifs__cctor_m1F9119E0240A4859393E497C2199424FB7381DE6,
	ChartSubstreamRecordAggregate__ctor_m2BF7CDF90AEE650123BA091A58B7082E591B52DF,
	ChartSubstreamRecordAggregate_VisitContainedRecords_mC221545C39F114E0D79C597C60C2B6C4AEF4FD01,
	WorksheetProtectionBlock__ctor_m446303FB4E3CFE3F05A51930771563A46FFAEE83,
	WorksheetProtectionBlock_IsComponentRecord_m73773C1CB029D6A163C0ACE680B38E8324F9A070,
	WorksheetProtectionBlock_ReadARecord_mF524D9648A236A5AEB88D14CFD5817CC7C7D0E0C,
	WorksheetProtectionBlock_CheckNotPresent_mCDDA8F0F67129CEB03968490808127E3E31B1EE2,
	WorksheetProtectionBlock_VisitContainedRecords_m650D7FC03FE358156CA2A3D5EE1141339A6864F3,
	WorksheetProtectionBlock_VisitIfPresent_m15DB497CE56AC5AA3975949993BCEEB81D860D74,
	WorksheetProtectionBlock_AddRecords_mF0BA084A87098D3B1C5569F53090874BF68F8736,
	AutoFilterRecord_get_Sid_m3B49DC4FB0F7ABD68A390CFC3C3EC2DF39402621,
	AutoFilterRecord_get_DataSize_mF98C17ACB40D40484E0A1FF40449F391DBC4D170,
	AutoFilterRecord_Serialize_m191202A4D1BEA2C4DF15F5DA0486324F06AD46EE,
	AutoFilterRecord__cctor_m13BBF0C329D9D4FF2CCADAB2847106352A693D53,
	FilterModeRecord_get_Sid_m2D95C92B23EFA4D6F0AB9B8CE8FE03D5246273B5,
	FilterModeRecord_get_DataSize_mE13295C37DAB4BC560DDFDCA732D4D60844F61AD,
	FilterModeRecord_Serialize_mC24DDD268C5DFF15B05D2BA3DBF6CB87857A1082,
	Chart3dRecord_get_DataSize_m112B107F670FD2FE865A4F0059F2A6673F57CADD,
	Chart3dRecord_Serialize_m654B0079E52CB207A7321C6190CAB7ED46AAE9E9,
	Chart3dRecord_get_Sid_mC9D6056453557C4494167F6020FE33605B5D98AB,
	CrtLayout12ARecord_get_DataSize_m72A3D3EDD5893F0500CE344A9265169C2F3FB827,
	CrtLayout12ARecord_Serialize_m3F6CD4855DA026F9F391C91A3CCD7B594BD2A8AA,
	CrtLayout12ARecord_get_Sid_m7A0FB550DB22A96298A87B94FC85EE805C699CE1,
	CrtLayout12Record_get_DataSize_m50C0EECFA325A9B76D15C3059AC43C6B3F022592,
	CrtLayout12Record_Serialize_m1145B3BD4392B2BF9B1C5DD1C32E3DD1C0C30296,
	CrtLayout12Record_get_Sid_m567DBCD333712C66ECCDE50535C69D0639E617F9,
	CrtLayout12Record__cctor_mF28AC53759218E47A980F01BBC373C3BC202220C,
	MarkerFormatRecord_get_DataSize_mA9B30DB1EF73E51588620C961547132213FEA12E,
	MarkerFormatRecord_Serialize_m0EE1C0851E1CC753166DE554F3D850C3E18C2607,
	MarkerFormatRecord_get_Sid_m8019543B349A83DA70843934DD6259BC31059076,
	PieFormatRecord_get_DataSize_m7800EF2A841E53591BFFC5A246588729453A3CB2,
	PieFormatRecord_Serialize_m53A173584057F06F35E5E0C1CF9088F24E688F5A,
	PieFormatRecord_get_Sid_mF8BFC3A91C98C63E1902B54E0BCC013429FCB15D,
	PieRecord_get_DataSize_m497E097C4387930291E217E35127F476A3EB51BC,
	PieRecord_Serialize_m0E3D87665651FFC0376DFA000FC48779B98611AD,
	PieRecord_get_Sid_m24D371C9C1D443C394A326509E700C8E456D8880,
	UnicodeString__ctor_mD57F53DB35A89F924647958BD23F7295F7294DB1,
	UnicodeString__ctor_m6EAEA26F4A00BDBC65611232311C50B17016D9F9,
	UnicodeString_GetHashCode_m9018E49602B987AFC9477C6F98CC8D51B965C855,
	UnicodeString_Equals_m5AE91ED767950342A2F6C87E14A0E84BAA1FF076,
	UnicodeString_get_CharCount_m4F57EE74A8631C6F626C15E197A7FD7B05373D55,
	UnicodeString_set_CharCount_m72E71DFED0FB7B7E9ECDA8F0A27276E0C411AEF9,
	UnicodeString_get_OptionFlags_mBDC6CF391CE2D1D08143B618E11D1F566B6C2C4D,
	UnicodeString_get_String_mDD16701C32D3A9A19AAF698243F63628ECE5DBDB,
	UnicodeString_set_String_mE999817575FAD3A570C04B871A2DAA5203D6F401,
	UnicodeString_get_FormatRunCount_mC3F8FC97485F693D5D3567746A317581596CC60F,
	UnicodeString_GetFormatRun_m1D22120AB55E2BCA6F9614CE287B75E9B21E0408,
	UnicodeString_ToString_m99AC54F22F25E5A64FFE9F93C42DBDC60A85D7A9,
	UnicodeString_GetDebugInfo_m6608325A4D38BEB0F88E14E6D0FC6E318B717D02,
	UnicodeString_Serialize_m8F22917CF43D03E63806FE276B3B2C78EFE031EC,
	UnicodeString_CompareTo_m0A0DB29AB7E155C85E4D406EFB28EFC53A9BE6E0,
	UnicodeString_get_IsRichText_m910CE1DA2AB7334EBEAC7CEA712615E4C60BC490,
	UnicodeString_get_IsExtendedText_mD871CB72878D588C59E40D02A209B61A8C17B384,
	UnicodeString_Clone_m662F1C04C85085582B78720BDC6F54A3C21C4576,
	UnicodeString__cctor_mB768C2FC3B11CA40B0D7DD3BB55F771F6D05BC8D,
	FormatRun__ctor_mA3F844FB5D6684413611005C87AB2EF6EAEA9CB7,
	FormatRun_get_CharacterPos_m65D1731ACDA2C019887D25E75B26DED3FF8AD12B,
	FormatRun_get_FontIndex_mE63AD5A9962B1C3A96B73F1E33BBC311C88DD0B5,
	FormatRun_Equals_mE7A1E715C5D9FDCAF956C080B59A4F53567A15E6,
	FormatRun_GetHashCode_mF0E37DD324BD23B59C3D45DAF6EE3067BBA45EAC,
	FormatRun_CompareTo_m96087FAF85EB87ECF9AF11BE39669D1106B5E3A9,
	FormatRun_ToString_mDEF533A49D54588E5AFFEC7476C8F1967A69A7F5,
	FormatRun_Serialize_m750F7D39F72CC87BEBD7B87AEF8062766F581CF3,
	ExtRst_populateEmpty_m5B8AFEEFBC0431EE13E0194C43DDE9D516B1185B,
	ExtRst_GetHashCode_mAC891B884BE774B09B5030E57863BF99B1744936,
	ExtRst__ctor_m67D05977368149ABDA8F52FFD8C3D1BD3A3E63CB,
	ExtRst_get_DataSize_m5EEA62D427EE5F940301FAF03D9D31A4E3E50547,
	ExtRst_Serialize_m42EADEFE7B8CB5C4D8F7D9BD5C3DC762F93A0371,
	ExtRst_Equals_m6150886A00FEAC78E78FC6CFF63E049079FD8EFD,
	ExtRst_CompareTo_mEB977CC0FB4F232D18C596F62CDDEE02A9FE5C03,
	ExtRst_Clone_m2166D89B2F7D7BDF841ED1439CDD81D683126533,
	PhRun__ctor_m1005C3636B609556A53C1053CE5AA1065DBCACAE,
	PhRun_Serialize_mA6FB104097A8A89FCEE15D9CE12CB3B027C2FCA6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SheetExtRecord__ctor_mCE91BBC5D3519246CF62344D2DF57E7680E178E6,
	SheetExtRecord_set_TabColorIndex_m02FDEFFB13531466871B941971701CE9324C8DAE,
	SheetExtRecord_set_IsAutoColor_mDC4E4DD672DD1A2AB2A7A9C52FA4C62B0C914E9B,
	SheetExtRecord_get_DataSize_mCF39C7C72353A16A67BFC18357661495859437AF,
	SheetExtRecord_get_Sid_m1FCA3E7DC3C56E403ABB632F91D1B5A4909113E0,
	SheetExtRecord_Serialize_m3A7C2A7EF2C762B2F8DBA1B08BEE517E36C1E9DC,
	SheetExtRecord_ToString_mA5C1B69B63BB16F9E7F7993183A2CEA726A9B2BF,
	SheetExtRecord_Clone_m4A3587705C08305638C3D984D9B444875371CCC8,
	DataFormatter__cctor_m4A5BE9132428C255697F9ED85631144814442E19,
	DataFormatter__ctor_m5BC226324CCE8309926C5B0425C1FE43E28C35A7,
	DataFormatter__ctor_m3FE570E146EFCBC29B841948F8F19C029E6D04D0,
	DataFormatter__ctor_m4D9E8BF6C4F7F5891F8F6E6E9CA8E6FBF81ACD41,
	DataFormatter_GetFormat_m8689333B68E45530B2046ACBAB888E6FA0C6DB7F,
	DataFormatter_GetFormat_mF0E2ACB129B1E40808EA5CD5840847E1096927E9,
	DataFormatter_CreateFormat_m3D4DF976D4054B61BF03E1D298C26F2FDE46D924,
	DataFormatter_IndexOfFraction_mF6A31616CCD87C298DC5A851B5C3F028EF22DA33,
	DataFormatter_LastIndexOfFraction_m7ED16AD2B5D54D931C49A28D38E3A0F395E831CD,
	DataFormatter_CreateDateFormat_m7AA0B47893D33CD77956C81610A77326419BC617,
	DataFormatter_cleanFormatForNumber_m51ABF94EB321B3AEA7002D976B0C7E51E6DDB4B7,
	DataFormatter_CreateNumberFormat_m6E51C9D95EB31E918E6F4FF282AFBBD0867C1715,
	DataFormatter_IsWholeNumber_mBE32AD16CF28006AFC610844CCC6347BC12EEFA4,
	DataFormatter_GetDefaultFormat_m5B5C1919E54C507EA9D9F9C5473508A2E5CA4D37,
	DataFormatter_GetFormattedDateString_mE055D37F09052AA5BBDC2C378C2D432D81196E6E,
	DataFormatter_GetFormattedNumberString_m45BBCDD48DC75734A70F2DA68C6E91C74D837CDC,
	DataFormatter_FormatCellValue_mD22498139AE747C65F3704DB60A096049E8ABD08,
	DataFormatter_FormatCellValue_m02660243A830BB4D171F3C107407F25DFDB52E3F,
	DataFormatter_AddFormat_m7302FA2FC8FDD4E92EC7AF5BF0CD45C2032F7C1C,
	DateUtil_GetJavaDate_mFDAD3DD5F0079219F2024A6EB85C0E7B6059C784,
	DateUtil_SetCalendar_mA095BD9ABA6D346012AF48A6407AD4EFF1647C25,
	DateUtil_GetJavaCalendar_m881EE417B868ED7A363C5F5410ECE178117DDEB1,
	DateUtil_IsADateFormat_mC201C001651BA76E56779408BD2B762D8C680978,
	DateUtil_IsInternalDateFormat_mD9146A6D7F59C492779D0A295BC49C6EFBAD1F63,
	DateUtil_IsCellDateFormatted_m5CEF3DC94B5CF3354129B7AE2D667F18F16233C2,
	DateUtil_IsValidExcelDate_m7F5CBF587C9D66A373815ED23556FFF1D4F4E506,
	DateUtil__cctor_mA9807E884FBBA4BDCE01296C84D7BF736F28032E,
	LazilyConcatenatedByteArray_Concatenate_m8460B4046DF405D8FAB2CB43E9167AA592D2B71B,
	LazilyConcatenatedByteArray_ToArray_mF341742704D4138CED6EEADDAEA22E42AC1080B2,
	EscherBlipRecord_Serialize_m2E71FFC0D913AC02A804970E641891BD39B27E37,
	EscherBlipRecord_get_RecordSize_m7B921AF1B3663E79292BAE24F741FC181174492F,
	EscherBSERecord_Serialize_m8C91A0259FD221EBC2E8FDAE8C30E6BAF93B269E,
	EscherBSERecord_get_RecordSize_m13D35BE3A350FDAB7EF5C7319B6789801C50F4C0,
	NULL,
	NULL,
	NullEscherSerializationListener_BeforeRecordSerialize_m1C86AC386A3993B1CC318692844682118E507EE3,
	NullEscherSerializationListener_AfterRecordSerialize_mAD89EBCD059270F39B71FA5DFD905A024EC3D48F,
	NullEscherSerializationListener__ctor_m074A0C73CB4E0AD8154AEBD31EE1A6E0980CD933,
	CustomProperties_Put_m551BDE5284AFAD7D96FB1A4658A3FFD04E089AF9,
	CustomProperties_Put_m9F10B2909E3EAC55B13EDCBD1C79CD7CA519EFC7,
	CustomProperties_Put_mF667DB7DA2C1CE06A1264799E68F0C511376CCDB,
	CustomProperties_ContainsKey_mF42E15E71E762BA1A0C5A505E64F0965BD83BDC4,
	CustomProperties_get_Dictionary_m3933700517372F39A61CAF2BA39DC22F2C269CC4,
	CustomProperties_get_Codepage_m7BFCBE25CF49077D981FDBC300A9470B3FB141DA,
	CustomProperties_set_Codepage_mA6F0475FF36A57B0E1196C327EC5C15A393384A2,
	CustomProperties_set_IsPure_mC142BF56B82A63C044C280CBD6284A16E8AC9C13,
	CustomProperties__ctor_m02FCF47208B32F48303665BB9D68C8F3158B75E6,
	Property_get_ID_mC73CA23AD32EC231806B371083C5C3E334650FEC,
	Property_set_ID_m48D18618F5A1F3C08BD4E21808EF8337CE912D42,
	Property_get_Type_m8AF3DCBF65D897C046E2E45AFC686CC2452A8B76,
	Property_set_Type_m6357AB9A12F41527FFA4A8062F44BE0D3AC5B729,
	Property_get_Value_mF8A3E52E5C741754DA83AF6820C10C5E3C95532E,
	Property_set_Value_mD7747B095EE0E3561EB82A9EFF50EFC5D4107B09,
	Property__ctor_mD3D80D8C8ECD8375483EA175EA14DCA1504B68CA,
	Property__ctor_mB9EFCEF8AECAFF6A40A44EBCD9F928D661330B5E,
	Property__ctor_m23B1AD3E354689BB7FCA2618B31431AD2FA1F66A,
	Property_ReadDictionary_m3DB1C6CDCE76E703C4FE0F0E49F769ABC58CAFB4,
	Property_Equals_m94BFADCEB9B1C59FF181BE94B22B20E0628343C4,
	Property_TypesAreEqual_m096CA025FBB02853A78523751ED3085B257E3D5B,
	Property_GetHashCode_mA51024E537544C645069309FD81248335C4D19A4,
	Property_ToString_m73BADC96B4603F2FCC5125988192D293B5567390,
	MutableProperty__ctor_m175879B971041603D976D702D492D7FBA1B4587A,
	MutableProperty__ctor_mDDB6B09A9BF02A4A0A16EA9535C2E6D5EC4F5DB5,
	MutableProperty_Write_m57893FE433E09F117FBE6604F55ECF2CEA9E0A1B,
	CustomProperty__ctor_mC5B74D769C0D0FDDEE1323C672F0D08A701B999B,
	CustomProperty__ctor_mC4CD6454F4CE5C86D3CBE756C7535F4C26537908,
	CustomProperty_get_Name_m6F5484E61B6055A7306032218464242E6FFFF04A,
	CustomProperty_GetHashCode_mA5CCB16A3E915A2D4B4F74A6C0F9BFECDB1D762B,
	PropertySet_get_ByteOrder_m7FEFBC8085D76A6A6B62A2522717470DEF6D44C4,
	PropertySet_get_Format_m0CC74C1598679E74BAE1690682EB1DBAD49D05C3,
	PropertySet_get_OSVersion_m0E5965049F570384B4AB0CAADA989EF42D2ED78C,
	PropertySet_get_ClassID_m6CA27561056D19AF99738DCD4D66C3A6B36F956F,
	PropertySet_set_ClassID_mAB24F894C77127864B993835FE311C6AB6DBA74A,
	PropertySet_get_SectionCount_mB85DA5473696ADBD99ACCC0B5F5FB50CD2668F77,
	PropertySet_get_Sections_m2E2D89C4A26A61256019773FFD633D6535FB8AE1,
	PropertySet__ctor_m051F8ABB53B1B8B4A1B687FB1B663BDA3823CD37,
	PropertySet__ctor_mF55D3F3CF0D2DBC2999D7A62BB4B33B358AFFD0F,
	PropertySet_IsPropertySetStream_m3E5FDF8597504E6E98A45F91DFB35CD36880FE4D,
	PropertySet_IsPropertySetStream_m349DC2D18910E2726D603E78270569EAB39A5620,
	PropertySet_init_mD1EB354F0FF6290632C04D71D1563E514C2E5230,
	PropertySet_get_IsSummaryInformation_mD2870D07516CA4569EECC031356262104B0AC110,
	PropertySet_get_IsDocumentSummaryInformation_m8C40EE6442B59D27C64D689E5C0251B064CA29FD,
	PropertySet_get_FirstSection_m1F23B2E815821C0E1E0B550FC86187D1B31681D6,
	PropertySet_Equals_m4777DCAEF4587D0B4E3A5D0B37C684805E78F6AC,
	PropertySet_GetHashCode_m819E5EFA9EBB95AC2773C3FED4E6DE42E5049952,
	PropertySet_ToString_m2E73FE35E9960B00B960AB4D3CC39BD3DDCD68D1,
	PropertySet__cctor_m8D3B96C170FC2F17C0FA39CD1E88E0D648ABE8B4,
	MutablePropertySet__ctor_mA582321633DF273A8670D994308486DB4C00614F,
	MutablePropertySet__ctor_m73E15CC350962489AE38ACB92A0F28A2D956D301,
	MutablePropertySet_get_ByteOrder_m605C3E7BD7E963FB913A14AA571C7D9EDDA82C2D,
	MutablePropertySet_get_Format_m87C4AD396891CC1388C16A8FC184576C76F283D2,
	MutablePropertySet_get_OSVersion_m74328B369B84A0A11F873A12DFED29F1D2116B26,
	MutablePropertySet_set_ClassID_m3B250CDE89658D8C3569A44C6932E11A61B65F63,
	MutablePropertySet_get_ClassID_m2C69D6D362503A6878C52EFBB707EA28673F9675,
	MutablePropertySet_ClearSections_mD017B46854E9F35FD08CAC12113F99D3F0290FCD,
	MutablePropertySet_AddSection_m027B793884E1E7060CBA36A64FED8B8664B754BB,
	MutablePropertySet_Write_m87775F7C91E90A88AB4AC437A97E7125DEE1C2AF,
	SpecialPropertySet__ctor_m108A78B355EC4227BAB38B7A9907397AA06957F6,
	SpecialPropertySet_get_ByteOrder_mE41882839A68F5263791AAB8ACAFB8BA9AAB98EA,
	SpecialPropertySet_get_Format_m8E68D2FF4D56558AC87F72B7BC82E5BA155EE46B,
	SpecialPropertySet_get_ClassID_mA6A748188F915679C91DBBBBD3558CA996470A96,
	SpecialPropertySet_set_ClassID_m2E09FDDCB97ACF15F41D8430B4AD94AFA82DEF6B,
	SpecialPropertySet_get_SectionCount_mAFC24A8D159BCB9A894E34A4CC0464FB15F44FF6,
	SpecialPropertySet_get_Sections_m13ADCFCA6AFADB5E6C4DE4B492FE38271DFBBA55,
	SpecialPropertySet_get_IsSummaryInformation_m3D4421C4F09632D20154801080FAF4E0E9F9C8F6,
	SpecialPropertySet_get_IsDocumentSummaryInformation_m59B47B49F3C874E061D9AC088C647AE8B8474DC5,
	SpecialPropertySet_get_FirstSection_m338D687F09884CDC688DD417D4BEAC17DAF7C2B2,
	SpecialPropertySet_AddSection_m7CC80DCA670DDAF296B28861BD531CA18E08382E,
	SpecialPropertySet_ClearSections_m1A79B2C1CF2FD7032B0AAFE86FDFD59C5971D858,
	SpecialPropertySet_get_OSVersion_m47B7A78D029CF75B790D59787E6F30EE18FE5852,
	SpecialPropertySet_Write_m78ECDBDEF2A2F342BFE7D53A334865037488A052,
	SpecialPropertySet_Equals_m4118ADBDED2E73A531992B4446FC2065E6C46FE0,
	SpecialPropertySet_GetHashCode_mA3891CECACC7151C090EA7EBE540EBDF202817B6,
	SpecialPropertySet_ToString_mE5D42A2AE038A658A6A1FF3CE94C38D4F767E9E3,
	DocumentSummaryInformation__ctor_mF728E91657AB8F788958F2CF0D4E6B9A72DC65FB,
	DocumentSummaryInformation_get_CustomProperties_m4DD3B52E09835A24DDEB5E41567D1876E8F41EA4,
	DocumentSummaryInformation_set_CustomProperties_mFAB08CE65590219FEDAEE1FFD5D87DB7A325841E,
	DocumentSummaryInformation_EnsureSection2_m8EE456086E39A2FF9F1EA44B39FBBBA6800E3047,
	POIDocument__ctor_m174D56D1A3FAC2341440A52A67D35AFE2578035B,
	POIDocument_get_DocumentSummaryInformation_m96CB5C743A3870FB59727C880ADD6DF55B48553F,
	POIDocument_set_DocumentSummaryInformation_m6A28E6674569B1E9080D712F5ABF867419B4C632,
	POIDocument_get_SummaryInformation_m489C696308EEDCDD421FB8E97FA35BF2894149CE,
	POIDocument_set_SummaryInformation_mB0A6537493E48B2B33FC6CB6C85533063E0C79B1,
	POIDocument_ReadProperties_m4E3F17AC0E55E5CFB625BE05733C96B3BF8DB85D,
	POIDocument_GetPropertySet_m269B8F797F39B621A5CA2904842F7DF4749B2F04,
	POIDocument_WriteProperties_m5B4FB8B721B48BEA9F5FD38F7186092EE385BC32,
	POIDocument_WritePropertySet_m54FF0CB543E3DF14BF974C0D8ED848CE44661DE5,
	NULL,
	HPSFException__ctor_mAD2E4E6BF7D4DC0E3A765A14AEC750F49B60A9A1,
	HPSFException__ctor_mADF9FBB44DF699130D42F1FA1708D260925B4F38,
	RuntimeException__ctor_mC1A67654CA09AA9AC0B66FB39251DD332D9554BB,
	RuntimeException__ctor_m017A8DCA7CA74FD5269ECC970DE9C978D208D3D3,
	RuntimeException__ctor_m6A252DEF610F9E66328331E4338BEA31593317FA,
	RuntimeException__ctor_mF4B80298200C5B587887FFC4723455C5DF886C76,
	HPSFRuntimeException__ctor_mCACA3439E8E1B0542AE07153586DC865E8836530,
	HPSFRuntimeException__ctor_m0A83BAA8D828753F054658A900F69CB36EF8F6C9,
	HPSFRuntimeException__ctor_m24319AE236E204FC886B2DF25328E42D4497F55F,
	IllegalPropertySetDataException__ctor_mBE57FA14D3442B47A68BEEBCA44814E8F9E81BD8,
	IllegalPropertySetDataException__ctor_m15180A5F6A75E3493CBFF2DDB4AF5FD95C3086D3,
	IllegalPropertySetDataException__ctor_m0F24F41C8D150D6578AE8AEA6AF02A110CC79372,
	VariantTypeException__ctor_m27FAD44BF410B94A081971E1D54A685180F06CE0,
	VariantTypeException_get_VariantType_m6BD6D387D0ACDFE4B3BAABC10551D56D374380FC,
	VariantTypeException_get_Value_m626860CD9849BDBA6161E69674F4C7C6D90F5888,
	MarkUnsupportedException__ctor_m4336781DD4D6CEB1EF355AD112AA1B1EB51BCFE5,
	MarkUnsupportedException__ctor_m657825340776F7EA98C43FF59981D834F3BAE38C,
	MissingSectionException__ctor_m199181D8EE1F66794A0036FF3DCE44C47E485964,
	MissingSectionException__ctor_m11F813845A12DB414172BC9DE39DEE2D37A23800,
	Section_get_FormatID_mDE17CB4EA309F8976FDF481C1AA3FED746ACDFFD,
	Section_get_OffSet_m370D8A687080A68BAB2B8114ADCE48386D25906C,
	Section_get_Size_mA824AF372B635DDCDB94A3F9958F9F3DB9B984D5,
	Section_get_PropertyCount_m3A7448DC12A350A0C8CCD397A3E99A282977FA98,
	Section_get_Properties_m8BFA36160F7F12A917E98FB0D947B879CA42FC57,
	Section__ctor_m6D3919E5748F527B7460F6E9444FA1CDC2D06134,
	Section__ctor_m2788472DFA395D3D2DE365EDD0F12AE404836D0E,
	Section_GetProperty_mD09A0A104CC0B692CA797363AEB503D892734F97,
	Section_Equals_m9B2CD11FC74FF866F6969A6B544DE81A7F722486,
	Section_Remove_mE3FB96B3D8A32EEB65450EAFA8241736F0FECE87,
	Section_GetHashCode_m7851C51892A3371B05AB2ABF24CD6A943492A5E6,
	Section_ToString_m9ED199998FEBCCBA69765331719D9135DFD0C4B6,
	Section_get_Dictionary_mEC0F2F61D31B5FE9340AE7CD949435E77320C8EF,
	Section_set_Dictionary_m885165EDE9D281B396D76CA6EC17738540E49355,
	Section_get_Codepage_m37B5B0279A4CD9FB5C9C6D54DF33E4B5F0972ED4,
	PropertyListEntry_CompareTo_m0AACE0F11EA910BCCEBD444DF1375A7CA4BC79A2,
	PropertyListEntry_ToString_m6E5728904F2F5997A9752CCAB80527183D9F5997,
	PropertyListEntry__ctor_m9658BB7B4DF5FD2AC5E2C4468A0FC3E5B2787DEF,
	MutableSection__ctor_mCDCF5F719AC63E2F6DE5341C7557F737358FD078,
	MutableSection__ctor_mB2A15FF4ECE116C67B5ED365245FD7AC6D172CA6,
	MutableSection_SetFormatID_mFB33131AAD90A64806096A950765A2E43453EE21,
	MutableSection_SetFormatID_mB2B389139E34E310AE24DEAEEF4C3E2860C69F11,
	MutableSection_SetProperties_mB0D9EC3402913E6AEB0C4784F2F571E46339793A,
	MutableSection_SetProperty_mC3A61578912F3A644329AE9DA8CD849F2BC95AB4,
	MutableSection_SetProperty_m55F0C7F6435DB400812000FB434301515BD18F94,
	MutableSection_SetProperty_mDD09B81EB1D3979375497BB313CDF16BEBD14118,
	MutableSection_RemoveProperty_m9F4338E44684CBC04AFB852DBF86ACAED6529208,
	MutableSection_get_Size_mB83BD12D9F683CC41DD0D99BF829B2B7EBEB8B8B,
	MutableSection_CalcSize_m5957F5EBDE1989CA3B0DD9EBA2BDE1379812520D,
	MutableSection_Write_m2825F40F95085EDCC557AC41CE37A2C408F1EE77,
	MutableSection_WriteDictionary_m2A045AE0229B2DD313ECAF3127CE34B7C6AC639F,
	MutableSection_get_PropertyCount_mC0FDD37DFFD5B08E5C9D6B21F4FC5FB50189DF25,
	MutableSection_get_Properties_m73AD8CBB87887F88C24580FD481AB8AE6B6ED18A,
	MutableSection_EnsureProperties_mEC0E5F516EB275DA394C59462B2BBFA65522C213,
	MutableSection_GetProperty_m126FFE0E5B8881CDB4D6C4D91EC478EF45DCD9E8,
	MutableSection_get_Dictionary_m6AB6666D36EB1DCFACD88B9C0F9E170D80A11AC1,
	MutableSection_set_Dictionary_m351C67BC193160AB76B0D7AE1DD21EFB61264B06,
	MutableSection_Clear_m276E94339FD2E65A879F6E8FD0DC44E4D376176E,
	MutableSection_get_Codepage_mCD22A0BDA946AB0A7160D82951CF8CAD980B6224,
	MutableSection_set_Codepage_m2B51B928B49616F7434E9AE8202F99A198F48533,
	PropertyComparer_System_Collections_IComparer_Compare_m1064CBB1330A87E71CCEF8432D911ADE75345FD9,
	PropertyComparer__ctor_m70778C1A661DB42F23573A77D72560780B944D32,
	NoFormatIDException__ctor_mC50F1E0D99F5585DF8DFE874F7C3F32D557261A2,
	NoPropertySetStreamException__ctor_m981FBC9655C2731C2BE8C133E30988F044229237,
	NoPropertySetStreamException__ctor_mF8057FBDAD7EC4526D87DE62FDA97DC94F621097,
	PropertySetFactory_Create_mD154B17C0F6D6EBE637BFF8C42849695F61EB1A7,
	PropertySetFactory_CreateSummaryInformation_mDA74CD70DFB40E68D98C7FFF20310351632CFAC1,
	PropertySetFactory_CreateDocumentSummaryInformation_m5F364E1DBC343E39C97F93BF30558F431FFBE8C7,
	UnsupportedVariantTypeException__ctor_mB9EA81D63C1C304D9980F0E22097C4C210CF2EC6,
	ReadingNotSupportedException__ctor_m60FE7CFBE7A77FE657687F54DA0074C0ED4E6880,
	SummaryInformation__ctor_m72FEB06864BF68DEF283631148DF990B611A2748,
	SummaryInformation_set_ApplicationName_m5006BF847A8AAB81B2A3F0B3C922638E1C332553,
	TypeWriter_WriteToStream_m4C3E50B38616222CD32E1DBCA1C4B88C898DFC75,
	TypeWriter_WriteToStream_m1A43C8EED310D190736D21751A2CDE4CE47A1E33,
	TypeWriter_WriteToStream_mBF65BD1E72B8A871248AF03C4702304AB3AC26F1,
	TypeWriter_WriteUIntToStream_mBBA17D23B19E65E0358BA632350433DE967FDAE0,
	TypeWriter_WriteToStream_mEE2F995A405A8F8B8448C076A87C61DAB7BE9478,
	TypeWriter_WriteToStream_mBE855396535327AEE1C9C10DD0DF844260EC915D,
	UnexpectedPropertySetTypeException__ctor_m67735FE30ABBA602E6FD0B07C611EF1904D4878E,
	UnexpectedPropertySetTypeException__ctor_m451CD805492095D06941EEBB8A4266D39B83CBA5,
	Util_FiletimeToDate_mC5AF04D973C110D52BD43166EF09BFBAD692FDD3,
	Util_FiletimeToDate_m287FD9C1209627B8E99CFE751D692722B5C8EDA8,
	Util_DateToFileTime_m65DE6C5110E169BB53E54EAEE03DC0EA075D6683,
	Util_AreEqual_mEE1805DCBAED01091D12FB6ECEE81279D6AD517A,
	Util_internalEquals_m574FDDA5F2DF61F32A47ADC267EE770D91749DDA,
	Util_Pad4_mF85793D6A0378A0D8AFBB5C33EA15BC5275563F4,
	Util__cctor_mC304A0D091B37E87A79F84161E5362D929D646F3,
	Variant__cctor_m67C4D1DAD6D3FCE482CEC662C34E29C966D2FF73,
	Variant_GetVariantName_mEBAA0EA8B1B87C193BBBC152453161EE029C2E68,
	VariantSupport_get_IsLogUnsupportedTypes_m4BEB68F828AC672CD59F3B1F23251AF4DAC82563,
	VariantSupport_WriteUnsupportedTypeMessage_m74E1820CE7C7A44501570B166C2912D89FF75BFE,
	VariantSupport_Read_m3A3C4ED81D3C2A33A68ED962387D3DE54C1575F3,
	VariantSupport_Write_m3B154078C66A81327D26F89BE57041BDA6CDDDD0,
	VariantSupport__cctor_mAF18827372D9C404F798215BA172EFEA05120FBB,
	SectionIDMap__cctor_m3D12E5EF35621E7E780D6D6026F0B84C84AD321A,
	WritingNotSupportedException__ctor_m170EF0B205D82B436D9C17DFBED50243640E3485,
	NULL,
	NULL,
	NULL,
	NULL,
	HSSFWorkbook__ctor_mBEBA83322F999D7662C6B763CF6109271DCE97DE,
	HSSFWorkbook__ctor_m48F58A7B9D403375FCD149718D7F1ACA1398EC3E,
	HSSFWorkbook__ctor_mE25CEF368A75748FF1D75DA2E6C09F263D15F3A7,
	HSSFWorkbook_GetWorkbookDirEntryName_m11F0872A9A89D8FF41606E8C34AEDF34E95A4613,
	HSSFWorkbook__ctor_mCF632E6FF65D9B711F86A4CBDBBBAA6ABF41E6DC,
	HSSFWorkbook__ctor_m167A74167EAE2886C33194EFFFD6DB85AD512598,
	HSSFWorkbook__ctor_m489DE323AEA1F41A8F243C7D9477B599F37E9FD2,
	HSSFWorkbook__ctor_mBFF5A532DBF7F5F6364DBBEDDB118D33DF584887,
	HSSFWorkbook_SetPropertiesFromWorkbook_m8BD0479F9D2BDBEE01E7219F77F48822A7BD69E1,
	HSSFWorkbook_ConvertLabelRecords_m79D74DA382BA643DA4586964A7E2B9BD98B249DB,
	HSSFWorkbook_get_MissingCellPolicy_mA7B5ECD76A7870F3EDD1F6E58F47D1D0B990D69A,
	HSSFWorkbook_GetSheetIndex_m518B888995E54A5D6A06C497743966A1309C4DAB,
	HSSFWorkbook_GetSheetIndex_m84618793ECE9540F1ADC893F59F7F88AD167E3A3,
	HSSFWorkbook_GetSheets_m1066778BEC6716A0E5F4DEF17EFBAABF0630EC32,
	HSSFWorkbook_GetSheet_mEA1380A7755A929519B0C2FAAC5C14FF2CBD31D0,
	HSSFWorkbook_Write_m93F974AF13AD14EDB17278C378DAAE6A8DABB08E,
	HSSFWorkbook_GetBytes_m4BE08C298BF64168C03C3D406283F113498AD4F6,
	HSSFWorkbook_GetUDFFinder_mEC2904FBAD32482E5441DBC7C6BC0B60A9C39B99,
	HSSFWorkbook_get_Workbook_m8B74C6B603C81DE7617F63113C705CC2470ABC5E,
	HSSFWorkbook__cctor_m5167972BB2433C52C816F0CF8E8117E21746F367,
	SheetRecordCollector__ctor_mD3D41EBCE91BD08AABAEAEEFEBDCCD983DFB1148,
	SheetRecordCollector_get_TotalSize_mAAFE0DA09DF4297877C0C258E9A22D00584EC49C,
	SheetRecordCollector_VisitRecord_mEC9973C8BE7F8A3CE95546B223C4132089468F74,
	SheetRecordCollector_Serialize_m240773C8CAECDA24D0157FD3044EC64A9A516E62,
	SheetRecordCollector_Dispose_mCF0551B87B4A181777C3AA10DD9C8C61F24BF79B,
	HSSFFormulaParser_ToFormulaString_m6EDD35A01845772B47F6F6152A957AD07D84277B,
	LinkTable_ReadExtSheetRecord_m49480F55436F61B937F28C454CE03E39DB641187,
	LinkTable__ctor_m466E52ECA84DF92BA634B803FF0F49D47F88CF11,
	LinkTable__ctor_m2566BA77B671BABAD44BB0A75C01512F0DA819F2,
	LinkTable_get_RecordCount_m40E3F59714C394DC0C5EDE93F0B9E7C832CFDDC8,
	LinkTable_GetIndexToInternalSheet_m74A8466E6179845A8A375EEFE7C029BF49CB0089,
	LinkTable_get_NumNames_mFBF81401C00851FE0170B8A80380B680EF74556C,
	LinkTable_FindRefIndexFromExtBookIndex_m6F1A8D7263B71FA88D09917A7887CB87DA9E5FB2,
	LinkTable_GetNameXPtg_m537343CA8BD866263C0FCDDB23A5CD814FD9545A,
	LinkTable_GetNameRecord_m905F99EEC0E24F28DE9B3F2C4FD9E94A38B626F3,
	LinkTable_AddNameXPtg_mEBFAA347D93566A4A7FC1C834718CA87E5A16281,
	LinkTable_GetSheetIndex_mFFFE41A46D4DC9F5A491C901A62DAD4B40F850F6,
	LinkTable_GetExternalSheetIndex_mA29F99E8D91B752E7181A0AF79AAB1ECF31F4EDE,
	LinkTable_GetExternalBookAndSheetName_m110202F4CCC79A3E38D3AC5C9E54E324EEDD8309,
	LinkTable_CheckExternSheet_m66E890116CAF8A4A9FC247BB9AB8DB04333AC165,
	LinkTable_FindFirstRecordLocBySid_m27A64E0FA24888C28978513A6E5B04161702A692,
	LinkTable_ResolveNameXText_m12807E1BFD85D458FD8183C1750FED831AFE89DC,
	CRNBlock__ctor_m963E6EE0852D8719D242026FE75661651EC3197F,
	ExternalBookBlock__ctor_m7F4A3466918969B2868F27C6977D90FC8726A1AC,
	ExternalBookBlock__ctor_mAE4B594B07FEA6ADDE9FD87EF58943B689C023E7,
	ExternalBookBlock_get_NumberOfNames_m17F6E73824F0917BF7FF8598F2D746BD9462A2A2,
	ExternalBookBlock_AddExternalName_m3B72095B696383492184004FFB3C9DF69263AA11,
	ExternalBookBlock__ctor_m1B59E9FBABED42774979D4F9E909D85BEECA2942,
	ExternalBookBlock_GetExternalBookRecord_m2D8B59E37DB815ADC5A9F62A0C7FE989B93FD3EB,
	ExternalBookBlock_GetNameText_m5EB61322FA47488A08ABCD4F73C23391C811620D,
	ExternalBookBlock_GetIndexOfName_m33448F05E061FDEED5F94EC6B26F2A6EABF2B64F,
	RecordOrderer_AddNewSheetRecord_m2F8D0AC266ADEB5E599795124AF9D5F83C1CF8C0,
	RecordOrderer_FindSheetInsertPos_mEAC59A5C7FECE74427AB0F7974DC702F597060A9,
	RecordOrderer_GetWorksheetProtectionBlockInsertPos_mE44F6251567D333CF252AB22301B75042C98F95D,
	RecordOrderer_IsProtectionSubsequentRecord_m701F64AE0C30A1666094B85B3E50838B79D2FFFD,
	RecordOrderer_GetPageBreakRecordInsertPos_mCEF0477F437AB07C82240D69C10D20D684FCC486,
	RecordOrderer_IsPageBreakPriorRecord_m43CB8F6DEE21C0D81B19C53D1A44EC5195083303,
	RecordOrderer_FindInsertPosForNewCondFormatTable_m37477085A9F801CB02A6DCD6876E994AEE8007C2,
	RecordOrderer_FindInsertPosForNewMergedRecordTable_mF330F73FBA88830A5E88115A7906B1FAF2B47846,
	RecordOrderer_FindDataValidationTableInsertPos_mF35480E67592B9E5100354732B9911ABB7E8CE43,
	RecordOrderer_IsDVTPriorRecord_m7F75E333EF0BE4B96E723DF6AB89DDA169C8EAF9,
	RecordOrderer_IsDVTSubsequentRecord_mCCDDAB4D6789AC8375362D0E1F6EBCEE880FD266,
	RecordOrderer_GetDimensionsIndex_mA283D0A7C2D122E9005D86340E922CD6A2784D91,
	RecordOrderer_GetGutsRecordInsertPos_mA26627C3AD4F56F4A6F352C3215EB6A7A9B58D04,
	RecordOrderer_IsGutsPriorRecord_m6F884DCA68223CA871B6D56A18B5B95DDA548C14,
	RecordOrderer_IsEndOfRowBlock_m00F24F4DEFD3387E8548BAB91B1F9FE6DA83EA72,
	RecordOrderer_IsRowBlockRecord_m3F30AA0969BE855299CEE06CF96C372477A2CE1E,
	RecordStream__ctor_m117AB967713C99CF31C6619ECDA31C86D87D1C6A,
	RecordStream__ctor_mC946AE164AF2C37293AE9A57F82B2F0C09E71998,
	RecordStream_HasNext_mE9B5B319CBA986ABF92FD0315F189F9D80D9C415,
	RecordStream_GetNext_mF746E2A54A9D51803BE3D8E3051A3C28B0FF0486,
	RecordStream_PeekNextSid_mC862FDC3398596567EA5E76876C642B71176EDF6,
	RecordStream_PeekNextClass_m722E758B3A1C92337BCCF867576412CDF83119F8,
	RecordStream_GetCountRead_m1E90E6037CD6AAEA6D76E57598D851A62AC69F36,
	RowBlocksReader__ctor_mD4E75DCDFFDCA34A91E6A7A1832088EA6363CB43,
	RowBlocksReader_get_LooseMergedCells_mC5B01FB3A0DBB7DC7183C9A6CF119A079EFDEE20,
	RowBlocksReader_get_SharedFormulaManager_m2D54D3DFD7C0A797224FCEBEBE2DC5EE1E5F713E,
	RowBlocksReader_get_PlainRecordStream_m054246CCAEC2A7E8388058D0DDBA678A3CDA9578,
	InternalSheet_GetValueRecords_mD4CEC44EF00C8624FC4FDB9EA4FCCC38498E1B02,
	InternalSheet_CreateSheet_m64648BF2CA14E6C19F1FC71682E0F24CDF283538,
	InternalSheet__ctor_m3729E092C4D0BF2A3024D9D8E14866D033AF77A7,
	InternalSheet_SpillAggregate_m0CC3318ABCE36FCE4B5364B56C5E172995D51ECD,
	InternalSheet__ctor_m85D5353F85ABD48396200CAC71FC19F55E6DF405,
	InternalSheet_AddValueRecord_m4BC68BBCCB369207007B1829FE626FCDB323110D,
	InternalSheet_ReplaceValueRecord_mC2DFA70FAB0ACC88F09C12CA0DA3EE1C904EF36C,
	InternalSheet_AddRow_mFF7CD52820A331D9E0CD1A69514145EBA97D2998,
	InternalSheet_get_NextRow_m1647CB0209D73F6AF9B0821BC7EAA6CB7232D317,
	InternalSheet_CreateBOF_mA53E13372DDAD102E65858CDBA94D7568C3043C9,
	InternalSheet_CreateCalcMode_mC1AAED521855153C76C6B145650EC4F15AE50352,
	InternalSheet_CreateCalcCount_mE45DE1F14DD7594DC94A0985446876EF1625F501,
	InternalSheet_CreateRefMode_mC26E1A2090D99584E3A06294CE56BE4D995C2A94,
	InternalSheet_CreateIteration_mEE610D4F037C8BE6510122F4328CA3E2F9F5BDEE,
	InternalSheet_CreateDelta_mC6AF04A33C915BD38A3AF0CE97F39CA7CF976B3B,
	InternalSheet_CreateSaveRecalc_mBF992F3525255049AA62C3CF5B6A5795AFB65EDC,
	InternalSheet_CreatePrintHeaders_mD233BE8C6FF47D0ECFCF7DFD860B515B2708B338,
	InternalSheet_CreatePrintGridlines_mF9F6ED42EC5F040C42E56DE3C44160B263756F4A,
	InternalSheet_CreateGridset_m1E33D62C9ADFBB7D094BA744E67E462C0448120F,
	InternalSheet_CreateGuts_m4690EFAD825C229231EEE63EB49FF40FFDD06779,
	InternalSheet_CreateDefaultRowHeight_m811C7B7AA38F5B86B1989B6A8C3EF38EEBD110DF,
	InternalSheet_CreateWSBool_m80BBD9F93F1939A136E27D34BD554CB29EDE0BC5,
	InternalSheet_CreateDefaultColWidth_m2F0C6E4E192AD253E9152D4206EE56B13D4E86F9,
	InternalSheet_get_DefaultRowHeight_m72EEDE9AF8C2CB774EF276BE53204F2577D2AFFF,
	InternalSheet_GetXFIndexForColAt_m0CDC322BB5D69CE8605B3EC20531D609051372FE,
	InternalSheet_CreateDimensions_mAA78D7D11EB7B6ED11494148C75FE5EBA493B40A,
	InternalSheet_CreateWindowTwo_m12837D4602C1EC3F83D536B516086D6FC34E3466,
	InternalSheet_CreateSelection_mD188BFB78F3F32CE8185354F8B0E897FB752EAB8,
	InternalSheet_get_Records_m9A06C4B54724DEE9BAE99E0FE74B46A0F7222546,
	InternalSheet_FindFirstRecordLocBySid_mE865E5054194A6AA5C77CDEBE939C1C5CCEA6DCB,
	InternalSheet_Preserialize_mCD84CB5C2563D02C1C249911AEDA8BB8D3415AAD,
	InternalSheet_get_RowsAggregate_mEDA8E232849E75FF9978928DBA544A17235F1B06,
	InternalSheet_VisitContainedRecords_mDC72D134F5F6A4FDAB540D5109BBEE0A63EE2002,
	InternalSheet_GetSizeOfInitialSheetRecords_m35A9D7733F4AAE4458C4DBBC155F85E4F09A4E59,
	RecordVisitor1__ctor_m4DC1907DA0642E3AC520C629946C716389227152,
	RecordVisitor1_VisitRecord_m73F18A779576848E5154DC6B75A4672BCB5704EE,
	InternalWorkbook__ctor_m4742DA02A956302D95282D296E422B73587CC7BE,
	InternalWorkbook_CreateWorkbook_mEE43BBF3820442412817422AC902BC7BF2C14F9F,
	InternalWorkbook_GetNameCommentRecord_m75B14021A35CBACA143B63157775D031D20AE0E9,
	InternalWorkbook_CreateWorkbook_mA74EE35DFCB6236F2ED63CCF5937EE26C58B31C0,
	InternalWorkbook_GetExternalSheet_mE682896BBC1DD0611F0D8B4933BEB8B64BD8CCBD,
	InternalWorkbook_get_NumRecords_m4E768375113A58CDBAAA8606F47A2CC872586D5A,
	InternalWorkbook_SetSheetBof_mB32626D753B251BA7841ECD5417F113F88066DFD,
	InternalWorkbook_GetBoundSheetRec_m1B1E0838FC38163144AC926C5D22B2C5EC36D0E4,
	InternalWorkbook_GetSheetName_m198D1A75213A8635F793B4D1D6D0C1DBBBFCB03F,
	InternalWorkbook_GetSheetIndex_m301048FC40152A38BCDEA82BA7F8F875CA893997,
	InternalWorkbook_CheckSheets_m59ADE30F97265A83EC0701993AF02AA52AE9CA13,
	InternalWorkbook_FixTabIdRecord_m09E64B7D5F7013B5FE9CDFC4F190B8102605FFB8,
	InternalWorkbook_get_NumSheets_m9B37EF3BD59BBF945526DC5EB0DB129598814BF9,
	InternalWorkbook_GetExFormatAt_m738D463D1311B3796A946B56A200329E91C58C82,
	InternalWorkbook_AddSSTString_m9C45FC9F79157DDC1BA7DD742A519CCA360AF303,
	InternalWorkbook_GetSSTString_m34F7143963A7898590FBF724664E58A4296510B7,
	InternalWorkbook_InsertSST_m0027CEF188B3A5AF6C7A1CCBB3AA318E2006FF4C,
	InternalWorkbook_Serialize_m421FF7E153F341090F75A6B6101CC2BFBF0D5651,
	InternalWorkbook_PreSerialize_m7BF0411B4982E8619B3FA34EEC3889E7B212EE8C,
	InternalWorkbook_get_Size_mEB4A06BF87B36AC2B5E4309B54C6B167D7668F41,
	InternalWorkbook_CreateBOF_mEABF08927E3F2C4EF3323161EB8B3E7D44C6B6D1,
	InternalWorkbook_CreateMMS_mBF7A35DB81A7183BE6B251BA684A249A892E3BEA,
	InternalWorkbook_CreateWriteAccess_mF525B7F27407ECAD3EE9CAD148597C4ACB3013D7,
	InternalWorkbook_CreateCodepage_m1A537C2C30E2463E96AE311ADFEF855CEC2DE1CB,
	InternalWorkbook_CreateDSF_m08D2824AA694DF15BCC71DC20E2346ADD0F2B19B,
	InternalWorkbook_CreateTabId_m4A2A3B046113C665C88947A5AAE8BC6D658F562C,
	InternalWorkbook_CreateFnGroupCount_mF5325F7101295B0EFA66D3A95419D6EACC8BC8AF,
	InternalWorkbook_CreateWindowProtect_m0B708EB532833C33F985F598A0ACECDACDB4CF4F,
	InternalWorkbook_CreateProtect_mA4AF57A79EC84083A0F0FB14124E1B79A61EED8C,
	InternalWorkbook_CreatePassword_mD8E05D0BB41EC6FCA26F57B2D801D42D9EE1989F,
	InternalWorkbook_CreateProtectionRev4_mFCA56839B2E6379508AC651F7777BED7E51C24A3,
	InternalWorkbook_CreatePasswordRev4_m590EADDA6991DD280ED8E9BE7ED60A77E5E26D5F,
	InternalWorkbook_CreateWindowOne_m31181DBF14CCC391F988BEEDF124BAD0591CD1E3,
	InternalWorkbook_CreateBackup_mB1D5835FF0B095BE4EC690FCDB9F34347F864A8A,
	InternalWorkbook_CreateHideObj_m9C0C022832B82CF05EA2F87368B7492C37AD0ABE,
	InternalWorkbook_CreateDateWindow1904_mCA97BCF4001FEE7BE79CF11182F756CBB3337F93,
	InternalWorkbook_CreatePrecision_m3EE62D32059D0B71BCD1FE9F14A01825BAD6CB37,
	InternalWorkbook_CreateRefreshAll_mCE9A33866DF9003A55799024B7FB2C90F2B18CE5,
	InternalWorkbook_CreateBookBool_m31E5E0988430A9EABE5C18B7F533165129589C0E,
	InternalWorkbook_CreateFont_m774346DD541B9ABF9289BF2F637C8A2E0BD23323,
	InternalWorkbook_CreateExtendedFormat_m7817F1254E670245525B759E769E7F36E39354C6,
	InternalWorkbook_CreateStyle_mEB7F7371745B91EC42BB72648FD654B899C44AD0,
	InternalWorkbook_CreateUseSelFS_mDBD6AC42C858972DF3E16377610CC7860FEBEC6E,
	InternalWorkbook_CreateBoundSheet_m4B14B3B454BB87459185B207087B35D9B273C558,
	InternalWorkbook_CreateCountry_mA0C29834B02836F1DD78813E088560F202F696FD,
	InternalWorkbook_CreateExtendedSST_m6CFDBA6ECBED877C2B7A9C826B12FEF3031182E3,
	InternalWorkbook_get_OrCreateLinkTable_m6E3B561B184C2AC37BA7C29CF1059EF23B6B1449,
	InternalWorkbook_FindSheetNameFromExternSheet_m07F8627501D00FC28DCF52F627765E8402E85B1F,
	InternalWorkbook_CheckExternSheet_m388D310FDBD4F4C0856078ACFDE1E55FEB4AFFCF,
	InternalWorkbook_GetExternalSheetIndex_mC0C1B81F0A5DACBEA4FC9DFD199CFA80B4790D85,
	InternalWorkbook_get_NumNames_m16E0C82C90234C339A8A9DDDEB33D28543088A34,
	InternalWorkbook_GetNameXPtg_mAF85063F84EEDA7E8ECC31646D2F977A48B36F4A,
	InternalWorkbook_GetNameRecord_mD38044D8C7A63B22E480E26AD031057CDECF68A7,
	InternalWorkbook_get_Formats_m11C1F4D4F02C6180767D61145FB01CEBDB0A8E43,
	InternalWorkbook_CreateFormat_mA3D3C480687565F2DEEC5B84810D5DE11567EA47,
	InternalWorkbook_get_IsUsing1904DateWindowing_m22836F2CA0CDF777B78B99B8103D9ED4E4089A25,
	InternalWorkbook_ResolveNameXText_m81830126ACBC6154DA2CFADA8082ED1188654340,
	WorkbookRecordList_get_Records_mE89F6DC492CD3D1C3CEFE865A932D8BF3BA2490E,
	WorkbookRecordList_set_Records_m0655B584FA875F3EA9F08EA5DD43A86F5333F739,
	WorkbookRecordList_get_Count_m0A3DB0D19B6F7C8C8BD9FDFA36CD0DD784D05F5D,
	WorkbookRecordList_get_Item_m5F91449AF833534300FFC93C5903B7211F7592A0,
	WorkbookRecordList_Add_m43ADD93156D5A565E11943CE58D4BB3C8A329275,
	WorkbookRecordList_GetEnumerator_mB5652616072DEF6D7E78B76A6776B43C0B2AEF92,
	WorkbookRecordList_get_Protpos_m37F88B5C41357C7E55ABFCD0C609D82EC74C4C76,
	WorkbookRecordList_set_Protpos_m5F894AF0BF584A11B307F84F4B354DE00FEC75B3,
	WorkbookRecordList_get_Bspos_mF082654D2B6FC73F90F7029473170DA688CC236F,
	WorkbookRecordList_set_Bspos_mD7F24C04DEFDF9FAB1C6187AEA5BC7EBD8E5D9E1,
	WorkbookRecordList_get_Tabpos_mA1E74C84E23C979502A287D9CCC00460F9FB85B0,
	WorkbookRecordList_set_Tabpos_m4D34260E87BAEB5E0A9B3457ED9E70B6D6BAD2D2,
	WorkbookRecordList_get_Fontpos_m3E8018546AB4CE689E0E0FC06EE4468FF27065C0,
	WorkbookRecordList_set_Fontpos_m92893074603DD1F575071B839C646D5DFDEC445D,
	WorkbookRecordList_get_Xfpos_mEEAD10E3CB777B2C7E3993B49477B8E384A0B0B7,
	WorkbookRecordList_set_Xfpos_mCF5FD3A401C71C70B95F0DE567747DC192CB1D93,
	WorkbookRecordList_get_Backuppos_m2DDDC6AD8367FB5E0CB13A3A5F1D530F702BA858,
	WorkbookRecordList_set_Backuppos_m98685CE4CEDB486CEA07BD4EF66433D76C707F53,
	WorkbookRecordList_get_Palettepos_m17A8DB680519127BD08B582B16925022FF373010,
	WorkbookRecordList_set_Palettepos_m99319D9CF724B28EA7E8B6B097FF77E46F640F10,
	WorkbookRecordList_get_Namepos_m6D0292EA108FF4C6118C657C772E07FEC9EC4DA4,
	WorkbookRecordList_set_Namepos_m24597AAB041D1458188E58033B225753FF2B3DE3,
	WorkbookRecordList_get_Supbookpos_m2C6D388C0FEA69DDDEB469B29D97D0F42DCDE1D9,
	WorkbookRecordList_set_Supbookpos_m778E4255BD42B86A00003A7EEB2C4E7D1783045D,
	WorkbookRecordList_get_ExternsheetPos_m76C218A02FB903A4B084332C7C20E76C16D9EBAA,
	WorkbookRecordList_set_ExternsheetPos_mD7E93B3B1A9C32DFE8EFB2CD4D06BE66EAC5F54A,
	WorkbookRecordList__ctor_mADFAC13DF9FB3EB6C05FD371EFA20D2BB1310E8A,
	OldExcelFormatException__ctor_m81D658F813B8D491FE0705004CD464CD4BAF0F31,
	AbstractEscherHolderRecord__cctor_m017B7704E917C4339B487008D68DE697D074D3FB,
	AbstractEscherHolderRecord_Serialize_m9C1533E2963FE4CE4EEFCBCB512A5EAFB1662EC0,
	AbstractEscherHolderRecord_get_RecordSize_m26DA84D2447EFBC3778042B5F06AAB9ADAFF5F30,
	AbstractEscherHolderRecord_Join_mBDA4AABAA46E7B8DD9E38D06181347952FAEE7DF,
	AbstractEscherHolderRecord_ProcessContinueRecord_mE1E57AEE075E677FD83DBA5E1595B4561663D2FA,
	AbstractEscherHolderRecord_get_RawData_m3CF9FF62453E44356BA40735ECA2EC1A0400BB19,
	CFRecordsAggregate__ctor_mC364273D3F58D662864308F664C029E8A6B79440,
	CFRecordsAggregate_CreateCFAggregate_mD3E96F7A6B0B726DF9D5FA232183BCF9F0754AFA,
	CFRecordsAggregate_VisitContainedRecords_m35130CBE7E3C1BAE73D4D2B89CD6382DDAECA998,
	CFRecordsAggregate_Serialize_m037BBC0E2534BD725C7A93D7915934AA001206D4,
	CFRecordsAggregate_CheckRuleIndex_mE684FB67649071615D116F814642848174389E7E,
	CFRecordsAggregate_GetRule_mC7242676FC917408A5C73108B446E81249CF9ACC,
	CFRecordsAggregate_get_RecordSize_m5BBAD5F6C86B4355227C6318E9831F035C3C7F9E,
	CFRecordsAggregate_ToString_m8E4C8A1137A3690A87B9D4303DFE9F3DF94CB363,
	ColumnInfoRecordsAggregate__ctor_m27647626478945607AE06213B5E971987FEFBEC2,
	ColumnInfoRecordsAggregate__ctor_mB6D873861E200C0BEB9E97F5FC7B19A2935702FC,
	ColumnInfoRecordsAggregate_get_RecordSize_m0CE8CE66BC7540FB81B8417137409F77B81CC6F0,
	ColumnInfoRecordsAggregate_Serialize_m36C1A6FC718387CA73F64ADBA93EAE4B864F9B2A,
	ColumnInfoRecordsAggregate_VisitContainedRecords_m2411EDB61BFC53A07CDA3C28E452E7EF7E732A6B,
	ColumnInfoRecordsAggregate_GetColInfo_mD3D7BED03B498E53244E96C4A7BCF59B9C5DFC62,
	ColumnInfoRecordsAggregate_FindColumnInfo_mA849FAC4B4A143A48D2396FAA820B09CDB1589C5,
	CIRComparator__ctor_mF21E5FAEDDD9DFCDB4AED8448B8B3DB0D0BCA481,
	CIRComparator_Compare_m3BAA89779072ABE0A285DBBA25483DB69F0F832A,
	CIRComparator_CompareColInfos_m8DE6DD6369722F5143D017AD35E28D691146865A,
	CIRComparator__cctor_mE9DF81FC39F8D642690866DCA99E478E67D9AE61,
	ConditionalFormattingTable__ctor_m5405FB6A6573B1B8E14079E2D191A6EF93B3E350,
	ConditionalFormattingTable_VisitContainedRecords_mA3708520C41FEAEBB9A1766D6E2396C1F1E3CCCA,
	CustomViewSettingsRecordAggregate__ctor_m6435990EA90075BB5C544C3661F90E45572011CB,
	CustomViewSettingsRecordAggregate_VisitContainedRecords_m26A3B8BB8E094E3850C44A3E3B2C4271C844542D,
	CustomViewSettingsRecordAggregate_IsBeginRecord_mD437DE1A5972C386ADBAC88FD40321FD3111575B,
	CustomViewSettingsRecordAggregate_Append_m22951626B90089A1A2F16E78E7AE22A0C32BE4A8,
	DataValidityTable__ctor_mC996F5A70A03FFAA8DBEEF9D12D67ABA27B59164,
	DataValidityTable_VisitContainedRecords_mEE0D017F45A206BEF64A1418089BE580AC5CF531,
	NULL,
	NULL,
	NULL,
	FormulaRecordAggregate__ctor_mB9D631E5ACA6C578E70913C42F4727D15AF9FBFA,
	FormulaRecordAggregate_NotifyFormulaChanging_mFA2ECF7A27E9EC4F5203B249D8605996FFD02E2F,
	FormulaRecordAggregate_get_IsPartOfArrayFormula_m0615E83CE4E493C42D4DB88ECC4470F41D9194DB,
	FormulaRecordAggregate_Serialize_m6088943A40CB25FAC45D1F5A170A1C74DF369C99,
	FormulaRecordAggregate_VisitContainedRecords_mB5E7BC3AED168E18849031F5823975AF696C61AA,
	FormulaRecordAggregate_get_RecordSize_m89CBCDBBA46BCC9F14B646226BE2A65670569CFE,
	FormulaRecordAggregate_HandleMissingSharedFormulaRecord_m6B8E375163F9BA29D6ED842FA392E84A947F1F35,
	FormulaRecordAggregate_get_FormulaRecord_mE33C347FD968811B6D14EA5872C1B5A470596B21,
	FormulaRecordAggregate_get_XFIndex_m69725CB594956F0CEF3F208D37DA6E0908F6D20E,
	FormulaRecordAggregate_set_XFIndex_m929EDACCD3ACA19A3D26A3C1E9725953646CF173,
	FormulaRecordAggregate_get_Column_m4142983F214CF7A48A74AFE989F88EA68E9EC829,
	FormulaRecordAggregate_set_Column_m6FE3C945F4F85496CE9B6B9C58BC9979B889C4F4,
	FormulaRecordAggregate_get_Row_mA651F61CA132508D1C450A576B7AC3E04ED400BE,
	FormulaRecordAggregate_set_Row_m189338813F12DCE945988769E4D188F8EECEE83E,
	FormulaRecordAggregate_CompareTo_m6222CE8B18F4BB24A4205913B25D494F995BBC09,
	FormulaRecordAggregate_Equals_m37F13B632CE527BD3A0DC2982B23231B7EE5DDFC,
	FormulaRecordAggregate_GetHashCode_m2DD1EC6CD7C19CB35F4BEA5B985BCD0627B6CE1A,
	FormulaRecordAggregate_ToString_m16ECE89FAB9465BDF50C2DA2411E1C303AFAD3F4,
	FormulaRecordAggregate_get_StringValue_m7B20A542031363C8CECF0E0037BF5AC20D93B573,
	FormulaRecordAggregate_SetCachedDoubleResult_m3D5FBE5143FD14B233BBB4D2AB50E2F427966C11,
	FormulaRecordAggregate_SetCachedStringResult_m87A80BC44BCD7999AEDB8C898ACE0BE562C4C6DA,
	FormulaRecordAggregate_SetCachedBooleanResult_m2A58DF3ACCD8990AC5C5F050BE1AB6B445D900B0,
	FormulaRecordAggregate_SetCachedErrorResult_m57E0FE8BD6430E1636C61C24B4783EE4DB16F295,
	FormulaRecordAggregate_Clone_m6BAF30ED571E57DA1600CA98A2FB9268FF60A547,
	FormulaRecordAggregate_get_FormulaTokens_m40BBC3F0CF25A7E6335B526C71C37A028A5ECC46,
	FormulaRecordAggregate_UnlinkSharedFormula_m9E51F67CC2BF45E6044477D39D2DF78613E4346F,
	FormulaRecordAggregate_GetArrayFormulaRange_m79079431CF13294B5A57399094AA72A1AA7EAD41,
	FormulaRecordAggregate_RemoveArrayFormula_mBCAEA618395F7215C2F15195BABAE5C30E3A0C0D,
	MergedCellsTable__ctor_mF9A745C310E576D4358C7EC8FC6A7C8C00277A1D,
	MergedCellsTable_Read_mE5F302186D972296D07F60267A8520C2B3E1565D,
	MergedCellsTable_get_RecordSize_m872F87904143058542B90AB3D4ED694DBF578302,
	MergedCellsTable_VisitContainedRecords_m07CE63498D48C10C49A42BC5F524F7D0885E91E9,
	MergedCellsTable_AddRecords_m34BF79D7645D3F41E2491AEB75B0CA27A36BD031,
	MergedCellsTable_AddMergeCellsRecord_mD482F4B8E7605D2F8DA0C86E464A8C7025C04254,
	PageSettingsBlock__ctor_m8E6813A93422DEC4AE96CA5A50B75D18CAD3E17A,
	PageSettingsBlock__ctor_m2E3A1B8A82A6AE4F2DB61918B83EC353106276D1,
	PageSettingsBlock_IsComponentRecord_m82476E2FE26BF3BFD8E31817852763600ABDFF3C,
	PageSettingsBlock_ReadARecord_mD097342646961CD67191B8CD8D213F9B9C69B1A0,
	PageSettingsBlock_CheckNotPresent_m4D41B4FC865DD806C63403B99C4DCABC36992930,
	PageSettingsBlock_VisitContainedRecords_m55FBBB11CACE63F9CD6487337699A178C5B6894F,
	PageSettingsBlock_VisitIfPresent_mA7F5AF761A33C7A2C813F4E5666EDD98EC2C385A,
	PageSettingsBlock_VisitIfPresent_m17CAA165A1DA92E715D2E06D1C745CD1D6D633AB,
	PageSettingsBlock_CreateHCenter_m40E4A0E85309AD3087127267124DB869AF8AFEE7,
	PageSettingsBlock_CreateVCenter_m4635A5A77E5178A45CB5ECB69ED799ECD2531357,
	PageSettingsBlock_CreatePrintSetup_mF8C5FCBD27C3F2C3C00553291542E5A03C24FAAC,
	PageSettingsBlock_AddLateHeaderFooter_m6FBF17FD92C402BE24EC00B9754E166F7E3A0B35,
	PageSettingsBlock_AddLateRecords_m6FFA04BD54706BE808AF3528312F9D99E59E8B12,
	PageSettingsBlock_PositionRecords_mAFB2EFEAAB9F7BC7DFFEF29A4D600177D9FF407E,
	CustomRecordVisitor1__ctor_m053515A95DEACAC8E7E20A24F656B775037B78BE,
	CustomRecordVisitor1_VisitRecord_mA18902558B7C1A0C190CBE6E105BFFF03B5B7DE5,
	PLSAggregate__ctor_mDE489484D25EF0513E9C79A309804714DC4E35B1,
	PLSAggregate_VisitContainedRecords_m46635C8D338C92DAC12C000DF147C5A32DD8B86A,
	PLSAggregate__cctor_mBF14FE2830F54B9C7476C7B99561114D95A41AC7,
	PositionTrackingVisitor__ctor_m319B490A3C2C6AF71401A32135A2FF2D29C04A4D,
	PositionTrackingVisitor_VisitRecord_mC9AF3B585B583E7A3ADCCDA5C96A27DBFA803D09,
	PositionTrackingVisitor_get_Position_mA5FFBCD741E6CF87477AE6C8B03A7CC93309138C,
	PositionTrackingVisitor_set_Position_m75569638CBF6BB41AFD18025DCA274B2F5B4D8E7,
	RowRecordsAggregate__ctor_mA6490AA48D62F0A61559EB7DB269F532993FB4FE,
	RowRecordsAggregate_GetValueRecords_mEFC4BB9EDDFE1390D344EE8C2D2B0D6C6D2FD6CB,
	RowRecordsAggregate__ctor_mBE13EA56E859C522043C3F5A60505446727313B4,
	RowRecordsAggregate_VisitRowRecordsForBlock_mB786FD64C4A3A9C989DE00AC41A3E7D181AE2EA1,
	RowRecordsAggregate_VisitContainedRecords_mAD7D29128B29621AF46295511CDF4DF8A75DD8DD,
	RowRecordsAggregate__ctor_mD9812990501AABF3A95ED646BEE8D0B60CFC252D,
	RowRecordsAggregate_AddUnknownRecord_m6A4D6C461DD21136BF9E95E013008507F3A06721,
	RowRecordsAggregate_InsertRow_m0B054F626CBAFDC07E455F36FB60A5E2A69F95BD,
	RowRecordsAggregate_RemoveRow_m45CF8093262956BB85B2B9A5BBDB9AE82397950F,
	RowRecordsAggregate_InsertCell_m1E5028EE28912751DB9041277E0F4D3F20DFBA22,
	RowRecordsAggregate_RemoveCell_mD969CAA8114A1FE8F4F3C371C39AE8DC4634CB1A,
	RowRecordsAggregate_GetRow_m518F3828D47CE9FC3BF3C6AA644C88B23B0A82A4,
	RowRecordsAggregate_CreateFormula_m3BE3518E1924A206538945588BD2FB009080E01F,
	RowRecordsAggregate_get_RowBlockCount_m3C6B2E19F7F39D17F4C3033463DDBB4DBCF2264E,
	RowRecordsAggregate_GetRowBlockSize_m9F72DD19C7A6A455360CB00846DD8DFB6E5EAC54,
	RowRecordsAggregate_GetRowCountForBlock_mE58E9891CAF2566E649A87BEA666BF0948D3F11E,
	RowRecordsAggregate_GetStartRowNumberForBlock_mD3DD9BAD566EC12164A91A035D92F0809D70C401,
	RowRecordsAggregate_GetEndRowNumberForBlock_m0F3E9CB9FD6D7873C53042AF7C7D125A7D5AD0AE,
	RowRecordsAggregate_GetEnumerator_m662D7E84C2D17E4CE69222361ED0921A0C383F4C,
	RowRecordsAggregate_CreateDimensions_m26772D633C8FD80582D527889F129E6E79C58E5B,
	RowRecordsAggregate_CreateIndexRecord_m075FB813908111B1718FB3B12D2A4E6827061114,
	SharedValueManager__ctor_m1974DE9509A7493E7899D8DD998A052D6D8CF25F,
	SharedValueManager_CreateEmpty_mB7B670210892B75DBA35B091554E0232A7BE65C2,
	SharedValueManager_Create_m9AA85A919294C1A1B55647B32CF488DEB04720A3,
	SharedValueManager_LinkSharedFormulaRecord_mC87DC76C4A84A81D79534312788EC1FCB94FA3B0,
	SharedValueManager_FindFormulaGroupForCell_mC83BC39640C3AB72FDE0258BF15FF94B7F4380AB,
	SharedValueManager_GetKeyForCache_m08FA885EBD2E06E710EEB3AC6335E22B1EC8E0D9,
	SharedValueManager_GetRecordForFirstCell_mD3A91FE534630D95E67B031C270BC57946CFA5D9,
	SharedValueManager_Unlink_m8DFB9FECFFB5449D5B335CACEA09B6345332A333,
	SharedValueManager_RemoveArrayFormula_mB3DC3E413FCCEFBCED10A23BD9AD7546BF973822,
	SharedValueManager_GetArrayRecord_m20B9FFF800CC0268F340AB0F4096980E816907B8,
	SharedValueManager__cctor_mE637D67A9D38E8724C1302810AE3548A55523C18,
	SharedFormulaGroup_get_FirstCell_mB56AC2FABB527AF3959827EED275D5B1981BF379,
	SharedFormulaGroup__ctor_mAE412200241228B144EB5E66E6030F5FE3AF9525,
	SharedFormulaGroup_Add_m4457B77E4C1EC53C16B3FE08E75FAE0AFE3F091E,
	SharedFormulaGroup_UnlinkSharedFormulas_m5D53AB8F73FCF46725A3B5300C45FCF13A09A47D,
	SharedFormulaGroup_get_SFR_m95ADDDC472C523C5142C0E22628A92063274E06C,
	SharedFormulaGroup_ToString_m0BA31A5FC63B6EBE35569A27C1A0B4B59DFF52E9,
	SharedFormulaGroupComparator_Compare_mFA3439A781C710586BD8E4A04566147028B62E7C,
	SharedFormulaGroupComparator__ctor_mCA2B5956D60B011B0BBC5D4F7D8CDB66C371059D,
	ValueRecordsAggregate__ctor_m4A6E50DCF119EFB4316F8A5D28EBBA2C867C4E08,
	ValueRecordsAggregate__ctor_m23EF022BCC8BC55BAD0156907B635F46A5E25E8D,
	ValueRecordsAggregate_InsertCell_mB46C0B4DC6BEF6A09913AFFACB92060F31308B7D,
	ValueRecordsAggregate_RemoveCell_mF2FA8760BDBF9EE9F50DB3553FF6215F62015F57,
	ValueRecordsAggregate_RemoveAllCellsValuesForRow_mCED3E92368EE96A0E6C8A516DE11B2E17E0CC85C,
	ValueRecordsAggregate_GetValueRecords_m9860BA578F7408938506725B221D44A75FA77CDA,
	ValueRecordsAggregate_get_FirstCellNum_m50BCF2F466E183DAC78296DD9273267F12B474AC,
	ValueRecordsAggregate_get_LastCellNum_mA99F71375380DFF3B60AB3156F79D757254551F8,
	ValueRecordsAggregate_AddMultipleBlanks_mBC6A16D7D1DEB428D1C51DB2B2C20F27AB0915D6,
	ValueRecordsAggregate_CreateMBR_mDD9AA044A376F26C6F33D7078C3541BC479DEEED,
	ValueRecordsAggregate_Construct_m1F7CE0702E3B58A704B9C790FBE28BAEF6EAB86A,
	ValueRecordsAggregate_GetRowCellBlockSize_m4F5AC60AB88007E49D85BD1B4D7949C19877ADDA,
	ValueRecordsAggregate_RowHasCells_mA6780B5A5A8D4783FC5164F1519EB49F8F3477BF,
	ValueRecordsAggregate_VisitCellsForRow_m147787A6A280BB3261D1B5520F98BEDEE387F847,
	ValueRecordsAggregate_CountBlanks_m89CA8A2BAB20966E37029F99F49361AFABEC1FBC,
	MyEnumerator__ctor_m82FB1D28FDE504FB67D3AAF7042053B4D9A1D8D8,
	MyEnumerator_MoveNext_mF19A2735748CE8EF0F01F42C2CB3DD697EB0BB0A,
	MyEnumerator_get_Current_mEB10713DDFCC2D24644BD4D2B0AFB0796705BB97,
	MyEnumerator_FindNext_m1F30F719E0CE513219F88CA79E76834621292CA1,
	MyEnumerator_Reset_mFD84D7E712CA8B6E549D5A6DBB50344CBD3A2C2C,
	SharedValueRecordBase_get_Range_mF46DAE7D4FA2F1B3723FD71635C9BCD35D91DC19,
	SharedValueRecordBase_get_FirstRow_m2C5046AA7BB4E296230D707663FB6E2E6433C814,
	SharedValueRecordBase_get_LastRow_m907CE817167D90288FF97373EE2C6CF5696BE306,
	SharedValueRecordBase_get_FirstColumn_m3E272FEA900C2C0D2FC17E7FD59A4D42E2DEEEE6,
	SharedValueRecordBase_get_LastColumn_m6EB6C9CEDE811566FA40F86549C50CBEF44BC252,
	SharedValueRecordBase_get_DataSize_m248736F8D2E30B59DF8D276DBBAA5E6FA937CA98,
	NULL,
	NULL,
	SharedValueRecordBase_Serialize_mE3BE67ECF38BB542F9DCCC46059AC60F95C6413B,
	SharedValueRecordBase_IsInRange_m57D088A6634C704A820D64304E553A4817064C5D,
	SharedValueRecordBase_IsFirstCell_mE1552663312516A04540FD184DB2D1023AE52C57,
	ArrayRecord_get_FormulaTokens_mAF016A6AAFFBCA2DC9D193F8E92C5613F5A38837,
	ArrayRecord_get_ExtraDataSize_m5D44BCC89B6AC88D52C7C815CA7F2CB18CCABF4D,
	ArrayRecord_SerializeExtraData_m68D8C07CFC57EF69EFBD724067809478235632A9,
	ArrayRecord_get_Sid_m18A24AA0860D66C67A3971EE9BA164276A7927F2,
	AutoFilterInfoRecord_get_Sid_m8713E726C540BF0B414E9056B49DA42BA8EDC052,
	AutoFilterInfoRecord_get_DataSize_m411F54EB20918C87388388DFA6B5F3AD4F3330BD,
	AutoFilterInfoRecord_Serialize_mE3C53A5AFA1C390D645909C8F7EC5B4551335554,
	DOPERRecord_get_RecordSize_m8238250FC5C414B6590C5389C1A7A17F30E21360,
	DOPERRecord_Serialize_mC55E70F9EA6728AE956D7243C115A508916ACF4A,
	DOPERRecord_Serialize_m4E516D30D8E208A2EF47620166829C7CFD311DF6,
	DOPERRecord_get_LengthOfString_m38B9B0B3FCBC5E83374A8BCFCE4C30F8208B3AB2,
	BackupRecord__ctor_m7252B3BBE153130131717864795AF206F0A7CC70,
	BackupRecord_get_Backup_mFEE323D63CF4846084E06CAA1C0BCBADE5B7A471,
	BackupRecord_set_Backup_m349C09B6B64C5DF92B6E5389E115F5071901DBAE,
	BackupRecord_ToString_m8461B405D9B98CD1D0E2822DFE7CA1E04A1143AF,
	BackupRecord_Serialize_mDD40A8A1122308FC61CF68CD73C895097A81D87E,
	BackupRecord_get_DataSize_m3F575A3011C7ACF60D6AD0779832E9DEF958B1CA,
	BackupRecord_get_Sid_m42A175EC4C08349BB2A6211EE314DFE7179BB277,
	NULL,
	NULL,
	NULL,
	BlankRecord__ctor_m7FB8C49E697380477AED4E53414AE6E2818FDF34,
	BlankRecord_get_Row_m61506427DD019A2873ADF6A0D96151EDE8D40F78,
	BlankRecord_set_Row_m1571613A46177E9CCC22638BA5D63FCFBB146EB6,
	BlankRecord_get_Column_m58C279D78D58EC3CDCF87FC37072BF4ED69E66B3,
	BlankRecord_set_Column_mAA43FFC11E5A462699BDB68CE4F0901878E2F1BE,
	BlankRecord_set_XFIndex_m4303E8568BCAD37A2D6C8E2C68105C1088E8CF05,
	BlankRecord_get_XFIndex_m59412B3F7721C2B73B34CC872CB4AC2710276F99,
	BlankRecord_get_Sid_m902598D4BB9AC0594DDA5DB2D6F2B3D02F6A5166,
	BlankRecord_ToString_m76927070847FD5FD9DDF1F5FF472C3CA7F88B8E0,
	BlankRecord_get_DataSize_m6C462051526EF9A06BBDF1FDE249F737CA229012,
	BlankRecord_Serialize_m761584383E7A4444613095B8A0899DB0763C04AF,
	BlankRecord_CompareTo_mF2E6130D55F2E54287D235C08183F75F2795972D,
	BlankRecord_Clone_m8BC5EF827D712CE7E26DEEFC57583CC585638DC6,
	BOFRecord__ctor_m8D6F00C6E53BE4F6405F20592ACD168E49E66FB2,
	BOFRecord_set_Version_mEF8B530F1FDECC980332ED3BD717B853A5507359,
	BOFRecord_get_Version_m2871AAC4493A07534997E1EDC4C19B197847284F,
	BOFRecord_set_HistoryBitMask_m562BA7155D37F4EDEA343F501AEEC769B05E6602,
	BOFRecord_get_HistoryBitMask_m27A6D9F3C7E00D0FC05282B82A7763D4D7AE55D1,
	BOFRecord_set_RequiredVersion_m8014E704F27493A107C47CA01BCFF1D6FC321105,
	BOFRecord_get_RequiredVersion_mBA104D778F80BB9C9ACDC18F7E13A6389157CA54,
	BOFRecord_get_Type_m098CEBE167D29BFEE2C976EE102EE4465A6A4497,
	BOFRecord_set_Type_m00979233FDC297894AF7CCCCB3B8B2B2C40319DA,
	BOFRecord_get_TypeName_m24D7ED37FD52E9C2EF7D5BE830B985889D721CDB,
	BOFRecord_get_Build_mDFCD87A5F0F85D145631D666CFBB72A1BBA31269,
	BOFRecord_set_Build_m012B3D526A36C24FB1340DA63FDB3C013369E278,
	BOFRecord_get_BuildYear_m04E739CD78C516102E1C01DDB3D91D97D1496BF4,
	BOFRecord_set_BuildYear_mC4DBF9D3BAD80DC8C7A2BC99C422D9094FD9C2E9,
	BOFRecord_ToString_m712544A602A66D6460FA5FABF732BD28561B3FA3,
	BOFRecord_Serialize_mBFA380558E921BFBB864679FE0877D5B6EFEB364,
	BOFRecord_get_DataSize_mE346A37F67D14B3C4C2DC45A31E28F97C9720C54,
	BOFRecord_get_Sid_mAC962195037C12E80DC3260E40E6A7C184506F4B,
	BOFRecord_Clone_m5174590C1C390FF812171B625C0075B60EC49371,
	BookBoolRecord__ctor_m0764C28450E135B952B670B652818D8ED1C1928C,
	BookBoolRecord_get_SaveLinkValues_m43EA7A93D99DDFAB67EA8738A0467A722422524F,
	BookBoolRecord_set_SaveLinkValues_mA5AAA76293ECF343F0EA0BBEB9596F50E6E90434,
	BookBoolRecord_ToString_m239435F13779504E773A9C67313A0BB844C0644D,
	BookBoolRecord_Serialize_mF19C15635CAB01BB05846BCBD7BB3E5ACE2B8BCB,
	BookBoolRecord_get_DataSize_m2B1D74F92B5B2BB6473BF9D0744B47CB775470CF,
	BookBoolRecord_get_Sid_m914CA8D15F34A6E469DFDB06041BD9FF7E245794,
	CellRecord__ctor_m8C991268792B29DCBEDA5FCF8900D34DC397D5B4,
	CellRecord_get_Row_m7DCA253871EA235DCD1C06234634C895F8D52155,
	CellRecord_set_Row_mA32BC49742240FF58C7279BCBB903B33BF364503,
	CellRecord_get_Column_m87EB8724FCC7E346B7D84C3810BC6C6F775278C9,
	CellRecord_set_Column_m5A2BF9213747C07519F1144D2F64921CD3A69E36,
	CellRecord_get_XFIndex_mE32C7C444FE2C857F634B5E3144DF70160E2BEE7,
	CellRecord_set_XFIndex_m8323E6461C004832E1C3B855DE5FC675CD37B16D,
	CellRecord_CompareTo_m43C7BE5A165CC80AAEB6741260B301438DF6EF01,
	CellRecord_ToString_m12B64CA72167B58DF8F23BBD6D5910E30EFC7495,
	NULL,
	NULL,
	NULL,
	NULL,
	CellRecord_Serialize_mCC3D7826924D9F63BBF22E2AE5E34CCD27925514,
	CellRecord_get_DataSize_mA44A3521D0AEAC94EE07A12F1B2DBD6ED38DFCE3,
	CellRecord_CopyBaseFields_m9687C55AC1E173C6BB8F41AA392D61A802E42D5C,
	CellRecord_Equals_m77E15913EC33CD06655F1AFAE389C7E5F70319FB,
	CellRecord_GetHashCode_mEE5AC1DB7C9A239DC0FA528FDE845A5DCC8388E4,
	BoolErrRecord__ctor_m2708A67D20122551E49CBCF1085E5BE7D289F429,
	BoolErrRecord_SetValue_m5C980A3F3660DEF1E60C1ABF535D9C3DDC45ACFB,
	BoolErrRecord_SetValue_mE074F541F3E39B153C5532171276B5FFCF56EC6C,
	BoolErrRecord_get_BooleanValue_m5063548A5632D9D9E0E3696C15181D38F942AC88,
	BoolErrRecord_get_ErrorValue_m6E3BFEC17BC2E69E587FF009CC3564EB2102A1BD,
	BoolErrRecord_get_IsBoolean_m84580A8B0001A7850543E172B5C48EB6B445EDB3,
	BoolErrRecord_get_RecordName_mB1B12870D7AE500A472195EF59147F45AC42CB90,
	BoolErrRecord_AppendValueText_mB0F36E3982E1B7A5545F44F3B12C647786A41410,
	BoolErrRecord_SerializeValue_m7B8C8FCA16A764900F1F43EB20EB5A7B9CA3AF0B,
	BoolErrRecord_get_ValueDataSize_m6520157E15C77A7F163A562BE8654491F3F21B04,
	BoolErrRecord_get_Sid_mA844C81C406AE5D894C515271E39796D3AA0C57A,
	BoolErrRecord_Clone_m5B417E025AF1B56ADB91E17F607140021A091692,
	BottomMarginRecord_Serialize_m910B0F5E98171112D63581778AE705A26841555C,
	BottomMarginRecord_get_DataSize_mC05D22C10F075BBBCBC81ADFEAF1B3C11EBCBB27,
	BottomMarginRecord_get_Sid_mFC7814B561263E049EE84A938D8CFAC909B7B8E4,
	BoundSheetRecord__ctor_m653B89501204A16618F813ADA3D2BD88743D439A,
	BoundSheetRecord_get_PositionOfBof_mCEE1DF4347454AE590DF3E0DF0C0F42FFD286C73,
	BoundSheetRecord_set_PositionOfBof_m6DDD4966C7C763D2EA63A79A06DBEACB20A218E9,
	BoundSheetRecord_get_Sheetname_m8E52BE9A21EDAF2FEC81BF5AB2B2623EF0C7A767,
	BoundSheetRecord_set_Sheetname_m696A7C93E443168ABDF676E3E347746E0DD791A0,
	BoundSheetRecord_get_IsMultibyte_mFE11E2CE5A25F27A321E2580683C1987048B4176,
	BoundSheetRecord_ToString_m63DB42B1950ECBB6EE2AAF1E99120D0C964138AA,
	BoundSheetRecord_get_DataSize_m5974530D02066BC8760AF549ECE1C8B2201B5E09,
	BoundSheetRecord_Serialize_mAF05F517CA9F3A4E234B76E1D7793DDBD404B001,
	BoundSheetRecord_get_Sid_m2F9903E6902C969C5D90834B02784B46C7570E51,
	BoundSheetRecord__cctor_m4B4F13BCEBA38A8A7D66E226348FB1663A0CCA82,
	CalcCountRecord__ctor_m78C6FB4504B70C37197DB7BCDEE0DE2AA4DC762B,
	CalcCountRecord_get_Iterations_mD26327DBE1081A331833ACC67094EF9885084688,
	CalcCountRecord_set_Iterations_m4461B6D7C862AB67E03A3CABB39344C7DAADAE4B,
	CalcCountRecord_ToString_m90C0E4C5771EB2B30237BD8440B074FEAE489272,
	CalcCountRecord_Serialize_mAD02BD1F9F6159D5052ABCED03FBDBA51FA374D1,
	CalcCountRecord_get_DataSize_mBE42BF2B98D6678E5FD6DCF6C48D7A426CF17902,
	CalcCountRecord_get_Sid_m9DE8A940DEE2BD233E360F102C4C1FF293146D6D,
	CalcCountRecord_Clone_m3DEB0F8EF5CBA8E0D60541370D50FC64392A0383,
	CalcModeRecord__ctor_m5DD9BF57300E1A53966BE5F6BF5CD42A6ADA60BA,
	CalcModeRecord_SetCalcMode_m848896B5D402B9582F44F92E176F4957D61860E1,
	CalcModeRecord_GetCalcMode_m559787B5B123A08AA3EF277969F955B9409CCEFD,
	CalcModeRecord_ToString_mE6639A76E3F4962E550573985F1EA6B0F9194CE4,
	CalcModeRecord_Serialize_mD9500B523BCA152512E2D2D8761C2AD8E2E32131,
	CalcModeRecord_get_DataSize_mDA3F6465CF390831DB27A66B691038F272EB8036,
	CalcModeRecord_get_Sid_mF6E1D1EF9BD3319497AFF71565942E28E18A8BD9,
	CalcModeRecord_Clone_m241DF179E860669F8BF703B1FBE7D0FF0935B7B1,
	CFHeaderRecord_get_NumberOfConditionalFormats_m52C0C53B8033D9DC4A2ABE3BD334E47995E2EB2C,
	CFHeaderRecord_set_NumberOfConditionalFormats_mF00ACEA4DAA21EAC539DBBB4FCF450161E2AE49C,
	CFHeaderRecord_get_DataSize_m10EEB66527F6D19240BCF53E6F1D27E8F8CE8A0A,
	CFHeaderRecord_Serialize_m5CBF83445594314268D8042F97975F72A6E77CFA,
	CFHeaderRecord_get_Sid_m31309EBA41B3B334E116B726451A20F7CE0C2988,
	CFRuleRecord_bf_m2195DED64814F0725B546E463D109505EED2CF25,
	CFRuleRecord_get_ContainsFontFormattingBlock_mB5BBB760E00FA05C2B135638B307C569B84FAE38,
	CFRuleRecord_get_ContainsBorderFormattingBlock_m13B3E486D15FDD4FB0216BB690BFD624337BD592,
	CFRuleRecord_get_ContainsPatternFormattingBlock_mEFF9D4053245509E71C213011A6B8F500F7DBB08,
	CFRuleRecord_GetOptionFlag_m73429DC8C54024B82EC2C1B3D8EA42A4BFF697C8,
	CFRuleRecord_get_Sid_mF5550C8FFCC632CEA66A111E52A0C9A4FB7F0FE0,
	CFRuleRecord_GetFormulaSize_mCB7B7FAA363C19A4040F51DA1E2AF5D838B97F9D,
	CFRuleRecord_Serialize_mE317818A9DB4EA3BFE3771B3A2DD32F4F424D302,
	CFRuleRecord_get_DataSize_m901FB5CE6AB827E6CC1A4F1ADFFEEE45F91975F6,
	CFRuleRecord__cctor_m3B2F684EAE1D5D66D01DF9B81DE44C10872E616C,
	BorderFormatting_Serialize_m7FF7C52620C767CDBBC06C1A0AEA46CD0895F178,
	BorderFormatting__cctor_mD9081A5CB50E3B2C9755503F4CBBA9EC09C038EE,
	FontFormatting_GetRawRecord_m7D85C819AD2346741ED1936B3B2268C433C50536,
	FontFormatting__cctor_m207722B9B3314F124C877D240B9F5DB6ABD70A95,
	PatternFormatting_Serialize_mB921A1CC93764EBCD94C2467C4232FB588A10190,
	PatternFormatting__cctor_mF0A14A3188996E3778237DAA71791274BF38692D,
	AreaFormatRecord_Serialize_m2DA17FB5236EB68C5D74698629EFC66088EC579E,
	AreaFormatRecord_get_DataSize_mF898E10353694CB2741C4F02E9CA32E48F824CA2,
	AreaFormatRecord_get_Sid_mC9591E15E1564769169995AE719C176EDFFA495A,
	AreaRecord_Serialize_mE9F16F31DCD7D0768E6B9CB1866CA92C3C4C256E,
	AreaRecord_get_DataSize_m0A4D181F7A1BE2CE2F09E5A09DB67515400ABD66,
	AreaRecord_get_Sid_m48530A82FA9A208815819EBE23529550E755AC3E,
	AxisParentRecord_Serialize_mAD8D449370A398D50E7A113C2786E7CD8E46F44B,
	AxisParentRecord_get_DataSize_mEBAB4FA463F17282ABE99FFB5AD4BB4F2FA120FF,
	AxisParentRecord_get_Sid_mBBC51138331974B9EE04871BF613D920C81673BB,
	AxisRecord_Serialize_mB9CA5052A143FFACE94BDCF80095E80E74A97ED1,
	AxisRecord_get_DataSize_m370BFAE0769DD79F77ADF0420C9067ABADC100AD,
	AxisRecord_get_Sid_m23333D6A907C0E1741FA7B57959732D7E56DF6A4,
	BarRecord_Serialize_mA4F9C054787CEF35F62A9F9B47784C2630F2AFBC,
	BarRecord_get_DataSize_m0F4DBDC0CE4C1555820900AC404CA04C3BC80F6D,
	BarRecord_get_Sid_m8B947528FF405C820C1628BD75E5624071AE4253,
	BeginRecord__ctor_m479DB3665602805FC5B7E92D69EA47FDA32EC9BD,
	BeginRecord_ToString_m8E92CACCF7C61FA2233B42358A287EF1A2E13CD1,
	BeginRecord_Serialize_mFACC74F6D79663E33A88F311DF0F4FD714B08796,
	BeginRecord_get_DataSize_m29BA06405C86CF8C0149CA521283B8CD45950522,
	BeginRecord_get_Sid_mBBE87AE98C11D13B7FA44BCE088057ECC515B502,
	BeginRecord_Clone_mCB5A27C12B5FDA4319DC06D271B6145375E84307,
	BeginRecord__cctor_mA5CC23B6FFE7208797B4D7D030252756962E6863,
	CatLabRecord_get_DataSize_mCFA431DA21092E47709706399D149D121FE5AEEF,
	CatLabRecord_get_Sid_m70DDC32B1E539181A82CEEC5C1B6B8C237A4C4FC,
	CatLabRecord_Serialize_m625DCE8078EFFF4033DC3DD020C10A5B0AC9491A,
	ChartEndObjectRecord_get_DataSize_m6BD6B223A4675867137905C722D5F14654B3345F,
	ChartEndObjectRecord_get_Sid_m2E60798900C9339C1CD767DB89264218ACCD0222,
	ChartEndObjectRecord_Serialize_m6AB217B1530CF541D37474EAD03ADA534BBA0493,
	ChartFormatRecord_Serialize_m00FB610F1645B4159CD9A74C26DCF6B3DB00AA31,
	ChartFormatRecord_get_DataSize_m63264F91A87E104DE791817E90B4C3166CE4DD58,
	ChartFormatRecord_get_Sid_mBCD15EE5C7B1B16AC1D86AC682DA1E2103ECF4A2,
	ChartFormatRecord_get_XPosition_m28C1CA5B1CDA2E158E54FCB62DCF4F7F2B308E1E,
	ChartFormatRecord_get_YPosition_m784F9496C6A436DF9D8F4C1EFB7FF4F6A44ED3E7,
	ChartFormatRecord_get_Width_m3A11BC87E943531E2F5DC21F84B5AE8CC3DF5CF4,
	ChartFormatRecord_get_Height_mC5096C4547F1AAAA28BFB51306F7F9A594A92FF8,
	ChartFRTInfoRecord_get_DataSize_m639384421D0C8483B35B5E27F225586001BF2113,
	ChartFRTInfoRecord_get_Sid_m5CFBA19FEC86629F89FCF26012F0A8F7798808E9,
	ChartFRTInfoRecord_Serialize_m95D1EC48A55E9C4407CFEE9977E298F894A7BDD6,
	CFRTID_Serialize_m8AB7EA7C636B30A3C637AB93F629A0AD83D76EFC,
	ChartRecord_Serialize_m27257DA75F014BD6BDF124E0DD15948C33CF7A13,
	ChartRecord_get_DataSize_m398CCB879A71F52A24E7567FE751723A51F29B8E,
	ChartRecord_get_Sid_mBDC2C067B4FA360069805C0E8E1B89798081305C,
	ChartStartObjectRecord_get_DataSize_m58ED76E776622C9959527D2FDB3311F0BFDD11D9,
	ChartStartObjectRecord_get_Sid_mF8BF0404772A9E76D69D1B8244C80E1C204B82DC,
	ChartStartObjectRecord_Serialize_mDC6EC1BA7322516ECBCC8D8C69771ED94ADBB824,
	CrtLinkRecord_get_DataSize_m5C65C3B1C8019AB596B361C9DFA917A8135FE333,
	CrtLinkRecord_Serialize_m6272106C5D30F110659040A27CD474736E446676,
	CrtLinkRecord_get_Sid_mF362D48A854BDAABCB1CB8321B4D09CEE0A32791,
	DataFormatRecord_Serialize_m71A73C959B11C52E7173269844670CD2222427D7,
	DataFormatRecord_get_DataSize_m6213A365CF5EB6314FD5E34053C4B392C4695BA3,
	DataFormatRecord_get_Sid_mB50C95E7638BDD0414BD5B4EE672611C3E7B4ED2,
	DatRecord_Serialize_m7D41CF5572F7B1D2199D0BAAAC82F16A5C90EE23,
	DatRecord_get_DataSize_m6CC92D77FD9F4A89F562F6C50436BD7E213CDE55,
	DatRecord_get_Sid_m3BB6E82D7AF9F11F131CA8CD1372922C4A602A9F,
	EndRecord__ctor_m16DB750F1C363543F5A3B892329AAE79D699D361,
	EndRecord_ToString_m5389D156DC61908D6A19CBF96A433381968D0373,
	EndRecord_Serialize_mEFE9D1EC17E7184EE8096EDA633DB82DD162CC2B,
	EndRecord_get_DataSize_m496EC9E608512EF0034C1504BF58C82EF11CB3D9,
	EndRecord_get_Sid_m5CF07A607503860199C9CF75FD05FB86CE58B28D,
	EndRecord_Clone_m9490D30FE1E400BABEE1143BFFBDEBE61FCF3BDC,
	EndRecord__cctor_m9A814F5E3BB6441A1148E953D563896E4C7B085E,
	FrameRecord_Serialize_m9D4D24E33D117DA856B317BECE72D459D3EADE38,
	FrameRecord_get_DataSize_mC391BAFC9F148762FD875B340E775BBB0E15F09E,
	FrameRecord_get_Sid_m515266862ED6A3D16945A8C1E40A6AA942E26630,
	LegendRecord_Serialize_mDDA8AADB6CA9FAE66B18ABACCD917F64A7B4E2FC,
	LegendRecord_get_DataSize_mFAA1EB8C47EC8961F300267051474FD6B0FF8AE0,
	LegendRecord_get_Sid_mA87A8E29975E7ECD9CEAEAA11BDACD0D935271E5,
	LineFormatRecord_Serialize_m7165A7171515C1A3FB60725D7353B079C5B66BB3,
	LineFormatRecord_get_DataSize_mD6D3C32288DC51B060CCF607788B74619930EB16,
	LineFormatRecord_get_Sid_m4368DCEA704227E0F4FD019A8A159F0380289275,
	ObjectLinkRecord_Serialize_mB118F2DE6B6FEF9842A3E7B0D7BC529C2CB031B2,
	ObjectLinkRecord_get_DataSize_m68B6E2D0129FD40D07B676DD3F99E65787328A52,
	ObjectLinkRecord_get_Sid_m2992E49BAFA1033C49C2119F2B78C89497848FDE,
	PlotAreaRecord_Serialize_m53568210AEC99025B65E5C2D9E288AF3188804B6,
	PlotAreaRecord_get_DataSize_m4CD5D0C00AFB030D8948B316785D294092FFAE83,
	PlotAreaRecord_get_Sid_mDE6D81218C56DE3CB55C96ADF9238D708EAEAC3A,
	PlotGrowthRecord_Serialize_m4769D809D43B9C440397ECBF59D09913D59DC8B6,
	PlotGrowthRecord_get_DataSize_m72B89381926EDE9E1C517D29E35DA01C566981B7,
	PlotGrowthRecord_get_Sid_mA023A85EC0E415FF080D1FC6EE2668D12D4C8071,
	PosRecord_get_DataSize_mD1022F3F9A982AA039772FCCCC7485A638F445C7,
	PosRecord_Serialize_m95CE4202F64107AB3E8D634C914416A9891626D9,
	PosRecord_get_Sid_m1E73FA0248C34D9A1315F7EA46B4BA876DC5A199,
	SeriesIndexRecord_Serialize_mA524752857140E8AD8398E0C5B0A79F36CC276A2,
	SeriesIndexRecord_get_DataSize_m6090A6AA9E32E132ED2271E5029BD031DB8B72F3,
	SeriesIndexRecord_get_Sid_m60419460BD87D73B5557A4A02E1BE7ED8858B92F,
	SeriesListRecord_Serialize_mF935FF7E23F8C1872FEC4055623845F8A566BF2D,
	SeriesListRecord_get_DataSize_m1169203800FF61332E95391BB69579F8FFCEBAD9,
	SeriesListRecord_get_Sid_m14547E7C3249153613B5FDF8ABC90B7D2E57DEEF,
	SeriesRecord_Serialize_m7BA0AA547C1CFA260EB63AEA0AD9F34793C8D010,
	SeriesRecord_get_DataSize_m65B865BFD971786084D21747BD60CC87BC200DEC,
	SeriesRecord_get_Sid_m19EEB104E33EC60E00C7465458B0E2A19DC0E6F5,
	SeriesTextRecord_Serialize_m512ADF17D2694FAD5432C7BF6738991CF91A0CD2,
	SeriesTextRecord_get_DataSize_mD9979FAB0E19CBE2C55ED41869DC0839F99805CF,
	SeriesTextRecord_get_Sid_m3F628CE47DEA6AFC86842D8BE4CE958BD2F2666C,
	TextRecord_Serialize_m47882C6CD87BAF72630A6974DBFE59B263C6734B,
	TextRecord_get_DataSize_m05C3CCEC8735F0B1C5AD2E8FC4582345824F48F3,
	TextRecord_get_Sid_mCD14A2737DBD0716C7C31F16A61F5C1896C5F66C,
	TickRecord_Serialize_m8984967C3514E96A13D1C824AA9ABA720819F0FF,
	TickRecord_get_DataSize_m812019C6C3D82F6608B60BC3A2CA78C5F61D55EC,
	TickRecord_get_Sid_m23528F7C2B283F74A261553AAC74F4DAE78ECCBC,
	UnitsRecord_Serialize_m921D7E7D9013D428B749BB2134CE17BBE55693DE,
	UnitsRecord_get_DataSize_m1ABD1F325FB9EF399186744A14F88CF8A7B2097B,
	UnitsRecord_get_Sid_m52F2630A585AF5D47918F6034177A5D608215C89,
	ValueRangeRecord_Serialize_m575A4F51699F64AAB33355CDF72F5427F70D9CE0,
	ValueRangeRecord_get_DataSize_mB0771FEE2A22528CE737228E3E614CD363CF041C,
	ValueRangeRecord_get_Sid_m98C13824D2CD3C3907DB9D908A65F89DCD234403,
	CodepageRecord__ctor_m1575B497F60F721F25685346E6F22D3FB71AA49B,
	CodepageRecord_get_Codepage_mBD535ED88C19E38066252547B165C49E0A55136A,
	CodepageRecord_set_Codepage_mB0F9B4F797D5E0F66AD99141F9819773936329C6,
	CodepageRecord_ToString_mCA8C15A1606D8DD68DDF306865E6EA5D31DC28A2,
	CodepageRecord_Serialize_m1C863C7DD30D604ED416CF7405E9D941C4D9CD47,
	CodepageRecord_get_DataSize_mAB6B32379FFF42A3B9B70EB10126354543B17F08,
	CodepageRecord_get_Sid_m1DF97C6BC01B05029F894534B587F9621A4937C6,
	ColumnInfoRecord_get_FirstColumn_m1BAFAB09DD3AB3C8A8B57B59A5144B744E78C84B,
	ColumnInfoRecord_get_LastColumn_m965173C7A2C44CF9F7588CB19F2DDD474198337D,
	ColumnInfoRecord_get_ColumnWidth_m78BFF73129BB3BA10ED8FA4885A108D8873485CA,
	ColumnInfoRecord_get_XFIndex_mDA4B9919937F0FAFBD9E829CEA5FDDC03E305A31,
	ColumnInfoRecord_get_Sid_m82E43E47AF488B70FA0C3A20863E96F365E6A365,
	ColumnInfoRecord_ContainsColumn_m5D0E86BF8868A447C3E4F625C9EB1B98950960C0,
	ColumnInfoRecord_get_DataSize_m9FB584AAE96AF3B30B157CC44210C86ECE9D9EA1,
	ColumnInfoRecord_Serialize_m8490A3162BFB03C5FA1E6795BA6F781F09EA64BB,
	ColumnInfoRecord__cctor_m486282E84DC8AAB11D50DDA758706C1CD9B25BD9,
	POIFSWriterEvent__ctor_m26DCC8C2342FA6BB2EA431DE616E1B6D1A4C098F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ChainLoopDetector__ctor_m11B00B3CFFDED002AF089E745AECD8B6B6A5DD8A,
	ChainLoopDetector_Claim_mCD49B10FECDA2CA18C0ABDA315DD063456255718,
	ByteArrayInputStream__ctor_m781A46CD2EE7EF10D9D7AF64C5AF382E43B23E06,
	ByteArrayInputStream_Read_mC1D8DBE30DE0ADC1F92619809D14B70B071351FC,
	ByteArrayInputStream_Available_mE83757F0936EB1628CC813D6FA87AC5439FA14CC,
	ByteArrayInputStream_MarkSupported_m8D80D4012DF40ACD0535E76226EB35826D3B3FCF,
	ByteArrayInputStream_Mark_m7BEFF39BA54F08B20F163D4E6AA9595A3096C25D,
	ByteArrayInputStream_Reset_m736B9A9237B69FFC46110A0A682EC371CD80B4A0,
	ByteArrayInputStream_Close_m67B612C1F73E27E8E03F0946AFCE5A324718B141,
	ByteArrayInputStream_get_CanRead_mBCB989183C004DF9847B535E80AA8D7D171CD1EB,
	ByteArrayInputStream_get_CanWrite_mF0905F1673F49D668644DD338E8FD22347CB9641,
	ByteArrayInputStream_get_CanSeek_m61376FE796A73BAE23838B4E16CEA21216D3CA87,
	ByteArrayInputStream_Flush_mF94FE387B40E1705973C4B3E7C10A63BC89248E2,
	ByteArrayInputStream_get_Length_m60A9C8DC9463FB7F060E78D2B312CA79D2C09917,
	ByteArrayInputStream_get_Position_m9728D9828E4B5E5C857DEBE0E923ACB3105464E5,
	ByteArrayInputStream_set_Position_m0092148347650B403E4224E6064E49ECF35B0B02,
	ByteArrayInputStream_Seek_m36EC1C9061EC85C1CCE7A7D7D55AE3CD11E8E62A,
	ByteArrayInputStream_Write_mB83130169E52EA258711DCC3D80601CC699A2331,
	DocumentInputStream__ctor_m581AFB79AC512FAB567BE6EAF7E2F06F441A385B,
	DocumentInputStream__ctor_m8516103ECB990B6AEE75F5C2E3DA55857108297C,
	DocumentInputStream_Seek_mB3B1295C11554C68859E82B1997EB37F8B9B957F,
	DocumentInputStream_get_Length_mFA7938BCA02BDB1001961A19F7EC4A515C11DB3A,
	DocumentInputStream_get_Position_m622A15DB862A3DCEB28DCADA100D88F0E97CFF36,
	DocumentInputStream_set_Position_m467ACB3B77940FBBCC8ED86B0AF8A0644E38DBB1,
	DocumentInputStream_Available_mF0A119D6E65E96876F61F9BCA2C2CBD93F68242F,
	DocumentInputStream_Close_m2A96E72D87D3E50220BCCFBD5CB687C9C3D26553,
	DocumentInputStream_Mark_mB6FEEC2BF386900D56A9DD2E80E16519925D694A,
	DocumentInputStream_MarkSupported_m27B7BCCBA3810F48C9539DB5CB0D505E3C606619,
	DocumentInputStream_Read_m869CE88788AE9C3BF1DF5981039AE274C9C79413,
	DocumentInputStream_Reset_mEE67E42FA7023AEB8CAF3774505DFD550F9E61AB,
	DocumentInputStream_ReadByte_mDE5DEF78109C29620C644085A9B9ED3D761639A7,
	DocumentInputStream_ReadDouble_m0C72A8B7C95B21D93FA3B490DB44B7E5FE75D7D5,
	DocumentInputStream_ReadShort_m28AF0BB6C7CABDB87B974FFA8B8CB0A6D1A2E7C8,
	DocumentInputStream_ReadFully_m1FE8460029D63012E8564284C15E31D59287FA4A,
	DocumentInputStream_ReadLong_mA7EA10E68CABF451F2F4B668E8D263741922691E,
	DocumentInputStream_ReadInt_m04504D79977195297342C1C896504C8A29A625C5,
	DocumentInputStream_ReadUShort_m891329AE8094A7E52EEFAE30DD5AD068E05B6E81,
	DocumentInputStream_ReadUByte_m8D23582251EDA54F2CBE49A578BE70754437CAC1,
	DocumentInputStream__cctor_mDE1AF6EF92D507F856A25CCBA2DE93D03C09B117,
	DocumentOutputStream__ctor_mC9B17E9D90F564C809B326B4FAB6BEB98C451A57,
	DocumentOutputStream_Write_mBBBCB56CC1BE751979C076E87D0637ECC7A2D4E3,
	DocumentOutputStream_Flush_m4616FA99D7F6910F3D7211A2B8C40915D71FCF42,
	DocumentOutputStream_Close_m554845D1C103C72922BAD41B87D0BF3809ED96EE,
	DocumentOutputStream_WriteFiller_m01DD916CFE05ECEB8CCF9F6D4ED16BB7CBE5102A,
	DocumentOutputStream_LimitCheck_m4C2C878BD8069D22E6E58273E89B7AF118FF215F,
	NDocumentInputStream__ctor_mD930B91F94BA6B0931FF45CC275A138D9092DF4A,
	NDocumentInputStream_Available_m466149E75B81061AFABB7DC74B8592FA61D87DA2,
	NDocumentInputStream_Close_m9C65EE804B43521EC3E1B410E167457A8B41DFCC,
	NDocumentInputStream_Mark_mB0E7B5D30C579BAC7ACBA48CC9DF720571F603ED,
	NDocumentInputStream_Read_m34266C2AD96B546FBDA8D3BC45B7269DBC06D1F0,
	NDocumentInputStream_Reset_mFBE7679F7B7472F9D9A9EA84B3E10742A66F2652,
	NDocumentInputStream_DieIfClosed_mA9E001A28EC2F1DF44C0DEDCCA45456EFCE4E42E,
	NDocumentInputStream_atEOD_m83D52AE747A3A50CB87975CE68E17B68DAF99E09,
	NDocumentInputStream_CheckAvaliable_m4733CC6CBE7DDF2B13D8FFD8E720F8888CF18016,
	NDocumentInputStream_ReadFully_m0A45E4B04CE467527836B2BE8F83981EB504090A,
	NDocumentInputStream_ReadByte_m40378B371B5AF9FA3120DB5A2A9B3166C96AC136,
	NDocumentInputStream_ReadDouble_mC74D301C95AF3A338FD9757FE80980E46DCE0079,
	NDocumentInputStream_ReadLong_m4E9B101CF2C06174A6BF2226F4D1A8A06FBD7594,
	NDocumentInputStream_ReadShort_mBA9BA3C9EF3D62320779BA235D975A2DC4BB6D62,
	NDocumentInputStream_ReadInt_mBFC23B0CCB1505FCF5EE20A4FC8569B6CED122C3,
	NDocumentInputStream_ReadUShort_mFCC0D76A454E150A4F1D971100A7253404FC2F66,
	NDocumentInputStream_ReadUByte_m9C6467313382CA9BFFD0957A3205BF5627A22856,
	NDocumentInputStream_get_Length_mAAE7911E61AE8F62476492F1EDE57FAB8DD26778,
	NDocumentInputStream_get_Position_mEC21710C2C086CF4D7116902DD3FF65934183B9F,
	NDocumentInputStream_set_Position_mBAE2A5DCA8605E8079B35030A0E5BC4DA0729F3B,
	NDocumentInputStream_Seek_mD38A2453A10E3741A6312B1D17FF6493D7A5D3BC,
	NPOIFSDocument__ctor_mC4CDF25553CA767DAD2F4FDD419886A187247415,
	NPOIFSDocument__ctor_mF12A8D0D91CFAF87B39DB71121738A6B6BC88065,
	NPOIFSDocument_GetBlockIterator_m020D9CFB482D3B92B8CF24BC295E338116038658,
	NPOIFSDocument_get_Size_mFC6E8DA64ABD3E773AB63E10A43957870FA31AEF,
	NPOIFSDocument_get_DocumentProperty_m4B3A6DAA7B928783B9E62F8B6A4195A7D06EDB20,
	NPOIFSFileSystem_CreateBAT_mEA951D91FEB9350347A6A9C499FD20C4CDCD37A6,
	NPOIFSFileSystem_GetBlockAt_m906D632FC22EE42218DEBCADEF84FFAE71C67AF9,
	NPOIFSFileSystem_CreateBlockIfNeeded_m14ADB99F37D0A6C4D53F0F91913348BE309199EF,
	NPOIFSFileSystem_GetBATBlockAndIndex_mBB626F7760054F8D6109FF92FCD01D1E5FBF205B,
	NPOIFSFileSystem_GetNextBlock_mC45736455D02E5F31298CE71FC66603B63BBB9E3,
	NPOIFSFileSystem_SetNextBlock_m1B36283D62438382D2B8E778A27BB7E10FDE15E1,
	NPOIFSFileSystem_GetFreeBlock_m2EF303EFF29B05E53F32CDA1A9FA7CF198399D7A,
	NPOIFSFileSystem_GetChainLoopDetector_mF4BF053127B83A7E3EF4CE35EACDC676E1CF7CA5,
	NPOIFSFileSystem_GetMiniStore_m1AC5A9705D8EA7AA26A6F44A283B2312CBD348A5,
	NPOIFSFileSystem_AddDocument_m8E4E0E4EE5F992EE0D0E2FE46F84C1D6B894460F,
	NPOIFSFileSystem_AddDirectory_m51E61F4B964F76E815A38F6965EBD69F0F807D2B,
	NPOIFSFileSystem_GetBigBlockSize_mD08447B635BB5893F3D81551C3BE7D8820422A48,
	NPOIFSFileSystem_GetBigBlockSizeDetails_mEBBFC89F674391478B6EFD543945382E60296ED9,
	NPOIFSFileSystem_GetBlockStoreBlockSize_mD7BDEE71E44A6D77D8396DC06D5C57D2BA7F15E5,
	NPOIFSFileSystem__cctor_mC5E7822A18E3CFAF34C743F985AABAFC300D7A9E,
	NPOIFSMiniStore_GetBlockAt_m9FEB3F785023B16A8FC35AC72940F73AF4F2522D,
	NPOIFSMiniStore_CreateBlockIfNeeded_m5A23A6F2A83CC8BF0C91A3F6B80C6BE5907298F4,
	NPOIFSMiniStore_GetBATBlockAndIndex_mC1BF8070E20A9D10990C4FB8FEA000B50CD6699E,
	NPOIFSMiniStore_GetNextBlock_m1D27DDA4B85CBF1BFD96F6A36D46AC63674D59F6,
	NPOIFSMiniStore_SetNextBlock_m458C294C5E4BC2419944FF30A4A45D8FE9D9F360,
	NPOIFSMiniStore_GetFreeBlock_m2F830BD5A4BCC3730A5B20087980DFAD4D40863D,
	NPOIFSMiniStore_GetChainLoopDetector_mF05A281B62FAA9E1D510C2D21FABC6FEF3A27FA5,
	NPOIFSMiniStore_GetBlockStoreBlockSize_m9DA270C1C8A2E1B8F4DE7F7E1CA4E6DC63AC887C,
	NPOIFSStream__ctor_m6203A4E216389C4E68B63508A66040BE898BEE2C,
	NPOIFSStream__ctor_mEA077DE2F07571DAF08199360CAD21D73C170BCE,
	NPOIFSStream_GetStartBlock_m65B7C7316CB80AF643C75CB9A1C83FC98B6254E9,
	NPOIFSStream_GetEnumerator_mF64A3E18CF2778875FC41513B8C7CE1319A2132D,
	NPOIFSStream_System_Collections_IEnumerable_GetEnumerator_m9D8717BFC3E8139C92489D47F2D95FDCBDF5AB23,
	NPOIFSStream_GetBlockIterator_m2AA6A61485AE2C80919F5153C4A17E3DED2FB1D9,
	NPOIFSStream_UpdateContents_m49EA6FB1EA93DF22AB0D82C32EF1EB287831E4A2,
	NPOIFSStream_free_m8FF35A9917C2623DF36195C34F943823BB892625,
	StreamBlockByteBufferIterator__ctor_m3E063ADCE634B149E2FFA12D82E27E09E3E5ECC7,
	StreamBlockByteBufferIterator_Next_m360AFDCE367D117657F30CC1A4F2594609DBA9D7,
	StreamBlockByteBufferIterator_get_Current_m6CDA5AD1019A0E4F6882EC7CFED819A0ECF36615,
	StreamBlockByteBufferIterator_System_Collections_IEnumerator_get_Current_mB1CC8494AA1B1B7F446A9C0B9166CA01913A5953,
	StreamBlockByteBufferIterator_System_Collections_IEnumerator_Reset_mB74AB3BF4A107C521B2AEDCE6E9351FC7FCC50BB,
	StreamBlockByteBufferIterator_System_Collections_IEnumerator_MoveNext_m4EEFF6093A0D9879B2D3778FFE0EED68380349BC,
	StreamBlockByteBufferIterator_Dispose_mF0A7AC010DC06D6DBFDD85E7CB3961AE2F9B8051,
	ODocumentInputStream__ctor_m7F8EE603D2B86F3CB365D8BEFD117D320E13EDD1,
	ODocumentInputStream_get_Length_m7AFFF6904FA4FE13CF9D0CB0C2FC18DD5B802621,
	ODocumentInputStream_Available_m28746DFC50345A3078D8F253C9D0E68E5E903AC5,
	ODocumentInputStream_Close_m0D336A1D7E53AAAB4988C5E7EE1EFEE4719770E6,
	ODocumentInputStream_Mark_m214714CA8673FA68E4FDCBD2E1AFA9A0A10FD3C5,
	ODocumentInputStream_GetDataInputBlock_mE74FDCC7FA25673ACC0813AAB68556F12762EF8D,
	ODocumentInputStream_Read_m2A813C4BE6D8B749FAFC71F3DC0177C8E21C8875,
	ODocumentInputStream_Reset_mA313C42CB01A417CF266F8FDDD61CC796683BA43,
	ODocumentInputStream_dieIfClosed_mEA3A5E18DA073ADF90130C4F541718C412327485,
	ODocumentInputStream_atEOD_mAC3ACB85D779796DDC3FC5DB75D2F8244CA5E59E,
	ODocumentInputStream_CheckAvaliable_m09B3558D162BDE0CCC3E003132459BD43D45E8E0,
	ODocumentInputStream_ReadByte_m8D78A79D9BDA6618D526F5CCE3D02EF5EA6F8391,
	ODocumentInputStream_ReadDouble_m708DBA4648ACA8DF67F75532A237F0BB2A0F458E,
	ODocumentInputStream_ReadShort_mE36C3EDFED6167357EB9583433571714649D9B78,
	ODocumentInputStream_ReadFully_m992306317AEC211BDBA3C8C177BA686CE89EE37C,
	ODocumentInputStream_ReadLong_m84184D21A992A9C8EC82E5B45B50BF6F38869655,
	ODocumentInputStream_ReadInt_m6F55983AA39E97001718A085D7A122C530300C57,
	ODocumentInputStream_ReadUShort_mC292175597CC831EDAC60204D9CC1DB088D789A0,
	ODocumentInputStream_ReadUByte_m4836A1929EC1C61EC2434FBA0EDBCCA50B225714,
	ODocumentInputStream_Seek_m87539D94588EFD19FF34F0B82724F2C5BBD4EC45,
	ODocumentInputStream_get_Position_mC4F4410A79302D2373AF54124B1C863D1C2D0F21,
	ODocumentInputStream_set_Position_m906F4804F4C01D538CD3686EC9111B081E075E6D,
	NULL,
	NULL,
	NULL,
	POIFSDocument_ConvertRawBlocksToBigBlocks_mC96C8293971B6C7A04BFE6257ABCA8383F75B5D8,
	POIFSDocument_ConvertRawBlocksToSmallBlocks_m80BA5B84A9AB102EC826BAA21A739EB102C704C5,
	POIFSDocument__ctor_m4EA2E513F08971EC3A497BE6D96C7EF7AFE6FA39,
	POIFSDocument__ctor_mC5747A121BDA029F82D356030493D97D018E55B0,
	POIFSDocument__ctor_mD1BC63A2BA2D3E6E8378EB1350CBA2C472825CBE,
	POIFSDocument__ctor_m59968A98E86D3775899B54293857CE3801272491,
	POIFSDocument_WriteBlocks_m43AF2E33252322BCFAB3C5BD182EF4F842F4F785,
	POIFSDocument_GetDataInputBlock_mFEA18D83D23EB395A297B534E03D91CB76A06F46,
	POIFSDocument_get_CountBlocks_m5650544605E7F120C5A95798C4C0292A970493FD,
	POIFSDocument_get_DocumentProperty_m0F91BA9737F75966EEB2BC5B9AA64C41FFFC73AD,
	POIFSDocument_get_SmallBlocks_mFF12629B1D25DFB977E5AC2CC7DE663DDA6EFB3A,
	POIFSDocument_set_StartBlock_mB06CAA221CCF8198BF85C84A57354519379B967C,
	POIFSDocument__cctor_m93D43A0A29FE63B10126B15EA2CEEC7EDE9B91C7,
	SmallBlockStore__ctor_mD6787E079DCC55ED46F0E79974D51C7AE9AE39BD,
	SmallBlockStore_get_Blocks_mA07A9D91B3FC2408F966BFFD75CB8BFD8481F37A,
	SmallBlockStore_get_Valid_m746A122B4186121E122D8A966721952947C16ACC,
	BigBlockStore__ctor_m3F361AA1BE0562B3E6FBD8A6666772ADDA0D9ED4,
	BigBlockStore_get_Valid_m304787C131816D773515546D679219D5E574DCBD,
	BigBlockStore_get_Blocks_m0C8A848C28751BB4B77569F6C064A73DE9DA2DA3,
	BigBlockStore_WriteBlocks_m01B944F8E44DA91345577D47EE0E4493B9880531,
	BigBlockStore_get_CountBlocks_m8C4E756AF92BEC30A932AA2A745FA0ACF2520EE8,
	NULL,
	NULL,
	NULL,
	PropertyTableBase__ctor_m3FF29D3A2A56699A2A690CDFD67D0ADCD290E7BA,
	PropertyTableBase__ctor_m9562FEB6F7624D6105367FCDC46B212E768CBC86,
	PropertyTableBase_AddProperty_m1C88A6F3EE4A2852694635552A0B12D05557CFA7,
	PropertyTableBase_get_Root_m1C717F2FA191A8FBA520F22094C10DAB8FF05835,
	PropertyTableBase_PopulatePropertyTree_m3EBEB2DE28227538C8D9C43E0D06A2480C1C22E1,
	PropertyTableBase_get_StartBlock_mA8DCBCC7C4855B369709DAEC91FCD3588443F1C6,
	PropertyTableBase_set_StartBlock_m2564B407A052502F18A1CEC381AAAA2BB6F534DE,
	PropertyTableBase_get_CountBlocks_m3128A5634D616322B40337B36B7AF7C20F06890E,
	HeaderBlockConstants__ctor_m5ECDDFA5506029EE654219E8D68049B8FE4C8209,
	HeaderBlock__ctor_m43A67C965840C82C0FC764572329712F5860BBF4,
	HeaderBlock_PrivateHeaderBlock_mA86CB1769AEE8C40004F9016369B8DAF7567506B,
	HeaderBlock__ctor_m81B08B00A1A4A91E3BB783C4341E8DB5D57139BF,
	HeaderBlock_ReadFirst512_m5ACEEAE70FF93EC536CFD7CFDF2AAB8B51A7EE16,
	HeaderBlock_LongToHex_m692CFF2DE3769E906BE748D21AF31EC82AF30D13,
	HeaderBlock_AlertShortRead_m38853947E94DEDA1578B4DE2D8A28E12D7A44170,
	HeaderBlock_get_PropertyStart_m944CD586A853916358C5E25248443E31E6A4FED4,
	HeaderBlock_set_PropertyStart_m9CEE42F905FB86C14A9D84C095E5E6BD68C94369,
	HeaderBlock_get_SBATStart_m27F4084017117007B24A217F95BCEA679C7B05AF,
	HeaderBlock_set_SBATStart_m7712BEBB016F39C72EFA48F67770938E9235EB2A,
	HeaderBlock_get_SBATCount_m6E9686FDD3D2AE5D2D4C4197D96E653D65E4F154,
	HeaderBlock_set_SBATBlockCount_m5E9FEEBFA2E7EF05AA79122A80B0B65C34B73A1E,
	HeaderBlock_get_BATCount_m07EC0BDA0D964ED344FD9E61391CFBB5641BD084,
	HeaderBlock_set_BATCount_m70A793FD88E4097A552DDE4D14BC1F9360D4D192,
	HeaderBlock_get_BATArray_mF8B72A22FDE5EDB26C2B4CE7104BAB1A19AB7688,
	HeaderBlock_set_BATArray_m8267969EBC57AD335905747C97F406D82734AC69,
	HeaderBlock_get_XBATCount_m54A170A09494C874CDAE488AE59D50BAAEC83219,
	HeaderBlock_set_XBATCount_m6AC26A5197EB9B846A71E750BC44E349119E7268,
	HeaderBlock_get_XBATIndex_m7A302D1726F9D44B7077FE38FDA4F43941D74B84,
	HeaderBlock_set_XBATStart_mD326A9A99D271253B8DF901D4A827D971FDC2011,
	HeaderBlock_get_BigBlockSize_m0FE57511C9E809B6D4DA3DDECA0499AFF152E223,
	HeaderBlock_WriteData_m8F94D4FDDCEAAD1D0E15BC6036FF6D19274CE7A7,
	HeaderBlock__cctor_m83876AED66CC563B21CCB772D464D7AF312889A3,
	EDate__ctor_mB728A6887153E24AA349CDB5D35617E8B1CCB0B1,
	EDate__cctor_m0D215CDEF96D307AA4EE8EC8ACFEB6F2B44B5647,
	ConstantValueParser_Parse_mF55A638A80B16327024CBC90FFA85D0220C03611,
	ConstantValueParser_ReadAConstantValue_mDD59FF89F3896C5EE734CCDBCC403848C09AD198,
	ConstantValueParser_ReadBoolean_m139E67FC0E16D9D434AC54F0FD8B00D2B68DA24D,
	ConstantValueParser_GetEncodedSize_mC9895521560E86A9451A9AD7D0E218012CEF4A63,
	ConstantValueParser_GetEncodedSize_m71F71406172D6194330563054C83100811ED3521,
	ConstantValueParser_Encode_m8A42AF6A12316F6B7D7909E8347E1514DEA8A884,
	ConstantValueParser_EncodeSingleValue_mC877F6067D955D2B24F8FF94CBF64108795079BD,
	ErrorConstant__ctor_m18398953EF90BAF2C911EF4B5CC4779D4A125CD0,
	ErrorConstant_get_ErrorCode_mB3361A6A76DF28E76F3C4391162EE68514ED67B6,
	ErrorConstant_get_Text_m091E4FCBA40253BB0D45AC66911144CDD7C66F9A,
	ErrorConstant_ValueOf_m42D6805E17A080EC1702CF4AB19242BD80C8990D,
	ErrorConstant_ToString_m0D5D6E5C32662AEF40D573D0837F15A6301EFADD,
	ErrorConstant__cctor_mAA810FF12A047800D899DE36147CDD6268621CBF,
	ContinueRecord_get_DataSize_m59B2975EC823281AA4E7DE55A70BFBA76B2D3ECF,
	ContinueRecord_Serialize_m62209FC4358C953A097F249C9BF2DA5F3F01F594,
	ContinueRecord_get_Data_m379FB669D7D88291D2168D838CCC17CF855218A2,
	ContinueRecord_get_Sid_m7E05B1C4FE6CEB4492425B6AB5013F468EDF2051,
	ContinuableRecord__ctor_mA00415E16B267D054266C00D47A6D3AD1405AB31,
	NULL,
	ContinuableRecord_get_RecordSize_mFDA76EFE371C6C900879180C47390777D6BA306B,
	ContinuableRecord_Serialize_m18B8B695E57E089EE923628FC9D8D3EF08A67F8E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ContinuableRecordOutput__ctor_mA97B3281697F60CB938B5A9E6A32FE09D8CED45C,
	ContinuableRecordOutput_CreateForCountingOnly_m6139DB524E9935B3AE7B74536D43B9683E11605B,
	ContinuableRecordOutput_get_TotalSize_mD3A8E710C5F8AD83250C063284A9D17F22757C3A,
	ContinuableRecordOutput_Terminate_m19D738E5DFB6B271400731C8D3420839F0973501,
	ContinuableRecordOutput_get_AvailableSpace_m86560DDDEAE62D08500AEC3DC7B621450D497600,
	ContinuableRecordOutput_WriteContinue_mF4DAE96762338EE1328BF167FFBB87A078666D96,
	ContinuableRecordOutput_WriteContinueIfRequired_mA5C94A1769394BF5A158D298390C751BC33E39C7,
	ContinuableRecordOutput_WriteStringData_mFF458433247A9D2C10A51C7BA54E4749D4FCAFE0,
	ContinuableRecordOutput_WriteString_m722522D735F62E2FBE526906A0450E66C1792E8D,
	ContinuableRecordOutput_WriteCharacterData_mBF9C576CFB857845D6D20D550F40A881D0D31B75,
	ContinuableRecordOutput_Write_m584D574C222D5ABAD25C58A47F40A12160626E29,
	ContinuableRecordOutput_Write_m5C5A39A45C5C8BC3074D9418A7956A5CC68A512E,
	ContinuableRecordOutput_WriteByte_mEE8CDE4B854B64161A294410A582CBA1B705374D,
	ContinuableRecordOutput_WriteDouble_mE922C1745F9C5095A4DBA3C6E35F04A21195530E,
	ContinuableRecordOutput_WriteInt_mEB0ACD4DBDA5B2B62D69CD7CE1897ED921BE0E49,
	ContinuableRecordOutput_WriteLong_m87EC7CDC4C944BE852B0647E60AAD18BC1AE19EC,
	ContinuableRecordOutput_WriteShort_mE1393EEB6DFD906953DC5A3E67763BABAAFCFBE1,
	ContinuableRecordOutput__cctor_mF4A642CF72A2F730406186F9422AE2A87AD008F3,
	DelayableLittleEndianOutput1_CreateDelayedOutput_m0C585EF87073C039EEF4129C7D314F60F98FFE95,
	DelayableLittleEndianOutput1_Write_m82D99A1C34ABA4773563D94FABE3689481951E6A,
	DelayableLittleEndianOutput1_Write_mE8217516251CB4846BBD32E7900456714D9A7561,
	DelayableLittleEndianOutput1_WriteByte_mA617208DE2FDA4EAEE9E35939FBE87AC18CDB428,
	DelayableLittleEndianOutput1_WriteDouble_mEEB3418ADE24B2ED4E045AC5861FD10C3520EAFB,
	DelayableLittleEndianOutput1_WriteInt_mB026D8EF70ADF6AA992AC2440A576AD4774F59B2,
	DelayableLittleEndianOutput1_WriteLong_mC77A160CE12EEB78D1F61AC2AC2C7A2C00EF0FA9,
	DelayableLittleEndianOutput1_WriteShort_mB8C1974CD7A658CBC66DAA1DF561CC8FC4D424D1,
	DelayableLittleEndianOutput1__ctor_m23C0B976C5F0C7A4488788E591F50181EEE39D27,
	NULL,
	UnknownLengthRecordOutput__ctor_m90059E77A9260AF43E040AB4BEE405F43D3D8566,
	UnknownLengthRecordOutput_get_TotalSize_mB47657A700D4A67DF31D37EBC5450DF070171169,
	UnknownLengthRecordOutput_get_AvailableSpace_m0DD6C13CBB633197EF122404969D16301A7228D7,
	UnknownLengthRecordOutput_Terminate_mFFF22B18914AE5B8586FF954386D90612A7BD9A2,
	UnknownLengthRecordOutput_Write_m728808C88787D379294D8B390982877489BA2238,
	UnknownLengthRecordOutput_Write_m84B6E37D8656FD8811B1B13E3C9E48332BC2D710,
	UnknownLengthRecordOutput_WriteByte_mBCB62F6D14466C1EEFCF25BF154A26216CF1D7FB,
	UnknownLengthRecordOutput_WriteDouble_mD0122D1C5A762E0AA45CD0A572B98D1A72E2A9BA,
	UnknownLengthRecordOutput_WriteInt_m51185957523E40116522EA90C4C0F7DFF54E0261,
	UnknownLengthRecordOutput_WriteLong_m455827770B5B8620ED46C7343EED29378CE7C6C2,
	UnknownLengthRecordOutput_WriteShort_mA76412480179FF853FCF7B04FB8F2C4DA19CEF2B,
	CountryRecord__ctor_mFDC3615D925A450C68EFE74210E019BBBA1A2C3B,
	CountryRecord_get_DefaultCountry_m2805267E5ED2DEBA5E5CEA6D4F84C13F582CDB87,
	CountryRecord_set_DefaultCountry_mF55FFDB314ADD7B070F1F470DBE24B0AB3E61D86,
	CountryRecord_get_CurrentCountry_m0EBDC6DB792C67B430F44E223100C1D36B40A4E3,
	CountryRecord_set_CurrentCountry_m52BF7A88917FE88881BA1EDBDFCBF6309AB14A44,
	CountryRecord_ToString_mE769CDDD0DCD26BCE9D6F7D904811E6074C45F9A,
	CountryRecord_Serialize_mF087C629D137A8963C24CD542EA9AEE6EC7DD56F,
	CountryRecord_get_DataSize_m86DC3A8449AD028E7B35CFE9B1D37347F7EAA82C,
	CountryRecord_get_Sid_mF7B2629686282771A7FB863F8D7C0711F26B7DF7,
	CRNCountRecord_get_NumberOfCRNs_m549277B63A8C9883174DC3A9AE6E40850562FD53,
	CRNCountRecord_Serialize_mC16900C789B6BA059D977F7B87934FE1ABA62B5E,
	CRNCountRecord_get_DataSize_m5E37FBB210AE773A65C0A7E4686AABBED9CA2529,
	CRNCountRecord_get_Sid_mC9E6D52B76B2C67CDD1312F023D33863078EC4D3,
	CRNRecord_get_DataSize_m4DD61C649B5EF60169AED2B526C17C7DF303224B,
	CRNRecord_Serialize_m50724060ABE236C6BE654377234CA7F2A02763A5,
	CRNRecord_get_Sid_mED7983413D6423520A4AE98F21E0693AD7701E41,
	Biff8DecryptingStream__ctor_m4BD586446360284C860BB78218A2DC24EB3C26D0,
	Biff8DecryptingStream_Available_m7CBD84BDE22555D32DA452909C7D71665809FFEC,
	Biff8DecryptingStream_ReadRecordSID_mEB6E54079E9AD71B4D288BDB3766F7F3872DAB1A,
	Biff8DecryptingStream_ReadDataSize_mB71A837C7474BF4407D96F06960B9A4E798C7CB0,
	Biff8DecryptingStream_ReadDouble_m2F365B7705EF1D7AB2A4DA9FB7074A09AC822447,
	Biff8DecryptingStream_ReadFully_m1C5CDA36182F3FD5DB2D1081C9841AEF675E5472,
	Biff8DecryptingStream_ReadUByte_m309746701DB93EAD7A3AB69A9236DE86ED96FC5D,
	Biff8DecryptingStream_ReadByte_m0678712C04F5B3D07DB7D011357EC43181E9A652,
	Biff8DecryptingStream_ReadUShort_m162CF922C516C11C575A053F5A9B6B1A7B447971,
	Biff8DecryptingStream_ReadShort_m54D393BE68A40AAA518B978E5D4C9F25F8945C72,
	Biff8DecryptingStream_ReadInt_mFFD0D3615D17800BCB71D6D5E8EB20B3BF8E0A89,
	Biff8DecryptingStream_ReadLong_m8246D497510F0A8AA1BC1A1BA678F4FED3DC3C6D,
	Biff8EncryptionKey_Create_m57071FAC1CABEAAD93F6F7AEE7D47126087246B1,
	Biff8EncryptionKey_Create_mCA7A044135D73E9775892925BBB8EE389650A6D9,
	Biff8EncryptionKey__ctor_mEDF30CC4E55C943C53669C23584C1170C102620A,
	Biff8EncryptionKey_CreateKeyDigest_mFF844B30F11461BA60A80A53C7C031F2DE13F0DF,
	Biff8EncryptionKey_Validate_mA2FB35C207DEFE42E7433702464178AA0189A182,
	Biff8EncryptionKey_Check16Bytes_m89DA20BAE4B5D8559A68E23C64D41BEEAC99FB03,
	Biff8EncryptionKey_CreateRC4_m8EA5477B8F800B165745768C64C07AFDF8D7CF50,
	Biff8EncryptionKey_get_CurrentUserPassword_m7469D0E0316599FFE50275F7B5E2B8A204E147E7,
	Biff8RC4__ctor_mAC14D7D28C16B1083AB55D6F4785263F62064C36,
	Biff8RC4_RekeyForNextBlock_m86FF0C41041ED51950F5C034B41E801482A31886,
	Biff8RC4_GetNextRC4Byte_mEB293C26978AB6F44AAC01E65577E8165F9B71E1,
	Biff8RC4_StartRecord_mBF93F59B09325FD8719FE2FAE96FAF38638DDD06,
	Biff8RC4_IsNeverEncryptedRecord_mFA5D9EE0AF094535E3CCD4EA12148B5B9C21C870,
	Biff8RC4_SkipTwoBytes_m9F55E617A7599141E2E9F378D6835A4C3EC72075,
	Biff8RC4_Xor_m29E517ADA732510F0895004A391860F995EE24EA,
	Biff8RC4_XorByte_mCB84CA640843FD7E3DA866EB5E670D9BE36EC090,
	Biff8RC4_Xorshort_m54CD3C94E6C1166647F7730C1CD6A80C3FF97388,
	Biff8RC4_XorInt_mC4E0605295F9AED68CF2EF9C019231E5B40756A6,
	Biff8RC4_XorLong_m66EC46DE2E93F1B419756343B85826420C493851,
	RC4__ctor_mA5331E6CFB9017A8F85CB170051D79F4B6310049,
	RC4_Output_mE3BD6657FEA74CB5C03E6D7DFE68F7C642E46088,
	RC4_Encrypt_m9005830D7E8DB1D9A6C0AA9C566124D6CA9E8DF3,
	RC4_Encrypt_m4348DB7DA70D0EF3BBF37EFB209EB249ECAEBC56,
	RC4_ToString_mD2DD2E895695AE80B90BFE074FBAF7B429D5F853,
	DateWindow1904Record__ctor_m56626A46BD7A7FDC42406795699307AEEA262DCB,
	DateWindow1904Record_get_Windowing_m6E8155C096B0B45713D236E355B058189FB590B0,
	DateWindow1904Record_set_Windowing_mC065FA0595787314951A80DA959A80E36CAEABEE,
	DateWindow1904Record_ToString_mB7A7778C5F120EEE211FC427EDB5B4282E9612BA,
	DateWindow1904Record_Serialize_m8E11F9511E4B8BAED68C8FB4898A3F6FB0FDF098,
	DateWindow1904Record_get_DataSize_mEA6046F2BCDBB5C8E8D2FA9EE41EA4785DE60831,
	DateWindow1904Record_get_Sid_m08C598E64D56DD2D9F8AB2BDE1D6954612600115,
	DBCellRecord__ctor_mF62F11F36F3FDDFA553E05A3B384441EC8AD0139,
	DBCellRecord_AddCellOffset_m3CBAD8DEE857365B3F28E9C040CCF03E5A2C343E,
	DBCellRecord_get_RowOffset_m0724E9BB16AAE9B0B278B3212067713926625A65,
	DBCellRecord_set_RowOffset_mA7A28FF8BBD9625EF6A96D8F431D7E0B94E71371,
	DBCellRecord_ToString_mE8EAC50277C8FC5F9A6BDC297FFB6863C564E618,
	DBCellRecord_Serialize_m816981347B0A0FCC693FBBFAA8EDC63ACEF9F1C5,
	DBCellRecord_get_DataSize_m3948DDCA413F41260A7F33025FC7750149A9BFA0,
	DBCellRecord_get_Sid_mC72AE6287B686DA542A243AA36D0880AEBE9DCEC,
	DBCellRecord_Clone_m0292DE29BFA03563EFEBF2FFE84219A8FA3DE11D,
	DefaultColWidthRecord__ctor_m0AA364C93E64DF0FBD22A9D828EFFB9A845596D0,
	DefaultColWidthRecord_get_ColWidth_m3E3DA225C126FAF8A31B7B7E30017EC0261C2B9A,
	DefaultColWidthRecord_set_ColWidth_m879E1E29FA65A7E8F8EEDF301C2D4989DAF35580,
	DefaultColWidthRecord_ToString_m17AF7FB3FDFC40B19BC051C9403A782256C3A0D5,
	DefaultColWidthRecord_Serialize_mC7765D57BE9EDE34BBCA8EFACD8141FD024B7E49,
	DefaultColWidthRecord_get_DataSize_m7188383F7EDF1DCFA252A42259EFCCE808BA9D81,
	DefaultColWidthRecord_get_Sid_m3878DDBAE3F316125A8779D0842631FA3D074ECD,
	DefaultColWidthRecord_Clone_m4AC9466CE1631C68D2CF6A35254C7C0B131EBA18,
	DefaultRowHeightRecord__ctor_m5262EE87AE3A966348839352939AAEFD4DA002F5,
	DefaultRowHeightRecord_get_OptionFlags_mF171F64AF6778D8F8792DB75DB4E33A82D2A4935,
	DefaultRowHeightRecord_set_OptionFlags_mA89F823AE164550DB0008A29DCBA65E09004C897,
	DefaultRowHeightRecord_get_RowHeight_m0E1C728301891B5F85EBBF35B90F08376DA6ECA3,
	DefaultRowHeightRecord_set_RowHeight_m33AC7F93E3C9F1F98DBF8D1193FBF2B9309933D1,
	DefaultRowHeightRecord_ToString_mD49C663778B130766EAB1A7B9BA5DC141202510F,
	DefaultRowHeightRecord_Serialize_mD1263D404C1ED1932C06D430C24996950FC550FE,
	DefaultRowHeightRecord_get_DataSize_m3A3FF1F6C6FDB37ADE540F7F0DAA88E09D64D75A,
	DefaultRowHeightRecord_get_Sid_m9A9897CA128F9106565437E03EF6318B578B328A,
	DefaultRowHeightRecord_Clone_m3E97E49C807D6462CC5226556B9A7084E930A317,
	DeltaRecord__ctor_m257F8B47A22F6B70DF6CEE5B92E10D1F0BD25E5D,
	DeltaRecord_get_MaxChange_mE7EBD159EB38B310A7F41759C2916E525BE707EE,
	DeltaRecord_ToString_m9AC7BA3B7DC204CD5A01B0F44AF0621E7004E953,
	DeltaRecord_Serialize_m8CEC7B1123174140E23223B1E2734F7644ADD7D0,
	DeltaRecord_get_DataSize_mEB6E36059600BE8C80F51FF6423914E724A51434,
	DeltaRecord_get_Sid_m3B57AF0C06D08EBAF6B3BE38E81F40D25E016E90,
	DeltaRecord_Clone_m1493A12DCDCF250CAA965581127A949660C3FBF9,
	DimensionsRecord__ctor_m55B27B81FD6E6F6CEE1668C004BBE16D5C621FA5,
	DimensionsRecord_get_FirstRow_mF065A9C854266500E5B8414CA059FC601B4AAFD5,
	DimensionsRecord_set_FirstRow_mEB9576556DE8D56EC062D12A682A9D700C69B87D,
	DimensionsRecord_get_LastRow_mB406DC18EF325F1E8F225B47137D30D0BFD29921,
	DimensionsRecord_set_LastRow_m9DAC7C66E2C72F187F1FD2B483DA68421E13DE5D,
	DimensionsRecord_get_FirstCol_m6077753F174AC1EB81A84DC1C701BFE0F9DD4487,
	DimensionsRecord_set_FirstCol_m62A7EE8178B817F6B286AF0C456A9AB73F45CC40,
	DimensionsRecord_get_LastCol_mBE15DC7E28054FEE05F7AE8483D2C6542E169EEE,
	DimensionsRecord_set_LastCol_m923A20E111FFE82C8C5A58F233DE1FEAC47EC3EE,
	DimensionsRecord_ToString_mCC8040312464EC5D2B6B18061B27D7B8059AA658,
	DimensionsRecord_Serialize_m57D554ECDF13EDB856E9C1B6CD699A22844E6D5F,
	DimensionsRecord_get_DataSize_mD9A81B94AF4DD278EB556745E062E588423975B7,
	DimensionsRecord_get_Sid_m303FF0D738A13DBD600C5AA66EA9304DC21E174C,
	DimensionsRecord_Clone_m5380EF17977928A137842DAF491AD3D5D27E1F18,
	DrawingGroupRecord_get_Sid_m2EEF7CCD893D2E19B338D30259AE0C563D75A41D,
	DrawingRecord__ctor_m16E20A9B427192F5277D12968C56137AC03D7BD4,
	DrawingRecord_ProcessContinueRecord_m87DC3ABB73B38636C4AEA8E1C505194B620F7A57,
	DrawingRecord_Serialize_mFEF341873EED72DE3272AF54CC646CA8340F6850,
	DrawingRecord_get_DataSize_mDCEF5182AD0EB1007AB2273ADAA9F644DDCA2BBC,
	DrawingRecord_get_Sid_m808E2972C92A0EEFA8CC522C42AEC5908F3C71BA,
	DrawingRecord_Clone_mCF8710EA4531BCC5573460BDD89B7C90B8554C64,
	DrawingRecord_ToString_mA396790FFB50638F06C168F4CC49F9748E8B84C2,
	DrawingRecord__cctor_m237B0A150B4B0EC999C97B06521E414C1C544A61,
	DrawingSelectionRecord_get_Sid_m6FAA5063C8F3B2FC759F8925BDDFAF79F26C948C,
	DSFRecord__ctor_m1E837A0FAEC4228A7861D5A33BEF3EC199C2382E,
	DSFRecord__ctor_mC6BB7427907839E90A848F6B96E733EFA1F4305B,
	DSFRecord_ToString_m03B2E411AACEB2065515E381FDFE704D4553C939,
	DSFRecord_Serialize_m751E75AF051269954B2EC282635D9CAD4D234067,
	DSFRecord_get_DataSize_m4C9434638C619DF0F31716DA136E164C59124D7D,
	DSFRecord_get_Sid_mBDC284B692C9B829DF96BDA14C8D774BC22C6CB9,
	DSFRecord__cctor_mF5CC112537E240E77885DEAB0AB239CE6B938BEB,
	DVALRecord_get_Options_m62A63332204835758A9CBCB3CBD7940D41E4C1F8,
	DVALRecord_get_HorizontalPos_m2A2683B44A6DF562B079AC88A8C95F43ED2AA6DF,
	DVALRecord_get_VerticalPos_mAEE635D27C56E677E5A361476F053B8A61535682,
	DVALRecord_get_ObjectID_m9AC6DF7AC67BFB3C6389B1EF833A4CFE4A25FF49,
	DVALRecord_get_DVRecNo_mC76A2D61B3F10E925647F1BF0173E99F5739C072,
	DVALRecord_Serialize_m5FB0E1140DB615F00026FF5E84CFA023C7C3C8E6,
	DVALRecord_get_DataSize_m9C15DADDEA37374CBBAC2CEE88272EE5E08132E7,
	DVALRecord_get_Sid_m058DED2375EF7CB3707983D32C71C2C997B14B83,
	DVRecord_Serialize_m54138FD224E1028EB4B1F9C19261A7353D6138AC,
	DVRecord_SerializeUnicodeString_m8CB4962F738E11D9D9B2B591D5498B03EBC9751E,
	DVRecord_GetUnicodeStringSize_mEB7957AA472765BB3AD1A7F12060A67A06203950,
	DVRecord_get_DataSize_mDD5681640757EAD7E681C625D468232B200FFE8B,
	DVRecord_get_Sid_mE2B92EDC836642A96C073E120AE7E128C52A0559,
	DVRecord__cctor_m7307229CF1C528897D40652B434094FF3C5A1269,
	EOFRecord__ctor_m7E9BEA0908118BA8271AB18DB216E5E17695EC7F,
	EOFRecord_ToString_m67D6F4BE683B1098A11FB966EBDF303C6C7CE1B1,
	EOFRecord_Serialize_m4B1A93C04424474BCAA8F579E33D25ADDC378799,
	EOFRecord_get_DataSize_mE21B4835D6FFFECB5BE57674253720E79DBA1AB2,
	EOFRecord_get_Sid_mB7A8CE02E3F0A8EA317DA7617C79392B6CC2A222,
	EOFRecord_Clone_m9750FE4792F39027CCB88D979F3C3A0A1BA7A0FB,
	EOFRecord__cctor_mE8AC4C4E505ECC4231856967953D6F8B34F21521,
	EscherAggregate_get_Sid_m7638B3FE9D50FA6E776E55A40FB212634C46B5A4,
	EscherAggregate_get_TailRecords_m7F652DB64D0B1C87E4E02F60816AA8F1D870E3D9,
	EscherAggregate__cctor_m916577A58D299DD66BC1F0FF1E38E7D2B0F5DC84,
	Excel9FileRecord_get_Sid_m13BBF02EC79AA3752592BA989BAD07584146CD71,
	Excel9FileRecord_get_DataSize_mDC831EF33C558B5C05378039B1DDBB950403F2F3,
	Excel9FileRecord_Serialize_m8DCF8F31CFDEA640C8FD1F6AE7E6419E41A740E3,
	ExtendedFormatRecord__ctor_mB5F17730743A9CC4202B15BD651D9B220853D2DF,
	ExtendedFormatRecord_get_FontIndex_mF6A7988D85ACFB66664C0BA8961E60528764C1B5,
	ExtendedFormatRecord_set_FontIndex_m79209570BC86596E9E86CBF268FC5A8A80152583,
	ExtendedFormatRecord_get_FormatIndex_m0D8AE8CADCA73EE596476D805918ED6EB148145B,
	ExtendedFormatRecord_set_FormatIndex_m95D3F7AA3CE0ECC678DD21948537E3B0241AFA90,
	ExtendedFormatRecord_get_CellOptions_mDFECBB909CCE54172CB21C77BBA14F15A11EB0FE,
	ExtendedFormatRecord_set_CellOptions_m8ED7912A6DE846BCCE88E60844325F9898C5D2F9,
	ExtendedFormatRecord_get_IsLocked_m714835D0023DFE97032CEAF99523824E3DF20B25,
	ExtendedFormatRecord_get_IsHidden_mB389AE3480C537484EFDA217BCDD5719804A9D29,
	ExtendedFormatRecord_get_XFType_m8CC9AB69DB5FF3755D2ED11BA2F9FAF4F523A24B,
	ExtendedFormatRecord_get_ParentIndex_m15B965D5C4F9BF0E8CC4B372CD180B11F262A059,
	ExtendedFormatRecord_get_AlignmentOptions_mAB61554927D2EAB9CCD4C1C05A0FAE854EA720DE,
	ExtendedFormatRecord_set_AlignmentOptions_mE920C132A10E4E578A8F90DD216AD9859108EDFA,
	ExtendedFormatRecord_get_Alignment_m408DD577BFE397C1F0CA54EF28F20807E6CCCE08,
	ExtendedFormatRecord_get_WrapText_mFB16D3C4A8774ABF7921B9A1D56C605DA6DB646C,
	ExtendedFormatRecord_get_VerticalAlignment_m14BCCD2DCE19ABAE8006C7CC396B40C12420ED56,
	ExtendedFormatRecord_get_JustifyLast_m22010DC9F24695E55E9AA574963E32A2E2FE4394,
	ExtendedFormatRecord_get_Rotation_mD2C2CCA83569B8B5654FF86BCDF602CB8BDE1B8A,
	ExtendedFormatRecord_get_IndentionOptions_m0A67A648825F97BB721C660EBB744A0C21C17144,
	ExtendedFormatRecord_set_IndentionOptions_m78A754F7C864DD8C9E7B6DD1FF6D8A016B8130D6,
	ExtendedFormatRecord_get_Indent_m47BF4131487657092F66EF72C7B437F50617D00E,
	ExtendedFormatRecord_get_ShrinkToFit_m6B0C8C214A7ECE95FACD575F9E8204E6A0BA8CCF,
	ExtendedFormatRecord_get_MergeCells_m2DD944C1C83509507427415B0885BC73BDBB6560,
	ExtendedFormatRecord_get_ReadingOrder_mC1A794335731265C76373FBAC8984671B9415624,
	ExtendedFormatRecord_get_IsIndentNotParentFormat_m8AE2E678151772B8582CBF20D194797AD10EF76A,
	ExtendedFormatRecord_get_IsIndentNotParentFont_mB4739AC997951FB3FF7CCF51FB91CCF8BFD95FDE,
	ExtendedFormatRecord_get_IsIndentNotParentAlignment_m2845122D857AE3124EF87462E827F89335D3612A,
	ExtendedFormatRecord_get_IsIndentNotParentBorder_m35E05A297D6A1A74128EEE12E01ACD8174D2CC8F,
	ExtendedFormatRecord_get_IsIndentNotParentPattern_m6FDA485A42E76480718DC8D4686B9AAF67FC3560,
	ExtendedFormatRecord_get_IsIndentNotParentCellOptions_m5C7E9E987BA2AC798725F9D8BB0189E1C12AF713,
	ExtendedFormatRecord_get_BorderOptions_mC8587B8108EAC4CD3D626DA7F1242B793AD26339,
	ExtendedFormatRecord_set_BorderOptions_mD66571F8D390901E294E93D0ADF58920EA5ACD6B,
	ExtendedFormatRecord_get_BorderLeft_mE1581D450371753F7EFDAD923CEA78B228DE1684,
	ExtendedFormatRecord_get_BorderRight_m1B63E7DA9FEC715F432A183EA904AFF71EA16044,
	ExtendedFormatRecord_get_BorderTop_m2FC09BA9846AAA1F124973D9B2E45F07615F444D,
	ExtendedFormatRecord_get_BorderBottom_m94ECDC371014A9C6DC5AB3D624EBD2423CF90C26,
	ExtendedFormatRecord_get_PaletteOptions_mD962C0BB31C2F47D848707C33212FFD4CE8A2A7E,
	ExtendedFormatRecord_set_PaletteOptions_mE2C48C6210CF63D98C1B853F94DDD44537B28FB0,
	ExtendedFormatRecord_get_LeftBorderPaletteIdx_m23874E8AECA8C970CCDC72878128A6F4D518FCBA,
	ExtendedFormatRecord_get_RightBorderPaletteIdx_mAA2A116746B7C93A051A29DEA14EB584A1EC5FF2,
	ExtendedFormatRecord_get_AdtlPaletteOptions_mE779A0C00DDF0BC6D3B53CE73899BFB41736113F,
	ExtendedFormatRecord_set_AdtlPaletteOptions_mA2D5AB24CDB3F9280658B4A9CB9793702691F477,
	ExtendedFormatRecord_get_TopBorderPaletteIdx_mF4F52BB25689805BE538D626673527B7047470D2,
	ExtendedFormatRecord_get_BottomBorderPaletteIdx_m1A8FFDD425513FBF44627C56A8835F5C9FC6E4B0,
	ExtendedFormatRecord_get_AdtlDiagBorderPaletteIdx_m693FEC40305F43175EA93D988E7062F3436821DE,
	ExtendedFormatRecord_get_AdtlDiagLineStyle_m177493DC0595600859E3B6288718D3C66C31BF9D,
	ExtendedFormatRecord_get_Diagonal_m55025B14AE2A515201ACCC782B817A6C9FDBF5C5,
	ExtendedFormatRecord_get_AdtlFillPattern_mE4EBE67FCEFA87A9FC9107DE5229C50FFC760A4A,
	ExtendedFormatRecord_get_FillPaletteOptions_mDBCDA362AED751E008FE047AC345A057383BEE16,
	ExtendedFormatRecord_set_FillPaletteOptions_m3BFC29858984083D9E3684C074D297A24DA635E3,
	ExtendedFormatRecord_get_FillForeground_mAA5A7517B0F47AE6FF7B0E44EAEFDD8D8E3D11C3,
	ExtendedFormatRecord_get_FillBackground_m248B706AC0D6346DD48D8978F8B6A6DD8C81583C,
	ExtendedFormatRecord_ToString_mAC0D98078AC68F5067998B3133EE326D36F46AD4,
	ExtendedFormatRecord_Serialize_m3769E09500258F6F983606F180C5E4E1F34251A2,
	ExtendedFormatRecord_get_DataSize_mB13E203C64BDAA03740D01666230681FBFD1D20A,
	ExtendedFormatRecord_get_Sid_m534740EC90A8527468414852D74729680C27BDE7,
	ExtendedFormatRecord_GetHashCode_mD6E3CC3C02C70B6858F352E0C350A2CF29A1A5A4,
	ExtendedFormatRecord_Equals_m133487B0C1A885D057773E9AEFBD7ED9888BDA1E,
	ExtendedFormatRecord__cctor_mC02A4BAF99272AA4612DBA8790407ECDECEB1319,
	ExternalNameRecord__ctor_mA08F4771A5CDBC88540B51B338382C8698132FD6,
	ExternalNameRecord_get_IsAutomaticLink_mF2AE75D6374EE53DE5859A6C6F71F40036C551AB,
	ExternalNameRecord_get_IsStdDocumentNameIdentifier_m4F64D24333568269DE9F5506BAAE2CF9526263F0,
	ExternalNameRecord_get_IsOLELink_mBADDCD59777CC0E25D9A5FA723B26857D35E03EF,
	ExternalNameRecord_get_Text_m4C9BC3AE71CB052FE557126C22E7658A94A0F6F8,
	ExternalNameRecord_set_Text_mB77EA01F511CF5D384358D0BB535F6AAEFC4BCC1,
	ExternalNameRecord_SetParsedExpression_m450F9EE6E6ADA2FB88BB0D0DC61BAAEF1C3EC082,
	ExternalNameRecord_get_DataSize_m4F11FE1981E2E3A5E0AB82BE28AF41EFDBC72756,
	ExternalNameRecord_Serialize_mE001C36A8E79AEEC97F3C3B3D6A2891A583E6F3D,
	ExternalNameRecord_get_Sid_mC67D24AFF956A2CA3F5AD1282A32E6C839D828EF,
	ExternalNameRecord_ToString_m9B36882BAA91E46B2BA279619B1FD20B5C7FA1DA,
	RefSubRecord__ctor_m298CE0B55AADCB6666D35FFBF75FD5C0E309E215,
	RefSubRecord_get_ExtBookIndex_m8D2A29319000B59CA8F32C9F94C4A4716540E576,
	RefSubRecord_get_FirstSheetIndex_m29ECB38AF49F94F921130B76ACF1764E69F6DAEA,
	RefSubRecord_get_LastSheetIndex_m5B676F858ABA668B3476E65F18994C4DEE2133C9,
	RefSubRecord_ToString_m714D33B1C537FE094A8747027DCC6F1D64134804,
	RefSubRecord_Serialize_mEFB1E5213C8E56F4E8B3A6C55A3D788F92B69348,
	ExternSheetRecord__ctor_m93BF5FEFFBCFE66A2B07F17706B10495FEDFDD46,
	ExternSheetRecord_AddRef_m16A63EA71899F956A5ABC40E9F80E37E8970F5FB,
	ExternSheetRecord_GetRefIxForSheet_m9CA8C02CD0B1AB70F85AF9CA787C0666689B9AE9,
	ExternSheetRecord_get_NumOfREFRecords_m7B5E842F344839A916488A55AAFD1270948B83DF,
	ExternSheetRecord_AddREFRecord_mE93A4D61874F29D6EBE116579FA8EF7995E8E2E6,
	ExternSheetRecord_GetRef_m82313526D07ADE3C26DA6E87388D39E36D5F1EBE,
	ExternSheetRecord_GetExtbookIndexFromRefIndex_m4207CA3ABA879DED341BB58D0E99B849AD8FEC2B,
	ExternSheetRecord_FindRefIndexFromExtBookIndex_m1069F3260288DB9D9E78609F1ACDD9BE8F51AF8F,
	ExternSheetRecord_Combine_mE8BEF4FF3163972218F3E016C70F6FEE5D009B4D,
	ExternSheetRecord_GetFirstSheetIndexFromRefIndex_mEC33E57D7F9B63F54B79B836285316A663D21596,
	ExternSheetRecord_ToString_m17FD01785FCACA0BDFC7B74D5CC4BFFCE312E54B,
	ExternSheetRecord_Serialize_m4CE17E7805786730F815925816FB522887282C21,
	ExternSheetRecord_get_DataSize_mAA5B42E649DD96A3A17D4A189DEF507561A17377,
	ExternSheetRecord_get_Sid_m9620774FAE5D4E85608CE978DC349C7C7A3652A3,
	InfoSubRecord__ctor_mC9D032BB1EFD44288A1A9D5E7D8196546414AD1E,
	InfoSubRecord_get_StreamPos_m4095800DD9E662871A910070301635A85737A980,
	InfoSubRecord_get_BucketSSTOffset_mC6CFACDBB9C40485F40FA5D7487749F7BF1814F1,
	InfoSubRecord_ToString_mFFAD461E9512C8299FC7FACC7650992659BF1F99,
	InfoSubRecord_Serialize_m04A98BD625BF7D0AC466D7DFC14C6A56ABAEFA5A,
	ExtSSTRecord__ctor_m96646D84DC69B11ADF2DBC43539D510D3B959BB2,
	ExtSSTRecord_get_NumStringsPerBucket_m8769C5DFA460C39F9C2D2A5C6032F52A60BC8D13,
	ExtSSTRecord_set_NumStringsPerBucket_mA2D566FDFD11C98DCF2655E7599B0E7861513C7F,
	ExtSSTRecord_ToString_m8DECCAE19C6B99701780C8561C18F6FD8784C105,
	ExtSSTRecord_Serialize_mCEDDA02DDF270CFFE0C4B2D8717835D74A3A40CE,
	ExtSSTRecord_GetNumberOfInfoRecsForStrings_m4E90946085491598338EACA47A3C3D024071CCF0,
	ExtSSTRecord_GetRecordSizeForStrings_m9E56C3A12C9B727C10BBA129CC7DB7AC28A4945F,
	ExtSSTRecord_get_Sid_m1016E9DF1DCB74FB1EC08406768B3A52AA036219,
	ExtSSTRecord_SetBucketOffsets_mDEA8DA87B5DC589DC4E663CD8EC58DF057DCB4A7,
	FilePassRecord_Serialize_m3051CE15C86D51FD60ED4F185A20E3673487BD39,
	FilePassRecord_get_DataSize_m576D28BC3EA6DF34483CB9DC897F18716D77BD9D,
	FilePassRecord_get_DocId_m3780F9E63F1AB0D8CFA7594308227E38B520F8C9,
	FilePassRecord_get_SaltData_m72419A244257CF207EE1A5597A915A805C4F3D40,
	FilePassRecord_get_SaltHash_m896D3947A7525B6F551CE4F2496C18C107079B2D,
	FilePassRecord_get_Sid_mBCB2F9B2FC4AE61D1F645CFE8B062E2EDAF95AA1,
	FileSharingRecord_get_ReadOnly_m91E552D494CBE4D675A6E15D0167E21ED6C61B41,
	FileSharingRecord_get_Password_mFE888D24EF9E959F9ADEF73157955FBEE268321A,
	FileSharingRecord_get_Username_m5D7D953C7EE92F0FD134C43311C3F827AD7CD4CA,
	FileSharingRecord_Serialize_mC3D2BC459061352EB29AA59A641C382232CF7735,
	FileSharingRecord_get_DataSize_m5AE7B2D1EA501E472C30D6EA8EA5A866C562FAD1,
	FileSharingRecord_get_Sid_mF470085217314C9F6B07817B892F9E1A47466530,
	FnGroupCountRecord__ctor_m1735BD5F568B4E23B327D6BFA96C55060E2A880A,
	FnGroupCountRecord_get_Count_m4F9A0AA6E4253364E256AF6CB322FCFEAF2E0103,
	FnGroupCountRecord_set_Count_mF12ADA329E1C4F0168836D24872B4FC50B30B93E,
	FnGroupCountRecord_ToString_m1DC31C876F60694CCBF723382D53DF2832F6C2C3,
	FnGroupCountRecord_Serialize_mE4711235EFCBE41DB6BA3C7C5207D4394E1ED7EC,
	FnGroupCountRecord_get_DataSize_mF40614533D8DC41441E1344147790E5510BA311D,
	FnGroupCountRecord_get_Sid_mBF42D71D7691A7458AAA6497D8DB2333AD485DE7,
	FontRecord__ctor_m8E897F3F601E95D9D2A3B03CFFAA96CEFF472107,
	FontRecord_get_IsItalic_m202C14A6344B77A7E471D69BEC2ED38000511981,
	FontRecord_get_IsStrikeout_mD61C402955F710D6C1C8F04AF6ED98A2FF161C0B,
	FontRecord_get_IsMacoutlined_mA1CE8433E9C529DFB0A8FD9AEC83DD8321289BB9,
	FontRecord_get_IsMacshadowed_mE4C7BBFD9AB05FF471B5D8BA11C4610C365C3905,
	FontRecord_get_Underline_m0F543BBD7D500D4BE73D60C9DABD684830585BCA,
	FontRecord_get_Family_m744B91D419746A4C4F63B67539741644DB71A263,
	FontRecord_get_Charset_m44A0444C2E57EB7CD68CB98FE8F19A13AA907705,
	FontRecord_set_FontName_m2C297BA4D4DF011090745B627A2B0D0EEF30FA8D,
	FontRecord_get_FontName_mD55244AE14FD2CF032AE8B56440F2CAF27278939,
	FontRecord_get_FontHeight_m1B9DC127AEED82205D063D38AE0A9274ECD0E167,
	FontRecord_set_FontHeight_m7A01E11F32B36D296A793B992FFBC40A6B71AE4C,
	FontRecord_get_Attributes_mDC98F6F0F40E771D1C8000D93CC2E9096B9777A0,
	FontRecord_set_Attributes_m02054F6403ADBBFF78DFA0EB486098B3F930EA91,
	FontRecord_get_ColorPaletteIndex_mE992DA3A9A9ECE7514CA79188723A418D8FCFB32,
	FontRecord_set_ColorPaletteIndex_mE87BE4F0191D3269CC8A80CAD0231AA6BD5F1C68,
	FontRecord_get_BoldWeight_m2F4C74684E3E810C063619E1E953B00575ADC258,
	FontRecord_set_BoldWeight_mDB162383D0D707C021878DEB3EA46739D4E4B3B3,
	FontRecord_get_SuperSubScript_m10DA594482D4C5D1E87D95E823A35806E4A3C232,
	FontRecord_ToString_m6FFDCA956431C7E4F4ACDEACA5706B8B8AAC7F6F,
	FontRecord_Serialize_mC0DE23A58286627EDD87B492637D7F1166CC6632,
	FontRecord_get_DataSize_m759F02B007E8F31EF0C2934676C83B6EB9EA05C3,
	FontRecord_get_Sid_m66AB9B9321BCB866F86C11BD96AC08EEC832256D,
	FontRecord_GetHashCode_m8F62D3767D790D89F64FC77425A1D44EC439E780,
	FontRecord_Equals_m619EE54A6D7533674EA52784EE5746D72744CFEA,
	FontRecord__cctor_mA09719A0A23C0BD9DBC543E3D68CDDA006631E9C,
	HeaderFooterBase__ctor_m7BE8491789EC9131D5F9973D9D7AAECC3AF179B5,
	HeaderFooterBase_get_TextLength_mC7E24E6891166B59E6076280E9537CF48A796AEB,
	HeaderFooterBase_get_Text_m17137CB328E57BEC6B9DB2B5C78B412B3937051E,
	HeaderFooterBase_set_Text_mC64ED03A90167998BC6AF290DE39E30E3AAB01BF,
	HeaderFooterBase_Serialize_m35A50553047596CDA37F913EF34DE1D8842683C3,
	HeaderFooterBase_get_DataSize_m8FBC3FFC6512AFC4FD2C2206605AAA337CFC8E44,
	FooterRecord__ctor_m4AAF3DCF1C51208DA0A450E53C4085BCA96D61B4,
	FooterRecord_ToString_m29E377CA4BD0F68D197DAB317273A7F37C8D888C,
	FooterRecord_get_Sid_mDCB33B5C9CF66D555758551901B665AB055E0B83,
	FooterRecord_Clone_m27383BE767A8DBA803BDD4E584AF0F9D53FC3276,
	FormatRecord__ctor_m94CAD2512D7B725019634FC9AAD67B2658740DA7,
	FormatRecord_get_IndexCode_m72F486E28462D0E7904BB786275BD51E2EB09505,
	FormatRecord_get_FormatString_m1091C9B9EED04FD6DFF6D9145E9F3EF75E2313D6,
	FormatRecord_ToString_mE8EBFA8B575A6CFFF3EDDA4CB8EF18298C5DBFF5,
	FormatRecord_Serialize_mFAD5D3B5CD0C803F43D9472A1DC936FF04F37397,
	FormatRecord_get_DataSize_m9AAA35E72BF58FE83DEFC486F74B1CA07D821ABF,
	FormatRecord_get_Sid_mF251093D932E49790EE8ACA455A484EA03B22376,
	FormatRecord_Clone_mDF25BEF3F0F5F9C7CB62AC1E86E4F1823D818CB9,
	SpecialCachedValue__ctor_mF301DF49A8E18803C8FC2ADC2D03B262CDCB4637,
	SpecialCachedValue_GetTypeCode_m0D7605C12D5119BA22F2A8472453B73213AE3CEF,
	SpecialCachedValue_Serialize_mA17E9C114D6CB1612D4A2FD22687DC31BF8CFEAD,
	SpecialCachedValue_get_FormatDebugString_mFD5305446A25B9BCDCC59DC15F329E6EC199C9BA,
	SpecialCachedValue_get_FormatValue_mF7206E18CBE0A99AD168C3F2193326E8B9AA752D,
	SpecialCachedValue_get_DataValue_m4D68C71CCD41843CA1CB9540A8BEB7420BAFD091,
	SpecialCachedValue_CreateCachedEmptyValue_m3F9813DFAC2435B2F185930138FD293E23F954DB,
	SpecialCachedValue_CreateForString_m1A8B0FE9CF8F949D9422822D89D4D0F0B218F51C,
	SpecialCachedValue_CreateCachedBoolean_m1CFC66E74081E472350977449BBC1F01E7526123,
	SpecialCachedValue_CreateCachedErrorCode_m8E904E577E0662AF098F60A478E7BFA190A9941E,
	SpecialCachedValue_Create_mDCD0F037E38447328C10B28D4309D11AAE664C5A,
	SpecialCachedValue_ToString_m911568568225C9C378A610C4CCEC505AFFC9E0EE,
	SpecialCachedValue_GetValueType_mEBCC10DAB1AD877084DD63096E6341E847760AE9,
	SpecialCachedValue_GetBooleanValue_mA7E99C446DA240CC327799F532B13A1B6E9D227E,
	SpecialCachedValue_GetErrorValue_mC95AA902EA71CC75EDBDE893B6107534183F99FA,
	FormulaRecord__ctor_m5C2A29C86D01F1986003FC2F56BB5046C9854F62,
	FormulaRecord_get_HasCachedResultString_mA98433BB779FEFBF88BCFBB02F7162D0BCBE0D26,
	FormulaRecord_SetParsedExpression_m3E3F9E3A88F7DCF13D203BF96F9042C8A21548C6,
	FormulaRecord_SetSharedFormula_mA21166DA33CE38F691BF93E600801090B25D9F63,
	FormulaRecord_get_Value_mE6CE2ABC2AF206B68388EB888E5AFE1AD26E2D21,
	FormulaRecord_set_Value_m082EFE2633A41A6B50A46787CBA8B79BB4A3B2AD,
	FormulaRecord_get_Options_mDB8CE731FC6032126970152730624813EDB6FB23,
	FormulaRecord_get_IsSharedFormula_mE9DB60987C8FD24C2D83FF7800C28BC6280BA6AF,
	FormulaRecord_set_IsSharedFormula_mD232AA9EAE5460F38265920E1B2B39AFC5A8CCF7,
	FormulaRecord_get_ParsedExpression_m8C0654D07CAEA7E1CA2528C9AB2378878F6EEF06,
	FormulaRecord_set_ParsedExpression_mF73C12785331D98EB37871A1A17DE3A6E0DAD9C5,
	FormulaRecord_get_Formula_mC39E382E20C0D15D01DB060534668061C281538D,
	FormulaRecord_get_RecordName_m0744C9E987B8EE0030B4A96550AA2FFFF513F3A9,
	FormulaRecord_get_ValueDataSize_mBB34A61781ACA17CF5BC36A83BF96BDCAE0386A2,
	FormulaRecord_get_Sid_m887046467DB2B0E13B4AE66A9E5C3A83FF4F5F5E,
	FormulaRecord_SetCachedResultTypeEmptyString_m4B81E02C64E47D3555708332FE6BC7666E1B3A9A,
	FormulaRecord_SetCachedResultTypeString_m5D6D157D0084818D3199C38A7912FD63C67EA913,
	FormulaRecord_SetCachedResultErrorCode_mDD33611E77DFA7F1A48E5EAD7784283588621629,
	FormulaRecord_SetCachedResultBoolean_mA856B8B893DFF2A028CF23C4162E97EF26B7F198,
	FormulaRecord_get_CachedBooleanValue_m5491F69242C66A2BFC389E9E85B6EF5AD43E8ADC,
	FormulaRecord_get_CachedErrorValue_m29E6203F45D5928201259D77D9EB8622EF9E373B,
	FormulaRecord_get_CachedResultType_mC2B994A1931D72F1766DDBE35DEBCA4D01B4F98F,
	FormulaRecord_Equals_m94ABE4BDB6689E9DD1976E4F810A302207A14644,
	FormulaRecord_GetHashCode_m1EC604FC85411040363E51DB95E6D26447AE1867,
	FormulaRecord_SerializeValue_m8D99A0B51AFDE90C22872A279E0C3205BE9E64BC,
	FormulaRecord_AppendValueText_mE04A65EF0E68A75D5D2D04319D030F8FD91F1405,
	FormulaRecord_Clone_mEB2AF381588E3C9747C2E4E3B6FC54728DED5E17,
	GridsetRecord__ctor_m9BE3B4CA2A97F0E1CBA3DAB9FF0CCF380EF620AF,
	GridsetRecord_get_Gridset_mC2CA024CBF3B70AA56017A677AF56888D76A5B89,
	GridsetRecord_set_Gridset_m10B38A81BB1521A28F9BBEF31AE05D89C29C0FBD,
	GridsetRecord_ToString_m405059A4B2F4FB4ED9D470E225CA535D8A277FD4,
	GridsetRecord_Serialize_m583512A2CF2AC2B86428FA6B06C56FDE2385AD73,
	GridsetRecord_get_DataSize_m9DB9B173700AD4CE50F99021A5DD567B0E7CF94D,
	GridsetRecord_get_Sid_m3CB886308C6EA879FBFB33AE1E1453D07021042D,
	GridsetRecord_Clone_m3B42BE966C6BD5BF2BBE5E67B711B57AC0092B30,
	GutsRecord__ctor_m283BE4B3A3C78B777106BD4FC95A77E821364D88,
	GutsRecord_get_LeftRowGutter_mC4594ED0798787507A3F8658073044815BB88E28,
	GutsRecord_set_LeftRowGutter_mB56D9D916BCAD4502FD8C63C3ED7EB9378D0ACB7,
	GutsRecord_get_TopColGutter_m3CC58A148D2B3388E48705E29A5C4FB4C6437FA7,
	GutsRecord_set_TopColGutter_m5CF648E6A42EBD37C92CA669C2C7F363DE2AD684,
	GutsRecord_get_RowLevelMax_m5065F8F2B7AED9746E7A2E1BA06C1CE372C2EC81,
	GutsRecord_set_RowLevelMax_m0F1A05C45F546C36FB391AF129E4089FFEBE4CF8,
	GutsRecord_get_ColLevelMax_m4D421AAFDF9D50069AC011BAEF3CF0E721C8D409,
	GutsRecord_set_ColLevelMax_mD7672729BC6D8EAC53A534BEAF841B1BCC4FD808,
	GutsRecord_ToString_mE743AB6312BD6EF07AF1CD9ED8065497FCA2D503,
	GutsRecord_Serialize_m902C98C01E9D15460A77B0789C5F2804D7A4703B,
	GutsRecord_get_DataSize_m93FE0C3F24E2C14870743ED2372096B31409759A,
	GutsRecord_get_Sid_m90F9F4291B243A04BDDD05B7D83F120D73FFFAA9,
	GutsRecord_Clone_mE107EC3B68618CE8FCD1086E16762D3C0D25EE7E,
	HCenterRecord__ctor_mC997B94DDF25AE36A1105D24AD0A8DA8D73FF428,
	HCenterRecord_get_HCenter_mD09172D70818A9BD983A3D8B4BD5664BA8853DBC,
	HCenterRecord_set_HCenter_m31D5146A74D80C2BF41530726AAB5213DB91FBE5,
	HCenterRecord_ToString_m3C68CDBB32414E9E9DB8EFFAF92041A6EAAFFC2E,
	HCenterRecord_Serialize_m017E6F4E42E7B0088261B6DCDB5DD1975F91CAA4,
	HCenterRecord_get_DataSize_mDE4D9453FB93BC5EF53441488EA38111C1ECA140,
	HCenterRecord_get_Sid_m74476FB8D7A93C6E04F26F33B97FFD15B08D9427,
	HCenterRecord_Clone_m763171C33B8D76BAD001E7EDC54BBDB598DDE9E9,
	HeaderFooterRecord_Serialize_m89B34CB9AB63D14736C4774B66AD09F7B3C3983A,
	HeaderFooterRecord_get_DataSize_m0FE07D35799F6997CC67B9F757E2D3F6818C84AC,
	HeaderFooterRecord_get_Sid_m02962AD1405A01525049C9EC504192508D4AEC27,
	HeaderFooterRecord_get_Guid_mFE497704AFD6F12052B9D017C7ABD32F1A128EA5,
	HeaderFooterRecord_get_IsCurrentSheet_m2853BB981105BE5CDDAC2FCAA7665B93D1A5AEDF,
	HeaderFooterRecord__cctor_m4126B9EB86EAC92F9D30F1F0C61BF6A694E3E9B1,
	HeaderRecord__ctor_m2314F9600D2E1AE0FC8C20DBB455A4A261BDF707,
	HeaderRecord_ToString_mDF7E915442326C8C7518FB29D271480A0DF85954,
	HeaderRecord_get_Sid_m9C1EEA11F242246A8EF4C69FF56277DAED4DBD2D,
	HeaderRecord_Clone_m542C0D503120A49FA58BB30B4E0B3FDCA06D09C2,
	HideObjRecord__ctor_mB4B7A499C4C5261B76D64E1D8D6214C4E952EF35,
	HideObjRecord_SetHideObj_m22974B736285877CDBFA910CD0CC36A791389492,
	HideObjRecord_GetHideObj_mC332A1EF339CC54E7F65ECFCA35E2CEBF7C97FEA,
	HideObjRecord_ToString_mFF6A211514FBF6C5D53AF789F7C94164A1AD3745,
	HideObjRecord_Serialize_mB16D5E59FD3F5189B3319E40CC63F340F54EBC34,
	HideObjRecord_get_DataSize_mF2E67E0F6B4090E8AB3AEAE3561FDA1E767BF5A6,
	HideObjRecord_get_Sid_m5929C5B2A0ADA9402959FB4054D521B9B0A4A0BF,
	PageBreakRecord__ctor_mEF5CB00E289D7D6632B5BE368C66EF9221D72052,
	PageBreakRecord_get_Sid_mBD45FD985E020BC24023D8C22AA41A793D58C338,
	PageBreakRecord_Serialize_m3B67CA2ACDF646C93BE40D3C371591B02BAE656E,
	PageBreakRecord_get_DataSize_mF0CF35230A6061507ECA29D31363A1AC6AD4E358,
	PageBreakRecord_GetBreaksEnumerator_mC31716AA43621697F16AA6031586BF14F58052EF,
	PageBreakRecord_ToString_mE780C793A8F4C392C61B1C479B6C6BF2633C7DA6,
	PageBreakRecord_AddBreak_m07CDD493A1C310C1E7DC0E72F034561D6CAE86BA,
	PageBreakRecord_get_RecordSize_m84A2A2CE1EF97FEC438D36D4BB6259F4C9E317EB,
	PageBreakRecord_get_NumBreaks_m460017B97011F03B3711104224A2EA5088C4DE56,
	PageBreakRecord_get_IsEmpty_m0291889761A8E7A064C3266A50DC44BF5B5BB9E7,
	PageBreakRecord__cctor_m7EA7239A05A486C28AA10C42A30F751E9794759B,
	Break__ctor_mAAA7A0A09C3D2659D307E0FFB1BEB4804899AC45,
	Break_Serialize_mCAD988EB19C710BBCE9F8F8E2ABC79F123511EC1,
	HorizontalPageBreakRecord__ctor_mDAC7BA8620BC495D719AF608CBE0F2F1F824C52A,
	HorizontalPageBreakRecord_get_Sid_m2DFF3C7AF7EE75B3CB1316BF8DAEBAFD4407A13F,
	HorizontalPageBreakRecord_Clone_m573B8BBAB4EFA0A9F03AB208F2E0AA30E160D01B,
	HyperlinkRecord_WriteTail_m6E0E315D2CAE85098ED69470879A1212639EFCAA,
	HyperlinkRecord_get_Sid_m624833D9F19E059D974CDC7FB4603E49F4F18411,
	HyperlinkRecord_Serialize_mC658A15F607ED147D745DF14262C8B2F1FF6C2FE,
	HyperlinkRecord_get_DataSize_m77895E326BD07027889F38966E363D6C2F0BAEFD,
	HyperlinkRecord__cctor_mC409C924FD19A5103EEFF6B981366E3242D2201C,
	IndexRecord__ctor_m2FE0CB7C5C1DFACB3412C4938226D8BF20C9ABEE,
	IndexRecord_AddDbcell_m2FFE1847DBA8658A86925DD008E7674660F5F9FA,
	IndexRecord_get_FirstRow_mAD9CDC0F5C040879E868E40EC7CC143AA67B0311,
	IndexRecord_set_FirstRow_m6CECCAA98B8E398AEDE7A7E333C1AF9776EA3350,
	IndexRecord_get_LastRowAdd1_mCE6165328AB4864A07B214E3B0C551178242EE27,
	IndexRecord_set_LastRowAdd1_m468F97D5EAC9C85A4F0392FCA419AFC07728C56E,
	IndexRecord_get_NumDbcells_m96D833732B816E16B685741FF3A3680CE1C79E60,
	IndexRecord_GetDbcellAt_m0A0014B813083D2D9A87F2D11C68BA82E548DACF,
	IndexRecord_ToString_mDBA7EFFB7AE47A5F88BF41B37EF69CEDB5600197,
	IndexRecord_Serialize_m2F56E7CF3DFA811628161F28DCF91B8E858E3875,
	IndexRecord_get_DataSize_m1929AA47BA0B8A740D6B772390FDB3578BE83ABF,
	IndexRecord_GetRecordSizeForBlockCount_m40FE317E1C9F07D95E0D296BDA9663AEF6728E0B,
	IndexRecord_get_Sid_m505DB6D9F7D869BCAE7DE7D0C461BA063AA59D86,
	IndexRecord_Clone_m5479BAA644C1832E131ACA1AD36BF138E5BFAF79,
	InterfaceEndRecord__ctor_mF8A068B91D53FEAE24FD6F81DA97C33C31A80D32,
	InterfaceEndRecord_ToString_mD50D291DEFE450C52C83105B1C9C4016E020EB35,
	InterfaceEndRecord_Serialize_m04DCF5BB22882184B47A551710ECEC7121DBCBFC,
	InterfaceEndRecord_get_DataSize_m6ECA7CA6410AA83D394F1C8B9BAC839AC9E63E11,
	InterfaceEndRecord_get_Sid_mA9F80EA75117698F3F77F8E57621BD1881797E9B,
	InterfaceEndRecord__cctor_m039348391E1EE6DDA4AB9BD505103E7C5FBF5129,
	InterfaceHdrRecord__ctor_m40BEAF753B54EEE58F6E95E8A64D7362DCCEC036,
	InterfaceHdrRecord_ToString_m4FBD9A5E96FAF5B9F668CC6D5AB5151E4682BBB5,
	InterfaceHdrRecord_Serialize_mE33E5290476E4C0F99657FAD5CA75C2AED3439CF,
	InterfaceHdrRecord_get_DataSize_m8760D68017A4AEF91CB47FAA7A991D5734101739,
	InterfaceHdrRecord_get_Sid_m5C314A8D5B496FE14AB48F37600A895BE7E1318A,
	IterationRecord__ctor_m2A311968360C8562A57FF2CC92DF5D970C4AAF36,
	IterationRecord_get_Iteration_mE2546EF1F6178355C8813CAB9FEC0DFBB16C76B0,
	IterationRecord_ToString_m545BD561C28B7434159B43F1A022E21DFE1E418D,
	IterationRecord_Serialize_m3F9D523766160CD693AF95CBF0265B3275C7B1AD,
	IterationRecord_get_DataSize_m128581089D06F16AB6D22F665CC1946FE67822D9,
	IterationRecord_get_Sid_m30A6F37CC909D28B50AB1382D9C411BE7EDD8740,
	IterationRecord_Clone_m500EA6DD1746A0F7E739A32E585F104CF341E8DB,
	IterationRecord__cctor_m4C9C9C78BB396F97DCD654B0040207E2CD67E302,
	LabelRecord_get_Row_m8993870F3CF6C408487E03E5494DA6ED0573273C,
	LabelRecord_get_Column_m0B59B8E81F8DC489DEFD4A355AC0D113E04DBA2F,
	LabelRecord_get_XFIndex_m9478F8540B91AB0A8A0F4F369818C4D637D4B0CB,
	LabelRecord_get_Value_m3A45054A9E87BCF1132E7380043718D0856141EC,
	LabelRecord_Serialize_m1BE41D69D306593288CAE3260B3BAB01F0ABC9C3,
	LabelRecord_get_RecordSize_m0A32A1C23BE0856B574BD7269B1541AA970CF97F,
	LabelRecord_get_Sid_m2238CD373A3C4C50729C2AD4EA89AC8AC44ED29F,
	LabelRecord__cctor_mDBB7805387F9A7CC14FF6E1D9879FC21EF53D3E3,
	LabelSSTRecord__ctor_m4FAA2F12C1E7AB5F845202CED41060E2AF2F4B24,
	LabelSSTRecord_get_RecordName_m50DCB94A8296B7ECFF420E9AFEC75D8CD5E9C13E,
	LabelSSTRecord_get_SSTIndex_mFD9BE1B192AA9D64267F2A122F9DC794EDA9CEA4,
	LabelSSTRecord_set_SSTIndex_mE06E0474755BD2B87755C7E556B138DE758AF1F5,
	LabelSSTRecord_AppendValueText_mB4880A8B4C176BACD10E9119E81ED214AD562DAA,
	LabelSSTRecord_SerializeValue_m9175FA82C94C3E4F8B47F29B3EB08AA5A77199B8,
	LabelSSTRecord_get_ValueDataSize_m625CC3632523E99C0851F67B9F0325A70FED5643,
	LabelSSTRecord_get_Sid_m587AEB7BCB5D8EE64AD1596BB15DE66CBD4AFCBD,
	LabelSSTRecord_Clone_m7C085FA80F51D4EE44B8B4C821D5B59CFC1ADDA8,
	LeftMarginRecord_Serialize_m64BC620A52442D624142688575D39D768F20C05E,
	LeftMarginRecord_get_DataSize_m5A1A9C371D3302CE4EF499E13290B721E11FD373,
	LeftMarginRecord_get_Sid_m4A2BDFDCDD8BDF0696902EA0FF96B21BF9423AEA,
	MergeCellsRecord__ctor_mEDA9851ADCA47B4744EA7E72DA12119CC76AD416,
	MergeCellsRecord_get_NumAreas_m6F5E5D9ACE04645E256AD945E34DF7073D30AABA,
	MergeCellsRecord_GetAreaAt_mEAF1D57015C664B13C40B6509F5E8A5538179833,
	MergeCellsRecord_get_DataSize_m6709F11C3B8EB209670F7800FE80030C1ABE6184,
	MergeCellsRecord_get_Sid_m0634D8C75767D9DCF7A236DB181605525F46D704,
	MergeCellsRecord_Serialize_m2A1A1072386BB3418EE2143DBBB9EE16736E1352,
	MergeCellsRecord_ToString_m9353C222073F665C878072D1B55C397B1A696A98,
	MergeCellsRecord_Clone_mEE5F50D8400734A1D56363E18E14CC78C65C7217,
	MMSRecord__ctor_m8E9816D1049BB92F8BF09213C98F26100337935A,
	MMSRecord_get_AddMenuCount_mD185C38DE20E45E418A47584F29C9E8CE04B823D,
	MMSRecord_set_AddMenuCount_m4F6A71D07C3DE87E53BC020106D2767C311BC030,
	MMSRecord_get_DelMenuCount_m0BB56027DCE3B2FDC4BDB154641A39AA0DBF0FC8,
	MMSRecord_set_DelMenuCount_mE6C2BE9F75CA39348661E17593DE1A05D6AFD2D9,
	MMSRecord_ToString_mC138A2D406588C0E81399C5D824CB75190C0EE76,
	MMSRecord_Serialize_m618772964BBBD984DF276EDE170776A01ADF0BB0,
	MMSRecord_get_DataSize_m19633ED01820E07EAB81E8E59FC87EB00FB7FDE4,
	MMSRecord_get_Sid_m1D4470918CF8FC1E98588ABC949A3D51B8B14891,
	MulBlankRecord__ctor_mE2C9B1DD3C109C1E4F3720BCA41D130BC625A764,
	MulBlankRecord_get_Row_mB574A1D6701D965889C560124B02D0A577F0BD61,
	MulBlankRecord_get_FirstColumn_mD933B953D2C5A849C49CE91AE0ED18210E2E2A26,
	MulBlankRecord_get_LastColumn_mF980F4D887CAD25423C9CD9B330020D017CCDD24,
	MulBlankRecord_get_NumColumns_mD347F2A1527E621F0753EEFA161F44D4ECCC82F2,
	MulBlankRecord_GetXFAt_m86E1E9EBAE0CC7AD2A302DE1390E5AF8AFD8EA32,
	MulBlankRecord_ToString_m9013DF4826C1EDD4B836DD06071E1497791B8418,
	MulBlankRecord_get_Sid_m782AE5CA3E4F4C5EB6AB06B7453EB8727AAE0940,
	MulBlankRecord_get_DataSize_m7C1B2DDF83A0B04820E1C2C247884578CD817A7B,
	MulBlankRecord_Serialize_m3982B94834BF855AD52F9301F1A6750B467ED43E,
	MulBlankRecord_Clone_m2DC2E274F0D4716707030E2BB661526376A51B7B,
	MulRKRecord_get_Row_m22C0E8B19034E85EDD3B7BABE7BDE8B235C02212,
	MulRKRecord_get_FirstColumn_m176099C0F7B078A40C6CAB0A806F221E4568AE70,
	MulRKRecord_get_NumColumns_m214186765FF7BA63E8339312CA37ACD46576EFDE,
	MulRKRecord_GetXFAt_mE5DED909C8FDF825ECDE4FB504B1EF98EDD1A74B,
	MulRKRecord_GetRKNumberAt_m8D7C1AB014F1942D842A9AE0198E87A9AA7ABCB4,
	MulRKRecord_get_Sid_m3D105D590AF1E497C2511052E04CF241E7B62DAA,
	MulRKRecord_Serialize_m9C7B6FBF42881DFD34948977C718ED12A442D83F,
	MulRKRecord_get_DataSize_m96CCB1525F66D7DB78D02AB1555F0A6B7142DBD8,
	NameCommentRecord_Serialize_mDDAF9FAA509042ED38E31EABF4455A90B0B15452,
	NameCommentRecord_get_DataSize_m60030D81292C74EF43FDA5EE4F0F74A9EFAFD70F,
	NameCommentRecord_get_Sid_m7BDDA57A59EB2402F922D1272C390710F6D65910,
	NameCommentRecord_get_NameText_m7E3D04909AE58EF37D6CBF5C5A98562300B12EE0,
	NameRecord_IsFormula_m4079C6BEDAE2B7211B597A25EF99956B55F03CB1,
	NameRecord_get_OptionFlag_m86C7B60291CED339C918FC21F68C4F444AA991E8,
	NameRecord_get_KeyboardShortcut_mF0D17F7E82639F056022A6CF0EF2A92B6C3BD45E,
	NameRecord_get_NameTextLength_m036F76CAAD5AD112CE69E31C7C7916C1790C2AD8,
	NameRecord_get_HasFormula_mBDFCB1F31F26056AF0F445651D3134B473E0BD8A,
	NameRecord_get_IsFunctionName_m836BCB101A079F879E85401C624DE709B8C8320C,
	NameRecord_get_IsBuiltInName_m2D1762C322FEF25ACE798129BE47A61E34DD28DA,
	NameRecord_get_NameText_m0B0C1C3A4AEDD3939DF122BA3952BE5D7AD73875,
	NameRecord_get_BuiltInName_m92D83A3DA1BB82DA83225369F76578BFA268F1B9,
	NameRecord_get_CustomMenuText_m3DB71BACD0F509B6B92FCE489A705BBA2609542B,
	NameRecord_get_DescriptionText_m7B05BEC2FC1E2B24AC23E6B52330A9A55580CD10,
	NameRecord_get_HelpTopicText_m42D2B5DAE791ACFBA165C339A8A335E5F3736A6E,
	NameRecord_get_StatusBarText_m8A8E7061F37809E12F44C77B6D74BC633B1217CF,
	NameRecord_get_SheetNumber_mDD9C10FB488E7705C16DB364A2A2D2AD33779222,
	NameRecord_Serialize_mFF052F390252613F5294C3E95C392DFF1691AC5E,
	NameRecord_get_Sid_mE442A6DA1ADB7561D14FC8B05500D2D2EE7C4DC2,
	NameRecord_TranslateBuiltInName_m40484A913AFC39600B0FAF285DCC9FEBA2269A99,
	NoteRecord_get_Sid_m42AE01A08643C120B3BBC9AD424E7D05A6BE72A2,
	NoteRecord_Serialize_m3887CD92EDEC88FAD1F7D752CEB5ED297ACBB77E,
	NoteRecord_get_DataSize_mC0F7CDB519B778BDF39E2F76ABE1201132DB6359,
	NoteRecord_get_Row_mCBD34FDC6B12BC14F1E08C4691011B35EB8F3F5C,
	NoteRecord_get_Column_m6D42D869C8DBAA47CBEAE9B3F51090BBB036CA24,
	NoteRecord__cctor_m5E58F9A6E13BFA005CE844B503F671A25B602EDB,
	NumberRecord__ctor_mC954D969DBEE80DC4C796C03646FF4556E4DDE80,
	NumberRecord_get_RecordName_mCE6891EB4AEAA06137FF6C7EF698036BFA662E78,
	NumberRecord_AppendValueText_m9449A5DD5523C2EC21F9D4F520FFEDCDD443D60C,
	NumberRecord_SerializeValue_mD5752D39181A99103D670A8398828C4043D98EC4,
	NumberRecord_get_ValueDataSize_m80CCA9F55E27A0A0BDC06FD346FD672BCD151CA2,
	NumberRecord_get_Value_m9FB71FB95D4940EE7DE9AC245F69D8D6E27F8ADD,
	NumberRecord_set_Value_mC532184E67BDBFA87319C18C14A6FEA9BEB6284D,
	NumberRecord_get_Sid_m6C8FEDB49569865F1EAA884377C6DD58CD78C516,
	NumberRecord_Clone_mDBE17D48825DB27CD5A701054C1928E745FF1250,
	ObjectProtectRecord_Serialize_m682DFDAF9E95E9CA85435D33993E0062F7E6E08C,
	ObjectProtectRecord_get_DataSize_m96555AEB820E62F73BFCF289997403C79057F4FD,
	ObjectProtectRecord_get_Sid_mE1E0116924F33BB8503ECD4DBCAEBF3F86A92E8B,
	ObjRecord_Serialize_m7033733E1C5EC6B4454ECB931E79FBBDB9524233,
	ObjRecord_get_RecordSize_m36DC6527EDB0E5F9BDD522D8A8618BDDC2869212,
	ObjRecord_get_Sid_mA76617D818FC15EFE23C03486C7DF136B077BD13,
	PaletteRecord_Serialize_m0A3C315330C0AD04B6589B0BCBB7D3799ECB648C,
	PaletteRecord_get_DataSize_m346BE74649D16271F1415CDCC3ED6B194DBEE92C,
	PaletteRecord_get_Sid_m38EC4BF9795DF8B0B757C485529A989B16F6FC73,
	PColor_Serialize_mD6688BB2CC6427672F9A3211158529D450AD9E12,
	PaneRecord_Serialize_mD4C57650BDA324A28587B4C71ACA488FC8A787BA,
	PaneRecord_get_DataSize_m50981724FDD93019385B814559E89C1161B812E6,
	PaneRecord_get_Sid_mE2EAB2A0AAC1B15F78BD31AFBA93CC7E6EA4DC9F,
	PasswordRecord__ctor_mAAA2BACC73A15B443E44F49A0DBD6FD05EC1DFE1,
	PasswordRecord_get_Password_m1D5CC388F2A21F671D0857A92FACC8EE4C303606,
	PasswordRecord_ToString_m52857690F0A02BEECBA5FFB22EE0986CC3905C7F,
	PasswordRecord_Serialize_m08BA7B78D821730B0279DB27B0ED924209EAB136,
	PasswordRecord_get_DataSize_m2D776B4222F58C03093A908C790E4F0A5D2D9307,
	PasswordRecord_get_Sid_m52F8B71B2045FE6D098A58E41E3D3D45BD89BC0F,
	PasswordRecord_Clone_m8161391E6FD3B96C6F3622345CD680963684D5F1,
	PasswordRev4Record__ctor_mB53B5225A4F76064FAA9790661AE1D00A93F0D51,
	PasswordRev4Record_ToString_mBFBBAA2B02936D10F6E23BFBEF5A5099B0AF983C,
	PasswordRev4Record_Serialize_m07E4A87A961AC0DF57A8D47970F7379F57DB7A48,
	PasswordRev4Record_get_DataSize_m4254AD603ED62894CE2BE92EB95983657BCA3F93,
	PasswordRev4Record_get_Sid_mABFD2F4DF161FE3755DEF6F5C39AFDB04A1EE65D,
	DataItemRecord_Serialize_m2D7602B458ED94215A7C12066C57A4CACE2639EB,
	DataItemRecord_get_DataSize_m7C1E08A6DB5B2893F35859D509E08C8CA0940183,
	DataItemRecord_get_Sid_mB5643295063F46978C4B87B19CEA59931C7C4902,
	ExtendedPivotTableViewFieldsRecord_Serialize_mC2F73B2A79D3F9C128841AB56565308F5D302498,
	ExtendedPivotTableViewFieldsRecord_get_DataSize_mEAEA8384D5479DEA29B42502DCA0711970F5A193,
	ExtendedPivotTableViewFieldsRecord_get_Sid_mEBE5D290E99D0A868E6E0F55F8F44798BE3FFE65,
	PageItemRecord_Serialize_mDD6E4CE43F5F0FF8A93B14711B50FACA7FE623AD,
	PageItemRecord_get_DataSize_m1A9F5D68EAD75312FFB0BEE43F0D2CD97A90F333,
	PageItemRecord_get_Sid_mB333E09BF9490102C992DC984DD19162CDE71C66,
	FieldInfo_Serialize_mD9FA292F54F8448B8ADC4A6B83DAF4CF118BED40,
	StreamIDRecord_Serialize_mFEF4D815574EF057E0E91DE9DAE1BB1F7715D8D6,
	StreamIDRecord_get_DataSize_m3BDC0C7F79CADDCCADD740E37D44FE375CF61429,
	StreamIDRecord_get_Sid_m39658F31DAFEC0D633D6909D7A4B8B8F74EB59E5,
	ViewDefinitionRecord_Serialize_mF71127D7D10F3B1A05B6413C7E5823FAA63ABECE,
	ViewDefinitionRecord_get_DataSize_m0A63F68F198D7BD42FE00F0971F163DF23661187,
	ViewDefinitionRecord_get_Sid_mB74B2D0334CFA8B9B3AB10C5EFBCBD6CCF68E54B,
	ViewFieldsRecord_Serialize_m9F9943A772A2C3FDB758C7891F27DBDFF1F5FD2E,
	ViewFieldsRecord_get_DataSize_m32CD4C24D56DAC0474D4FB9A1DD944D6A08987EA,
	ViewFieldsRecord_get_Sid_m1817980F3C6F4C2813BCF1E495741ECF79E1B5F4,
	ViewSourceRecord_Serialize_m9428C1D76C179FEC4B10093B7D6852F1B29B5EF3,
	ViewSourceRecord_get_DataSize_m07C3D61EA19AFA730E79F6DA844DD5611F0BBFEE,
	ViewSourceRecord_get_Sid_m036BD59B35CC7A4EDD1433FB7B18D34B20415FD4,
	PrecisionRecord__ctor_m915CDA695D284ED0FDF92FBB92926FF8070E4A5E,
	PrecisionRecord_get_FullPrecision_mD0BECD2B12B0EDDDF9ED57CBBF1766AC835435AF,
	PrecisionRecord_set_FullPrecision_m791FCCB85DDFE912835E50A6CA76115109768F46,
	PrecisionRecord_ToString_mBBFA7D88675CC37ECC04DA90AB41080FD0F3B817,
	PrecisionRecord_Serialize_m2F734CC7C666395345FAD453C72D898662D596E8,
	PrecisionRecord_get_DataSize_m5830BC5A0B38C985393142AEDC19B39C906DCC4C,
	PrecisionRecord_get_Sid_m98C7695328E9D3A669E437F2CEDD98020FA20F89,
	PrintGridlinesRecord__ctor_mD79DA42C40924B7B79EE9B47B43E469C1CA84188,
	PrintGridlinesRecord_get_PrintGridlines_m0DC5BE1E6D552DEEF7A815A862541909244222F7,
	PrintGridlinesRecord_set_PrintGridlines_m23C45FE550D38CB1D6044AC143BC1F6C192A9E7B,
	PrintGridlinesRecord_ToString_mB4F996D83795886E3546609880B22996F070C9AA,
	PrintGridlinesRecord_Serialize_m3E9CE8DFDAFB72DE30829F7D6F24C2F9F8DB4DB4,
	PrintGridlinesRecord_get_DataSize_mD7F7D8E24FD95B36960BF74FCBE739608FFEF490,
	PrintGridlinesRecord_get_Sid_mF24A418E2CB8C8D3409AF0D17068A20960449DFA,
	PrintGridlinesRecord_Clone_mB173F3B03CE1EDDD6F73109AB6CEFAE3A882AAE0,
	PrintHeadersRecord__ctor_mF7792CBE8E86F7016802777A130408DF3DCCB07C,
	PrintHeadersRecord_get_PrintHeaders_m384D8B0F4F3B4A59CE5C02509C59FBD9C8DA8D62,
	PrintHeadersRecord_set_PrintHeaders_mD7E4095A2C860965C45E17A0BCD9A30D4C0A5823,
	PrintHeadersRecord_ToString_m9C81694799260D0662825871764AB73F4F8B68EE,
	PrintHeadersRecord_Serialize_mEAC6374A558686A2A4E71031A8CF40829D2BB460,
	PrintHeadersRecord_get_DataSize_m07EB5FDAB1AA1153FDC81D50137672A13BEA97E3,
	PrintHeadersRecord_get_Sid_m80B7ED2189C83239A727D6B0CBAB4F546A9C6CB9,
	PrintHeadersRecord_Clone_m50C2B3094673A06586E9F7DF831666B7C704A56E,
	PrintSetupRecord__ctor_m2E6F4E57BB9C1B744B48CA6E2ABA41CA6A8C42C5,
	PrintSetupRecord_get_PaperSize_m5266952DED2C108AA405FD092FFB6527E3F93085,
	PrintSetupRecord_set_PaperSize_mEF5B76F49D2ABBA4E60698F4DF768F915CDB1C17,
	PrintSetupRecord_get_Scale_m52E87501084B95B4A97AC5A57456C8616C38101D,
	PrintSetupRecord_set_Scale_m79E6A13D69B649A1ABA327BA9556211A16EB0CFE,
	PrintSetupRecord_get_PageStart_m35C61948D127C983ABF1FA8CEFDB9573B010E8E6,
	PrintSetupRecord_set_PageStart_mBF22D369004D2F3B64A57E69ADBFE3CECEDD8702,
	PrintSetupRecord_get_FitWidth_mF1AD751210605E009EC5CE9E0B41B4F360BE4D41,
	PrintSetupRecord_set_FitWidth_mAD77A591855B6910BD6290F65DA73AE0F597E8DA,
	PrintSetupRecord_get_FitHeight_mCE7EB4FE6F4EEBD81509900B06C2CE93B7927D6E,
	PrintSetupRecord_set_FitHeight_m872B7A737AB0C7C839B01A2C8CDAD672E1826FEA,
	PrintSetupRecord_get_Options_mA663E53444DE36E7A52C952B1603C44AE8D751F5,
	PrintSetupRecord_set_Options_mB77B33B60F3A7FFFCC9293CA9F5FFAD8146F2715,
	PrintSetupRecord_get_LeftToRight_m7F3F67BB3405A41AE6CE5E964698ABBAAA99858F,
	PrintSetupRecord_get_Landscape_m88A13290C1233CAF13F0899546802EB7B7A44E43,
	PrintSetupRecord_get_ValidSettings_m6D76EEAFD4BF1870A27F287B4657C9F0D791B565,
	PrintSetupRecord_get_NoColor_m9957A57119AED5F4420B2A5E6DA3313046BE5697,
	PrintSetupRecord_get_Draft_mC19C654F1F7DF840766D7BC4E1FF3A581FD1F4F5,
	PrintSetupRecord_get_Notes_mA011C0B1F4BF7C25E7EA2AAEDE194B46FBE07379,
	PrintSetupRecord_get_NoOrientation_m78E48912DB73CAF7FEE500829374E213B75C5651,
	PrintSetupRecord_get_UsePage_mC8460A3965847BF0B580C7C1AFB5060BF4665D80,
	PrintSetupRecord_get_HResolution_mD8855CEFD66F22034B89474ACFC4C4047BCCCA9F,
	PrintSetupRecord_set_HResolution_m15BE907CD9B1D2EF7FF3B86423EFC72E4678E85D,
	PrintSetupRecord_get_VResolution_m0EB640FC219C931090CD379B8DBA9E7AD348D3A0,
	PrintSetupRecord_set_VResolution_m522AE0D98AA83FCC5E5705219C504EC3E198B35B,
	PrintSetupRecord_get_HeaderMargin_m092FB57C60EFB924BAA57AE17184EFF2F224258B,
	PrintSetupRecord_set_HeaderMargin_m8870CD90AFC65C3DF4AA671518169BCB66FEA08A,
	PrintSetupRecord_get_FooterMargin_mBA21AE9CC3B06B8E03CD429FBF164D1F3D6FC563,
	PrintSetupRecord_set_FooterMargin_m42007AC07BD91DB2A34A7EB1BB240265553E8A03,
	PrintSetupRecord_get_Copies_mBC016463B0DB9AD55E209DB0970D125DCA31F54C,
	PrintSetupRecord_set_Copies_mA3E8633F174B9DCD195D94AEC6C6F1C16F901894,
	PrintSetupRecord_ToString_m8993A29EE0F16A47F3E8DB807B5834591B9A7B3B,
	PrintSetupRecord_Serialize_mBDFD50B06970910194A849121189A6FD9F0AE7BB,
	PrintSetupRecord_get_DataSize_m6B85B6621DD98EA01A7A67D745BC63E11D718CCF,
	PrintSetupRecord_get_Sid_mF30FB18376F5970E024A8FF35DE995705C5EAA25,
	PrintSetupRecord_Clone_m88E7AA81BF98809D97AEBE3D1DB34EA1EEEF5973,
	PrintSizeRecord_get_DataSize_mD2CA16C2730818E7697BDB2E5874F8DF61B38672,
	PrintSizeRecord_Serialize_m1B1B44E58A19F9C032E8A8FBC2C7A73BF44B73A8,
	PrintSizeRecord_get_Sid_mD89A0EAA575D03E4F4D0D89BEDBFD4D403B85896,
	ProtectionRev4Record__ctor_m77A35D6A2E91605F72C39E04D80730C1675D8263,
	ProtectionRev4Record__ctor_m9DD0CC60DE85D565A581D7C6DC5FBF67FC05817A,
	ProtectionRev4Record_get_Protect_mFC522EF8825B93CDF1761862CFFC9BAD98AA07F0,
	ProtectionRev4Record_set_Protect_m711302FA321114331BFFE685AB02474F497F59C5,
	ProtectionRev4Record_ToString_m54627711313CAA0D740D4B6D8029E0A15482271D,
	ProtectionRev4Record_Serialize_m221BB85443BDDE7E53280B740BA47A23C9AE6A8E,
	ProtectionRev4Record_get_DataSize_m160D5065E77FFA15A03A6A1710C2F40EDF4308C6,
	ProtectionRev4Record_get_Sid_mC3902FC9BEC1A8C29AAAC30B2A47D1BAF69AB2EB,
	ProtectionRev4Record__cctor_m21AB2D185176AA64BB16C6D928DD3AFEE96FF33A,
	ProtectRecord__ctor_m4EB355D565AE2BF998CABB8934776C34245CF349,
	ProtectRecord__ctor_m360A2B4940ADA7C92EA5560AFDFAAB6F4C3CA5D9,
	ProtectRecord_set_Protect_m9D2D50C348A55A760225BE672350C40B1F309234,
	ProtectRecord_ToString_mA1F70C1AD2DD5F1E5D8D5D6790425502982FFB45,
	ProtectRecord_Serialize_mD9465DFEC2D02406F09B800149C7CB7C119C920B,
	ProtectRecord_get_DataSize_m56E828B18D91570322AA4C8341AD685AE4F24CDD,
	ProtectRecord_get_Sid_m56051A11E54E356D0DC85AFE9C50A989CCE96C5F,
	ProtectRecord_Clone_mB702AD85271C7909AB76DC8C61CF65F94A309159,
	ProtectRecord__cctor_m3B30DDE51208552D640E11852981A28983664361,
	RecalcIdRecord_get_IsNeeded_mCDC2DFF869D57A560C827584534CBE4D505D0B24,
	RecalcIdRecord_Serialize_m4460EF842CBF90C67067ADEC9504BABE439497BB,
	RecalcIdRecord_get_DataSize_m78250B3E70A38B0C7EC5A96D7AECFA9D9A50F416,
	RecalcIdRecord_get_Sid_mC6E06D2255755075700793ABD7CC37AA4123F1DD,
	RecordFactory__cctor_m53B89A6E49C2E21539BC635411704BEB15BF779E,
	RecordFactory_CreateRecords_mEBFF740D8C3E6462F76C9CB2D5FFCF2E94C6B67C,
	RecordFactory_CreateSingleRecord_mE6AFDA1C3E520E0C25FABBF52089DCE60E2058F9,
	RecordFactory_ConvertToNumberRecord_m88D03709F67A2DB4362FAC1EC28C8607D9F553DB,
	RecordFactory_ConvertRKRecords_m5609A7051BA679F428090F725ACE273094844704,
	RecordFactory_RecordsToMap_m7F351388AA82167D5C7D5D83BA34A870C694133E,
	RecordFactory_GetRecordCreator_m0FDA9F1A8FA1137592F1BEBD7A1CE93C629101B1,
	NULL,
	NULL,
	ReflectionConstructorRecordCreator__ctor_m50CB74A2DEA44C34289B3EFBAC932E882BACFB5C,
	ReflectionConstructorRecordCreator_Create_m77FA2E7789CB73E6B23CB8275336D7B4A23547BA,
	ReflectionConstructorRecordCreator_GetRecordClass_m6167B3F8355793FA37E9183D77BE171D8E85F9CB,
	ReflectionMethodRecordCreator__ctor_m8431EF8FD447E6AC656A6D824C68E1829D233CFF,
	ReflectionMethodRecordCreator_Create_m29577722763241DF383CFE1BBBC427CF43A1E749,
	ReflectionMethodRecordCreator_GetRecordClass_m881BB83A4CF742D8D421997791E01FC15CA1C4C9,
	RecordFactoryInputStream__ctor_mBF56FB3D3179B84236B44BD3223DCADEF87AFA04,
	RecordFactoryInputStream_NextRecord_m4A7383BEE7B11FF1BFBA8AB2ED1E2476019D2BFB,
	RecordFactoryInputStream_GetNextUnreadRecord_m6A549A564CA56588DDC2CEB5CFBB64BD969C0988,
	RecordFactoryInputStream_ReadNextRecord_mB0CC037CDB5AE3A53FE4F65A218AF20F56188A19,
	StreamEncryptionInfo__ctor_m754B9CFD524DF414B1378C2480F007202CF1C313,
	StreamEncryptionInfo_CreateDecryptingStream_m4D56C598134EA6D79FC9BBA8956EBC5B1A786DD8,
	StreamEncryptionInfo_get_HasEncryption_mCEAAA2F3BB2F4E3F17393DA72122531876BCA80F,
	StreamEncryptionInfo_get_LastRecord_m9E2A7E5DB57DACA82098973E91F7941AE186EEB9,
	StreamEncryptionInfo_get_HasBOFRecord_m64D1ACB329416DF861E27A79C894F49AA53E0DA3,
	LeftoverDataException__ctor_m0E9F1E5CC662CF5CB791CABFBA5200546829D106,
	SimpleHeaderInput_GetLEI_m561E5EC47A50D378FC0E0A99CE5497F17ACA3A4B,
	SimpleHeaderInput__ctor_m5469DD62B58D79DF21DFB89B83D5CD08F39CA155,
	SimpleHeaderInput_Available_m652BFAEF563D065932F425F63BCE906764EAF215,
	SimpleHeaderInput_ReadDataSize_m41580F8E84F13604187E4A2439DF3B9274388C08,
	SimpleHeaderInput_ReadRecordSID_mB3E3E36ECD147CD589728DD59C61857B4E3861AF,
	RecordInputStream__ctor_mFD341E929CB85E0D1445F5658A591DDE15A95045,
	RecordInputStream__ctor_m8ABA7689D8ABF810D12D8F08F2455BA32CF7472A,
	RecordInputStream_Available_mCA63DF46EB20ACBD70A8DB13564034882B89BF4D,
	RecordInputStream_ReadNextSid_m7575504638C54B563C579327EC8C8A99FB9FE6EF,
	RecordInputStream_get_Sid_m20755873AC0545FEC5F690B73EF2ECB17BAB8B92,
	RecordInputStream_get_Position_m8DCD05DECCDA5F5C8663CFFCA9B1006187373B01,
	RecordInputStream_set_Position_mDDFECEE932D00B63459AE2FFE5821CCB9A735E7B,
	RecordInputStream_Seek_mA249A618F69BB2ADBC1FAD051383118272077FA7,
	RecordInputStream_get_HasNextRecord_m0738131F79912DC2E2028BFCAC1EE258B4CCF4F7,
	RecordInputStream_NextRecord_mE639B61BCBE6D82174ABBDA1B81170CCE08D97BF,
	RecordInputStream_CheckRecordPosition_mBD8533D26039A6745FA38E7E8BAE16141B9320FE,
	RecordInputStream_ReadByte_m4F63EF1BAC86659D46D92913615505AC58D610E8,
	RecordInputStream_ReadShort_m1879DB5630221848FA7AD15EEB916BA4B710DA01,
	RecordInputStream_ReadInt_m86790BDDD9960BB55BC4D04FF112B69A933211C3,
	RecordInputStream_ReadLong_m6ADB749C4C7A4CB5633048D49E644F513527B84A,
	RecordInputStream_ReadUByte_m01C8165DDD1DA4DAC33A5C58457C7A7850D3F392,
	RecordInputStream_ReadUShort_m15A4EC65BA16D9707BC42A0DA8C148E93520A33F,
	RecordInputStream_ReadDouble_m57253C018417A021F6974C662A49E2DD4145BF54,
	RecordInputStream_ReadFully_mAC93FA0D427DA56A9AD8696E18E5E51C1FDF77F4,
	RecordInputStream_ReadFully_mB3024FA97F779CA1E508B33BF0581F38772777E3,
	RecordInputStream_ReadRemainder_mE68F07FB0A7CC9F3F10C8A3B20896D6D136FA9A9,
	RecordInputStream_get_Remaining_mF9C377253635FDCC7983633FE1109162614D1C3B,
	RecordInputStream_get_IsContinueNext_m846C00D6D5DCF252FF9E9A2797A52B31B53AFAC1,
	RecordInputStream_get_Length_m8919B2BED9812A55C042B6BDE7D8B911D0986F3D,
	RecordInputStream_Flush_m2399182A7F1D7404C5EC4F3B1E19DF2033E37097,
	RecordInputStream_get_CanRead_m070DAC024C10AAC40256ABF9EDEEE28B0B9970B8,
	RecordInputStream_get_CanSeek_m58332DCC31CFEE58B9AD9C12F51C38EC3EF96A05,
	RecordInputStream_get_CanWrite_mEE4DA255CD956757D9AD16B4C975A648BD904232,
	RecordInputStream_Write_mE84515D753B76A723FCC9352984D33A11E9456E3,
	RecordInputStream_Read_m76D86CAB6A255CAFE2CA9CDC9B29EFEC73E1C8A7,
	RefModeRecord__ctor_mCB1F96C36D437903F89B96255F0FD17B4C6C0D57,
	RefModeRecord_get_Mode_mA8ED8CAD88B1116A8F15403A1845B6B767DC0309,
	RefModeRecord_set_Mode_mF5154A84EE00CECDD7C53B3FE4AD24E314540E82,
	RefModeRecord_ToString_mAC9DE512B076F1B5E8B50D2837CC0E12CDD4BCAD,
	RefModeRecord_get_Sid_mE5F2E2102162F22AA7041E70C5CBF7BC8A49905D,
	RefModeRecord_Clone_m6E5453D3F8723856D4F2310BD59D6B7EB117A1CD,
	RefModeRecord_get_DataSize_m5B39EEFE4D16E731E609CEB6BFB256A876F8A89D,
	RefModeRecord_Serialize_m58FEA4888B0EA3744E62934BD6F846A143FBFFE5,
	RefreshAllRecord__ctor_mF70BD90CCB19B42929402D013C0C7E545ED6654F,
	RefreshAllRecord__ctor_mAE2596D80F1985AB83E2A6CEC6C7DCDD04EA017E,
	RefreshAllRecord_get_RefreshAll_m395B110C79480B52AA65387190D1E17DC3F059B1,
	RefreshAllRecord_set_RefreshAll_mB1C9ED266D9ADD54B8627F49982106AFE17AF116,
	RefreshAllRecord_ToString_mFF0B00CFCF36B7DC1E30C3B55CC55082E239CE81,
	RefreshAllRecord_Serialize_mB0801F7F26A1F01E9E0A764D37A00D0260BFE91E,
	RefreshAllRecord_get_DataSize_m250079BE70840290A69D6E19FEF5ECA14ABDB58D,
	RefreshAllRecord_get_Sid_m7C9E5DA8605499478518E39D8FE9E549A4C9DA28,
	RefreshAllRecord_Clone_m72244268FBC0A08346F515E4BDC8F3373CF7A566,
	RefreshAllRecord__cctor_m4A24EB452BCF2DB44BD12766A5DC5AD1411738B7,
	RightMarginRecord_get_Sid_m2F49FC6ADE01BEF4923560579326BCD03146F6B7,
	RightMarginRecord_Serialize_m2037F6D07A14DFF069B0C771E77E88BD6BA72E28,
	RightMarginRecord_get_DataSize_mDD02CCFAC970CD111B507A0A2C6B3B723B4A6F44,
	RKRecord_get_RKNumber_m8A4BC6AA85080A5F738A5687F7B90283D181D441,
	RKRecord_get_RecordName_m3120DD9BE116B1BB73BB4CE9C27C587F775A0BB1,
	RKRecord_AppendValueText_m6FF89EC7D6F1FAF2D88F82E55F620AE7491D9C80,
	RKRecord_SerializeValue_m478535A8DAFBE9CE55FDA8A0D867F9A82BAD04AF,
	RKRecord_get_ValueDataSize_mBBF80778BA3E2A6C8BDA11C4301DF5D12F98C0F7,
	RKRecord_get_Sid_m3538896B26FDFC91F076A8B39DBD460E31733689,
	RowRecord__ctor_m64447BC45A8B1CEAC6F089E71EF9DF86B1A501FE,
	RowRecord_SetEmpty_m8920764924C85A59343DD5BCBB60973509CC30E6,
	RowRecord_get_IsEmpty_m851082F7DBFAD5C52981251CD61A81B17B09537E,
	RowRecord_get_RowNumber_mA648AC3F757988093B2CD9F60C123F908A577A7D,
	RowRecord_set_RowNumber_m768069F42B510C3604E88C078BB52CD770DE1C2B,
	RowRecord_get_FirstCol_mE304528FBA4FD174077CAEF5713280816AD70C35,
	RowRecord_set_FirstCol_mF74A9A34B9622579F041D8D0A681038D93A9E9DE,
	RowRecord_get_LastCol_mE3FF1A6560C1A37CF260F2A03D6B7843E16F8D7B,
	RowRecord_set_LastCol_mA6B7CFB039124CC1D1BECA54BD90DFB65CBDA1D8,
	RowRecord_get_Height_m9229E558B1A38DA12E348074782CD2AFCA6BE3CC,
	RowRecord_set_Height_mE813EC82E8B6D799C7C010E022DF8A52274FDF98,
	RowRecord_get_Optimize_mC2BA93A84FB80D2E729EE93DA587DCF7F60B2061,
	RowRecord_get_OptionFlags_mFAA2DA29B83B400244A2DA6B768B8E56D7464BD1,
	RowRecord_get_OutlineLevel_m3FC19EABDA1FA5AC9C2AF9CBBE670857B5C727DC,
	RowRecord_get_Colapsed_m1C046233253CC33203D8FEBD163DAA06234AFDE7,
	RowRecord_get_ZeroHeight_m30847EAE295F2D566AC5E5283B568F7E299BFFAC,
	RowRecord_get_BadFontHeight_m9A3FC9BFB50A654BD7A6F187B2264176DFC803E1,
	RowRecord_set_BadFontHeight_mC293EA09476D261F25EE0AFA6F2A9B291ED487A6,
	RowRecord_get_Formatted_m7BCE31099548B900855AD4857EE80AA5CA628F7B,
	RowRecord_get_OptionFlags2_m9638015715C1F2D2E2A7389126E70E62BE57CD96,
	RowRecord_get_XFIndex_m9368AF6B19146326F1ED001B7522DECFC063A423,
	RowRecord_get_TopBorder_m402FA13990E8D99B1FAD6718692011172E5A960B,
	RowRecord_get_BottomBorder_m78A05858BEBC67B86CD965F96A24ECB6A687DF24,
	RowRecord_get_PhoeneticGuide_m9F9E9496DD5BD6EBD444BFA5E0DCEF7731B3B326,
	RowRecord_ToString_m728903718B3E66361F7E186BA3900274339903DF,
	RowRecord_Serialize_m9A841F60E630B17BC51C90BA5EFE55D9D1114EB1,
	RowRecord_get_DataSize_m0370A97B894B18CB0E489084DF7034C501296472,
	RowRecord_get_RecordSize_mEDFDA60A5BDD4ADA3822997F519D9E81C64F4372,
	RowRecord_get_Sid_m88BBA6264111DE03E070228CCA34EBB2AFDB4D45,
	RowRecord_CompareTo_mC3069F7587A1BDD03A93081852074DCD4D585C03,
	RowRecord_Equals_mF2707AF38AA6BCD6D56590AACDC8DAD098DDA8DE,
	RowRecord_GetHashCode_mB8F5B1BC203DA9C174199FF822B9C298B0D1C158,
	RowRecord_Clone_m8DB4D3B5B521A218D620D41DE1521E9587656816,
	RowRecord__cctor_m85A4877E8E3CB837E4BA282A85A7A05179FFEFF7,
	SaveRecalcRecord__ctor_mF91141866EFCA6402B5E0BC9EA930CD4F9E86D46,
	SaveRecalcRecord_get_Recalc_m9307D2A5E217A008C63F29AED78D3FB02058C790,
	SaveRecalcRecord_set_Recalc_m527F16E6A7D10626D980FFF2D6DB19DB3D94A719,
	SaveRecalcRecord_ToString_m3199E59498688D920315AF9955A876E965D6E43F,
	SaveRecalcRecord_Serialize_m3026EE4833A5BDB49C8359B4AEF848D177B79526,
	SaveRecalcRecord_get_DataSize_m77233AF480A4A876B778E5EBB9DA62556E12DE47,
	SaveRecalcRecord_get_Sid_mD132C8F50703BBE6F422C5B3475059C7304A0F34,
	SaveRecalcRecord_Clone_m4E3BCC580D6BBF3136FBD1A85CF5816E6219FF12,
	ScenarioProtectRecord_get_Sid_mEB8BBE033EC8B544AE1E802039123B1A82EE60CB,
	ScenarioProtectRecord_Serialize_m2D75CEB6EF166947FD01AD011844E9212520D973,
	ScenarioProtectRecord_get_DataSize_m90CE173DA34C1E2F93910B1ABA466DB99BE51080,
	SCLRecord_Serialize_m67CA1339A3221E1CC1A9582C756D03B61DF2E233,
	SCLRecord_get_DataSize_mB30EC64F67E86A555DCAE873FC5528DB127B926A,
	SCLRecord_get_Sid_m5A96B24EF418E7359E6F3B4AA885FA3043C79DCC,
	SelectionRecord__ctor_m195145E332740605A46134509EF83BBEC17133D2,
	SelectionRecord_get_Pane_m65414EDAE5CD0769E0626F1DC3E2C8D95AA41A39,
	SelectionRecord_get_ActiveCellRow_mAF3CF2451A8699CF26A5827178D0828BCAD51134,
	SelectionRecord_get_ActiveCellCol_mFB843BE6FF40B1BB28E809762AC1506160677781,
	SelectionRecord_get_ActiveCellRef_m5AB781F09226D96559B2C9FD3B6F0F2A87C15237,
	SelectionRecord_ToString_mC9E1970F3DF4994C7098FC45E11929EC1A5E3D95,
	SelectionRecord_Serialize_m4BDBF617CD3736E379289FD29C715F3B31BDDA7B,
	SelectionRecord_get_DataSize_mB9E7AFC76269B8EBB0851DBD8B689098DFF2E52F,
	SelectionRecord_get_Sid_m5567BA9EEE76F4B4F2EA7EE7E820A71E1715EBD8,
	SelectionRecord_Clone_m21152CEF9E86011F3EBB203C762FDFF741BD1C8B,
	SharedFormulaRecord_get_ExtraDataSize_m6BC7A9C252B246E5E1C7B3D2B1A2FF4845F2FA1D,
	SharedFormulaRecord_get_Sid_m9D1C282D6213948F86367CEF89C4B807C5801420,
	SharedFormulaRecord_SerializeExtraData_m30D906C37AC832CAC29257DE675BA7C10177CFE5,
	SharedFormulaRecord_GetFormulaTokens_m83DEFD61C4CCD5328DFC27B4287B6C6C863F4D7A,
	SSTDeserializer__ctor_m23D4D6855B13D2B96362EC12B8CA7325F70F8EE3,
	SSTDeserializer_AddToStringTable_m5544F1786FF31C1B4A0A18131F81BC0D73E4C78C,
	SSTRecord__ctor_m16E36A1FD6D724F1B666DA3EF05255F0AD309A53,
	SSTRecord_AddString_m2CEA4C37428D22640E81AC92CE07E92CBFD0B066,
	SSTRecord_get_NumStrings_m5706A91830E050B25675CB4492A3C08FEE1B0F72,
	SSTRecord_get_NumUniqueStrings_mD3BADD3D8F85E8427E289D936CE4470E0E18A476,
	SSTRecord_GetString_m4A94A3447156B44694D46DB7D293423B327CCA08,
	SSTRecord_ToString_m67F8610001F36CD03E11076518C6DD946CAF2E2E,
	SSTRecord_get_Sid_m5E27CEA8F35FA65D83B532C4F7817359BB9D270B,
	SSTRecord_GetHashCode_m1B05942A74649CA9FB45CF4A82D69051F8157098,
	SSTRecord_Equals_m8CD4067D2AEB2F6B9E4709B75E8CB2FB66E55AB4,
	SSTRecord_Serialize_m9B2B379597CEC52ED1BEFAD4C77A1209D9430339,
	SSTRecord_CreateExtSSTRecord_m9F271C4C21C42B4A1F141C1C6FC5BE636F2E0356,
	SSTRecord_CalcExtSSTRecordSize_m01EE3F383ED470DE31AEBC2725C71736700B1AFB,
	SSTRecord__cctor_m5722030B9CDF941C49DBB4FE38233F862F815745,
	SSTSerializer__ctor_m9519E155243E90CF84030CF2AEE895BB1EFB86D4,
	SSTSerializer_Serialize_mED4EF4451A564D1E7643C5E2961618A2DC946CF4,
	SSTSerializer_GetUnicodeString_m7F7AA5CCE7118425BB6938278F2D02BC3DDD21A0,
	SSTSerializer_GetUnicodeString_m7267C72068A9F1FC7FFDFADD483EB8DD76560C6B,
	SSTSerializer_get_BucketAbsoluteOffsets_m2DA8DF91F235063D07ABD937943CA7110A9BE14A,
	SSTSerializer_get_BucketRelativeOffsets_m1915B6CA02D34D1BFD4865CE341FBB4108AD7782,
	StringRecord__ctor_m7FA3D2DB7E7B354ADE10301DA892B62C1A0331EB,
	StringRecord_Serialize_mF1B4E60A643A03E3A72A162BFD3D2BD17358D5BF,
	StringRecord_get_Sid_m8B40120F6045DF7AFE7D9FC288B0508A52840C3C,
	StringRecord_get_String_m425EBF16927CCEC85DCC3EB3C8190A3C8644FC48,
	StringRecord_set_String_mD4BB8FE3FD99EF96F205D9C242A5E30478020B2F,
	StringRecord_ToString_mF3F36939DEF70AF993C1ED29D994C2DBB8D8243E,
	StringRecord_Clone_m6EB537D9C7DBBF7A29C80503AFD8E3FDEF33362D,
	StyleRecord__ctor_mF3D5E85475EB0336D9A32E351B748B2A842A46C1,
	StyleRecord_get_IsBuiltin_mDD815FC2A966DCF32AA4BD61D2F4304FDCEE8ED7,
	StyleRecord_SetBuiltinStyle_m720C42202F3868F87B402D364312FFB86E87E3AB,
	StyleRecord_get_XFIndex_m5D11B6BF3D1029CE33EB279CD7B15C6CE039EF68,
	StyleRecord_set_XFIndex_m42A56088B921969A1AE2E0695E0E9FAF45A8C9DF,
	StyleRecord_get_Name_mB71565F2051DA923E1E5360DC59EB9DD0A0E22AC,
	StyleRecord_set_OutlineStyleLevel_mF09F106E7FD36521154F6475EA830D3C6742C3F7,
	StyleRecord_ToString_mFB816A88104B97D23B53D4E3FDDCA70C26F26C37,
	StyleRecord_SetField_m09980269327CF33AA08FA9B6FAFB032A786223FE,
	StyleRecord_Serialize_mAC591FB00DBA4E8E1C7C17D440BFCD1F5C50C9D5,
	StyleRecord_get_DataSize_m6A9DCCB55C9F42590D554F58DA289F17AE83105A,
	StyleRecord_get_Sid_mD675A03B83D54FF68B8BF1AB88352D0154A8EE7E,
	StyleRecord__cctor_mE5C077996C01E21FA928012BE865CC9B6763937C,
	SupBookRecord_CreateInternalReferences_mE1D1C9207538FDAD55CAC4B9B91B144B4DF80922,
	SupBookRecord_CreateAddInFunctions_mE436D378B9C588DBE90312D909A29C968AEC6393,
	SupBookRecord__ctor_mCD9367D74AC28A8BF6C75CDADC3A7E90CFF8C17B,
	SupBookRecord_get_IsExternalReferences_m10DC2DCC19DAB338473CD24E71FE4910F8A6E7CC,
	SupBookRecord_get_IsInternalReferences_mCD9A73B2E33C1866AAB2384ED7D05DD15759ECDB,
	SupBookRecord_get_IsAddInFunctions_m82206CCD10ECD3DCD1FE030F1DA51AE2792C7D9E,
	SupBookRecord_ToString_mE9E52418C3A11067165FDA712B16AB7FD88CB286,
	SupBookRecord_get_DataSize_m70D309DDA8E31A978D478490F18DB9041AB45E38,
	SupBookRecord_Serialize_mFFC74830AC6C64435FEC9548C2A2CE3FBC02A688,
	SupBookRecord_get_Sid_mD7075157D4D490AB3B3C6A08F389D32435F290B9,
	SupBookRecord_get_URL_m13A855469DDAE1CD3F78428B7EDE14352D32D572,
	SupBookRecord_DecodeFileName_mB2B15C027A510B7518469542F3A3804D0859D751,
	SupBookRecord_get_SheetNames_m9596A1618289281D7CCF59408AE2FF3ECEB6CC47,
	SupBookRecord__cctor_m1521D4BACD4F498C86B80FA67F4859120F46FBB5,
	TabIdRecord__ctor_m86CA0ACF858380289C4F05CE1E48A6CA9B26A783,
	TabIdRecord_SetTabIdArray_mB6772DC98FAE9D7033C8FB6120D406F155648D53,
	TabIdRecord_ToString_m3BE0A5359422C6787C1214EB2779344D37501616,
	TabIdRecord_Serialize_mCD63B7430A66905E72B0BC605CE174B0F55134EA,
	TabIdRecord_get_DataSize_m0487C99B33D7062F82DF9AE6A040670D2E31C0E7,
	TabIdRecord_get_Sid_m0E4494940459494B92902B9959D68A98AA1EE1AA,
	TabIdRecord__cctor_m16F36EC45AC2FC884C302082190DE8B5E5448E82,
	TableRecord_get_Sid_mEB2024D21C41DCAA09998F8E0D7E6E0E9DAA2CA0,
	TableRecord_get_ExtraDataSize_mE47BFC867B05E658FDA125C730857CB6A345B633,
	TableRecord_SerializeExtraData_m02D0ED231A5AA1B45A60C52DFAD76AAAF3D31234,
	TableRecord__cctor_m22EE4D6C9F8070FE91FC06F96ACC6D9DD56C8435,
	TableStylesRecord_Serialize_mC55FC1F64AB59E3AF4CD5CE8E56754A4E5B5B1BD,
	TableStylesRecord_get_DataSize_m493CC60BBC24A6B4DC089FC74793C5EAC6FC15D7,
	TableStylesRecord_get_Sid_m243282FB5116A7705A620058F66A89D7E24538DB,
	TextObjectRecord_SerializeTrailingRecords_m7DC645337938509CEA1855E829FB1F3497050D6A,
	TextObjectRecord_WriteFormatData_m7A66B2B0D4185C6950AC7E5EFC3E6B5E2060C8B0,
	TextObjectRecord_get_FormattingDataLength_m4B02F69C40173021DD918AF1247BC96D13A975AC,
	TextObjectRecord_SerializeTXORecord_mFF33259EDABFFEAEFE1D9AEEA6EE8711E759C7B3,
	TextObjectRecord_Serialize_mD6BD621BC31A3182A53F31009C2BA01745283B0B,
	TextObjectRecord_get_Sid_mC3F4D5E96731BBD9CDE85CB29CAD55FA885CE930,
	TopMarginRecord_Serialize_m7C87F6F6383D3058493F37C79E9675BC45FF6F9D,
	TopMarginRecord_get_DataSize_m16506D5E2C80DA69C9A4EF797BA6D9A8B51BB90F,
	TopMarginRecord_get_Sid_m03550DD863A55529DF2D4A99DC8792BC6B32B696,
	UncalcedRecord__ctor_mDBD38C2B1A3A2A287E0C85C1050B341B3CF58D89,
	UncalcedRecord_get_Sid_m4ACEC0F202F1DA0636355B24C3D323CE65D8736C,
	UncalcedRecord_ToString_m160B4DBF9BB2B092052B3F8BFC292675541F97DC,
	UncalcedRecord_Serialize_m0CFEE37AF03ACAAE4386BB5ECFEFFEE7A72269A3,
	UncalcedRecord_get_DataSize_mDA4C820E27BB19D081508DB573BD8D73F8A9D7A4,
	UncalcedRecord_get_StaticRecordSize_m0C119B7696D8847CC8EDAA5B2615A62CCEA6C4B9,
	UnknownRecord__ctor_mC1AEBA1253D120FBAD09307F93039677DFCA7422,
	UnknownRecord_Serialize_mEEAC7AFF3F8E70DDA272763068D061D1E7F6418F,
	UnknownRecord_get_DataSize_mA543475F50A7E0E4C5DBE2EEF54B6B5B28E31034,
	UnknownRecord_ToString_m9936256AD4CDEC4CCBD3E75FDF35D7FDEB65CB97,
	UnknownRecord_GetBiffName_m7208D968F0BA424F6C448A705A572342DC2F6CB3,
	UnknownRecord_IsObservedButUnknown_mC0F63D37A69AB5021BC60007378DB62A414D3D9E,
	UnknownRecord_get_Sid_m9C6D7133C649CDE1B03A15C556B599B1B1ABF232,
	UnknownRecord_Clone_mDED682E1B3CC57C94499FF3DD133A4508B7834BD,
	UserSViewBegin_Serialize_mF995472A0E2B43D0AA2CF54C558D50E5C3A7D426,
	UserSViewBegin_get_DataSize_m94B6920AA76DFC94FC167239CCC01F5F105A3AA9,
	UserSViewBegin_get_Sid_m90AB68816D708D9170B096BAC89F960A1BEA9736,
	UserSViewBegin_get_Guid_m3F3D1F152482F1074EB21C871CECCA00242A3567,
	UserSViewEnd_Serialize_mD1D3CCD1B942B9D9D6CBAD7E0D2B9B757F9CE8FE,
	UserSViewEnd_get_DataSize_m4D9FA452DF1C33372FFADAAB4C4B07D12FCCC4FF,
	UserSViewEnd_get_Sid_mC54E0376BE432613B5D2FF6C29474456F3709885,
	UseSelFSRecord__ctor_m062180E5DED5AE2B64CE0582410A9D91C67F728E,
	UseSelFSRecord__ctor_m7653FFDEC53F5E05F4F2CC4B183BEC9F4167A1D8,
	UseSelFSRecord_ToString_m0376E93F2B5D7DE7A584A2076EBE64A7F3C08A49,
	UseSelFSRecord_Serialize_m6070089CCBB6AAE62A486524DE446311FB165CC2,
	UseSelFSRecord_get_DataSize_m3DDAB77262CFBEE9A0BB4E5B550E099BE328F97C,
	UseSelFSRecord_get_Sid_m619AD03C0B6E1656C14D127D4D0548DE6EA2118F,
	UseSelFSRecord__cctor_m4A87FCA2EC681EEC23E684CAA03EE8E59655770F,
	VCenterRecord__ctor_mA27B0E9DC294B9215F99FA870BDFB64DBED4C3D3,
	VCenterRecord_get_VCenter_m5F4EB5D310F64E02000B7F4C45CD558B3478CF7A,
	VCenterRecord_set_VCenter_m00211E31B9F1697A3E80A977CA9C4F5F6ED625B5,
	VCenterRecord_ToString_mF04DAA820C6C7C6683599FD6333E4A26B823AD01,
	VCenterRecord_Serialize_m7B05021B44A0B1BCBA4504D41599123CF6C3AE48,
	VCenterRecord_get_DataSize_m4DC8B2E7164303711D7CDD683FFA058480A29467,
	VCenterRecord_get_Sid_mDEABFD542CB62F9080E42864E767D489F33C94C4,
	VCenterRecord_Clone_mEA5830243D0C6BFE18B1BFD85F757C0C7536B58A,
	VerticalPageBreakRecord__ctor_m74A845B1EFC5CAC1E0637FACF9CA8654FACC0445,
	VerticalPageBreakRecord_get_Sid_m4A5E0B55272EFF5AAA7725B17EEEC9EDA364B547,
	VerticalPageBreakRecord_Clone_mAC6958EF6D928B5C68413002F798D1DFA3F656BE,
	WindowOneRecord__ctor_m9FD43904F6705267858AC46C9136BBB6BD38BD99,
	WindowOneRecord_get_HorizontalHold_m4B887AB2E155B79E253927F89FC82C6CEE363399,
	WindowOneRecord_set_HorizontalHold_m02861F7B478F4EDF68711521D9A21847293CE843,
	WindowOneRecord_get_VerticalHold_m25AA5233D03E779892424743CA553503C64B25D9,
	WindowOneRecord_set_VerticalHold_mF7A919B38E683DBA8C64095040A8703BA7CCAC5B,
	WindowOneRecord_get_Width_m60EB2A2D963B3B4D2DD451A4DE81CD20DC50EF9E,
	WindowOneRecord_set_Width_m3C248EDC7943DA48722F06C9C192748CADDCF19E,
	WindowOneRecord_get_Height_mD19FD203A60DDBF3A83BBDCAD8059884325908D4,
	WindowOneRecord_set_Height_m6F4D0DC2AA316B638CF4FE0876485E6FC797EEFB,
	WindowOneRecord_get_Options_m9665626BEFF57F892BAE35BB39DC362A30CADCD4,
	WindowOneRecord_set_Options_m99057C6BAA284B369A9D5BFBA43388B68799C204,
	WindowOneRecord_get_Hidden_mB40D2DDB2562946A9406220C03737FBB8F2C5FD1,
	WindowOneRecord_get_Iconic_m157C4213AADD11D4401E9DA510153C019D2335A9,
	WindowOneRecord_get_DisplayHorizontalScrollbar_mA0DFA8216A026F4EC547CE9DCD7310DB372B07DF,
	WindowOneRecord_get_DisplayVerticalScrollbar_mDC9C76D4F2D4B1A269CBBF0CD5FCA27BA23D7541,
	WindowOneRecord_get_DisplayTabs_m1435A9014F3C786CB4ED6830EA364A927D47E2FD,
	WindowOneRecord_get_ActiveSheetIndex_m5977C93DD6A1700792116C86DDBE7DB4809CD58E,
	WindowOneRecord_set_ActiveSheetIndex_m7F28D8AE0298B8465E5D7447C99B87CA5B72673E,
	WindowOneRecord_get_FirstVisibleTab_m8D537304676907ACA3B0C7625420C458DF916664,
	WindowOneRecord_set_FirstVisibleTab_mD0D172C34056D56A4F642D8F077BFD7FE0D88FF6,
	WindowOneRecord_get_NumSelectedTabs_m839B69AF7ED308548AC06AD6C4488FC2BEE87DD8,
	WindowOneRecord_set_NumSelectedTabs_mC2DCE1E7036E3E2247FF801BF4F526DC59E56F87,
	WindowOneRecord_get_TabWidthRatio_m7799D82BA8F1AEA0AA63F06206A35E8F5F6348FE,
	WindowOneRecord_set_TabWidthRatio_mFE1937D7EECBC1EE03D5470ED72412DFC988959B,
	WindowOneRecord_ToString_m545B713CAD638871BE30A459A3E5ED9DEB0411AC,
	WindowOneRecord_Serialize_m1EEAE7DD84EC16198FD90FB0B7D5F048EF88F308,
	WindowOneRecord_get_DataSize_m819E03954B090FC750AF8F324C07E54F8FE457CE,
	WindowOneRecord_get_Sid_mFAC78172FB45323FF0F0525031C1327E40644B18,
	WindowOneRecord__cctor_m0FC15A9BCB748EAD2A5F3AE3A314A12775E9AC8D,
	WindowProtectRecord__ctor_mBA21EEA0017A2C4F71C9926FB51B38E1F172688A,
	WindowProtectRecord__ctor_m505838284CDDC340810CCB6E2280F13B05C63A50,
	WindowProtectRecord_get_Protect_m099F28916E7C03F65871FE8CCB2347032456DD4B,
	WindowProtectRecord_set_Protect_m261C8D9A225C85CE0107EF75BF7E987A38C8631A,
	WindowProtectRecord_ToString_m213070F7BBFF6B7D7D42978CFB1918776C2F44AA,
	WindowProtectRecord_Serialize_mE997690B1601B3BD5785C8CE6763EFA31FE42E75,
	WindowProtectRecord_get_DataSize_mE546FF7A3487D58675C63C8C67B7EFBE9AA5C269,
	WindowProtectRecord_get_Sid_mE4EA6C98BC17083D4B2FB631367FEF86716A7F6F,
	WindowProtectRecord__cctor_m84E1D107EA41B2CE325F3847F331508577FEA859,
	WindowTwoRecord__ctor_mF307C8BF23F18E7B35642DB13D9CEE3950A6F2A9,
	WindowTwoRecord_get_Options_m50E2BEFE97961752A0869563277F810713C21E4F,
	WindowTwoRecord_set_Options_m03AF67CB3A84EFFA8C1CF77AC99E713723CF025E,
	WindowTwoRecord_get_DisplayFormulas_m405A1FC3069FCF75AED96EF63E855C9ABD957806,
	WindowTwoRecord_get_DisplayGridlines_m87D28B47AA9D2B363ED573DB3E9762BD679BDC85,
	WindowTwoRecord_get_DisplayRowColHeadings_m9E0B85E6EF005542ED85B69C7E0D0236AF602A97,
	WindowTwoRecord_get_FreezePanes_mB12E34C184DD22FA3DD5DE647F3CE3ED1D4DCCC5,
	WindowTwoRecord_get_DisplayZeros_mAE698D4F191488FD2BF9DEE1F482D3AB005ED781,
	WindowTwoRecord_get_DefaultHeader_m40DFD00D06C56581898734178353B294DF679D08,
	WindowTwoRecord_get_Arabic_m24983272B7A9B949BC46D3B80B0D67370B907C31,
	WindowTwoRecord_get_DisplayGuts_m371B14F695A5863708384FE272DD1FA64EB89F46,
	WindowTwoRecord_get_FreezePanesNoSplit_m20E7EE102563EAA78F95576D71B935EB6E6056BE,
	WindowTwoRecord_get_IsSelected_mAA96C37F90D63025CFC4FE7F941CC7F57F3B991E,
	WindowTwoRecord_get_IsActive_mB38587CAC3DF3E6DE1B3691ED6E55AA26539A58A,
	WindowTwoRecord_get_SavedInPageBreakPreview_mC5D6B966956FD98B2411F82A0B54FCDA96BCCE72,
	WindowTwoRecord_get_TopRow_m2358AE76E963BAAA5D6817DBDD24BF9BE38CEEB5,
	WindowTwoRecord_set_TopRow_m73AD89D570353ADD70AE3CF9F014F0D3714C68A0,
	WindowTwoRecord_get_LeftCol_mBD84FB3893515F11D0220B4BD646C1DEDECB213B,
	WindowTwoRecord_set_LeftCol_m31A261D93CBA6AE9F669C29510B053D4CF47C7EA,
	WindowTwoRecord_get_HeaderColor_m22D6FDEC4BDA37DFB9DDA12FB1440E15386DE68A,
	WindowTwoRecord_set_HeaderColor_mAB8F660F4F49EFD20CB0148BC5C86191FCCADECA,
	WindowTwoRecord_get_PageBreakZoom_m93C462F2E106590786CFD74F35AC4BCCDFF5CC0B,
	WindowTwoRecord_set_PageBreakZoom_m9223B363D4F65C85589FF82377B3317442353692,
	WindowTwoRecord_get_NormalZoom_mE00C045D17926A754131F27EE16AE16BFBD6F98B,
	WindowTwoRecord_set_NormalZoom_mA9194DDF56D05456D47D2DC1F5E557453599D748,
	WindowTwoRecord_get_Reserved_mA697746E435CEF203C7A80D8477BC71B307211F9,
	WindowTwoRecord_ToString_m13B381012570F173649A5A4C547A86CC0F62722B,
	WindowTwoRecord_Serialize_mD2CEB7C5E5196D0704780FFF917F07B97ECAD49C,
	WindowTwoRecord_get_DataSize_m923E608C332653109EA18A9EBDF8F30793510BF6,
	WindowTwoRecord_get_Sid_m9A1111ADCE7FBB52CAF27357A91B46AD3D1DCCCB,
	WindowTwoRecord_Clone_m437622D4BE07D618C1F60430A7C32A8657B814AD,
	WriteAccessRecord__cctor_mB1506226A35C6F91A287A61256D5308D7EFF24BC,
	WriteAccessRecord__ctor_m5E713DEF127E3CC376BAD835AC369E8C2735F201,
	WriteAccessRecord_get_Username_m8FD5EA280BD6746FDEEF0F14B804BB1FE9890735,
	WriteAccessRecord_set_Username_m5B73FC6F60FC62ED2EB366C424D5499DB9321AB3,
	WriteAccessRecord_ToString_m8D6E2BF68290426F32113C0DC4BAD34238B2F971,
	WriteAccessRecord_Serialize_m85D235C351B1310CE6360261D2663E8EC47E30FC,
	WriteAccessRecord_get_DataSize_mC4D129D8751D323DCFB8C5CC701B830511BE547A,
	WriteAccessRecord_get_Sid_mDA4A5CCF267BE09F1CC937C680AC33C4A570D771,
	WriteProtectRecord_Serialize_m80AC514AD113688F2B22F88726791430D5BAC5FB,
	WriteProtectRecord_get_DataSize_mCCAAA7A44995D7603021ACC456284B53DB72C9B5,
	WriteProtectRecord_get_Sid_mF56D38B6BF711C34EA08269F869FB267F643BBEB,
	WSBoolRecord__ctor_m4AEE3B82DAB5CF62EC7BF6D2C3FC68CA24E4A4CF,
	WSBoolRecord_get_WSBool1_m643F14C06361EA86A65D0D3EC494208AFD3160A3,
	WSBoolRecord_set_WSBool1_m9CB4A3EA7C9C8D00EDB7BBC48A8F8655D3B52D48,
	WSBoolRecord_get_Autobreaks_m8748D741738775EA853115AE872EF7DFA77F37D2,
	WSBoolRecord_get_Dialog_m5BF0FCC6CFA3ADC74321583E826746131D056437,
	WSBoolRecord_get_RowSumsBelow_m6F5252F847DA1BE6724D0779D8BE807BDD4F2E67,
	WSBoolRecord_get_RowSumsRight_mB89FF659909FF67756B9D31C47D4A4C274444574,
	WSBoolRecord_get_WSBool2_mD289102389BC268ED6FC9784686655D35D02B446,
	WSBoolRecord_set_WSBool2_m616F6D699A6DC1B13B31F8749CF512C1EC4551EF,
	WSBoolRecord_get_FitToPage_m25D1A56EAC89130580FD616903675261F5BF9E9A,
	WSBoolRecord_get_DisplayGuts_m972E1436A569E207CF2101F3F00B0FEB772D1BA6,
	WSBoolRecord_get_AlternateExpression_mDDD87A1A79184BFEF40661972EC85C8D03E0A5C4,
	WSBoolRecord_get_AlternateFormula_m766A709539DD7AA62BC52EFFE062B0B8C6F00786,
	WSBoolRecord_ToString_m7A9FBE687C5B2EBFE7A24027AC9E54424B7D61F2,
	WSBoolRecord_Serialize_mD764A178210C1C68BA3D7BF62F9F775D77B38527,
	WSBoolRecord_get_DataSize_m147686A1DA646CC381C2AE3C7F8EA0D35A8156E2,
	WSBoolRecord_get_Sid_mEAEAB568FFA93A1B3735B91BB33270BE07EAE132,
	WSBoolRecord_Clone_mA88A50C9727BCBF57FAACD92D3F2D2B2B30937A0,
	WSBoolRecord__cctor_mAD7A81DF4F5A8030BDD69280093D1000B74E61A6,
	HSSFErrorConstants_GetText_m173B74E37189DB1BBEFF369F4DB084E1346DEAD0,
	HSSFErrorConstants_IsValidCode_m97CEE3E59FAABD51E727A6B02211E900EACE8CCB,
	EncryptedDocumentException__ctor_m5ED29DC71BA3B6D3B245FFBEEDDD185252626CF5,
	POIFSBigBlockSize__ctor_m941483271C02CAF5EC7723419C0FEAC8C4A26AFA,
	POIFSBigBlockSize_GetBigBlockSize_m1238D54BFE455B9A814D0D51D1D58880E59DB9C4,
	POIFSBigBlockSize_GetHeaderValue_mD69BB61FA1154A55170C31A8B284A8F29E0C773B,
	POIFSBigBlockSize_GetPropertiesPerBlock_m6F470C0D7C61AE19AA8CD6777E3B476A82DC642E,
	POIFSBigBlockSize_GetBATEntriesPerBlock_m42D4E077C529FEB8C02C93E7A9D9E7AE02DBF693,
	POIFSBigBlockSize_GetXBATEntriesPerBlock_m5425AABA29DEBD7200BBC19BA8ED32645DB1157E,
	POIFSConstants__cctor_m9A3547AF887390BC5439DB7368FA9A2141FE83B2,
	SharedFormula__ctor_m982F3B256178C2D3100F744FE5523210BF8D4A36,
	SharedFormula_ConvertSharedFormulas_mB7424E05E61F0C01C07C90FF6A98260566F9968B,
	SharedFormula_FixupRelativeColumn_m41646512F1022D96775F4D543334827A78AF4527,
	SharedFormula_FixupRelativeRow_mE66E08FCE9B8F33FAC80A781110E8C19605AF215,
	ByteBuffer__ctor_mBAE7DF973D9D315EF8EE56668C968BF536979C0D,
	ByteBuffer__ctor_m64EA2B693DE61F693BF69927CEB4B6642D8C99A6,
	ByteBuffer__ctor_mFF95E807620201E6EA297F95FDF9C0D4711BC890,
	ByteBuffer_get_Position_m1EF103055C90545A15AC5E1B17F4DD02522C2A34,
	ByteBuffer_set_Position_mACC17578EE11C5682DAA9ADB37646ADFF9FAFD80,
	ByteBuffer_set_Limit_mB6F25797AF55368B3632742F58DCA574B7ACD683,
	ByteBuffer_CreateBuffer_mC445B1033AA61F668EFE02AE004FBCDFE74AFE66,
	ByteBuffer_Slice_m83FE191CEDDA230F38243250D5087A53296EECAE,
	ByteBuffer_Index_m6CCE3CFE8421DF1E3DAA3924AF0B58B8D8A6B96C,
	ByteBuffer_CheckBounds_mAF9F72DEAAD77B21C3A7189C00D4EAC49528C787,
	ByteBuffer_Read_m355BDDB37B0F610F19AD02C240203C6EF1BE5E89,
	ByteBuffer_Write_mE92A3C351DB42C1F755F65F2494AADCF3D0DB873,
	ByteBuffer_get_Remain_mF87693658AE333D9FD36FE66E7575BFE5B3E15C0,
	EntryNode__ctor_mB598C29F5A1EDA1D29F00F82AEEA140C1858F5C2,
	EntryNode__ctor_m78A992885F76BD1BBFBC1B0813C07EFE97BA3F1B,
	EntryNode_get_Property_m24E0588F36F0CDA0DD10E00657CF2735D4F52131,
	EntryNode_get_Name_mD693C74C073ED389B0AC037F2647EB0B2E7F491C,
	EntryNode_get_IsDirectoryEntry_mEE8E4E6FDE66E9794462490D529972F0ED7C6AA7,
	EntryNode_get_IsDocumentEntry_m1A72880BBD6271C5D831A83B8D404336442D0447,
	EntryNode_get_Parent_m4673D9C1205FCE2409B3CF16A8C251C929697B75,
	EntryNode_ToString_m1AA72E8347B43DF787805AC4743F3317D474D6D5,
	DirectoryNode__ctor_m708AF1375EFA72F1AF9052C8D7984A560BF852C9,
	DirectoryNode__ctor_m99A5195C48220AED28690B47426027CE0CE56763,
	DirectoryNode__ctor_mB282A68FE4985297C30D6872822EB0BCEE04201E,
	DirectoryNode_CreatePOIFSDocumentReader_m628FA405B8B8401DFE4FC2D3548AF59E361AF0D2,
	DirectoryNode_CreateDocument_m80178B350D03207926E04D34B5B6DA86055AEBD2,
	DirectoryNode_get_FileSystem_m3388C0146EC2F5B9E148CCC74E0EE68C3BB0BD30,
	DirectoryNode_get_NFileSystem_mBCA0456C5716112121B5E8D64C6CC1F1ACD6A175,
	DirectoryNode_get_Entries_m1A79A038D72684FF76A792548DF69581BC8432CD,
	DirectoryNode_HasEntry_m29F0CCABEFC53F8AD58EBB74433E4A5FBD5C3018,
	DirectoryNode_GetEntry_m2155284DF1F9132E61FF9F574C5EA90540119CA8,
	DirectoryNode_CreateDocumentInputStream_mDF3358E640B4148AA36F6092D630098CA287979D,
	DirectoryNode_CreateDocumentInputStream_m53A086A8E9F9CB1B4778B8A7FC2481489BE20967,
	DirectoryNode_CreateDocument_mEE5A09468FE1CE6945D4D9CB3440CF2019F8D0D6,
	DirectoryNode_CreateDirectory_mC8A4F9EE26A7CBD29CD236B2D231F7A3E5121710,
	DirectoryNode_set_StorageClsid_mFA873F379B4334B5F249D2D86770959D6D64F3DD,
	DirectoryNode_get_StorageClsid_mC392CA7D38A44739DD16425D77C40A8F4CA78BC6,
	DirectoryNode_get_IsDirectoryEntry_m71B978B987B2EE1F7AB0081D406775FCA5E989E8,
	DirectoryNode_CreateDocument_m1CC16312081815C345C02233CCA5E10130359311,
	DirectoryNode_GetEnumerator_mB115C755708D2AD9B526A019A694EBC7E58CB8F6,
	DirectoryNode_System_Collections_IEnumerable_GetEnumerator_m9559207752BC635B24FA7938197729DA1E820936,
	NULL,
	DocumentNode__ctor_m877CF47AF5E3BD72DF778DA5476D17CBC7AD3C50,
	DocumentNode_get_Document_m604883E1298373E3BDD0336F4812ED6D7DB97609,
	DocumentNode_get_Size_mF0748B1C79811DD4CDBF268F4A1BF1B08E6052E3,
	DocumentNode_get_IsDocumentEntry_m5C0C4FA783F5D741F2CB6ECD653C51DDA3F64ACE,
	OfficeXmlFileException__ctor_m6C5239A7DC157B13A826848BE9B7CACDC3DCE1DB,
	POIFSDocumentPath__ctor_mA91A0B2B413D21705B9BA014757A0E15FF02FD41,
	POIFSDocumentPath__ctor_mDB73EBFFD154EA43A63FE8C3DCDB68F937D6BEF4,
	POIFSDocumentPath_Equals_m8359FB95C97010EA408CFC578946EFB63AB862E0,
	POIFSDocumentPath_GetComponent_m076D1638FEB568242B7A743983094CAAFA0C3754,
	POIFSDocumentPath_GetHashCode_mF410DFDD36D4F6DD1C3362E622726C0AA42A83FE,
	POIFSDocumentPath_ToString_mE9BFAF25A547CA404211C8B89B4DB4A2E35C552C,
	POIFSDocumentPath_get_Length_m3D00C8E3C1DE6BC2DCDB613F251740EAF47A14F8,
	POIFSFileSystem__ctor_mFAE900696D6FDEC95EDE0613C77BE938D9FF9FDD,
	POIFSFileSystem__ctor_m6EB4266E056251BA5C8511572EE21E8544A6144B,
	POIFSFileSystem_CloseInputStream_mB587B555736CBE5BCBA3DAE4A41D82D841F498D9,
	POIFSFileSystem_CreateDocument_mEAC0A0C414E6786573E78FBEF6FAA794A8F66F68,
	POIFSFileSystem_WriteFileSystem_mF36FFED01EAD68EE785C0021F059EFD00AC3A8DC,
	POIFSFileSystem_get_Root_m783D1C8275C25B99CFA8304D9D44D2CA04FAE239,
	POIFSFileSystem_AddDocument_m5FFA495BFC15BAE665D977E0CF18C4883D20E482,
	POIFSFileSystem_AddDirectory_m5781A31B23C54108D8D270D3BBC81BE8F98BB803,
	POIFSFileSystem_ProcessProperties_mE32D2E36601D8B441FDDD5801ED880EFB574C3D2,
	Property__ctor_m8657E0559C7F21E3593201E52D5CE47D248DCEF0,
	Property__ctor_m7949074CC4CC4856FC980A27184CE6D661339A66,
	Property_WriteData_m29E292BD481367EDFFF8D678A620E15F0602D4E0,
	Property_set_StartBlock_m439EE7F1E97A2103F05B320CB9ED762C7B7431AA,
	Property_get_StartBlock_mEC80A0657D39D9839B3E424179565D124502D804,
	Property_get_ShouldUseSmallBlocks_m2E4402BD019A3646D8D3A4C1F124A773A2FE09CE,
	Property_IsSmall_mA4E30AEE4EE03474D7836BBADF763217F4F202A8,
	Property_get_Name_mD7F7E9D59F76B19BD6487DF8B81F5C1650030201,
	Property_set_Name_mF3B2FD44200B19B963D14AA774933E4F19F5AD36,
	Property_get_IsDirectory_m87AFD49524DB209F8C493B426D0C9110A1CD8F54,
	Property_get_StorageClsid_mEA65D8DD731820FEBCF050D6C2D5841D090A4FDE,
	Property_set_StorageClsid_m018FBECCF069744EF0FCBBD758C9BAD5371F4F98,
	Property_set_PropertyType_m10B85E586220B70789FDA33EA3E5B90297A8D561,
	Property_set_NodeColor_mC1462B906E2AB84B44BB56675FA6E4ECB2336B07,
	Property_set_ChildProperty_m42E58929ABAFA47044FEDF40205E421F93FD93DF,
	Property_get_ChildIndex_mA005CA5EEE877D6852FFA50FB7DAFA5F55A86B0B,
	Property_set_Size_m4C3ADD05CB159D466D4DA61E6C11484C718A4D44,
	Property_get_Size_mE9FC6CB91CBCB1CA0A51AA050B0AA9ACE1F56876,
	Property_get_Index_m4405F35D7B97C436A806D8AC34045A2715FE9765,
	Property_set_Index_mE6718D7F70E12FC3FE2438031DDE8D11C590539F,
	NULL,
	Property_get_NextChildIndex_m6FD4DA28644C11B5A66CE3917A464C3E1C61D22C,
	Property_get_PreviousChildIndex_m31B535A952F1E2091CB227147FFBB20B775F52FF,
	Property_IsValidIndex_m641E1DC5B3A9B4CEC1E376EFE097EBE2DBF4D622,
	Property_set_PreviousChild_m8A6E951A89DB067F236C5521B62113828A63D576,
	Property_set_NextChild_mF2480C4A679D01A3AF41D6A92AF7F2633B8D7119,
	DirectoryProperty__ctor_mD6F9C620C163D495AFD4AB1DC8E33F63165E9A85,
	DirectoryProperty__ctor_m600A822FFBE910581FB0A35A5A47E3107C811E85,
	DirectoryProperty_get_IsDirectory_mA43F8D714491E4A6880E69372E63F040CD8F1CB5,
	DirectoryProperty_PreWrite_mF2EEB20489102682E1EF8665D7B10C1A6F62BD7D,
	DirectoryProperty_get_Children_m23D4FD4FA50BB5AEA091F8A5D758293E0EFDA4C7,
	DirectoryProperty_AddChild_m3146C1D71EFEBBE5D50F1FD1750CEF3CD18181B9,
	PropertyComparator_Equals_m17AFCEE9CDCCAE7B3DE3113245A911161E962947,
	PropertyComparator_GetHashCode_mD1A8F9ED9AA5EF0F26DC617AD3CA0C92BD0A0784,
	PropertyComparator_Compare_mA86CE137D6823E5AAF676C12F81D2401C58A0174,
	PropertyComparator__ctor_m148777A490A62E18A3913546C102BD050F6CB2BF,
	DocumentProperty__ctor_m510F09B94E66D8BA02303910ADA21735D8B3308E,
	DocumentProperty__ctor_m3094C2516BE0C2846C9BE39DF44ED978A5555C66,
	DocumentProperty_set_Document_m762713A71F0601527B74669B8A03FCB23E04032C,
	DocumentProperty_get_Document_mFD4B7BB75D5786A69B0F4B7910709E9FA10643CC,
	DocumentProperty_get_IsDirectory_mD5E70A2A1A595DC4B73B21CE0A9B93D724411A75,
	DocumentProperty_PreWrite_mD2A2413E4228011C42B0B5456047CA1C62CABEDE,
	PropertyFactory_ConvertToProperties_m9602E49750159F73D8ECE2736C87F7CC2AC1851B,
	PropertyFactory_ConvertToProperties_mC362ADC25BE4DCB54F923719E76644C42ABDD15E,
	PropertyTable__ctor_m9C4D70DCFF7A923591A128ADD136B5BD8D60DF1E,
	PropertyTable__ctor_m293E09537AC1DC36E5385E2F32651BAF90E50D23,
	PropertyTable_PreWrite_mE48067B42EF23FEC3E29CE155F40FFD6E751183D,
	PropertyTable_get_CountBlocks_m5768A5D3D174F04DE111FCAE5110F0B8A8074DDC,
	PropertyTable_WriteBlocks_mC76A5A710C178A358D7E5C09FC04D15EED8D7FFC,
	RootProperty__ctor_m2FB5A68B2CE7C466317C900D08FEBFC3B1AAE5A2,
	RootProperty__ctor_m3502117E479CF549431CC14C18B1BF2686021F16,
	RootProperty_set_Size_m561DA208E7010E59B354F9BEFC64DB5363AD9485,
	BigBlock__ctor_mC61987309864F917FCB4537A01722F365DA10404,
	BigBlock_WriteData_m5FEC7B01B448077465295A51541A9A0CDF43C435,
	BigBlock_WriteBlocks_mE689E5C36B06C81B537B7AA305F5F55DE0BB30FA,
	NULL,
	BATBlock__ctor_mE4E2D5A81FED648232CC661FE9F8A85D2AE879CD,
	BATBlock__ctor_mAF27AA8CD2600D7CB90CB29F237C8B1767FF7F45,
	BATBlock_RecomputeFree_m41CBB7F013C62E6C931FA895476FFA9B8F5238BE,
	BATBlock_CreateEmptyBATBlock_m72BC5BC5CEBD9818C68D6E0DCB2DB3B29CC3738C,
	BATBlock_CreateBATBlocks_mC7049A5D268325A533FF9BA634693F5A98178C9F,
	BATBlock_CreateXBATBlocks_m0B64A46AB634491A8FF9AEFA5FF0D38C7B95374C,
	BATBlock_CalculateStorageRequirements_m174942B14CAAE12EBB339E2EEAE91A5B4A54A3C0,
	BATBlock_CalculateStorageRequirements_m7BD10E5B3B042EE4BB27C67158D911AFF3C54CA9,
	BATBlock_CalculateXBATStorageRequirements_mD449F07A493FFBF422F89151AB8CB866E643BB5D,
	BATBlock_CalculateXBATStorageRequirements_m262E6E39F21C318842CA765FC39B7354ABC3AE0A,
	BATBlock_GetBATBlockAndIndex_mF8C64079874CCAEAD465EBC1187D847E1F39FA14,
	BATBlock_GetSBATBlockAndIndex_mC378B63F1FD8BA42F0926B9B1F2441870B0E795A,
	BATBlock_get_EntriesPerXBATBlock_m4C88DAA546A7473F1108024C7CBC6B42D6C588A2,
	BATBlock_get_XBATChainOffset_mA827A9C48F83346EBABD9C5E85589247D100A7BC,
	BATBlock_SetXBATChain_mBD50E79225A280C5B696AF2E67FB46C06F4B8F23,
	BATBlock_get_HasFreeSectors_mA5301DC3967DF26330836F03D8AF52223E378D9F,
	BATBlock_GetValueAt_m0BE433CF60C17A5553FB0DF0473D740C2AD825ED,
	BATBlock_SetValueAt_m92AE0B057F56B14654D13DF7BDA040EA87015A41,
	BATBlock_set_OurBlockIndex_m0DFD350F4507D31923591A5DC6A7B01E9E07368F,
	BATBlock_WriteData_mBF18825801725760B856609A2C1BC59FC757AECA,
	BATBlock_Serialize_m439CFCE7D43D411F5E7BE5F2E679CCA4DF5BF47F,
	BATBlock__cctor_mDCF64B219356C06CDE791FABD593649395DC0991,
	BATBlockAndIndex__ctor_mF371ADBB61FD841A39CFF01B8387AAA6A5FE8533,
	BATBlockAndIndex_get_Index_m140BEB2245C36B36029EB33A93813B4580EE20FE,
	BATBlockAndIndex_get_Block_mF94565940AC9329BF8AA94948CEC98BF393340FF,
	BlockAllocationTableReader__ctor_mAFB85118F9ED0DE905D8A2847D315F1B6C352B7C,
	BlockAllocationTableReader__ctor_mF2B56AF5F54D72C16C62C3F8C6C2A7461B6BDA9E,
	BlockAllocationTableReader__ctor_m12EE3E7E87FE61F35DAB1C1C96118DC11ACBE7A7,
	BlockAllocationTableReader_FetchBlocks_mB7A5F1D05B7364E8748B646333A255D0429AFD03,
	BlockAllocationTableReader_SetEntries_m45D3EB6F2AE9C10B7F0B6C2DECD24E750B35BDE4,
	BlockAllocationTableReader_SanityCheckBlockCount_m6DE2743A0606FFD3DD87E1AB579A4BF742BBFD19,
	BlockAllocationTableReader__cctor_m78A89416FC60DBAC24E99E4447988A5887DC09CF,
	BlockAllocationTableWriter__ctor_mCCBA267CBBF9369DBD1B1A76921AD6DF46EDAF68,
	BlockAllocationTableWriter_CreateBlocks_mBC2E4208F82C51FA9648E185FA4619D2310B0BF5,
	BlockAllocationTableWriter_AllocateSpace_mFC4F53BC1C4A7C84C79C61D6D9E79DA7CB03EAD8,
	BlockAllocationTableWriter_get_StartBlock_m6014885C781BD4EBE912EAC5A5AA5CD6B8710F76,
	BlockAllocationTableWriter_set_StartBlock_mB894749FB0F18356160A2B68CF189803FADD5E39,
	BlockAllocationTableWriter_SimpleCreateBlocks_m2DB4A302398C3DC274FAB1AF562F989CEFD42DC0,
	BlockAllocationTableWriter_WriteBlocks_m2DAB38FA9825B2591828FFA48DE1F6D6C4388FB7,
	BlockAllocationTableWriter_get_CountBlocks_m2BDE57BA942C5531B1177DC524A9F6C1295A4EA3,
	BlockAllocationTableWriter__cctor_m12F55A699956B62C3372D9B187603A7037ECEE74,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BlockListImpl__ctor_m1F827653BE601F11961575C85ED2B8442F54026A,
	BlockListImpl_SetBlocks_mBFE1793728373C14C1E97821FD19C0EB55283547,
	BlockListImpl_Zap_m4EC485BCDC34D76F850AD86AC711893E0DB41898,
	BlockListImpl_Remove_mB0F6F17F2B7F4FFCD8C61392486061DE340389EA,
	BlockListImpl_FetchBlocks_mD7E26290391BFC4823E688C6A7BC183021F35AA5,
	BlockListImpl_set_BAT_mBA1470DD23418F76DFA811C91B584BBC4C24A085,
	BlockListImpl_BlockCount_m83969BAB5416B5AB89D9D3895461E2BBA521A5C2,
	DataInputBlock__ctor_mA95EFA66395EC6944D19962516B0530982F2E270,
	DataInputBlock_Available_m623AD20DD1F71F65347A6071CE451806FB23A091,
	DataInputBlock_ReadUByte_mA63395E6EA83CA6F94DFC3C9CC4971993CE24BFA,
	DataInputBlock_ReadUshortLE_m57CEBDC426DF93415323440E2DFFD8B6428F1DE2,
	DataInputBlock_ReadUshortLE_mA239ED7CF0FD1E901EF676836E78B05C647F5950,
	DataInputBlock_ReadIntLE_mF3479462BAD83F3F8F5845024350D02C43126926,
	DataInputBlock_ReadIntLE_mD75614664280F337060E80B446C819C125A8B56B,
	DataInputBlock_ReadLongLE_mD33843F1AA32FDFE52D361255781A74AF81A4DA0,
	DataInputBlock_ReadLongLE_m7EAD4AE33FFD1A8C153BBA880DDBE500CDE03ECA,
	DataInputBlock_ReadSpanning_mEEB2C7EDCC769E6C0E6B17CEF93AA662669EB9A8,
	DataInputBlock_ReadFully_mFC1719084A7B38DBF1F19F9963E23F9094435738,
	DocumentBlock__ctor_m9DB80B52406A4C78EB980D546A6FB72CD93AB896,
	DocumentBlock__ctor_mB9CFCF6CF2A84740E01CF02FD242265993BC52A8,
	DocumentBlock__ctor_m17EDA2B0243376CDDF2E180462B845728E62DBFB,
	DocumentBlock_get_Size_mAC3A9B4E3FB59D928130248D0B9712C5C956A1B6,
	DocumentBlock_get_PartiallyRead_m253FBBF8C0F49EA1F8BDA6EE670515DC6345AC55,
	DocumentBlock_get_FillByte_m707F7D8DAF239B79D61A420B3686680849AB6886,
	DocumentBlock_Convert_m1408F64A80E1EBDFF924693540F6D897D74A5239,
	DocumentBlock_GetDataInputBlock_m615D7824EC88FCFA910D69EDBC3ACF4A669DC1CB,
	DocumentBlock_WriteData_m2202C07E8307343BB51E9273F32B79AADAF61FEE,
	DocumentBlock__cctor_m0556049E6D453ED420B68E65CA9CAC1CE73568E5,
	HeaderBlockWriter__ctor_m29DD3AC8B1C2B51F01AB3EC643FFA8300F1EE4CB,
	HeaderBlockWriter_SetBATBlocks_m7392DEE99595A181EC7474F40C3CC73D7DB0760C,
	HeaderBlockWriter_set_PropertyStart_m7924D828508D032020857AEB8FFB886BE9F04BEB,
	HeaderBlockWriter_set_SBATStart_m6380204BF177CEB731E7E372F960A59B6184D957,
	HeaderBlockWriter_set_SBATBlockCount_mAB04D8D8FAF965FEC060C84C7D9F6DA0517C8425,
	HeaderBlockWriter_CalculateXBATStorageRequirements_m07B82A5A91C98070DD0E09D4E09FDC5666A26C09,
	HeaderBlockWriter_WriteBlocks_mBF0CFABB27C3E33B96ED692129EB2C2CB445A2AC,
	NULL,
	PropertyBlock__ctor_mA4D6B6402423E3BC8ADEECCC04E3149E0743F3BD,
	PropertyBlock_CreatePropertyBlockArray_m1182BEC209F6C0AA9BD8D6F60182792909FA03D1,
	PropertyBlock_WriteData_m7261E865647A9A29B2228023BB7EE9B1EBD6B913,
	AnonymousProperty_PreWrite_mDA816F4D5A0DECA615758C206571FBB7701411F6,
	AnonymousProperty_get_IsDirectory_m661BC0713F0ECDA001AF63C462327176D4D9E8C8,
	AnonymousProperty__ctor_mF8A8D1FF59287D8742B82416208AD82F8F29C53A,
	RawDataBlock__ctor_m0F0E9D97998902B2686EC6E757404B4A4A446A4C,
	RawDataBlock_get_EOF_mCF1D79AEFDE08FFA3F4CE85F1200FB0A0A0CB7A5,
	RawDataBlock_get_HasData_m875CA9B748A33473FC3037A992B30BCFF15EAC66,
	RawDataBlock_get_Data_m78F0DC99E0BF7D7F1026E0BC57760C2109C40D5A,
	RawDataBlock_ToString_m26E6F1579DE4E085A0D6831546A9ADBBF75A82B0,
	RawDataBlock_get_BigBlockSize_mC6C11C7A2504D5A52E06B3FB16295410F551A8F1,
	RawDataBlock__cctor_m2FED7FA16EF65FDE700287BE2001FDC3CD8430E6,
	RawDataBlockList__ctor_m47E988D6D9F5C253161D95D2B152E2905AA2FE4D,
	SmallBlockTableReader_GetSmallDocumentBlocks_m74431943DAB02B90C5315C72B1D559698C6CC37B,
	SmallBlockTableWriter__ctor_m6AC547EA5A6D264A9FDFE6DD5AA898D3EA2D3EC9,
	SmallBlockTableWriter_get_SBATBlockCount_mACA3F1A8B638A0161371132464BB6C6DC81C31DE,
	SmallBlockTableWriter_get_SBAT_mB942CA5F42D5BB334C088F5B39E28C00B58DCE39,
	SmallBlockTableWriter_get_CountBlocks_m9A4F0A96BA704B81CBB09B376DC89FE0C7CB73E4,
	SmallBlockTableWriter_set_StartBlock_m3C0D9A2A98BAC6498CFACC11742755E821BB170A,
	SmallBlockTableWriter_WriteBlocks_m4D9CC52DB516FF65210A261C2D19452B587CF100,
	SmallDocumentBlock__ctor_m624D950E81E06176D0E3F00E30BE7E678F0AB809,
	SmallDocumentBlock__ctor_m87526FD90B327BA9508FD518CB88548F9D96EA4E,
	SmallDocumentBlock_GetBlocksPerBigBlock_mA96D61377C666559E531A628A2AAC8DB83EB4886,
	SmallDocumentBlock_Convert_mD06C5384958860DE5A321E47EC894C38FEBF03EF,
	SmallDocumentBlock_Fill_m6334BA3212ED704975DD8E016DE3C11F0193E13B,
	SmallDocumentBlock_Convert_m3C4BA518D643A2A01D1644D71AED7FB6673026D0,
	SmallDocumentBlock_Extract_m4B5247879806F696DC7AE3E24751180D98A27AE0,
	SmallDocumentBlock_GetDataInputBlock_m4822583DD7A2E762E1B2BDD6931761ADF8B0D3EF,
	SmallDocumentBlock_CalcSize_mF5BBDFBDE8C949A4EAE221D8921AB95DCA780749,
	SmallDocumentBlock_MakeEmptySmallDocumentBlock_m2DFA5A7470F2A77315D585E4F94C432F322020B4,
	SmallDocumentBlock_ConvertToBlockCount_m36FF691977585CF8F85470517C1D0801AE6D402A,
	SmallDocumentBlock_WriteBlocks_m11ECC27AEDCB91EDDF46C6021D34C7576EEB29C5,
	SmallDocumentBlock_get_Data_m0BFDD48023B9F70BCAF729D765281291FB05421A,
	SmallDocumentBlock__cctor_m39A98D225D656CA90FF59FC763D7EC869AAD014A,
	SmallDocumentBlockList__ctor_m7BB5D41298E5260305CE3CBA399AE0A87AE13B14,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	HSSFCell__ctor_mBE96AB275D10A146B2AFB543CDC3D1EADDA56459,
	HSSFCell__ctor_m34946F8045C76CBC87EA3E53E307241406DA85F9,
	HSSFCell__ctor_mB4F0881EDEAF8677B4BD66E1D9FDF5D261D2D404,
	HSSFCell_DetermineType_m2BC7D1EB221FC21A8DCA0994F1B3CEF42BFF2EBC,
	HSSFCell_get_Sheet_m8E834383A8CD8B5CE0C0A55FABAE978469910BF6,
	HSSFCell_get_Row_mD23B7F22461413B151F9EA2016FFE2AE61FE3BBB,
	HSSFCell_SetCellType_mC30B4560711497C7B0FC592F3B7BF15BFD6244B6,
	HSSFCell_SetCellType_m9BEC5675DA9B138E605BF603D1094240CE704981,
	HSSFCell_get_CellType_mE403E2CAB5C4D3DF8972E25D2FADD19B136147DB,
	HSSFCell_ConvertCellValueToString_mCF6C54A9C168FE2D0B67751EC47EC4E7AB9F277D,
	HSSFCell_SetCellValue_m1E334F6E0DB2200CE7D3076AD9F6F616275F7622,
	HSSFCell_SetCellValue_m0BDDB85954F747FF4B004DDB82BEA08B35565A96,
	HSSFCell_SetCellErrorValue_mBDB423E3FB809B7FDBA35E553E4A0B0CECA2F12E,
	HSSFCell_SetCellValue_mA9620129A176BE7414B1E6CA56D0B78659E4F313,
	HSSFCell_NotifyFormulaChanging_m55AC3208C81BC8C8D54296D2D5CDB3BC6F8457EA,
	HSSFCell_get_CellFormula_m913F5B49131AB2384F12E8DC7609991BE38623B7,
	HSSFCell_get_NumericCellValue_mC77199558ED1106F0276527D6ED6BCBB20F21D96,
	HSSFCell_GetCellTypeName_m9DF8171C25C9E9590A9F200FC120D0E83311E216,
	HSSFCell_TypeMismatch_m3CAC109F396096A40AA0D27A9622CDA1B13228C8,
	HSSFCell_CheckFormulaCachedValueType_mFF8F4C8582BE9099E764FA2038C934718DD61EF8,
	HSSFCell_get_DateCellValue_m568151241A852485C56E3CC70684BED67CBC602F,
	HSSFCell_get_StringCellValue_m27D1A744609102EA1FA94E9F4FEE3867AD4B133C,
	HSSFCell_get_RichStringCellValue_mF2071E01518B31301A02BD0EAA1BD38D03F4FB9F,
	HSSFCell_SetCellValue_m8E3153D7895269B8A1A8023001DBFDDB0443918D,
	HSSFCell_ConvertCellValueToBoolean_m51E88CBF78649F9E0652941CA2B3D943DC5F3CD4,
	HSSFCell_get_BooleanCellValue_m3F713C7B2AA1857B9F516A52F38741C86363F313,
	HSSFCell_get_ErrorCellValue_mE6D4E7DDC208975BBD8BA20AAB6B125E26AF78B6,
	HSSFCell_get_CellStyle_m00B2C346B221631389E7E7EAB3E7E4E517DA579F,
	HSSFCell_get_CellValueRecord_m8AFB0D4A349F135FFA4CBBC56771CC8D14C754FA,
	HSSFCell_CheckBounds_m6163E3426B9205BD4D3B6BAF2B317E7CB222DB78,
	HSSFCell_ToString_mFAF26AAC803F2A0112856E15E9E2FE2A21D34218,
	HSSFCell_get_ColumnIndex_m0BE9086D60532F676A805C0362586643CF21CB7B,
	HSSFCell_get_RowIndex_mC7A930A49CB594ED4C31403C07A1500205A6D2EE,
	HSSFCell_get_IsPartOfArrayFormulaGroup_m5D20A5EC49D307784C5B7BBB7CE1F06E6029DFD1,
	HSSFCell_get_ArrayFormulaRange_m68817B78ED7D57ECAB027381F606297ABC9F02BE,
	HSSFCell_NotifyArrayFormulaChanging_m195070C03C71EFFDF7F09A9F9CBB14133EDAA59A,
	HSSFCell_NotifyArrayFormulaChanging_mDF76A9C3289771F66A68A215682BB08E7F6A454A,
	HSSFCell__cctor_m741E41780695747AE3B43B69B107125B990162B6,
	NULL,
	NULL,
	HSSFCellStyle__ctor_m2FE25DCAD9A643B526C68B71DD46E03F9ACFC03D,
	HSSFCellStyle__ctor_m7C29921F7BDF46ADD0A03A6220605FAE786DF885,
	HSSFCellStyle_get_DataFormat_m25EC97585A07344CE733D0F13866DADAE1E2C828,
	HSSFCellStyle_GetDataFormatString_mE9DA1FFAA7250ECB4386F33D57B547DF3967DDB6,
	HSSFCellStyle_GetHashCode_m7368C6B3F8354FEC5AEF60CBE0A196D70AA457F8,
	HSSFCellStyle_Equals_mC663069B4D7B592A88799F2FA8202498D6BF9A41,
	HSSFDataFormat__ctor_m41AC3CE035FEFAF4FEEAF8FEE8C3A15969B6B6C8,
	HSSFDataFormat_GetFormat_m044914E65F92E4B70AB5BCAAEE0546D7CFF72772,
	HSSFDataFormat__cctor_mD25BC16DA05F083D077D55BC144ABA6BAB0B5380,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	HSSFEvaluationWorkbook_Create_m1AC732DF89755AC1BE8240BED4188E9BCC564D03,
	HSSFEvaluationWorkbook__ctor_m4ABAC2B614069039333D78AD2C099FDE535E2C7A,
	HSSFEvaluationWorkbook_GetExternalSheetIndex_mADEBAA152135E762160802FF19728B3A00D8B761,
	HSSFEvaluationWorkbook_GetExternalSheetIndex_m690F9D093A3095CA2EBE90D81024757065EBB9B9,
	HSSFEvaluationWorkbook_GetNameXPtg_mD6C9AA45F55F57744E3397D08F79FFD39F2BBD43,
	HSSFEvaluationWorkbook_GetName_m88ACC4C1AF210DE3E4DE06F2E11456B984217EEC,
	HSSFEvaluationWorkbook_GetExternalSheet_m9DD101B46D3801800D5C50FBA0590EB520DABF83,
	HSSFEvaluationWorkbook_ResolveNameXText_m27807FC76D5B996FF354763AE38FD93964D543D2,
	HSSFEvaluationWorkbook_GetSheetNameByExternSheet_m71A4EE0C744C035BC89131CA7EACDBBDC0D8B75B,
	HSSFEvaluationWorkbook_GetNameText_m3CC600914A65788F00E63560C7CD5DB66E1B5FBD,
	HSSFEvaluationWorkbook_GetSpreadsheetVersion_mB11E0201A6B3E5DD26A131E9BC138663891347DD,
	HSSFEvaluationWorkbook__cctor_m2738BE1F07ECCFD5067D0FEFE2949E21245D2F12,
	Name__ctor_mB7947220D3599D0CF6A640CF88B5B4D64BF3E019,
	Name_get_IsFunctionName_m78B81A5D1343E4C44596F881D0DFA218197E673F,
	Name_get_IsRange_mD372D65327E88FD57F1BE86E9250F82B45F62E4E,
	Name_CreatePtg_m94CAB9200C834EF517584DD13F0C0139681AF71B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	HSSFName__ctor_m78AFA0517882707700F91F6962F7750198392C43,
	HSSFName_get_NameName_m0FD1F5A903BECD8E598A1F6CEF46CF0F3E6CAFCD,
	HSSFName_get_SheetIndex_m1C4DF9993BE91BA28E0EA6B4AEA69F6E626C65AA,
	HSSFName_ToString_mD5AB691D49CAA0F358CE8E4491CA217502EDF9E6,
	HSSFColor__ctor_mE548D32E94F5A2FF170B296F1BE293456D6CEDA0,
	Black__ctor_mDD512ABE1DF9BB056BD72A195DC90DF0810FB963,
	Black__cctor_m0DDF80B6D757E1AEFF4FD9A6C9689AF52475FDE1,
	Brown__ctor_m0E80E41050A24C9FA9406EFC299B05728F33AF9D,
	Brown__cctor_mF463CFA772B94E3D35F13A9058269192E7FEAE35,
	OliveGreen__ctor_m0E43B7D05819D33711ED8EB1DB910C1852EF6039,
	OliveGreen__cctor_m60083D98DACCA883A426A97622CEB7A713250B58,
	DarkGreen__ctor_m5C2A8B2D6B2AF4E38A428E61A0651FF7AD34CE86,
	DarkGreen__cctor_m8AE6F0D0498DD766C66418A791A8595ED40E24A3,
	DarkTeal__ctor_m869EC86D21EA5D02A213A369304216AC15107F6D,
	DarkTeal__cctor_m522106859A6E51E81C9FCAF5ABCEC944ADFEB90A,
	DarkBlue__ctor_mCDF3E17EB660E4B4F9FB2390AAC81BD8EAA171EC,
	DarkBlue__cctor_m2BB89C834104FAC5B76D90F6B023C4094D10655E,
	Indigo__ctor_m189535777D7D924CD30E24AB4968574C7A78E749,
	Indigo__cctor_m02120AE45583B1B913016FE6A825A75BC2E35E9E,
	Grey80Percent__ctor_mBFB19CCA82A75CCDFEB7B1FA31CAEFCF34B4A00E,
	Grey80Percent__cctor_m893447BD04E619EBC73FD6BB67FAFC59DE39DD14,
	DarkRed__ctor_m51C47FAC15626F8C51AAC18EA73A5F00A1C8CBB4,
	DarkRed__cctor_mB41DF7EEF26ACFC9D70ECEF73F8E3EA91E0433FD,
	Orange__ctor_m55351A08AC385F6B7D4A1C56538CCDC3344F3178,
	Orange__cctor_m3B9C6D73AD5BFBCEB2583F0AD14722FD3692ECF9,
	DarkYellow__ctor_mE7D98866483DB83A8AC5585D1B94A95AF9DE481E,
	DarkYellow__cctor_m85421E1DFA911EA982260CB780AD67935D76694C,
	Green__ctor_m425821F7CE7EF6CEE72D99548AD72F60D1EDDCCF,
	Green__cctor_m4A143F7672968E0942AE70BD9522747F9723C721,
	Teal__ctor_m50D26DE4B307BA823BE4AA5442DDC2A69CE26296,
	Teal__cctor_m9539606D5E54C4953D23FF97A6E6D6C9F03BD9D8,
	Blue__ctor_m4F73B113B8050F8EA64BFF05F7D93DD10759EE7D,
	Blue__cctor_m1158E1C639891019AD2A70A3B2266FC39E3C7032,
	BlueGrey__ctor_m303715C4D2E3D59F227F982A3DFA0DC444080408,
	BlueGrey__cctor_mD5A1B25B45FC039D3EAF4E027C4DD9791236ACA0,
	Grey50Percent__ctor_m96B618DF2D8F9AB964F842BB962DB2E0D062B24D,
	Grey50Percent__cctor_m9D44C37CCC03B7C5E44D3FCBA841F33239C15534,
	Red__ctor_mCB26E089499D343D656405B091BCC0279AE6B5EE,
	Red__cctor_mCAE40A1A9C644BD2665C95AADD0013A0653F9027,
	LightOrange__ctor_mED1C0930F7592E5D39D255A249D7070B45CB1114,
	LightOrange__cctor_mD118C8ED4F78C1EF6CB72CC55730D25DC4E74A9A,
	Lime__ctor_mBF0A6EC9F661FCEDE27FCCF70ECB07E358D07A60,
	Lime__cctor_m0DEAC99DA5647600412E613C8FD66F2D58E21160,
	SeaGreen__ctor_mC838FB6634715AFA25F80DC92F63B8BFB21ABFE6,
	SeaGreen__cctor_m48484033F3300FC081BC99819E59DED6A1946FC8,
	Aqua__ctor_mA77E252F3BD76E5BC8D99326640B8A4E9E051D61,
	Aqua__cctor_m3835560656C293B6F6F784C79E23515FB4234FE6,
	LightBlue__ctor_m54C137C308C53DDA50F3DDC39C5E466147B0F2A6,
	LightBlue__cctor_mFCBCC88DE5107CCB5287DB8D827E6E479D8B0E61,
	Violet__ctor_m9F29FEF7DC3A523EC9B925047FA58B78F9ABADED,
	Violet__cctor_mECD9BC37F0A360A13035E03C59E622DE60E5A21A,
	Grey40Percent__ctor_m7121D52B98FD82381A06678E1D7EE11B7A869B94,
	Grey40Percent__cctor_mBE0270F9D9EA050DB86F846919A112CB023A8077,
	Pink__ctor_m7332C4C0E0FFDBBDF5DC6217B0B7DE7B41831A8D,
	Pink__cctor_mD008BAFA1E0118ADD67020512AB961ED46CC0576,
	Gold__ctor_m0C9E33A40BF98F1F5C0E32465BD7FECD108DC1B5,
	Gold__cctor_m863F8EC5FF0DA0484533516053F92DED81E2EFC9,
	Yellow__ctor_m9432629D83B482FFBD1B580098DCCE9DB927332C,
	Yellow__cctor_m12CDBB7603907802E99E8EB2350E4216A4E1BE8A,
	BrightGreen__ctor_m4A5175FACF2D6F7E4374BC2C0B37F5D7582C36F1,
	BrightGreen__cctor_mB3F70276C338F7DBD752C14252FF0E545465A3F4,
	Turquoise__ctor_m9859BDCED223DF963AA53853065CC62C3CDD6787,
	Turquoise__cctor_mF7CED81644BC41F7FDA3855EB2E1786F1C6C59B8,
	SkyBlue__ctor_m7D7402BAED2222ACCDAB95E9350C2BE8A17102CF,
	SkyBlue__cctor_m8830856562ACB02FD0427A07C2EE2717781DD35A,
	Plum__ctor_m86D2888A5FAA1BB749F869C36E7F63B0F43E673B,
	Plum__cctor_m6408C7E726EF562D5AD21912C84D8B21A283FC4B,
	Grey25Percent__ctor_mC7155DD6955BEE2BC772D68FD7E08B2C1ACE9C33,
	Grey25Percent__cctor_mA637492EA2294FC7660CE12CC4994546C46D0BF9,
	Rose__ctor_m590E654772BD238DC4D6B1AFE1C8797C8578AB1E,
	Rose__cctor_m1017DDF96ED78FAB1833904D0C3681C9B2F77FB9,
	Tan__ctor_m8137E897A94AF1C1B7E1B15E724B95778CB32556,
	Tan__cctor_m25493A292077F53628E65ED357CBBAABBD5BD0D8,
	LightYellow__ctor_m5054C67D7AB413A95F45BB6E8A8563F2632D4428,
	LightYellow__cctor_mF53D2A47DE9A36DEA5B61C16C637D6FB55346050,
	LightGreen__ctor_m0F2711909BF35AEE56E28385965181723F1644D9,
	LightGreen__cctor_mF2969F13D1C63D4779C5C59F5FF738D11A04BF5B,
	LightTurquoise__ctor_m41A011FE8ADCD74084E2018307EEF0B2C3AE8C99,
	LightTurquoise__cctor_m7C2F990E1BCC1D3C6550699AE877B7D8C01064A6,
	PaleBlue__ctor_mF9AED760C2C1390068CC62E50430066A85504FA7,
	PaleBlue__cctor_mBD247D98D60511655EAE83D23ECB8A8AABAB620F,
	Lavender__ctor_m9237CEAA3A8F5D849DF35A8FF1577F7439F3FC36,
	Lavender__cctor_mCAF182E57369046A56190A7844081FB208A299FA,
	White__ctor_mC52C81F7A6448439687A43C57EA3D2DCD722B63B,
	White__cctor_m9482336456A16DBA2381B6865A29D4DD996A86AB,
	CornflowerBlue__ctor_mB73855045DA56BBA94176D61451AFA66B52CBF4F,
	CornflowerBlue__cctor_m4DE055505B5AA20E950418B66F509D8DD2BF5771,
	LemonChiffon__ctor_m3D1A00D485001BB20AB984F6A73B340752873A79,
	LemonChiffon__cctor_m1A6AED69F1E0CAF685DB38B07BD36A35256A523C,
	Maroon__ctor_mAB56B99ACE15250932291862FF2BAE2E2B7B361C,
	Maroon__cctor_m70E409BF0E304D6B1E9B62F94F52E7E85358EE0F,
	Orchid__ctor_m1E8F180B5DD78F138B6A574E07D950C9B9C93F81,
	Orchid__cctor_m1E0B1867791D66820AAA230CE662B5A372C24A9C,
	Coral__ctor_m8B852E22B40F60BDD3713A77CE38B63D7932DE30,
	Coral__cctor_mDD14C3A52871DA017E6542106474F915F092207F,
	RoyalBlue__ctor_m7533D174EBEEFE201060CECC9D5E6FEFE71CC352,
	RoyalBlue__cctor_m8CE982F25FDD91B3B55C25FDD07CE8AEBDCA962C,
	LightCornflowerBlue__ctor_m129E612D5EC328AA330E2115B2879639CFFF01DA,
	LightCornflowerBlue__cctor_mC43AFEC84272469E3048D95E3A76484C82DFD104,
	Automatic__ctor_m7C94DBEDE6650C6804F561E682E02FB3B0F4CD12,
	Automatic__cctor_mAA31F16D5EF41AD157913F2BC17B9549F497C307,
	HSSFPatriarch_PreSerialize_mB489B53C42B563088E24C27182002F4A17C5F20C,
	HSSFPatriarch__cctor_m74575E7F24D70A849D2E48343B7A3312A2FFBC1F,
	NULL,
	NULL,
	NULL,
	NULL,
	HSSFRichTextString__ctor_m71088321C30F2A6B02DBD10E04A5FAB8CC9FB313,
	HSSFRichTextString__ctor_mF4B27CA41A5A5010EF3ADD105A5F6EE4FCBE1A92,
	HSSFRichTextString__ctor_m2DDDE3EF3F0A97D49100FC43FB916883400F0FA3,
	HSSFRichTextString_SetWorkbookReferences_mCDAD5BA9648B1F0616139F2D18543D44422A708F,
	HSSFRichTextString_CloneStringIfRequired_m64A7224527955C03191C7089CE5A47E212C6E30D,
	HSSFRichTextString_get_String_m8895E76FB566F7CFFF11A9BB8800A56D44C5F3C6,
	HSSFRichTextString_get_UnicodeString_m642C0FFEBB0679B545C8BDD9DA44E1994DEE4066,
	HSSFRichTextString_set_UnicodeString_mB5D0ADE580DEC104438137E4EDDEC9B33066C1F3,
	HSSFRichTextString_get_Length_m1597AE1AA654328303118D97C24CA569FEC75C50,
	HSSFRichTextString_get_NumFormattingRuns_mEFD1877EFAC3A6B0348DC6406B8EF71F73A35B74,
	HSSFRichTextString_GetIndexOfFormattingRun_mFC25F86A5B740F1A3902AEEE1115F0FCE7BEEEEF,
	HSSFRichTextString_GetFontOfFormattingRun_mB6E823B6128450B96D542A117897799F3289B6FA,
	HSSFRichTextString_CompareTo_m95E8FCC11C5CD47D3EA9CE456A33A71C53BCA53A,
	HSSFRichTextString_Equals_mB06AA50B99B4A629EC7DC7ACE54D3935E8C82B7B,
	HSSFRichTextString_GetHashCode_m90EB6400F31E355B74FE2338BF8224CAE9AB045F,
	HSSFRichTextString_ToString_mCFFDD602B119A5682549C31E3559CCBE585EBB8A,
	NULL,
	NULL,
	NULL,
	HSSFRow__ctor_m0B676076B68D85DDF13618D42941CAE6CD3D9296,
	HSSFRow__ctor_m7994354160DB140555E8392B5626365249CD3379,
	HSSFRow__ctor_m364D0A74DB207BF79BC61B4C81A070AF4E9A9E16,
	HSSFRow_CreateCell_m46627A68A40CB4E628FDDE861484BBEA2DA45C64,
	HSSFRow_CreateCell_m046067534F3AE3AADE0E9A7AEEAAEAA095A48368,
	HSSFRow_CreateCellFromRecord_m861666F8B98FEC9C6F18379842E68D6C3E560E64,
	HSSFRow_get_RowNum_mA41D0120D7BACD29A3F4FE622C5EDB0D273EB58B,
	HSSFRow_set_RowNum_m112F982D83AFB39A9DE645C76E26CBD1B5523278,
	HSSFRow_get_Sheet_m4D77D7C1D71D9FB409C0FBCE138679EE59A755ED,
	HSSFRow_AddCell_m28BA5A7BD67CB28979743A191E6675E2A5B0A751,
	HSSFRow_RetrieveCell_m22F34A94D24E4BFD5302B4B1E47DA8031B8D963B,
	HSSFRow_GetCell_m63348D625B6D11D49C95CE0D801CBFE5F65FFA78,
	HSSFRow_GetCell_mD19C4636BE3893593FED378AA93E791ADCC7770E,
	HSSFRow_set_Height_m480C6EAD7D8E52500A6972A487C0A9EBC7CDEDBB,
	HSSFRow_get_RowRecord_m235649A9E68AF661A21A3EF0A582DA69B09A8FEB,
	HSSFRow_CompareTo_m9482EE7D29A72AD4B5D9585E90FCBB518263BCFC,
	HSSFRow_Equals_m432D0ED96FC1ABEE378F167AE327B2620C561330,
	HSSFRow_GetHashCode_mDF6F7C58476DCE598EE98C031D54E78FE453AE6F,
	NULL,
	NULL,
	NULL,
	HSSFSheet__ctor_m0AFB95C0772A8DA991304D7FCE66193BB7524E8A,
	HSSFSheet_PreSerialize_m7F9AE9DE585444CB3EF31CDA0D1B60F2FF6584A3,
	HSSFSheet_SetPropertiesFromSheet_mEF4336DA61118CDF0E5D2045A8969155A103DCFE,
	HSSFSheet_CreateRow_m9391164B1E0C568EA2D7201A6B3395B43070A5C9,
	HSSFSheet_CreateRowFromRecord_m95393737A4982877B32D263E7707A5C32EAA5CC7,
	HSSFSheet_AddRow_m6992B0DEB336F5EA4F1CE78D1AB31767AF42BFE3,
	HSSFSheet_GetRow_m47EBB212C8B4B769D66867260803D177774CA5DE,
	HSSFSheet_get_FirstRowNum_mAAF5136FF708F515D74A9472F0C6F43B970B5EBC,
	HSSFSheet_get_LastRowNum_m6934E14ADD3A57F54FA2F5A63AC7581362A8EF8D,
	HSSFSheet_get_DefaultRowHeight_mFFEB31E238D296C0BE43AED40655CF23749B2223,
	HSSFSheet_get_Sheet_m80231677F6B929806333500F6F520AFF273AA69A,
	HSSFSheet_RemoveArrayFormula_m22627DE153AA74C73659AC553B5C503892884EF4,
	HSSFSheet_GetCellRange_mD3147B9EEA09E4FCCC9B87F9BE85DD196229B4A8,
	HSSFSheet_get_Workbook_m9C48D069F02C86975FB70B7878CC70921C6A5CF6,
	GUID__ctor_mD8794259110005CF8CF5E810DA97A01F244C7C33,
	GUID_Serialize_mC78A54D48026683221ECD7B19F4D298110D65140,
	GUID_Equals_mD914C6C5EFB6AA7C119E98A69C0E16A3F7ABE532,
	GUID_GetHashCode_m9290B718E23C76117399F2CB97A9CE576989D1F2,
	GUID_get_D4_m80650DD474C19C9AC8C3032547E4C5DB268AF740,
	GUID_FormatAsString_mE04212B995A45FBC1B952EF4424445E47832CB75,
	GUID_ToString_m7261078AD406749A876E28DDF30D61FF5A8E7A6A,
	GUID_Parse_m961CCB29EAA4A49EAA446E0E606656307F5F4C39,
	GUID_ParseLELong_m6F2CDBF227864DEF0AA64E88451A2643C7667B32,
	GUID_ParseShort_mD336D1DFC45B21A14CBC7ABA4C12E142ADF6E007,
	GUID_ParseHexChar_mC153A244B167B8A1D6807D62DE66D5B0BF11AF02,
	RKUtil_DecodeNumber_mACAE8CA8D4C38C21AEDDA311B00091231EC989F8,
	NotImplemented__ctor_m494F2A3B87FE13DA199789FA345AF0D00DE68B87,
	NULL,
	UDFFinder__ctor_m6B1A81999F40EB7C806C35DE4E483BD6E9924EA0,
	UDFFinder__cctor_m856F2A16BAAD1E6C3CB7517B1068BED85C955793,
	AnalysisToolPak__ctor_mD4B3D7DB65DEC35A8E26B743B14FA19ED6D7FAE5,
	AnalysisToolPak_FindFunction_m17D7CF066843CD14D7E53887133ACD816224CC75,
	AnalysisToolPak_CreateFunctionsMap_mC3C10A533C6DF9CAC0320A6F4059E8C88BF00519,
	AnalysisToolPak_r_m5CDA787762F97C907B03FCC7FB0E4E0A010A6E92,
	AnalysisToolPak__cctor_mE939623F627F2F8A833489AA39D03A8699583791,
	MRound__ctor_mC544B878B44E3DF454C5656CCE10BF302CDE7A56,
	MRound__cctor_m0C169AD7856FF03BF948CD61355B0FEA2A8F9B66,
	ParityFunction__ctor_m700DD79DDB8C9810E73BB5507D2B8A302A082B14,
	ParityFunction__cctor_m3D69D69C57E3092D493BC19FD72B822F304AA12D,
	RandBetween__ctor_mB0B2C846DEA4B6A890123CAABB21DFA6CADF7C0C,
	RandBetween__cctor_m942D1940B989EFD943C5B5BCEC875215A0EBCA36,
	YearFrac__ctor_m3E2A445742AF7CD1F301B5FA6C4D5E09AF0B2143,
	YearFrac__cctor_m85E4E9C77D268B74A5572EEFB60EAF2DF1F9C111,
	ExternalSheet__ctor_m1E70E35D8E412FEC85067FC763C428B771F4035E,
	ExternalSheet_GetWorkbookName_m9BC3E8AF26978A59658B1AB8D3B9AD14D2DE8AE2,
	ExternalSheet_GetSheetName_mFE68F76ADC42B35AEBE02A4B9F625E2D577CBCC7,
	ErrorEval_GetText_m63C81B63A1B452E7C6A87C061DE91583F196B61A,
	ErrorEval__ctor_m08B9E060855FAD013187DD83659A8EE582522324,
	ErrorEval_ToString_m4B1958710E095087E730C4DFB073783C8C250BCA,
	ErrorEval__cctor_m7E285F5ADC2EB3647E42E808FC108BC8BCF3B93D,
	Formula__ctor_m0246B5728D58DAF8226B3387225554B488732B39,
	Formula_get_Tokens_mC05CA4973D84C36873FD405BAAF829E5275A3B63,
	Formula_Serialize_mD3A508B900B5DE6EBECA3086AAA96C747C34A292,
	Formula_SerializeTokens_m93B761AE47D8B8B5141EC694F5299C646C8F1C98,
	Formula_SerializeArrayConstantData_mF5C16E2AECBF411F334CE0B6DF967E8F69DD29D0,
	Formula_get_EncodedSize_m66258B72D883D70341F3739406B1E96D3B64FEDF,
	Formula_get_EncodedTokenSize_m65FE0DEBAB856D632212DF80037913CDBE3ADA83,
	Formula_Create_m5B3B3C53127C08B08CFDE7EC60F041A91CA464D6,
	Formula_Copy_m0B4D06570AFB73CAEF6B79B9ED5ED019B22F4DBD,
	Formula_get_ExpReference_mA47CA19BBA03C7CF591502CC95AE950B4653DE78,
	Formula__cctor_mEFB43DA0402613F3976F9FA3FE5DC169E67E3AC2,
	FormulaParseException__ctor_mC48145AED34991A6705ADF32650C4042BAA1CA84,
	FormulaParser__ctor_mC585BE1D888EB7B82DA486A0E3A93C3EBC3CCFF0,
	FormulaParser_Parse_m0A1C0DF9CCEE8E5FE753A0A281E411C583D9580F,
	FormulaParser_GetChar_mEA6B357BDCD946FE6AB521F4D1DDAAE2F3F6233A,
	FormulaParser_expected_m11712A67E51FF3FA6D349837013E60B6E794BE41,
	FormulaParser_IsAlpha_m6C11155787A5DC9036E0B51CB1C73E17A0FFF36E,
	FormulaParser_IsDigit_m8B56B2E7DDEC7BA5C150E16FE826C5A662E7E442,
	FormulaParser_IsWhite_m58837A75FA520F9E98EEE7937399B64CCD73129F,
	FormulaParser_SkipWhite_m25E7790158D711C03D5048DFCBF4EEAE4AFDC701,
	FormulaParser_Match_m9BA1B07BD8CF8102A7617EE6883391D976714346,
	FormulaParser_ParseUnquotedIdentifier_mC593242B5FAEC4ED7676E638BE35A995887DC494,
	FormulaParser_GetNum_m9AE7F13683C172A275E5DB79A30E2309A8DA0DFD,
	FormulaParser_ParseRangeExpression_m0EBD053994FB6F1DDF66EE4652D30525678E4416,
	FormulaParser_AugmentWithMemPtg_mB6E528CC01900E0A4B3CC3F114A21939A6844D07,
	FormulaParser_NeedsMemFunc_m9CDFB2AC99217197351FAD9BBA7C0CC5DF13BA2A,
	FormulaParser_IsValidDefinedNameChar_mDD15E5B8CD4E0224EEE1AAC0C5803024F38AF8A8,
	FormulaParser_CheckValidRangeOperand_m7CD1F37361330D27B661C7C3AA0215ACA6624DEF,
	FormulaParser_IsValidRangeOperand_m650F3651F3F7289B72B6BC01B42E07F6F3362929,
	FormulaParser_ParseRangeable_m2DB9927748CDBDDC17D6D65AEA9A569BDD6F2433,
	FormulaParser_ParseNonRange_m1255CDF4D37F8A0956A8571E0DE0264F649EC056,
	FormulaParser_CreateAreaRefParseNode_m6FCB69D23B2681D810CB3BD3A176F9590A6B91EF,
	FormulaParser_CreateAreaRef_mE5E64844E32339AA10E70C319A6C7BD1C0044BD4,
	FormulaParser_ParseSimpleRangePart_m07CD0B3694FF76552A37FF5FF7859618BAF5DC9C,
	FormulaParser_ParseSheetName_m63A519BA57EBD6AABE4AAF11498B48D835AA20E6,
	FormulaParser_IsUnquotedSheetNameChar_m73D9E38266E8434E669CEC7D3F16F92B4EB7DCAC,
	FormulaParser_ResetPointer_m2BFEF8937FE22AF3A201D6383A5A555ACC19F9FD,
	FormulaParser_IsValidCellReference_m1894BED3E7E786490B6C40C7C5F748D687CA9665,
	FormulaParser_Function_m44AF79BFACD6CAF75F4143E043403ADB30B65F11,
	FormulaParser_GetFunction_mDACAC6E7C983706B98CF059657441E1DCBE9ADCA,
	FormulaParser_ValidateNumArgs_m90719D56C7EEF394C91493B3E0D8DC00430A5CFE,
	FormulaParser_IsArgumentDelimiter_m0E5F8505D033CE1D731F52905FF407BAD1F4A102,
	FormulaParser_Arguments_mE00CCA1690A428D119B9E74023D903256F0431D9,
	FormulaParser_PowerFactor_mD909A608D245C8A01F9C727FA838DECB0EAD5B1D,
	FormulaParser_PercentFactor_m803D8F4C596CEA8CD2DB4B1D32185EF220920470,
	FormulaParser_ParseSimpleFactor_mEF6DCBA91736C96DB01C6C00DAB6095FEE65218C,
	FormulaParser_ParseUnary_m36BC5DB44A7822D1A0586E12FEADD8BD8478DA0D,
	FormulaParser_ParseArray_mFB1A1B0D199C95D1E713B1EC186AE7B7B483DB49,
	FormulaParser_CheckRowLengths_m2F420D6A216BEB88F16649A5A47B91708A026045,
	FormulaParser_ParseArrayRow_m5C162202B9F0E1EBFA3CD8EE88160D3B777D2549,
	FormulaParser_ParseArrayItem_m230C1379014A997BA30F0FD045D7209103C06CF5,
	FormulaParser_ParseBooleanLiteral_mC0AAF0D1467A12E2B915EBA7529CA484BB1E2545,
	FormulaParser_ConvertArrayNumber_m0F4D483B5402FBE8C1D234C51C37143283FCC7CD,
	FormulaParser_ParseNumber_mC93706E16C3BDE6AA9A2C81D0016C9CA9B65B9B2,
	FormulaParser_ParseErrorLiteral_m3B483EBFB5E822CD7E7623FAE5ED6450C8891605,
	FormulaParser_GetNumberPtgFromString_m2749FA3328B550F911D868965A3F0541572FD816,
	FormulaParser_ParseStringLiteral_m060091DF89D33C0D4EF1F38818632A75863673CC,
	FormulaParser_Term_m5356470EE912B13A0B200033E962036C1E59B853,
	FormulaParser_ComparisonExpression_mCD90EAEB2EB719CF915C7AF06A2D63E6ACC9865D,
	FormulaParser_GetComparisonToken_m47DA1182ED1269125C016A3E4F705EAB2B551BB2,
	FormulaParser_ConcatExpression_m5DA19F60E1E064168D2D9E3B2B5BA0B2AD1137E2,
	FormulaParser_AdditiveExpression_mE11C8330EE92B7BC4105D6748A2ABD69090EB177,
	FormulaParser_Parse_mB525F580E43656098BA8D5FAD8E4AF8601B7A1ED,
	FormulaParser_UnionExpression_m5C32CB1EC71F1445C018FDA43C5FD367C517991A,
	FormulaParser_GetRPNPtg_m49A3E3A462EC914B75AA09AB46E084778E988043,
	Identifier__ctor_m99CC113806A08F2822E1866DD1C59BA566043792,
	Identifier_get_Name_m87836E899CAB864F693689FABCFC74947E15A425,
	Identifier_get_IsQuoted_mADBA9BE13F2EEC245A31FA4DA0B31779E87DB60D,
	Identifier_ToString_m47550E91627B7FA8B192A8C15082E8F66C62140B,
	SheetIdentifier__ctor_m6F3C6A03911242D36A487A7C0AF53602EBDC404D,
	SheetIdentifier_get_BookName_m4B1D064E20C0FE813AEAB4CD6BAEC5A9609EAA97,
	SheetIdentifier_get_SheetID_mB6005A00E2664E8888FC3DCF020DBECE112364F6,
	SheetIdentifier_ToString_m568F31173DE3B9F3E859FF15117FFCAD45E2D47A,
	SimpleRangePart_Get_m8D794BE9CF2A6D46A008E977225F0E016AB53FFD,
	SimpleRangePart__ctor_mB534461B8FDB2AC49C0BBB86BA019521D82E99E5,
	SimpleRangePart_get_IsCell_m55981A1C2B907B39CA6A6D5AB53E68D38E0A7BF6,
	SimpleRangePart_get_IsRowOrColumn_m91DCC31D747B6D24F286F51541EFD155B3183E79,
	SimpleRangePart_getCellReference_m17C0F79F6420611E1B0662DA10A1DEC06674F30F,
	SimpleRangePart_get_IsColumn_mE07A44EDFD808CE117532CBFA4784E3D64AE14A8,
	SimpleRangePart_get_IsRow_m44C09D6C82F75C35BA0FD086D5EE9A5CCB8E51E6,
	SimpleRangePart_get_Rep_m188F00F3CF0812D91EC88D5B80D3C1E6C58C51A4,
	SimpleRangePart_IsCompatibleForArea_mE258B11B4BC067D5945B987B8B6503D65A3010AA,
	SimpleRangePart_ToString_m87F45DB60D5B4BD2E412476C776030583A84E01B,
	FormulaRenderer_ToFormulaString_m7244B965BD5A1F846F0F1365F43183F9EBCDD3CB,
	FormulaRenderer_GetOperands_m8C229B4066B17AC6BA44B6E1F3FCA8C20208A1B3,
	FunctionDataBuilder__ctor_m0E5AA3DF5282389E51CB537EDE7AF841BDCBAA5F,
	FunctionDataBuilder_Add_m84967CEDA9AC8D00B3328F1E6B785ABDCD9D7199,
	FunctionDataBuilder_Build_m8628A8E57C9AF0AAB7D615E4D9D93B5CBF270F50,
	FunctionMetadata__ctor_m4BDEFD5B55E8EDD2244D70687026F9E9161240F2,
	FunctionMetadata_get_Index_m48BF885652356A3BFFE54E0C54BCD75437C91757,
	FunctionMetadata_get_Name_m2F7AFFD764774E4BBD0A8A29AED2252B8DFC805C,
	FunctionMetadata_get_MinParams_mD70488860BB8A41D22E46AABEFD9FFCF3588C383,
	FunctionMetadata_get_MaxParams_mC38217C5C4AB4C62E92C428F153284FAF0751D7D,
	FunctionMetadata_get_HasFixedArgsLength_m86F67ED8876FB262422ECBE1FFD97E9F9164C3CC,
	FunctionMetadata_get_ReturnClassCode_mA2F23F43E91C23C4A4C0E0A7EB2B13C370E1BADA,
	FunctionMetadata_get_ParameterClassCodes_m15BB055D607317C4DD40D0C66313F0B20E4C57D7,
	FunctionMetadata_get_HasUnlimitedVarags_m05E4D7C9901B04F3AC1278FF9EA6D5EB32813C2C,
	FunctionMetadata_ToString_m4E2D8F3E54AF55EEE17E3AAB4A1A63DA1BDC1E00,
	FunctionMetadataReader_CreateRegistry_m037C5FAB408976B0599AEBBD44C4CBD14878FEA7,
	FunctionMetadataReader_ProcessLine_m09E6DC81F4C85C8F9C909369A53509B3FFBE1C7E,
	FunctionMetadataReader_ParseReturnTypeCode_m6AA90BB8CD35B58AF14DC710B6ED6C26123C951C,
	FunctionMetadataReader_ParseOperandTypeCodes_mBEEE4176CE9F62002A15324A70D3AB52B0972928,
	FunctionMetadataReader_IsDash_mCC80C04697BDD3BDDD0F17F5FFE87037AE382D53,
	FunctionMetadataReader_ParseOperandTypeCode_mFBE8BEEB9E464AE10B6604D15DA0E94C4A6FF84B,
	FunctionMetadataReader_ValidateFunctionName_mF6C78975BC5F4F647831047DD0A89F458B01C44F,
	FunctionMetadataReader_ParseInt_m1A5E406CA3FDA67DCE9941D7BEE0552F9D0C28A4,
	FunctionMetadataReader__cctor_mFBC1BFF17E8C4522A2EEF2B15CDE164E56757150,
	FunctionMetadataRegistry_GetInstance_m76847BA51659B0BB6262715FC160066BF62A043C,
	FunctionMetadataRegistry__ctor_m48BB698FEA7832FAAD751671E2A736C053581315,
	FunctionMetadataRegistry_GetFunctionByIndex_mA47703D4785DF45A0A676719658387536104AABA,
	FunctionMetadataRegistry_GetFunctionByIndexInternal_m38889774C6E7833201A1BB3DA5B0D86B396D520F,
	FunctionMetadataRegistry_LookupIndexByName_m0B821DB40F069A2BF5400B1C9D4E3A9FC0D27DBA,
	FunctionMetadataRegistry_GetFunctionByNameInternal_m932CEA2AC7A49B6F3287B6A9892577619BB9D49D,
	FunctionMetadataRegistry_GetFunctionByName_m968D4956B6A45A68A7BEEE49D08E5692131BEDA2,
	OperandClassTransformer__ctor_mE6548A3D5C365F7FA3D177922C765ADEE10E74CC,
	OperandClassTransformer_TransformFormula_m49F263C63E533D7D0C48FC57EEA5085FC9152218,
	OperandClassTransformer_TransformNode_m48C475030F55352137F3AF58E486D9B8D42E5E01,
	OperandClassTransformer_IsSingleArgSum_mE7930564D078BA455BD4B360E09985584EE3FB5B,
	OperandClassTransformer_IsSimpleValueFunction_m0D7A35132B760A45A0C0CA1008CFCFB5825D69A4,
	OperandClassTransformer_TransformClass_m454BA192BE0CA284C9921E2CDB7A9DDFF2A3591E,
	OperandClassTransformer_TransformFunctionNode_m7A6D9E751E88A7BD16CB209ED205954D50F63500,
	OperandClassTransformer_SetSimpleValueFuncClass_mB63DF446EF1CA8E940A39BA42F570C7DACF59B33,
	ParseNode__ctor_m5565C4215F77B1555A3906D22893BBD003343118,
	ParseNode__ctor_m87FA0031DDD2858AC67292D61F13498BDABC675F,
	ParseNode__ctor_m0FA0A74ED2AF86C8858D2174B2FE5366760FF2CB,
	ParseNode__ctor_m4D800012CFFF2A3A0277764E536E9710A8F66D70,
	ParseNode_get_TokenCount_m4909339096F2385DCF9E5D8DD70DD287C81202FB,
	ParseNode_get_EncodedSize_m7ACD97A6F723C6E25B0BF9D527BD8DA32D072B3D,
	ParseNode_ToTokenArray_m42ECE15566C0B8B1335F5FC335BDDBFB6C85930A,
	ParseNode_CollectPtgs_m33D532C80DA6C7D5C2F6200A85FFE9CCE2B2C3A6,
	ParseNode_CollectIfPtgs_m8D317A1EB550DBBC7689A169A1B0F67BB840DAFE,
	ParseNode_IsIf_m8599172DE8A769AD43F7A7DFDB61FE6B0D5E4A87,
	ParseNode_GetToken_mD3800B2BFAAE6A27E7DCD4C40EA57F1EA34AB473,
	ParseNode_GetChildren_mD263AC2618DFB32978AD70AD5DE1208D9AA59BC1,
	ParseNode__cctor_mD10A7DFCB9EAD71A3B672914148F26442C768F2A,
	TokenCollector__ctor_m37A149E0A2D3BA54C70ABDCA174B1ECA7227A087,
	TokenCollector_sumTokenSizes_m5BC3C5D7C196F7F23A43686F13622A90134EC89C,
	TokenCollector_CreatePlaceholder_m70E20568ACB06940F5166E727E139A6EB455FE22,
	TokenCollector_Add_mF782C603CCFA717FA0669320FCE8CCDCEA16AE93,
	TokenCollector_SetPlaceholder_mEB46F9A5DC8C65CF087634F2598962C9004B8AAF,
	TokenCollector_GetResult_m9255D5F9DA567183FF5093AB9F463BD3FEA72985,
	Ptg_ReadTokens_m027D788025E61243FE491099B3C721A04BDE8D75,
	Ptg_CreatePtg_mA23F361D4DFEDAC283F88D0760F9314D3E057714,
	Ptg_CreateClassifiedPtg_m103732D22241DF012AC862F2490B4042DE6252D4,
	Ptg_CreateBasePtg_m0E733A91A7E8019AB77592BBF72121BBEA97A723,
	Ptg_ToPtgArray_mABDB65A07D0292FA03539311D989093BFC5533FC,
	Ptg_Clone_m53C9FA081C6B4DF8B8EE12E7F049C449DA593A35,
	Ptg_GetEncodedSize_m4837F62AD9D858CD8E56D438BA6E4A0769AE2820,
	Ptg_GetEncodedSizeWithoutArrayData_mDCF7A3EE7F85C2CCB06784CD15A200C2B7A9A80D,
	Ptg_SerializePtgs_mCCA9085233798F636AB38D7DCF514558436145E0,
	NULL,
	NULL,
	NULL,
	NULL,
	Ptg_ToString_m8665E41D057F48DFC91CB6912F4D9E1F4CFE3916,
	Ptg_get_PtgClass_mE87396305D94739E6735735EFAC8BA82FFC1F925,
	Ptg_set_PtgClass_m4994637286E7D4A3B842D6C2C9D1492AA89EDCF0,
	NULL,
	Ptg_get_RVAType_mFCF3CE5E1BDA8B65A85A3A8DB47592C376474377,
	Ptg_System_ICloneable_Clone_mF1132B72DCC4D8E2409C8C5912AB6106223548C7,
	Ptg__ctor_m0915325ECFFF03858A8FAD9099BAD7EE5C2F998E,
	Ptg__cctor_m835C4059A388435F9F6976D3CAC0B925F4943777,
	NULL,
	NULL,
	OperationPtg_get_DefaultOperandClass_m3F8D1FAE07A60AED59344A67E50DAB2D75F7AC70,
	OperationPtg__ctor_m84079E2B621DE6792A0E1652209F4F292EEADAF2,
	AbstractFunctionPtg__ctor_mA9D7865B64F7C07CE50700A41AFF27D348378454,
	AbstractFunctionPtg_get_IsBaseToken_m9E12B83C2B6929E851E20010822997B2B4EC6400,
	AbstractFunctionPtg_ToString_m0E62590E987FA5DBBCEDBCEAFFAF7BF57AE7A619,
	AbstractFunctionPtg_get_NumberOfOperands_mFAD01192E8D715FE8D3B3394B0BD7D83C23F0ACD,
	AbstractFunctionPtg_get_Name_mF20C7BDCD2F3FAC0691743949B31BF088C460A15,
	AbstractFunctionPtg_get_IsExternalFunction_mE8F681EAC1E7ECB6369251FAC8830E535BD4912C,
	AbstractFunctionPtg_ToFormulaString_m8549803B5F37CB3579EBD2F50015E0CDEAEC9460,
	AbstractFunctionPtg_ToFormulaString_m53F05D484F32C8566650DC2AD674CDC89891739D,
	AbstractFunctionPtg_AppendArgs_mD7A3F52BD4A92794EA13FE6E8C5A4F6AAC7BDBF8,
	AbstractFunctionPtg_IsBuiltInFunctionName_m2B85DC19B9FAA6C7202D0C213F2018983FD37D36,
	AbstractFunctionPtg_LookupName_m9E69C2E383D0EDBBE7D35B8F75B7899FC82E011D,
	AbstractFunctionPtg_LookupIndex_m7B0BEE60E04C52FC6F6B3324EDF5FD6ACE715462,
	AbstractFunctionPtg_get_DefaultOperandClass_m9698CDBF1DB2C4EE551330542C912EF8D0049B34,
	AbstractFunctionPtg_GetParameterClass_m70E8347D2966F60896B596AF371A7C1B7FF4EAD0,
	ValueOperatorPtg_get_IsBaseToken_mEFDE7C9A0C2ABAF61728E8CD3E11D0984073B029,
	ValueOperatorPtg_get_DefaultOperandClass_m21894AF2F09A1B8AD4ED2CB7336DB5C517F246F2,
	ValueOperatorPtg_Write_m6D83074BF75DADBEB9D9F1CF27CFB449AD5876C3,
	NULL,
	ValueOperatorPtg_get_Size_m9A33BDCC3A0C26582635D2E262E6AC6E2483CA59,
	ValueOperatorPtg_ToFormulaString_m4A8D9363BBB896A303320EAB252F2134F12535AE,
	ValueOperatorPtg__ctor_m7D6F367A7F0E3E49092847AEBF259B1278EB58C2,
	AddPtg__ctor_m4DF480028748B84007CAD928E34245E70E2CCFC4,
	AddPtg_get_Sid_m633C0CCE749BE84D7567F3EC2A8ED42123C5BD7F,
	AddPtg_get_NumberOfOperands_m174FACEFF3381FF2AD07192B608792804BE4C94E,
	AddPtg_ToFormulaString_m9622B436943992F22C240C9C33998CAE72C4097C,
	AddPtg__cctor_m2914BCEC07AC06AB107D866123F2FD81C16BE0E1,
	OperandPtg_get_IsBaseToken_m7CECE041B6934ED688B818AA5BC03E4955AE1EE9,
	OperandPtg_Copy_m86124DA7827EBD1A76BD3932C4FEF81B8B88DE63,
	OperandPtg__ctor_m417B39AD4DF7A1FC6B691A3BF015F1A3FED20718,
	AreaPtgBase__ctor_m869233DD0E3BCF4EA9736C9F1CBDBBA131782B55,
	AreaPtgBase__ctor_m1CDAA664389A8C04CCC94BABB1971119195944AC,
	AreaPtgBase__ctor_mBE221B60305160301DFB5168CC001DC79263AD7B,
	AreaPtgBase_ReadCoordinates_mB5BF8D2E14A51C974D529854ACA3386B04869BF2,
	AreaPtgBase_WriteCoordinates_m7176B2763BB642CCE5A99420171F27DBD46AF9DD,
	AreaPtgBase_get_FirstRow_m44D07AD5388447B93F07127195F52739972B2DA3,
	AreaPtgBase_set_FirstRow_mE6707980FC6887BCE32E917DC24DC0692D07B102,
	AreaPtgBase_get_LastRow_m3F411334B85FC1E981054D7813C9F7E00BA1AD34,
	AreaPtgBase_set_LastRow_m0C396F10EAB203DDF15195C7728670A29EB8B0D9,
	AreaPtgBase_get_FirstColumn_m73896B03BB406AD7C9A272D2B2B26570BBC8F213,
	AreaPtgBase_set_FirstColumn_mB2CEE9A15E9493E8FC32F001AB86BBB2C6B211EC,
	AreaPtgBase_get_IsFirstRowRelative_mB3FB872B40596A5C33D44C459E9487A213EBC4D3,
	AreaPtgBase_set_IsFirstRowRelative_m977B982F6B627E5364D155F3C542E0786ABED964,
	AreaPtgBase_get_IsFirstColRelative_mAA9D66FCE2529D5A28BA3E5F514B36723ED910F9,
	AreaPtgBase_set_IsFirstColRelative_m9AE04069219190FA64433B8ED784614CDDF92DEE,
	AreaPtgBase_get_LastColumn_mBFBC0F0691E8ABC920F86F0F319C688CDD19573A,
	AreaPtgBase_set_LastColumn_m6FBDBC76343730F841CE698C353828D6D27057DC,
	AreaPtgBase_get_IsLastRowRelative_mD554BDE275050E386C91C415CEB39C07B1FD98CC,
	AreaPtgBase_set_IsLastRowRelative_mE36452A30FE9341C3ECF98A3FBEE2903CFBC46EA,
	AreaPtgBase_get_IsLastColRelative_mEDE524022567B6360512E592D0E94FE14D605662,
	AreaPtgBase_set_IsLastColRelative_m424CABDBE3A62050B7F6644684CE49C89AF910AE,
	AreaPtgBase_ToFormulaString_m265E05F14F615DB2C8E52770C5D2F68B2BDD30E9,
	AreaPtgBase_get_DefaultOperandClass_m12D33C5173451EFBCD1EC0D7A9B5A4B42B1C5D5F,
	AreaPtgBase_FormatReferenceAsString_m5E85E21CB6FED757299A4DF852931A39126D0349,
	AreaPtgBase__cctor_mED087609FA476008D65E9C0E494A8B611F2BD98A,
	Area2DPtgBase__ctor_m23DB8669380073AEAE9502D7754D34C58590B3C3,
	Area2DPtgBase__ctor_m32D31588FED59D14DDFBB044CF1816E16912FAA1,
	Area2DPtgBase__ctor_m6A9650BA6BEF783768286BFB79C6AC457A943DFF,
	NULL,
	Area2DPtgBase_Write_m1BDF63E2DA02705175DB832632CE5C3F25F99F10,
	Area2DPtgBase_get_Size_mEDD126C99C0B83EBDAF58A0E7BD1A21D91A4308F,
	Area2DPtgBase_ToFormulaString_mA324A8F005781595BDA957DDA869F874423697F0,
	Area2DPtgBase_ToString_mEFEF18ED39DAC55A8BE709332B52E1024EC5C39F,
	NULL,
	Area3DPtg__ctor_m66FC30CC6719B41ADEA958B61BCE7A8212B60FF6,
	Area3DPtg__ctor_m4344D85A4C5179B43EBC75D80A7E6D0FF3B93470,
	Area3DPtg_ToString_mA90FD5D80D87705C94178C8E0878EE5219CABFAF,
	Area3DPtg_Write_m892A1EE0340829DE01689D4381BE94D24CD7E7EB,
	Area3DPtg_get_Size_mE0212C5FA2C16F9D17495370CB443DDACE110DFA,
	Area3DPtg_get_ExternSheetIndex_mF6650091AA7FE96A2AC3ACEF426C09E1CC4B27A8,
	Area3DPtg_set_ExternSheetIndex_mC723C4B353126EA7AEAE4FDF8AEC0111A1DDF2B2,
	Area3DPtg_ToFormulaString_m5217B11AE4155DD4A2799D0802B9A85689C64E75,
	Area3DPtg_ToFormulaString_m63FC7CD4A38DBC318193FD099A64165B7035E27C,
	Area3DPtg_get_DefaultOperandClass_mC2D623A1255D707F45816AC06152A2F3CD0232CA,
	AreaErrPtg__ctor_m47C598A779B2A45F63BBDADE5288A77CBDB8BD5A,
	AreaErrPtg_Write_mE42FA3B5C1DC2CECF9943BA9BB49FA86A6A17058,
	AreaErrPtg_ToFormulaString_m5D87950804AFA3D9EA0D85C513D858EF4CC77CD8,
	AreaErrPtg_get_DefaultOperandClass_m86BEE7EAED9E109D0D064EB9436E875BA6DEB5FA,
	AreaErrPtg_get_Size_mF084C2BDBF3C0998F21132852B5483344B8923C7,
	AreaNPtg__ctor_mFD390E29C5BC11DE0DF2484FBAD188D708379EBC,
	AreaNPtg_get_Sid_m3A23AB53BE1476E8AE061500633F25EF51772CC0,
	AreaPtg__ctor_m990D9D141773CFD1681236E5CB01D3E1A1B29036,
	AreaPtg__ctor_mC916875EFAE8D739033F8F2AC92C3FCC5493632C,
	AreaPtg__ctor_mFB2FBD8624811E5D2BB3CDE7B0F889D4CB6665DF,
	AreaPtg_get_Sid_mBEB621AF2AD101E5E31F97EFD683AF7FCC0DC5FD,
	ArrayPtg__ctor_mE69531E2414F65A383B534C27F286212761BB331,
	ArrayPtg__ctor_mCFE677A8E3F24B10548743DE76EDE8A845B154CC,
	ArrayPtg_get_IsBaseToken_mEF57847718560DDB28ABE2B3396F4DE76E7814F9,
	ArrayPtg_ToString_m8A584FE709E51C91CC94E757749D1ABF2D5BFDDC,
	ArrayPtg_GetValueIndex_mABCB4A69BE737F79539C83B3A745C2B5B942760F,
	ArrayPtg_Write_mA1B224A53C4418C31918835B168CD945C9C4E319,
	ArrayPtg_WriteTokenValueBytes_m03F1596D8A0A286CC565993C29DE0F0B92686C5C,
	ArrayPtg_get_RowCount_mD5ABBCF393428E3914846159C7116843E9DF801C,
	ArrayPtg_get_ColumnCount_m3116C129AA425617E0F4BB3947FC0D65BF4FFF97,
	ArrayPtg_get_Size_m9EC58D80C910087B2836DED8DB98E86D491BF7A3,
	ArrayPtg_ToFormulaString_mE65C6E7FBE77D98AA93E0C2E0802C95670FB57BC,
	ArrayPtg_GetConstantText_mEB901D5BB86B57FF5F51DA46C3C0A8A7D6D8FAA9,
	ArrayPtg_get_DefaultOperandClass_m30F355EC9E7970D897D3E7A540A30DBEF55D6836,
	Initial__ctor_mE9F24CD3DB9CF8DCB6CE35A22641928D507DA666,
	Initial_Invalid_m49A38081C21CBBAAE0E7801C745EBC1DF3476C48,
	Initial_get_DefaultOperandClass_m59B454FF4ADB779252B4363B40959ECFCA2C4B37,
	Initial_get_Size_m3B62D4C4792FB960CDF0A32587B5FF66F4136734,
	Initial_get_IsBaseToken_mFB1F8F9A3E1B2A320A24795F8108D64E951E1B26,
	Initial_ToFormulaString_mDADC9DDD60F094351E9120FAECD00773BC9B2BD6,
	Initial_Write_m807B4A43AC02CF870D03CC66281C9EC58A4C6206,
	Initial_FinishReading_mFF2F9581B23D36158EC136EA62AD1044D8B6D6FF,
	ControlPtg_get_IsBaseToken_m73EBDD798E110D703381F92761D8ADB57FD9674A,
	ControlPtg_get_DefaultOperandClass_m9D576F60775D6A84062FED5632151E0CCE045BE4,
	ControlPtg__ctor_m5EB4B239F30F10D518C51108593445197C827BCA,
	AttrPtg__ctor_m5B7F4EA37CC98BA3CD7F49FEF219DA6F81104B5D,
	AttrPtg__ctor_m491EC7B79BF2F75109B8CE59D71037FABA0D9434,
	AttrPtg_CreateIf_m9B4B74726A8B0DAA732A16D5F9BF8AFC5C628C3E,
	AttrPtg_CreateSkip_mD9DD769FD8650DF1A068D5C2C4C3CC343A27BA51,
	AttrPtg_GetSumSingle_m7895B0908465DB1BC72F0425AB60CAA2F798BCD0,
	AttrPtg_get_IsSemiVolatile_mF7DDDCECAB3229D456687391F8479BA9FE0CCF35,
	AttrPtg_get_IsOptimizedIf_m561F72C33376373DCCE7AE4CBFD2CA7A344A7827,
	AttrPtg_get_IsOptimizedChoose_m4F8F6CAD03790DD2A307A87214D41A8C73FFEEA8,
	AttrPtg_get_IsSum_m7D9DF3792629BBA98EE8E297F7550B744BBAE60C,
	AttrPtg_get_IsBaxcel_mD84C0CF7BBE51EB45ADEB9B5A3197877385CB358,
	AttrPtg_get_IsSpace_mD8CDBF143DDE57ADF09CAE897EBD601DA01EE29D,
	AttrPtg_get_IsSkip_m29D941D9FB92D405C9F25327C8C8CF247DFF0AFC,
	AttrPtg_get_Data_m41CC582758AC242B741F47096E6018AEC0610BA1,
	AttrPtg_ToString_m33B7D5123FA21CD9A01FF2C943F0F5C3C5F51E43,
	AttrPtg_Write_mB438B7CF3786116EF48A12305B2CECDC139E5A74,
	AttrPtg_get_Size_m25372E31D02FC7FC0C9A38F1CE5BC9093260FC9C,
	AttrPtg_ToFormulaString_mDF8374EBF4884668A8DFC9FCE63F75E23394280E,
	AttrPtg_get_NumberOfOperands_m5A4FA3B007408DF640F6F6654455D46C0BF302EB,
	AttrPtg_ToFormulaString_m45ECEE5DB7F3E10A186A02E5298EBAAAAA77A01C,
	AttrPtg_Clone_m51CB8CFB8BD24E49EF2EC96CDCDC70D847D6D0B2,
	AttrPtg__cctor_m021FA6FE8816B1ACE9197AE108E8385F4ED9C69F,
	ScalarConstantPtg_get_IsBaseToken_mE7EB7CB4A9DB54935EB25D6F477A73CB4BD00461,
	ScalarConstantPtg_get_DefaultOperandClass_m576653EDE5527DB08C30EC47955A925CA584CC03,
	ScalarConstantPtg__ctor_m9C4E5F9F50C36FE3DB2445CF2642B171AD705E53,
	BoolPtg__ctor_mC8B69E0692AC36098F832AEE0461127624BAA1FF,
	BoolPtg__ctor_m879EC0BCC321A48B26B6D10555513EB5F9D6AB71,
	BoolPtg_Write_m940A6A5415B16062E2AB3C353B719122B1CAB3A4,
	BoolPtg_get_Size_m8EEF1368E76148165E3D5813AB324B71E35C3434,
	BoolPtg_ToFormulaString_m9AE9265C2F253403BD40C7F0437D2E45BCB253EA,
	ConcatPtg__ctor_m2A054415BCFF9C15324EC63759FCF43279E2C547,
	ConcatPtg_get_Sid_m11BCD1D1295605D8B400464B54F93AB01137B6B9,
	ConcatPtg_get_NumberOfOperands_m7F0745E548507149897B0C5CE782F8043D5FEDF5,
	ConcatPtg_ToFormulaString_m8C89DBA2E3B2EE0A40A9431E4EF4A31FEBA1A69F,
	ConcatPtg__cctor_mB363DB620F141F98687E49C89D8D2560421DB79B,
	DeletedArea3DPtg__ctor_m6EF521126B01A2EDB0BC27B88E9A3F26C71F1B9D,
	DeletedArea3DPtg_ToFormulaString_m6A796166617F11B2A1B04C0C01C30154045777FD,
	DeletedArea3DPtg_ToFormulaString_mD1B51292E22A7AD2260B1B1A376DA18B332BE7A2,
	DeletedArea3DPtg_get_DefaultOperandClass_mC2D2D3C6819A27CDD92178CD7AD7A5C3CCA34962,
	DeletedArea3DPtg_get_Size_m5FA0090DA3FE821438485BAD13FD2DF7F496FD81,
	DeletedArea3DPtg_Write_mB38DFFF5DF25132B87F149BEAF6D78D6A3EF42E4,
	DeletedRef3DPtg__ctor_m3E2A836F3734E1B50342019980BB26C1BD43A021,
	DeletedRef3DPtg_ToFormulaString_mA29F22BD5DF4E0CB4A6027F58E517761DBBB1202,
	DeletedRef3DPtg_ToFormulaString_m3E9D01E4A0C11BC475AE8972EBBC1955B91E4EBD,
	DeletedRef3DPtg_get_DefaultOperandClass_m75E6EA9A314D28B6D8B511A7D4D3BC5EF59070B0,
	DeletedRef3DPtg_get_Size_m9AA84BBC8343269FB3E0E0796940CA16061F8DAC,
	DeletedRef3DPtg_Write_m8FF623E7B078C59AC9C6C1FA68F71A0C468812E4,
	DividePtg__ctor_m723933048829DC88F43796458DF4948FFD728AA5,
	DividePtg_get_Sid_m7E4F9F8A63147AAD5AE74FB338F08C50F9642D7A,
	DividePtg_get_NumberOfOperands_m6A340A5FCF0A5F664565BF48C5BEDFD72744B55C,
	DividePtg_ToFormulaString_mC7A6B33708574CD986E92F070AB6B06AD2F97FB7,
	DividePtg__cctor_mB8D74935473A3FBF4F134DF08B7A95FADB563DDF,
	EqualPtg__ctor_mAE4EF77D3C21A632A77B4894D05B750B985B2B6E,
	EqualPtg_get_Sid_m7B694A93BDA8B3D85B10EB8425445ABCE6CC4BFD,
	EqualPtg_get_NumberOfOperands_m25E0F33EBBC8FD445713BC314461A4F6A55802EA,
	EqualPtg_ToFormulaString_m67B8FF445AAD0BFB838E936DBE67C569FF7BAC84,
	EqualPtg__cctor_m540E26CFB1DD96360D30945E69D0261A71F6BFBE,
	ErrPtg_ValueOf_m2E63C5E71C67E94DB0458D92D2766F657B879118,
	ErrPtg__ctor_m4B0C7672A84BD21F1F6DD108477F1F1A3D369426,
	ErrPtg__ctor_m431F52E388DF57CADE747EA81EAF2147EC8D80E5,
	ErrPtg_Write_mF7384BB304E85907D3084EE8DB267F91A9D1C85C,
	ErrPtg_ToFormulaString_m3ACACCCE35EB73FDBF5360C9FB852198CF75BA72,
	ErrPtg_get_Size_m06661FDF57D23D49DDDB783334858BBAB369587A,
	ErrPtg__cctor_mF147161A942B92CA89FCDB282C19C8EB3A336DBA,
	ExpPtg__ctor_mB8B5CDC80DE308CF91A7FB62F85265344E7A69BE,
	ExpPtg_Write_mCA80E85815AB7223BAEC88014E77E175325221ED,
	ExpPtg_get_Size_m829D43F0A31FF1C849C268F1A0EAAE3166195ED2,
	ExpPtg_get_Row_m6C078D7586A2192B89114D6441A2CD2A3BC311B4,
	ExpPtg_get_Column_m28118A28B4539E7A7F25FA857222847784C0764D,
	ExpPtg_ToFormulaString_mB35A6E530133E13217B0B0F9372E08C30A635911,
	ExpPtg_ToString_m685C68EDBBA3645313768EF18D17A6045A765D3D,
	ExternSheetNameResolver_PrependSheetName_m4BDDB7A6728C7EF8A16FE78CF3C18985E43C7D7E,
	FuncPtg_Create_m4B7E645A4000FDD9C18769EFC01969C7E5CD8051,
	FuncPtg__ctor_m07D78592E5563AFF818D2129381A87566C9CB648,
	FuncPtg_Create_m1E6E689736498CF9E0DB085358399E9DE0D4E7BB,
	FuncPtg_Write_m56BC7A08A87B3F345176AD211EB0F9EFDA2FCF08,
	FuncPtg_get_Size_m4E0FF689DB5A3AEDD222FA91E892A7B9519AB002,
	FuncVarPtg__ctor_m7B95D4B182FCBF93500D6BEF9FD1E18CA34AD11D,
	FuncVarPtg_Create_m1DE3EBF1343594F5D749DB050154360F98459C7F,
	FuncVarPtg_Create_mEC5C8B9FCC6D626562C862AED63B669E47E813BC,
	FuncVarPtg_Create_mD000C571F3F96B5E752B2EF41C0558C0A841B2C5,
	FuncVarPtg_Write_m3CDD2E9431888795FD271AE2A847B439089F3D7C,
	FuncVarPtg_get_Size_mF4BDEA807BD6F3756247ABFBBD8CEB41C01CA67E,
	FuncVarPtg__cctor_mB4C468FAE253AED34FFB74E3C64A452D8642918F,
	GreaterEqualPtg__ctor_m3258051F6265721ABF3AB09456BFBF127B53D21C,
	GreaterEqualPtg_get_Sid_mE8089C3C61FE1D445C45F4E18958264B9DF49510,
	GreaterEqualPtg_get_NumberOfOperands_m92703B808AA6734459AA5141BCEE093C442D2BC5,
	GreaterEqualPtg_ToFormulaString_mEB92A7A5060DEBFB357C168E51D167393A9BB364,
	GreaterEqualPtg__cctor_m0B48C2EB5E56E7EC67AF87666E5B80D497E6031C,
	GreaterThanPtg__ctor_mFC29B29CC16904C21730B88980A4E9650AB632CD,
	GreaterThanPtg_get_Sid_m2967150BC486C429BDEF63C6D0481EDE518D54DE,
	GreaterThanPtg_get_NumberOfOperands_m8C017120E896C8547EB83AAC9C8660AA19758BD7,
	GreaterThanPtg_ToFormulaString_m1CCDC221154A37EAB636CADA4237BB4DF5C45A7C,
	GreaterThanPtg__cctor_mF4BB9D836426C6B50A590D332CAA6194074DEB91,
	IntersectionPtg__ctor_mE79F00CC93677B0DC9288A0E86E0BFE44D249049,
	IntersectionPtg_get_IsBaseToken_m616EF909E6BF642C1A70F7F63FFCF729015A80FD,
	IntersectionPtg_get_Size_m1FC9DACD14C989D90934958B2DC203295E3D2759,
	IntersectionPtg_Write_m8CA4EEE97558C9CF9D0493F81D1C2EC3BD8D4CB3,
	IntersectionPtg_ToFormulaString_m00655AA3A7F3077C9B3B4FED82ACBB1DDB454989,
	IntersectionPtg_ToFormulaString_mA1B8BD7119F6173C49E98A2F44C557B1654EA6A9,
	IntersectionPtg_get_NumberOfOperands_m6F9BCF69EA1E572AB879223B2FF6AE8D507F7CA4,
	IntersectionPtg__cctor_m16EA527A97AA92C261C2A14DEE83A7429988C020,
	IntPtg_IsInRange_m51549EFB756D1D44F9D3637B8464ED485FFE3CF7,
	IntPtg__ctor_mE52010129A048BACBE24ED9B4D8B09BBD519F6CD,
	IntPtg__ctor_m40BDAF2591BC4DE414BE6677BC6C943AE3911DEB,
	IntPtg_get_Value_mE136F8201D92006780E75BCFA51E51AB5D33E38B,
	IntPtg_Write_mB852D52CB4BB1EC502B9FD5C237951BC2699E98D,
	IntPtg_get_Size_m8B191FCE427AB321209A9A0C9BE73BBB5397959F,
	IntPtg_ToFormulaString_m65EE6394E47A690EC78015A12C7FDE9981AFC941,
	IntPtg_ToString_m72BB00CD5526778158F53FBFE0DA1C2DFDF6026B,
	LessEqualPtg__ctor_m89040A0EDBAC284404DFC0E2A8D85EA3FCEE4BA5,
	LessEqualPtg_get_Sid_m86FC7BA3A0DE5CF16ED10A269981E1533B0C8E65,
	LessEqualPtg_get_NumberOfOperands_mC91114DFF12770D334376639565F3170D2CDF160,
	LessEqualPtg_ToFormulaString_m539560CF676FA206D54A694A78061BABAE3F5114,
	LessEqualPtg__cctor_mF51EE80975A34FB5EE7820AD60A9D78604DB1B78,
	LessThanPtg__ctor_m44A025A6E5BA5467524522BE01B1844C675B9049,
	LessThanPtg_get_Sid_mD13EF9A714BBBAD25EA55FB11867321D8B55C76B,
	LessThanPtg_get_NumberOfOperands_mC6514C08E5D59087BC7B6D0EA622CBC0CE7D09D7,
	LessThanPtg_ToFormulaString_m7B319B98F4C325793AE566A56C5DBEC48E7BC630,
	LessThanPtg__cctor_mC9ED5EE2920A82BB915C9B7CFA8A1E71961D9F5A,
	MemAreaPtg__ctor_m9C175ACAFA04CF0CC843ACD445310B62399E463D,
	MemAreaPtg__ctor_m0CE1184C37439F924088631863E95FBEEA2D0898,
	MemAreaPtg_Write_m5E094F84E747BF846CAA46CD56B33D6AE3FDFB43,
	MemAreaPtg_get_Size_m6491721CC421662D71447489DD5E32AB428EDDC1,
	MemAreaPtg_ToFormulaString_m69DAD1E1454F086ADDA1EFC96667DBEA7B4C84BC,
	MemAreaPtg_get_DefaultOperandClass_mACE078FC7DF3AAD733BBCD62412222E5E019A000,
	MemAreaPtg_ToString_m1784065EBA580F547DEE077F91F35F3C75D71EED,
	MemErrPtg__ctor_m22BE1FC4FEE1C114150FE5FBAA10F183578482A0,
	MemErrPtg_Write_m2190A83AD3A9EBB5F6AD4BB0842C9C97A2DB9080,
	MemErrPtg_ToFormulaString_m3848CB189A2556412C2D0068DE8769C41A756A48,
	MemFuncPtg__ctor_mC3967FA59060A633861E323620F4ACA5FC2BE4D7,
	MemFuncPtg__ctor_m7059EBC983A69A3EC4F2CE6E71FE7D1A88D14B66,
	MemFuncPtg_get_Size_m480A6367A4E15798D7CAAD7282751BD12ADF5281,
	MemFuncPtg_Write_m703B662F9C2B5624815014E935937FFBC978CDDA,
	MemFuncPtg_ToFormulaString_mDA23E2BCA8823B07C2E56F4AE63FB8F33446F893,
	MemFuncPtg_get_DefaultOperandClass_m000F157939BE94EDEB6AD2FE0441DCF4D8E54720,
	MissingArgPtg__ctor_m34F7D2A481AD3E89F382F22BCF03C0ACD88DB3DF,
	MissingArgPtg_Write_mACD4D14E2C40A8875FE86B47DE7639D450CD0B3A,
	MissingArgPtg_get_Size_m6BCB0C87A86B1A34E076780B27705ED9A8593A7D,
	MissingArgPtg_ToFormulaString_mADCFC7A1784842F958ACA4E77348DD967F0288BB,
	MissingArgPtg__cctor_m7C5540806CCB6E5E12B07AE926B6AC4B9BB73291,
	MultiplyPtg__ctor_mA36C2367C803C561A03D8C7C4BDB34B8E5482650,
	MultiplyPtg_get_Sid_m5F0702132FF12831751A2B31799E6C0932D18A16,
	MultiplyPtg_get_NumberOfOperands_m0F1A85E560E3D40E16D6677D1A47E1111D748D11,
	MultiplyPtg_ToFormulaString_m2650D38909757C32FEEEEAE5155FD681198549A2,
	MultiplyPtg__cctor_m4F65A901D1010E0E41E6E83BB698560C6FCBD0F8,
	NamePtg__ctor_mED709AFD2487988D704D6CD39572FEA3D50D1B54,
	NamePtg__ctor_m2130067E42BB01B3E1EAF6E88C4FACF1E4809941,
	NamePtg_get_Index_mBE476DBD0577173C9FCEDF17BD048911EA69821B,
	NamePtg_Write_m7E7D383C9A4D89D33E58EFBCD6798DB7F3E2A3EA,
	NamePtg_get_Size_m36371EAB1AED55535554ADFB8655784A450233F4,
	NamePtg_ToFormulaString_m24A66B05F5ECBD41E1C5214B346CFCB6737AD073,
	NamePtg_ToFormulaString_m8BE26F0AB7CC689CFE5313482494A57871B003C0,
	NamePtg_get_DefaultOperandClass_m3424173D4E9B8FAF416C1E6516AADF0649781970,
	NameXPtg__ctor_m5870792E09746836417C70631481FD4DDF72C068,
	NameXPtg__ctor_m10F55461B20008E835765461D92C1262D6072C3D,
	NameXPtg__ctor_mC600250C02A1108874313B7192DD41C846580BDB,
	NameXPtg_Write_mA22E3611E5E59E9EE5AA777571F5CB245A201D68,
	NameXPtg_get_Size_mCF8A906595F9793710BE6D58BBFC4A8D2658DE32,
	NameXPtg_ToFormulaString_m3BEF5B75C31F946091E5C757CC01DE69ACB4FEFA,
	NameXPtg_ToFormulaString_mEE06F315D850D0CEBACF407F23D335E9A8D809BB,
	NameXPtg_get_DefaultOperandClass_mB08CB060EF09212FCC9EAC27FC493E74716B854A,
	NameXPtg_get_SheetRefIndex_mF5E2E65E1479D244DEA2D27657AF642714986AE3,
	NameXPtg_get_NameIndex_mDC70B5D581F996B4DC3D277FE673AD9F08B14117,
	NotEqualPtg__ctor_mD7D87B2549CE5EB9B922CB78283D08D6BCD042A4,
	NotEqualPtg_get_Sid_m56FA2F0605AC7A1A091130CD4A5985C89C6B6784,
	NotEqualPtg_get_NumberOfOperands_mE0B7424CF9B373D9F1B5B737E1E815EFBBDD3576,
	NotEqualPtg_ToFormulaString_m42F96EAA19C27594F67740DF4DA87C2382A23C1F,
	NotEqualPtg__cctor_m597D01547D08967EAAB00308A76EE20FB200F982,
	NumberPtg__ctor_mFE22945A475E496AD37DFBE37F75F5993B2288D1,
	NumberPtg__ctor_m8518FF5D66F93674181E098B402C6D7350A151BE,
	NumberPtg__ctor_m2F806DF7449717F9869A5FA0206A07B980A833DF,
	NumberPtg_get_Value_m414AEEA91CC67A11DEA7CFF6AAB4C076D220DB40,
	NumberPtg_Write_m375D063F51D55183CA32F994B418E6517D887594,
	NumberPtg_get_Size_m76BEFF98391D28F7AD8456BB65686CE35C70CB41,
	NumberPtg_ToFormulaString_m1A161471ABA10F5C3AE994EFCFB261CDEB851DAA,
	ParenthesisPtg__ctor_m50B47035AB95E58A9FC0BD6B1E8F945C553E0CBB,
	ParenthesisPtg_Write_m13D6A5B8C3C15E629FD53876F9B2CD58CF8AD828,
	ParenthesisPtg_get_Size_mC71DB7D373F7D6103A89E75FEC45E2282E956A3F,
	ParenthesisPtg_ToFormulaString_m9C959E43A6BF1A9CCD9F741D03962C5940783E72,
	ParenthesisPtg__cctor_m092006373D05F283E6746EEAE8D33BD833EB7C0C,
	PercentPtg__ctor_m63CC498D93E721AE07C0B65880861310213493C2,
	PercentPtg_get_Sid_mA69F08BCA765F4D6EBEBB44B730FD7536B2C8183,
	PercentPtg_get_NumberOfOperands_mB6B0834194453CEE3720DFB87F6298F42C2FD1C8,
	PercentPtg_ToFormulaString_m32DC6B78151C6E97B99FD0E013DB82340C7338C8,
	PercentPtg__cctor_m1A981623A669EA3E873FB3AF8B25A2DC524EABDA,
	PowerPtg__ctor_mA240EE0F20EA9E93CAEE754D2E18817B7B5C0B0F,
	PowerPtg_get_Sid_m584FF7750E9DB448488D6A0B20742411B34D5BA6,
	PowerPtg_get_NumberOfOperands_mE8DB482CBDF8AC649094D242A84AD95C3405244A,
	PowerPtg_ToFormulaString_m83CC92A7866A01633C38F34DF3785638882085EB,
	PowerPtg__cctor_m23AF0035228428F86E8281C026B20D29CC4BE767,
	RangePtg__ctor_m8F907E27F0E9D87D91FF0E00ED94A9005AC945BF,
	RangePtg_get_IsBaseToken_m7DA17F241C4822CD772251D33653282572E19E7F,
	RangePtg_get_Size_m127189097A7D917CB40EEE7E4100EBCD79DE550B,
	RangePtg_Write_mEFD7E970987B73319B23D30816D71AD0D51AD165,
	RangePtg_ToFormulaString_mE445074FFFEA672AA87B543CC17492AE618D3A80,
	RangePtg_ToFormulaString_mF6F91372D57E9687241197528D1EA42738FAE33D,
	RangePtg_get_NumberOfOperands_mBF6F2A52FB9D2D4EEA568769A761E463E62F3A2B,
	RangePtg__cctor_m1566E1AF3852C2EBEF0EB71C4F3AD5B1BEE2DE37,
	RefPtgBase__ctor_mCD06C55E5EE63543FEE6D674A406C488336EB91B,
	RefPtgBase__ctor_m6632163B5816E5C1C5D3E34AF5D5FE0658D2C655,
	RefPtgBase_ReadCoordinates_m2B7388BF9AF3086D22FC4569F8EC8FF672B7C8F2,
	RefPtgBase_WriteCoordinates_m3754E96E396CCC71C97695947828ADDA93457F75,
	RefPtgBase_get_Row_m79F554E80DDE625AB26C7B66E194822C35F2AFC0,
	RefPtgBase_set_Row_mA5400CA251353461DBB300ED29C2091E94C2B181,
	RefPtgBase_get_IsRowRelative_m53BCEB7F7B615CC22D9F6716E3A2EE6411BEF29A,
	RefPtgBase_set_IsRowRelative_mBD4039C7F4869ABD3812CBD3B1B63B91E03B99D4,
	RefPtgBase_get_IsColRelative_m9392E393FDA0410B3A94C949F02B4A7DA159440B,
	RefPtgBase_set_IsColRelative_m9666E259DE64D68CBB7AC80E6E60E35E4B263E89,
	RefPtgBase_get_Column_mC596B5277A95C27BBDF14561FD163030440CFC7F,
	RefPtgBase_set_Column_m9113F5D2A3121F8142D151E115D6D98A91ED16C5,
	RefPtgBase_FormatReferenceAsString_m6B59D043536822913C53438BBEC14ECB2EFC74D4,
	RefPtgBase_get_DefaultOperandClass_mE327EBEA32911F4B1DA7AA077E010E9A891C3A60,
	RefPtgBase__cctor_mDF6D1BFBA36501C67D612EB77DA767F5F64A8638,
	Ref2DPtgBase__ctor_m2932C99158FDBAF44362733125B5CACCC8B34C38,
	Ref2DPtgBase__ctor_m5C92B260431471FE61A93F4EB0A9E02C3FEFA9C6,
	Ref2DPtgBase__ctor_m97D2E61085D7C77B6EC80AAE90DA75F3F15DC2EA,
	Ref2DPtgBase_Write_mF941CF2B00DB0FE1111F5D1B3238FCEB5BCD0E16,
	Ref2DPtgBase_ToFormulaString_m11F7C20ADC8B3BAA18A80289B5CE49B1E28C60B4,
	NULL,
	Ref2DPtgBase_get_Size_m55F81CD84E0B06CD89621F3D287200E9283CEB09,
	Ref2DPtgBase_ToString_mC663DE1A98D231E42E89E5679E72C3DA308C9B3E,
	Ref3DPtg__ctor_mBC70058378D3C76A9EDE5F20B19567BF7084ED0F,
	Ref3DPtg__ctor_m786918885CDD73662311FB20A5D65FA4911DBFC0,
	Ref3DPtg_ToString_mBE3466452EEB4B5C16175F10CB725567F7DA2E3B,
	Ref3DPtg_Write_mDF6DAD9F340BB682DB517F001B6FD3D4AF513A90,
	Ref3DPtg_get_Size_m46A8837A68379E92C13A37FEFE3F18B900AC4194,
	Ref3DPtg_get_ExternSheetIndex_m17CFD3E5B6CB6532A67FEE671D0D9918B134492E,
	Ref3DPtg_set_ExternSheetIndex_m6A197FDA5C280177E9937AA1EE3101A918AB1F5D,
	Ref3DPtg_ToFormulaString_m9F8CCF0F3C2A4B8DAE7EF03C70BA748033D40933,
	Ref3DPtg_ToFormulaString_mA6C95D3B8E4DAD5B2D019C084EA95C9801700793,
	Ref3DPtg_get_DefaultOperandClass_mC67499BB2BE75935E6CD043A315EF5B0F4262B07,
	RefErrorPtg__ctor_m144E0FD6A6640B67D574D5FF5126F4E58E55C21E,
	RefErrorPtg_ToString_mE164732DA6C458DE12A7CCACDF8BECDC58948868,
	RefErrorPtg_Write_m7EE4BD023D71FFBB404A0DA3D7F8DA08362BCA00,
	RefErrorPtg_get_Reserved_m6A37614601F4D78BCD141D0399BFB35C4C7DA8C5,
	RefErrorPtg_get_Size_m67452D35CF904766CFAD2A847E7982E7B065C559,
	RefErrorPtg_ToFormulaString_m6A1CA5E16DD22FC21E43DA8D28D272E2B965022A,
	RefErrorPtg_get_DefaultOperandClass_mA90C6B8646480046765CF8CB3F62A60F94EB0725,
	RefNPtg__ctor_mD0DEBEE8D8443D9B7F911289AD9EBD62011067EC,
	RefNPtg_get_Sid_mF2C6D124747BA74F0494DF0B92D0887513DC41AF,
	RefPtg__ctor_m31091C5E00A22687AB73FC7D326CC76CE4DF0BC2,
	RefPtg__ctor_m1840930156C019DCB868E02F33A713D6507A986F,
	RefPtg__ctor_m2C17DED88D445D95BFC960DA3D0A381AC7EA5DEF,
	RefPtg_get_Sid_m03B950F8BEEDE490C8B856D9B8FA6C679A9EC08D,
	StringPtg__ctor_mDD2F209B9D7F5D9125A8F1F8A1DA849F903E7598,
	StringPtg__ctor_m69B54E1CF1C92DBEE9908430206308B6DA967F20,
	StringPtg_Write_mA9FF22E16F79FB3E8E5527661ACFA0956ED15209,
	StringPtg_get_Size_m05A38E73046BA5F00563BE913754B4840C397E28,
	StringPtg_ToFormulaString_m7E64D03CBB702FE9259ECD581770AA599B5BB7F7,
	StringPtg_ToString_mB9E77F1BA9BE117E0B67A9473A90FC03385899B9,
	StringPtg__cctor_mAB36BCE60E7B43C9E512FEB9451CD185303320AA,
	SubtractPtg__ctor_m92BD64E2292076E20DA068460C167B8A7EA17A2D,
	SubtractPtg_get_Sid_m632842229595B69F6A2013E3F829EC3DBE73CAA0,
	SubtractPtg_get_NumberOfOperands_m4E5A44D8C3B76964C7E453F7A9F3A2F4256A25C8,
	SubtractPtg_ToFormulaString_m8D38A7F1C2ADDFDFD389A81277C930ACF8E33E99,
	SubtractPtg__cctor_m5F6F5DC4720F3EBEB91D7166C315D23C2B4ACD29,
	TblPtg__ctor_m94971FA1DB5E4D3B613785443CE2B7908DF53B7D,
	TblPtg_Write_mB5D2B2215D3ECA98EA564740F221D340EDCCDE13,
	TblPtg_get_Size_m817AFE04B726FC96EAAB66DC8008099B532BF214,
	TblPtg_get_Row_m36CB880D3353772181AECE6AF874862F4E43D53F,
	TblPtg_get_Column_mFC4FF9466B369B94C300C60B7EE6BB55A8CE4877,
	TblPtg_ToFormulaString_mE999C51986D899A9465FF1C36B90FDFD70C8EC1F,
	TblPtg_ToString_m43C7FA27BCF8B227171C0EE9D5B9A6CFAED6CA2A,
	UnaryMinusPtg__ctor_m649DAC719E7B77897D3660BF90A65CBC073FE2A8,
	UnaryMinusPtg_get_Sid_m6FD1089EEE7047A632AC617A28F04D1171B6F0E4,
	UnaryMinusPtg_get_NumberOfOperands_m0B3B64C5CB403B11FCC55024BA70C2BC43F93E6C,
	UnaryMinusPtg_ToFormulaString_m97C315B319E04FB05945F2EC13DDB8EB5A1A9E15,
	UnaryMinusPtg__cctor_mA70BAC21219765255E5E6AE21F090FAC432AC47F,
	UnaryPlusPtg__ctor_mF316973EB42D5655EF4F129270DFC169444EFF38,
	UnaryPlusPtg_get_Sid_m2D82EAD4F5FE4C5AFDAFADCFD222BF4F372064AF,
	UnaryPlusPtg_get_NumberOfOperands_mB04C50EFAB4620F6228B7ABB214D40640E998202,
	UnaryPlusPtg_ToFormulaString_mC3AE2389326298BE51B1AE1B5D4A1445D1549242,
	UnaryPlusPtg__cctor_mFBAE1AD75D76CA2C247968B9624347A55F79C649,
	UnionPtg__ctor_m20D17B7700CAD1350DE2433B18DACFF9FBA6B7EF,
	UnionPtg_get_IsBaseToken_m551F919F4A513418A8CD771BFB475760AA299074,
	UnionPtg_get_Size_m96B347CD302F0C1991984A2E70BDF3148BF4BE39,
	UnionPtg_Write_m01BCD3E246C86A22E3B006A4009FE554E8C2162E,
	UnionPtg_ToFormulaString_mC7C2DC045004140D377D7CDAF6F9FD007E7BB1D4,
	UnionPtg_ToFormulaString_mB925AC8C527CED120FD57A6108883D1C207976F0,
	UnionPtg_get_NumberOfOperands_m29882FD8570040A6B2010CFB6178C66DA5D29B9A,
	UnionPtg__cctor_m8E227AB82DFC6D51A75898E74950EDEA29B05498,
	UnknownPtg__ctor_mAB8A1741074512B1B9F10F3335A6DB1ECA6E8571,
	UnknownPtg_get_IsBaseToken_mA11A38AB7C039360D041AEB065A28466BBDB9C9E,
	UnknownPtg_Write_mFF8209D7B21AE77D72EEA488370E30B0CC03FFB2,
	UnknownPtg_get_Size_m73036F7A04C8D442E3615BB5AB124D6150F22407,
	UnknownPtg_ToFormulaString_m492658C08085D3A98FD1D6868EF1BCB76922D49E,
	UnknownPtg_get_DefaultOperandClass_m4050DF16BEAF7D4E8B54F04CFB70FE44ADCAAA8E,
	UnknownPtg_Clone_mA7E5424A72C3E2B7F93792D50FCD4E9BD78A644F,
	SheetNameFormatter_Format_m974014B99C387F91DDFEAB7F9855269B02CA4828,
	SheetNameFormatter_AppendFormat_m9B5094F38ED1C0B0C980B294C2A14FD7AC17B945,
	SheetNameFormatter_AppendFormat_mC60EF6F98A25779130713F9F67358C3345D00EA9,
	SheetNameFormatter_AppendAndEscape_m9FA933482C2E177889E1A6D650DC197D68AF91B2,
	SheetNameFormatter_NeedsDelimiting_m343469ACFDCC92F9B9F03C506DAAE300C98910D4,
	SheetNameFormatter_NameLooksLikeBooleanLiteral_m4C70C8D0B5CF91D2C1790C9BBED3D633C95670FB,
	SheetNameFormatter_IsSpecialChar_m9E7115414B914E058603A7268F875118C1713A70,
	SheetNameFormatter_CellReferenceIsWithinRange_mDBA2370345687AFCDF8C6230C46778EA575888B3,
	SheetNameFormatter_NameLooksLikePlainCellReference_m0D43511414807DCB4FE41CEC1620A2531E7BF791,
	SheetNameFormatter__cctor_m2A00D244FD59F7B78DC1AA6726D59F4CB33DED4C,
	AggregatingUDFFinder__ctor_mB9C34047EFEED19AE6532DA5D909D33DAEEC31E2,
	AggregatingUDFFinder_FindFunction_mAE4D33E5DE614DCEB6E0D50D3FEBA61049B50946,
	SpreadsheetVersion__ctor_m0653C59C87D6252906235511B6A5599934F2BCF6,
	SpreadsheetVersion_get_MaxRows_m80E6CEEE7233D746914B82F98C1AB501327860DC,
	SpreadsheetVersion_get_LastRowIndex_m7CEC5758DB68207FC9EAF5FDB9E6D3ABBA8316C7,
	SpreadsheetVersion_get_LastColumnIndex_m83583EB13FB068B4341519E837908455EFA94B57,
	SpreadsheetVersion_get_MaxFunctionArgs_mD82CEDA37AC54BDA3F4BA65AF804860C3753455D,
	SpreadsheetVersion_get_LastColumnName_mFF12078480A2B53A68BA8315C75824B7D2DF3E7E,
	SpreadsheetVersion_get_MaxTextLength_m3072FBF2D7E55DE393B3A8C3E15FE63743EFDA55,
	SpreadsheetVersion__cctor_mE5F1168E4CE880F0101BEC268ECFB0A12FA475B3,
	BuiltinFormats__cctor_mEFDFB391266E5C138A12A04994A50730252056FC,
	BuiltinFormats_PutFormat_m92D6B4D4976580BB05470B2ADB9BB43194497175,
	BuiltinFormats_GetAll_mE37FC454F14487C5AF1F67B1B4110F8AEB1583DC,
	BuiltinFormats_GetBuiltinFormat_m58EE2BC476E35C0114F3E07009F58512B51B3B4E,
	FormatBase__ctor_m5DDEE8973CE6327218D6FB790EFE2EF8902F02AE,
	FormatBase_Format_m824AE30BACFB62B5BA0072E0CDA6946CEA166601,
	NULL,
	SimpleDateFormat__ctor_m55829BEB655069C2026346E3C27D57022096D764,
	SimpleDateFormat_Format_mEAC99B457219581C050A3FE06E23823CE1E32685,
	SimpleDateFormat_ParseObject_m36AA6D3FFF748668549EA9360690360F29188313,
	ExcelStyleDateFormatter__ctor_m3A90CA276B2C827152A22B95D1621E16C0765B8D,
	ExcelStyleDateFormatter_ProcessFormatPattern_m9C0144A75F48F0494C8EC6A60699F94F0B13E992,
	ExcelStyleDateFormatter_Format_mF8493BC1D05A966B7AF702BD9061061AA415C4BD,
	ExcelStyleDateFormatter_Format_mDEDA0754190EFF5F1CE3F2FF6C7A7E51C92234AA,
	FormulaError__ctor_mDBD1FCC265496B30C9316556A2A68F45E79C343F,
	FormulaError_get_Code_m7F01BFC3A5C363A133236160F2969B60D53E4661,
	FormulaError_get_String_m3B49928B93AEFE862F07E8826788B50A4D8F90CE,
	FormulaError_ForInt_m5F3575271AEDDBE340BBB474710E45705EC8CEA1,
	FormulaError_ForString_mF2A40B55F3712EE76D2128805BC1C3EE41FF1D87,
	FormulaError__cctor_m4FC2E63B1492B9A74BB1C7AC737CD09D95005BEE,
	MissingCellPolicy__ctor_mA72538F03C2668812708B24F25C693F280897F9E,
	MissingCellPolicy__cctor_m94A927385AD40A3884FF223C207EEF58A8DC1770,
	AreaReference__ctor_m6E92717D13625367576E141E5E91E8E9899CF1C2,
	AreaReference_IsPlainColumn_m9A48FCC91B459F3733DBB39342BFF6D39FD51AB8,
	AreaReference_GetWholeRow_m10B1F69EAD5506D883B4998EB1F3E8B60FE88638,
	AreaReference_GetWholeColumn_m08531C4F030808A6E4151FADA9890AEF8191E35B,
	AreaReference__ctor_mCE36D6791894DF717978DED7120A219802093B06,
	AreaReference_IsContiguous_m5262992DA3B343683D5E1BE1FEF65906A6DCC89D,
	AreaReference_IsWholeColumnReference_mE1FA2CC46EF512895267795FC3F0CC5A75ABC68E,
	AreaReference_IsWholeColumnReference_mCE3F40823D6DEC12F89B357ED8463483F09A636C,
	AreaReference_get_FirstCell_m0C3F651A4243EB63F99AB44053AD6711BF5D5C25,
	AreaReference_get_LastCell_m7AA221E50DADBCEED52DFE5E26CC43877C22878F,
	AreaReference_FormatAsString_m53BB787492A094E95F82E3F87D33B4657D3CEBFF,
	AreaReference_ToString_m960A91E8E1335B900A169EF3EE8ACF9D8B619D60,
	AreaReference_SeparateAreaRefs_m0163E2F94B30EA5B271BE77443B941FB090E92A5,
	CellRangeAddressBase__ctor_m513861A46225913583543B53D4A114DD63FFD861,
	CellRangeAddressBase_IsInRange_mE18E101F8E1C6A01F60B6207613B80EAA4517400,
	CellRangeAddressBase_get_IsFullColumnRange_mA4CC545B591EB9571F0C73324F5538B9169A70FE,
	CellRangeAddressBase_get_IsFullRowRange_m55F485829AFA876F3291DA41D163DE7E970393D8,
	CellRangeAddressBase_get_FirstColumn_m34C04F347C9C6078F89BB6038E7869D0483EDCB8,
	CellRangeAddressBase_get_FirstRow_mEA1AD7011F2CD4B5D154A43A7CBEB684CF2A5E89,
	CellRangeAddressBase_get_LastColumn_mC9608C98A300394E6C16C07CC14ED888D5F7519C,
	CellRangeAddressBase_get_LastRow_m7922F88203E11F96F4AC6B096B0F243E4559A92F,
	CellRangeAddressBase_get_NumberOfCells_m2CF30919B11B5E9A513D1020038C46276F74D24A,
	CellRangeAddressBase_ToString_mC091D9ED4BB5A23C4A9BCE21DDF57011739FC07B,
	CellRangeAddress__ctor_m6964CCAAD84DA087C60F64D654BFF7B28B5C5036,
	CellRangeAddress_Serialize_mCF8819993C67901705734977253DFB90B854A766,
	CellRangeAddress_FormatAsString_m0B37D03B665C96FCF5E0F2300C2214C63392BEE6,
	CellRangeAddress_FormatAsString_m9AE730F3E8277916055AACFB4B8C2DBA9937D521,
	CellRangeAddress_Copy_mF94B32AEDF14370CC6B64BE92CF7F9CECA14AADC,
	CellRangeAddress_GetEncodedSize_mA7B454915F76259968B978A6F53046648F12FA8E,
	CellRangeAddress_ValueOf_m6A63027E25DEF53BC731AA2375FB0C9E35FB4DC1,
	CellRangeAddress8Bit__ctor_m0EC0802DFE5E63A15D29CE3E6ABBA5E07B3BB36B,
	CellRangeAddress8Bit_Serialize_m1C11DA1576DD346A0B6F2CB0E5E8E60FE27ECA7B,
	CellRangeAddress8Bit_GetEncodedSize_m060AC382C78CB55A225BFB2738D439D2C7490735,
	CellRangeAddressList_Serialize_mECBC4F5FE589DB1EB820AF69451D31A7D6B2032E,
	CellRangeAddressList_get_Size_mF088329B2E94C74C297F563071029FEE13CFE47B,
	CellRangeAddressList_GetEncodedSize_m20DC0B9B79B822EF03BA31A03B26B3925B456D8C,
	CellReference__ctor_m9DA291B78BC76586AFAE6EF254888A50C3148E12,
	CellReference__ctor_mBF41570E5C5B9B75A97434ED22D06F546A3DC839,
	CellReference__ctor_m2AB3376F1707F71F5FCBF815EE8A09599331C818,
	CellReference__ctor_m53C8DA42A140B4A247C05BEC1C4019A064D1BE58,
	CellReference__ctor_mCFE6FF3894EC60F6577D85C1C533A839DB5B05A3,
	CellReference_get_Row_mF70C04A56A2499D823C247E091A0B38590E7F0FF,
	CellReference_get_Col_mB5458650AD434748C6A3DEDA98724685BF100766,
	CellReference_get_IsRowAbsolute_m1914748618922FAFD3DE78066807DF4794E7656F,
	CellReference_get_IsColAbsolute_mCE96B8739983C8288E52D0526507AA3981566A20,
	CellReference_get_SheetName_m753CF1BCCABD92AEC5A674BC5D4F94952F9ECF18,
	CellReference_ConvertColStringToIndex_m21A9F81F935C26CC0A9E98AB58D9B1FF59E48791,
	CellReference_IsPartAbsolute_m57636C639940E76B8C3C18EE5D5A7A3B6F488833,
	CellReference_ClassifyCellReference_m3CFC8B45CD37615533D5BF51A0F83D6987BBA392,
	CellReference_ValidateNamedRangeName_m5A4ADAC0BBD07E7F1883BD312680CE9F12815B1A,
	CellReference_ConvertNumToColString_mE1D4D5C9E0EB481B3DCA1D52ADFC5E707FC1030A,
	CellReference_SeparateRefParts_m6031C4557C60EDFD36EBDE37DDE8DF368608EE2D,
	CellReference_ParseSheetName_mCA06225ABB48BE5F725604CB5B9342011C129752,
	CellReference_FormatAsString_mAD8B736FB344548352F9639D7A29617BDE7009C7,
	CellReference_ToString_m894B5367C085A969B9D67BFC475FFC358FAF1EF6,
	CellReference_AppendCellReference_m182A795925A7FF3B14E57C452C7A64A8FDAE95BE,
	CellReference_CellReferenceIsWithinRange_m4C778E913DEB6AE3B3042EA7586557B6634AD41A,
	CellReference_IsRowWithnRange_m35E2BC4C4DD03935FF8DD1A08422F97B112397EA,
	CellReference_IsColumnWithnRange_m151A82C8A6B54097BD17086161A37DDB624B3558,
	CellReference_Equals_m38040E7AEA719EF7E1ACB8A5EA1609D3B7B4B002,
	CellReference_GetHashCode_mADC43CC485CBA0A4D1BFD4906517F597EE5E5242,
	ExpandedDouble_GetFrac_m6999BAFA1C80B4B0CC71B16753B460418EB0B056,
	ExpandedDouble__ctor_mF998D7F7DB2B942480953A1893AB601B837A74A9,
	ExpandedDouble_NormaliseBaseTen_mF0A26045825B1FAB9F5A7FB6C080C4DC8E2167CC,
	ExpandedDouble_GetBinaryExponent_m7B3AFEB8424371FC34F5C309B5231A4C85147E4D,
	ExpandedDouble__cctor_m73518193D9F68E12C75D3813B85559C9F9914A6B,
	SSNFormat__ctor_mB19E7F833F5FCAECBF2392CC3101CF9892F68ACB,
	SSNFormat_Format_m2034159DE0DBAAFAABEE87935F5CA878C5038787,
	SSNFormat_ParseObject_m1D867B4739DD0EE632FFDB00ACA086D889C054A2,
	SSNFormat__cctor_mA60858D4620CCA95F1FC62A90EF608F6C9958146,
	ZipPlusFourFormat__ctor_m1CED2DEF44219F690FD7BFA4D9F9AC564A2974FD,
	ZipPlusFourFormat_Format_m0BE99A1CC895B752A1AC9438EFCF50DE1786B069,
	ZipPlusFourFormat_ParseObject_m1FDC1DCB8300220E400C42781E5F46169AF78599,
	ZipPlusFourFormat__cctor_m1CF7EC852C1720F605669EDFAAC246D3697C992D,
	PhoneFormat__ctor_m680503A6FB8D3F18FCE047B46910F4A120548280,
	PhoneFormat_Format_m9305D82B8F07B4F4B15B80A0F7E3D76827F8DF25,
	PhoneFormat_ParseObject_m9EFD3C1BC42506610B5A4453977ABB632EB48555,
	PhoneFormat__cctor_m4E909A1CB8C30FE5C035D1D5EBA8639973A14466,
	DecimalFormat__ctor_m47D37833D47C51FA092375AADCE17B2BF94DFBF0,
	DecimalFormat_Format_m9BE6C3AA92F4A7B1A8EB97E07913A8EE023A7929,
	DecimalFormat_ParseObject_mAFD1765CEA1A9F95760AC3A6FC17DC7D64AB20C4,
	DecimalFormat__cctor_mB88E3D251664044F444643AB506F11ACC800DEB8,
	FractionFormat__ctor_m85E713261FCC439EDC96151DF83265EA613C655D,
	FractionFormat_Format_mA13ADEB3A17A6ADC134C5CEE056EE1B97A20C7D9,
	FractionFormat_CountHashes_m6ECCF24E33E5CBEEFCC76950D04228FE97FD3298,
	FractionFormat_Format_mB452B53136E0752CCA1FA80688F468271EFB5F81,
	FractionFormat_ParseObject_mCFADE1808079A86D8ED5EB141989DC29C8BC7603,
	FractionFormat__cctor_mCE29544FD6BCA4C9E32FC520B525CD17CD190C13,
	ConstantStringFormat__ctor_m5A8A53684B2BBA1DFD132FD975AF7DE6F1FEC629,
	ConstantStringFormat_Format_m78BBAE67E53CC90FC6187786D30D3AB71E37CB37,
	ConstantStringFormat_ParseObject_m8511681DF00DD22EC47ADBEBBA73D6667B1492C0,
	ConstantStringFormat__cctor_m5F6CFC2123430DFA7C59B303E1C9F1056E661DC0,
	MutableFPNumber__ctor_mDACD7CFEFF9124A272F856A2ED851753CF580DA1,
	MutableFPNumber_Normalise64bit_m6538983CCE5F3031EC51FDCFAA92ED1671D71C57,
	MutableFPNumber_Get64BitNormalisedExponent_m110E36B33D253F7048128F3D4B44EF5F94BE77E8,
	MutableFPNumber_IsBelowMaxRep_mAAFDD67C80C0BE47DD47D75D5CDBC18A50DCC1BF,
	MutableFPNumber_IsAboveMinRep_mDC1A6D5DE76D3734BA98C645AD6182876E024471,
	MutableFPNumber_CreateNormalisedDecimal_m9BDCA89E28CBF75021723EBBD7939105AB4DFC05,
	MutableFPNumber_multiplyByPowerOfTen_mE2602533FAD47C1A353D4130962C526A3ECB0E62,
	MutableFPNumber_mulShift_m040EA6C1FD53313779AF5936B12B7AE860A476CD,
	MutableFPNumber__cctor_m65D7E57B8A44EF7EDE67D9BB42E803A773240170,
	Rounder__cctor_m6CCAF86FF450C8A5C5A73FF37B7C372046981809,
	Rounder_Round_m82490AA089A7DBD17240899E561C4508ED03C7DF,
	TenPower__ctor_mF58646C1A1BCA06E5B09F8B431918F50C93B88B6,
	TenPower_GetInstance_mA72AA8A3869EDEAB2C579064BC33ED13E08F1FCA,
	TenPower__cctor_m49907AA7D1C11D5AB62AB32C54F1353E96096DC5,
	NormalisedDecimal_Create_mFDFC63E360B5BCE4FE3996B76A59B51FC95316AA,
	NormalisedDecimal_RoundUnits_mB0643CCC8B90FC92C8D989BB3A014D8A309EB28D,
	NormalisedDecimal__ctor_m1496EA6187A0507F661CB957D44E62BC0E7C4A7E,
	NormalisedDecimal_GetSignificantDecimalDigits_mC3EEF83247CAA8580359A3DF183D3AFA3A19C7C4,
	NormalisedDecimal_GetSignificantDecimalDigitsLastDigitRounded_mB17E8944C8CAC63561B0BDB37FD2A0F21276EE79,
	NormalisedDecimal_GetDecimalExponent_mA70FA799B406C260DB4A597394103A3A7112FA7A,
	NormalisedDecimal_GetFractionalPart_m1E891C8F4AFD5FDDB6F453B49FDFD020E13DD10E,
	NormalisedDecimal_GetFractionalDigits_m66CBD39FA6F0114C99B296503CD2173355FD2011,
	NormalisedDecimal_ToString_mFA3BF2C420B8B6C42F39D03C77D1FFEECB7F5DBE,
	NormalisedDecimal__cctor_mDCF13703BE5000C4E64EF18F40B779E75E9CDC13,
	NumberToTextConverter_ToText_m80E6E05EF0B18F7C2D82C7FFED2B8B0C337C7E55,
	NumberToTextConverter_RawDoubleBitsToText_m996C3954C99445C294202D82020A9C9AFA4E7CF9,
	NumberToTextConverter_ConvertToText_mDAAE07BA6BDCFC355E07A5C1A5691FB04B18CF40,
	NumberToTextConverter_FormatLessThanOne_m6383002A7A8499914F7B61C63C6A68168E5D3FE0,
	NumberToTextConverter_FormatGreaterThanOne_mA45CE88A44574380E02B85CEC994DFD42D612DF1,
	NumberToTextConverter_NeedsScientificNotation_m62F24EAD17EC565DE346AE8883139E0EE7458492,
	NumberToTextConverter_CountSignifantDigits_mC9FFCF348C094972AB05E55AFAC3E86931D661F1,
	NumberToTextConverter_AppendExp_mE623BADDFBAA6F11C213EFEB45854B12D93B7689,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WorkbookUtil_ValidateSheetName_mDED7A7A081A5EF4DC70225E15652FE18E1FCCCC0,
	Arrays_Fill_m5A44FF369636AB4812A73107CEFCC329E580A909,
	Arrays_Equals_m6DBCBB63CF8F34776881CF03E8FF72F15B94172F,
	Arrays_CopyOfRange_m8262C1A4B753EAA021E2AF5692575C33534D19B3,
	BigInteger__cctor_m03B31BB7EAF2686B75E2F8EE0E6414A9EDA4B592,
	BigInteger_Init_mA1724ACD6AD153FDEC83540FFCA17C83E9653855,
	BigInteger__ctor_mAFC5FDB262A33A85F0BCC6A4C06B70AF4FCACAC1,
	BigInteger__ctor_mE9FBA1AF5C802F74405384420DCABA8B606E9F14,
	BigInteger__ctor_m63C178579EFB35B2556E70810B2A994C1A6AF086,
	BigInteger_TrustedStripLeadingZeroInts_mFC4BFBEADD08449A230930FC8E25D249315DD756,
	BigInteger_ValueOf_mE92722A72FD10E320409F7724C98EAF26DD0AD3A,
	BigInteger_ValueOf_m610ED8DCBCF6B68BBFEEB6B038733F2F150E0969,
	BigInteger_BitLengthForInt_mDE733E1E30CFC64FE3696241F526D017BDA30C49,
	BigInteger_BitLength_mFD945F9F5A7C7A584C953F746D6D495142AE7500,
	BigInteger_Pow_mB8ACAE706C2C89598198ABCB66956FB3D0AEF3B3,
	BigInteger_MultiplyToLen_m32030999786563D51D74BA6561F3926836AD2A82,
	BigInteger_mulAdd_mCB70F353891318DBBB4508AF1D7F3843903695E5,
	BigInteger_squareToLen_mA2C06566CFB132DEE67B73613D168985F4122BA2,
	BigInteger_PrimitiveLeftShift_m3AE4B9F92BB0A324B89C3C54E1473EDEE1ECA5DC,
	BigInteger_addOne_m26B7604EF4991B35D996395BACEE49AE7C2B460C,
	BigInteger_intLength_mA2A33CFF8C11754202D6F72DEDE203914DD13F3F,
	BigInteger_signInt_mF7ED72253FE9007C12EAF140BA3D6891CE421527,
	BigInteger_GetInt_m900BA5491C988D5A0EB62C244879E9890B5776F3,
	BigInteger_FirstNonzeroIntNum_mF4B813E70E36797B9D264548174B613CF8CC793E,
	BigInteger_makePositive_m73E8520919F427FDD23933DD7A684336855ED02C,
	BigInteger_NumberOfLeadingZeros_m3B514A1048D1AB141413FCF4E4BFA488A2EE06F2,
	BigInteger_BitCountForInt_m641781FCD5DD2C22770BF2F6B9142A07FD5A0BA6,
	BigInteger_CompareTo_mB81B65FDEA90C6419B468C56030BC10367107A04,
	BigInteger_compareMagnitude_m24B0A802F50B750A24BD375CF78412D7754413AB,
	BigInteger_Equals_m7B5EDB496A03526E16F79128B78C62DEB83D0FB1,
	BigInteger_GetHashCode_m01B2D69D364B4B3DC74BBF7B254D9FFD17DD3950,
	BigInteger_IntValue_m9006863772CA25E235BDCF308B2E7B81C3092A59,
	BigInteger_ShiftLeft_m9DA6FAE0CC9739A6125A336D4E3A7A04C7DF3B76,
	BigInteger_LongValue_mD780DE30AD906DDE0F5A521D851C3F58609D5CBC,
	BigInteger_ShiftRight_m121E3736131CD74242B0A2C69CACCF5ECEC4F666,
	BigInteger_Increment_mB48C1D8CD90788751B8282847636234D9CEE9C94,
	BigInteger_and_m72FBA8800494CEB80C9297CE48FA6ECCAE7D96C0,
	BigInteger_Or_mB7E0CF40CBFE3F98B1DCEE626DB51244ACB6B218,
	BigInteger_Multiply_m7042C76B0A354ABAF8EA8B0E54A34C4FC7CFF3FF,
	BigInteger_Add_m6C1B39461B440662DD7263F3EAB77F279A51FC2C,
	BigInteger_add_m279031CC95AB6E2275ACF76F304CB72AA61F669F,
	BigInteger_Subtract_mE8128D2BDE75E57CD37437BC30B947C4EBDD0421,
	BigInteger_Divide_mED98325A2D60E12905E2D88FF5CFDDEF355B6DBF,
	BigInteger_op_RightShift_mB98854BF605C032DFE9ED60E34A733AE1C672F0B,
	BigInteger_op_LeftShift_m884739A657C91F0460175D55B179542DC71D4B60,
	BigInteger_op_BitwiseAnd_mDEC6AF8E34218F3198969B99908C6F4030460088,
	BigInteger_op_BitwiseOr_mC56696B69EBD4B0A5EC61952225B983BD0E98270,
	BigInteger_op_Multiply_m0491622D4B2C8CC6F9F18EAE8978B28083B4DA65,
	BigInteger_op_Addition_mE2454552AF008E85C9CFA97744BD539E4EABD6CC,
	BigInteger_op_Division_m3FDE7CD999102BEB4CB3890E80CE1D9914B5065E,
	MutableBigInteger__ctor_m1EEE90956FFB238F8BF1503C70789DD3399E0355,
	MutableBigInteger__ctor_m9068565F5592C295D38D2B2A3B7638BA6C18D6B4,
	MutableBigInteger__ctor_mFAB746FBE1143BCC16BE535A832A39FCCAED1955,
	MutableBigInteger_ArraysCopyOfRange_mFF2781B6A042487A162D3F61C80D9EF15B6BD3A7,
	MutableBigInteger__ctor_m8537F6D7050FD9AE3F137EB038064DAFAB469E67,
	MutableBigInteger_getMagnitudeArray_m90D072FA050E8CD7714496A858C67DC0BA654F3D,
	MutableBigInteger_toBigInteger_m8634C9B2A5F5023AFA55CE381F10D423C783C666,
	MutableBigInteger_clear_m9438262C2CBA306037AD04DD7DC4B39EB91C29A0,
	MutableBigInteger_compare_m130AA2FBD0523B7C9EEF093E34C79F4A05579D07,
	MutableBigInteger_normalize_m6A829F4B80032E707BD4E0037A0E914EFB71FB31,
	MutableBigInteger_setValue_m9F3D725C9CCDD82140E8B7434AF186C3A71987CB,
	MutableBigInteger_rightShift_mAE314FBD7996242288029D08A73CE844EACDC929,
	MutableBigInteger_leftShift_mB8A22158D4A0E5001356DA223E1981B9E5BA4A3C,
	MutableBigInteger_divadd_m0AA4D7F236075C35D466D5795BD7152F183D17E4,
	MutableBigInteger_mulsub_m85BAD4FBE14BE354B302E209FE63385BE105690F,
	MutableBigInteger_primitiveRightShift_m4B127B7526C3D6FF51D5E60841AE0ABFDB9CF5F6,
	MutableBigInteger_primitiveLeftShift_m20B90AB3E1B6476F63CF970160AF9650E0DDA321,
	MutableBigInteger_divideOneWord_mA07B5EB9E3A27A3EEA6D509295A6BB1C90C0D688,
	MutableBigInteger_divide_m0ECCB9D5BE05D568676E9947483ADE8E2B4DC02E,
	MutableBigInteger_divideMagnitude_mD6DE8DAED117A8DB028C9DDDE58C43135E0FB160,
	MutableBigInteger_unsignedLongCompare_m4CBC08BE1D27CCEBE328CA0DAA9F9CE697A13990,
	MutableBigInteger_divWord_m381F6B63761EF53DA05AFB1008F4F9403B6B6159,
	MutableBigInteger__cctor_mFCDB0CFA2AD1642BB367DCC1AB533BC4764BA61C,
	BitField__ctor_m5CD8E28AFB42A0CB6CB782BEDD6349055A44FA45,
	BitField_Clear_mDEF6676B745582552F6DCAA019D5F7296913A057,
	BitField_ClearShort_m87C09E296908A249F1EF37496CD65D678FB2DAF6,
	BitField_GetRawValue_mF51C82E6CD24AE3A2C3D451E8EC0203F552DBB06,
	BitField_GetShortValue_mFC51F5B19ED26B4F14BBF6631D1AAEAEFA38F5AB,
	BitField_GetValue_m55E378DCB7D5F0499CF3B6E550AA1B541F931A97,
	BitField_IsSet_m1ED44B529C844D7699119715608FC0E4F338098F,
	BitField_Set_m12B83A2BD5F9268ADCF761E6408E377BEDEF7DCE,
	BitField_SetBoolean_mCC228C9A0FA0EEE04F020BE092A957362694E3F0,
	BitField_SetShort_m2E49A070F0CDA23A5402CEF35660474335D731E4,
	BitField_SetShortBoolean_mEB3E65DB9EB5F6F179B249C4D954CD527F213B06,
	BitField_SetShortValue_m3EA5D9080D508710AF22FA0364CFCC4155861443,
	BitField_SetValue_m3B396B7F64519E079BA0703849383F6118451F6F,
	BitField_ClearByte_m246B0695279569828AD8098CB553B3AA5E9211B1,
	BitField_SetByte_mD89E96AEC7CF03C79E1B2036169EC87ADED64B92,
	BitFieldFactory_GetInstance_m19F8E3319B9498FF8EE3066B76C7E3531762DF73,
	BitFieldFactory__cctor_mD5D302B6C5F60962B5F396DED2A2D83149A8AA1F,
	ByteField__ctor_m55462DF74145676AC636B10648BCD7FAABFE9538,
	ByteField__ctor_mBF5CCEE8850EE95D4ACF08CF63EA915A43981245,
	ByteField__ctor_m247A69125A8FBD4A3B8B88D166E21B4DA6D9A7AB,
	ByteField_set_Value_m0FCF2117E13754B8AE600BB6CDDBD472A9103835,
	ByteField_ReadFromBytes_m983920715CC65DF0FACD9F799816549E24E10B79,
	ByteField_Set_m14015FDBEECCEBF94A6FD5F43D312CF9D54F0C6F,
	ByteField_ToString_mDF043EF879D8D9D549CFB4E670CBA3460CD5707F,
	ByteField_WriteToBytes_m6036CF46D3FB23B30BD7EC5EE4B10587418BA2F0,
	Character_GetNumericValue_m9C2ABA1A05E0328E2D082A13811C255B223746C4,
	ClassID__ctor_mCE617454E404E7D81C2137A14AFB249A93500C61,
	ClassID__ctor_mBA7A611793EF8D17CE13B160420B14C649D29C1F,
	ClassID_get_Length_m9ACDC1CB2CFF7D69FE8447AEE02F29EFFF5893D8,
	ClassID_get_Bytes_m06F4ADEB47F992DBE040CF8564DE1ACE6F607AA9,
	ClassID_set_Bytes_m37490D37A942E0E7A773ADE5C86EF18F654C16B1,
	ClassID_Read_mC69943882CA6FA13C394B30CE871DECC35D477EE,
	ClassID_Write_m860E4F3050B1FFCB5732F68A6E34C616D83B7B69,
	ClassID_Equals_m1B368E827BDAD3DF2C2BCAA64333274BFF9A5F7B,
	ClassID_GetHashCode_m395B4569B464CF459D1E80AF5664810F48AFE3E2,
	ClassID_ToString_m11123D1DF018FD401277B38D16E59C556AFABE65,
	HexDump_Dump_mDECF260C8CD8BE236D8547DCCB3D1E34F8BDACB6,
	HexDump_Dump_mE42871DCD32E961EF38D5564EC1C0EEDDBFB587F,
	HexDump_Dump_m657308EAF6DFBCDD141CE54D7A7BE0404F0D1B2C,
	HexDump_ShortToHex_mDC2CB868F01C48D553DC66206303D2EA6608A71D,
	HexDump_ByteToHex_m55E85212F60C5F103F80F159ED90A77397B978E1,
	HexDump_IntToHex_m13C120BBCD4138973743C0D68346D1BFC473F554,
	HexDump_LongToHex_m50FDEA1B06E6F211DEFA0611B302E4967DAF93E7,
	HexDump_ToHexChars_mADDFEFCB8D0491B2107AC6DDE190B153852C8362,
	HexDump_ToHex_m2B0277AB0112C37AE07437CC89584885632F88BB,
	HexDump_ToHex_m48CB587CA1D1AF1B7EEC59560CC1B714CEEE175C,
	HexDump_ToHex_m0E639753CFAC7F574004F84F14CEFA46793AC97E,
	HexDump_ToHex_m6B51733E34DB9BA4D4E652BDD0268189B5713141,
	HexDump__cctor_m2A369E9172F04C6DB3134732AB069C1F365D56FA,
	HexRead_ReadData_m61F6AC6EA9AE20EBFD3B395FC2880093A43FC1DC,
	HexRead_ReadFromString_m5B5C5222DF64AC086B7CC72B40CECB2DCD3B3EEF,
	HexRead_ReadToEOL_m0D243F2CBB5BB54D5EBB214C8487017B96370F0F,
	IntegerField__ctor_mF9A34BFA0F2D2A2A3E604A35326D56568B05BF2B,
	IntegerField__ctor_m4D664EE20B2D7674F211FA341DE2404539C55DDB,
	IntegerField__ctor_mEE69B6ED96E115BA59288AB1AE44B2697590D612,
	IntegerField_get_Value_mB494D3E17FB3E0F52D047C848E207E38493482CE,
	IntegerField_Set_m1DF9A74F1B72E549F8D2E79D2B8931D37BE0DE89,
	IntegerField_ReadFromBytes_m41554DE9454EBDE7C18EB2297CFDD82CB533068A,
	IntegerField_WriteToBytes_m09148AC3B0F7EEF94F8B0626711C25DED3FC5390,
	IntegerField_ToString_mFCFF2DE39A17D47B536288D13831D23301DCE24C,
	IntList__ctor_mD575312D9F4A93D75EF6BB7CCA47CBAAC530F7B4,
	IntList__ctor_m386B9096450A2543A4CC27DC75C080ED0DEF7CB9,
	IntList__ctor_mA9ADF880B9CEF0E58411C4F0DFD3670AE52BE7C8,
	IntList_FillArray_mCBD51C0BBC58C5DB09C1866667D3FF75587D6F1A,
	IntList_Add_m01D16FE0324D1B8E540F40BFAE37A20767113E72,
	IntList_AddAll_m46760A0B26979D28C5E3A72B43C948E7BE211D11,
	IntList_Equals_m65D4D9714B72312E23418268C9B210DC610282E6,
	IntList_Get_m06EB8278A0E85548F56166B213FC64CA23767484,
	IntList_GetHashCode_m8549D1008746294CD2A4A73251B6BBC8B6A9EF1F,
	IntList_get_Count_m7E0041194791E5526FC06421E87C20BEEEC417F9,
	IntList_growArray_m992A817C575CBFA7012C073E0FC1FB91E54EC170,
	IntList__cctor_m7D1893A70D34072ACE4984345B2F230052E826A9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	IOUtils_ReadFully_m9FED4E4E7275BADDD475C8D9A0EDCF95AB067D42,
	IOUtils_ReadFully_m4F19FECCB930EB5BB23A7F1389DF8C332B02F7AA,
	IOUtils_Copy_m7916DDB83BEC6F5E71A813F62EC57F756BB71CD7,
	LittleEndianByteArrayInputStream__ctor_m86B9FA47B8F416E4A5FA648779353982A491E1E8,
	LittleEndianByteArrayInputStream__ctor_mDEC236E9550884C404649C66381232BDD121BF35,
	LittleEndianByteArrayInputStream_Available_m9EFA77807A091914B520097FC1290FABAEB4D7DD,
	LittleEndianByteArrayInputStream_CheckPosition_m596DB720B2DBD4040607FE92A6D68A13FE39264B,
	LittleEndianByteArrayInputStream_ReadByte_m646A05D9D87E8CDED7F74E0BA51EEDAC10671B2E,
	LittleEndianByteArrayInputStream_ReadInt_m8EE4A7FEA7E5CAF725E85D9A7876D68173340469,
	LittleEndianByteArrayInputStream_ReadLong_mB414C3DB7504C6378CF1E0618FD664C3D9176545,
	LittleEndianByteArrayInputStream_ReadShort_m97439305F4F80679BB98F2D3DACC5B3DDB798B5F,
	LittleEndianByteArrayInputStream_ReadUByte_m129998D184ED0971FD60DA4FF89CBF24EFDE1446,
	LittleEndianByteArrayInputStream_ReadUShort_m8B3A3914A092A3485DC49EB2AEDD332D23991CD3,
	LittleEndianByteArrayInputStream_ReadFully_m3163836CB0FC75CFB7F0B7BB32D097DAE210FF77,
	LittleEndianByteArrayInputStream_ReadDouble_mF85301E78F4B7C58ADF562EDDEB2D3DDFEDC4847,
	LittleEndianByteArrayOutputStream__ctor_mB3E42860E8B57D76B39F37FA1F0A469CD265EAE7,
	LittleEndianByteArrayOutputStream__ctor_m94B9933DF04D29E0FE1A1068D51A15A1A3DE791C,
	LittleEndianByteArrayOutputStream_CheckPosition_mD62E5EEA3335A899F412BCAE8CC12EEF2320815F,
	LittleEndianByteArrayOutputStream_WriteByte_m2C6724BDD03BEFFA3D9A4CDED6348BB6B30408A7,
	LittleEndianByteArrayOutputStream_WriteDouble_m7789EB501A71E68EFF1610223B061EB7F20A4EEE,
	LittleEndianByteArrayOutputStream_WriteInt_m0F3FC2951321557E1D7685DD42BD6A2238359785,
	LittleEndianByteArrayOutputStream_WriteLong_m6B0973C2816F3A885DBDF0877E1ABDCDDE867C11,
	LittleEndianByteArrayOutputStream_WriteShort_m1A21E7019A87A370D8E4124902F5851E3AB5DB4B,
	LittleEndianByteArrayOutputStream_Write_mEDB8823616A67D1EB4A987842C6CDE5EF6EF9E68,
	LittleEndianByteArrayOutputStream_Write_m31A939E183A47291BD366CB7B3C15395E2960C2A,
	LittleEndianByteArrayOutputStream_get_WriteIndex_m102424BA14ADADBBEF633FCBEB50BDF26DC3A901,
	LittleEndianByteArrayOutputStream_CreateDelayedOutput_m5B5F4B8255345D8C135E83A4183B79F8BE3DA556,
	LittleEndianInputStream_Available_m38A54A020090C77C4375A9C2ECB7CFB5A2B9250C,
	LittleEndianInputStream__ctor_m0BF3D602FD8FA6F86447D5205E132CD0D11B6568,
	LittleEndianInputStream_ReadByte_m8C0B8A2DB2B78034E7703BD7036528DBA4B07E92,
	LittleEndianInputStream_ReadUByte_mEAFACC97FD157D04CE92BFA1D954A0F9C22523A4,
	LittleEndianInputStream_ReadDouble_mF3C8E2A2738EDA05CAAF2220890CE92077E572E2,
	LittleEndianInputStream_ReadInt_mA05499D6FE05D756EE8F8D301E2B646F83AA70A1,
	LittleEndianInputStream_ReadLong_m4255DCDF134B959AB3B3C6272FD0FE12A147B104,
	LittleEndianInputStream_ReadShort_mB1E2C2858941DE2DC8B8D45A0840E6C1FD3CDB47,
	LittleEndianInputStream_ReadUShort_mC10887A2DAC3738939D26C257D093276B539366F,
	LittleEndianInputStream_CheckEOF_m0DEECCBB5A01CD791A114EE4CC6A5DB915CFD889,
	LittleEndianInputStream_ReadFully_m0617EE000A49A99D35CC7D96C68F0237D57D7E3F,
	LittleEndianOutputStream_Dispose_mDF2AC7441F4583F76D77F0F4302AA765E2EBB6BF,
	LittleEndianOutputStream_Dispose_mF6B4D623608D962A2B900BBA2B3A5AFEE7CBD9EC,
	LittleEndianOutputStream__ctor_mDC70EABEE04BC96276C5186FD11E92429C4B5FA1,
	LittleEndianOutputStream_WriteByte_mC391FB2B611D688F46DB37E8D731109A42E93329,
	LittleEndianOutputStream_WriteDouble_m930B77EFE7C010A7DE07B11CB4558A8BF673C035,
	LittleEndianOutputStream_WriteInt_m6DA7E55D733CF81B8DE7529CB10A9D9124163C1D,
	LittleEndianOutputStream_WriteLong_m23FFDCD56A541F1B2248F498A90864DF8D746FD2,
	LittleEndianOutputStream_WriteShort_mB28C8E241FBDA7FC2BB20D4352AAC5929E629903,
	LittleEndianOutputStream_Write_m774509B1FD49EEF999DB35A0237FA47327962A82,
	LittleEndianOutputStream_Write_mCE6FAF974EDB23E70A7DC35623E339A2804DD509,
	LittleEndian_GetShort_mF3BA89D51340D9BEB4C02510D683AB797325A9A5,
	LittleEndian_GetUShort_mDEC12F6FB3B73465A03EBE6BDB2BEE7FE1E33233,
	LittleEndian_GetShort_m90910A445E3D735B3A360B021BC2301D646FC00E,
	LittleEndian_GetUShort_mC6170325385FC9D70B73F1C0580FA249123C7880,
	LittleEndian_GetInt_m91A1A30D11579869A0A81267BCF0C06A76DE572E,
	LittleEndian_GetInt_mA6EF389995FFD75F3F94167BB0A3A23A4E904D28,
	LittleEndian_GetUInt_m89B76D884B3F8EBFCE8951A8777B04CD0FE1D775,
	LittleEndian_GetLong_m902DF9ECD3F727BA02D6CECD8C56CF3A1442CD0C,
	LittleEndian_GetDouble_mA60858AF36F7AAB0AB242E7C1366E1EFD202A4A3,
	LittleEndian_PutShort_mBC5815570164ED653AFDA2E39E24B1C9716AECAB,
	LittleEndian_PutShort_m02D659EA29583434ECCC1333BB149AA17DA2BCBD,
	LittleEndian_PutInt_mA3B280611EED79C16825438BB1C8C74161B97B31,
	LittleEndian_PutInt_m05FF1C514A2E8D7D93FF3A5A3BB5E11CD0192340,
	LittleEndian_PutLong_mB7D2F415C4B259EB4759DEACD938F90344BE48E5,
	LittleEndian_GetByteArray_mA5B5C4325F64E256BB7DB8548828D7ED369D2774,
	LittleEndian_GetUByte_mEEFDC552CF0995F5CACF7C55BD0A846FA9657D7F,
	LittleEndian_PutDouble_mE92DAEBCA183902D8C0381893477199485E00757,
	LittleEndian_PutUInt_mA6A6D1EB1298FD0AC5F3CD67B774292C6AFF6EED,
	LittleEndian_PutLong_mBAA8781144CC6032642957E4BFB32D5EFFC309E3,
	LongField__ctor_mF089E4684CF5D8AAD008588573C35E5756C177FE,
	LongField__ctor_m77B02D2C0B44F61DE01A9D552F8950CB47C89C69,
	LongField_Set_m96B8152512C2CB101CC8DC71BF39FE4E248A590E,
	LongField_WriteToBytes_mBFEB8C9EE648EE9D1F40B274304D2E79F8D0CC3D,
	LongField_ToString_mC1A16C0985B2B0F8D8C0FDF8994D47BACD79AC46,
	POILogger__ctor_m6A535072F52D6DA111EFAC584E31D8EF5BEDE1F4,
	NULL,
	NULL,
	NULL,
	POILogger_Log_mFA0811BE7F90C904CA3F3506C59DA6D8A1E15AA3,
	NullLogger_Initialize_m1D49789A09F6E205DFCB2203597E01729163D8CF,
	NullLogger_Log_m969F3CD089FC86CB9D120A52A2CD8CA3308AEC52,
	NullLogger_Log_m91AFCC9A3B1977F85E8F283FB9A75F8DFC92EAA2,
	NullLogger__ctor_mC9ABEF89C9C6CD05345517763DB7A86733B51B7E,
	Operator_UnsignedRightShift_m2BF9A4E449CF5A8C7355951488E669FA87455FEA,
	Operator_UnsignedRightShift_m48CEF7A353E39C6B6E173099FFFA0C15D140462F,
	POILogFactory_GetLogger_m48771ED92B379411190DB546CA7BECF6AEBAA964,
	POILogFactory_GetLogger_m5436FD72FC502BDC0FEA30A98200C9DBE41A87C5,
	POILogFactory__cctor_m660C1DDED14ECAE67D36E9338A861F5C46C32862,
	RecordFormatException__ctor_mEC2C3B5D050AD3AEF8CFC9E9F783532321514633,
	RecordFormatException__ctor_m9DFF8F587A56A7783F445FF2A8A7F556AC9B4FCB,
	ShortField__ctor_m36CFD283551C4D065D6507A67FA46B38BEE64889,
	ShortField__ctor_mB4E4D39D3BDD179BEBC1BD8895281B42B6CB15EE,
	ShortField__ctor_mA2F9102CDA841C14509C1F19A506E8FE3C44FB5D,
	ShortField_get_Value_mA0BB87F3502CC1ED87AE113610CBC9E339CF72B7,
	ShortField_Set_m9559CA66A9B37547A5513F7C95C0E87E31F84A8A,
	ShortField_ReadFromBytes_mD082E5846BD66F03639D0D69E9027B5691C2DCB2,
	ShortField_WriteToBytes_m6D2FEDFD04C4151F3BE4B7A106162FAEBF29129C,
	ShortField_Write_m7F5C30B7C7FADD79CB9642BB7E35BE21AE1D1DDF,
	ShortField_ToString_m3A54C171D01E0426A1AB80642B36683AC2BA4063,
	StringUtil_GetFromUnicodeLE_m5FD4F5569390663B7BCCEA72CCBC6AF84F793393,
	StringUtil_PutCompressedUnicode_mB34DC0049DA2FCD2653119D890B3BE96372C189F,
	StringUtil_PutUnicodeLE_m5985127E462F11622F3A1CCF817BEC7F76BB5BD5,
	StringUtil_HasMultibyte_m32B525077C2CD33B5A3B98324D75C2580B564514,
	StringUtil_ReadCompressedUnicode_mB942D0AAB9655D7AF5BD6F96F6DE3BADAB34F5BD,
	StringUtil_ReadUnicodeLE_m635B0C77BAB7766DAA2E2E16695B8C2656854B3B,
	StringUtil_ReadUnicodeString_m0C1E3EF489201FC50FE397E606C7CE1A67ADDA9F,
	StringUtil_WriteUnicodeString_m8928B19E925B0A93FDF74CF05B441042940698DE,
	StringUtil_WriteUnicodeStringFlagAndData_m8A94DC799A5C1895DAEEEBDC77089D4A68C85E5A,
	StringUtil_GetEncodedSize_mBE92CC89FBEA47CCCAC643D7A0FD025AE86F0455,
	StringUtil_ToHexString_m4DEBE36008DDD972F00F3124EEAC9FFE0CDEBC2E,
	StringUtil_ToHexString_mA523B43FEF94A2F5D754D41E501635A68403A85F,
	StringUtil_ToHexString_m0A3DB93B261B0D1FF65E8E38AC6B233CCB91B3A4,
};
static const int32_t s_InvokerIndices[3964] = 
{
	249,
	1103,
	1502,
	10,
	249,
	3,
	23,
	511,
	130,
	130,
	184,
	10,
	130,
	10,
	130,
	10,
	14,
	130,
	130,
	34,
	10,
	130,
	112,
	130,
	130,
	130,
	130,
	129,
	184,
	184,
	112,
	130,
	130,
	10,
	23,
	62,
	14,
	511,
	511,
	511,
	3,
	130,
	10,
	14,
	130,
	89,
	614,
	511,
	130,
	10,
	1103,
	10,
	23,
	26,
	1103,
	10,
	23,
	130,
	10,
	26,
	23,
	10,
	26,
	26,
	23,
	249,
	14,
	10,
	10,
	1103,
	26,
	23,
	26,
	10,
	249,
	26,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	10,
	26,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	10,
	249,
	26,
	26,
	10,
	249,
	249,
	10,
	249,
	26,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	249,
	249,
	249,
	249,
	10,
	249,
	26,
	3,
	10,
	26,
	249,
	14,
	89,
	89,
	14,
	14,
	105,
	28,
	137,
	198,
	23,
	3,
	23,
	3,
	26,
	3,
	26,
	3,
	23,
	23,
	3,
	23,
	3,
	26,
	26,
	23,
	46,
	9,
	26,
	26,
	137,
	26,
	249,
	10,
	26,
	3,
	249,
	10,
	26,
	10,
	26,
	249,
	10,
	26,
	249,
	10,
	26,
	249,
	3,
	10,
	26,
	249,
	10,
	26,
	249,
	10,
	26,
	249,
	23,
	26,
	10,
	9,
	10,
	32,
	89,
	14,
	26,
	10,
	34,
	14,
	14,
	26,
	112,
	89,
	89,
	14,
	3,
	1130,
	249,
	249,
	9,
	10,
	112,
	14,
	26,
	23,
	10,
	23,
	10,
	26,
	9,
	112,
	14,
	38,
	26,
	10,
	10,
	10,
	249,
	10,
	10,
	184,
	463,
	35,
	10,
	26,
	23,
	614,
	31,
	10,
	249,
	26,
	14,
	14,
	3,
	23,
	26,
	31,
	28,
	1964,
	1964,
	112,
	112,
	1965,
	28,
	1965,
	255,
	673,
	28,
	28,
	28,
	105,
	27,
	1966,
	1967,
	1966,
	1968,
	46,
	114,
	255,
	3,
	26,
	14,
	1502,
	10,
	1502,
	10,
	1162,
	1969,
	1162,
	1969,
	23,
	105,
	28,
	105,
	9,
	14,
	10,
	32,
	31,
	23,
	184,
	210,
	184,
	210,
	14,
	26,
	1970,
	1971,
	23,
	1972,
	9,
	1973,
	10,
	14,
	23,
	26,
	511,
	26,
	27,
	14,
	10,
	10,
	10,
	10,
	14,
	26,
	10,
	14,
	23,
	26,
	114,
	1189,
	35,
	89,
	89,
	14,
	9,
	10,
	14,
	3,
	23,
	26,
	10,
	10,
	10,
	26,
	14,
	23,
	26,
	26,
	26,
	10,
	10,
	14,
	26,
	10,
	14,
	89,
	89,
	14,
	26,
	23,
	10,
	26,
	9,
	10,
	14,
	26,
	14,
	26,
	23,
	26,
	14,
	26,
	14,
	26,
	23,
	28,
	27,
	209,
	26,
	23,
	26,
	23,
	26,
	26,
	27,
	23,
	26,
	26,
	23,
	26,
	26,
	954,
	184,
	14,
	23,
	26,
	23,
	26,
	14,
	184,
	10,
	10,
	14,
	23,
	130,
	186,
	9,
	58,
	10,
	14,
	14,
	26,
	10,
	112,
	14,
	23,
	23,
	26,
	26,
	26,
	26,
	62,
	1055,
	26,
	210,
	10,
	10,
	112,
	192,
	10,
	14,
	23,
	186,
	14,
	26,
	23,
	10,
	32,
	41,
	23,
	23,
	23,
	26,
	0,
	4,
	4,
	1974,
	1974,
	26,
	26,
	1975,
	131,
	1976,
	131,
	138,
	1977,
	23,
	26,
	1978,
	321,
	1979,
	135,
	135,
	0,
	3,
	3,
	219,
	49,
	163,
	1980,
	1981,
	3,
	3,
	1974,
	112,
	28,
	26,
	14,
	23,
	26,
	459,
	0,
	156,
	459,
	26,
	459,
	26,
	130,
	14,
	112,
	112,
	14,
	28,
	26,
	14,
	14,
	14,
	3,
	23,
	10,
	26,
	1103,
	23,
	1,
	0,
	1982,
	62,
	10,
	37,
	10,
	37,
	28,
	34,
	28,
	138,
	41,
	34,
	37,
	243,
	202,
	26,
	23,
	26,
	10,
	112,
	32,
	14,
	34,
	112,
	137,
	138,
	94,
	114,
	94,
	114,
	94,
	94,
	94,
	114,
	48,
	94,
	94,
	114,
	46,
	46,
	35,
	130,
	89,
	14,
	10,
	14,
	10,
	26,
	14,
	14,
	14,
	14,
	0,
	26,
	137,
	23,
	62,
	26,
	26,
	14,
	4,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	249,
	662,
	4,
	4,
	4,
	14,
	243,
	23,
	14,
	130,
	37,
	26,
	26,
	23,
	0,
	28,
	4,
	34,
	10,
	129,
	34,
	34,
	112,
	32,
	10,
	10,
	34,
	112,
	34,
	23,
	1103,
	23,
	10,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	43,
	43,
	4,
	43,
	4,
	4,
	14,
	34,
	37,
	41,
	10,
	105,
	34,
	14,
	43,
	89,
	202,
	14,
	26,
	10,
	34,
	62,
	14,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	23,
	26,
	3,
	1103,
	10,
	26,
	26,
	14,
	27,
	0,
	26,
	1103,
	32,
	34,
	10,
	14,
	23,
	26,
	10,
	1103,
	26,
	34,
	34,
	23,
	41,
	138,
	3,
	26,
	26,
	26,
	26,
	46,
	26,
	26,
	26,
	10,
	10,
	249,
	209,
	23,
	89,
	1103,
	26,
	10,
	163,
	14,
	249,
	614,
	10,
	32,
	10,
	32,
	112,
	9,
	10,
	14,
	14,
	341,
	26,
	31,
	32,
	14,
	14,
	23,
	14,
	202,
	23,
	26,
	10,
	26,
	26,
	26,
	26,
	23,
	46,
	9,
	26,
	26,
	137,
	137,
	4,
	4,
	4,
	26,
	26,
	26,
	446,
	26,
	26,
	26,
	3,
	130,
	26,
	10,
	32,
	23,
	14,
	26,
	1103,
	26,
	27,
	26,
	26,
	26,
	26,
	26,
	34,
	202,
	10,
	37,
	37,
	37,
	37,
	14,
	14,
	203,
	446,
	4,
	361,
	105,
	28,
	112,
	28,
	26,
	202,
	202,
	3,
	14,
	27,
	26,
	23,
	14,
	14,
	41,
	23,
	23,
	623,
	26,
	26,
	32,
	14,
	10,
	10,
	26,
	54,
	209,
	56,
	30,
	62,
	131,
	497,
	89,
	14,
	23,
	23,
	14,
	10,
	10,
	10,
	10,
	10,
	10,
	26,
	26,
	52,
	52,
	14,
	10,
	26,
	249,
	249,
	10,
	26,
	10,
	112,
	1103,
	89,
	23,
	249,
	614,
	14,
	26,
	10,
	249,
	10,
	10,
	10,
	23,
	10,
	32,
	10,
	32,
	614,
	249,
	249,
	14,
	10,
	26,
	112,
	14,
	23,
	32,
	10,
	32,
	10,
	32,
	10,
	10,
	32,
	14,
	10,
	32,
	10,
	32,
	14,
	26,
	10,
	249,
	14,
	23,
	249,
	614,
	14,
	26,
	10,
	249,
	23,
	10,
	32,
	10,
	32,
	249,
	614,
	112,
	14,
	26,
	14,
	26,
	10,
	26,
	10,
	26,
	9,
	10,
	23,
	31,
	31,
	89,
	89,
	89,
	14,
	26,
	26,
	10,
	249,
	14,
	26,
	10,
	249,
	26,
	10,
	32,
	14,
	26,
	89,
	14,
	10,
	26,
	249,
	3,
	23,
	249,
	614,
	14,
	26,
	10,
	249,
	14,
	23,
	614,
	249,
	14,
	26,
	10,
	249,
	14,
	10,
	32,
	10,
	26,
	249,
	43,
	89,
	89,
	89,
	9,
	249,
	112,
	26,
	10,
	3,
	26,
	3,
	14,
	3,
	26,
	3,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	23,
	14,
	26,
	10,
	249,
	14,
	3,
	10,
	249,
	26,
	10,
	249,
	26,
	26,
	10,
	249,
	10,
	10,
	10,
	10,
	10,
	249,
	26,
	26,
	26,
	10,
	249,
	10,
	249,
	26,
	10,
	26,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	23,
	14,
	26,
	10,
	249,
	14,
	3,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	10,
	26,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	23,
	249,
	614,
	14,
	26,
	10,
	249,
	10,
	10,
	10,
	10,
	249,
	30,
	10,
	26,
	3,
	115,
	26,
	10,
	34,
	34,
	34,
	37,
	129,
	10,
	14,
	1974,
	32,
	23,
	512,
	10,
	89,
	32,
	23,
	23,
	89,
	89,
	89,
	23,
	184,
	184,
	210,
	742,
	35,
	23,
	26,
	742,
	184,
	184,
	210,
	10,
	23,
	32,
	89,
	512,
	23,
	10,
	463,
	249,
	35,
	184,
	10,
	10,
	10,
	3,
	130,
	35,
	23,
	23,
	133,
	32,
	26,
	10,
	23,
	32,
	512,
	23,
	23,
	89,
	32,
	35,
	10,
	463,
	184,
	249,
	10,
	10,
	10,
	184,
	184,
	210,
	742,
	27,
	209,
	14,
	10,
	14,
	93,
	34,
	34,
	34,
	37,
	129,
	10,
	14,
	14,
	26,
	26,
	10,
	14,
	10,
	3,
	34,
	34,
	34,
	37,
	129,
	10,
	14,
	10,
	130,
	26,
	10,
	14,
	14,
	14,
	26,
	26,
	130,
	14,
	14,
	14,
	23,
	89,
	23,
	26,
	184,
	10,
	23,
	32,
	186,
	512,
	23,
	23,
	89,
	32,
	10,
	463,
	249,
	35,
	184,
	10,
	10,
	10,
	742,
	184,
	210,
	10,
	32,
	26,
	0,
	0,
	115,
	209,
	27,
	118,
	26,
	34,
	10,
	14,
	14,
	32,
	3,
	27,
	14,
	89,
	27,
	89,
	14,
	26,
	10,
	1983,
	183,
	184,
	26,
	27,
	26,
	14,
	26,
	10,
	32,
	10,
	23,
	26,
	26,
	26,
	0,
	219,
	301,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	26,
	10,
	32,
	10,
	32,
	14,
	26,
	3,
	23,
	3,
	119,
	0,
	0,
	94,
	94,
	137,
	137,
	32,
	10,
	14,
	43,
	14,
	3,
	10,
	26,
	14,
	249,
	23,
	26,
	10,
	1103,
	32,
	32,
	32,
	210,
	341,
	26,
	35,
	130,
	4,
	10,
	23,
	10,
	23,
	32,
	26,
	35,
	459,
	26,
	35,
	32,
	341,
	32,
	210,
	32,
	3,
	34,
	26,
	35,
	32,
	341,
	32,
	210,
	32,
	23,
	34,
	130,
	10,
	10,
	23,
	26,
	35,
	32,
	341,
	32,
	210,
	32,
	23,
	249,
	614,
	249,
	614,
	14,
	26,
	10,
	249,
	10,
	26,
	10,
	249,
	10,
	26,
	249,
	107,
	10,
	10,
	10,
	463,
	35,
	10,
	10,
	10,
	249,
	10,
	184,
	0,
	1,
	26,
	1,
	90,
	137,
	34,
	4,
	62,
	23,
	10,
	32,
	46,
	23,
	35,
	37,
	37,
	37,
	984,
	26,
	89,
	26,
	35,
	14,
	23,
	249,
	614,
	14,
	26,
	10,
	249,
	23,
	614,
	10,
	32,
	14,
	26,
	10,
	249,
	14,
	23,
	10,
	32,
	14,
	26,
	10,
	249,
	14,
	23,
	249,
	614,
	249,
	614,
	14,
	26,
	10,
	249,
	14,
	341,
	463,
	14,
	26,
	10,
	249,
	14,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	26,
	10,
	249,
	14,
	249,
	23,
	26,
	26,
	10,
	249,
	14,
	14,
	3,
	249,
	32,
	31,
	14,
	26,
	10,
	249,
	3,
	249,
	10,
	10,
	10,
	10,
	26,
	10,
	249,
	26,
	137,
	94,
	10,
	249,
	3,
	23,
	14,
	26,
	10,
	249,
	14,
	3,
	249,
	14,
	3,
	249,
	10,
	26,
	23,
	249,
	614,
	249,
	614,
	249,
	614,
	89,
	89,
	249,
	249,
	249,
	614,
	249,
	89,
	249,
	249,
	249,
	249,
	614,
	249,
	89,
	89,
	249,
	89,
	89,
	89,
	89,
	89,
	89,
	249,
	614,
	249,
	249,
	249,
	249,
	249,
	614,
	249,
	249,
	10,
	32,
	249,
	249,
	249,
	249,
	249,
	249,
	249,
	614,
	249,
	249,
	14,
	26,
	10,
	249,
	10,
	9,
	3,
	23,
	89,
	89,
	89,
	14,
	26,
	26,
	10,
	26,
	249,
	14,
	38,
	10,
	10,
	10,
	14,
	26,
	23,
	57,
	56,
	10,
	26,
	34,
	37,
	37,
	0,
	37,
	14,
	26,
	10,
	249,
	129,
	10,
	10,
	14,
	26,
	23,
	249,
	614,
	14,
	26,
	21,
	21,
	249,
	27,
	26,
	10,
	14,
	14,
	14,
	249,
	249,
	249,
	14,
	26,
	10,
	249,
	23,
	249,
	614,
	14,
	26,
	10,
	249,
	23,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	26,
	14,
	249,
	614,
	249,
	614,
	249,
	614,
	249,
	614,
	249,
	14,
	26,
	10,
	249,
	10,
	9,
	3,
	26,
	10,
	14,
	26,
	26,
	10,
	26,
	14,
	249,
	14,
	62,
	10,
	14,
	14,
	26,
	10,
	249,
	14,
	26,
	10,
	26,
	14,
	14,
	10,
	4,
	4,
	829,
	43,
	301,
	14,
	10,
	89,
	10,
	23,
	89,
	26,
	31,
	463,
	341,
	249,
	89,
	31,
	14,
	26,
	14,
	14,
	10,
	249,
	23,
	23,
	32,
	31,
	89,
	10,
	10,
	9,
	10,
	26,
	26,
	14,
	23,
	89,
	31,
	14,
	26,
	10,
	249,
	14,
	23,
	249,
	614,
	249,
	614,
	249,
	614,
	249,
	614,
	14,
	26,
	10,
	249,
	14,
	23,
	89,
	31,
	14,
	26,
	10,
	249,
	14,
	26,
	10,
	249,
	14,
	89,
	3,
	26,
	14,
	249,
	14,
	23,
	614,
	249,
	14,
	26,
	10,
	249,
	23,
	249,
	26,
	10,
	14,
	14,
	38,
	10,
	10,
	89,
	3,
	38,
	26,
	23,
	249,
	14,
	137,
	249,
	26,
	10,
	3,
	23,
	32,
	10,
	32,
	10,
	32,
	10,
	37,
	14,
	26,
	10,
	21,
	249,
	14,
	23,
	14,
	26,
	10,
	249,
	3,
	32,
	14,
	26,
	10,
	249,
	31,
	89,
	14,
	26,
	10,
	249,
	14,
	3,
	10,
	10,
	249,
	14,
	1103,
	10,
	249,
	3,
	23,
	14,
	10,
	32,
	26,
	26,
	10,
	249,
	14,
	26,
	10,
	249,
	35,
	249,
	34,
	10,
	249,
	26,
	14,
	14,
	23,
	89,
	31,
	89,
	31,
	14,
	26,
	10,
	249,
	623,
	10,
	10,
	10,
	10,
	422,
	14,
	249,
	10,
	26,
	14,
	10,
	249,
	10,
	422,
	1246,
	249,
	26,
	10,
	26,
	10,
	249,
	14,
	46,
	249,
	89,
	89,
	89,
	89,
	89,
	14,
	89,
	14,
	14,
	14,
	14,
	10,
	26,
	249,
	123,
	249,
	26,
	10,
	10,
	10,
	3,
	23,
	14,
	26,
	26,
	10,
	463,
	341,
	249,
	14,
	26,
	10,
	249,
	1103,
	10,
	249,
	26,
	10,
	249,
	26,
	26,
	10,
	249,
	32,
	10,
	14,
	26,
	10,
	249,
	14,
	32,
	14,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	26,
	10,
	249,
	23,
	89,
	31,
	14,
	26,
	10,
	249,
	23,
	89,
	31,
	14,
	26,
	10,
	249,
	14,
	23,
	89,
	31,
	14,
	26,
	10,
	249,
	14,
	23,
	249,
	614,
	249,
	614,
	249,
	614,
	249,
	614,
	249,
	614,
	249,
	614,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	249,
	614,
	249,
	614,
	463,
	341,
	463,
	341,
	249,
	614,
	14,
	26,
	10,
	249,
	14,
	10,
	26,
	249,
	614,
	31,
	89,
	31,
	14,
	26,
	10,
	249,
	3,
	614,
	31,
	31,
	14,
	26,
	10,
	249,
	14,
	3,
	89,
	26,
	10,
	249,
	3,
	0,
	0,
	0,
	0,
	0,
	0,
	28,
	14,
	26,
	28,
	14,
	26,
	28,
	14,
	459,
	14,
	14,
	14,
	27,
	28,
	89,
	14,
	89,
	129,
	0,
	26,
	10,
	10,
	10,
	26,
	118,
	10,
	10,
	249,
	184,
	210,
	742,
	89,
	23,
	32,
	10,
	249,
	10,
	184,
	10,
	10,
	463,
	26,
	35,
	14,
	10,
	89,
	184,
	23,
	89,
	89,
	89,
	35,
	512,
	23,
	249,
	614,
	14,
	249,
	14,
	10,
	26,
	32,
	31,
	89,
	31,
	14,
	26,
	10,
	249,
	14,
	3,
	249,
	26,
	10,
	463,
	14,
	26,
	26,
	10,
	249,
	32,
	23,
	89,
	10,
	32,
	10,
	32,
	10,
	32,
	249,
	614,
	249,
	249,
	249,
	89,
	89,
	89,
	31,
	89,
	249,
	249,
	89,
	89,
	89,
	14,
	26,
	10,
	10,
	249,
	112,
	9,
	10,
	14,
	3,
	23,
	89,
	31,
	14,
	26,
	10,
	249,
	14,
	249,
	26,
	10,
	26,
	10,
	249,
	129,
	89,
	10,
	10,
	10,
	14,
	26,
	10,
	249,
	14,
	10,
	249,
	26,
	28,
	26,
	137,
	23,
	112,
	10,
	10,
	34,
	14,
	249,
	10,
	9,
	26,
	34,
	10,
	3,
	35,
	26,
	34,
	119,
	14,
	14,
	23,
	26,
	249,
	14,
	26,
	14,
	14,
	23,
	89,
	32,
	249,
	614,
	14,
	32,
	14,
	1984,
	26,
	10,
	249,
	3,
	218,
	4,
	1985,
	89,
	89,
	89,
	14,
	10,
	26,
	249,
	14,
	0,
	14,
	3,
	23,
	26,
	14,
	26,
	10,
	249,
	3,
	249,
	10,
	26,
	3,
	26,
	10,
	249,
	26,
	27,
	10,
	26,
	26,
	249,
	26,
	10,
	249,
	23,
	249,
	14,
	26,
	10,
	106,
	26,
	26,
	10,
	14,
	43,
	46,
	249,
	14,
	26,
	10,
	249,
	14,
	26,
	10,
	249,
	32,
	31,
	14,
	26,
	10,
	249,
	3,
	23,
	89,
	31,
	14,
	26,
	10,
	249,
	14,
	23,
	249,
	14,
	23,
	249,
	614,
	249,
	614,
	249,
	614,
	249,
	614,
	249,
	614,
	89,
	89,
	89,
	89,
	89,
	10,
	32,
	10,
	32,
	249,
	614,
	249,
	614,
	14,
	26,
	10,
	249,
	3,
	32,
	31,
	89,
	31,
	14,
	26,
	10,
	249,
	3,
	23,
	249,
	614,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	249,
	614,
	249,
	614,
	10,
	32,
	249,
	614,
	249,
	614,
	10,
	14,
	26,
	10,
	249,
	14,
	3,
	23,
	14,
	26,
	14,
	26,
	10,
	249,
	26,
	10,
	249,
	23,
	89,
	31,
	89,
	89,
	89,
	89,
	89,
	31,
	89,
	89,
	89,
	89,
	14,
	26,
	10,
	249,
	14,
	3,
	43,
	46,
	26,
	671,
	10,
	249,
	10,
	10,
	10,
	3,
	26,
	54,
	820,
	820,
	1681,
	129,
	759,
	10,
	32,
	32,
	43,
	14,
	37,
	38,
	54,
	54,
	10,
	23,
	27,
	14,
	14,
	89,
	89,
	14,
	14,
	209,
	209,
	446,
	28,
	28,
	14,
	14,
	14,
	9,
	28,
	28,
	28,
	28,
	28,
	26,
	14,
	89,
	105,
	14,
	14,
	10,
	27,
	14,
	10,
	89,
	26,
	23,
	27,
	9,
	34,
	10,
	14,
	10,
	23,
	26,
	459,
	105,
	26,
	14,
	26,
	26,
	1986,
	23,
	808,
	26,
	32,
	10,
	89,
	46,
	14,
	26,
	89,
	14,
	26,
	31,
	31,
	32,
	10,
	32,
	10,
	10,
	32,
	23,
	10,
	10,
	46,
	26,
	26,
	26,
	808,
	89,
	23,
	14,
	26,
	9,
	10,
	41,
	23,
	130,
	808,
	26,
	14,
	89,
	23,
	0,
	137,
	26,
	27,
	23,
	10,
	26,
	23,
	808,
	32,
	26,
	27,
	26,
	26,
	26,
	36,
	23,
	162,
	1,
	560,
	21,
	131,
	21,
	131,
	449,
	449,
	106,
	106,
	130,
	89,
	37,
	129,
	32,
	26,
	14,
	3,
	62,
	10,
	14,
	1987,
	209,
	26,
	1988,
	27,
	173,
	3,
	26,
	10,
	37,
	10,
	32,
	23,
	26,
	10,
	3,
	32,
	34,
	202,
	26,
	10,
	23,
	26,
	32,
	34,
	202,
	26,
	10,
	130,
	10,
	10,
	10,
	112,
	10,
	511,
	184,
	1989,
	107,
	35,
	26,
	27,
	26,
	10,
	89,
	49,
	560,
	119,
	26,
	3,
	26,
	202,
	32,
	32,
	32,
	131,
	26,
	14,
	118,
	1,
	26,
	23,
	89,
	23,
	130,
	89,
	89,
	14,
	14,
	10,
	3,
	27,
	1064,
	209,
	10,
	14,
	10,
	32,
	26,
	118,
	26,
	94,
	560,
	138,
	560,
	1,
	119,
	21,
	0,
	21,
	26,
	14,
	3,
	26,
	10,
	10,
	14,
	10,
	32,
	341,
	26,
	14,
	463,
	110,
	14,
	89,
	14,
	31,
	89,
	14,
	1990,
	209,
	23,
	112,
	14,
	14,
	32,
	1991,
	10,
	14,
	341,
	26,
	31,
	26,
	23,
	14,
	463,
	34,
	785,
	62,
	110,
	14,
	14,
	31,
	89,
	89,
	89,
	14,
	14,
	32,
	14,
	10,
	10,
	89,
	14,
	26,
	23,
	3,
	249,
	14,
	1992,
	1992,
	249,
	14,
	10,
	9,
	26,
	672,
	3,
	34,
	34,
	28,
	28,
	58,
	28,
	112,
	41,
	14,
	0,
	26,
	112,
	41,
	28,
	58,
	34,
	28,
	34,
	28,
	14,
	3,
	130,
	89,
	89,
	14,
	89,
	89,
	14,
	112,
	14,
	10,
	209,
	14,
	10,
	14,
	23,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	23,
	3,
	14,
	10,
	10,
	37,
	23,
	26,
	27,
	27,
	14,
	14,
	14,
	26,
	10,
	10,
	37,
	422,
	112,
	9,
	10,
	14,
	34,
	34,
	14,
	23,
	118,
	209,
	34,
	202,
	28,
	10,
	32,
	14,
	26,
	34,
	34,
	136,
	614,
	14,
	112,
	9,
	10,
	34,
	14,
	28,
	27,
	23,
	26,
	34,
	28,
	459,
	34,
	10,
	10,
	249,
	14,
	28,
	28,
	14,
	1993,
	26,
	9,
	10,
	184,
	14,
	14,
	0,
	221,
	131,
	241,
	286,
	26,
	28,
	23,
	3,
	23,
	28,
	4,
	198,
	3,
	23,
	3,
	32,
	3,
	23,
	3,
	23,
	3,
	27,
	14,
	14,
	43,
	32,
	14,
	3,
	130,
	14,
	26,
	26,
	26,
	10,
	10,
	0,
	14,
	14,
	3,
	26,
	118,
	493,
	23,
	28,
	48,
	48,
	48,
	23,
	614,
	14,
	14,
	14,
	0,
	114,
	48,
	107,
	9,
	14,
	34,
	215,
	1,
	14,
	14,
	242,
	32,
	9,
	28,
	215,
	62,
	48,
	14,
	14,
	14,
	14,
	123,
	14,
	130,
	14,
	14,
	89,
	1994,
	14,
	10,
	2,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	14,
	34,
	459,
	14,
	89,
	14,
	27,
	14,
	14,
	14,
	1995,
	817,
	89,
	89,
	14,
	89,
	89,
	14,
	9,
	14,
	1,
	119,
	32,
	1996,
	14,
	1997,
	10,
	14,
	10,
	10,
	89,
	89,
	14,
	89,
	14,
	4,
	137,
	114,
	0,
	114,
	114,
	163,
	94,
	3,
	4,
	27,
	43,
	34,
	244,
	28,
	0,
	32,
	26,
	817,
	114,
	114,
	1185,
	1209,
	817,
	27,
	26,
	27,
	209,
	10,
	10,
	0,
	26,
	26,
	114,
	14,
	14,
	3,
	32,
	56,
	10,
	26,
	62,
	14,
	132,
	0,
	298,
	298,
	0,
	14,
	94,
	94,
	192,
	10,
	89,
	26,
	14,
	14,
	89,
	31,
	89,
	249,
	14,
	23,
	3,
	28,
	10,
	89,
	23,
	61,
	89,
	14,
	10,
	14,
	89,
	14,
	28,
	500,
	114,
	672,
	244,
	89,
	30,
	89,
	89,
	26,
	89,
	10,
	14,
	23,
	23,
	89,
	10,
	28,
	3,
	89,
	14,
	23,
	23,
	26,
	1998,
	26,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	89,
	31,
	89,
	31,
	10,
	32,
	89,
	31,
	89,
	31,
	14,
	89,
	14,
	3,
	1998,
	26,
	26,
	89,
	26,
	10,
	14,
	14,
	28,
	130,
	26,
	14,
	26,
	10,
	10,
	32,
	14,
	28,
	89,
	26,
	26,
	14,
	89,
	10,
	26,
	89,
	1998,
	26,
	26,
	89,
	1999,
	26,
	89,
	14,
	56,
	26,
	112,
	10,
	10,
	10,
	14,
	0,
	89,
	26,
	4,
	89,
	10,
	89,
	14,
	26,
	28,
	89,
	89,
	23,
	26,
	61,
	43,
	43,
	4,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	249,
	14,
	26,
	10,
	28,
	10,
	14,
	14,
	3,
	89,
	89,
	23,
	26,
	26,
	26,
	10,
	14,
	23,
	89,
	10,
	28,
	3,
	26,
	28,
	14,
	89,
	10,
	26,
	26,
	28,
	14,
	89,
	10,
	26,
	23,
	89,
	10,
	28,
	3,
	23,
	89,
	10,
	28,
	3,
	43,
	32,
	26,
	26,
	14,
	10,
	3,
	26,
	26,
	10,
	249,
	249,
	14,
	14,
	252,
	0,
	62,
	43,
	26,
	10,
	61,
	0,
	119,
	301,
	26,
	10,
	3,
	23,
	89,
	10,
	28,
	3,
	23,
	89,
	10,
	28,
	3,
	23,
	89,
	10,
	26,
	14,
	28,
	10,
	3,
	46,
	26,
	32,
	10,
	26,
	10,
	14,
	14,
	23,
	89,
	10,
	28,
	3,
	23,
	89,
	10,
	28,
	3,
	32,
	26,
	26,
	10,
	14,
	89,
	14,
	26,
	26,
	14,
	26,
	32,
	10,
	26,
	14,
	89,
	23,
	26,
	10,
	14,
	3,
	23,
	89,
	10,
	28,
	3,
	32,
	26,
	10,
	26,
	10,
	28,
	14,
	89,
	38,
	129,
	26,
	26,
	10,
	28,
	14,
	89,
	10,
	10,
	23,
	89,
	10,
	28,
	3,
	26,
	26,
	341,
	463,
	26,
	10,
	14,
	23,
	26,
	10,
	14,
	3,
	23,
	89,
	10,
	28,
	3,
	23,
	89,
	10,
	28,
	3,
	23,
	89,
	10,
	26,
	14,
	28,
	10,
	3,
	23,
	26,
	26,
	26,
	10,
	32,
	89,
	31,
	89,
	31,
	10,
	32,
	14,
	89,
	3,
	26,
	2000,
	26,
	26,
	14,
	89,
	10,
	14,
	26,
	130,
	14,
	26,
	10,
	10,
	32,
	28,
	14,
	89,
	26,
	14,
	26,
	10,
	10,
	14,
	89,
	26,
	89,
	2000,
	26,
	26,
	89,
	26,
	26,
	26,
	10,
	14,
	14,
	3,
	23,
	89,
	10,
	28,
	3,
	26,
	26,
	10,
	10,
	10,
	14,
	14,
	23,
	89,
	10,
	28,
	3,
	23,
	89,
	10,
	28,
	3,
	23,
	89,
	10,
	26,
	14,
	28,
	10,
	3,
	23,
	89,
	26,
	10,
	14,
	89,
	14,
	0,
	137,
	198,
	137,
	114,
	114,
	48,
	135,
	114,
	3,
	26,
	28,
	523,
	10,
	10,
	10,
	10,
	14,
	10,
	3,
	3,
	500,
	4,
	43,
	23,
	105,
	58,
	26,
	105,
	58,
	26,
	0,
	105,
	2001,
	62,
	89,
	14,
	829,
	0,
	3,
	23,
	3,
	26,
	9,
	1,
	1,
	27,
	114,
	135,
	89,
	14,
	14,
	14,
	14,
	0,
	344,
	52,
	89,
	89,
	10,
	10,
	10,
	10,
	10,
	14,
	344,
	26,
	14,
	157,
	14,
	21,
	0,
	344,
	26,
	21,
	26,
	10,
	21,
	26,
	26,
	129,
	2000,
	740,
	10,
	249,
	89,
	89,
	14,
	94,
	114,
	138,
	138,
	43,
	0,
	119,
	14,
	14,
	26,
	363,
	135,
	135,
	9,
	10,
	219,
	210,
	14,
	10,
	3,
	23,
	105,
	58,
	3,
	23,
	105,
	58,
	3,
	23,
	105,
	58,
	3,
	26,
	105,
	58,
	3,
	26,
	673,
	112,
	105,
	58,
	3,
	26,
	105,
	58,
	3,
	130,
	23,
	10,
	89,
	89,
	34,
	32,
	130,
	3,
	3,
	119,
	32,
	43,
	3,
	119,
	14,
	2002,
	14,
	14,
	10,
	738,
	14,
	14,
	3,
	98,
	219,
	137,
	199,
	199,
	46,
	94,
	376,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	163,
	622,
	135,
	205,
	3,
	3,
	130,
	26,
	210,
	0,
	219,
	0,
	21,
	10,
	34,
	2003,
	1386,
	252,
	194,
	2004,
	10,
	10,
	37,
	10,
	0,
	21,
	21,
	112,
	112,
	9,
	10,
	10,
	34,
	184,
	34,
	28,
	28,
	28,
	28,
	28,
	1,
	1,
	28,
	119,
	119,
	1,
	1,
	1,
	1,
	1,
	23,
	32,
	26,
	205,
	26,
	14,
	34,
	23,
	112,
	23,
	130,
	32,
	32,
	782,
	71,
	32,
	32,
	1103,
	105,
	105,
	1973,
	2005,
	3,
	32,
	37,
	662,
	37,
	662,
	37,
	30,
	37,
	116,
	662,
	2006,
	2007,
	56,
	225,
	225,
	43,
	3,
	32,
	133,
	62,
	31,
	26,
	88,
	14,
	26,
	241,
	130,
	23,
	10,
	14,
	26,
	58,
	130,
	9,
	10,
	14,
	829,
	219,
	2008,
	43,
	43,
	43,
	219,
	302,
	829,
	219,
	0,
	302,
	3,
	119,
	0,
	163,
	32,
	62,
	623,
	10,
	62,
	26,
	26,
	14,
	23,
	32,
	129,
	808,
	30,
	9,
	9,
	37,
	10,
	10,
	32,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	138,
	193,
	137,
	35,
	26,
	10,
	32,
	10,
	10,
	184,
	249,
	10,
	10,
	35,
	463,
	35,
	130,
	32,
	32,
	341,
	32,
	210,
	32,
	26,
	35,
	10,
	34,
	10,
	26,
	10,
	10,
	463,
	10,
	184,
	249,
	10,
	173,
	35,
	23,
	31,
	26,
	32,
	341,
	32,
	210,
	32,
	26,
	35,
	220,
	131,
	244,
	94,
	131,
	94,
	221,
	221,
	101,
	2009,
	2010,
	194,
	586,
	2011,
	205,
	220,
	2012,
	2013,
	2013,
	32,
	1055,
	1974,
	26,
	14,
	23,
	26,
	62,
	372,
	62,
	26,
	62,
	372,
	23,
	177,
	930,
	0,
	0,
	3,
	26,
	27,
	32,
	62,
	2014,
	249,
	2015,
	26,
	26,
	2016,
	14,
	205,
	137,
	137,
	114,
	119,
	119,
	0,
	137,
	137,
	94,
	218,
	218,
	43,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000271, { 0, 7 } },
	{ 0x02000272, { 7, 2 } },
	{ 0x02000280, { 9, 13 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[22] = 
{
	{ (Il2CppRGCTXDataType)3, 33043 },
	{ (Il2CppRGCTXDataType)2, 16153 },
	{ (Il2CppRGCTXDataType)3, 33044 },
	{ (Il2CppRGCTXDataType)2, 16156 },
	{ (Il2CppRGCTXDataType)3, 33045 },
	{ (Il2CppRGCTXDataType)2, 22784 },
	{ (Il2CppRGCTXDataType)3, 33046 },
	{ (Il2CppRGCTXDataType)3, 33047 },
	{ (Il2CppRGCTXDataType)2, 16162 },
	{ (Il2CppRGCTXDataType)2, 22786 },
	{ (Il2CppRGCTXDataType)3, 33048 },
	{ (Il2CppRGCTXDataType)2, 22787 },
	{ (Il2CppRGCTXDataType)3, 33049 },
	{ (Il2CppRGCTXDataType)2, 22788 },
	{ (Il2CppRGCTXDataType)3, 33050 },
	{ (Il2CppRGCTXDataType)3, 33051 },
	{ (Il2CppRGCTXDataType)3, 33052 },
	{ (Il2CppRGCTXDataType)3, 33053 },
	{ (Il2CppRGCTXDataType)3, 33054 },
	{ (Il2CppRGCTXDataType)3, 33055 },
	{ (Il2CppRGCTXDataType)3, 33056 },
	{ (Il2CppRGCTXDataType)3, 33057 },
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationNPOI;
extern const Il2CppCodeGenModule g_NPOICodeGenModule;
const Il2CppCodeGenModule g_NPOICodeGenModule = 
{
	"NPOI.dll",
	3964,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	22,
	s_rgctxValues,
	&g_DebuggerMetadataRegistrationNPOI,
};
