﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern void RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E_AdjustorThunk (void);
// 0x00000002 UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern void RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E_AdjustorThunk (void);
// 0x00000003 UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern void RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674_AdjustorThunk (void);
// 0x00000004 System.Single UnityEngine.RaycastHit::get_distance()
extern void RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA_AdjustorThunk (void);
// 0x00000005 System.Void UnityEngine.RaycastHit::set_distance(System.Single)
extern void RaycastHit_set_distance_m287DCD9DAE1D3AE76C7C4DE2F192E4DFBA911F41_AdjustorThunk (void);
// 0x00000006 UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern void RaycastHit_get_transform_m2DD983DBD3602DE848DE287EE5233FD02EEC608D_AdjustorThunk (void);
// 0x00000007 UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern void RaycastHit_get_rigidbody_mE48E45893FDAFD03162C6A73AAF02C6CB0E079FB_AdjustorThunk (void);
// 0x00000008 System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern void Rigidbody_set_isKinematic_mCF74D680205544826F2DE2CAB929C9F25409A311 (void);
// 0x00000009 System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints)
extern void Rigidbody_set_constraints_mA76F562D16D3BE8889E095D0309C8FE38DA914F1 (void);
// 0x0000000A System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700 (void);
// 0x0000000B System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void Rigidbody_AddTorque_mEDE3483056FB07222A4D096F22D45C7D8A6E2552 (void);
// 0x0000000C System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3)
extern void Rigidbody_AddTorque_mAEB5758FA773B1A0ECDD328934BB3A7202D21EB3 (void);
// 0x0000000D System.Void UnityEngine.Rigidbody::.ctor()
extern void Rigidbody__ctor_m0E43BA3B0E70E71B2CA62B165EE5B7CFAEFACDE9 (void);
// 0x0000000E System.Void UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddForce_Injected_m233C3E22C3FE9D2BCBBC510132B82CE26057370C (void);
// 0x0000000F System.Void UnityEngine.Rigidbody::AddTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode)
extern void Rigidbody_AddTorque_Injected_mFFD31FDF8F82D3D740EA83348BBC0D0D5EB0DB3A (void);
// 0x00000010 System.Boolean UnityEngine.Collider::get_enabled()
extern void Collider_get_enabled_m03B73B5C97033F939387D1785BDF2619CADAEEB0 (void);
// 0x00000011 UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern void Collider_get_attachedRigidbody_m101FED12AD292F372F98E94A6D02A5E428AA896A (void);
// 0x00000012 UnityEngine.Vector3 UnityEngine.Collider::ClosestPoint(UnityEngine.Vector3)
extern void Collider_ClosestPoint_m7777917E298B31796DEE906B54F0102F6ED76676 (void);
// 0x00000013 System.Void UnityEngine.Collider::.ctor()
extern void Collider__ctor_m09D7A9B985D74FD50346DA08D88EB1874E968B69 (void);
// 0x00000014 System.Void UnityEngine.Collider::ClosestPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Collider_ClosestPoint_Injected_m6D72FF73D51838EE47234CB4D6234521C08B780D (void);
// 0x00000015 UnityEngine.Mesh UnityEngine.MeshCollider::get_sharedMesh()
extern void MeshCollider_get_sharedMesh_m77D27894F87619BD2AF868BC1156583EEC0D8CB4 (void);
// 0x00000016 UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
extern void CapsuleCollider_get_center_m6374F7457A9450CAFFAD2DF0C9D1419BF9E304CB (void);
// 0x00000017 System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3)
extern void CapsuleCollider_set_center_m36F35F070DFC2CBFC87532004073CA8D56F3678F (void);
// 0x00000018 System.Single UnityEngine.CapsuleCollider::get_radius()
extern void CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0 (void);
// 0x00000019 System.Void UnityEngine.CapsuleCollider::set_radius(System.Single)
extern void CapsuleCollider_set_radius_mD4502A8676AAC093F1E9958FB7D5D765EA206432 (void);
// 0x0000001A System.Single UnityEngine.CapsuleCollider::get_height()
extern void CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526 (void);
// 0x0000001B System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
extern void CapsuleCollider_set_height_m728C9AF3772EEC1DA9845E19F3C2899CDD2D9496 (void);
// 0x0000001C System.Int32 UnityEngine.CapsuleCollider::get_direction()
extern void CapsuleCollider_get_direction_m2C5110BBCA2CC1C63183CDBD73B6D11CC89B0918 (void);
// 0x0000001D System.Void UnityEngine.CapsuleCollider::set_direction(System.Int32)
extern void CapsuleCollider_set_direction_mE5E7B7BB7FBBBA97148E8CFCDC46D1BB14673984 (void);
// 0x0000001E UnityEngine.Vector2 UnityEngine.CapsuleCollider::GetGlobalExtents()
extern void CapsuleCollider_GetGlobalExtents_m96E63F24BCAF34DDBC49E743C3203D19638501AF (void);
// 0x0000001F UnityEngine.Matrix4x4 UnityEngine.CapsuleCollider::CalculateTransform()
extern void CapsuleCollider_CalculateTransform_m5DA33B8D319E92F1FBE72447A8CDE5223DB69258 (void);
// 0x00000020 System.Void UnityEngine.CapsuleCollider::.ctor()
extern void CapsuleCollider__ctor_mB993913A662CF20B09C4A7475B66E820C87879A4 (void);
// 0x00000021 System.Void UnityEngine.CapsuleCollider::get_center_Injected(UnityEngine.Vector3&)
extern void CapsuleCollider_get_center_Injected_mBDF07E6DE932FBCAF0AA6F167EB993BE03BE6872 (void);
// 0x00000022 System.Void UnityEngine.CapsuleCollider::set_center_Injected(UnityEngine.Vector3&)
extern void CapsuleCollider_set_center_Injected_mB43C17AC81236FB74E61E21215D2145663A7DEEE (void);
// 0x00000023 System.Void UnityEngine.CapsuleCollider::GetGlobalExtents_Injected(UnityEngine.Vector2&)
extern void CapsuleCollider_GetGlobalExtents_Injected_m3604AFE1AB8E8F50E203CF50E7A484346DC72C18 (void);
// 0x00000024 System.Void UnityEngine.CapsuleCollider::CalculateTransform_Injected(UnityEngine.Matrix4x4&)
extern void CapsuleCollider_CalculateTransform_Injected_mA1A6CB7A01489F8442B921163D0149EE6AE329EC (void);
// 0x00000025 UnityEngine.Vector3 UnityEngine.BoxCollider::get_center()
extern void BoxCollider_get_center_m832B93439717C72D4A3B427C6E8F5B54E2DBD669 (void);
// 0x00000026 UnityEngine.Vector3 UnityEngine.BoxCollider::get_size()
extern void BoxCollider_get_size_mBC38D4926D4BE54A6532F6E1D642F363CA3A58A1 (void);
// 0x00000027 System.Void UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&)
extern void BoxCollider_get_center_Injected_m5F060E93AE651FB756B9371C7A716BDAA2A96F51 (void);
// 0x00000028 System.Void UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&)
extern void BoxCollider_get_size_Injected_mC9ABFED6FEC27351BC116B44021578CB39CBCB22 (void);
// 0x00000029 UnityEngine.Vector3 UnityEngine.SphereCollider::get_center()
extern void SphereCollider_get_center_mBFAE4FFFC76B8FD8F1B2B2F12C52A30470443D3A (void);
// 0x0000002A System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3)
extern void SphereCollider_set_center_mD5E898A2FED304A82BC67ABB11B60BB0F612CED7 (void);
// 0x0000002B System.Single UnityEngine.SphereCollider::get_radius()
extern void SphereCollider_get_radius_m403989140BDAD513299276953B481167CF08D02F (void);
// 0x0000002C System.Void UnityEngine.SphereCollider::set_radius(System.Single)
extern void SphereCollider_set_radius_m55A0D144B32871AECC2A83FBCF423FBE1E5A63A0 (void);
// 0x0000002D System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_get_center_Injected_mF082CC62A7E8DE3DD5B86518C210D85139DD93CB (void);
// 0x0000002E System.Void UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&)
extern void SphereCollider_set_center_Injected_mB213D6CB194E150BBEF8EC2AA31EA391C2F4A641 (void);
// 0x0000002F System.String UnityEngine.PhysicsScene::ToString()
extern void PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F_AdjustorThunk (void);
// 0x00000030 System.Int32 UnityEngine.PhysicsScene::GetHashCode()
extern void PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2_AdjustorThunk (void);
// 0x00000031 System.Boolean UnityEngine.PhysicsScene::Equals(System.Object)
extern void PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC_AdjustorThunk (void);
// 0x00000032 System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene)
extern void PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8_AdjustorThunk (void);
// 0x00000033 System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187_AdjustorThunk (void);
// 0x00000034 System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_m6715CAD8D0330195269AA7FCCD91613BC564E3EB (void);
// 0x00000035 System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208_AdjustorThunk (void);
// 0x00000036 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_m1E630AA11805114B090FC50741AEF64D677AF2C7 (void);
// 0x00000037 System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73_AdjustorThunk (void);
// 0x00000038 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_mE7833EE5F1082431A4C2D29362F5DCDEFBF6D2BD (void);
// 0x00000039 System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_CapsuleCast_mEF92DCD7186E4139FA84959DC45DD26E8DBFAC6C (void);
// 0x0000003A System.Boolean UnityEngine.PhysicsScene::Internal_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_CapsuleCast_mAB95448C77F1E823CA8028F693724C9D385B0406 (void);
// 0x0000003B System.Boolean UnityEngine.PhysicsScene::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_CapsuleCast_mE4112B782763A75FE885111A8C45F0B74C2CB784_AdjustorThunk (void);
// 0x0000003C System.Boolean UnityEngine.PhysicsScene::Query_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_SphereCast_m47352512958CE213DD034AE53DB2E0C0247A38C3 (void);
// 0x0000003D System.Boolean UnityEngine.PhysicsScene::Internal_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_SphereCast_mFEE43A5306A097D0D2CB389A200921320130E55B (void);
// 0x0000003E System.Boolean UnityEngine.PhysicsScene::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_SphereCast_m72928994362EE11F9A4F72BD79954444B9FD1C2D_AdjustorThunk (void);
// 0x0000003F System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastTest_Injected_m956474EFEA507F82ABE0BCB75DDB3CC8EFF4D12B (void);
// 0x00000040 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_Raycast_Injected_m8868E0BF89F8C42D11F42F94F9CF767647433BB1 (void);
// 0x00000041 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Internal_RaycastNonAlloc_Injected_m59FB44C18E62D21E9320F30229C0AA9F9B971211 (void);
// 0x00000042 System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_CapsuleCast_Injected_m20A80C56467001CAD7BAF8FCADE40D58EF90EF57 (void);
// 0x00000043 System.Boolean UnityEngine.PhysicsScene::Query_SphereCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void PhysicsScene_Query_SphereCast_Injected_m6DD8E497811A3098113F5E9F52D7B0CAE63DC1EB (void);
// 0x00000044 UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene()
extern void Physics_get_defaultPhysicsScene_mA6361488FBAC110DA8397E5E4CB473EBF4191010 (void);
// 0x00000045 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m9DE0EEA1CF8DEF7D06216225F19E2958D341F7BA (void);
// 0x00000046 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_Raycast_m09F21E13465121A73F19C07FC5F5314CF15ACD15 (void);
// 0x00000047 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_Raycast_m284670765E1627E43B7B0F5EC811A688EE595091 (void);
// 0x00000048 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_Raycast_mDA2EB8C7692308A7178222D31CBA4C7A1C7DC915 (void);
// 0x00000049 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m2EA572B4613E1BD7DBAA299511CFD2DBA179A163 (void);
// 0x0000004A System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C (void);
// 0x0000004B System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_m18E12C65F127D1AA50D196623F04F81CB138FD12 (void);
// 0x0000004C System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern void Physics_Raycast_mE84D30EEFE59DA28DA172342068F092A35B2BE4A (void);
// 0x0000004D System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_m77560CCAA49DC821E51FDDD1570B921D7704646F (void);
// 0x0000004E System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_Raycast_mFDC4B8E7979495E3C22D0E3CEA4BCAB271EEC25A (void);
// 0x0000004F System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern void Physics_Raycast_mD7A711D3A790AC505F7229A4FFFA2E389008176B (void);
// 0x00000050 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern void Physics_Raycast_m111AFC36A136A67BE4776670E356E99A82010BFE (void);
// 0x00000051 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Raycast_mCA3F2DD1DC08199AAD8466BB857318CD454AC774 (void);
// 0x00000052 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8 (void);
// 0x00000053 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern void Physics_Raycast_mA64F8C30681E3A6A8F2B7EDE592FE7BBC0D354F4 (void);
// 0x00000054 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern void Physics_Raycast_m80EC8EEDA0ABA8B01838BA9054834CD1A381916E (void);
// 0x00000055 System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_CapsuleCast_m3FFA2ACA1FEC8CF446F1E3FB15A65C6F32341A99 (void);
// 0x00000056 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_SphereCast_m3179B64AAFD6E888FF0A9CBB967CE342529C333D (void);
// 0x00000057 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_SphereCast_mF2EE3CD3E7169B32E5BCCEEC2EED4D2759E6CB36 (void);
// 0x00000058 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32)
extern void Physics_SphereCast_m8918B0C1F40CDC1DB9C19089684D7F50B706635E (void);
// 0x00000059 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_SphereCast_mF17F4D3A66B1BFA1EEDA20593C6F84FE99596AC1 (void);
// 0x0000005A System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single)
extern void Physics_SphereCast_m0B5F74AE8EFE7570453233161FA7CA7336E300D3 (void);
// 0x0000005B UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_m39EE7CF896F9D8BA58CC623F14E4DB0641480523 (void);
// 0x0000005C UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_m5103E7C60CC66BAFA6534D1736138A92EB1EF2B8 (void);
// 0x0000005D UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics_RaycastAll_m2C977C8B022672F42B5E18F40B529C0A74B7E471 (void);
// 0x0000005E UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_RaycastAll_m2BBC8D2731B38EE0B704A5C6A09D0F8BBA074A4D (void);
// 0x0000005F UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Physics_RaycastAll_m83F28CB671653C07995FB1BCDC561121DE3D9CA6 (void);
// 0x00000060 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastAll_m55EB4478198ED6EF838500257FA3BE1A871D3605 (void);
// 0x00000061 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics_RaycastAll_mA24B9B922C98C5D18397FAE55C04AEAFDD47F029 (void);
// 0x00000062 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern void Physics_RaycastAll_m72947571EFB0EFB34E48340AA2EC0C8030D27C50 (void);
// 0x00000063 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern void Physics_RaycastAll_m529EE59D6D03E4CFAECE4016C8CC4181BEC2D26D (void);
// 0x00000064 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m7C81125AD7D5891EBC3AB48C6DED7B60F53DF099 (void);
// 0x00000065 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_m8F62EE5A33E81A02E4983A171023B7BC4AC700CA (void);
// 0x00000066 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m41BB7BB8755B09700C10F59A29C3541D45AB9CCD (void);
// 0x00000067 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_mBC5BE514E55B8D98A8F4752DED6850E02EE8AD98 (void);
// 0x00000068 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_RaycastNonAlloc_m2C90D14E472DE7929EFD54FDA7BC512D3FBDE4ED (void);
// 0x00000069 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32)
extern void Physics_RaycastNonAlloc_m316F597067055C9F923F57CC68D68FF917C3B4D1 (void);
// 0x0000006A System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single)
extern void Physics_RaycastNonAlloc_m1B8F31BF41F756F561F6AC3A2E0736F2BC9C4DB4 (void);
// 0x0000006B System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[])
extern void Physics_RaycastNonAlloc_m9076DDE1A65F87DB3D2824DAD4E5B8B534159F1F (void);
// 0x0000006C UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapCapsule_Internal_m8D8A4C03D77370B97A7190ACFD78AE6D94E7B9C2 (void);
// 0x0000006D UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapCapsule_mDFB0E8D7E473403C0634287300D469467792332D (void);
// 0x0000006E UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Physics_OverlapCapsule_m66E08CDF8C35FF7E05F990E718CF1B791F43B1D2 (void);
// 0x0000006F UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_Internal_m92572431C76930C21D10CEFC0F0D37B945A6F4FF (void);
// 0x00000070 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_m1CDA8916BA89EE2B497158A08CD571044D60054B (void);
// 0x00000071 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
extern void Physics_OverlapSphere_mE4A0577DF7C0681DE7FFC4F2A2C1BFB8D402CA0C (void);
// 0x00000072 System.Boolean UnityEngine.Physics::Query_ComputePenetration(UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern void Physics_Query_ComputePenetration_m6B55B585ECB5C4EF7A699781719BC23A8638103D (void);
// 0x00000073 System.Boolean UnityEngine.Physics::ComputePenetration(UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern void Physics_ComputePenetration_m1F892D7CBF091AB513D0DA638E4718D8AC7769D4 (void);
// 0x00000074 UnityEngine.Collider[] UnityEngine.Physics::OverlapBox_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapBox_Internal_mE1F3A9A8FADD5F467589855FC1E975ECE233CCB1 (void);
// 0x00000075 UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapBox_mA61881B48584EF4E10B8AFB452D8B6170310B3D9 (void);
// 0x00000076 UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Physics_OverlapBox_m94CD6EB347697FF2E69E36D1EDA1A05510FE7DA4 (void);
// 0x00000077 System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&)
extern void Physics_get_defaultPhysicsScene_Injected_m0B71068269B3C8499A258BBCAEA7E3D9D020BADA (void);
// 0x00000078 UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_Internal_RaycastAll_Injected_mD4C3E8762D9FCEE654CE0415E636EB7DBEC92A5B (void);
// 0x00000079 UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapCapsule_Internal_Injected_m67F689F5122A45D4FF2F37C0A3CE19945A2496B4 (void);
// 0x0000007A UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapSphere_Internal_Injected_mBC7164D0CB4C0ED3299EE6515F162F324C16D291 (void);
// 0x0000007B System.Boolean UnityEngine.Physics::Query_ComputePenetration_Injected(UnityEngine.Collider,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Collider,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern void Physics_Query_ComputePenetration_Injected_mF8C23B35E141DFDE213242F410BC8B89159B22A9 (void);
// 0x0000007C UnityEngine.Collider[] UnityEngine.Physics::OverlapBox_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern void Physics_OverlapBox_Internal_Injected_mA489DC1405BBFF275A074ED62F604EE2CA2AFC12 (void);
static Il2CppMethodPointer s_methodPointers[124] = 
{
	RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E_AdjustorThunk,
	RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E_AdjustorThunk,
	RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674_AdjustorThunk,
	RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA_AdjustorThunk,
	RaycastHit_set_distance_m287DCD9DAE1D3AE76C7C4DE2F192E4DFBA911F41_AdjustorThunk,
	RaycastHit_get_transform_m2DD983DBD3602DE848DE287EE5233FD02EEC608D_AdjustorThunk,
	RaycastHit_get_rigidbody_mE48E45893FDAFD03162C6A73AAF02C6CB0E079FB_AdjustorThunk,
	Rigidbody_set_isKinematic_mCF74D680205544826F2DE2CAB929C9F25409A311,
	Rigidbody_set_constraints_mA76F562D16D3BE8889E095D0309C8FE38DA914F1,
	Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700,
	Rigidbody_AddTorque_mEDE3483056FB07222A4D096F22D45C7D8A6E2552,
	Rigidbody_AddTorque_mAEB5758FA773B1A0ECDD328934BB3A7202D21EB3,
	Rigidbody__ctor_m0E43BA3B0E70E71B2CA62B165EE5B7CFAEFACDE9,
	Rigidbody_AddForce_Injected_m233C3E22C3FE9D2BCBBC510132B82CE26057370C,
	Rigidbody_AddTorque_Injected_mFFD31FDF8F82D3D740EA83348BBC0D0D5EB0DB3A,
	Collider_get_enabled_m03B73B5C97033F939387D1785BDF2619CADAEEB0,
	Collider_get_attachedRigidbody_m101FED12AD292F372F98E94A6D02A5E428AA896A,
	Collider_ClosestPoint_m7777917E298B31796DEE906B54F0102F6ED76676,
	Collider__ctor_m09D7A9B985D74FD50346DA08D88EB1874E968B69,
	Collider_ClosestPoint_Injected_m6D72FF73D51838EE47234CB4D6234521C08B780D,
	MeshCollider_get_sharedMesh_m77D27894F87619BD2AF868BC1156583EEC0D8CB4,
	CapsuleCollider_get_center_m6374F7457A9450CAFFAD2DF0C9D1419BF9E304CB,
	CapsuleCollider_set_center_m36F35F070DFC2CBFC87532004073CA8D56F3678F,
	CapsuleCollider_get_radius_m5746DDE5E6A099269FC9DAD253887C58E8A064D0,
	CapsuleCollider_set_radius_mD4502A8676AAC093F1E9958FB7D5D765EA206432,
	CapsuleCollider_get_height_mD6CF93CB2C222F8E5B77D65B0356F8FD8005B526,
	CapsuleCollider_set_height_m728C9AF3772EEC1DA9845E19F3C2899CDD2D9496,
	CapsuleCollider_get_direction_m2C5110BBCA2CC1C63183CDBD73B6D11CC89B0918,
	CapsuleCollider_set_direction_mE5E7B7BB7FBBBA97148E8CFCDC46D1BB14673984,
	CapsuleCollider_GetGlobalExtents_m96E63F24BCAF34DDBC49E743C3203D19638501AF,
	CapsuleCollider_CalculateTransform_m5DA33B8D319E92F1FBE72447A8CDE5223DB69258,
	CapsuleCollider__ctor_mB993913A662CF20B09C4A7475B66E820C87879A4,
	CapsuleCollider_get_center_Injected_mBDF07E6DE932FBCAF0AA6F167EB993BE03BE6872,
	CapsuleCollider_set_center_Injected_mB43C17AC81236FB74E61E21215D2145663A7DEEE,
	CapsuleCollider_GetGlobalExtents_Injected_m3604AFE1AB8E8F50E203CF50E7A484346DC72C18,
	CapsuleCollider_CalculateTransform_Injected_mA1A6CB7A01489F8442B921163D0149EE6AE329EC,
	BoxCollider_get_center_m832B93439717C72D4A3B427C6E8F5B54E2DBD669,
	BoxCollider_get_size_mBC38D4926D4BE54A6532F6E1D642F363CA3A58A1,
	BoxCollider_get_center_Injected_m5F060E93AE651FB756B9371C7A716BDAA2A96F51,
	BoxCollider_get_size_Injected_mC9ABFED6FEC27351BC116B44021578CB39CBCB22,
	SphereCollider_get_center_mBFAE4FFFC76B8FD8F1B2B2F12C52A30470443D3A,
	SphereCollider_set_center_mD5E898A2FED304A82BC67ABB11B60BB0F612CED7,
	SphereCollider_get_radius_m403989140BDAD513299276953B481167CF08D02F,
	SphereCollider_set_radius_m55A0D144B32871AECC2A83FBCF423FBE1E5A63A0,
	SphereCollider_get_center_Injected_mF082CC62A7E8DE3DD5B86518C210D85139DD93CB,
	SphereCollider_set_center_Injected_mB213D6CB194E150BBEF8EC2AA31EA391C2F4A641,
	PhysicsScene_ToString_mBB6D0AC1E3E2EDC34CB4A7A34485B24B6271903F_AdjustorThunk,
	PhysicsScene_GetHashCode_m5344CC6CCCB1CA6EBB149775EE028089DD74B8A2_AdjustorThunk,
	PhysicsScene_Equals_m2645ABB96C598F197F734EB712494795928CBCEC_AdjustorThunk,
	PhysicsScene_Equals_m8C948B86D105177D519A3608816CDC698C8686B8_AdjustorThunk,
	PhysicsScene_Raycast_m8C3E61EB7C3DB8AAC6FEB098D815062BEF821187_AdjustorThunk,
	PhysicsScene_Internal_RaycastTest_m6715CAD8D0330195269AA7FCCD91613BC564E3EB,
	PhysicsScene_Raycast_mFC0D59C4439EE9DA6E8AA5F6891915132D587208_AdjustorThunk,
	PhysicsScene_Internal_Raycast_m1E630AA11805114B090FC50741AEF64D677AF2C7,
	PhysicsScene_Raycast_m95CAB68EB9165E3AB2A4D441F7DF9767AD4B7F73_AdjustorThunk,
	PhysicsScene_Internal_RaycastNonAlloc_mE7833EE5F1082431A4C2D29362F5DCDEFBF6D2BD,
	PhysicsScene_Query_CapsuleCast_mEF92DCD7186E4139FA84959DC45DD26E8DBFAC6C,
	PhysicsScene_Internal_CapsuleCast_mAB95448C77F1E823CA8028F693724C9D385B0406,
	PhysicsScene_CapsuleCast_mE4112B782763A75FE885111A8C45F0B74C2CB784_AdjustorThunk,
	PhysicsScene_Query_SphereCast_m47352512958CE213DD034AE53DB2E0C0247A38C3,
	PhysicsScene_Internal_SphereCast_mFEE43A5306A097D0D2CB389A200921320130E55B,
	PhysicsScene_SphereCast_m72928994362EE11F9A4F72BD79954444B9FD1C2D_AdjustorThunk,
	PhysicsScene_Internal_RaycastTest_Injected_m956474EFEA507F82ABE0BCB75DDB3CC8EFF4D12B,
	PhysicsScene_Internal_Raycast_Injected_m8868E0BF89F8C42D11F42F94F9CF767647433BB1,
	PhysicsScene_Internal_RaycastNonAlloc_Injected_m59FB44C18E62D21E9320F30229C0AA9F9B971211,
	PhysicsScene_Query_CapsuleCast_Injected_m20A80C56467001CAD7BAF8FCADE40D58EF90EF57,
	PhysicsScene_Query_SphereCast_Injected_m6DD8E497811A3098113F5E9F52D7B0CAE63DC1EB,
	Physics_get_defaultPhysicsScene_mA6361488FBAC110DA8397E5E4CB473EBF4191010,
	Physics_Raycast_m9DE0EEA1CF8DEF7D06216225F19E2958D341F7BA,
	Physics_Raycast_m09F21E13465121A73F19C07FC5F5314CF15ACD15,
	Physics_Raycast_m284670765E1627E43B7B0F5EC811A688EE595091,
	Physics_Raycast_mDA2EB8C7692308A7178222D31CBA4C7A1C7DC915,
	Physics_Raycast_m2EA572B4613E1BD7DBAA299511CFD2DBA179A163,
	Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C,
	Physics_Raycast_m18E12C65F127D1AA50D196623F04F81CB138FD12,
	Physics_Raycast_mE84D30EEFE59DA28DA172342068F092A35B2BE4A,
	Physics_Raycast_m77560CCAA49DC821E51FDDD1570B921D7704646F,
	Physics_Raycast_mFDC4B8E7979495E3C22D0E3CEA4BCAB271EEC25A,
	Physics_Raycast_mD7A711D3A790AC505F7229A4FFFA2E389008176B,
	Physics_Raycast_m111AFC36A136A67BE4776670E356E99A82010BFE,
	Physics_Raycast_mCA3F2DD1DC08199AAD8466BB857318CD454AC774,
	Physics_Raycast_m46E12070D6996F4C8C91D49ADC27C74AC1D6A3D8,
	Physics_Raycast_mA64F8C30681E3A6A8F2B7EDE592FE7BBC0D354F4,
	Physics_Raycast_m80EC8EEDA0ABA8B01838BA9054834CD1A381916E,
	Physics_CapsuleCast_m3FFA2ACA1FEC8CF446F1E3FB15A65C6F32341A99,
	Physics_SphereCast_m3179B64AAFD6E888FF0A9CBB967CE342529C333D,
	Physics_SphereCast_mF2EE3CD3E7169B32E5BCCEEC2EED4D2759E6CB36,
	Physics_SphereCast_m8918B0C1F40CDC1DB9C19089684D7F50B706635E,
	Physics_SphereCast_mF17F4D3A66B1BFA1EEDA20593C6F84FE99596AC1,
	Physics_SphereCast_m0B5F74AE8EFE7570453233161FA7CA7336E300D3,
	Physics_Internal_RaycastAll_m39EE7CF896F9D8BA58CC623F14E4DB0641480523,
	Physics_RaycastAll_m5103E7C60CC66BAFA6534D1736138A92EB1EF2B8,
	Physics_RaycastAll_m2C977C8B022672F42B5E18F40B529C0A74B7E471,
	Physics_RaycastAll_m2BBC8D2731B38EE0B704A5C6A09D0F8BBA074A4D,
	Physics_RaycastAll_m83F28CB671653C07995FB1BCDC561121DE3D9CA6,
	Physics_RaycastAll_m55EB4478198ED6EF838500257FA3BE1A871D3605,
	Physics_RaycastAll_mA24B9B922C98C5D18397FAE55C04AEAFDD47F029,
	Physics_RaycastAll_m72947571EFB0EFB34E48340AA2EC0C8030D27C50,
	Physics_RaycastAll_m529EE59D6D03E4CFAECE4016C8CC4181BEC2D26D,
	Physics_RaycastNonAlloc_m7C81125AD7D5891EBC3AB48C6DED7B60F53DF099,
	Physics_RaycastNonAlloc_m8F62EE5A33E81A02E4983A171023B7BC4AC700CA,
	Physics_RaycastNonAlloc_m41BB7BB8755B09700C10F59A29C3541D45AB9CCD,
	Physics_RaycastNonAlloc_mBC5BE514E55B8D98A8F4752DED6850E02EE8AD98,
	Physics_RaycastNonAlloc_m2C90D14E472DE7929EFD54FDA7BC512D3FBDE4ED,
	Physics_RaycastNonAlloc_m316F597067055C9F923F57CC68D68FF917C3B4D1,
	Physics_RaycastNonAlloc_m1B8F31BF41F756F561F6AC3A2E0736F2BC9C4DB4,
	Physics_RaycastNonAlloc_m9076DDE1A65F87DB3D2824DAD4E5B8B534159F1F,
	Physics_OverlapCapsule_Internal_m8D8A4C03D77370B97A7190ACFD78AE6D94E7B9C2,
	Physics_OverlapCapsule_mDFB0E8D7E473403C0634287300D469467792332D,
	Physics_OverlapCapsule_m66E08CDF8C35FF7E05F990E718CF1B791F43B1D2,
	Physics_OverlapSphere_Internal_m92572431C76930C21D10CEFC0F0D37B945A6F4FF,
	Physics_OverlapSphere_m1CDA8916BA89EE2B497158A08CD571044D60054B,
	Physics_OverlapSphere_mE4A0577DF7C0681DE7FFC4F2A2C1BFB8D402CA0C,
	Physics_Query_ComputePenetration_m6B55B585ECB5C4EF7A699781719BC23A8638103D,
	Physics_ComputePenetration_m1F892D7CBF091AB513D0DA638E4718D8AC7769D4,
	Physics_OverlapBox_Internal_mE1F3A9A8FADD5F467589855FC1E975ECE233CCB1,
	Physics_OverlapBox_mA61881B48584EF4E10B8AFB452D8B6170310B3D9,
	Physics_OverlapBox_m94CD6EB347697FF2E69E36D1EDA1A05510FE7DA4,
	Physics_get_defaultPhysicsScene_Injected_m0B71068269B3C8499A258BBCAEA7E3D9D020BADA,
	Physics_Internal_RaycastAll_Injected_mD4C3E8762D9FCEE654CE0415E636EB7DBEC92A5B,
	Physics_OverlapCapsule_Internal_Injected_m67F689F5122A45D4FF2F37C0A3CE19945A2496B4,
	Physics_OverlapSphere_Internal_Injected_mBC7164D0CB4C0ED3299EE6515F162F324C16D291,
	Physics_Query_ComputePenetration_Injected_mF8C23B35E141DFDE213242F410BC8B89159B22A9,
	Physics_OverlapBox_Internal_Injected_mA489DC1405BBFF275A074ED62F604EE2CA2AFC12,
};
static const int32_t s_InvokerIndices[124] = 
{
	14,
	1552,
	1552,
	737,
	340,
	14,
	14,
	31,
	32,
	1819,
	1819,
	1553,
	23,
	64,
	64,
	89,
	14,
	1538,
	23,
	579,
	14,
	1552,
	1553,
	737,
	340,
	737,
	340,
	10,
	32,
	1560,
	1536,
	23,
	6,
	6,
	6,
	6,
	1552,
	1552,
	6,
	6,
	1552,
	1553,
	737,
	340,
	6,
	6,
	14,
	10,
	9,
	2054,
	2055,
	2056,
	2057,
	2058,
	2059,
	2060,
	2061,
	2062,
	2063,
	2064,
	2065,
	2066,
	2067,
	2068,
	2069,
	2070,
	2071,
	2072,
	2073,
	2074,
	2075,
	1755,
	2076,
	2077,
	2078,
	2079,
	2080,
	2081,
	2082,
	2083,
	2084,
	2085,
	2086,
	2087,
	2088,
	2089,
	2090,
	2091,
	2092,
	2093,
	2094,
	2095,
	2096,
	2097,
	2098,
	2099,
	2046,
	2045,
	2044,
	2100,
	2050,
	2049,
	2048,
	2101,
	2102,
	2103,
	2104,
	2105,
	2095,
	2097,
	2106,
	2107,
	2108,
	2109,
	2109,
	2110,
	2111,
	2112,
	17,
	2113,
	2114,
	2113,
	2115,
	2116,
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_PhysicsModule;
extern const Il2CppCodeGenModule g_UnityEngine_PhysicsModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_PhysicsModuleCodeGenModule = 
{
	"UnityEngine.PhysicsModule.dll",
	124,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	&g_DebuggerMetadataRegistrationUnityEngine_PhysicsModule,
};
