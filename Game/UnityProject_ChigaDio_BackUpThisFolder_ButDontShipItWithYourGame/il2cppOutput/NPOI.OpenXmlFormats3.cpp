﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// NPOI.OpenXmlFormats.Spreadsheet.CT_AutoFilter
struct CT_AutoFilter_tD17FACB8B3FA6B2D42EAF7D283C59FB6E354E54A;
// NPOI.OpenXmlFormats.Spreadsheet.CT_BookViews
struct CT_BookViews_t3A648C8F40EE0569A11F73CB2248C165DD3D383C;
// NPOI.OpenXmlFormats.Spreadsheet.CT_CalcPr
struct CT_CalcPr_tE59C9AC5980E29877AC4145B82328585616B6512;
// NPOI.OpenXmlFormats.Spreadsheet.CT_CellSmartTags
struct CT_CellSmartTags_t848F74CABCB4CC4048AF8189644352B4999AD91E;
// NPOI.OpenXmlFormats.Spreadsheet.CT_CellWatches
struct CT_CellWatches_t01F7D7188788534CB4D6FA87507126AE3F3C467B;
// NPOI.OpenXmlFormats.Spreadsheet.CT_Controls
struct CT_Controls_t564AEB49B666E470DF52CDC90498C94EEE684B9A;
// NPOI.OpenXmlFormats.CT_CustomProperties
struct CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF;
// NPOI.OpenXmlFormats.Spreadsheet.CT_CustomProperties
struct CT_CustomProperties_t7BE31A9D707BD3B4EE745A9AF575A41AE26E49BF;
// NPOI.OpenXmlFormats.Spreadsheet.CT_CustomSheetViews
struct CT_CustomSheetViews_t6AF3C33141DEBB763F7E617B6DD7F40D7865E69C;
// NPOI.OpenXmlFormats.Spreadsheet.CT_CustomWorkbookViews
struct CT_CustomWorkbookViews_tD84E01AD55489ED6E4160B12832B5FE429C9EE9D;
// NPOI.OpenXmlFormats.Spreadsheet.CT_DataConsolidate
struct CT_DataConsolidate_t6BA590472F9874782F6D09BF8502377F8A85BB62;
// NPOI.OpenXmlFormats.Spreadsheet.CT_DataValidations
struct CT_DataValidations_t8D8CBC64BC7AE3A87A2D97CDA62009D74292C35A;
// NPOI.OpenXmlFormats.Spreadsheet.CT_DefinedNames
struct CT_DefinedNames_t626C75D86F91C16E37CA24049B7248459E8D944E;
// NPOI.OpenXmlFormats.CT_DigSigBlob
struct CT_DigSigBlob_tABF9531F503B14657DF6DBEF6AB09BEFAECF18AF;
// NPOI.OpenXmlFormats.Spreadsheet.CT_Drawing
struct CT_Drawing_tD2F6724924616C586D3810460A5E0962BCE3A0BB;
// NPOI.OpenXmlFormats.CT_ExtendedProperties
struct CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D;
// NPOI.OpenXmlFormats.Spreadsheet.CT_ExtensionList
struct CT_ExtensionList_t79ED67739D16E8748F4B890D07A37591BB568F66;
// NPOI.OpenXmlFormats.Spreadsheet.CT_ExternalReferences
struct CT_ExternalReferences_t7EC75ED08C5E6BAA88A66AB3B78834839BAB4E1B;
// NPOI.OpenXmlFormats.Spreadsheet.CT_FileSharing
struct CT_FileSharing_t3C62AA72F30681F926B179B922162E4FA5264534;
// NPOI.OpenXmlFormats.Spreadsheet.CT_FileVersion
struct CT_FileVersion_t57AC2B828FFBB31C09019731C69A7F04BC998C60;
// NPOI.OpenXmlFormats.Spreadsheet.CT_FunctionGroups
struct CT_FunctionGroups_t87EBBDC862F6D4F7ACC107C56E2B94B055D5455D;
// NPOI.OpenXmlFormats.Spreadsheet.CT_HeaderFooter
struct CT_HeaderFooter_tE734591E584CBD00BD317DE44533E600FAAE9D2B;
// NPOI.OpenXmlFormats.Spreadsheet.CT_Hyperlinks
struct CT_Hyperlinks_t967310005D63AA3C3E942B1AECD8EAC275E7359F;
// NPOI.OpenXmlFormats.Spreadsheet.CT_IgnoredErrors
struct CT_IgnoredErrors_t5D0C0D432AA19CC3B66CB5F5BEA9F5009ABE599E;
// NPOI.OpenXmlFormats.Spreadsheet.CT_LegacyDrawing
struct CT_LegacyDrawing_t37EC868D8930A4F935DB6358433328195ACFAC70;
// NPOI.OpenXmlFormats.Spreadsheet.CT_MergeCells
struct CT_MergeCells_tC244C81D9B6D7B8B262A3244CEC1E33DBC05465C;
// NPOI.OpenXmlFormats.Spreadsheet.CT_OleObjects
struct CT_OleObjects_tD28B4F45856085620F2065D8441AAF8E4956415D;
// NPOI.OpenXmlFormats.Spreadsheet.CT_OleSize
struct CT_OleSize_t1E498D69E55B6A5C0EE32E37488BC7E525FF817A;
// NPOI.OpenXmlFormats.Spreadsheet.CT_PageBreak
struct CT_PageBreak_tBA4435226581E50E78304255BED5D4775F85BBB8;
// NPOI.OpenXmlFormats.Spreadsheet.CT_PageMargins
struct CT_PageMargins_t48C935B82A4C55550510A8CF006E0817C13982E9;
// NPOI.OpenXmlFormats.Spreadsheet.CT_PageSetup
struct CT_PageSetup_t136C6F167D4E7A2E2480A50CAD462FBE1D1715A5;
// NPOI.OpenXmlFormats.Spreadsheet.CT_PhoneticPr
struct CT_PhoneticPr_t9B7890742E83E837E73C48AF52676C18F50240E8;
// NPOI.OpenXmlFormats.Spreadsheet.CT_PivotCaches
struct CT_PivotCaches_t9EBD199955E5E828CFB50EA206BD076D823D9920;
// NPOI.OpenXmlFormats.Spreadsheet.CT_PrintOptions
struct CT_PrintOptions_tBF0437CD33D2F94344998C5F43108143353F1148;
// NPOI.OpenXmlFormats.Spreadsheet.CT_ProtectedRanges
struct CT_ProtectedRanges_tF0FA33DE18F463BB6E99986F30F9A20695B9A01C;
// NPOI.OpenXmlFormats.Spreadsheet.CT_Scenarios
struct CT_Scenarios_tAC7DDBB1B4BC086B57A911B030C5D784A0D7F643;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetBackgroundPicture
struct CT_SheetBackgroundPicture_tA51AFAD22D4F4C6CA5A208A29AF4586B25C56FD3;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetCalcPr
struct CT_SheetCalcPr_t222A8620FBDEEA62B5B596C64700D3F0C6CC9EDB;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetData
struct CT_SheetData_t538FCD2B39075937476FBB9823513F06345D5E1D;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetDimension
struct CT_SheetDimension_t431685C4E43A0CD54A4F48D2A588E0B2958E44E6;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetFormatPr
struct CT_SheetFormatPr_t2329725F61309FF2A6490FB54F40E223914A05F7;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetPr
struct CT_SheetPr_tF94369547EE810CB1FAF7CC5564291692AF7B636;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetProtection
struct CT_SheetProtection_t6B812F2AEC7469E24F2FAC813DB2BB5DB76DEEAF;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetViews
struct CT_SheetViews_tD7012A0FDF0ED74C8251C71A86ACB4A86C9C4E03;
// NPOI.OpenXmlFormats.Spreadsheet.CT_Sheets
struct CT_Sheets_t1E3738CE072A59495E71685E1CBC5559B4F61CF7;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SmartTagPr
struct CT_SmartTagPr_tD04E077C0776903B30B9FC960D060B8758C3B579;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SmartTagTypes
struct CT_SmartTagTypes_tBADE31A08C8B4974A2EF07FB04CF951CC95E109F;
// NPOI.OpenXmlFormats.Spreadsheet.CT_SortState
struct CT_SortState_tB6AFFB8A5F46F8E04AF0E3F26B09AA548B9D447B;
// NPOI.OpenXmlFormats.Spreadsheet.CT_Sst
struct CT_Sst_tB80B6A0914663BCEE99DB793DA7F73354949D6A9;
// NPOI.OpenXmlFormats.Spreadsheet.CT_TableParts
struct CT_TableParts_t78D1D9FFB376C6DECADD9D4FE55C2F180B8F7DA1;
// NPOI.OpenXmlFormats.CT_VectorLpstr
struct CT_VectorLpstr_tCCFA913567FDE259EEEEFBD13599DF1E94AE600F;
// NPOI.OpenXmlFormats.CT_VectorVariant
struct CT_VectorVariant_t896EB2F643B186E8667F296CEC786D96E3511B20;
// NPOI.OpenXmlFormats.Spreadsheet.CT_WebPublishItems
struct CT_WebPublishItems_tE2398221E4BEA0EFC8BF2AAB546AD668BC9C3267;
// NPOI.OpenXmlFormats.Spreadsheet.CT_WebPublishObjects
struct CT_WebPublishObjects_t86BA08D531D57ADD5CEA7360566E2E7FAA3143C5;
// NPOI.OpenXmlFormats.Spreadsheet.CT_WebPublishing
struct CT_WebPublishing_t23C3EF722A362B0DEB34022832E41D43CF5978E6;
// NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook
struct CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0;
// NPOI.OpenXmlFormats.Spreadsheet.CT_WorkbookPr
struct CT_WorkbookPr_t797A81C397F684944238BC1706A781E51320FFC2;
// NPOI.OpenXmlFormats.Spreadsheet.CT_WorkbookProtection
struct CT_WorkbookProtection_t8B126050A994F3A5F553F3EA5FB9C136AD3E004F;
// NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet
struct CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF;
// NPOI.OpenXmlFormats.Spreadsheet.CT_Xf
struct CT_Xf_t1790BDF05B20998683A5649F037942CB892C80AB;
// NPOI.OpenXmlFormats.CustomPropertiesDocument
struct CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169;
// System.Xml.DomNameTable
struct DomNameTable_tE88EEF20D313A83C002E18FFF7688CC74A2D1CAB;
// System.Xml.EmptyEnumerator
struct EmptyEnumerator_t138901A02D453E19CDE87DFD96981F8A98928E13;
// System.Text.Encoder
struct Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A;
// NPOI.OpenXmlFormats.ExtendedPropertiesDocument
struct ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07;
// System.IFormatProvider
struct IFormatProvider_tF2AECC4B14F41D36718920D67F930CED940412DF;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_tC697CEDEA24AC6EF87E998BF72B96EDBD84DB229;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_tDED785F47546ED3F5EEC6EA77577917420BF3BF5;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385;
// NPOI.OpenXmlFormats.Spreadsheet.SstDocument
struct SstDocument_t29C4C193FFCF0E33A36E3211726074BEDE7D8F81;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.Tasks.Task
struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60;
// System.Text.UnicodeEncoding
struct UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68;
// System.Xml.Serialization.UnreferencedObjectEventHandler
struct UnreferencedObjectEventHandler_t8C7AD11596BB1575262B62EE1128DABB07434988;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// NPOI.OpenXmlFormats.Spreadsheet.WorkbookDocument
struct WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3;
// NPOI.OpenXmlFormats.Spreadsheet.WorksheetDocument
struct WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F;
// System.Xml.XmlAttribute
struct XmlAttribute_t3F58A4BDFB486D0E610E4003E54A89BCCB65AB6D;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_tDC800F98FE32D4723967772453EB600D1C368AA3;
// System.Xml.Serialization.XmlAttributeEventHandler
struct XmlAttributeEventHandler_tECA22B67D5D89D1A3EF50E02048CE6E7EC40C286;
// System.Xml.Serialization.XmlElementEventHandler
struct XmlElementEventHandler_t5110F67CA4FDC0867D3457AAC06E2E6DADA1DE29;
// System.Xml.XmlImplementation
struct XmlImplementation_tE66EA5FEAE495F36B03F697C51E4815D4B5D1D59;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0;
// System.Xml.Serialization.XmlMapping
struct XmlMapping_t98D53F12A90FEA32EB59D51D4461C16AE85978F9;
// System.Xml.XmlName
struct XmlName_t641FE51F9AE9A7BF9B333D2FB61E2867328F5A63;
// System.Xml.XmlNameTable
struct XmlNameTable_t5A8AA505CA799E0DC25E9815E4106817D2E1E280;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t7554E5F015FED975E1E8B075F57EBA65773CF771;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9;
// System.Xml.Serialization.XmlNodeEventHandler
struct XmlNodeEventHandler_tF1C40439AD043BB08EEEAA54CC23FD03DF241563;
// System.Xml.XmlResolver
struct XmlResolver_t7666FB44FF30D5CE53CC8DD913B2A8D2BD74343A;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t7CE09F2ECFA8C5EB28DF4724C87EC50B539699E0;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974;
// System.Xml.XmlQualifiedName/HashCodeOfStringDelegate
struct HashCodeOfStringDelegate_tAE9DAB0A55A64F35CCEE05D71856BAAF6C0B668E;
// System.Xml.Serialization.XmlSerializer/SerializerData
struct SerializerData_tE81861A5E031D40D9732A8CF036A091D10F3478D;
// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162;
// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.Spreadsheet.CT_Cols>
struct List_1_t835AE5436C3D76B93E8EDF3561BE98BE17BC578D;
// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.Spreadsheet.CT_ConditionalFormatting>
struct List_1_t48E5086280838656B52038009060E22420EC1756;
// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.Spreadsheet.CT_FileRecoveryPr>
struct List_1_t657B3A9797162495F76252E7E48DC1E8FA1054CF;
// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.CT_Property>
struct List_1_t302185706367D15E89CC829FB163861ABE02AAF1;
// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.Spreadsheet.CT_Rst>
struct List_1_t4C6AD5154887CA031844733D9DD5AC7A568F10D1;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827;
// System.Collections.Hashtable
struct Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC;
// System.Xml.XmlNamespaceManager/NamespaceDeclaration[]
struct NamespaceDeclarationU5BU5D_t4D11B362C1D22E79294A2F9CB4710B02ED6D03F5;
// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB;
// System.IO.StreamWriter
struct StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6;
// System.String
struct String_t;
// System.IO.StringWriter
struct StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839;
// System.IO.TextWriter
struct TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Xml.XmlDocument
struct XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F;
// System.Xml.XmlElement
struct XmlElement_tF11C508FEEF5FBE169DCE4A7538BE55B1F0C4BCF;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465;
// System.Xml.XmlNode
struct XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97;
// System.Xml.Serialization.XmlSerializer
struct XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5;

IL2CPP_EXTERN_C RuntimeClass* CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral4786DFC3C080A29EBD4B2900E245E855CEF9E8CD;
IL2CPP_EXTERN_C String_t* _stringLiteral4B752498CA45AB14E5BD67D359EAE6F257213868;
IL2CPP_EXTERN_C String_t* _stringLiteralACE1E943256EA7DB61DDD34139D1D5AFD4198882;
IL2CPP_EXTERN_C String_t* _stringLiteralC8FEA3F224EC0FC2366083D184EA5ECB892AD023;
IL2CPP_EXTERN_C String_t* _stringLiteralDA1EAAB8C040E1D2164AFCC76733E93F84C8450B;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* CT_Xf__ctor_m15FCF8C05265CC6DCC101CE1FD7A9EE42A015AC7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CT_Xf_get_numFmtId_mE0511C1C5E328F6F8F8D361ED021DB9AA855CABA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CT_Xf_get_xfId_mE8A307F65E25D6EC9FAF00C9517A3EB5BAA70728_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CustomPropertiesDocument_AddNewProperties_mED723B1E354F720A394195E0FBF3368C92BB1095_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CustomPropertiesDocument_Copy_mDB09FADB49AE554CEE43FB77BD81B65D9A85C0F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CustomPropertiesDocument_GetProperties_mE2B356A6D190DD705E6E20BCC08E030991801F71_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CustomPropertiesDocument_Parse_mF328E4120FA705EA25D8695806A0017E7F3F1B17_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CustomPropertiesDocument_Save_m42CC47BDD91E9FFCC91CBE4CDC5A886DC11D6072_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CustomPropertiesDocument_ToString_m1FF181DCD427665E7075FB4A46967A579A572DAD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CustomPropertiesDocument__cctor_m376128E78D1B613FCB9BB6AEDE4D60872324D4F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CustomPropertiesDocument__ctor_mB5BEB3DF621B26DFB1E9ACEC2343742E958E95DD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CustomPropertiesDocument__ctor_mD878434C70188BB9CF84239F20DD5A62B9E3E346_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ExtendedPropertiesDocument_AddNewProperties_mB6C794C704FEA9589B9A63CBC0A520A7C40E0F3F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ExtendedPropertiesDocument_Copy_mC881B00C57964162E969FC61AEEC746569BF3DF3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ExtendedPropertiesDocument_Parse_mABF1B98F82F6065964207D3CA2E65D7100D8D1E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ExtendedPropertiesDocument_Save_m3EE1CEC29ABD41B94D30CF4AE7D44F2E1A291845_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ExtendedPropertiesDocument_ToString_mDB4A3FB4F96CBF4887B93638B9077A04AABA873B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ExtendedPropertiesDocument__cctor_mF76B98CF4CCC316997BBB7E5323B656689C81BAB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ExtendedPropertiesDocument__ctor_m02C70057BF414D1E94B23BA662E1011750741F92_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ExtendedPropertiesDocument__ctor_m5D41034398ED048377CE914B6C2021AAB4D59776_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ST_RelationshipId_get_NamespaceURI_mF28D1E6311B6146FF405D329E287EFA7150FADBB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SstDocument_GetSst_m3792D1A5D7B33DCA4D7EFAD7EC30A2923ABC53EB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WorkbookDocument_Parse_m3BF0168B2F5B493DADC485694231E04119FDA3F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WorkbookDocument_Save_m714D79ACEDB5BF223849F94DFAEFED69314B99F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WorkbookDocument__ctor_m2768119D7911DDBCC60E3DF0CE82B045C0F32F5B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WorkbookDocument_get_Workbook_m1BF2B67FA8CB24435C74A58F8CF6F2FFD36B2B74_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WorksheetDocument_GetWorksheet_m821F090160952368CDD7D0869318E56111F44958_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WorksheetDocument_Parse_m12B4F17FB5C4BEECB4C56D253FDECEB3DF0A7C49_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WorksheetDocument_Save_m00947922034D769962E1AE6091C91AC62021110D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WorksheetDocument__ctor_m13BF2C9B76DC8213925339202E6543766F0245DA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D_0_0_0_var;
IL2CPP_EXTERN_C const uint32_t CT_Xf__ctor_m15FCF8C05265CC6DCC101CE1FD7A9EE42A015AC7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CT_Xf_get_numFmtId_mE0511C1C5E328F6F8F8D361ED021DB9AA855CABA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CT_Xf_get_xfId_mE8A307F65E25D6EC9FAF00C9517A3EB5BAA70728_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CustomPropertiesDocument_AddNewProperties_mED723B1E354F720A394195E0FBF3368C92BB1095_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CustomPropertiesDocument_Copy_mDB09FADB49AE554CEE43FB77BD81B65D9A85C0F5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CustomPropertiesDocument_GetProperties_mE2B356A6D190DD705E6E20BCC08E030991801F71_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CustomPropertiesDocument_Parse_mF328E4120FA705EA25D8695806A0017E7F3F1B17_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CustomPropertiesDocument_Save_m42CC47BDD91E9FFCC91CBE4CDC5A886DC11D6072_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CustomPropertiesDocument_ToString_m1FF181DCD427665E7075FB4A46967A579A572DAD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CustomPropertiesDocument__cctor_m376128E78D1B613FCB9BB6AEDE4D60872324D4F2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CustomPropertiesDocument__ctor_mB5BEB3DF621B26DFB1E9ACEC2343742E958E95DD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CustomPropertiesDocument__ctor_mD878434C70188BB9CF84239F20DD5A62B9E3E346_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExtendedPropertiesDocument_AddNewProperties_mB6C794C704FEA9589B9A63CBC0A520A7C40E0F3F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExtendedPropertiesDocument_Copy_mC881B00C57964162E969FC61AEEC746569BF3DF3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExtendedPropertiesDocument_Parse_mABF1B98F82F6065964207D3CA2E65D7100D8D1E9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExtendedPropertiesDocument_Save_m3EE1CEC29ABD41B94D30CF4AE7D44F2E1A291845_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExtendedPropertiesDocument_ToString_mDB4A3FB4F96CBF4887B93638B9077A04AABA873B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExtendedPropertiesDocument__cctor_mF76B98CF4CCC316997BBB7E5323B656689C81BAB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExtendedPropertiesDocument__ctor_m02C70057BF414D1E94B23BA662E1011750741F92_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ExtendedPropertiesDocument__ctor_m5D41034398ED048377CE914B6C2021AAB4D59776_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ST_RelationshipId_get_NamespaceURI_mF28D1E6311B6146FF405D329E287EFA7150FADBB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SstDocument_GetSst_m3792D1A5D7B33DCA4D7EFAD7EC30A2923ABC53EB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorkbookDocument_Parse_m3BF0168B2F5B493DADC485694231E04119FDA3F6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorkbookDocument_Save_m714D79ACEDB5BF223849F94DFAEFED69314B99F7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorkbookDocument__ctor_m2768119D7911DDBCC60E3DF0CE82B045C0F32F5B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorkbookDocument_get_Workbook_m1BF2B67FA8CB24435C74A58F8CF6F2FFD36B2B74_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorksheetDocument_GetWorksheet_m821F090160952368CDD7D0869318E56111F44958_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorksheetDocument_Parse_m12B4F17FB5C4BEECB4C56D253FDECEB3DF0A7C49_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorksheetDocument_Save_m00947922034D769962E1AE6091C91AC62021110D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WorksheetDocument__ctor_m13BF2C9B76DC8213925339202E6543766F0245DA_MetadataUsageId;

struct XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// NPOI.OpenXmlFormats.CT_CustomProperties
struct  CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.CT_Property> NPOI.OpenXmlFormats.CT_CustomProperties::propertyField
	List_1_t302185706367D15E89CC829FB163861ABE02AAF1 * ___propertyField_0;

public:
	inline static int32_t get_offset_of_propertyField_0() { return static_cast<int32_t>(offsetof(CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF, ___propertyField_0)); }
	inline List_1_t302185706367D15E89CC829FB163861ABE02AAF1 * get_propertyField_0() const { return ___propertyField_0; }
	inline List_1_t302185706367D15E89CC829FB163861ABE02AAF1 ** get_address_of_propertyField_0() { return &___propertyField_0; }
	inline void set_propertyField_0(List_1_t302185706367D15E89CC829FB163861ABE02AAF1 * value)
	{
		___propertyField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___propertyField_0), (void*)value);
	}
};


// NPOI.OpenXmlFormats.CT_ExtendedProperties
struct  CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D  : public RuntimeObject
{
public:
	// System.String NPOI.OpenXmlFormats.CT_ExtendedProperties::templateField
	String_t* ___templateField_0;
	// System.String NPOI.OpenXmlFormats.CT_ExtendedProperties::managerField
	String_t* ___managerField_1;
	// System.String NPOI.OpenXmlFormats.CT_ExtendedProperties::companyField
	String_t* ___companyField_2;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::pagesField
	int32_t ___pagesField_3;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::pagesFieldSpecified
	bool ___pagesFieldSpecified_4;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::wordsField
	int32_t ___wordsField_5;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::charactersField
	int32_t ___charactersField_6;
	// System.String NPOI.OpenXmlFormats.CT_ExtendedProperties::presentationFormatField
	String_t* ___presentationFormatField_7;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::linesField
	int32_t ___linesField_8;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::paragraphsField
	int32_t ___paragraphsField_9;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::paragraphsFieldSpecified
	bool ___paragraphsFieldSpecified_10;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::slidesField
	int32_t ___slidesField_11;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::notesField
	int32_t ___notesField_12;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::notesFieldSpecified
	bool ___notesFieldSpecified_13;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::totalTimeField
	int32_t ___totalTimeField_14;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::totalTimeFieldSpecified
	bool ___totalTimeFieldSpecified_15;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::hiddenSlidesField
	int32_t ___hiddenSlidesField_16;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::mMClipsField
	int32_t ___mMClipsField_17;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::mMClipsFieldSpecified
	bool ___mMClipsFieldSpecified_18;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::scaleCropField
	bool ___scaleCropField_19;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::scaleCropFieldSpecified
	bool ___scaleCropFieldSpecified_20;
	// NPOI.OpenXmlFormats.CT_VectorVariant NPOI.OpenXmlFormats.CT_ExtendedProperties::headingPairsField
	CT_VectorVariant_t896EB2F643B186E8667F296CEC786D96E3511B20 * ___headingPairsField_21;
	// NPOI.OpenXmlFormats.CT_VectorLpstr NPOI.OpenXmlFormats.CT_ExtendedProperties::titlesOfPartsField
	CT_VectorLpstr_tCCFA913567FDE259EEEEFBD13599DF1E94AE600F * ___titlesOfPartsField_22;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::linksUpToDateField
	bool ___linksUpToDateField_23;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::linksUpToDateFieldSpecified
	bool ___linksUpToDateFieldSpecified_24;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::charactersWithSpacesField
	int32_t ___charactersWithSpacesField_25;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::sharedDocField
	bool ___sharedDocField_26;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::sharedDocFieldSpecified
	bool ___sharedDocFieldSpecified_27;
	// System.String NPOI.OpenXmlFormats.CT_ExtendedProperties::hyperlinkBaseField
	String_t* ___hyperlinkBaseField_28;
	// NPOI.OpenXmlFormats.CT_VectorVariant NPOI.OpenXmlFormats.CT_ExtendedProperties::hLinksField
	CT_VectorVariant_t896EB2F643B186E8667F296CEC786D96E3511B20 * ___hLinksField_29;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::hyperlinksChangedField
	bool ___hyperlinksChangedField_30;
	// System.Boolean NPOI.OpenXmlFormats.CT_ExtendedProperties::hyperlinksChangedFieldSpecified
	bool ___hyperlinksChangedFieldSpecified_31;
	// NPOI.OpenXmlFormats.CT_DigSigBlob NPOI.OpenXmlFormats.CT_ExtendedProperties::digSigField
	CT_DigSigBlob_tABF9531F503B14657DF6DBEF6AB09BEFAECF18AF * ___digSigField_32;
	// System.String NPOI.OpenXmlFormats.CT_ExtendedProperties::applicationField
	String_t* ___applicationField_33;
	// System.String NPOI.OpenXmlFormats.CT_ExtendedProperties::appVersionField
	String_t* ___appVersionField_34;
	// System.Int32 NPOI.OpenXmlFormats.CT_ExtendedProperties::docSecurityField
	int32_t ___docSecurityField_35;

public:
	inline static int32_t get_offset_of_templateField_0() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___templateField_0)); }
	inline String_t* get_templateField_0() const { return ___templateField_0; }
	inline String_t** get_address_of_templateField_0() { return &___templateField_0; }
	inline void set_templateField_0(String_t* value)
	{
		___templateField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___templateField_0), (void*)value);
	}

	inline static int32_t get_offset_of_managerField_1() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___managerField_1)); }
	inline String_t* get_managerField_1() const { return ___managerField_1; }
	inline String_t** get_address_of_managerField_1() { return &___managerField_1; }
	inline void set_managerField_1(String_t* value)
	{
		___managerField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___managerField_1), (void*)value);
	}

	inline static int32_t get_offset_of_companyField_2() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___companyField_2)); }
	inline String_t* get_companyField_2() const { return ___companyField_2; }
	inline String_t** get_address_of_companyField_2() { return &___companyField_2; }
	inline void set_companyField_2(String_t* value)
	{
		___companyField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___companyField_2), (void*)value);
	}

	inline static int32_t get_offset_of_pagesField_3() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___pagesField_3)); }
	inline int32_t get_pagesField_3() const { return ___pagesField_3; }
	inline int32_t* get_address_of_pagesField_3() { return &___pagesField_3; }
	inline void set_pagesField_3(int32_t value)
	{
		___pagesField_3 = value;
	}

	inline static int32_t get_offset_of_pagesFieldSpecified_4() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___pagesFieldSpecified_4)); }
	inline bool get_pagesFieldSpecified_4() const { return ___pagesFieldSpecified_4; }
	inline bool* get_address_of_pagesFieldSpecified_4() { return &___pagesFieldSpecified_4; }
	inline void set_pagesFieldSpecified_4(bool value)
	{
		___pagesFieldSpecified_4 = value;
	}

	inline static int32_t get_offset_of_wordsField_5() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___wordsField_5)); }
	inline int32_t get_wordsField_5() const { return ___wordsField_5; }
	inline int32_t* get_address_of_wordsField_5() { return &___wordsField_5; }
	inline void set_wordsField_5(int32_t value)
	{
		___wordsField_5 = value;
	}

	inline static int32_t get_offset_of_charactersField_6() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___charactersField_6)); }
	inline int32_t get_charactersField_6() const { return ___charactersField_6; }
	inline int32_t* get_address_of_charactersField_6() { return &___charactersField_6; }
	inline void set_charactersField_6(int32_t value)
	{
		___charactersField_6 = value;
	}

	inline static int32_t get_offset_of_presentationFormatField_7() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___presentationFormatField_7)); }
	inline String_t* get_presentationFormatField_7() const { return ___presentationFormatField_7; }
	inline String_t** get_address_of_presentationFormatField_7() { return &___presentationFormatField_7; }
	inline void set_presentationFormatField_7(String_t* value)
	{
		___presentationFormatField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___presentationFormatField_7), (void*)value);
	}

	inline static int32_t get_offset_of_linesField_8() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___linesField_8)); }
	inline int32_t get_linesField_8() const { return ___linesField_8; }
	inline int32_t* get_address_of_linesField_8() { return &___linesField_8; }
	inline void set_linesField_8(int32_t value)
	{
		___linesField_8 = value;
	}

	inline static int32_t get_offset_of_paragraphsField_9() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___paragraphsField_9)); }
	inline int32_t get_paragraphsField_9() const { return ___paragraphsField_9; }
	inline int32_t* get_address_of_paragraphsField_9() { return &___paragraphsField_9; }
	inline void set_paragraphsField_9(int32_t value)
	{
		___paragraphsField_9 = value;
	}

	inline static int32_t get_offset_of_paragraphsFieldSpecified_10() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___paragraphsFieldSpecified_10)); }
	inline bool get_paragraphsFieldSpecified_10() const { return ___paragraphsFieldSpecified_10; }
	inline bool* get_address_of_paragraphsFieldSpecified_10() { return &___paragraphsFieldSpecified_10; }
	inline void set_paragraphsFieldSpecified_10(bool value)
	{
		___paragraphsFieldSpecified_10 = value;
	}

	inline static int32_t get_offset_of_slidesField_11() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___slidesField_11)); }
	inline int32_t get_slidesField_11() const { return ___slidesField_11; }
	inline int32_t* get_address_of_slidesField_11() { return &___slidesField_11; }
	inline void set_slidesField_11(int32_t value)
	{
		___slidesField_11 = value;
	}

	inline static int32_t get_offset_of_notesField_12() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___notesField_12)); }
	inline int32_t get_notesField_12() const { return ___notesField_12; }
	inline int32_t* get_address_of_notesField_12() { return &___notesField_12; }
	inline void set_notesField_12(int32_t value)
	{
		___notesField_12 = value;
	}

	inline static int32_t get_offset_of_notesFieldSpecified_13() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___notesFieldSpecified_13)); }
	inline bool get_notesFieldSpecified_13() const { return ___notesFieldSpecified_13; }
	inline bool* get_address_of_notesFieldSpecified_13() { return &___notesFieldSpecified_13; }
	inline void set_notesFieldSpecified_13(bool value)
	{
		___notesFieldSpecified_13 = value;
	}

	inline static int32_t get_offset_of_totalTimeField_14() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___totalTimeField_14)); }
	inline int32_t get_totalTimeField_14() const { return ___totalTimeField_14; }
	inline int32_t* get_address_of_totalTimeField_14() { return &___totalTimeField_14; }
	inline void set_totalTimeField_14(int32_t value)
	{
		___totalTimeField_14 = value;
	}

	inline static int32_t get_offset_of_totalTimeFieldSpecified_15() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___totalTimeFieldSpecified_15)); }
	inline bool get_totalTimeFieldSpecified_15() const { return ___totalTimeFieldSpecified_15; }
	inline bool* get_address_of_totalTimeFieldSpecified_15() { return &___totalTimeFieldSpecified_15; }
	inline void set_totalTimeFieldSpecified_15(bool value)
	{
		___totalTimeFieldSpecified_15 = value;
	}

	inline static int32_t get_offset_of_hiddenSlidesField_16() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___hiddenSlidesField_16)); }
	inline int32_t get_hiddenSlidesField_16() const { return ___hiddenSlidesField_16; }
	inline int32_t* get_address_of_hiddenSlidesField_16() { return &___hiddenSlidesField_16; }
	inline void set_hiddenSlidesField_16(int32_t value)
	{
		___hiddenSlidesField_16 = value;
	}

	inline static int32_t get_offset_of_mMClipsField_17() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___mMClipsField_17)); }
	inline int32_t get_mMClipsField_17() const { return ___mMClipsField_17; }
	inline int32_t* get_address_of_mMClipsField_17() { return &___mMClipsField_17; }
	inline void set_mMClipsField_17(int32_t value)
	{
		___mMClipsField_17 = value;
	}

	inline static int32_t get_offset_of_mMClipsFieldSpecified_18() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___mMClipsFieldSpecified_18)); }
	inline bool get_mMClipsFieldSpecified_18() const { return ___mMClipsFieldSpecified_18; }
	inline bool* get_address_of_mMClipsFieldSpecified_18() { return &___mMClipsFieldSpecified_18; }
	inline void set_mMClipsFieldSpecified_18(bool value)
	{
		___mMClipsFieldSpecified_18 = value;
	}

	inline static int32_t get_offset_of_scaleCropField_19() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___scaleCropField_19)); }
	inline bool get_scaleCropField_19() const { return ___scaleCropField_19; }
	inline bool* get_address_of_scaleCropField_19() { return &___scaleCropField_19; }
	inline void set_scaleCropField_19(bool value)
	{
		___scaleCropField_19 = value;
	}

	inline static int32_t get_offset_of_scaleCropFieldSpecified_20() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___scaleCropFieldSpecified_20)); }
	inline bool get_scaleCropFieldSpecified_20() const { return ___scaleCropFieldSpecified_20; }
	inline bool* get_address_of_scaleCropFieldSpecified_20() { return &___scaleCropFieldSpecified_20; }
	inline void set_scaleCropFieldSpecified_20(bool value)
	{
		___scaleCropFieldSpecified_20 = value;
	}

	inline static int32_t get_offset_of_headingPairsField_21() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___headingPairsField_21)); }
	inline CT_VectorVariant_t896EB2F643B186E8667F296CEC786D96E3511B20 * get_headingPairsField_21() const { return ___headingPairsField_21; }
	inline CT_VectorVariant_t896EB2F643B186E8667F296CEC786D96E3511B20 ** get_address_of_headingPairsField_21() { return &___headingPairsField_21; }
	inline void set_headingPairsField_21(CT_VectorVariant_t896EB2F643B186E8667F296CEC786D96E3511B20 * value)
	{
		___headingPairsField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___headingPairsField_21), (void*)value);
	}

	inline static int32_t get_offset_of_titlesOfPartsField_22() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___titlesOfPartsField_22)); }
	inline CT_VectorLpstr_tCCFA913567FDE259EEEEFBD13599DF1E94AE600F * get_titlesOfPartsField_22() const { return ___titlesOfPartsField_22; }
	inline CT_VectorLpstr_tCCFA913567FDE259EEEEFBD13599DF1E94AE600F ** get_address_of_titlesOfPartsField_22() { return &___titlesOfPartsField_22; }
	inline void set_titlesOfPartsField_22(CT_VectorLpstr_tCCFA913567FDE259EEEEFBD13599DF1E94AE600F * value)
	{
		___titlesOfPartsField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___titlesOfPartsField_22), (void*)value);
	}

	inline static int32_t get_offset_of_linksUpToDateField_23() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___linksUpToDateField_23)); }
	inline bool get_linksUpToDateField_23() const { return ___linksUpToDateField_23; }
	inline bool* get_address_of_linksUpToDateField_23() { return &___linksUpToDateField_23; }
	inline void set_linksUpToDateField_23(bool value)
	{
		___linksUpToDateField_23 = value;
	}

	inline static int32_t get_offset_of_linksUpToDateFieldSpecified_24() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___linksUpToDateFieldSpecified_24)); }
	inline bool get_linksUpToDateFieldSpecified_24() const { return ___linksUpToDateFieldSpecified_24; }
	inline bool* get_address_of_linksUpToDateFieldSpecified_24() { return &___linksUpToDateFieldSpecified_24; }
	inline void set_linksUpToDateFieldSpecified_24(bool value)
	{
		___linksUpToDateFieldSpecified_24 = value;
	}

	inline static int32_t get_offset_of_charactersWithSpacesField_25() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___charactersWithSpacesField_25)); }
	inline int32_t get_charactersWithSpacesField_25() const { return ___charactersWithSpacesField_25; }
	inline int32_t* get_address_of_charactersWithSpacesField_25() { return &___charactersWithSpacesField_25; }
	inline void set_charactersWithSpacesField_25(int32_t value)
	{
		___charactersWithSpacesField_25 = value;
	}

	inline static int32_t get_offset_of_sharedDocField_26() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___sharedDocField_26)); }
	inline bool get_sharedDocField_26() const { return ___sharedDocField_26; }
	inline bool* get_address_of_sharedDocField_26() { return &___sharedDocField_26; }
	inline void set_sharedDocField_26(bool value)
	{
		___sharedDocField_26 = value;
	}

	inline static int32_t get_offset_of_sharedDocFieldSpecified_27() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___sharedDocFieldSpecified_27)); }
	inline bool get_sharedDocFieldSpecified_27() const { return ___sharedDocFieldSpecified_27; }
	inline bool* get_address_of_sharedDocFieldSpecified_27() { return &___sharedDocFieldSpecified_27; }
	inline void set_sharedDocFieldSpecified_27(bool value)
	{
		___sharedDocFieldSpecified_27 = value;
	}

	inline static int32_t get_offset_of_hyperlinkBaseField_28() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___hyperlinkBaseField_28)); }
	inline String_t* get_hyperlinkBaseField_28() const { return ___hyperlinkBaseField_28; }
	inline String_t** get_address_of_hyperlinkBaseField_28() { return &___hyperlinkBaseField_28; }
	inline void set_hyperlinkBaseField_28(String_t* value)
	{
		___hyperlinkBaseField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hyperlinkBaseField_28), (void*)value);
	}

	inline static int32_t get_offset_of_hLinksField_29() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___hLinksField_29)); }
	inline CT_VectorVariant_t896EB2F643B186E8667F296CEC786D96E3511B20 * get_hLinksField_29() const { return ___hLinksField_29; }
	inline CT_VectorVariant_t896EB2F643B186E8667F296CEC786D96E3511B20 ** get_address_of_hLinksField_29() { return &___hLinksField_29; }
	inline void set_hLinksField_29(CT_VectorVariant_t896EB2F643B186E8667F296CEC786D96E3511B20 * value)
	{
		___hLinksField_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hLinksField_29), (void*)value);
	}

	inline static int32_t get_offset_of_hyperlinksChangedField_30() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___hyperlinksChangedField_30)); }
	inline bool get_hyperlinksChangedField_30() const { return ___hyperlinksChangedField_30; }
	inline bool* get_address_of_hyperlinksChangedField_30() { return &___hyperlinksChangedField_30; }
	inline void set_hyperlinksChangedField_30(bool value)
	{
		___hyperlinksChangedField_30 = value;
	}

	inline static int32_t get_offset_of_hyperlinksChangedFieldSpecified_31() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___hyperlinksChangedFieldSpecified_31)); }
	inline bool get_hyperlinksChangedFieldSpecified_31() const { return ___hyperlinksChangedFieldSpecified_31; }
	inline bool* get_address_of_hyperlinksChangedFieldSpecified_31() { return &___hyperlinksChangedFieldSpecified_31; }
	inline void set_hyperlinksChangedFieldSpecified_31(bool value)
	{
		___hyperlinksChangedFieldSpecified_31 = value;
	}

	inline static int32_t get_offset_of_digSigField_32() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___digSigField_32)); }
	inline CT_DigSigBlob_tABF9531F503B14657DF6DBEF6AB09BEFAECF18AF * get_digSigField_32() const { return ___digSigField_32; }
	inline CT_DigSigBlob_tABF9531F503B14657DF6DBEF6AB09BEFAECF18AF ** get_address_of_digSigField_32() { return &___digSigField_32; }
	inline void set_digSigField_32(CT_DigSigBlob_tABF9531F503B14657DF6DBEF6AB09BEFAECF18AF * value)
	{
		___digSigField_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___digSigField_32), (void*)value);
	}

	inline static int32_t get_offset_of_applicationField_33() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___applicationField_33)); }
	inline String_t* get_applicationField_33() const { return ___applicationField_33; }
	inline String_t** get_address_of_applicationField_33() { return &___applicationField_33; }
	inline void set_applicationField_33(String_t* value)
	{
		___applicationField_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___applicationField_33), (void*)value);
	}

	inline static int32_t get_offset_of_appVersionField_34() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___appVersionField_34)); }
	inline String_t* get_appVersionField_34() const { return ___appVersionField_34; }
	inline String_t** get_address_of_appVersionField_34() { return &___appVersionField_34; }
	inline void set_appVersionField_34(String_t* value)
	{
		___appVersionField_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appVersionField_34), (void*)value);
	}

	inline static int32_t get_offset_of_docSecurityField_35() { return static_cast<int32_t>(offsetof(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D, ___docSecurityField_35)); }
	inline int32_t get_docSecurityField_35() const { return ___docSecurityField_35; }
	inline int32_t* get_address_of_docSecurityField_35() { return &___docSecurityField_35; }
	inline void set_docSecurityField_35(int32_t value)
	{
		___docSecurityField_35 = value;
	}
};


// NPOI.OpenXmlFormats.CustomPropertiesDocument
struct  CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169  : public RuntimeObject
{
public:
	// NPOI.OpenXmlFormats.CT_CustomProperties NPOI.OpenXmlFormats.CustomPropertiesDocument::_props
	CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * ____props_2;

public:
	inline static int32_t get_offset_of__props_2() { return static_cast<int32_t>(offsetof(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169, ____props_2)); }
	inline CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * get__props_2() const { return ____props_2; }
	inline CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF ** get_address_of__props_2() { return &____props_2; }
	inline void set__props_2(CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * value)
	{
		____props_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____props_2), (void*)value);
	}
};

struct CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_StaticFields
{
public:
	// System.Xml.Serialization.XmlSerializer NPOI.OpenXmlFormats.CustomPropertiesDocument::serializer
	XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * ___serializer_0;
	// System.Xml.Serialization.XmlSerializerNamespaces NPOI.OpenXmlFormats.CustomPropertiesDocument::namespaces
	XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * ___namespaces_1;

public:
	inline static int32_t get_offset_of_serializer_0() { return static_cast<int32_t>(offsetof(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_StaticFields, ___serializer_0)); }
	inline XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * get_serializer_0() const { return ___serializer_0; }
	inline XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B ** get_address_of_serializer_0() { return &___serializer_0; }
	inline void set_serializer_0(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * value)
	{
		___serializer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializer_0), (void*)value);
	}

	inline static int32_t get_offset_of_namespaces_1() { return static_cast<int32_t>(offsetof(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_StaticFields, ___namespaces_1)); }
	inline XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * get_namespaces_1() const { return ___namespaces_1; }
	inline XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 ** get_address_of_namespaces_1() { return &___namespaces_1; }
	inline void set_namespaces_1(XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * value)
	{
		___namespaces_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___namespaces_1), (void*)value);
	}
};


// NPOI.OpenXmlFormats.ExtendedPropertiesDocument
struct  ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07  : public RuntimeObject
{
public:
	// NPOI.OpenXmlFormats.CT_ExtendedProperties NPOI.OpenXmlFormats.ExtendedPropertiesDocument::_props
	CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * ____props_2;

public:
	inline static int32_t get_offset_of__props_2() { return static_cast<int32_t>(offsetof(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07, ____props_2)); }
	inline CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * get__props_2() const { return ____props_2; }
	inline CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D ** get_address_of__props_2() { return &____props_2; }
	inline void set__props_2(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * value)
	{
		____props_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____props_2), (void*)value);
	}
};

struct ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_StaticFields
{
public:
	// System.Xml.Serialization.XmlSerializer NPOI.OpenXmlFormats.ExtendedPropertiesDocument::serializer
	XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * ___serializer_0;
	// System.Xml.Serialization.XmlSerializerNamespaces NPOI.OpenXmlFormats.ExtendedPropertiesDocument::namespaces
	XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * ___namespaces_1;

public:
	inline static int32_t get_offset_of_serializer_0() { return static_cast<int32_t>(offsetof(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_StaticFields, ___serializer_0)); }
	inline XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * get_serializer_0() const { return ___serializer_0; }
	inline XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B ** get_address_of_serializer_0() { return &___serializer_0; }
	inline void set_serializer_0(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * value)
	{
		___serializer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializer_0), (void*)value);
	}

	inline static int32_t get_offset_of_namespaces_1() { return static_cast<int32_t>(offsetof(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_StaticFields, ___namespaces_1)); }
	inline XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * get_namespaces_1() const { return ___namespaces_1; }
	inline XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 ** get_address_of_namespaces_1() { return &___namespaces_1; }
	inline void set_namespaces_1(XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * value)
	{
		___namespaces_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___namespaces_1), (void*)value);
	}
};


// NPOI.OpenXmlFormats.ST_RelationshipId
struct  ST_RelationshipId_t3C559C7F8F9F3B3B12C7E8A99771D58A2DEB9C4C  : public RuntimeObject
{
public:

public:
};


// NPOI.OpenXmlFormats.Spreadsheet.CT_Sst
struct  CT_Sst_tB80B6A0914663BCEE99DB793DA7F73354949D6A9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.Spreadsheet.CT_Rst> NPOI.OpenXmlFormats.Spreadsheet.CT_Sst::siField
	List_1_t4C6AD5154887CA031844733D9DD5AC7A568F10D1 * ___siField_0;

public:
	inline static int32_t get_offset_of_siField_0() { return static_cast<int32_t>(offsetof(CT_Sst_tB80B6A0914663BCEE99DB793DA7F73354949D6A9, ___siField_0)); }
	inline List_1_t4C6AD5154887CA031844733D9DD5AC7A568F10D1 * get_siField_0() const { return ___siField_0; }
	inline List_1_t4C6AD5154887CA031844733D9DD5AC7A568F10D1 ** get_address_of_siField_0() { return &___siField_0; }
	inline void set_siField_0(List_1_t4C6AD5154887CA031844733D9DD5AC7A568F10D1 * value)
	{
		___siField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___siField_0), (void*)value);
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook
struct  CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0  : public RuntimeObject
{
public:
	// NPOI.OpenXmlFormats.Spreadsheet.CT_FileVersion NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::fileVersionField
	CT_FileVersion_t57AC2B828FFBB31C09019731C69A7F04BC998C60 * ___fileVersionField_0;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_FileSharing NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::fileSharingField
	CT_FileSharing_t3C62AA72F30681F926B179B922162E4FA5264534 * ___fileSharingField_1;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_WorkbookPr NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::workbookPrField
	CT_WorkbookPr_t797A81C397F684944238BC1706A781E51320FFC2 * ___workbookPrField_2;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_WorkbookProtection NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::workbookProtectionField
	CT_WorkbookProtection_t8B126050A994F3A5F553F3EA5FB9C136AD3E004F * ___workbookProtectionField_3;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_BookViews NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::bookViewsField
	CT_BookViews_t3A648C8F40EE0569A11F73CB2248C165DD3D383C * ___bookViewsField_4;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_Sheets NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::sheetsField
	CT_Sheets_t1E3738CE072A59495E71685E1CBC5559B4F61CF7 * ___sheetsField_5;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_FunctionGroups NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::functionGroupsField
	CT_FunctionGroups_t87EBBDC862F6D4F7ACC107C56E2B94B055D5455D * ___functionGroupsField_6;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_ExternalReferences NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::externalReferencesField
	CT_ExternalReferences_t7EC75ED08C5E6BAA88A66AB3B78834839BAB4E1B * ___externalReferencesField_7;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_DefinedNames NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::definedNamesField
	CT_DefinedNames_t626C75D86F91C16E37CA24049B7248459E8D944E * ___definedNamesField_8;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_CalcPr NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::calcPrField
	CT_CalcPr_tE59C9AC5980E29877AC4145B82328585616B6512 * ___calcPrField_9;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_OleSize NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::oleSizeField
	CT_OleSize_t1E498D69E55B6A5C0EE32E37488BC7E525FF817A * ___oleSizeField_10;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_CustomWorkbookViews NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::customWorkbookViewsField
	CT_CustomWorkbookViews_tD84E01AD55489ED6E4160B12832B5FE429C9EE9D * ___customWorkbookViewsField_11;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_PivotCaches NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::pivotCachesField
	CT_PivotCaches_t9EBD199955E5E828CFB50EA206BD076D823D9920 * ___pivotCachesField_12;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SmartTagPr NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::smartTagPrField
	CT_SmartTagPr_tD04E077C0776903B30B9FC960D060B8758C3B579 * ___smartTagPrField_13;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SmartTagTypes NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::smartTagTypesField
	CT_SmartTagTypes_tBADE31A08C8B4974A2EF07FB04CF951CC95E109F * ___smartTagTypesField_14;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_WebPublishing NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::webPublishingField
	CT_WebPublishing_t23C3EF722A362B0DEB34022832E41D43CF5978E6 * ___webPublishingField_15;
	// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.Spreadsheet.CT_FileRecoveryPr> NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::fileRecoveryPrField
	List_1_t657B3A9797162495F76252E7E48DC1E8FA1054CF * ___fileRecoveryPrField_16;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_WebPublishObjects NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::webPublishObjectsField
	CT_WebPublishObjects_t86BA08D531D57ADD5CEA7360566E2E7FAA3143C5 * ___webPublishObjectsField_17;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_ExtensionList NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::extLstField
	CT_ExtensionList_t79ED67739D16E8748F4B890D07A37591BB568F66 * ___extLstField_18;

public:
	inline static int32_t get_offset_of_fileVersionField_0() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___fileVersionField_0)); }
	inline CT_FileVersion_t57AC2B828FFBB31C09019731C69A7F04BC998C60 * get_fileVersionField_0() const { return ___fileVersionField_0; }
	inline CT_FileVersion_t57AC2B828FFBB31C09019731C69A7F04BC998C60 ** get_address_of_fileVersionField_0() { return &___fileVersionField_0; }
	inline void set_fileVersionField_0(CT_FileVersion_t57AC2B828FFBB31C09019731C69A7F04BC998C60 * value)
	{
		___fileVersionField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileVersionField_0), (void*)value);
	}

	inline static int32_t get_offset_of_fileSharingField_1() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___fileSharingField_1)); }
	inline CT_FileSharing_t3C62AA72F30681F926B179B922162E4FA5264534 * get_fileSharingField_1() const { return ___fileSharingField_1; }
	inline CT_FileSharing_t3C62AA72F30681F926B179B922162E4FA5264534 ** get_address_of_fileSharingField_1() { return &___fileSharingField_1; }
	inline void set_fileSharingField_1(CT_FileSharing_t3C62AA72F30681F926B179B922162E4FA5264534 * value)
	{
		___fileSharingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileSharingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_workbookPrField_2() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___workbookPrField_2)); }
	inline CT_WorkbookPr_t797A81C397F684944238BC1706A781E51320FFC2 * get_workbookPrField_2() const { return ___workbookPrField_2; }
	inline CT_WorkbookPr_t797A81C397F684944238BC1706A781E51320FFC2 ** get_address_of_workbookPrField_2() { return &___workbookPrField_2; }
	inline void set_workbookPrField_2(CT_WorkbookPr_t797A81C397F684944238BC1706A781E51320FFC2 * value)
	{
		___workbookPrField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___workbookPrField_2), (void*)value);
	}

	inline static int32_t get_offset_of_workbookProtectionField_3() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___workbookProtectionField_3)); }
	inline CT_WorkbookProtection_t8B126050A994F3A5F553F3EA5FB9C136AD3E004F * get_workbookProtectionField_3() const { return ___workbookProtectionField_3; }
	inline CT_WorkbookProtection_t8B126050A994F3A5F553F3EA5FB9C136AD3E004F ** get_address_of_workbookProtectionField_3() { return &___workbookProtectionField_3; }
	inline void set_workbookProtectionField_3(CT_WorkbookProtection_t8B126050A994F3A5F553F3EA5FB9C136AD3E004F * value)
	{
		___workbookProtectionField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___workbookProtectionField_3), (void*)value);
	}

	inline static int32_t get_offset_of_bookViewsField_4() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___bookViewsField_4)); }
	inline CT_BookViews_t3A648C8F40EE0569A11F73CB2248C165DD3D383C * get_bookViewsField_4() const { return ___bookViewsField_4; }
	inline CT_BookViews_t3A648C8F40EE0569A11F73CB2248C165DD3D383C ** get_address_of_bookViewsField_4() { return &___bookViewsField_4; }
	inline void set_bookViewsField_4(CT_BookViews_t3A648C8F40EE0569A11F73CB2248C165DD3D383C * value)
	{
		___bookViewsField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bookViewsField_4), (void*)value);
	}

	inline static int32_t get_offset_of_sheetsField_5() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___sheetsField_5)); }
	inline CT_Sheets_t1E3738CE072A59495E71685E1CBC5559B4F61CF7 * get_sheetsField_5() const { return ___sheetsField_5; }
	inline CT_Sheets_t1E3738CE072A59495E71685E1CBC5559B4F61CF7 ** get_address_of_sheetsField_5() { return &___sheetsField_5; }
	inline void set_sheetsField_5(CT_Sheets_t1E3738CE072A59495E71685E1CBC5559B4F61CF7 * value)
	{
		___sheetsField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sheetsField_5), (void*)value);
	}

	inline static int32_t get_offset_of_functionGroupsField_6() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___functionGroupsField_6)); }
	inline CT_FunctionGroups_t87EBBDC862F6D4F7ACC107C56E2B94B055D5455D * get_functionGroupsField_6() const { return ___functionGroupsField_6; }
	inline CT_FunctionGroups_t87EBBDC862F6D4F7ACC107C56E2B94B055D5455D ** get_address_of_functionGroupsField_6() { return &___functionGroupsField_6; }
	inline void set_functionGroupsField_6(CT_FunctionGroups_t87EBBDC862F6D4F7ACC107C56E2B94B055D5455D * value)
	{
		___functionGroupsField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___functionGroupsField_6), (void*)value);
	}

	inline static int32_t get_offset_of_externalReferencesField_7() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___externalReferencesField_7)); }
	inline CT_ExternalReferences_t7EC75ED08C5E6BAA88A66AB3B78834839BAB4E1B * get_externalReferencesField_7() const { return ___externalReferencesField_7; }
	inline CT_ExternalReferences_t7EC75ED08C5E6BAA88A66AB3B78834839BAB4E1B ** get_address_of_externalReferencesField_7() { return &___externalReferencesField_7; }
	inline void set_externalReferencesField_7(CT_ExternalReferences_t7EC75ED08C5E6BAA88A66AB3B78834839BAB4E1B * value)
	{
		___externalReferencesField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___externalReferencesField_7), (void*)value);
	}

	inline static int32_t get_offset_of_definedNamesField_8() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___definedNamesField_8)); }
	inline CT_DefinedNames_t626C75D86F91C16E37CA24049B7248459E8D944E * get_definedNamesField_8() const { return ___definedNamesField_8; }
	inline CT_DefinedNames_t626C75D86F91C16E37CA24049B7248459E8D944E ** get_address_of_definedNamesField_8() { return &___definedNamesField_8; }
	inline void set_definedNamesField_8(CT_DefinedNames_t626C75D86F91C16E37CA24049B7248459E8D944E * value)
	{
		___definedNamesField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___definedNamesField_8), (void*)value);
	}

	inline static int32_t get_offset_of_calcPrField_9() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___calcPrField_9)); }
	inline CT_CalcPr_tE59C9AC5980E29877AC4145B82328585616B6512 * get_calcPrField_9() const { return ___calcPrField_9; }
	inline CT_CalcPr_tE59C9AC5980E29877AC4145B82328585616B6512 ** get_address_of_calcPrField_9() { return &___calcPrField_9; }
	inline void set_calcPrField_9(CT_CalcPr_tE59C9AC5980E29877AC4145B82328585616B6512 * value)
	{
		___calcPrField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___calcPrField_9), (void*)value);
	}

	inline static int32_t get_offset_of_oleSizeField_10() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___oleSizeField_10)); }
	inline CT_OleSize_t1E498D69E55B6A5C0EE32E37488BC7E525FF817A * get_oleSizeField_10() const { return ___oleSizeField_10; }
	inline CT_OleSize_t1E498D69E55B6A5C0EE32E37488BC7E525FF817A ** get_address_of_oleSizeField_10() { return &___oleSizeField_10; }
	inline void set_oleSizeField_10(CT_OleSize_t1E498D69E55B6A5C0EE32E37488BC7E525FF817A * value)
	{
		___oleSizeField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oleSizeField_10), (void*)value);
	}

	inline static int32_t get_offset_of_customWorkbookViewsField_11() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___customWorkbookViewsField_11)); }
	inline CT_CustomWorkbookViews_tD84E01AD55489ED6E4160B12832B5FE429C9EE9D * get_customWorkbookViewsField_11() const { return ___customWorkbookViewsField_11; }
	inline CT_CustomWorkbookViews_tD84E01AD55489ED6E4160B12832B5FE429C9EE9D ** get_address_of_customWorkbookViewsField_11() { return &___customWorkbookViewsField_11; }
	inline void set_customWorkbookViewsField_11(CT_CustomWorkbookViews_tD84E01AD55489ED6E4160B12832B5FE429C9EE9D * value)
	{
		___customWorkbookViewsField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customWorkbookViewsField_11), (void*)value);
	}

	inline static int32_t get_offset_of_pivotCachesField_12() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___pivotCachesField_12)); }
	inline CT_PivotCaches_t9EBD199955E5E828CFB50EA206BD076D823D9920 * get_pivotCachesField_12() const { return ___pivotCachesField_12; }
	inline CT_PivotCaches_t9EBD199955E5E828CFB50EA206BD076D823D9920 ** get_address_of_pivotCachesField_12() { return &___pivotCachesField_12; }
	inline void set_pivotCachesField_12(CT_PivotCaches_t9EBD199955E5E828CFB50EA206BD076D823D9920 * value)
	{
		___pivotCachesField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pivotCachesField_12), (void*)value);
	}

	inline static int32_t get_offset_of_smartTagPrField_13() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___smartTagPrField_13)); }
	inline CT_SmartTagPr_tD04E077C0776903B30B9FC960D060B8758C3B579 * get_smartTagPrField_13() const { return ___smartTagPrField_13; }
	inline CT_SmartTagPr_tD04E077C0776903B30B9FC960D060B8758C3B579 ** get_address_of_smartTagPrField_13() { return &___smartTagPrField_13; }
	inline void set_smartTagPrField_13(CT_SmartTagPr_tD04E077C0776903B30B9FC960D060B8758C3B579 * value)
	{
		___smartTagPrField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___smartTagPrField_13), (void*)value);
	}

	inline static int32_t get_offset_of_smartTagTypesField_14() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___smartTagTypesField_14)); }
	inline CT_SmartTagTypes_tBADE31A08C8B4974A2EF07FB04CF951CC95E109F * get_smartTagTypesField_14() const { return ___smartTagTypesField_14; }
	inline CT_SmartTagTypes_tBADE31A08C8B4974A2EF07FB04CF951CC95E109F ** get_address_of_smartTagTypesField_14() { return &___smartTagTypesField_14; }
	inline void set_smartTagTypesField_14(CT_SmartTagTypes_tBADE31A08C8B4974A2EF07FB04CF951CC95E109F * value)
	{
		___smartTagTypesField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___smartTagTypesField_14), (void*)value);
	}

	inline static int32_t get_offset_of_webPublishingField_15() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___webPublishingField_15)); }
	inline CT_WebPublishing_t23C3EF722A362B0DEB34022832E41D43CF5978E6 * get_webPublishingField_15() const { return ___webPublishingField_15; }
	inline CT_WebPublishing_t23C3EF722A362B0DEB34022832E41D43CF5978E6 ** get_address_of_webPublishingField_15() { return &___webPublishingField_15; }
	inline void set_webPublishingField_15(CT_WebPublishing_t23C3EF722A362B0DEB34022832E41D43CF5978E6 * value)
	{
		___webPublishingField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___webPublishingField_15), (void*)value);
	}

	inline static int32_t get_offset_of_fileRecoveryPrField_16() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___fileRecoveryPrField_16)); }
	inline List_1_t657B3A9797162495F76252E7E48DC1E8FA1054CF * get_fileRecoveryPrField_16() const { return ___fileRecoveryPrField_16; }
	inline List_1_t657B3A9797162495F76252E7E48DC1E8FA1054CF ** get_address_of_fileRecoveryPrField_16() { return &___fileRecoveryPrField_16; }
	inline void set_fileRecoveryPrField_16(List_1_t657B3A9797162495F76252E7E48DC1E8FA1054CF * value)
	{
		___fileRecoveryPrField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileRecoveryPrField_16), (void*)value);
	}

	inline static int32_t get_offset_of_webPublishObjectsField_17() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___webPublishObjectsField_17)); }
	inline CT_WebPublishObjects_t86BA08D531D57ADD5CEA7360566E2E7FAA3143C5 * get_webPublishObjectsField_17() const { return ___webPublishObjectsField_17; }
	inline CT_WebPublishObjects_t86BA08D531D57ADD5CEA7360566E2E7FAA3143C5 ** get_address_of_webPublishObjectsField_17() { return &___webPublishObjectsField_17; }
	inline void set_webPublishObjectsField_17(CT_WebPublishObjects_t86BA08D531D57ADD5CEA7360566E2E7FAA3143C5 * value)
	{
		___webPublishObjectsField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___webPublishObjectsField_17), (void*)value);
	}

	inline static int32_t get_offset_of_extLstField_18() { return static_cast<int32_t>(offsetof(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0, ___extLstField_18)); }
	inline CT_ExtensionList_t79ED67739D16E8748F4B890D07A37591BB568F66 * get_extLstField_18() const { return ___extLstField_18; }
	inline CT_ExtensionList_t79ED67739D16E8748F4B890D07A37591BB568F66 ** get_address_of_extLstField_18() { return &___extLstField_18; }
	inline void set_extLstField_18(CT_ExtensionList_t79ED67739D16E8748F4B890D07A37591BB568F66 * value)
	{
		___extLstField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extLstField_18), (void*)value);
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet
struct  CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF  : public RuntimeObject
{
public:
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetPr NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::sheetPrField
	CT_SheetPr_tF94369547EE810CB1FAF7CC5564291692AF7B636 * ___sheetPrField_0;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetDimension NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::dimensionField
	CT_SheetDimension_t431685C4E43A0CD54A4F48D2A588E0B2958E44E6 * ___dimensionField_1;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetViews NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::sheetViewsField
	CT_SheetViews_tD7012A0FDF0ED74C8251C71A86ACB4A86C9C4E03 * ___sheetViewsField_2;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetFormatPr NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::sheetFormatPrField
	CT_SheetFormatPr_t2329725F61309FF2A6490FB54F40E223914A05F7 * ___sheetFormatPrField_3;
	// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.Spreadsheet.CT_Cols> NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::colsField
	List_1_t835AE5436C3D76B93E8EDF3561BE98BE17BC578D * ___colsField_4;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetData NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::sheetDataField
	CT_SheetData_t538FCD2B39075937476FBB9823513F06345D5E1D * ___sheetDataField_5;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetCalcPr NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::sheetCalcPrField
	CT_SheetCalcPr_t222A8620FBDEEA62B5B596C64700D3F0C6CC9EDB * ___sheetCalcPrField_6;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetProtection NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::sheetProtectionField
	CT_SheetProtection_t6B812F2AEC7469E24F2FAC813DB2BB5DB76DEEAF * ___sheetProtectionField_7;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_ProtectedRanges NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::protectedRangesField
	CT_ProtectedRanges_tF0FA33DE18F463BB6E99986F30F9A20695B9A01C * ___protectedRangesField_8;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_Scenarios NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::scenariosField
	CT_Scenarios_tAC7DDBB1B4BC086B57A911B030C5D784A0D7F643 * ___scenariosField_9;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_AutoFilter NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::autoFilterField
	CT_AutoFilter_tD17FACB8B3FA6B2D42EAF7D283C59FB6E354E54A * ___autoFilterField_10;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SortState NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::sortStateField
	CT_SortState_tB6AFFB8A5F46F8E04AF0E3F26B09AA548B9D447B * ___sortStateField_11;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_DataConsolidate NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::dataConsolidateField
	CT_DataConsolidate_t6BA590472F9874782F6D09BF8502377F8A85BB62 * ___dataConsolidateField_12;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_CustomSheetViews NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::customSheetViewsField
	CT_CustomSheetViews_t6AF3C33141DEBB763F7E617B6DD7F40D7865E69C * ___customSheetViewsField_13;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_MergeCells NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::mergeCellsField
	CT_MergeCells_tC244C81D9B6D7B8B262A3244CEC1E33DBC05465C * ___mergeCellsField_14;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_PhoneticPr NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::phoneticPrField
	CT_PhoneticPr_t9B7890742E83E837E73C48AF52676C18F50240E8 * ___phoneticPrField_15;
	// System.Collections.Generic.List`1<NPOI.OpenXmlFormats.Spreadsheet.CT_ConditionalFormatting> NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::conditionalFormattingField
	List_1_t48E5086280838656B52038009060E22420EC1756 * ___conditionalFormattingField_16;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_DataValidations NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::dataValidationsField
	CT_DataValidations_t8D8CBC64BC7AE3A87A2D97CDA62009D74292C35A * ___dataValidationsField_17;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_Hyperlinks NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::hyperlinksField
	CT_Hyperlinks_t967310005D63AA3C3E942B1AECD8EAC275E7359F * ___hyperlinksField_18;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_PrintOptions NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::printOptionsField
	CT_PrintOptions_tBF0437CD33D2F94344998C5F43108143353F1148 * ___printOptionsField_19;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_PageMargins NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::pageMarginsField
	CT_PageMargins_t48C935B82A4C55550510A8CF006E0817C13982E9 * ___pageMarginsField_20;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_PageSetup NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::pageSetupField
	CT_PageSetup_t136C6F167D4E7A2E2480A50CAD462FBE1D1715A5 * ___pageSetupField_21;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_HeaderFooter NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::headerFooterField
	CT_HeaderFooter_tE734591E584CBD00BD317DE44533E600FAAE9D2B * ___headerFooterField_22;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_PageBreak NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::rowBreaksField
	CT_PageBreak_tBA4435226581E50E78304255BED5D4775F85BBB8 * ___rowBreaksField_23;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_PageBreak NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::colBreaksField
	CT_PageBreak_tBA4435226581E50E78304255BED5D4775F85BBB8 * ___colBreaksField_24;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_CustomProperties NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::customPropertiesField
	CT_CustomProperties_t7BE31A9D707BD3B4EE745A9AF575A41AE26E49BF * ___customPropertiesField_25;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_CellWatches NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::cellWatchesField
	CT_CellWatches_t01F7D7188788534CB4D6FA87507126AE3F3C467B * ___cellWatchesField_26;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_IgnoredErrors NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::ignoredErrorsField
	CT_IgnoredErrors_t5D0C0D432AA19CC3B66CB5F5BEA9F5009ABE599E * ___ignoredErrorsField_27;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_CellSmartTags NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::smartTagsField
	CT_CellSmartTags_t848F74CABCB4CC4048AF8189644352B4999AD91E * ___smartTagsField_28;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_Drawing NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::drawingField
	CT_Drawing_tD2F6724924616C586D3810460A5E0962BCE3A0BB * ___drawingField_29;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_LegacyDrawing NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::legacyDrawingField
	CT_LegacyDrawing_t37EC868D8930A4F935DB6358433328195ACFAC70 * ___legacyDrawingField_30;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_LegacyDrawing NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::legacyDrawingHFField
	CT_LegacyDrawing_t37EC868D8930A4F935DB6358433328195ACFAC70 * ___legacyDrawingHFField_31;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_SheetBackgroundPicture NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::pictureField
	CT_SheetBackgroundPicture_tA51AFAD22D4F4C6CA5A208A29AF4586B25C56FD3 * ___pictureField_32;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_OleObjects NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::oleObjectsField
	CT_OleObjects_tD28B4F45856085620F2065D8441AAF8E4956415D * ___oleObjectsField_33;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_Controls NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::controlsField
	CT_Controls_t564AEB49B666E470DF52CDC90498C94EEE684B9A * ___controlsField_34;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_WebPublishItems NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::webPublishItemsField
	CT_WebPublishItems_tE2398221E4BEA0EFC8BF2AAB546AD668BC9C3267 * ___webPublishItemsField_35;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_TableParts NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::tablePartsField
	CT_TableParts_t78D1D9FFB376C6DECADD9D4FE55C2F180B8F7DA1 * ___tablePartsField_36;
	// NPOI.OpenXmlFormats.Spreadsheet.CT_ExtensionList NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::extLstField
	CT_ExtensionList_t79ED67739D16E8748F4B890D07A37591BB568F66 * ___extLstField_37;

public:
	inline static int32_t get_offset_of_sheetPrField_0() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___sheetPrField_0)); }
	inline CT_SheetPr_tF94369547EE810CB1FAF7CC5564291692AF7B636 * get_sheetPrField_0() const { return ___sheetPrField_0; }
	inline CT_SheetPr_tF94369547EE810CB1FAF7CC5564291692AF7B636 ** get_address_of_sheetPrField_0() { return &___sheetPrField_0; }
	inline void set_sheetPrField_0(CT_SheetPr_tF94369547EE810CB1FAF7CC5564291692AF7B636 * value)
	{
		___sheetPrField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sheetPrField_0), (void*)value);
	}

	inline static int32_t get_offset_of_dimensionField_1() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___dimensionField_1)); }
	inline CT_SheetDimension_t431685C4E43A0CD54A4F48D2A588E0B2958E44E6 * get_dimensionField_1() const { return ___dimensionField_1; }
	inline CT_SheetDimension_t431685C4E43A0CD54A4F48D2A588E0B2958E44E6 ** get_address_of_dimensionField_1() { return &___dimensionField_1; }
	inline void set_dimensionField_1(CT_SheetDimension_t431685C4E43A0CD54A4F48D2A588E0B2958E44E6 * value)
	{
		___dimensionField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dimensionField_1), (void*)value);
	}

	inline static int32_t get_offset_of_sheetViewsField_2() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___sheetViewsField_2)); }
	inline CT_SheetViews_tD7012A0FDF0ED74C8251C71A86ACB4A86C9C4E03 * get_sheetViewsField_2() const { return ___sheetViewsField_2; }
	inline CT_SheetViews_tD7012A0FDF0ED74C8251C71A86ACB4A86C9C4E03 ** get_address_of_sheetViewsField_2() { return &___sheetViewsField_2; }
	inline void set_sheetViewsField_2(CT_SheetViews_tD7012A0FDF0ED74C8251C71A86ACB4A86C9C4E03 * value)
	{
		___sheetViewsField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sheetViewsField_2), (void*)value);
	}

	inline static int32_t get_offset_of_sheetFormatPrField_3() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___sheetFormatPrField_3)); }
	inline CT_SheetFormatPr_t2329725F61309FF2A6490FB54F40E223914A05F7 * get_sheetFormatPrField_3() const { return ___sheetFormatPrField_3; }
	inline CT_SheetFormatPr_t2329725F61309FF2A6490FB54F40E223914A05F7 ** get_address_of_sheetFormatPrField_3() { return &___sheetFormatPrField_3; }
	inline void set_sheetFormatPrField_3(CT_SheetFormatPr_t2329725F61309FF2A6490FB54F40E223914A05F7 * value)
	{
		___sheetFormatPrField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sheetFormatPrField_3), (void*)value);
	}

	inline static int32_t get_offset_of_colsField_4() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___colsField_4)); }
	inline List_1_t835AE5436C3D76B93E8EDF3561BE98BE17BC578D * get_colsField_4() const { return ___colsField_4; }
	inline List_1_t835AE5436C3D76B93E8EDF3561BE98BE17BC578D ** get_address_of_colsField_4() { return &___colsField_4; }
	inline void set_colsField_4(List_1_t835AE5436C3D76B93E8EDF3561BE98BE17BC578D * value)
	{
		___colsField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colsField_4), (void*)value);
	}

	inline static int32_t get_offset_of_sheetDataField_5() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___sheetDataField_5)); }
	inline CT_SheetData_t538FCD2B39075937476FBB9823513F06345D5E1D * get_sheetDataField_5() const { return ___sheetDataField_5; }
	inline CT_SheetData_t538FCD2B39075937476FBB9823513F06345D5E1D ** get_address_of_sheetDataField_5() { return &___sheetDataField_5; }
	inline void set_sheetDataField_5(CT_SheetData_t538FCD2B39075937476FBB9823513F06345D5E1D * value)
	{
		___sheetDataField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sheetDataField_5), (void*)value);
	}

	inline static int32_t get_offset_of_sheetCalcPrField_6() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___sheetCalcPrField_6)); }
	inline CT_SheetCalcPr_t222A8620FBDEEA62B5B596C64700D3F0C6CC9EDB * get_sheetCalcPrField_6() const { return ___sheetCalcPrField_6; }
	inline CT_SheetCalcPr_t222A8620FBDEEA62B5B596C64700D3F0C6CC9EDB ** get_address_of_sheetCalcPrField_6() { return &___sheetCalcPrField_6; }
	inline void set_sheetCalcPrField_6(CT_SheetCalcPr_t222A8620FBDEEA62B5B596C64700D3F0C6CC9EDB * value)
	{
		___sheetCalcPrField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sheetCalcPrField_6), (void*)value);
	}

	inline static int32_t get_offset_of_sheetProtectionField_7() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___sheetProtectionField_7)); }
	inline CT_SheetProtection_t6B812F2AEC7469E24F2FAC813DB2BB5DB76DEEAF * get_sheetProtectionField_7() const { return ___sheetProtectionField_7; }
	inline CT_SheetProtection_t6B812F2AEC7469E24F2FAC813DB2BB5DB76DEEAF ** get_address_of_sheetProtectionField_7() { return &___sheetProtectionField_7; }
	inline void set_sheetProtectionField_7(CT_SheetProtection_t6B812F2AEC7469E24F2FAC813DB2BB5DB76DEEAF * value)
	{
		___sheetProtectionField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sheetProtectionField_7), (void*)value);
	}

	inline static int32_t get_offset_of_protectedRangesField_8() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___protectedRangesField_8)); }
	inline CT_ProtectedRanges_tF0FA33DE18F463BB6E99986F30F9A20695B9A01C * get_protectedRangesField_8() const { return ___protectedRangesField_8; }
	inline CT_ProtectedRanges_tF0FA33DE18F463BB6E99986F30F9A20695B9A01C ** get_address_of_protectedRangesField_8() { return &___protectedRangesField_8; }
	inline void set_protectedRangesField_8(CT_ProtectedRanges_tF0FA33DE18F463BB6E99986F30F9A20695B9A01C * value)
	{
		___protectedRangesField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___protectedRangesField_8), (void*)value);
	}

	inline static int32_t get_offset_of_scenariosField_9() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___scenariosField_9)); }
	inline CT_Scenarios_tAC7DDBB1B4BC086B57A911B030C5D784A0D7F643 * get_scenariosField_9() const { return ___scenariosField_9; }
	inline CT_Scenarios_tAC7DDBB1B4BC086B57A911B030C5D784A0D7F643 ** get_address_of_scenariosField_9() { return &___scenariosField_9; }
	inline void set_scenariosField_9(CT_Scenarios_tAC7DDBB1B4BC086B57A911B030C5D784A0D7F643 * value)
	{
		___scenariosField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scenariosField_9), (void*)value);
	}

	inline static int32_t get_offset_of_autoFilterField_10() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___autoFilterField_10)); }
	inline CT_AutoFilter_tD17FACB8B3FA6B2D42EAF7D283C59FB6E354E54A * get_autoFilterField_10() const { return ___autoFilterField_10; }
	inline CT_AutoFilter_tD17FACB8B3FA6B2D42EAF7D283C59FB6E354E54A ** get_address_of_autoFilterField_10() { return &___autoFilterField_10; }
	inline void set_autoFilterField_10(CT_AutoFilter_tD17FACB8B3FA6B2D42EAF7D283C59FB6E354E54A * value)
	{
		___autoFilterField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___autoFilterField_10), (void*)value);
	}

	inline static int32_t get_offset_of_sortStateField_11() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___sortStateField_11)); }
	inline CT_SortState_tB6AFFB8A5F46F8E04AF0E3F26B09AA548B9D447B * get_sortStateField_11() const { return ___sortStateField_11; }
	inline CT_SortState_tB6AFFB8A5F46F8E04AF0E3F26B09AA548B9D447B ** get_address_of_sortStateField_11() { return &___sortStateField_11; }
	inline void set_sortStateField_11(CT_SortState_tB6AFFB8A5F46F8E04AF0E3F26B09AA548B9D447B * value)
	{
		___sortStateField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sortStateField_11), (void*)value);
	}

	inline static int32_t get_offset_of_dataConsolidateField_12() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___dataConsolidateField_12)); }
	inline CT_DataConsolidate_t6BA590472F9874782F6D09BF8502377F8A85BB62 * get_dataConsolidateField_12() const { return ___dataConsolidateField_12; }
	inline CT_DataConsolidate_t6BA590472F9874782F6D09BF8502377F8A85BB62 ** get_address_of_dataConsolidateField_12() { return &___dataConsolidateField_12; }
	inline void set_dataConsolidateField_12(CT_DataConsolidate_t6BA590472F9874782F6D09BF8502377F8A85BB62 * value)
	{
		___dataConsolidateField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataConsolidateField_12), (void*)value);
	}

	inline static int32_t get_offset_of_customSheetViewsField_13() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___customSheetViewsField_13)); }
	inline CT_CustomSheetViews_t6AF3C33141DEBB763F7E617B6DD7F40D7865E69C * get_customSheetViewsField_13() const { return ___customSheetViewsField_13; }
	inline CT_CustomSheetViews_t6AF3C33141DEBB763F7E617B6DD7F40D7865E69C ** get_address_of_customSheetViewsField_13() { return &___customSheetViewsField_13; }
	inline void set_customSheetViewsField_13(CT_CustomSheetViews_t6AF3C33141DEBB763F7E617B6DD7F40D7865E69C * value)
	{
		___customSheetViewsField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customSheetViewsField_13), (void*)value);
	}

	inline static int32_t get_offset_of_mergeCellsField_14() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___mergeCellsField_14)); }
	inline CT_MergeCells_tC244C81D9B6D7B8B262A3244CEC1E33DBC05465C * get_mergeCellsField_14() const { return ___mergeCellsField_14; }
	inline CT_MergeCells_tC244C81D9B6D7B8B262A3244CEC1E33DBC05465C ** get_address_of_mergeCellsField_14() { return &___mergeCellsField_14; }
	inline void set_mergeCellsField_14(CT_MergeCells_tC244C81D9B6D7B8B262A3244CEC1E33DBC05465C * value)
	{
		___mergeCellsField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mergeCellsField_14), (void*)value);
	}

	inline static int32_t get_offset_of_phoneticPrField_15() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___phoneticPrField_15)); }
	inline CT_PhoneticPr_t9B7890742E83E837E73C48AF52676C18F50240E8 * get_phoneticPrField_15() const { return ___phoneticPrField_15; }
	inline CT_PhoneticPr_t9B7890742E83E837E73C48AF52676C18F50240E8 ** get_address_of_phoneticPrField_15() { return &___phoneticPrField_15; }
	inline void set_phoneticPrField_15(CT_PhoneticPr_t9B7890742E83E837E73C48AF52676C18F50240E8 * value)
	{
		___phoneticPrField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___phoneticPrField_15), (void*)value);
	}

	inline static int32_t get_offset_of_conditionalFormattingField_16() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___conditionalFormattingField_16)); }
	inline List_1_t48E5086280838656B52038009060E22420EC1756 * get_conditionalFormattingField_16() const { return ___conditionalFormattingField_16; }
	inline List_1_t48E5086280838656B52038009060E22420EC1756 ** get_address_of_conditionalFormattingField_16() { return &___conditionalFormattingField_16; }
	inline void set_conditionalFormattingField_16(List_1_t48E5086280838656B52038009060E22420EC1756 * value)
	{
		___conditionalFormattingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___conditionalFormattingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_dataValidationsField_17() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___dataValidationsField_17)); }
	inline CT_DataValidations_t8D8CBC64BC7AE3A87A2D97CDA62009D74292C35A * get_dataValidationsField_17() const { return ___dataValidationsField_17; }
	inline CT_DataValidations_t8D8CBC64BC7AE3A87A2D97CDA62009D74292C35A ** get_address_of_dataValidationsField_17() { return &___dataValidationsField_17; }
	inline void set_dataValidationsField_17(CT_DataValidations_t8D8CBC64BC7AE3A87A2D97CDA62009D74292C35A * value)
	{
		___dataValidationsField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataValidationsField_17), (void*)value);
	}

	inline static int32_t get_offset_of_hyperlinksField_18() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___hyperlinksField_18)); }
	inline CT_Hyperlinks_t967310005D63AA3C3E942B1AECD8EAC275E7359F * get_hyperlinksField_18() const { return ___hyperlinksField_18; }
	inline CT_Hyperlinks_t967310005D63AA3C3E942B1AECD8EAC275E7359F ** get_address_of_hyperlinksField_18() { return &___hyperlinksField_18; }
	inline void set_hyperlinksField_18(CT_Hyperlinks_t967310005D63AA3C3E942B1AECD8EAC275E7359F * value)
	{
		___hyperlinksField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hyperlinksField_18), (void*)value);
	}

	inline static int32_t get_offset_of_printOptionsField_19() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___printOptionsField_19)); }
	inline CT_PrintOptions_tBF0437CD33D2F94344998C5F43108143353F1148 * get_printOptionsField_19() const { return ___printOptionsField_19; }
	inline CT_PrintOptions_tBF0437CD33D2F94344998C5F43108143353F1148 ** get_address_of_printOptionsField_19() { return &___printOptionsField_19; }
	inline void set_printOptionsField_19(CT_PrintOptions_tBF0437CD33D2F94344998C5F43108143353F1148 * value)
	{
		___printOptionsField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___printOptionsField_19), (void*)value);
	}

	inline static int32_t get_offset_of_pageMarginsField_20() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___pageMarginsField_20)); }
	inline CT_PageMargins_t48C935B82A4C55550510A8CF006E0817C13982E9 * get_pageMarginsField_20() const { return ___pageMarginsField_20; }
	inline CT_PageMargins_t48C935B82A4C55550510A8CF006E0817C13982E9 ** get_address_of_pageMarginsField_20() { return &___pageMarginsField_20; }
	inline void set_pageMarginsField_20(CT_PageMargins_t48C935B82A4C55550510A8CF006E0817C13982E9 * value)
	{
		___pageMarginsField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pageMarginsField_20), (void*)value);
	}

	inline static int32_t get_offset_of_pageSetupField_21() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___pageSetupField_21)); }
	inline CT_PageSetup_t136C6F167D4E7A2E2480A50CAD462FBE1D1715A5 * get_pageSetupField_21() const { return ___pageSetupField_21; }
	inline CT_PageSetup_t136C6F167D4E7A2E2480A50CAD462FBE1D1715A5 ** get_address_of_pageSetupField_21() { return &___pageSetupField_21; }
	inline void set_pageSetupField_21(CT_PageSetup_t136C6F167D4E7A2E2480A50CAD462FBE1D1715A5 * value)
	{
		___pageSetupField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pageSetupField_21), (void*)value);
	}

	inline static int32_t get_offset_of_headerFooterField_22() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___headerFooterField_22)); }
	inline CT_HeaderFooter_tE734591E584CBD00BD317DE44533E600FAAE9D2B * get_headerFooterField_22() const { return ___headerFooterField_22; }
	inline CT_HeaderFooter_tE734591E584CBD00BD317DE44533E600FAAE9D2B ** get_address_of_headerFooterField_22() { return &___headerFooterField_22; }
	inline void set_headerFooterField_22(CT_HeaderFooter_tE734591E584CBD00BD317DE44533E600FAAE9D2B * value)
	{
		___headerFooterField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___headerFooterField_22), (void*)value);
	}

	inline static int32_t get_offset_of_rowBreaksField_23() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___rowBreaksField_23)); }
	inline CT_PageBreak_tBA4435226581E50E78304255BED5D4775F85BBB8 * get_rowBreaksField_23() const { return ___rowBreaksField_23; }
	inline CT_PageBreak_tBA4435226581E50E78304255BED5D4775F85BBB8 ** get_address_of_rowBreaksField_23() { return &___rowBreaksField_23; }
	inline void set_rowBreaksField_23(CT_PageBreak_tBA4435226581E50E78304255BED5D4775F85BBB8 * value)
	{
		___rowBreaksField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rowBreaksField_23), (void*)value);
	}

	inline static int32_t get_offset_of_colBreaksField_24() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___colBreaksField_24)); }
	inline CT_PageBreak_tBA4435226581E50E78304255BED5D4775F85BBB8 * get_colBreaksField_24() const { return ___colBreaksField_24; }
	inline CT_PageBreak_tBA4435226581E50E78304255BED5D4775F85BBB8 ** get_address_of_colBreaksField_24() { return &___colBreaksField_24; }
	inline void set_colBreaksField_24(CT_PageBreak_tBA4435226581E50E78304255BED5D4775F85BBB8 * value)
	{
		___colBreaksField_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colBreaksField_24), (void*)value);
	}

	inline static int32_t get_offset_of_customPropertiesField_25() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___customPropertiesField_25)); }
	inline CT_CustomProperties_t7BE31A9D707BD3B4EE745A9AF575A41AE26E49BF * get_customPropertiesField_25() const { return ___customPropertiesField_25; }
	inline CT_CustomProperties_t7BE31A9D707BD3B4EE745A9AF575A41AE26E49BF ** get_address_of_customPropertiesField_25() { return &___customPropertiesField_25; }
	inline void set_customPropertiesField_25(CT_CustomProperties_t7BE31A9D707BD3B4EE745A9AF575A41AE26E49BF * value)
	{
		___customPropertiesField_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customPropertiesField_25), (void*)value);
	}

	inline static int32_t get_offset_of_cellWatchesField_26() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___cellWatchesField_26)); }
	inline CT_CellWatches_t01F7D7188788534CB4D6FA87507126AE3F3C467B * get_cellWatchesField_26() const { return ___cellWatchesField_26; }
	inline CT_CellWatches_t01F7D7188788534CB4D6FA87507126AE3F3C467B ** get_address_of_cellWatchesField_26() { return &___cellWatchesField_26; }
	inline void set_cellWatchesField_26(CT_CellWatches_t01F7D7188788534CB4D6FA87507126AE3F3C467B * value)
	{
		___cellWatchesField_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellWatchesField_26), (void*)value);
	}

	inline static int32_t get_offset_of_ignoredErrorsField_27() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___ignoredErrorsField_27)); }
	inline CT_IgnoredErrors_t5D0C0D432AA19CC3B66CB5F5BEA9F5009ABE599E * get_ignoredErrorsField_27() const { return ___ignoredErrorsField_27; }
	inline CT_IgnoredErrors_t5D0C0D432AA19CC3B66CB5F5BEA9F5009ABE599E ** get_address_of_ignoredErrorsField_27() { return &___ignoredErrorsField_27; }
	inline void set_ignoredErrorsField_27(CT_IgnoredErrors_t5D0C0D432AA19CC3B66CB5F5BEA9F5009ABE599E * value)
	{
		___ignoredErrorsField_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ignoredErrorsField_27), (void*)value);
	}

	inline static int32_t get_offset_of_smartTagsField_28() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___smartTagsField_28)); }
	inline CT_CellSmartTags_t848F74CABCB4CC4048AF8189644352B4999AD91E * get_smartTagsField_28() const { return ___smartTagsField_28; }
	inline CT_CellSmartTags_t848F74CABCB4CC4048AF8189644352B4999AD91E ** get_address_of_smartTagsField_28() { return &___smartTagsField_28; }
	inline void set_smartTagsField_28(CT_CellSmartTags_t848F74CABCB4CC4048AF8189644352B4999AD91E * value)
	{
		___smartTagsField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___smartTagsField_28), (void*)value);
	}

	inline static int32_t get_offset_of_drawingField_29() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___drawingField_29)); }
	inline CT_Drawing_tD2F6724924616C586D3810460A5E0962BCE3A0BB * get_drawingField_29() const { return ___drawingField_29; }
	inline CT_Drawing_tD2F6724924616C586D3810460A5E0962BCE3A0BB ** get_address_of_drawingField_29() { return &___drawingField_29; }
	inline void set_drawingField_29(CT_Drawing_tD2F6724924616C586D3810460A5E0962BCE3A0BB * value)
	{
		___drawingField_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___drawingField_29), (void*)value);
	}

	inline static int32_t get_offset_of_legacyDrawingField_30() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___legacyDrawingField_30)); }
	inline CT_LegacyDrawing_t37EC868D8930A4F935DB6358433328195ACFAC70 * get_legacyDrawingField_30() const { return ___legacyDrawingField_30; }
	inline CT_LegacyDrawing_t37EC868D8930A4F935DB6358433328195ACFAC70 ** get_address_of_legacyDrawingField_30() { return &___legacyDrawingField_30; }
	inline void set_legacyDrawingField_30(CT_LegacyDrawing_t37EC868D8930A4F935DB6358433328195ACFAC70 * value)
	{
		___legacyDrawingField_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___legacyDrawingField_30), (void*)value);
	}

	inline static int32_t get_offset_of_legacyDrawingHFField_31() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___legacyDrawingHFField_31)); }
	inline CT_LegacyDrawing_t37EC868D8930A4F935DB6358433328195ACFAC70 * get_legacyDrawingHFField_31() const { return ___legacyDrawingHFField_31; }
	inline CT_LegacyDrawing_t37EC868D8930A4F935DB6358433328195ACFAC70 ** get_address_of_legacyDrawingHFField_31() { return &___legacyDrawingHFField_31; }
	inline void set_legacyDrawingHFField_31(CT_LegacyDrawing_t37EC868D8930A4F935DB6358433328195ACFAC70 * value)
	{
		___legacyDrawingHFField_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___legacyDrawingHFField_31), (void*)value);
	}

	inline static int32_t get_offset_of_pictureField_32() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___pictureField_32)); }
	inline CT_SheetBackgroundPicture_tA51AFAD22D4F4C6CA5A208A29AF4586B25C56FD3 * get_pictureField_32() const { return ___pictureField_32; }
	inline CT_SheetBackgroundPicture_tA51AFAD22D4F4C6CA5A208A29AF4586B25C56FD3 ** get_address_of_pictureField_32() { return &___pictureField_32; }
	inline void set_pictureField_32(CT_SheetBackgroundPicture_tA51AFAD22D4F4C6CA5A208A29AF4586B25C56FD3 * value)
	{
		___pictureField_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pictureField_32), (void*)value);
	}

	inline static int32_t get_offset_of_oleObjectsField_33() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___oleObjectsField_33)); }
	inline CT_OleObjects_tD28B4F45856085620F2065D8441AAF8E4956415D * get_oleObjectsField_33() const { return ___oleObjectsField_33; }
	inline CT_OleObjects_tD28B4F45856085620F2065D8441AAF8E4956415D ** get_address_of_oleObjectsField_33() { return &___oleObjectsField_33; }
	inline void set_oleObjectsField_33(CT_OleObjects_tD28B4F45856085620F2065D8441AAF8E4956415D * value)
	{
		___oleObjectsField_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___oleObjectsField_33), (void*)value);
	}

	inline static int32_t get_offset_of_controlsField_34() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___controlsField_34)); }
	inline CT_Controls_t564AEB49B666E470DF52CDC90498C94EEE684B9A * get_controlsField_34() const { return ___controlsField_34; }
	inline CT_Controls_t564AEB49B666E470DF52CDC90498C94EEE684B9A ** get_address_of_controlsField_34() { return &___controlsField_34; }
	inline void set_controlsField_34(CT_Controls_t564AEB49B666E470DF52CDC90498C94EEE684B9A * value)
	{
		___controlsField_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controlsField_34), (void*)value);
	}

	inline static int32_t get_offset_of_webPublishItemsField_35() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___webPublishItemsField_35)); }
	inline CT_WebPublishItems_tE2398221E4BEA0EFC8BF2AAB546AD668BC9C3267 * get_webPublishItemsField_35() const { return ___webPublishItemsField_35; }
	inline CT_WebPublishItems_tE2398221E4BEA0EFC8BF2AAB546AD668BC9C3267 ** get_address_of_webPublishItemsField_35() { return &___webPublishItemsField_35; }
	inline void set_webPublishItemsField_35(CT_WebPublishItems_tE2398221E4BEA0EFC8BF2AAB546AD668BC9C3267 * value)
	{
		___webPublishItemsField_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___webPublishItemsField_35), (void*)value);
	}

	inline static int32_t get_offset_of_tablePartsField_36() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___tablePartsField_36)); }
	inline CT_TableParts_t78D1D9FFB376C6DECADD9D4FE55C2F180B8F7DA1 * get_tablePartsField_36() const { return ___tablePartsField_36; }
	inline CT_TableParts_t78D1D9FFB376C6DECADD9D4FE55C2F180B8F7DA1 ** get_address_of_tablePartsField_36() { return &___tablePartsField_36; }
	inline void set_tablePartsField_36(CT_TableParts_t78D1D9FFB376C6DECADD9D4FE55C2F180B8F7DA1 * value)
	{
		___tablePartsField_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tablePartsField_36), (void*)value);
	}

	inline static int32_t get_offset_of_extLstField_37() { return static_cast<int32_t>(offsetof(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF, ___extLstField_37)); }
	inline CT_ExtensionList_t79ED67739D16E8748F4B890D07A37591BB568F66 * get_extLstField_37() const { return ___extLstField_37; }
	inline CT_ExtensionList_t79ED67739D16E8748F4B890D07A37591BB568F66 ** get_address_of_extLstField_37() { return &___extLstField_37; }
	inline void set_extLstField_37(CT_ExtensionList_t79ED67739D16E8748F4B890D07A37591BB568F66 * value)
	{
		___extLstField_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extLstField_37), (void*)value);
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.CT_Xf
struct  CT_Xf_t1790BDF05B20998683A5649F037942CB892C80AB  : public RuntimeObject
{
public:
	// System.UInt32 NPOI.OpenXmlFormats.Spreadsheet.CT_Xf::numFmtIdField
	uint32_t ___numFmtIdField_0;
	// System.UInt32 NPOI.OpenXmlFormats.Spreadsheet.CT_Xf::xfIdField
	uint32_t ___xfIdField_1;

public:
	inline static int32_t get_offset_of_numFmtIdField_0() { return static_cast<int32_t>(offsetof(CT_Xf_t1790BDF05B20998683A5649F037942CB892C80AB, ___numFmtIdField_0)); }
	inline uint32_t get_numFmtIdField_0() const { return ___numFmtIdField_0; }
	inline uint32_t* get_address_of_numFmtIdField_0() { return &___numFmtIdField_0; }
	inline void set_numFmtIdField_0(uint32_t value)
	{
		___numFmtIdField_0 = value;
	}

	inline static int32_t get_offset_of_xfIdField_1() { return static_cast<int32_t>(offsetof(CT_Xf_t1790BDF05B20998683A5649F037942CB892C80AB, ___xfIdField_1)); }
	inline uint32_t get_xfIdField_1() const { return ___xfIdField_1; }
	inline uint32_t* get_address_of_xfIdField_1() { return &___xfIdField_1; }
	inline void set_xfIdField_1(uint32_t value)
	{
		___xfIdField_1 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.SstDocument
struct  SstDocument_t29C4C193FFCF0E33A36E3211726074BEDE7D8F81  : public RuntimeObject
{
public:
	// NPOI.OpenXmlFormats.Spreadsheet.CT_Sst NPOI.OpenXmlFormats.Spreadsheet.SstDocument::sst
	CT_Sst_tB80B6A0914663BCEE99DB793DA7F73354949D6A9 * ___sst_0;

public:
	inline static int32_t get_offset_of_sst_0() { return static_cast<int32_t>(offsetof(SstDocument_t29C4C193FFCF0E33A36E3211726074BEDE7D8F81, ___sst_0)); }
	inline CT_Sst_tB80B6A0914663BCEE99DB793DA7F73354949D6A9 * get_sst_0() const { return ___sst_0; }
	inline CT_Sst_tB80B6A0914663BCEE99DB793DA7F73354949D6A9 ** get_address_of_sst_0() { return &___sst_0; }
	inline void set_sst_0(CT_Sst_tB80B6A0914663BCEE99DB793DA7F73354949D6A9 * value)
	{
		___sst_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sst_0), (void*)value);
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.WorkbookDocument
struct  WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3  : public RuntimeObject
{
public:
	// NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook NPOI.OpenXmlFormats.Spreadsheet.WorkbookDocument::workbook
	CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * ___workbook_0;

public:
	inline static int32_t get_offset_of_workbook_0() { return static_cast<int32_t>(offsetof(WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3, ___workbook_0)); }
	inline CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * get_workbook_0() const { return ___workbook_0; }
	inline CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 ** get_address_of_workbook_0() { return &___workbook_0; }
	inline void set_workbook_0(CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * value)
	{
		___workbook_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___workbook_0), (void*)value);
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.WorksheetDocument
struct  WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F  : public RuntimeObject
{
public:
	// NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet NPOI.OpenXmlFormats.Spreadsheet.WorksheetDocument::sheet
	CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * ___sheet_0;

public:
	inline static int32_t get_offset_of_sheet_0() { return static_cast<int32_t>(offsetof(WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F, ___sheet_0)); }
	inline CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * get_sheet_0() const { return ___sheet_0; }
	inline CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF ** get_address_of_sheet_0() { return &___sheet_0; }
	inline void set_sheet_0(CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * value)
	{
		___sheet_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sheet_0), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.MarshalByRefObject
struct  MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identity_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Xml.Serialization.XmlSerializer
struct  XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Serialization.XmlSerializer::customSerializer
	bool ___customSerializer_4;
	// System.Xml.Serialization.XmlMapping System.Xml.Serialization.XmlSerializer::typeMapping
	XmlMapping_t98D53F12A90FEA32EB59D51D4461C16AE85978F9 * ___typeMapping_5;
	// System.Xml.Serialization.XmlSerializer_SerializerData System.Xml.Serialization.XmlSerializer::serializerData
	SerializerData_tE81861A5E031D40D9732A8CF036A091D10F3478D * ___serializerData_6;
	// System.Xml.Serialization.UnreferencedObjectEventHandler System.Xml.Serialization.XmlSerializer::onUnreferencedObject
	UnreferencedObjectEventHandler_t8C7AD11596BB1575262B62EE1128DABB07434988 * ___onUnreferencedObject_8;
	// System.Xml.Serialization.XmlAttributeEventHandler System.Xml.Serialization.XmlSerializer::onUnknownAttribute
	XmlAttributeEventHandler_tECA22B67D5D89D1A3EF50E02048CE6E7EC40C286 * ___onUnknownAttribute_9;
	// System.Xml.Serialization.XmlElementEventHandler System.Xml.Serialization.XmlSerializer::onUnknownElement
	XmlElementEventHandler_t5110F67CA4FDC0867D3457AAC06E2E6DADA1DE29 * ___onUnknownElement_10;
	// System.Xml.Serialization.XmlNodeEventHandler System.Xml.Serialization.XmlSerializer::onUnknownNode
	XmlNodeEventHandler_tF1C40439AD043BB08EEEAA54CC23FD03DF241563 * ___onUnknownNode_11;

public:
	inline static int32_t get_offset_of_customSerializer_4() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B, ___customSerializer_4)); }
	inline bool get_customSerializer_4() const { return ___customSerializer_4; }
	inline bool* get_address_of_customSerializer_4() { return &___customSerializer_4; }
	inline void set_customSerializer_4(bool value)
	{
		___customSerializer_4 = value;
	}

	inline static int32_t get_offset_of_typeMapping_5() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B, ___typeMapping_5)); }
	inline XmlMapping_t98D53F12A90FEA32EB59D51D4461C16AE85978F9 * get_typeMapping_5() const { return ___typeMapping_5; }
	inline XmlMapping_t98D53F12A90FEA32EB59D51D4461C16AE85978F9 ** get_address_of_typeMapping_5() { return &___typeMapping_5; }
	inline void set_typeMapping_5(XmlMapping_t98D53F12A90FEA32EB59D51D4461C16AE85978F9 * value)
	{
		___typeMapping_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeMapping_5), (void*)value);
	}

	inline static int32_t get_offset_of_serializerData_6() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B, ___serializerData_6)); }
	inline SerializerData_tE81861A5E031D40D9732A8CF036A091D10F3478D * get_serializerData_6() const { return ___serializerData_6; }
	inline SerializerData_tE81861A5E031D40D9732A8CF036A091D10F3478D ** get_address_of_serializerData_6() { return &___serializerData_6; }
	inline void set_serializerData_6(SerializerData_tE81861A5E031D40D9732A8CF036A091D10F3478D * value)
	{
		___serializerData_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializerData_6), (void*)value);
	}

	inline static int32_t get_offset_of_onUnreferencedObject_8() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B, ___onUnreferencedObject_8)); }
	inline UnreferencedObjectEventHandler_t8C7AD11596BB1575262B62EE1128DABB07434988 * get_onUnreferencedObject_8() const { return ___onUnreferencedObject_8; }
	inline UnreferencedObjectEventHandler_t8C7AD11596BB1575262B62EE1128DABB07434988 ** get_address_of_onUnreferencedObject_8() { return &___onUnreferencedObject_8; }
	inline void set_onUnreferencedObject_8(UnreferencedObjectEventHandler_t8C7AD11596BB1575262B62EE1128DABB07434988 * value)
	{
		___onUnreferencedObject_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUnreferencedObject_8), (void*)value);
	}

	inline static int32_t get_offset_of_onUnknownAttribute_9() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B, ___onUnknownAttribute_9)); }
	inline XmlAttributeEventHandler_tECA22B67D5D89D1A3EF50E02048CE6E7EC40C286 * get_onUnknownAttribute_9() const { return ___onUnknownAttribute_9; }
	inline XmlAttributeEventHandler_tECA22B67D5D89D1A3EF50E02048CE6E7EC40C286 ** get_address_of_onUnknownAttribute_9() { return &___onUnknownAttribute_9; }
	inline void set_onUnknownAttribute_9(XmlAttributeEventHandler_tECA22B67D5D89D1A3EF50E02048CE6E7EC40C286 * value)
	{
		___onUnknownAttribute_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUnknownAttribute_9), (void*)value);
	}

	inline static int32_t get_offset_of_onUnknownElement_10() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B, ___onUnknownElement_10)); }
	inline XmlElementEventHandler_t5110F67CA4FDC0867D3457AAC06E2E6DADA1DE29 * get_onUnknownElement_10() const { return ___onUnknownElement_10; }
	inline XmlElementEventHandler_t5110F67CA4FDC0867D3457AAC06E2E6DADA1DE29 ** get_address_of_onUnknownElement_10() { return &___onUnknownElement_10; }
	inline void set_onUnknownElement_10(XmlElementEventHandler_t5110F67CA4FDC0867D3457AAC06E2E6DADA1DE29 * value)
	{
		___onUnknownElement_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUnknownElement_10), (void*)value);
	}

	inline static int32_t get_offset_of_onUnknownNode_11() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B, ___onUnknownNode_11)); }
	inline XmlNodeEventHandler_tF1C40439AD043BB08EEEAA54CC23FD03DF241563 * get_onUnknownNode_11() const { return ___onUnknownNode_11; }
	inline XmlNodeEventHandler_tF1C40439AD043BB08EEEAA54CC23FD03DF241563 ** get_address_of_onUnknownNode_11() { return &___onUnknownNode_11; }
	inline void set_onUnknownNode_11(XmlNodeEventHandler_tF1C40439AD043BB08EEEAA54CC23FD03DF241563 * value)
	{
		___onUnknownNode_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUnknownNode_11), (void*)value);
	}
};

struct XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_StaticFields
{
public:
	// System.Int32 System.Xml.Serialization.XmlSerializer::generationThreshold
	int32_t ___generationThreshold_0;
	// System.Boolean System.Xml.Serialization.XmlSerializer::backgroundGeneration
	bool ___backgroundGeneration_1;
	// System.Boolean System.Xml.Serialization.XmlSerializer::deleteTempFiles
	bool ___deleteTempFiles_2;
	// System.Boolean System.Xml.Serialization.XmlSerializer::generatorFallback
	bool ___generatorFallback_3;
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializer::serializerTypes
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___serializerTypes_7;
	// System.Text.Encoding System.Xml.Serialization.XmlSerializer::DefaultEncoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___DefaultEncoding_12;

public:
	inline static int32_t get_offset_of_generationThreshold_0() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_StaticFields, ___generationThreshold_0)); }
	inline int32_t get_generationThreshold_0() const { return ___generationThreshold_0; }
	inline int32_t* get_address_of_generationThreshold_0() { return &___generationThreshold_0; }
	inline void set_generationThreshold_0(int32_t value)
	{
		___generationThreshold_0 = value;
	}

	inline static int32_t get_offset_of_backgroundGeneration_1() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_StaticFields, ___backgroundGeneration_1)); }
	inline bool get_backgroundGeneration_1() const { return ___backgroundGeneration_1; }
	inline bool* get_address_of_backgroundGeneration_1() { return &___backgroundGeneration_1; }
	inline void set_backgroundGeneration_1(bool value)
	{
		___backgroundGeneration_1 = value;
	}

	inline static int32_t get_offset_of_deleteTempFiles_2() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_StaticFields, ___deleteTempFiles_2)); }
	inline bool get_deleteTempFiles_2() const { return ___deleteTempFiles_2; }
	inline bool* get_address_of_deleteTempFiles_2() { return &___deleteTempFiles_2; }
	inline void set_deleteTempFiles_2(bool value)
	{
		___deleteTempFiles_2 = value;
	}

	inline static int32_t get_offset_of_generatorFallback_3() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_StaticFields, ___generatorFallback_3)); }
	inline bool get_generatorFallback_3() const { return ___generatorFallback_3; }
	inline bool* get_address_of_generatorFallback_3() { return &___generatorFallback_3; }
	inline void set_generatorFallback_3(bool value)
	{
		___generatorFallback_3 = value;
	}

	inline static int32_t get_offset_of_serializerTypes_7() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_StaticFields, ___serializerTypes_7)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_serializerTypes_7() const { return ___serializerTypes_7; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_serializerTypes_7() { return &___serializerTypes_7; }
	inline void set_serializerTypes_7(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___serializerTypes_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializerTypes_7), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultEncoding_12() { return static_cast<int32_t>(offsetof(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_StaticFields, ___DefaultEncoding_12)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_DefaultEncoding_12() const { return ___DefaultEncoding_12; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_DefaultEncoding_12() { return &___DefaultEncoding_12; }
	inline void set_DefaultEncoding_12(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___DefaultEncoding_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultEncoding_12), (void*)value);
	}
};


// System.Xml.Serialization.XmlSerializerNamespaces
struct  XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.XmlSerializerNamespaces::namespaces
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5, ___namespaces_0)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_namespaces_0() const { return ___namespaces_0; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___namespaces_0), (void*)value);
	}
};


// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager_NamespaceDeclaration[] System.Xml.XmlNamespaceManager::nsdecls
	NamespaceDeclarationU5BU5D_t4D11B362C1D22E79294A2F9CB4710B02ED6D03F5* ___nsdecls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::lastDecl
	int32_t ___lastDecl_1;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t5A8AA505CA799E0DC25E9815E4106817D2E1E280 * ___nameTable_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopeId
	int32_t ___scopeId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::hashTable
	Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * ___hashTable_4;
	// System.Boolean System.Xml.XmlNamespaceManager::useHashtable
	bool ___useHashtable_5;
	// System.String System.Xml.XmlNamespaceManager::xml
	String_t* ___xml_6;
	// System.String System.Xml.XmlNamespaceManager::xmlNs
	String_t* ___xmlNs_7;

public:
	inline static int32_t get_offset_of_nsdecls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465, ___nsdecls_0)); }
	inline NamespaceDeclarationU5BU5D_t4D11B362C1D22E79294A2F9CB4710B02ED6D03F5* get_nsdecls_0() const { return ___nsdecls_0; }
	inline NamespaceDeclarationU5BU5D_t4D11B362C1D22E79294A2F9CB4710B02ED6D03F5** get_address_of_nsdecls_0() { return &___nsdecls_0; }
	inline void set_nsdecls_0(NamespaceDeclarationU5BU5D_t4D11B362C1D22E79294A2F9CB4710B02ED6D03F5* value)
	{
		___nsdecls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nsdecls_0), (void*)value);
	}

	inline static int32_t get_offset_of_lastDecl_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465, ___lastDecl_1)); }
	inline int32_t get_lastDecl_1() const { return ___lastDecl_1; }
	inline int32_t* get_address_of_lastDecl_1() { return &___lastDecl_1; }
	inline void set_lastDecl_1(int32_t value)
	{
		___lastDecl_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465, ___nameTable_2)); }
	inline XmlNameTable_t5A8AA505CA799E0DC25E9815E4106817D2E1E280 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t5A8AA505CA799E0DC25E9815E4106817D2E1E280 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t5A8AA505CA799E0DC25E9815E4106817D2E1E280 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameTable_2), (void*)value);
	}

	inline static int32_t get_offset_of_scopeId_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465, ___scopeId_3)); }
	inline int32_t get_scopeId_3() const { return ___scopeId_3; }
	inline int32_t* get_address_of_scopeId_3() { return &___scopeId_3; }
	inline void set_scopeId_3(int32_t value)
	{
		___scopeId_3 = value;
	}

	inline static int32_t get_offset_of_hashTable_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465, ___hashTable_4)); }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * get_hashTable_4() const { return ___hashTable_4; }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 ** get_address_of_hashTable_4() { return &___hashTable_4; }
	inline void set_hashTable_4(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * value)
	{
		___hashTable_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hashTable_4), (void*)value);
	}

	inline static int32_t get_offset_of_useHashtable_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465, ___useHashtable_5)); }
	inline bool get_useHashtable_5() const { return ___useHashtable_5; }
	inline bool* get_address_of_useHashtable_5() { return &___useHashtable_5; }
	inline void set_useHashtable_5(bool value)
	{
		___useHashtable_5 = value;
	}

	inline static int32_t get_offset_of_xml_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465, ___xml_6)); }
	inline String_t* get_xml_6() const { return ___xml_6; }
	inline String_t** get_address_of_xml_6() { return &___xml_6; }
	inline void set_xml_6(String_t* value)
	{
		___xml_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xml_6), (void*)value);
	}

	inline static int32_t get_offset_of_xmlNs_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465, ___xmlNs_7)); }
	inline String_t* get_xmlNs_7() const { return ___xmlNs_7; }
	inline String_t** get_address_of_xmlNs_7() { return &___xmlNs_7; }
	inline void set_xmlNs_7(String_t* value)
	{
		___xmlNs_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xmlNs_7), (void*)value);
	}
};


// System.Xml.XmlNode
struct  XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * ___parentNode_0;

public:
	inline static int32_t get_offset_of_parentNode_0() { return static_cast<int32_t>(offsetof(XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1, ___parentNode_0)); }
	inline XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * get_parentNode_0() const { return ___parentNode_0; }
	inline XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 ** get_address_of_parentNode_0() { return &___parentNode_0; }
	inline void set_parentNode_0(XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * value)
	{
		___parentNode_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parentNode_0), (void*)value);
	}
};


// System.Xml.XmlQualifiedName
struct  XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlQualifiedName::name
	String_t* ___name_1;
	// System.String System.Xml.XmlQualifiedName::ns
	String_t* ___ns_2;
	// System.Int32 System.Xml.XmlQualifiedName::hash
	int32_t ___hash_3;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_1), (void*)value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ns_2), (void*)value);
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905, ___hash_3)); }
	inline int32_t get_hash_3() const { return ___hash_3; }
	inline int32_t* get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(int32_t value)
	{
		___hash_3 = value;
	}
};

struct XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905_StaticFields
{
public:
	// System.Xml.XmlQualifiedName_HashCodeOfStringDelegate System.Xml.XmlQualifiedName::hashCodeDelegate
	HashCodeOfStringDelegate_tAE9DAB0A55A64F35CCEE05D71856BAAF6C0B668E * ___hashCodeDelegate_0;
	// System.Xml.XmlQualifiedName System.Xml.XmlQualifiedName::Empty
	XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * ___Empty_4;

public:
	inline static int32_t get_offset_of_hashCodeDelegate_0() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905_StaticFields, ___hashCodeDelegate_0)); }
	inline HashCodeOfStringDelegate_tAE9DAB0A55A64F35CCEE05D71856BAAF6C0B668E * get_hashCodeDelegate_0() const { return ___hashCodeDelegate_0; }
	inline HashCodeOfStringDelegate_tAE9DAB0A55A64F35CCEE05D71856BAAF6C0B668E ** get_address_of_hashCodeDelegate_0() { return &___hashCodeDelegate_0; }
	inline void set_hashCodeDelegate_0(HashCodeOfStringDelegate_tAE9DAB0A55A64F35CCEE05D71856BAAF6C0B668E * value)
	{
		___hashCodeDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hashCodeDelegate_0), (void*)value);
	}

	inline static int32_t get_offset_of_Empty_4() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905_StaticFields, ___Empty_4)); }
	inline XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * get_Empty_4() const { return ___Empty_4; }
	inline XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 ** get_address_of_Empty_4() { return &___Empty_4; }
	inline void set_Empty_4(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * value)
	{
		___Empty_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_4), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IO.Stream
struct  Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB  : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____activeReadWriteTask_2), (void*)value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____asyncActiveSemaphore_3), (void*)value);
	}
};

struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_StaticFields, ___Null_1)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_Null_1() const { return ___Null_1; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_1), (void*)value);
	}
};


// System.IO.TextWriter
struct  TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643  : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___CoreNewLine_9;
	// System.IFormatProvider System.IO.TextWriter::InternalFormatProvider
	RuntimeObject* ___InternalFormatProvider_10;

public:
	inline static int32_t get_offset_of_CoreNewLine_9() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643, ___CoreNewLine_9)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_CoreNewLine_9() const { return ___CoreNewLine_9; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_CoreNewLine_9() { return &___CoreNewLine_9; }
	inline void set_CoreNewLine_9(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___CoreNewLine_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CoreNewLine_9), (void*)value);
	}

	inline static int32_t get_offset_of_InternalFormatProvider_10() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643, ___InternalFormatProvider_10)); }
	inline RuntimeObject* get_InternalFormatProvider_10() const { return ___InternalFormatProvider_10; }
	inline RuntimeObject** get_address_of_InternalFormatProvider_10() { return &___InternalFormatProvider_10; }
	inline void set_InternalFormatProvider_10(RuntimeObject* value)
	{
		___InternalFormatProvider_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InternalFormatProvider_10), (void*)value);
	}
};

struct TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___Null_1;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteCharDelegate_2;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteStringDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteStringDelegate_3;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteCharArrayRangeDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteCharArrayRangeDelegate_4;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteLineCharDelegate_5;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineStringDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteLineStringDelegate_6;
	// System.Action`1<System.Object> System.IO.TextWriter::_WriteLineCharArrayRangeDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____WriteLineCharArrayRangeDelegate_7;
	// System.Action`1<System.Object> System.IO.TextWriter::_FlushDelegate
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ____FlushDelegate_8;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ___Null_1)); }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * get_Null_1() const { return ___Null_1; }
	inline TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_1), (void*)value);
	}

	inline static int32_t get_offset_of__WriteCharDelegate_2() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteCharDelegate_2)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteCharDelegate_2() const { return ____WriteCharDelegate_2; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteCharDelegate_2() { return &____WriteCharDelegate_2; }
	inline void set__WriteCharDelegate_2(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteCharDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteCharDelegate_2), (void*)value);
	}

	inline static int32_t get_offset_of__WriteStringDelegate_3() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteStringDelegate_3)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteStringDelegate_3() const { return ____WriteStringDelegate_3; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteStringDelegate_3() { return &____WriteStringDelegate_3; }
	inline void set__WriteStringDelegate_3(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteStringDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteStringDelegate_3), (void*)value);
	}

	inline static int32_t get_offset_of__WriteCharArrayRangeDelegate_4() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteCharArrayRangeDelegate_4)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteCharArrayRangeDelegate_4() const { return ____WriteCharArrayRangeDelegate_4; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteCharArrayRangeDelegate_4() { return &____WriteCharArrayRangeDelegate_4; }
	inline void set__WriteCharArrayRangeDelegate_4(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteCharArrayRangeDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteCharArrayRangeDelegate_4), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineCharDelegate_5() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteLineCharDelegate_5)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteLineCharDelegate_5() const { return ____WriteLineCharDelegate_5; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteLineCharDelegate_5() { return &____WriteLineCharDelegate_5; }
	inline void set__WriteLineCharDelegate_5(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteLineCharDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineCharDelegate_5), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineStringDelegate_6() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteLineStringDelegate_6)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteLineStringDelegate_6() const { return ____WriteLineStringDelegate_6; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteLineStringDelegate_6() { return &____WriteLineStringDelegate_6; }
	inline void set__WriteLineStringDelegate_6(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteLineStringDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineStringDelegate_6), (void*)value);
	}

	inline static int32_t get_offset_of__WriteLineCharArrayRangeDelegate_7() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____WriteLineCharArrayRangeDelegate_7)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__WriteLineCharArrayRangeDelegate_7() const { return ____WriteLineCharArrayRangeDelegate_7; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__WriteLineCharArrayRangeDelegate_7() { return &____WriteLineCharArrayRangeDelegate_7; }
	inline void set__WriteLineCharArrayRangeDelegate_7(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____WriteLineCharArrayRangeDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WriteLineCharArrayRangeDelegate_7), (void*)value);
	}

	inline static int32_t get_offset_of__FlushDelegate_8() { return static_cast<int32_t>(offsetof(TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643_StaticFields, ____FlushDelegate_8)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get__FlushDelegate_8() const { return ____FlushDelegate_8; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of__FlushDelegate_8() { return &____FlushDelegate_8; }
	inline void set__FlushDelegate_8(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		____FlushDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____FlushDelegate_8), (void*)value);
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.UInt32
struct  UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Xml.XmlDocument
struct  XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F  : public XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1
{
public:
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_tE66EA5FEAE495F36B03F697C51E4815D4B5D1D59 * ___implementation_1;
	// System.Xml.DomNameTable System.Xml.XmlDocument::domNameTable
	DomNameTable_tE88EEF20D313A83C002E18FFF7688CC74A2D1CAB * ___domNameTable_2;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastChild
	XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * ___lastChild_3;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocument::entities
	XmlNamedNodeMap_t7554E5F015FED975E1E8B075F57EBA65773CF771 * ___entities_4;
	// System.Collections.Hashtable System.Xml.XmlDocument::htElementIdMap
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___htElementIdMap_5;
	// System.Collections.Hashtable System.Xml.XmlDocument::htElementIDAttrDecl
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___htElementIDAttrDecl_6;
	// System.Xml.Schema.SchemaInfo System.Xml.XmlDocument::schemaInfo
	SchemaInfo_tDED785F47546ED3F5EEC6EA77577917420BF3BF5 * ___schemaInfo_7;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlDocument::schemas
	XmlSchemaSet_t7CE09F2ECFA8C5EB28DF4724C87EC50B539699E0 * ___schemas_8;
	// System.Boolean System.Xml.XmlDocument::reportValidity
	bool ___reportValidity_9;
	// System.Boolean System.Xml.XmlDocument::actualLoadingStatus
	bool ___actualLoadingStatus_10;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeInsertingDelegate
	XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * ___onNodeInsertingDelegate_11;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeInsertedDelegate
	XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * ___onNodeInsertedDelegate_12;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeRemovingDelegate
	XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * ___onNodeRemovingDelegate_13;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeRemovedDelegate
	XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * ___onNodeRemovedDelegate_14;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeChangingDelegate
	XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * ___onNodeChangingDelegate_15;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeChangedDelegate
	XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * ___onNodeChangedDelegate_16;
	// System.Boolean System.Xml.XmlDocument::fEntRefNodesPresent
	bool ___fEntRefNodesPresent_17;
	// System.Boolean System.Xml.XmlDocument::fCDataNodesPresent
	bool ___fCDataNodesPresent_18;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_19;
	// System.Boolean System.Xml.XmlDocument::isLoading
	bool ___isLoading_20;
	// System.String System.Xml.XmlDocument::strDocumentName
	String_t* ___strDocumentName_21;
	// System.String System.Xml.XmlDocument::strDocumentFragmentName
	String_t* ___strDocumentFragmentName_22;
	// System.String System.Xml.XmlDocument::strCommentName
	String_t* ___strCommentName_23;
	// System.String System.Xml.XmlDocument::strTextName
	String_t* ___strTextName_24;
	// System.String System.Xml.XmlDocument::strCDataSectionName
	String_t* ___strCDataSectionName_25;
	// System.String System.Xml.XmlDocument::strEntityName
	String_t* ___strEntityName_26;
	// System.String System.Xml.XmlDocument::strID
	String_t* ___strID_27;
	// System.String System.Xml.XmlDocument::strXmlns
	String_t* ___strXmlns_28;
	// System.String System.Xml.XmlDocument::strXml
	String_t* ___strXml_29;
	// System.String System.Xml.XmlDocument::strSpace
	String_t* ___strSpace_30;
	// System.String System.Xml.XmlDocument::strLang
	String_t* ___strLang_31;
	// System.String System.Xml.XmlDocument::strEmpty
	String_t* ___strEmpty_32;
	// System.String System.Xml.XmlDocument::strNonSignificantWhitespaceName
	String_t* ___strNonSignificantWhitespaceName_33;
	// System.String System.Xml.XmlDocument::strSignificantWhitespaceName
	String_t* ___strSignificantWhitespaceName_34;
	// System.String System.Xml.XmlDocument::strReservedXmlns
	String_t* ___strReservedXmlns_35;
	// System.String System.Xml.XmlDocument::strReservedXml
	String_t* ___strReservedXml_36;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_37;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_t7666FB44FF30D5CE53CC8DD913B2A8D2BD74343A * ___resolver_38;
	// System.Boolean System.Xml.XmlDocument::bSetResolver
	bool ___bSetResolver_39;
	// System.Object System.Xml.XmlDocument::objLock
	RuntimeObject * ___objLock_40;
	// System.Xml.XmlAttribute System.Xml.XmlDocument::namespaceXml
	XmlAttribute_t3F58A4BDFB486D0E610E4003E54A89BCCB65AB6D * ___namespaceXml_41;

public:
	inline static int32_t get_offset_of_implementation_1() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___implementation_1)); }
	inline XmlImplementation_tE66EA5FEAE495F36B03F697C51E4815D4B5D1D59 * get_implementation_1() const { return ___implementation_1; }
	inline XmlImplementation_tE66EA5FEAE495F36B03F697C51E4815D4B5D1D59 ** get_address_of_implementation_1() { return &___implementation_1; }
	inline void set_implementation_1(XmlImplementation_tE66EA5FEAE495F36B03F697C51E4815D4B5D1D59 * value)
	{
		___implementation_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___implementation_1), (void*)value);
	}

	inline static int32_t get_offset_of_domNameTable_2() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___domNameTable_2)); }
	inline DomNameTable_tE88EEF20D313A83C002E18FFF7688CC74A2D1CAB * get_domNameTable_2() const { return ___domNameTable_2; }
	inline DomNameTable_tE88EEF20D313A83C002E18FFF7688CC74A2D1CAB ** get_address_of_domNameTable_2() { return &___domNameTable_2; }
	inline void set_domNameTable_2(DomNameTable_tE88EEF20D313A83C002E18FFF7688CC74A2D1CAB * value)
	{
		___domNameTable_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___domNameTable_2), (void*)value);
	}

	inline static int32_t get_offset_of_lastChild_3() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___lastChild_3)); }
	inline XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * get_lastChild_3() const { return ___lastChild_3; }
	inline XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 ** get_address_of_lastChild_3() { return &___lastChild_3; }
	inline void set_lastChild_3(XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * value)
	{
		___lastChild_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastChild_3), (void*)value);
	}

	inline static int32_t get_offset_of_entities_4() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___entities_4)); }
	inline XmlNamedNodeMap_t7554E5F015FED975E1E8B075F57EBA65773CF771 * get_entities_4() const { return ___entities_4; }
	inline XmlNamedNodeMap_t7554E5F015FED975E1E8B075F57EBA65773CF771 ** get_address_of_entities_4() { return &___entities_4; }
	inline void set_entities_4(XmlNamedNodeMap_t7554E5F015FED975E1E8B075F57EBA65773CF771 * value)
	{
		___entities_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entities_4), (void*)value);
	}

	inline static int32_t get_offset_of_htElementIdMap_5() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___htElementIdMap_5)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_htElementIdMap_5() const { return ___htElementIdMap_5; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_htElementIdMap_5() { return &___htElementIdMap_5; }
	inline void set_htElementIdMap_5(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___htElementIdMap_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___htElementIdMap_5), (void*)value);
	}

	inline static int32_t get_offset_of_htElementIDAttrDecl_6() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___htElementIDAttrDecl_6)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_htElementIDAttrDecl_6() const { return ___htElementIDAttrDecl_6; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_htElementIDAttrDecl_6() { return &___htElementIDAttrDecl_6; }
	inline void set_htElementIDAttrDecl_6(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___htElementIDAttrDecl_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___htElementIDAttrDecl_6), (void*)value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___schemaInfo_7)); }
	inline SchemaInfo_tDED785F47546ED3F5EEC6EA77577917420BF3BF5 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_tDED785F47546ED3F5EEC6EA77577917420BF3BF5 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_tDED785F47546ED3F5EEC6EA77577917420BF3BF5 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___schemaInfo_7), (void*)value);
	}

	inline static int32_t get_offset_of_schemas_8() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___schemas_8)); }
	inline XmlSchemaSet_t7CE09F2ECFA8C5EB28DF4724C87EC50B539699E0 * get_schemas_8() const { return ___schemas_8; }
	inline XmlSchemaSet_t7CE09F2ECFA8C5EB28DF4724C87EC50B539699E0 ** get_address_of_schemas_8() { return &___schemas_8; }
	inline void set_schemas_8(XmlSchemaSet_t7CE09F2ECFA8C5EB28DF4724C87EC50B539699E0 * value)
	{
		___schemas_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___schemas_8), (void*)value);
	}

	inline static int32_t get_offset_of_reportValidity_9() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___reportValidity_9)); }
	inline bool get_reportValidity_9() const { return ___reportValidity_9; }
	inline bool* get_address_of_reportValidity_9() { return &___reportValidity_9; }
	inline void set_reportValidity_9(bool value)
	{
		___reportValidity_9 = value;
	}

	inline static int32_t get_offset_of_actualLoadingStatus_10() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___actualLoadingStatus_10)); }
	inline bool get_actualLoadingStatus_10() const { return ___actualLoadingStatus_10; }
	inline bool* get_address_of_actualLoadingStatus_10() { return &___actualLoadingStatus_10; }
	inline void set_actualLoadingStatus_10(bool value)
	{
		___actualLoadingStatus_10 = value;
	}

	inline static int32_t get_offset_of_onNodeInsertingDelegate_11() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___onNodeInsertingDelegate_11)); }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * get_onNodeInsertingDelegate_11() const { return ___onNodeInsertingDelegate_11; }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 ** get_address_of_onNodeInsertingDelegate_11() { return &___onNodeInsertingDelegate_11; }
	inline void set_onNodeInsertingDelegate_11(XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * value)
	{
		___onNodeInsertingDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeInsertingDelegate_11), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeInsertedDelegate_12() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___onNodeInsertedDelegate_12)); }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * get_onNodeInsertedDelegate_12() const { return ___onNodeInsertedDelegate_12; }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 ** get_address_of_onNodeInsertedDelegate_12() { return &___onNodeInsertedDelegate_12; }
	inline void set_onNodeInsertedDelegate_12(XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * value)
	{
		___onNodeInsertedDelegate_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeInsertedDelegate_12), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeRemovingDelegate_13() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___onNodeRemovingDelegate_13)); }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * get_onNodeRemovingDelegate_13() const { return ___onNodeRemovingDelegate_13; }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 ** get_address_of_onNodeRemovingDelegate_13() { return &___onNodeRemovingDelegate_13; }
	inline void set_onNodeRemovingDelegate_13(XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * value)
	{
		___onNodeRemovingDelegate_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeRemovingDelegate_13), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeRemovedDelegate_14() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___onNodeRemovedDelegate_14)); }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * get_onNodeRemovedDelegate_14() const { return ___onNodeRemovedDelegate_14; }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 ** get_address_of_onNodeRemovedDelegate_14() { return &___onNodeRemovedDelegate_14; }
	inline void set_onNodeRemovedDelegate_14(XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * value)
	{
		___onNodeRemovedDelegate_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeRemovedDelegate_14), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeChangingDelegate_15() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___onNodeChangingDelegate_15)); }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * get_onNodeChangingDelegate_15() const { return ___onNodeChangingDelegate_15; }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 ** get_address_of_onNodeChangingDelegate_15() { return &___onNodeChangingDelegate_15; }
	inline void set_onNodeChangingDelegate_15(XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * value)
	{
		___onNodeChangingDelegate_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeChangingDelegate_15), (void*)value);
	}

	inline static int32_t get_offset_of_onNodeChangedDelegate_16() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___onNodeChangedDelegate_16)); }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * get_onNodeChangedDelegate_16() const { return ___onNodeChangedDelegate_16; }
	inline XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 ** get_address_of_onNodeChangedDelegate_16() { return &___onNodeChangedDelegate_16; }
	inline void set_onNodeChangedDelegate_16(XmlNodeChangedEventHandler_tA0D165ACE065C3934721B68C6762B79372D7ECE9 * value)
	{
		___onNodeChangedDelegate_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onNodeChangedDelegate_16), (void*)value);
	}

	inline static int32_t get_offset_of_fEntRefNodesPresent_17() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___fEntRefNodesPresent_17)); }
	inline bool get_fEntRefNodesPresent_17() const { return ___fEntRefNodesPresent_17; }
	inline bool* get_address_of_fEntRefNodesPresent_17() { return &___fEntRefNodesPresent_17; }
	inline void set_fEntRefNodesPresent_17(bool value)
	{
		___fEntRefNodesPresent_17 = value;
	}

	inline static int32_t get_offset_of_fCDataNodesPresent_18() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___fCDataNodesPresent_18)); }
	inline bool get_fCDataNodesPresent_18() const { return ___fCDataNodesPresent_18; }
	inline bool* get_address_of_fCDataNodesPresent_18() { return &___fCDataNodesPresent_18; }
	inline void set_fCDataNodesPresent_18(bool value)
	{
		___fCDataNodesPresent_18 = value;
	}

	inline static int32_t get_offset_of_preserveWhitespace_19() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___preserveWhitespace_19)); }
	inline bool get_preserveWhitespace_19() const { return ___preserveWhitespace_19; }
	inline bool* get_address_of_preserveWhitespace_19() { return &___preserveWhitespace_19; }
	inline void set_preserveWhitespace_19(bool value)
	{
		___preserveWhitespace_19 = value;
	}

	inline static int32_t get_offset_of_isLoading_20() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___isLoading_20)); }
	inline bool get_isLoading_20() const { return ___isLoading_20; }
	inline bool* get_address_of_isLoading_20() { return &___isLoading_20; }
	inline void set_isLoading_20(bool value)
	{
		___isLoading_20 = value;
	}

	inline static int32_t get_offset_of_strDocumentName_21() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strDocumentName_21)); }
	inline String_t* get_strDocumentName_21() const { return ___strDocumentName_21; }
	inline String_t** get_address_of_strDocumentName_21() { return &___strDocumentName_21; }
	inline void set_strDocumentName_21(String_t* value)
	{
		___strDocumentName_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strDocumentName_21), (void*)value);
	}

	inline static int32_t get_offset_of_strDocumentFragmentName_22() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strDocumentFragmentName_22)); }
	inline String_t* get_strDocumentFragmentName_22() const { return ___strDocumentFragmentName_22; }
	inline String_t** get_address_of_strDocumentFragmentName_22() { return &___strDocumentFragmentName_22; }
	inline void set_strDocumentFragmentName_22(String_t* value)
	{
		___strDocumentFragmentName_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strDocumentFragmentName_22), (void*)value);
	}

	inline static int32_t get_offset_of_strCommentName_23() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strCommentName_23)); }
	inline String_t* get_strCommentName_23() const { return ___strCommentName_23; }
	inline String_t** get_address_of_strCommentName_23() { return &___strCommentName_23; }
	inline void set_strCommentName_23(String_t* value)
	{
		___strCommentName_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strCommentName_23), (void*)value);
	}

	inline static int32_t get_offset_of_strTextName_24() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strTextName_24)); }
	inline String_t* get_strTextName_24() const { return ___strTextName_24; }
	inline String_t** get_address_of_strTextName_24() { return &___strTextName_24; }
	inline void set_strTextName_24(String_t* value)
	{
		___strTextName_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strTextName_24), (void*)value);
	}

	inline static int32_t get_offset_of_strCDataSectionName_25() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strCDataSectionName_25)); }
	inline String_t* get_strCDataSectionName_25() const { return ___strCDataSectionName_25; }
	inline String_t** get_address_of_strCDataSectionName_25() { return &___strCDataSectionName_25; }
	inline void set_strCDataSectionName_25(String_t* value)
	{
		___strCDataSectionName_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strCDataSectionName_25), (void*)value);
	}

	inline static int32_t get_offset_of_strEntityName_26() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strEntityName_26)); }
	inline String_t* get_strEntityName_26() const { return ___strEntityName_26; }
	inline String_t** get_address_of_strEntityName_26() { return &___strEntityName_26; }
	inline void set_strEntityName_26(String_t* value)
	{
		___strEntityName_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strEntityName_26), (void*)value);
	}

	inline static int32_t get_offset_of_strID_27() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strID_27)); }
	inline String_t* get_strID_27() const { return ___strID_27; }
	inline String_t** get_address_of_strID_27() { return &___strID_27; }
	inline void set_strID_27(String_t* value)
	{
		___strID_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strID_27), (void*)value);
	}

	inline static int32_t get_offset_of_strXmlns_28() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strXmlns_28)); }
	inline String_t* get_strXmlns_28() const { return ___strXmlns_28; }
	inline String_t** get_address_of_strXmlns_28() { return &___strXmlns_28; }
	inline void set_strXmlns_28(String_t* value)
	{
		___strXmlns_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strXmlns_28), (void*)value);
	}

	inline static int32_t get_offset_of_strXml_29() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strXml_29)); }
	inline String_t* get_strXml_29() const { return ___strXml_29; }
	inline String_t** get_address_of_strXml_29() { return &___strXml_29; }
	inline void set_strXml_29(String_t* value)
	{
		___strXml_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strXml_29), (void*)value);
	}

	inline static int32_t get_offset_of_strSpace_30() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strSpace_30)); }
	inline String_t* get_strSpace_30() const { return ___strSpace_30; }
	inline String_t** get_address_of_strSpace_30() { return &___strSpace_30; }
	inline void set_strSpace_30(String_t* value)
	{
		___strSpace_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strSpace_30), (void*)value);
	}

	inline static int32_t get_offset_of_strLang_31() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strLang_31)); }
	inline String_t* get_strLang_31() const { return ___strLang_31; }
	inline String_t** get_address_of_strLang_31() { return &___strLang_31; }
	inline void set_strLang_31(String_t* value)
	{
		___strLang_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strLang_31), (void*)value);
	}

	inline static int32_t get_offset_of_strEmpty_32() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strEmpty_32)); }
	inline String_t* get_strEmpty_32() const { return ___strEmpty_32; }
	inline String_t** get_address_of_strEmpty_32() { return &___strEmpty_32; }
	inline void set_strEmpty_32(String_t* value)
	{
		___strEmpty_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strEmpty_32), (void*)value);
	}

	inline static int32_t get_offset_of_strNonSignificantWhitespaceName_33() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strNonSignificantWhitespaceName_33)); }
	inline String_t* get_strNonSignificantWhitespaceName_33() const { return ___strNonSignificantWhitespaceName_33; }
	inline String_t** get_address_of_strNonSignificantWhitespaceName_33() { return &___strNonSignificantWhitespaceName_33; }
	inline void set_strNonSignificantWhitespaceName_33(String_t* value)
	{
		___strNonSignificantWhitespaceName_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strNonSignificantWhitespaceName_33), (void*)value);
	}

	inline static int32_t get_offset_of_strSignificantWhitespaceName_34() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strSignificantWhitespaceName_34)); }
	inline String_t* get_strSignificantWhitespaceName_34() const { return ___strSignificantWhitespaceName_34; }
	inline String_t** get_address_of_strSignificantWhitespaceName_34() { return &___strSignificantWhitespaceName_34; }
	inline void set_strSignificantWhitespaceName_34(String_t* value)
	{
		___strSignificantWhitespaceName_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strSignificantWhitespaceName_34), (void*)value);
	}

	inline static int32_t get_offset_of_strReservedXmlns_35() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strReservedXmlns_35)); }
	inline String_t* get_strReservedXmlns_35() const { return ___strReservedXmlns_35; }
	inline String_t** get_address_of_strReservedXmlns_35() { return &___strReservedXmlns_35; }
	inline void set_strReservedXmlns_35(String_t* value)
	{
		___strReservedXmlns_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strReservedXmlns_35), (void*)value);
	}

	inline static int32_t get_offset_of_strReservedXml_36() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___strReservedXml_36)); }
	inline String_t* get_strReservedXml_36() const { return ___strReservedXml_36; }
	inline String_t** get_address_of_strReservedXml_36() { return &___strReservedXml_36; }
	inline void set_strReservedXml_36(String_t* value)
	{
		___strReservedXml_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___strReservedXml_36), (void*)value);
	}

	inline static int32_t get_offset_of_baseURI_37() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___baseURI_37)); }
	inline String_t* get_baseURI_37() const { return ___baseURI_37; }
	inline String_t** get_address_of_baseURI_37() { return &___baseURI_37; }
	inline void set_baseURI_37(String_t* value)
	{
		___baseURI_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseURI_37), (void*)value);
	}

	inline static int32_t get_offset_of_resolver_38() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___resolver_38)); }
	inline XmlResolver_t7666FB44FF30D5CE53CC8DD913B2A8D2BD74343A * get_resolver_38() const { return ___resolver_38; }
	inline XmlResolver_t7666FB44FF30D5CE53CC8DD913B2A8D2BD74343A ** get_address_of_resolver_38() { return &___resolver_38; }
	inline void set_resolver_38(XmlResolver_t7666FB44FF30D5CE53CC8DD913B2A8D2BD74343A * value)
	{
		___resolver_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resolver_38), (void*)value);
	}

	inline static int32_t get_offset_of_bSetResolver_39() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___bSetResolver_39)); }
	inline bool get_bSetResolver_39() const { return ___bSetResolver_39; }
	inline bool* get_address_of_bSetResolver_39() { return &___bSetResolver_39; }
	inline void set_bSetResolver_39(bool value)
	{
		___bSetResolver_39 = value;
	}

	inline static int32_t get_offset_of_objLock_40() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___objLock_40)); }
	inline RuntimeObject * get_objLock_40() const { return ___objLock_40; }
	inline RuntimeObject ** get_address_of_objLock_40() { return &___objLock_40; }
	inline void set_objLock_40(RuntimeObject * value)
	{
		___objLock_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objLock_40), (void*)value);
	}

	inline static int32_t get_offset_of_namespaceXml_41() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F, ___namespaceXml_41)); }
	inline XmlAttribute_t3F58A4BDFB486D0E610E4003E54A89BCCB65AB6D * get_namespaceXml_41() const { return ___namespaceXml_41; }
	inline XmlAttribute_t3F58A4BDFB486D0E610E4003E54A89BCCB65AB6D ** get_address_of_namespaceXml_41() { return &___namespaceXml_41; }
	inline void set_namespaceXml_41(XmlAttribute_t3F58A4BDFB486D0E610E4003E54A89BCCB65AB6D * value)
	{
		___namespaceXml_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___namespaceXml_41), (void*)value);
	}
};

struct XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F_StaticFields
{
public:
	// System.Xml.EmptyEnumerator System.Xml.XmlDocument::EmptyEnumerator
	EmptyEnumerator_t138901A02D453E19CDE87DFD96981F8A98928E13 * ___EmptyEnumerator_42;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::NotKnownSchemaInfo
	RuntimeObject* ___NotKnownSchemaInfo_43;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::ValidSchemaInfo
	RuntimeObject* ___ValidSchemaInfo_44;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::InvalidSchemaInfo
	RuntimeObject* ___InvalidSchemaInfo_45;

public:
	inline static int32_t get_offset_of_EmptyEnumerator_42() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F_StaticFields, ___EmptyEnumerator_42)); }
	inline EmptyEnumerator_t138901A02D453E19CDE87DFD96981F8A98928E13 * get_EmptyEnumerator_42() const { return ___EmptyEnumerator_42; }
	inline EmptyEnumerator_t138901A02D453E19CDE87DFD96981F8A98928E13 ** get_address_of_EmptyEnumerator_42() { return &___EmptyEnumerator_42; }
	inline void set_EmptyEnumerator_42(EmptyEnumerator_t138901A02D453E19CDE87DFD96981F8A98928E13 * value)
	{
		___EmptyEnumerator_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyEnumerator_42), (void*)value);
	}

	inline static int32_t get_offset_of_NotKnownSchemaInfo_43() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F_StaticFields, ___NotKnownSchemaInfo_43)); }
	inline RuntimeObject* get_NotKnownSchemaInfo_43() const { return ___NotKnownSchemaInfo_43; }
	inline RuntimeObject** get_address_of_NotKnownSchemaInfo_43() { return &___NotKnownSchemaInfo_43; }
	inline void set_NotKnownSchemaInfo_43(RuntimeObject* value)
	{
		___NotKnownSchemaInfo_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NotKnownSchemaInfo_43), (void*)value);
	}

	inline static int32_t get_offset_of_ValidSchemaInfo_44() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F_StaticFields, ___ValidSchemaInfo_44)); }
	inline RuntimeObject* get_ValidSchemaInfo_44() const { return ___ValidSchemaInfo_44; }
	inline RuntimeObject** get_address_of_ValidSchemaInfo_44() { return &___ValidSchemaInfo_44; }
	inline void set_ValidSchemaInfo_44(RuntimeObject* value)
	{
		___ValidSchemaInfo_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ValidSchemaInfo_44), (void*)value);
	}

	inline static int32_t get_offset_of_InvalidSchemaInfo_45() { return static_cast<int32_t>(offsetof(XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F_StaticFields, ___InvalidSchemaInfo_45)); }
	inline RuntimeObject* get_InvalidSchemaInfo_45() const { return ___InvalidSchemaInfo_45; }
	inline RuntimeObject** get_address_of_InvalidSchemaInfo_45() { return &___InvalidSchemaInfo_45; }
	inline void set_InvalidSchemaInfo_45(RuntimeObject* value)
	{
		___InvalidSchemaInfo_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InvalidSchemaInfo_45), (void*)value);
	}
};


// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0  : public XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::next
	XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * ___next_1;

public:
	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0, ___next_1)); }
	inline XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * get_next_1() const { return ___next_1; }
	inline XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___next_1), (void*)value);
	}
};


// NPOI.OpenXmlFormats.ItemChoiceType
struct  ItemChoiceType_tE1EAE3DFCC05CAD1CADF31AE8D265A81F2F9E2BC 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.ItemChoiceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ItemChoiceType_tE1EAE3DFCC05CAD1CADF31AE8D265A81F2F9E2BC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.ST_VectorBaseType
struct  ST_VectorBaseType_t3D9DFCFF962DF13BDB100C89AFFB9539F69E62EF 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.ST_VectorBaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_VectorBaseType_t3D9DFCFF962DF13BDB100C89AFFB9539F69E62EF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_Axis
struct  ST_Axis_tA27FEC9E2CF5CEBB78592171B1F68BC0E7568092 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_Axis_tA27FEC9E2CF5CEBB78592171B1F68BC0E7568092, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_CalcMode
struct  ST_CalcMode_tC12E0FD4100AED72A2FA61F155BBFDC9DCFDB8A4 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_CalcMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_CalcMode_tC12E0FD4100AED72A2FA61F155BBFDC9DCFDB8A4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_CellComments
struct  ST_CellComments_t9B6BF240E205BAF2F0F5BB2900B82B61B69B2F35 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_CellComments::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_CellComments_t9B6BF240E205BAF2F0F5BB2900B82B61B69B2F35, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_CellFormulaType
struct  ST_CellFormulaType_tB4695A71A51C735B5BB7782DDC9A3ED7FB3FD178 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_CellFormulaType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_CellFormulaType_tB4695A71A51C735B5BB7782DDC9A3ED7FB3FD178, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_CellType
struct  ST_CellType_t1AA0D3DA92F02A35BAFFC25C269E02BA7C9BE37A 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_CellType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_CellType_t1AA0D3DA92F02A35BAFFC25C269E02BA7C9BE37A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_CfType
struct  ST_CfType_t2A9E2625103B3BDB778108F3309DC79C7F2009A2 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_CfType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_CfType_t2A9E2625103B3BDB778108F3309DC79C7F2009A2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_CfvoType
struct  ST_CfvoType_tC294634ED92DE76B423E213F829B87D82F798460 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_CfvoType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_CfvoType_tC294634ED92DE76B423E213F829B87D82F798460, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_Comments
struct  ST_Comments_t01E4A73DCE96274DEA771EB57CC85F9B10A3F944 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_Comments::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_Comments_t01E4A73DCE96274DEA771EB57CC85F9B10A3F944, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_ConditionalFormattingOperator
struct  ST_ConditionalFormattingOperator_t40969649FAEF9CD423B253D700C514FB6AA715B2 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_ConditionalFormattingOperator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_ConditionalFormattingOperator_t40969649FAEF9CD423B253D700C514FB6AA715B2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_DataConsolidateFunction
struct  ST_DataConsolidateFunction_t083040A432F372336AB9D5344C82CC474553D7AD 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_DataConsolidateFunction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_DataConsolidateFunction_t083040A432F372336AB9D5344C82CC474553D7AD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_DataValidationErrorStyle
struct  ST_DataValidationErrorStyle_t50C16233C604974CC47FDBA5A14AA7666E2C064C 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_DataValidationErrorStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_DataValidationErrorStyle_t50C16233C604974CC47FDBA5A14AA7666E2C064C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_DataValidationImeMode
struct  ST_DataValidationImeMode_t842F6459BAD93D690A9AF49387E0D3C73472F586 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_DataValidationImeMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_DataValidationImeMode_t842F6459BAD93D690A9AF49387E0D3C73472F586, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_DataValidationOperator
struct  ST_DataValidationOperator_t0A9CB3F4AE0F2498FC64737457CCA532E889976A 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_DataValidationOperator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_DataValidationOperator_t0A9CB3F4AE0F2498FC64737457CCA532E889976A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_DataValidationType
struct  ST_DataValidationType_t62445448AB19EAE635BF33DB26222DE53EFA7F25 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_DataValidationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_DataValidationType_t62445448AB19EAE635BF33DB26222DE53EFA7F25, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_DvAspect
struct  ST_DvAspect_t9488F810995A9DC2973551FAED2375DD141F10BA 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_DvAspect::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_DvAspect_t9488F810995A9DC2973551FAED2375DD141F10BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_FontScheme
struct  ST_FontScheme_t13D428971E924011FBA4E273589DF6AEFE135638 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_FontScheme::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_FontScheme_t13D428971E924011FBA4E273589DF6AEFE135638, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_IconSetType
struct  ST_IconSetType_t1F8A5623EC035B1EE9A912BF7955C1E8EF8DCAB9 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_IconSetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_IconSetType_t1F8A5623EC035B1EE9A912BF7955C1E8EF8DCAB9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_Objects
struct  ST_Objects_tB40A45516832E0BEA733C9C01CCA5FBF68203E5A 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_Objects::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_Objects_tB40A45516832E0BEA733C9C01CCA5FBF68203E5A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_OleUpdate
struct  ST_OleUpdate_t2550A0A0D7A5BEE8857E9CACA1ADD5FABFDEEF89 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_OleUpdate::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_OleUpdate_t2550A0A0D7A5BEE8857E9CACA1ADD5FABFDEEF89, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_Orientation
struct  ST_Orientation_tAE825664E5340483372FACEFD76813FD99E5343D 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_Orientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_Orientation_tAE825664E5340483372FACEFD76813FD99E5343D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_PageOrder
struct  ST_PageOrder_t5197D9687913FE594C0724F594FDEF275B14DCDC 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_PageOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_PageOrder_t5197D9687913FE594C0724F594FDEF275B14DCDC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_Pane
struct  ST_Pane_tA169FE1583249AD9990CFC10924E9F0340F0CC9B 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_Pane::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_Pane_tA169FE1583249AD9990CFC10924E9F0340F0CC9B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_PaneState
struct  ST_PaneState_tEBC75E2CF83E7DA8EEABE6102F70BA59D161CD54 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_PaneState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_PaneState_tEBC75E2CF83E7DA8EEABE6102F70BA59D161CD54, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_PhoneticAlignment
struct  ST_PhoneticAlignment_t314E8420E710B5961CA8DD59FC88F89CB0C86441 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_PhoneticAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_PhoneticAlignment_t314E8420E710B5961CA8DD59FC88F89CB0C86441, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_PhoneticType
struct  ST_PhoneticType_t437599D3EC14593F1E2BF209CA59BD760C06011B 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_PhoneticType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_PhoneticType_t437599D3EC14593F1E2BF209CA59BD760C06011B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_PivotAreaType
struct  ST_PivotAreaType_t74E8787276F0A71D6D071A1FC772F0DB57E982E8 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_PivotAreaType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_PivotAreaType_t74E8787276F0A71D6D071A1FC772F0DB57E982E8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_PrintError
struct  ST_PrintError_tB915CB0F0B436CDEDEC9C8AACAC11B5D07BB90A1 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_PrintError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_PrintError_tB915CB0F0B436CDEDEC9C8AACAC11B5D07BB90A1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_RefMode
struct  ST_RefMode_tF912BBC92DC2B98E8D32D5718EA205B8F7BB9412 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_RefMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_RefMode_tF912BBC92DC2B98E8D32D5718EA205B8F7BB9412, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_SheetState
struct  ST_SheetState_tF7FA9395560403B9CA2C7CEF15C607C40BDA66DD 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_SheetState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_SheetState_tF7FA9395560403B9CA2C7CEF15C607C40BDA66DD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_SheetViewType
struct  ST_SheetViewType_t48477C71581543A9831864666A9FEE0FC5767353 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_SheetViewType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_SheetViewType_t48477C71581543A9831864666A9FEE0FC5767353, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_SmartTagShow
struct  ST_SmartTagShow_t8BDD80239FBF21F69C0471F4D7702217DDEDEAB0 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_SmartTagShow::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_SmartTagShow_t8BDD80239FBF21F69C0471F4D7702217DDEDEAB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_SortBy
struct  ST_SortBy_tA85B632213E7942279984731302A4B5583BCF448 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_SortBy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_SortBy_tA85B632213E7942279984731302A4B5583BCF448, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_SortMethod
struct  ST_SortMethod_tA3E273A04F2821E3E9E37F2C156BC333F37DCDF3 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_SortMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_SortMethod_tA3E273A04F2821E3E9E37F2C156BC333F37DCDF3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_TargetScreenSize
struct  ST_TargetScreenSize_tBE90E992E85B225FFBF9C4735D8351095F1CC2D7 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_TargetScreenSize::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_TargetScreenSize_tBE90E992E85B225FFBF9C4735D8351095F1CC2D7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_TimePeriod
struct  ST_TimePeriod_t928025D87A661E96F9BF79F8D17E6A08A8BB2FDA 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_TimePeriod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_TimePeriod_t928025D87A661E96F9BF79F8D17E6A08A8BB2FDA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_UnderlineValues
struct  ST_UnderlineValues_tCDD863737EC05552B534F45B8D0F9D3826D4F04F 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_UnderlineValues::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_UnderlineValues_tCDD863737EC05552B534F45B8D0F9D3826D4F04F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_UpdateLinks
struct  ST_UpdateLinks_t443EC192010B751405BFE1966E6C23277CF6506A 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_UpdateLinks::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_UpdateLinks_t443EC192010B751405BFE1966E6C23277CF6506A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_VerticalAlignRun
struct  ST_VerticalAlignRun_t3EF8118882178BE61736F5DD0D7C8AD9ED44A2D4 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_VerticalAlignRun::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_VerticalAlignRun_t3EF8118882178BE61736F5DD0D7C8AD9ED44A2D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_Visibility
struct  ST_Visibility_t09767338EEFF6A9C0F57A5311CF976C0649E746D 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_Visibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_Visibility_t09767338EEFF6A9C0F57A5311CF976C0649E746D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// NPOI.OpenXmlFormats.Spreadsheet.ST_WebSourceType
struct  ST_WebSourceType_t2C3D0730343FA5C56D672E30C71D42BB1084CD29 
{
public:
	// System.Int32 NPOI.OpenXmlFormats.Spreadsheet.ST_WebSourceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ST_WebSourceType_t2C3D0730343FA5C56D672E30C71D42BB1084CD29, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.StreamWriter
struct  StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6  : public TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643
{
public:
	// System.IO.Stream System.IO.StreamWriter::stream
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream_12;
	// System.Text.Encoding System.IO.StreamWriter::encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ___encoding_13;
	// System.Text.Encoder System.IO.StreamWriter::encoder
	Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A * ___encoder_14;
	// System.Byte[] System.IO.StreamWriter::byteBuffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___byteBuffer_15;
	// System.Char[] System.IO.StreamWriter::charBuffer
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___charBuffer_16;
	// System.Int32 System.IO.StreamWriter::charPos
	int32_t ___charPos_17;
	// System.Int32 System.IO.StreamWriter::charLen
	int32_t ___charLen_18;
	// System.Boolean System.IO.StreamWriter::autoFlush
	bool ___autoFlush_19;
	// System.Boolean System.IO.StreamWriter::haveWrittenPreamble
	bool ___haveWrittenPreamble_20;
	// System.Boolean System.IO.StreamWriter::closable
	bool ___closable_21;
	// System.Threading.Tasks.Task modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StreamWriter::_asyncWriteTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ____asyncWriteTask_22;

public:
	inline static int32_t get_offset_of_stream_12() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___stream_12)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_stream_12() const { return ___stream_12; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_stream_12() { return &___stream_12; }
	inline void set_stream_12(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___stream_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stream_12), (void*)value);
	}

	inline static int32_t get_offset_of_encoding_13() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___encoding_13)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get_encoding_13() const { return ___encoding_13; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of_encoding_13() { return &___encoding_13; }
	inline void set_encoding_13(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		___encoding_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoding_13), (void*)value);
	}

	inline static int32_t get_offset_of_encoder_14() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___encoder_14)); }
	inline Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A * get_encoder_14() const { return ___encoder_14; }
	inline Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A ** get_address_of_encoder_14() { return &___encoder_14; }
	inline void set_encoder_14(Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A * value)
	{
		___encoder_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___encoder_14), (void*)value);
	}

	inline static int32_t get_offset_of_byteBuffer_15() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___byteBuffer_15)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_byteBuffer_15() const { return ___byteBuffer_15; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_byteBuffer_15() { return &___byteBuffer_15; }
	inline void set_byteBuffer_15(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___byteBuffer_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___byteBuffer_15), (void*)value);
	}

	inline static int32_t get_offset_of_charBuffer_16() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___charBuffer_16)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_charBuffer_16() const { return ___charBuffer_16; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_charBuffer_16() { return &___charBuffer_16; }
	inline void set_charBuffer_16(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___charBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___charBuffer_16), (void*)value);
	}

	inline static int32_t get_offset_of_charPos_17() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___charPos_17)); }
	inline int32_t get_charPos_17() const { return ___charPos_17; }
	inline int32_t* get_address_of_charPos_17() { return &___charPos_17; }
	inline void set_charPos_17(int32_t value)
	{
		___charPos_17 = value;
	}

	inline static int32_t get_offset_of_charLen_18() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___charLen_18)); }
	inline int32_t get_charLen_18() const { return ___charLen_18; }
	inline int32_t* get_address_of_charLen_18() { return &___charLen_18; }
	inline void set_charLen_18(int32_t value)
	{
		___charLen_18 = value;
	}

	inline static int32_t get_offset_of_autoFlush_19() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___autoFlush_19)); }
	inline bool get_autoFlush_19() const { return ___autoFlush_19; }
	inline bool* get_address_of_autoFlush_19() { return &___autoFlush_19; }
	inline void set_autoFlush_19(bool value)
	{
		___autoFlush_19 = value;
	}

	inline static int32_t get_offset_of_haveWrittenPreamble_20() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___haveWrittenPreamble_20)); }
	inline bool get_haveWrittenPreamble_20() const { return ___haveWrittenPreamble_20; }
	inline bool* get_address_of_haveWrittenPreamble_20() { return &___haveWrittenPreamble_20; }
	inline void set_haveWrittenPreamble_20(bool value)
	{
		___haveWrittenPreamble_20 = value;
	}

	inline static int32_t get_offset_of_closable_21() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ___closable_21)); }
	inline bool get_closable_21() const { return ___closable_21; }
	inline bool* get_address_of_closable_21() { return &___closable_21; }
	inline void set_closable_21(bool value)
	{
		___closable_21 = value;
	}

	inline static int32_t get_offset_of__asyncWriteTask_22() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6, ____asyncWriteTask_22)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get__asyncWriteTask_22() const { return ____asyncWriteTask_22; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of__asyncWriteTask_22() { return &____asyncWriteTask_22; }
	inline void set__asyncWriteTask_22(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		____asyncWriteTask_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____asyncWriteTask_22), (void*)value);
	}
};

struct StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6_StaticFields
{
public:
	// System.IO.StreamWriter System.IO.StreamWriter::Null
	StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * ___Null_11;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StreamWriter::_UTF8NoBOM
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ____UTF8NoBOM_23;

public:
	inline static int32_t get_offset_of_Null_11() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6_StaticFields, ___Null_11)); }
	inline StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * get_Null_11() const { return ___Null_11; }
	inline StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 ** get_address_of_Null_11() { return &___Null_11; }
	inline void set_Null_11(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * value)
	{
		___Null_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_11), (void*)value);
	}

	inline static int32_t get_offset_of__UTF8NoBOM_23() { return static_cast<int32_t>(offsetof(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6_StaticFields, ____UTF8NoBOM_23)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get__UTF8NoBOM_23() const { return ____UTF8NoBOM_23; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of__UTF8NoBOM_23() { return &____UTF8NoBOM_23; }
	inline void set__UTF8NoBOM_23(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		____UTF8NoBOM_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____UTF8NoBOM_23), (void*)value);
	}
};


// System.IO.StringWriter
struct  StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839  : public TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643
{
public:
	// System.Text.StringBuilder System.IO.StringWriter::_sb
	StringBuilder_t * ____sb_12;
	// System.Boolean System.IO.StringWriter::_isOpen
	bool ____isOpen_13;

public:
	inline static int32_t get_offset_of__sb_12() { return static_cast<int32_t>(offsetof(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839, ____sb_12)); }
	inline StringBuilder_t * get__sb_12() const { return ____sb_12; }
	inline StringBuilder_t ** get_address_of__sb_12() { return &____sb_12; }
	inline void set__sb_12(StringBuilder_t * value)
	{
		____sb_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sb_12), (void*)value);
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}
};

struct StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_StaticFields
{
public:
	// System.Text.UnicodeEncoding modreq(System.Runtime.CompilerServices.IsVolatile) System.IO.StringWriter::m_encoding
	UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 * ___m_encoding_11;

public:
	inline static int32_t get_offset_of_m_encoding_11() { return static_cast<int32_t>(offsetof(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_StaticFields, ___m_encoding_11)); }
	inline UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 * get_m_encoding_11() const { return ___m_encoding_11; }
	inline UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 ** get_address_of_m_encoding_11() { return &___m_encoding_11; }
	inline void set_m_encoding_11(UnicodeEncoding_tBB60B97AFC49D6246F28BF16D3E09822FCCACC68 * value)
	{
		___m_encoding_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_encoding_11), (void*)value);
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Xml.XmlElement
struct  XmlElement_tF11C508FEEF5FBE169DCE4A7538BE55B1F0C4BCF  : public XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0
{
public:
	// System.Xml.XmlName System.Xml.XmlElement::name
	XmlName_t641FE51F9AE9A7BF9B333D2FB61E2867328F5A63 * ___name_2;
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_tDC800F98FE32D4723967772453EB600D1C368AA3 * ___attributes_3;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastChild
	XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * ___lastChild_4;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlElement_tF11C508FEEF5FBE169DCE4A7538BE55B1F0C4BCF, ___name_2)); }
	inline XmlName_t641FE51F9AE9A7BF9B333D2FB61E2867328F5A63 * get_name_2() const { return ___name_2; }
	inline XmlName_t641FE51F9AE9A7BF9B333D2FB61E2867328F5A63 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(XmlName_t641FE51F9AE9A7BF9B333D2FB61E2867328F5A63 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_2), (void*)value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(XmlElement_tF11C508FEEF5FBE169DCE4A7538BE55B1F0C4BCF, ___attributes_3)); }
	inline XmlAttributeCollection_tDC800F98FE32D4723967772453EB600D1C368AA3 * get_attributes_3() const { return ___attributes_3; }
	inline XmlAttributeCollection_tDC800F98FE32D4723967772453EB600D1C368AA3 ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(XmlAttributeCollection_tDC800F98FE32D4723967772453EB600D1C368AA3 * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___attributes_3), (void*)value);
	}

	inline static int32_t get_offset_of_lastChild_4() { return static_cast<int32_t>(offsetof(XmlElement_tF11C508FEEF5FBE169DCE4A7538BE55B1F0C4BCF, ___lastChild_4)); }
	inline XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * get_lastChild_4() const { return ___lastChild_4; }
	inline XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 ** get_address_of_lastChild_4() { return &___lastChild_4; }
	inline void set_lastChild_4(XmlLinkedNode_tAF992FE43A99E1889622121C0D712C80586580F0 * value)
	{
		___lastChild_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastChild_4), (void*)value);
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * m_Items[1];

public:
	inline XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.CT_CustomProperties::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CT_CustomProperties__ctor_m4ABFA779228B918F22B544F34C4FAEEA479C9B9F (CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * __this, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.CustomPropertiesDocument::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomPropertiesDocument__ctor_mB5BEB3DF621B26DFB1E9ACEC2343742E958E95DD (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * __this, const RuntimeMethod* method);
// NPOI.OpenXmlFormats.CT_CustomProperties NPOI.OpenXmlFormats.CT_CustomProperties::Copy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * CT_CustomProperties_Copy_mED060BB274803D2FA81426DE9CCBC7E505773F08 (CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * __this, const RuntimeMethod* method);
// System.Object System.Xml.Serialization.XmlSerializer::Deserialize(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * XmlSerializer_Deserialize_m25B138EB9DFEAC76221ED87D8FDFBE31A7136AD5 (XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.CustomPropertiesDocument::.ctor(NPOI.OpenXmlFormats.CT_CustomProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomPropertiesDocument__ctor_mD878434C70188BB9CF84239F20DD5A62B9E3E346 (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * __this, CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * ___prop0, const RuntimeMethod* method);
// System.Void System.Xml.Serialization.XmlSerializer::Serialize(System.IO.Stream,System.Object,System.Xml.Serialization.XmlSerializerNamespaces)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlSerializer_Serialize_mF835A744F6A8229166EC3F0ED20BEFA77A38B31E (XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, RuntimeObject * ___o1, XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * ___namespaces2, const RuntimeMethod* method);
// System.Void System.IO.StringWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringWriter__ctor_mBA5BDF99FA78C7B9402872EBB9324A9311040264 (StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * __this, const RuntimeMethod* method);
// System.Void System.Xml.Serialization.XmlSerializer::Serialize(System.IO.TextWriter,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlSerializer_Serialize_mCC448A59320EA45913C8E32A5E9867D591F61E89 (XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * __this, TextWriter_t4CB195237F3B6CADD850FBC3604A049C7C564643 * ___textWriter0, RuntimeObject * ___o1, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Void System.Xml.Serialization.XmlSerializer::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlSerializer__ctor_mF7CC2501D7EF3FF3D559F457AB84A9393D086881 (XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void System.Xml.XmlQualifiedName::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlQualifiedName__ctor_m1A19E27B7945587EBC7FF67CF33F4920D28C4E70 (XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * __this, String_t* ___name0, String_t* ___ns1, const RuntimeMethod* method);
// System.Void System.Xml.Serialization.XmlSerializerNamespaces::.ctor(System.Xml.XmlQualifiedName[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XmlSerializerNamespaces__ctor_m3099A4F409C126946903DCCEA0B9919A7BE131B0 (XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * __this, XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* ___namespaces0, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.CT_ExtendedProperties::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CT_ExtendedProperties__ctor_mC0BED520DC123F1391033EC842E7610047B926F2 (CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * __this, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.ExtendedPropertiesDocument::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtendedPropertiesDocument__ctor_m5D41034398ED048377CE914B6C2021AAB4D59776 (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * __this, const RuntimeMethod* method);
// NPOI.OpenXmlFormats.CT_ExtendedProperties NPOI.OpenXmlFormats.CT_ExtendedProperties::Copy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * CT_ExtendedProperties_Copy_m2E860CA279A4C2AE037537E75A9989158F36DED4 (CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * __this, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.ExtendedPropertiesDocument::.ctor(NPOI.OpenXmlFormats.CT_ExtendedProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtendedPropertiesDocument__ctor_m02C70057BF414D1E94B23BA662E1011750741F92 (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * __this, CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * ___prop0, const RuntimeMethod* method);
// System.Xml.XmlElement System.Xml.XmlDocument::get_DocumentElement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XmlElement_tF11C508FEEF5FBE169DCE4A7538BE55B1F0C4BCF * XmlDocument_get_DocumentElement_mDC08B28B14D903B3FF8D750C6C0B6905DF7ADF91 (XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F * __this, const RuntimeMethod* method);
// NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::Parse(System.Xml.XmlNode,System.Xml.XmlNamespaceManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * CT_Workbook_Parse_mE92BD400083261FB1FB33E9E62CC084B4336EB0F (XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * ___node0, XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465 * ___namespaceManager1, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.Spreadsheet.WorkbookDocument::.ctor(NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorkbookDocument__ctor_m2768119D7911DDBCC60E3DF0CE82B045C0F32F5B (WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3 * __this, CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * ___workbook0, const RuntimeMethod* method);
// System.Void System.IO.StreamWriter::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamWriter__ctor_m3D516CFFEDAC2849A8C3E997FD0233A61A482762 (StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook::Write(System.IO.StreamWriter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CT_Workbook_Write_mC089666D149A3596B0DBF22D655FD16897C86A2A (CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * __this, StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * ___sw0, const RuntimeMethod* method);
// NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::Parse(System.Xml.XmlNode,System.Xml.XmlNamespaceManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * CT_Worksheet_Parse_m2F71C58F30E905EEA0103BD3ECEAF0E2139EAFAA (XmlNode_t26782CDADA207DFC891B2772C8DB236DD3D324A1 * ___node0, XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465 * ___namespaceManager1, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.Spreadsheet.WorksheetDocument::.ctor(NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorksheetDocument__ctor_m13BF2C9B76DC8213925339202E6543766F0245DA (WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F * __this, CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * ___sheet0, const RuntimeMethod* method);
// System.Void NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet::Write(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CT_Worksheet_Write_mF9D00458E34BE7F75817D596DA4C4477CCBCC498 (CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 NPOI.OpenXmlFormats.Spreadsheet.CT_Xf::get_numFmtId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t CT_Xf_get_numFmtId_mE0511C1C5E328F6F8F8D361ED021DB9AA855CABA (CT_Xf_t1790BDF05B20998683A5649F037942CB892C80AB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CT_Xf_get_numFmtId_mE0511C1C5E328F6F8F8D361ED021DB9AA855CABA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CT_Xf_get_numFmtId_mE0511C1C5E328F6F8F8D361ED021DB9AA855CABA_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		uint32_t L_0 = __this->get_numFmtIdField_0();
		return L_0;
	}
}
// System.UInt32 NPOI.OpenXmlFormats.Spreadsheet.CT_Xf::get_xfId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t CT_Xf_get_xfId_mE8A307F65E25D6EC9FAF00C9517A3EB5BAA70728 (CT_Xf_t1790BDF05B20998683A5649F037942CB892C80AB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CT_Xf_get_xfId_mE8A307F65E25D6EC9FAF00C9517A3EB5BAA70728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CT_Xf_get_xfId_mE8A307F65E25D6EC9FAF00C9517A3EB5BAA70728_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		uint32_t L_0 = __this->get_xfIdField_1();
		return L_0;
	}
}
// System.Void NPOI.OpenXmlFormats.Spreadsheet.CT_Xf::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CT_Xf__ctor_m15FCF8C05265CC6DCC101CE1FD7A9EE42A015AC7 (CT_Xf_t1790BDF05B20998683A5649F037942CB892C80AB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CT_Xf__ctor_m15FCF8C05265CC6DCC101CE1FD7A9EE42A015AC7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CT_Xf__ctor_m15FCF8C05265CC6DCC101CE1FD7A9EE42A015AC7_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NPOI.OpenXmlFormats.CustomPropertiesDocument::.ctor(NPOI.OpenXmlFormats.CT_CustomProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomPropertiesDocument__ctor_mD878434C70188BB9CF84239F20DD5A62B9E3E346 (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * __this, CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * ___prop0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomPropertiesDocument__ctor_mD878434C70188BB9CF84239F20DD5A62B9E3E346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CustomPropertiesDocument__ctor_mD878434C70188BB9CF84239F20DD5A62B9E3E346_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * L_0 = ___prop0;
		__this->set__props_2(L_0);
		return;
	}
}
// System.Void NPOI.OpenXmlFormats.CustomPropertiesDocument::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomPropertiesDocument__ctor_mB5BEB3DF621B26DFB1E9ACEC2343742E958E95DD (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomPropertiesDocument__ctor_mB5BEB3DF621B26DFB1E9ACEC2343742E958E95DD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CustomPropertiesDocument__ctor_mB5BEB3DF621B26DFB1E9ACEC2343742E958E95DD_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// NPOI.OpenXmlFormats.CT_CustomProperties NPOI.OpenXmlFormats.CustomPropertiesDocument::GetProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * CustomPropertiesDocument_GetProperties_mE2B356A6D190DD705E6E20BCC08E030991801F71 (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomPropertiesDocument_GetProperties_mE2B356A6D190DD705E6E20BCC08E030991801F71_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CustomPropertiesDocument_GetProperties_mE2B356A6D190DD705E6E20BCC08E030991801F71_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * L_0 = __this->get__props_2();
		return L_0;
	}
}
// NPOI.OpenXmlFormats.CT_CustomProperties NPOI.OpenXmlFormats.CustomPropertiesDocument::AddNewProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * CustomPropertiesDocument_AddNewProperties_mED723B1E354F720A394195E0FBF3368C92BB1095 (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomPropertiesDocument_AddNewProperties_mED723B1E354F720A394195E0FBF3368C92BB1095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CustomPropertiesDocument_AddNewProperties_mED723B1E354F720A394195E0FBF3368C92BB1095_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * L_0 = (CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF *)il2cpp_codegen_object_new(CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF_il2cpp_TypeInfo_var);
		CT_CustomProperties__ctor_m4ABFA779228B918F22B544F34C4FAEEA479C9B9F(L_0, /*hidden argument*/NULL);
		__this->set__props_2(L_0);
		CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * L_1 = __this->get__props_2();
		return L_1;
	}
}
// NPOI.OpenXmlFormats.CustomPropertiesDocument NPOI.OpenXmlFormats.CustomPropertiesDocument::Copy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * CustomPropertiesDocument_Copy_mDB09FADB49AE554CEE43FB77BD81B65D9A85C0F5 (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomPropertiesDocument_Copy_mDB09FADB49AE554CEE43FB77BD81B65D9A85C0F5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CustomPropertiesDocument_Copy_mDB09FADB49AE554CEE43FB77BD81B65D9A85C0F5_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * L_0 = (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 *)il2cpp_codegen_object_new(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var);
		CustomPropertiesDocument__ctor_mB5BEB3DF621B26DFB1E9ACEC2343742E958E95DD(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * L_1 = V_0;
		CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * L_2 = __this->get__props_2();
		NullCheck(L_2);
		CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * L_3 = CT_CustomProperties_Copy_mED060BB274803D2FA81426DE9CCBC7E505773F08(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set__props_2(L_3);
		CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * L_4 = V_0;
		return L_4;
	}
}
// NPOI.OpenXmlFormats.CustomPropertiesDocument NPOI.OpenXmlFormats.CustomPropertiesDocument::Parse(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * CustomPropertiesDocument_Parse_mF328E4120FA705EA25D8695806A0017E7F3F1B17 (Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomPropertiesDocument_Parse_mF328E4120FA705EA25D8695806A0017E7F3F1B17_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CustomPropertiesDocument_Parse_mF328E4120FA705EA25D8695806A0017E7F3F1B17_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var);
		XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * L_0 = ((CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_StaticFields*)il2cpp_codegen_static_fields_for(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var))->get_serializer_0();
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = ___stream0;
		NullCheck(L_0);
		RuntimeObject * L_2 = XmlSerializer_Deserialize_m25B138EB9DFEAC76221ED87D8FDFBE31A7136AD5(L_0, L_1, /*hidden argument*/NULL);
		V_0 = ((CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF *)CastclassClass((RuntimeObject*)L_2, CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF_il2cpp_TypeInfo_var));
		CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * L_3 = V_0;
		CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * L_4 = (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 *)il2cpp_codegen_object_new(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var);
		CustomPropertiesDocument__ctor_mD878434C70188BB9CF84239F20DD5A62B9E3E346(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void NPOI.OpenXmlFormats.CustomPropertiesDocument::Save(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomPropertiesDocument_Save_m42CC47BDD91E9FFCC91CBE4CDC5A886DC11D6072 (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomPropertiesDocument_Save_m42CC47BDD91E9FFCC91CBE4CDC5A886DC11D6072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CustomPropertiesDocument_Save_m42CC47BDD91E9FFCC91CBE4CDC5A886DC11D6072_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var);
		XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * L_0 = ((CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_StaticFields*)il2cpp_codegen_static_fields_for(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var))->get_serializer_0();
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = ___stream0;
		CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * L_2 = __this->get__props_2();
		XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * L_3 = ((CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_StaticFields*)il2cpp_codegen_static_fields_for(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var))->get_namespaces_1();
		NullCheck(L_0);
		XmlSerializer_Serialize_mF835A744F6A8229166EC3F0ED20BEFA77A38B31E(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String NPOI.OpenXmlFormats.CustomPropertiesDocument::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* CustomPropertiesDocument_ToString_m1FF181DCD427665E7075FB4A46967A579A572DAD (CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomPropertiesDocument_ToString_m1FF181DCD427665E7075FB4A46967A579A572DAD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * V_0 = NULL;
	String_t* V_1 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CustomPropertiesDocument_ToString_m1FF181DCD427665E7075FB4A46967A579A572DAD_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_0 = (StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 *)il2cpp_codegen_object_new(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_il2cpp_TypeInfo_var);
		StringWriter__ctor_mBA5BDF99FA78C7B9402872EBB9324A9311040264(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		STORE_TRY_ID(methodExecutionContext, 0);
		IL2CPP_RUNTIME_CLASS_INIT(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var);
		XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * L_1 = ((CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_StaticFields*)il2cpp_codegen_static_fields_for(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var))->get_serializer_0();
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_2 = V_0;
		CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF * L_3 = __this->get__props_2();
		NullCheck(L_1);
		XmlSerializer_Serialize_mCC448A59320EA45913C8E32A5E9867D591F61E89(L_1, L_2, L_3, /*hidden argument*/NULL);
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		V_1 = L_5;
		IL2CPP_LEAVE(0x2A, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		STORE_TRY_ID(methodExecutionContext, -1);
		{
			StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_6 = V_0;
			if (!L_6)
			{
				goto IL_0029;
			}
		}

IL_0023:
		{
			StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_7 = V_0;
			NullCheck(L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_7);
		}

IL_0029:
		{
			IL2CPP_END_FINALLY(32)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
	}

IL_002a:
	{
		String_t* L_8 = V_1;
		return L_8;
	}
}
// System.Void NPOI.OpenXmlFormats.CustomPropertiesDocument::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CustomPropertiesDocument__cctor_m376128E78D1B613FCB9BB6AEDE4D60872324D4F2 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomPropertiesDocument__cctor_m376128E78D1B613FCB9BB6AEDE4D60872324D4F2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, CustomPropertiesDocument__cctor_m376128E78D1B613FCB9BB6AEDE4D60872324D4F2_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (CT_CustomProperties_t7946A88384B7907BE80D2257E2AC79E774F3F7FF_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_0, /*hidden argument*/NULL);
		XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * L_2 = (XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B *)il2cpp_codegen_object_new(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_il2cpp_TypeInfo_var);
		XmlSerializer__ctor_mF7CC2501D7EF3FF3D559F457AB84A9393D086881(L_2, L_1, /*hidden argument*/NULL);
		((CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_StaticFields*)il2cpp_codegen_static_fields_for(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var))->set_serializer_0(L_2);
		XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* L_3 = (XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97*)(XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97*)SZArrayNew(XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97_il2cpp_TypeInfo_var, (uint32_t)2);
		V_0 = L_3;
		XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* L_4 = V_0;
		XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * L_5 = (XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 *)il2cpp_codegen_object_new(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m1A19E27B7945587EBC7FF67CF33F4920D28C4E70(L_5, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, _stringLiteralDA1EAAB8C040E1D2164AFCC76733E93F84C8450B, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 *)L_5);
		XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* L_6 = V_0;
		XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * L_7 = (XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 *)il2cpp_codegen_object_new(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m1A19E27B7945587EBC7FF67CF33F4920D28C4E70(L_7, _stringLiteral4B752498CA45AB14E5BD67D359EAE6F257213868, _stringLiteralC8FEA3F224EC0FC2366083D184EA5ECB892AD023, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 *)L_7);
		XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* L_8 = V_0;
		XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * L_9 = (XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 *)il2cpp_codegen_object_new(XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5_il2cpp_TypeInfo_var);
		XmlSerializerNamespaces__ctor_m3099A4F409C126946903DCCEA0B9919A7BE131B0(L_9, L_8, /*hidden argument*/NULL);
		((CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_StaticFields*)il2cpp_codegen_static_fields_for(CustomPropertiesDocument_tACA45B9D15AC7089C56D712F290FA6A0A2085169_il2cpp_TypeInfo_var))->set_namespaces_1(L_9);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NPOI.OpenXmlFormats.ExtendedPropertiesDocument::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtendedPropertiesDocument__ctor_m5D41034398ED048377CE914B6C2021AAB4D59776 (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendedPropertiesDocument__ctor_m5D41034398ED048377CE914B6C2021AAB4D59776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, ExtendedPropertiesDocument__ctor_m5D41034398ED048377CE914B6C2021AAB4D59776_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NPOI.OpenXmlFormats.ExtendedPropertiesDocument::.ctor(NPOI.OpenXmlFormats.CT_ExtendedProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtendedPropertiesDocument__ctor_m02C70057BF414D1E94B23BA662E1011750741F92 (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * __this, CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * ___prop0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendedPropertiesDocument__ctor_m02C70057BF414D1E94B23BA662E1011750741F92_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, ExtendedPropertiesDocument__ctor_m02C70057BF414D1E94B23BA662E1011750741F92_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * L_0 = ___prop0;
		__this->set__props_2(L_0);
		return;
	}
}
// NPOI.OpenXmlFormats.CT_ExtendedProperties NPOI.OpenXmlFormats.ExtendedPropertiesDocument::AddNewProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * ExtendedPropertiesDocument_AddNewProperties_mB6C794C704FEA9589B9A63CBC0A520A7C40E0F3F (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendedPropertiesDocument_AddNewProperties_mB6C794C704FEA9589B9A63CBC0A520A7C40E0F3F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, ExtendedPropertiesDocument_AddNewProperties_mB6C794C704FEA9589B9A63CBC0A520A7C40E0F3F_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * L_0 = (CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D *)il2cpp_codegen_object_new(CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D_il2cpp_TypeInfo_var);
		CT_ExtendedProperties__ctor_mC0BED520DC123F1391033EC842E7610047B926F2(L_0, /*hidden argument*/NULL);
		__this->set__props_2(L_0);
		CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * L_1 = __this->get__props_2();
		return L_1;
	}
}
// NPOI.OpenXmlFormats.ExtendedPropertiesDocument NPOI.OpenXmlFormats.ExtendedPropertiesDocument::Copy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * ExtendedPropertiesDocument_Copy_mC881B00C57964162E969FC61AEEC746569BF3DF3 (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendedPropertiesDocument_Copy_mC881B00C57964162E969FC61AEEC746569BF3DF3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, ExtendedPropertiesDocument_Copy_mC881B00C57964162E969FC61AEEC746569BF3DF3_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * L_0 = (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 *)il2cpp_codegen_object_new(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var);
		ExtendedPropertiesDocument__ctor_m5D41034398ED048377CE914B6C2021AAB4D59776(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * L_1 = V_0;
		CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * L_2 = __this->get__props_2();
		NullCheck(L_2);
		CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * L_3 = CT_ExtendedProperties_Copy_m2E860CA279A4C2AE037537E75A9989158F36DED4(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set__props_2(L_3);
		ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * L_4 = V_0;
		return L_4;
	}
}
// NPOI.OpenXmlFormats.ExtendedPropertiesDocument NPOI.OpenXmlFormats.ExtendedPropertiesDocument::Parse(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * ExtendedPropertiesDocument_Parse_mABF1B98F82F6065964207D3CA2E65D7100D8D1E9 (Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendedPropertiesDocument_Parse_mABF1B98F82F6065964207D3CA2E65D7100D8D1E9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, ExtendedPropertiesDocument_Parse_mABF1B98F82F6065964207D3CA2E65D7100D8D1E9_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var);
		XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * L_0 = ((ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_StaticFields*)il2cpp_codegen_static_fields_for(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var))->get_serializer_0();
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = ___stream0;
		NullCheck(L_0);
		RuntimeObject * L_2 = XmlSerializer_Deserialize_m25B138EB9DFEAC76221ED87D8FDFBE31A7136AD5(L_0, L_1, /*hidden argument*/NULL);
		V_0 = ((CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D *)CastclassClass((RuntimeObject*)L_2, CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D_il2cpp_TypeInfo_var));
		CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * L_3 = V_0;
		ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * L_4 = (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 *)il2cpp_codegen_object_new(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var);
		ExtendedPropertiesDocument__ctor_m02C70057BF414D1E94B23BA662E1011750741F92(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void NPOI.OpenXmlFormats.ExtendedPropertiesDocument::Save(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtendedPropertiesDocument_Save_m3EE1CEC29ABD41B94D30CF4AE7D44F2E1A291845 (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendedPropertiesDocument_Save_m3EE1CEC29ABD41B94D30CF4AE7D44F2E1A291845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, ExtendedPropertiesDocument_Save_m3EE1CEC29ABD41B94D30CF4AE7D44F2E1A291845_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var);
		XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * L_0 = ((ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_StaticFields*)il2cpp_codegen_static_fields_for(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var))->get_serializer_0();
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = ___stream0;
		CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * L_2 = __this->get__props_2();
		XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * L_3 = ((ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_StaticFields*)il2cpp_codegen_static_fields_for(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var))->get_namespaces_1();
		NullCheck(L_0);
		XmlSerializer_Serialize_mF835A744F6A8229166EC3F0ED20BEFA77A38B31E(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String NPOI.OpenXmlFormats.ExtendedPropertiesDocument::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ExtendedPropertiesDocument_ToString_mDB4A3FB4F96CBF4887B93638B9077A04AABA873B (ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendedPropertiesDocument_ToString_mDB4A3FB4F96CBF4887B93638B9077A04AABA873B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * V_0 = NULL;
	String_t* V_1 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, ExtendedPropertiesDocument_ToString_mDB4A3FB4F96CBF4887B93638B9077A04AABA873B_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_0 = (StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 *)il2cpp_codegen_object_new(StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839_il2cpp_TypeInfo_var);
		StringWriter__ctor_mBA5BDF99FA78C7B9402872EBB9324A9311040264(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		STORE_TRY_ID(methodExecutionContext, 0);
		IL2CPP_RUNTIME_CLASS_INIT(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var);
		XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * L_1 = ((ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_StaticFields*)il2cpp_codegen_static_fields_for(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var))->get_serializer_0();
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_2 = V_0;
		CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D * L_3 = __this->get__props_2();
		NullCheck(L_1);
		XmlSerializer_Serialize_mCC448A59320EA45913C8E32A5E9867D591F61E89(L_1, L_2, L_3, /*hidden argument*/NULL);
		StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		V_1 = L_5;
		IL2CPP_LEAVE(0x2A, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		STORE_TRY_ID(methodExecutionContext, -1);
		{
			StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_6 = V_0;
			if (!L_6)
			{
				goto IL_0029;
			}
		}

IL_0023:
		{
			StringWriter_t7BEF6B06B35BC25817D8BE28593FB52F0525B839 * L_7 = V_0;
			NullCheck(L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_7);
		}

IL_0029:
		{
			IL2CPP_END_FINALLY(32)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
	}

IL_002a:
	{
		String_t* L_8 = V_1;
		return L_8;
	}
}
// System.Void NPOI.OpenXmlFormats.ExtendedPropertiesDocument::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtendedPropertiesDocument__cctor_mF76B98CF4CCC316997BBB7E5323B656689C81BAB (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendedPropertiesDocument__cctor_mF76B98CF4CCC316997BBB7E5323B656689C81BAB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, ExtendedPropertiesDocument__cctor_mF76B98CF4CCC316997BBB7E5323B656689C81BAB_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (CT_ExtendedProperties_t9C0166441F5CA28368ACFE51A651A8077F588B9D_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_0, /*hidden argument*/NULL);
		XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B * L_2 = (XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B *)il2cpp_codegen_object_new(XmlSerializer_t2960FF2C2C062704541774440D09CA3DAFFCB01B_il2cpp_TypeInfo_var);
		XmlSerializer__ctor_mF7CC2501D7EF3FF3D559F457AB84A9393D086881(L_2, L_1, /*hidden argument*/NULL);
		((ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_StaticFields*)il2cpp_codegen_static_fields_for(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var))->set_serializer_0(L_2);
		XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* L_3 = (XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97*)(XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97*)SZArrayNew(XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97_il2cpp_TypeInfo_var, (uint32_t)2);
		V_0 = L_3;
		XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* L_4 = V_0;
		XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * L_5 = (XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 *)il2cpp_codegen_object_new(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m1A19E27B7945587EBC7FF67CF33F4920D28C4E70(L_5, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, _stringLiteralACE1E943256EA7DB61DDD34139D1D5AFD4198882, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 *)L_5);
		XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* L_6 = V_0;
		XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 * L_7 = (XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 *)il2cpp_codegen_object_new(XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m1A19E27B7945587EBC7FF67CF33F4920D28C4E70(L_7, _stringLiteral4B752498CA45AB14E5BD67D359EAE6F257213868, _stringLiteralC8FEA3F224EC0FC2366083D184EA5ECB892AD023, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (XmlQualifiedName_t7A0154DCFCA5749B28B5B0CAE578FDF65EE17905 *)L_7);
		XmlQualifiedNameU5BU5D_tE9A62FBF4DF15B65C9A4430EBAE36CC671776E97* L_8 = V_0;
		XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 * L_9 = (XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5 *)il2cpp_codegen_object_new(XmlSerializerNamespaces_t915BE34D1ECD2020C14B87B702FAAE8A757C0CB5_il2cpp_TypeInfo_var);
		XmlSerializerNamespaces__ctor_m3099A4F409C126946903DCCEA0B9919A7BE131B0(L_9, L_8, /*hidden argument*/NULL);
		((ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_StaticFields*)il2cpp_codegen_static_fields_for(ExtendedPropertiesDocument_tF1DD1D7EC523875FE979B95B52E268237A16BE07_il2cpp_TypeInfo_var))->set_namespaces_1(L_9);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String NPOI.OpenXmlFormats.ST_RelationshipId::get_NamespaceURI()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ST_RelationshipId_get_NamespaceURI_mF28D1E6311B6146FF405D329E287EFA7150FADBB (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ST_RelationshipId_get_NamespaceURI_mF28D1E6311B6146FF405D329E287EFA7150FADBB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, ST_RelationshipId_get_NamespaceURI_mF28D1E6311B6146FF405D329E287EFA7150FADBB_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		return _stringLiteral4786DFC3C080A29EBD4B2900E245E855CEF9E8CD;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// NPOI.OpenXmlFormats.Spreadsheet.CT_Sst NPOI.OpenXmlFormats.Spreadsheet.SstDocument::GetSst()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_Sst_tB80B6A0914663BCEE99DB793DA7F73354949D6A9 * SstDocument_GetSst_m3792D1A5D7B33DCA4D7EFAD7EC30A2923ABC53EB (SstDocument_t29C4C193FFCF0E33A36E3211726074BEDE7D8F81 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SstDocument_GetSst_m3792D1A5D7B33DCA4D7EFAD7EC30A2923ABC53EB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, SstDocument_GetSst_m3792D1A5D7B33DCA4D7EFAD7EC30A2923ABC53EB_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		CT_Sst_tB80B6A0914663BCEE99DB793DA7F73354949D6A9 * L_0 = __this->get_sst_0();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// NPOI.OpenXmlFormats.Spreadsheet.WorkbookDocument NPOI.OpenXmlFormats.Spreadsheet.WorkbookDocument::Parse(System.Xml.XmlDocument,System.Xml.XmlNamespaceManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3 * WorkbookDocument_Parse_m3BF0168B2F5B493DADC485694231E04119FDA3F6 (XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F * ___xmlDoc0, XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465 * ___NameSpaceManager1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorkbookDocument_Parse_m3BF0168B2F5B493DADC485694231E04119FDA3F6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, WorkbookDocument_Parse_m3BF0168B2F5B493DADC485694231E04119FDA3F6_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F * L_0 = ___xmlDoc0;
		NullCheck(L_0);
		XmlElement_tF11C508FEEF5FBE169DCE4A7538BE55B1F0C4BCF * L_1 = XmlDocument_get_DocumentElement_mDC08B28B14D903B3FF8D750C6C0B6905DF7ADF91(L_0, /*hidden argument*/NULL);
		XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465 * L_2 = ___NameSpaceManager1;
		CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * L_3 = CT_Workbook_Parse_mE92BD400083261FB1FB33E9E62CC084B4336EB0F(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * L_4 = V_0;
		WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3 * L_5 = (WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3 *)il2cpp_codegen_object_new(WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3_il2cpp_TypeInfo_var);
		WorkbookDocument__ctor_m2768119D7911DDBCC60E3DF0CE82B045C0F32F5B(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void NPOI.OpenXmlFormats.Spreadsheet.WorkbookDocument::.ctor(NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorkbookDocument__ctor_m2768119D7911DDBCC60E3DF0CE82B045C0F32F5B (WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3 * __this, CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * ___workbook0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorkbookDocument__ctor_m2768119D7911DDBCC60E3DF0CE82B045C0F32F5B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, WorkbookDocument__ctor_m2768119D7911DDBCC60E3DF0CE82B045C0F32F5B_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * L_0 = ___workbook0;
		__this->set_workbook_0(L_0);
		return;
	}
}
// NPOI.OpenXmlFormats.Spreadsheet.CT_Workbook NPOI.OpenXmlFormats.Spreadsheet.WorkbookDocument::get_Workbook()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * WorkbookDocument_get_Workbook_m1BF2B67FA8CB24435C74A58F8CF6F2FFD36B2B74 (WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorkbookDocument_get_Workbook_m1BF2B67FA8CB24435C74A58F8CF6F2FFD36B2B74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, WorkbookDocument_get_Workbook_m1BF2B67FA8CB24435C74A58F8CF6F2FFD36B2B74_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * L_0 = __this->get_workbook_0();
		return L_0;
	}
}
// System.Void NPOI.OpenXmlFormats.Spreadsheet.WorkbookDocument::Save(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorkbookDocument_Save_m714D79ACEDB5BF223849F94DFAEFED69314B99F7 (WorkbookDocument_tF726A4BBCD99780A131873BB1B7843C24B70D9E3 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorkbookDocument_Save_m714D79ACEDB5BF223849F94DFAEFED69314B99F7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, WorkbookDocument_Save_m714D79ACEDB5BF223849F94DFAEFED69314B99F7_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = ___stream0;
		StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * L_1 = (StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 *)il2cpp_codegen_object_new(StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6_il2cpp_TypeInfo_var);
		StreamWriter__ctor_m3D516CFFEDAC2849A8C3E997FD0233A61A482762(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		STORE_TRY_ID(methodExecutionContext, 0);
		CT_Workbook_t0137D577026EA2DBE63EB46547FE956678E92DC0 * L_2 = __this->get_workbook_0();
		StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * L_3 = V_0;
		NullCheck(L_2);
		CT_Workbook_Write_mC089666D149A3596B0DBF22D655FD16897C86A2A(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x1F, FINALLY_0015);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0015;
	}

FINALLY_0015:
	{ // begin finally (depth: 1)
		STORE_TRY_ID(methodExecutionContext, -1);
		{
			StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_001e;
			}
		}

IL_0018:
		{
			StreamWriter_t3E267B7F3C9522AF936C26ABF158398BB779FAF6 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_5);
		}

IL_001e:
		{
			IL2CPP_END_FINALLY(21)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(21)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1F, IL_001f)
	}

IL_001f:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void NPOI.OpenXmlFormats.Spreadsheet.WorksheetDocument::.ctor(NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorksheetDocument__ctor_m13BF2C9B76DC8213925339202E6543766F0245DA (WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F * __this, CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * ___sheet0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorksheetDocument__ctor_m13BF2C9B76DC8213925339202E6543766F0245DA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, WorksheetDocument__ctor_m13BF2C9B76DC8213925339202E6543766F0245DA_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * L_0 = ___sheet0;
		__this->set_sheet_0(L_0);
		return;
	}
}
// NPOI.OpenXmlFormats.Spreadsheet.WorksheetDocument NPOI.OpenXmlFormats.Spreadsheet.WorksheetDocument::Parse(System.Xml.XmlDocument,System.Xml.XmlNamespaceManager)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F * WorksheetDocument_Parse_m12B4F17FB5C4BEECB4C56D253FDECEB3DF0A7C49 (XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F * ___xmldoc0, XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465 * ___namespaceMgr1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorksheetDocument_Parse_m12B4F17FB5C4BEECB4C56D253FDECEB3DF0A7C49_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * V_0 = NULL;
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, WorksheetDocument_Parse_m12B4F17FB5C4BEECB4C56D253FDECEB3DF0A7C49_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		XmlDocument_t513899C58F800C43E8D78C0B72BD18C2C036233F * L_0 = ___xmldoc0;
		NullCheck(L_0);
		XmlElement_tF11C508FEEF5FBE169DCE4A7538BE55B1F0C4BCF * L_1 = XmlDocument_get_DocumentElement_mDC08B28B14D903B3FF8D750C6C0B6905DF7ADF91(L_0, /*hidden argument*/NULL);
		XmlNamespaceManager_t6A4FCF4236F34CF069932BF51B62FD2E62402465 * L_2 = ___namespaceMgr1;
		CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * L_3 = CT_Worksheet_Parse_m2F71C58F30E905EEA0103BD3ECEAF0E2139EAFAA(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * L_4 = V_0;
		WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F * L_5 = (WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F *)il2cpp_codegen_object_new(WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F_il2cpp_TypeInfo_var);
		WorksheetDocument__ctor_m13BF2C9B76DC8213925339202E6543766F0245DA(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// NPOI.OpenXmlFormats.Spreadsheet.CT_Worksheet NPOI.OpenXmlFormats.Spreadsheet.WorksheetDocument::GetWorksheet()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * WorksheetDocument_GetWorksheet_m821F090160952368CDD7D0869318E56111F44958 (WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorksheetDocument_GetWorksheet_m821F090160952368CDD7D0869318E56111F44958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, WorksheetDocument_GetWorksheet_m821F090160952368CDD7D0869318E56111F44958_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * L_0 = __this->get_sheet_0();
		return L_0;
	}
}
// System.Void NPOI.OpenXmlFormats.Spreadsheet.WorksheetDocument::Save(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WorksheetDocument_Save_m00947922034D769962E1AE6091C91AC62021110D (WorksheetDocument_tC79CBAC1997E5E7C4CE2393E95A53939A7A0A61F * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorksheetDocument_Save_m00947922034D769962E1AE6091C91AC62021110D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DECLARE_METHOD_EXEC_CTX(methodExecutionContext, WorksheetDocument_Save_m00947922034D769962E1AE6091C91AC62021110D_RuntimeMethod_var, NULL, NULL, NULL);
	CHECK_PAUSE_POINT;
	{
		CT_Worksheet_tBC954AC24CF9FBE62F144DB99AC0A84D5320F7FF * L_0 = __this->get_sheet_0();
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = ___stream0;
		NullCheck(L_0);
		CT_Worksheet_Write_mF9D00458E34BE7F75817D596DA4C4477CCBCC498(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
