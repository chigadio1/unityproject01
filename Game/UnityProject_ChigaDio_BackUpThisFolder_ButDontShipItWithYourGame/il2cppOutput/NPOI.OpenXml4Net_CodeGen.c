﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void NPOI.OpenXml4Net.OpenXml4NetException::.ctor(System.String)
extern void OpenXml4NetException__ctor_m6C899D9023150431D2881FD559BEE8770C5761F4 (void);
// 0x00000002 System.Void NPOI.OpenXml4Net.Exceptions.InvalidFormatException::.ctor(System.String)
extern void InvalidFormatException__ctor_m654349C24AB2808E1A1D31769F2333596EC159AF (void);
// 0x00000003 System.Void NPOI.OpenXml4Net.Exceptions.PartAlreadyExistsException::.ctor(System.String)
extern void PartAlreadyExistsException__ctor_m794FAF4D0AA217655F74A32333A52A894061F82D (void);
// 0x00000004 System.Void NPOI.OpenXml4Net.OPC.ContentTypes::.cctor()
extern void ContentTypes__cctor_m2DE4DD48B78C24483D95027D88E24A0F92D48186 (void);
// 0x00000005 System.Void NPOI.OpenXml4Net.OPC.Internal.ContentType::.cctor()
extern void ContentType__cctor_m8CE4A47002AE3257555E3B571EEB8B28F98A5EE7 (void);
// 0x00000006 System.Void NPOI.OpenXml4Net.OPC.Internal.ContentType::.ctor(System.String)
extern void ContentType__ctor_m81E9FCEBBB63F9A2C06392C968F13132BFC860A5 (void);
// 0x00000007 System.String NPOI.OpenXml4Net.OPC.Internal.ContentType::ToString()
extern void ContentType_ToString_m2F1FAFE69B7F21544A76D668FF8D679FE8170061 (void);
// 0x00000008 System.Boolean NPOI.OpenXml4Net.OPC.Internal.ContentType::Equals(System.Object)
extern void ContentType_Equals_m1A4022F4B9F7013D2EC04116660D5E92AED2B3AF (void);
// 0x00000009 System.Int32 NPOI.OpenXml4Net.OPC.Internal.ContentType::GetHashCode()
extern void ContentType_GetHashCode_m43F5ECB78FD289798A7BE4667C756A4C6D716099 (void);
// 0x0000000A System.String NPOI.OpenXml4Net.OPC.Internal.ContentType::get_SubType()
extern void ContentType_get_SubType_m346E2F16EAFF49D7AACB3A54B6EF44CE13346A5B (void);
// 0x0000000B System.String NPOI.OpenXml4Net.OPC.Internal.ContentType::get_Type()
extern void ContentType_get_Type_mD00A61D092ECF8E78718528FF24E37EB659882DF (void);
// 0x0000000C System.Int32 NPOI.OpenXml4Net.OPC.Internal.ContentType::CompareTo(System.Object)
extern void ContentType_CompareTo_mDEE691295D2ECFEB79A6A064986372D3753735DC (void);
// 0x0000000D System.Void NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::.ctor(System.IO.Stream,NPOI.OpenXml4Net.OPC.OPCPackage)
extern void ContentTypeManager__ctor_m412AA6F5F8E653A18C815D73877BE5A60740F9A0 (void);
// 0x0000000E System.Void NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::AddContentType(NPOI.OpenXml4Net.OPC.PackagePartName,System.String)
extern void ContentTypeManager_AddContentType_m61414649EFC84ED426EB9F0E4222703C5D70E97A (void);
// 0x0000000F System.Void NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::AddOverrideContentType(NPOI.OpenXml4Net.OPC.PackagePartName,System.String)
extern void ContentTypeManager_AddOverrideContentType_m0C6CFCF1622D636FD6E50258DE669B29E38FA643 (void);
// 0x00000010 System.Void NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::AddDefaultContentType(System.String,System.String)
extern void ContentTypeManager_AddDefaultContentType_m07F7CCB24975636A79596C7D13ED0D9C752B271A (void);
// 0x00000011 System.Void NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::RemoveContentType(NPOI.OpenXml4Net.OPC.PackagePartName)
extern void ContentTypeManager_RemoveContentType_m094ED4CFE665EE29E24011DD9BD1B84879184FFA (void);
// 0x00000012 System.Boolean NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::IsContentTypeRegister(System.String)
extern void ContentTypeManager_IsContentTypeRegister_m3C14AFC238F889497A81E745E8A99361DBDB0C2F (void);
// 0x00000013 System.String NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::GetContentType(NPOI.OpenXml4Net.OPC.PackagePartName)
extern void ContentTypeManager_GetContentType_mF4741AA0BAF4243040763AE616162C78A93D95E2 (void);
// 0x00000014 System.Void NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::ParseContentTypesFile(System.IO.Stream)
extern void ContentTypeManager_ParseContentTypesFile_m789A4EB4BB6051BB0ADE652F2DB22EB4A2194B36 (void);
// 0x00000015 System.Boolean NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::Save(System.IO.Stream)
extern void ContentTypeManager_Save_mD4EDE5986AC537FFE5992EEE68DE379AC8ACA681 (void);
// 0x00000016 System.Void NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::AppendSpecificTypes(System.Xml.XmlDocument,System.Xml.XmlElement,System.Collections.Generic.KeyValuePair`2<NPOI.OpenXml4Net.OPC.PackagePartName,System.String>)
extern void ContentTypeManager_AppendSpecificTypes_mB4AABD3BA58854502D965BF0227C43FBD6B5929D (void);
// 0x00000017 System.Void NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::AppendDefaultType(System.Xml.XmlDocument,System.Xml.XmlElement,System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void ContentTypeManager_AppendDefaultType_m236D8787324737F16C52997186F8D0ACB82A8098 (void);
// 0x00000018 System.Boolean NPOI.OpenXml4Net.OPC.Internal.ContentTypeManager::SaveImpl(System.Xml.XmlDocument,System.IO.Stream)
// 0x00000019 System.Boolean NPOI.OpenXml4Net.OPC.Internal.PartMarshaller::Marshall(NPOI.OpenXml4Net.OPC.PackagePart,System.IO.Stream)
// 0x0000001A System.Boolean NPOI.OpenXml4Net.OPC.Internal.Marshallers.DefaultMarshaller::Marshall(NPOI.OpenXml4Net.OPC.PackagePart,System.IO.Stream)
extern void DefaultMarshaller_Marshall_m8B91F14049D989FAF70748F3F3FEAD6DF41A8C6B (void);
// 0x0000001B System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.DefaultMarshaller::.ctor()
extern void DefaultMarshaller__ctor_mEDDC1CA4C92F78A9CF3AE2FFD49D8D05756665D2 (void);
// 0x0000001C System.Boolean NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::Marshall(NPOI.OpenXml4Net.OPC.PackagePart,System.IO.Stream)
extern void PackagePropertiesMarshaller_Marshall_m1EB154DE9DB730757B95E9B74189127DC23DA0DF (void);
// 0x0000001D System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddCategory()
extern void PackagePropertiesMarshaller_AddCategory_m5D12A2744D06BBF9EFB2FA76F38788CA2CA057ED (void);
// 0x0000001E System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddContentStatus()
extern void PackagePropertiesMarshaller_AddContentStatus_m75766AB0E1D08429BA1927AFAF36CF23EB5BBCDA (void);
// 0x0000001F System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddContentType()
extern void PackagePropertiesMarshaller_AddContentType_mD80A18668EB22DAB4885EFEC0B0BEAA166DB274E (void);
// 0x00000020 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddCreated()
extern void PackagePropertiesMarshaller_AddCreated_mB33DD5B473730FC6A99E4BE157758CB12EC36299 (void);
// 0x00000021 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddCreator()
extern void PackagePropertiesMarshaller_AddCreator_mFF2CCFDD24681B6F6F46E9142DF30B6F66C7888A (void);
// 0x00000022 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddDescription()
extern void PackagePropertiesMarshaller_AddDescription_m10E47BAA0AD40D47378411D6839310E51F074F6D (void);
// 0x00000023 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddIdentifier()
extern void PackagePropertiesMarshaller_AddIdentifier_mF7B10D9977A94958C2630FEC5185588C49532E46 (void);
// 0x00000024 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddKeywords()
extern void PackagePropertiesMarshaller_AddKeywords_mB3ACB5AF02F519FF9E3914106C3382ABA406B150 (void);
// 0x00000025 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddLanguage()
extern void PackagePropertiesMarshaller_AddLanguage_m57CDD79E7AFCFA898A0129FEEC4F8AAC36DA4CDD (void);
// 0x00000026 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddLastModifiedBy()
extern void PackagePropertiesMarshaller_AddLastModifiedBy_mF1856428D018FA27A1DCDE23224C85BB83A34553 (void);
// 0x00000027 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddLastPrinted()
extern void PackagePropertiesMarshaller_AddLastPrinted_m8F52345A0A2D89A4F2F99624A18770A765A3CCE1 (void);
// 0x00000028 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddModified()
extern void PackagePropertiesMarshaller_AddModified_m72BF5F7014118DFE80665A0E14C68ED40407C812 (void);
// 0x00000029 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddRevision()
extern void PackagePropertiesMarshaller_AddRevision_m00C9C61821524FF3344001B59ECBC594CCB27603 (void);
// 0x0000002A System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddSubject()
extern void PackagePropertiesMarshaller_AddSubject_m7A69413019E5716D20F5D143573B1832D6A567A6 (void);
// 0x0000002B System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddTitle()
extern void PackagePropertiesMarshaller_AddTitle_mB3B1D7DFEAE0D144B8982FE7EC21985EAF10B047 (void);
// 0x0000002C System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::AddVersion()
extern void PackagePropertiesMarshaller_AddVersion_m814F5FF9401E81626FC4B5ACF559A26E09F218C7 (void);
// 0x0000002D System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::.ctor()
extern void PackagePropertiesMarshaller__ctor_mADA9904E2A66D962D196DB9EA5DEF87B762CDED6 (void);
// 0x0000002E System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.PackagePropertiesMarshaller::.cctor()
extern void PackagePropertiesMarshaller__cctor_mC6765660EDA5C1AC5F7E8BFC2396CB3360EA6E56 (void);
// 0x0000002F System.Boolean NPOI.OpenXml4Net.OPC.Internal.Marshallers.ZipPackagePropertiesMarshaller::Marshall(NPOI.OpenXml4Net.OPC.PackagePart,System.IO.Stream)
extern void ZipPackagePropertiesMarshaller_Marshall_m4D48C4AF34F58AA777AAA334D9B9899F629C6C6B (void);
// 0x00000030 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.ZipPackagePropertiesMarshaller::.ctor()
extern void ZipPackagePropertiesMarshaller__ctor_mCE6289D72D8D5599A430127823DAD234DFBF192A (void);
// 0x00000031 System.Boolean NPOI.OpenXml4Net.OPC.Internal.Marshallers.ZipPartMarshaller::Marshall(NPOI.OpenXml4Net.OPC.PackagePart,System.IO.Stream)
extern void ZipPartMarshaller_Marshall_m279CEDE31A0028531B0695CB0CCC414BC9C52333 (void);
// 0x00000032 System.Boolean NPOI.OpenXml4Net.OPC.Internal.Marshallers.ZipPartMarshaller::MarshallRelationshipPart(NPOI.OpenXml4Net.OPC.PackageRelationshipCollection,NPOI.OpenXml4Net.OPC.PackagePartName,ICSharpCode.SharpZipLib.Zip.ZipOutputStream)
extern void ZipPartMarshaller_MarshallRelationshipPart_m48FB9D43F6B91E9CEF9B9D43BA444C41B7D43B4B (void);
// 0x00000033 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.ZipPartMarshaller::.ctor()
extern void ZipPartMarshaller__ctor_m096761F199BE21DD282FDCCE4601BD510ACA7B89 (void);
// 0x00000034 System.Void NPOI.OpenXml4Net.OPC.Internal.Marshallers.ZipPartMarshaller::.cctor()
extern void ZipPartMarshaller__cctor_m070B480BBCD03494872847AC0C937AA1B0196E3E (void);
// 0x00000035 System.Void NPOI.OpenXml4Net.OPC.PackagePart::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage,NPOI.OpenXml4Net.OPC.PackagePartName,NPOI.OpenXml4Net.OPC.Internal.ContentType)
extern void PackagePart__ctor_m5218968E8ECEF93C295A07FDA89271D8409B8239 (void);
// 0x00000036 System.Void NPOI.OpenXml4Net.OPC.PackagePart::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage,NPOI.OpenXml4Net.OPC.PackagePartName,NPOI.OpenXml4Net.OPC.Internal.ContentType,System.Boolean)
extern void PackagePart__ctor_mE778EF606EEFBF4EA5BB5A5521A146B91EE26207 (void);
// 0x00000037 System.Void NPOI.OpenXml4Net.OPC.PackagePart::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage,NPOI.OpenXml4Net.OPC.PackagePartName,System.String)
extern void PackagePart__ctor_m82429EE439C2781F6A10566337AC5FE0F3F48A64 (void);
// 0x00000038 NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.PackagePart::AddExternalRelationship(System.String,System.String)
extern void PackagePart_AddExternalRelationship_m4C7E49865DC3EC92675C62623BB9668B928AB706 (void);
// 0x00000039 NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.PackagePart::AddExternalRelationship(System.String,System.String,System.String)
extern void PackagePart_AddExternalRelationship_m7380111AEE8B3B2818F0FCAD2640565869E376E3 (void);
// 0x0000003A NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.PackagePart::AddRelationship(NPOI.OpenXml4Net.OPC.PackagePartName,NPOI.OpenXml4Net.OPC.TargetMode,System.String)
extern void PackagePart_AddRelationship_m6F2B99FE4B7E6C7604DA352B8C8607D752F67964 (void);
// 0x0000003B NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.PackagePart::AddRelationship(NPOI.OpenXml4Net.OPC.PackagePartName,NPOI.OpenXml4Net.OPC.TargetMode,System.String,System.String)
extern void PackagePart_AddRelationship_m1622749938B4FD2B62F1E2B415C2D49498EB183F (void);
// 0x0000003C System.Void NPOI.OpenXml4Net.OPC.PackagePart::ClearRelationships()
extern void PackagePart_ClearRelationships_mD4EC0737A5326C3E1049FA23AB93B804640C0AD1 (void);
// 0x0000003D System.Void NPOI.OpenXml4Net.OPC.PackagePart::RemoveRelationship(System.String)
extern void PackagePart_RemoveRelationship_m8FF47E081EC4B32A748DE1D17D0C32F6D7C909E9 (void);
// 0x0000003E NPOI.OpenXml4Net.OPC.PackageRelationshipCollection NPOI.OpenXml4Net.OPC.PackagePart::get_Relationships()
extern void PackagePart_get_Relationships_mEBC5A2BF0A6113A2AF6BFB8FC9396F8E8BCD735E (void);
// 0x0000003F NPOI.OpenXml4Net.OPC.PackageRelationshipCollection NPOI.OpenXml4Net.OPC.PackagePart::GetRelationshipsByType(System.String)
extern void PackagePart_GetRelationshipsByType_mAE97FDB1F2CD84E0C8F517A01520CCEBC4715E3D (void);
// 0x00000040 NPOI.OpenXml4Net.OPC.PackageRelationshipCollection NPOI.OpenXml4Net.OPC.PackagePart::GetRelationshipsCore(System.String)
extern void PackagePart_GetRelationshipsCore_m4FA8884FEC2D1BDA682D2353EA9371EA637485AB (void);
// 0x00000041 System.Boolean NPOI.OpenXml4Net.OPC.PackagePart::get_HasRelationships()
extern void PackagePart_get_HasRelationships_m9F543E34C433B95439A998C8F747A5108FC45765 (void);
// 0x00000042 System.IO.Stream NPOI.OpenXml4Net.OPC.PackagePart::GetInputStream()
extern void PackagePart_GetInputStream_m26C4864490C6F4D667315E1AA0F6673AB454CEBC (void);
// 0x00000043 System.IO.Stream NPOI.OpenXml4Net.OPC.PackagePart::GetOutputStream()
extern void PackagePart_GetOutputStream_m0924062667C4A9BDD6DED8F9754AC507AA71ABE8 (void);
// 0x00000044 System.Void NPOI.OpenXml4Net.OPC.PackagePart::ThrowExceptionIfRelationship()
extern void PackagePart_ThrowExceptionIfRelationship_mC532189876CDE7F499724B7E7FFD09822E048C28 (void);
// 0x00000045 System.Void NPOI.OpenXml4Net.OPC.PackagePart::LoadRelationships()
extern void PackagePart_LoadRelationships_m694278EFCAE55C4513CCFC637973BF3634D7175B (void);
// 0x00000046 NPOI.OpenXml4Net.OPC.PackagePartName NPOI.OpenXml4Net.OPC.PackagePart::get_PartName()
extern void PackagePart_get_PartName_m4F8D952D7F3DD08A420E8734F8D79B75BF9DA459 (void);
// 0x00000047 System.String NPOI.OpenXml4Net.OPC.PackagePart::get_ContentType()
extern void PackagePart_get_ContentType_mE756395E779DD8794A554A0A0F9A4A7249579C44 (void);
// 0x00000048 NPOI.OpenXml4Net.OPC.OPCPackage NPOI.OpenXml4Net.OPC.PackagePart::get_Package()
extern void PackagePart_get_Package_mE084072340DC174A9FCC0184AD6487F35A0CB9A7 (void);
// 0x00000049 System.Boolean NPOI.OpenXml4Net.OPC.PackagePart::get_IsRelationshipPart()
extern void PackagePart_get_IsRelationshipPart_m590A85BFEE831C3AE57FA4FEB77DACC8D0EFD05C (void);
// 0x0000004A System.Boolean NPOI.OpenXml4Net.OPC.PackagePart::get_IsDeleted()
extern void PackagePart_get_IsDeleted_mDA7356EBCADFC462C3823A11471B7C2D3464ED3C (void);
// 0x0000004B System.Void NPOI.OpenXml4Net.OPC.PackagePart::set_IsDeleted(System.Boolean)
extern void PackagePart_set_IsDeleted_mC520BF23F1A487BE22C6750C021CFBE9C92DE25C (void);
// 0x0000004C System.String NPOI.OpenXml4Net.OPC.PackagePart::ToString()
extern void PackagePart_ToString_m9C528A049CCCC5B4E20DDF018789F5C4EEF9B665 (void);
// 0x0000004D System.IO.Stream NPOI.OpenXml4Net.OPC.PackagePart::GetInputStreamImpl()
// 0x0000004E System.IO.Stream NPOI.OpenXml4Net.OPC.PackagePart::GetOutputStreamImpl()
// 0x0000004F System.Boolean NPOI.OpenXml4Net.OPC.PackagePart::Save(System.IO.Stream)
// 0x00000050 System.Void NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePart::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage,NPOI.OpenXml4Net.OPC.PackagePartName,System.String,System.Boolean)
extern void MemoryPackagePart__ctor_m778B26D3333A396BA4E764A40009602B7DCE5C1F (void);
// 0x00000051 System.IO.Stream NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePart::GetInputStreamImpl()
extern void MemoryPackagePart_GetInputStreamImpl_mA496A76884E636398B5EA0A7F744D7C3D11076EE (void);
// 0x00000052 System.IO.Stream NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePart::GetOutputStreamImpl()
extern void MemoryPackagePart_GetOutputStreamImpl_mF3E14180318C9A5EB6AF59CB4B1C71AFE201055A (void);
// 0x00000053 System.Boolean NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePart::Save(System.IO.Stream)
extern void MemoryPackagePart_Save_m987E627CFBAE395266795ED48D5C5DCF17F5B002 (void);
// 0x00000054 System.Void NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::.ctor(NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePart)
extern void MemoryPackagePartOutputStream__ctor_m46135E8B32D50A86517E547302A33C16368D5BFD (void);
// 0x00000055 System.Int32 NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void MemoryPackagePartOutputStream_Read_mBCABADA6139EA08A1D70A9C6D993A9ED19F88F05 (void);
// 0x00000056 System.Boolean NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::get_CanRead()
extern void MemoryPackagePartOutputStream_get_CanRead_m2126B6DDC97F946348BBF5E4962ED2A19686BD72 (void);
// 0x00000057 System.Boolean NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::get_CanWrite()
extern void MemoryPackagePartOutputStream_get_CanWrite_m7485A45AEB08745325CB594069219785EC887C69 (void);
// 0x00000058 System.Boolean NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::get_CanSeek()
extern void MemoryPackagePartOutputStream_get_CanSeek_m472A70C09626D12748D4307A44C46A76E4013589 (void);
// 0x00000059 System.Int64 NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::get_Length()
extern void MemoryPackagePartOutputStream_get_Length_mA6F906DD8725CCE0F99B6F62A7C2A29952D9C976 (void);
// 0x0000005A System.Int64 NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void MemoryPackagePartOutputStream_Seek_m303D6335F2AB3FC27759A3ADA4AAC01B066895AE (void);
// 0x0000005B System.Int64 NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::get_Position()
extern void MemoryPackagePartOutputStream_get_Position_mC7FE8A398820C34873F74F4AD67A45BBBE17EBC8 (void);
// 0x0000005C System.Void NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::set_Position(System.Int64)
extern void MemoryPackagePartOutputStream_set_Position_m73CAD3C23917CEC62DF59A7DBEF2FB03EB6E1DE2 (void);
// 0x0000005D System.Void NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::Close()
extern void MemoryPackagePartOutputStream_Close_m286ACA4E727EBE6E836CFE5DB0FAF8F63A541AF6 (void);
// 0x0000005E System.Void NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::Flush()
extern void MemoryPackagePartOutputStream_Flush_m97B7ABAD34C9CAFD78C80A22F14B4546DD048DE1 (void);
// 0x0000005F System.Void NPOI.OpenXml4Net.OPC.Internal.MemoryPackagePartOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void MemoryPackagePartOutputStream_Write_mB69F110A3383BD2DE3C18CAC7FB65A5A239333AE (void);
// 0x00000060 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage,NPOI.OpenXml4Net.OPC.PackagePartName)
extern void PackagePropertiesPart__ctor_mD66E4FBD5F267FA4AC8370CF34B6EDFD19CEF37A (void);
// 0x00000061 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetCategoryProperty()
extern void PackagePropertiesPart_GetCategoryProperty_m007EDE04DB6D18839D452C923ACDB5F51A837256 (void);
// 0x00000062 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetContentStatusProperty()
extern void PackagePropertiesPart_GetContentStatusProperty_m9CB340943A373E5AA9853BB019B58D8DACD4000A (void);
// 0x00000063 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetContentTypeProperty()
extern void PackagePropertiesPart_GetContentTypeProperty_m1A2C27496631B6D8588B8833F3C14D017A0D26B2 (void);
// 0x00000064 System.Nullable`1<System.DateTime> NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetCreatedProperty()
extern void PackagePropertiesPart_GetCreatedProperty_mEC332351FDFD1EFE52B976E879AF79EE3A27C2BF (void);
// 0x00000065 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetCreatedPropertyString()
extern void PackagePropertiesPart_GetCreatedPropertyString_m1AEAE5722715BCE56CFFE0FF7FCACCA268056C9B (void);
// 0x00000066 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetCreatorProperty()
extern void PackagePropertiesPart_GetCreatorProperty_mB04D65232109085CB7A322D4B6DDAA54C740E009 (void);
// 0x00000067 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetDescriptionProperty()
extern void PackagePropertiesPart_GetDescriptionProperty_mC2827B7B173CE596B00F4CD136648462B81A565E (void);
// 0x00000068 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetIdentifierProperty()
extern void PackagePropertiesPart_GetIdentifierProperty_m01725BB1843A5A61CC196DF39BA0AF6655B53A73 (void);
// 0x00000069 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetKeywordsProperty()
extern void PackagePropertiesPart_GetKeywordsProperty_m6E195A6949FFF51FCAC639F8843365EFB39E8BCF (void);
// 0x0000006A System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetLanguageProperty()
extern void PackagePropertiesPart_GetLanguageProperty_m4284E00871650991018489461472D2F1574B7215 (void);
// 0x0000006B System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetLastModifiedByProperty()
extern void PackagePropertiesPart_GetLastModifiedByProperty_m6F0B3BB7071089F79FF172A14C264AC76E5884C8 (void);
// 0x0000006C System.Nullable`1<System.DateTime> NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetLastPrintedProperty()
extern void PackagePropertiesPart_GetLastPrintedProperty_m56C51482A309EB80625E6F3E62274F799F9820CB (void);
// 0x0000006D System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetLastPrintedPropertyString()
extern void PackagePropertiesPart_GetLastPrintedPropertyString_m74D2634C9F537A00B661EE58BE6253F5820D2F67 (void);
// 0x0000006E System.Nullable`1<System.DateTime> NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetModifiedProperty()
extern void PackagePropertiesPart_GetModifiedProperty_m0C951D272B41707FC9B4945BA1AF8BC67578FC76 (void);
// 0x0000006F System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetModifiedPropertyString()
extern void PackagePropertiesPart_GetModifiedPropertyString_m453CE116FDA2C5920A087C91C1F3E36EE7CB7FD0 (void);
// 0x00000070 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetRevisionProperty()
extern void PackagePropertiesPart_GetRevisionProperty_mB4DC9D04BAC12940554F19B9A3EBAC9A22DB1A78 (void);
// 0x00000071 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetSubjectProperty()
extern void PackagePropertiesPart_GetSubjectProperty_mD716DA6E05B4BC7289A4D98FD6256D08021FC6BA (void);
// 0x00000072 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetTitleProperty()
extern void PackagePropertiesPart_GetTitleProperty_m3A02B95EC1CF4D7E7066F28672BA92062CF52CE8 (void);
// 0x00000073 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetVersionProperty()
extern void PackagePropertiesPart_GetVersionProperty_m4343B98931AA392F3E939C56C7E45B2F3FEDD720 (void);
// 0x00000074 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetCategoryProperty(System.String)
extern void PackagePropertiesPart_SetCategoryProperty_mACE06F9E3F9E83CB06031DAE1D270FA881BBBCD3 (void);
// 0x00000075 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetContentStatusProperty(System.String)
extern void PackagePropertiesPart_SetContentStatusProperty_m6540C7D0E6EDB090AE7C7D0843E89A7B8E56E1EC (void);
// 0x00000076 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetContentTypeProperty(System.String)
extern void PackagePropertiesPart_SetContentTypeProperty_m2CD8E66F178E1D0D120496E8AE8B0FE344DC3A10 (void);
// 0x00000077 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetCreatedProperty(System.String)
extern void PackagePropertiesPart_SetCreatedProperty_m2ABDB0629FFC457CD3BC4FE8212B5F5CC1B48746 (void);
// 0x00000078 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetCreatorProperty(System.String)
extern void PackagePropertiesPart_SetCreatorProperty_mF40D51F705F67F7AEC112B9057075B1D112A112A (void);
// 0x00000079 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetDescriptionProperty(System.String)
extern void PackagePropertiesPart_SetDescriptionProperty_m630728B656641D5428CEE2BCF0DBE793410436EB (void);
// 0x0000007A System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetIdentifierProperty(System.String)
extern void PackagePropertiesPart_SetIdentifierProperty_mEC7DC1C968506577B01B0A30A1691164D3361A86 (void);
// 0x0000007B System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetKeywordsProperty(System.String)
extern void PackagePropertiesPart_SetKeywordsProperty_m2B95DC42FF2EFB05A193EE26C9FEB1FDA22A4495 (void);
// 0x0000007C System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetLanguageProperty(System.String)
extern void PackagePropertiesPart_SetLanguageProperty_m4307317FF641BA3FD474517A6B731ADD3C2F8CD7 (void);
// 0x0000007D System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetLastModifiedByProperty(System.String)
extern void PackagePropertiesPart_SetLastModifiedByProperty_mD029455B8C477EAFD2B3E5C9F42F45963D5C5D8F (void);
// 0x0000007E System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetLastPrintedProperty(System.String)
extern void PackagePropertiesPart_SetLastPrintedProperty_m96095BD028ED7DF393E2C3D998F84EC8FA1E23C8 (void);
// 0x0000007F System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetModifiedProperty(System.String)
extern void PackagePropertiesPart_SetModifiedProperty_m6413E1BC8FED9394CBC60BACBE169FFAAB0F5849 (void);
// 0x00000080 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetRevisionProperty(System.String)
extern void PackagePropertiesPart_SetRevisionProperty_m876C0D4060F392042ECE5BD6D52A6A3E030205B7 (void);
// 0x00000081 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetSubjectProperty(System.String)
extern void PackagePropertiesPart_SetSubjectProperty_m1FD1D042971431A04AE1FD036E9C3D0AB52040F4 (void);
// 0x00000082 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetTitleProperty(System.String)
extern void PackagePropertiesPart_SetTitleProperty_m8656F3BFCAB86192E2F7E0DBBFF834E51F61CD5E (void);
// 0x00000083 System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetVersionProperty(System.String)
extern void PackagePropertiesPart_SetVersionProperty_m948B3F1E9D6BEC08985404731882E2520EF33665 (void);
// 0x00000084 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetStringValue(System.String)
extern void PackagePropertiesPart_SetStringValue_mA5B0F3907637E7F5ACA85DBF4E63003ACEF54518 (void);
// 0x00000085 System.Nullable`1<System.DateTime> NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::SetDateValue(System.String)
extern void PackagePropertiesPart_SetDateValue_m267BBB5AB065DE9F4818E2A5C00F706EE36255B3 (void);
// 0x00000086 System.String NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetDateValue(System.Nullable`1<System.DateTime>)
extern void PackagePropertiesPart_GetDateValue_m1BEA9153AB2FAE0B8B57CEEE3F247DC4481FBCDB (void);
// 0x00000087 System.IO.Stream NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetInputStreamImpl()
extern void PackagePropertiesPart_GetInputStreamImpl_m8102D5D02F664DC669389B6A2C711A498890B4ED (void);
// 0x00000088 System.IO.Stream NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::GetOutputStreamImpl()
extern void PackagePropertiesPart_GetOutputStreamImpl_mCB83EDDDA785888C7131990CB5740C9568B26A2B (void);
// 0x00000089 System.Boolean NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::Save(System.IO.Stream)
extern void PackagePropertiesPart_Save_mD3CB20CFC446E183DF95B2BF79249B43B370B196 (void);
// 0x0000008A System.Void NPOI.OpenXml4Net.OPC.Internal.PackagePropertiesPart::.cctor()
extern void PackagePropertiesPart__cctor_m17163805CA54B0873FCA32F12B928E8EE2816436 (void);
// 0x0000008B NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.Internal.PartUnmarshaller::Unmarshall(NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.UnmarshallContext,System.IO.Stream)
// 0x0000008C NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::Unmarshall(NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.UnmarshallContext,System.IO.Stream)
extern void PackagePropertiesUnmarshaller_Unmarshall_m1DFDA25357C04DEF523C48DAE5D64F0258744156 (void);
// 0x0000008D System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadCategory(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadCategory_m3ABCE3BD4C09693CAF8547B72845B4945A1EFDF0 (void);
// 0x0000008E System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadContentStatus(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadContentStatus_mCFFEBE6309ED455C57946488834CDB6EA7648B23 (void);
// 0x0000008F System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadContentType(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadContentType_mD59CDE2552C431954C173E39A27C32186662B2A6 (void);
// 0x00000090 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadCreated(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadCreated_m1A93357B7BD6E73BD81809E3CCF7CF01320D4428 (void);
// 0x00000091 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadCreator(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadCreator_m4043BA63D5772984F48D0677B6EE4972999E9716 (void);
// 0x00000092 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadDescription(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadDescription_m0C81884D047EC38EEDAF830C08BEA17E9398DA4C (void);
// 0x00000093 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadIdentifier(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadIdentifier_m0AB3C9135381356161F84E2031DE1A35FFCCFD1C (void);
// 0x00000094 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadKeywords(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadKeywords_m9CCBB0A08A92EE88D888ED444180F4C105584E54 (void);
// 0x00000095 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadLanguage(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadLanguage_m507955AF960349DE49049F8A1788CD033FAEAC95 (void);
// 0x00000096 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadLastModifiedBy(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadLastModifiedBy_mC7863306CFB79F3FB8693F55CC39D99949ECD72A (void);
// 0x00000097 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadLastPrinted(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadLastPrinted_m4E423C1FDAD1B7E7C7049AE6737778CE0AD8AA5A (void);
// 0x00000098 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadModified(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadModified_m48F7BCEFA556FC3CD28DA064F42EC155317A913C (void);
// 0x00000099 System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadRevision(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadRevision_m15CF985F898BD9E1F8BC0FA7F2A94668191052B9 (void);
// 0x0000009A System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadSubject(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadSubject_mFEC1558263071EC75FD70E75FCC0ED3527CB4290 (void);
// 0x0000009B System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadTitle(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadTitle_mC58EC077333498FC1B135F06BE6ED9C717AE89A4 (void);
// 0x0000009C System.String NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::LoadVersion(System.Xml.XmlDocument)
extern void PackagePropertiesUnmarshaller_LoadVersion_mCBD152C8877F8841BCE7D12CEA57F5315AA49B04 (void);
// 0x0000009D System.Void NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::CheckElementForOPCCompliance(System.Xml.XmlElement)
extern void PackagePropertiesUnmarshaller_CheckElementForOPCCompliance_m731D43C5BF93F162540DEB9E21555D9287E1EEC5 (void);
// 0x0000009E System.Void NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::.ctor()
extern void PackagePropertiesUnmarshaller__ctor_m62841C493D0C20789096E0D77A463FD0D7C9C972 (void);
// 0x0000009F System.Void NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.PackagePropertiesUnmarshaller::.cctor()
extern void PackagePropertiesUnmarshaller__cctor_m8EDADDECD26009524B3FB6C6885A2EA41BE7CD46 (void);
// 0x000000A0 System.Void NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.UnmarshallContext::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage,NPOI.OpenXml4Net.OPC.PackagePartName)
extern void UnmarshallContext__ctor_mEF8351B9DA07D35469D5F30186ED4655120784DF (void);
// 0x000000A1 NPOI.OpenXml4Net.OPC.OPCPackage NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.UnmarshallContext::get_Package()
extern void UnmarshallContext_get_Package_m67BA8C1348A8B394C4356907FA3D72F1F076F416 (void);
// 0x000000A2 NPOI.OpenXml4Net.OPC.PackagePartName NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.UnmarshallContext::get_PartName()
extern void UnmarshallContext_get_PartName_m3C57262BA9CA175010AE969100E95AB412269952 (void);
// 0x000000A3 ICSharpCode.SharpZipLib.Zip.ZipEntry NPOI.OpenXml4Net.OPC.Internal.Unmarshallers.UnmarshallContext::get_ZipEntry()
extern void UnmarshallContext_get_ZipEntry_m035909CE6FF1F68F5DAFA1B62C81EC19B549CB38 (void);
// 0x000000A4 System.Void NPOI.OpenXml4Net.OPC.Internal.ZipContentTypeManager::.ctor(System.IO.Stream,NPOI.OpenXml4Net.OPC.OPCPackage)
extern void ZipContentTypeManager__ctor_m12FD825BFAE187E2F3EC71B2CEFB25C7B76F4BC0 (void);
// 0x000000A5 System.Boolean NPOI.OpenXml4Net.OPC.Internal.ZipContentTypeManager::SaveImpl(System.Xml.XmlDocument,System.IO.Stream)
extern void ZipContentTypeManager_SaveImpl_mA57F907DEC096CCC3139673E83255B5CA35D2BEA (void);
// 0x000000A6 System.Void NPOI.OpenXml4Net.OPC.Internal.ZipContentTypeManager::.cctor()
extern void ZipContentTypeManager__cctor_mDF4DE5E83C66F612849D4EE5D54D28AFCBA28351 (void);
// 0x000000A7 ICSharpCode.SharpZipLib.Zip.ZipEntry NPOI.OpenXml4Net.OPC.Internal.ZipHelper::GetCorePropertiesZipEntry(NPOI.OpenXml4Net.OPC.ZipPackage)
extern void ZipHelper_GetCorePropertiesZipEntry_mD4CECD530CD71DA44F8C641150BDA953A98C9EAB (void);
// 0x000000A8 System.String NPOI.OpenXml4Net.OPC.Internal.ZipHelper::GetOPCNameFromZipItemName(System.String)
extern void ZipHelper_GetOPCNameFromZipItemName_m676D77AAB2D5A6393C80B654D21C194B979BFF54 (void);
// 0x000000A9 System.String NPOI.OpenXml4Net.OPC.Internal.ZipHelper::GetZipItemNameFromOPCName(System.String)
extern void ZipHelper_GetZipItemNameFromOPCName_mB569A3EAB11667FCAED4DF2224370B59C17E89A3 (void);
// 0x000000AA System.Uri NPOI.OpenXml4Net.OPC.Internal.ZipHelper::GetZipURIFromOPCName(System.String)
extern void ZipHelper_GetZipURIFromOPCName_mF1D8F3F72FED0A76347393706CC2709ACD22FC24 (void);
// 0x000000AB System.Void NPOI.OpenXml4Net.OPC.Internal.ZipHelper::.cctor()
extern void ZipHelper__cctor_m1AEA7DD2871F60625B2E24E5571914D41038FF52 (void);
// 0x000000AC System.Void NPOI.OpenXml4Net.OPC.OPCPackage::.ctor(NPOI.OpenXml4Net.OPC.PackageAccess)
extern void OPCPackage__ctor_mD95C34675F2B43BA89DCDF87A9A0ACB09E089CD6 (void);
// 0x000000AD System.Void NPOI.OpenXml4Net.OPC.OPCPackage::Init()
extern void OPCPackage_Init_m2623BFE7DC1F9EC5E47CB27EC7E58BA24AEE98D9 (void);
// 0x000000AE NPOI.OpenXml4Net.OPC.OPCPackage NPOI.OpenXml4Net.OPC.OPCPackage::Open(System.IO.Stream)
extern void OPCPackage_Open_m1D91ACF774E300E90D2F87F74681746532A68FB7 (void);
// 0x000000AF System.Void NPOI.OpenXml4Net.OPC.OPCPackage::ThrowExceptionIfReadOnly()
extern void OPCPackage_ThrowExceptionIfReadOnly_m69736C3183972AD66D1990FFB5F488CDB8EDB0CF (void);
// 0x000000B0 System.Void NPOI.OpenXml4Net.OPC.OPCPackage::ThrowExceptionIfWriteOnly()
extern void OPCPackage_ThrowExceptionIfWriteOnly_mC5623AFC38D0798E2DDBF42955CFCE7C4E363C77 (void);
// 0x000000B1 NPOI.OpenXml4Net.OPC.PackageProperties NPOI.OpenXml4Net.OPC.OPCPackage::GetPackageProperties()
extern void OPCPackage_GetPackageProperties_m76EC283FB5FB3FB627E641BDA31E463D942A371B (void);
// 0x000000B2 NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.OPCPackage::GetPart(NPOI.OpenXml4Net.OPC.PackagePartName)
extern void OPCPackage_GetPart_m7550E32E3DA094F0E92429D6C4C883D789A8320A (void);
// 0x000000B3 System.Collections.Generic.List`1<NPOI.OpenXml4Net.OPC.PackagePart> NPOI.OpenXml4Net.OPC.OPCPackage::GetPartsByRelationshipType(System.String)
extern void OPCPackage_GetPartsByRelationshipType_mFCDD18276119EACB40B08370BC0472605AAB5A0A (void);
// 0x000000B4 NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.OPCPackage::GetPart(NPOI.OpenXml4Net.OPC.PackageRelationship)
extern void OPCPackage_GetPart_m6769F7CDCB4929026379454AAE2A3983C18E2425 (void);
// 0x000000B5 System.Collections.Generic.List`1<NPOI.OpenXml4Net.OPC.PackagePart> NPOI.OpenXml4Net.OPC.OPCPackage::GetParts()
extern void OPCPackage_GetParts_m80C7F7DB5CC9E8140F91A4596511C1DA84D53B2A (void);
// 0x000000B6 NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.OPCPackage::CreatePart(NPOI.OpenXml4Net.OPC.PackagePartName,System.String)
extern void OPCPackage_CreatePart_m372587D3911A6B69CC378AE51E959F44E9B53079 (void);
// 0x000000B7 NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.OPCPackage::CreatePart(NPOI.OpenXml4Net.OPC.PackagePartName,System.String,System.Boolean)
extern void OPCPackage_CreatePart_m769C4CACD14CC721C722CCAFC94FF68706D54B05 (void);
// 0x000000B8 System.Void NPOI.OpenXml4Net.OPC.OPCPackage::RemovePart(NPOI.OpenXml4Net.OPC.PackagePart)
extern void OPCPackage_RemovePart_m22C03A14F5A209CCAAEB93950063F6DAE5BB51DA (void);
// 0x000000B9 System.Void NPOI.OpenXml4Net.OPC.OPCPackage::RemovePart(NPOI.OpenXml4Net.OPC.PackagePartName)
extern void OPCPackage_RemovePart_m2F876C973B893D8E773B47C7406FBF0652907748 (void);
// 0x000000BA System.Boolean NPOI.OpenXml4Net.OPC.OPCPackage::ContainPart(NPOI.OpenXml4Net.OPC.PackagePartName)
extern void OPCPackage_ContainPart_mFB84E7B3AAC5AAFCE61D67459A71CFF0DFE947B7 (void);
// 0x000000BB NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.OPCPackage::AddRelationship(NPOI.OpenXml4Net.OPC.PackagePartName,NPOI.OpenXml4Net.OPC.TargetMode,System.String,System.String)
extern void OPCPackage_AddRelationship_m17A9EA8BEB7D6C1DD6C894EF8034751E0ED5045B (void);
// 0x000000BC NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.OPCPackage::AddRelationship(NPOI.OpenXml4Net.OPC.PackagePartName,NPOI.OpenXml4Net.OPC.TargetMode,System.String)
extern void OPCPackage_AddRelationship_mD288268E3052E5520B18C4F583CF084599213171 (void);
// 0x000000BD NPOI.OpenXml4Net.OPC.PackageRelationshipCollection NPOI.OpenXml4Net.OPC.OPCPackage::get_Relationships()
extern void OPCPackage_get_Relationships_mA7BC97D9CD73E8EF0CC9CD9FCDA4BAB26FC62B88 (void);
// 0x000000BE NPOI.OpenXml4Net.OPC.PackageRelationshipCollection NPOI.OpenXml4Net.OPC.OPCPackage::GetRelationshipsByType(System.String)
extern void OPCPackage_GetRelationshipsByType_m6A00A08F5C2FEA23607B3636BE2B2E7249F02A58 (void);
// 0x000000BF NPOI.OpenXml4Net.OPC.PackageRelationshipCollection NPOI.OpenXml4Net.OPC.OPCPackage::GetRelationshipsHelper(System.String)
extern void OPCPackage_GetRelationshipsHelper_m8EA4FDD69F9FB24866B8A02E2713781FD64408A8 (void);
// 0x000000C0 System.Void NPOI.OpenXml4Net.OPC.OPCPackage::ClearRelationships()
extern void OPCPackage_ClearRelationships_m16D4756A6AF5BD14091B2DF1780A2B3DD6861FA9 (void);
// 0x000000C1 System.Void NPOI.OpenXml4Net.OPC.OPCPackage::EnsureRelationships()
extern void OPCPackage_EnsureRelationships_mEAB54F3C19D8305EAE7FACA0ABD642EF3C628FEC (void);
// 0x000000C2 NPOI.OpenXml4Net.OPC.PackageAccess NPOI.OpenXml4Net.OPC.OPCPackage::GetPackageAccess()
extern void OPCPackage_GetPackageAccess_m85000CB1009DF6DFD18897540B2B4346937456EC (void);
// 0x000000C3 System.Void NPOI.OpenXml4Net.OPC.OPCPackage::Save(System.IO.Stream)
extern void OPCPackage_Save_m21B8E4B5C27DCEED5341CC58FB44C8E18A8D1DA5 (void);
// 0x000000C4 NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.OPCPackage::CreatePartImpl(NPOI.OpenXml4Net.OPC.PackagePartName,System.String,System.Boolean)
// 0x000000C5 System.Void NPOI.OpenXml4Net.OPC.OPCPackage::RemovePartImpl(NPOI.OpenXml4Net.OPC.PackagePartName)
// 0x000000C6 System.Void NPOI.OpenXml4Net.OPC.OPCPackage::SaveImpl(System.IO.Stream)
// 0x000000C7 NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.OPCPackage::GetPartImpl(NPOI.OpenXml4Net.OPC.PackagePartName)
// 0x000000C8 NPOI.OpenXml4Net.OPC.PackagePart[] NPOI.OpenXml4Net.OPC.OPCPackage::GetPartsImpl()
// 0x000000C9 System.Void NPOI.OpenXml4Net.OPC.OPCPackage::.cctor()
extern void OPCPackage__cctor_m46373163C8513CB95E42854493204535324FCEA0 (void);
// 0x000000CA System.Void NPOI.OpenXml4Net.OPC.PackagePartCollection::Remove(NPOI.OpenXml4Net.OPC.PackagePartName)
extern void PackagePartCollection_Remove_m88DDDA1C9624F03A0643FDE3CE154153C09BD041 (void);
// 0x000000CB System.Void NPOI.OpenXml4Net.OPC.PackagePartCollection::.ctor()
extern void PackagePartCollection__ctor_mEC2F5212E19E9E32BDF3B80305DC7AE04BCD9DE1 (void);
// 0x000000CC System.Void NPOI.OpenXml4Net.OPC.PackagePartCollection::.cctor()
extern void PackagePartCollection__cctor_m29203A4D4174E6A7EC053C7115E3C896B1AA0B7D (void);
// 0x000000CD System.Void NPOI.OpenXml4Net.OPC.PackagePartName::.ctor(System.Uri,System.Boolean)
extern void PackagePartName__ctor_mE55FCFAEAF3FFE9474FFEA5429B951D6221D7145 (void);
// 0x000000CE System.Boolean NPOI.OpenXml4Net.OPC.PackagePartName::IsRelationshipPartURI(System.Uri)
extern void PackagePartName_IsRelationshipPartURI_m91F380CEF8F36336352A2A30068154570040BFEA (void);
// 0x000000CF System.Boolean NPOI.OpenXml4Net.OPC.PackagePartName::IsRelationshipPartURI()
extern void PackagePartName_IsRelationshipPartURI_m9BCA0BD95D515525462541BC9B10B5FCF1E857F2 (void);
// 0x000000D0 System.Void NPOI.OpenXml4Net.OPC.PackagePartName::ThrowExceptionIfInvalidPartUri(System.Uri)
extern void PackagePartName_ThrowExceptionIfInvalidPartUri_mA0569C268055D55036A412CC2B6EEFF5CF14632E (void);
// 0x000000D1 System.Void NPOI.OpenXml4Net.OPC.PackagePartName::ThrowExceptionIfEmptyURI(System.Uri)
extern void PackagePartName_ThrowExceptionIfEmptyURI_m32371A677A60BF58B12537F06FB4AA4F9C6A6CC3 (void);
// 0x000000D2 System.Void NPOI.OpenXml4Net.OPC.PackagePartName::ThrowExceptionIfPartNameHaveInvalidSegments(System.Uri)
extern void PackagePartName_ThrowExceptionIfPartNameHaveInvalidSegments_mE2172BEF98A93E10A13C6441AA282840172CFE4F (void);
// 0x000000D3 System.Void NPOI.OpenXml4Net.OPC.PackagePartName::CheckPCharCompliance(System.String)
extern void PackagePartName_CheckPCharCompliance_mB8B3A8305FDE57A9ADA8983706F8BCA7623957A3 (void);
// 0x000000D4 System.Void NPOI.OpenXml4Net.OPC.PackagePartName::ThrowExceptionIfPartNameNotStartsWithForwardSlashChar(System.Uri)
extern void PackagePartName_ThrowExceptionIfPartNameNotStartsWithForwardSlashChar_m08156A103ABFD09C35B9C1F3FB7E70FF725C0303 (void);
// 0x000000D5 System.Void NPOI.OpenXml4Net.OPC.PackagePartName::ThrowExceptionIfPartNameEndsWithForwardSlashChar(System.Uri)
extern void PackagePartName_ThrowExceptionIfPartNameEndsWithForwardSlashChar_mD94723400426D683B28A4B15233CB0D617F79BE6 (void);
// 0x000000D6 System.Void NPOI.OpenXml4Net.OPC.PackagePartName::ThrowExceptionIfAbsoluteUri(System.Uri)
extern void PackagePartName_ThrowExceptionIfAbsoluteUri_m1D31877C803CD103EC6F1EF77049385373E0BDB6 (void);
// 0x000000D7 System.Int32 NPOI.OpenXml4Net.OPC.PackagePartName::Compare(NPOI.OpenXml4Net.OPC.PackagePartName,NPOI.OpenXml4Net.OPC.PackagePartName)
extern void PackagePartName_Compare_mF7BED3A8A7CD14391FC719042F400EEC09C71CB6 (void);
// 0x000000D8 System.Int32 NPOI.OpenXml4Net.OPC.PackagePartName::CompareTo(System.Object)
extern void PackagePartName_CompareTo_m19A4817BE18FB68C90048E52251A0D051A7D874C (void);
// 0x000000D9 System.String NPOI.OpenXml4Net.OPC.PackagePartName::get_Extension()
extern void PackagePartName_get_Extension_m94A111451C91BA04A1BE4E21D0C2955588FC17D1 (void);
// 0x000000DA System.String NPOI.OpenXml4Net.OPC.PackagePartName::get_Name()
extern void PackagePartName_get_Name_m42D4E23FD298C2ABABEF741F36D6D86FD15F4141 (void);
// 0x000000DB System.Boolean NPOI.OpenXml4Net.OPC.PackagePartName::Equals(System.Object)
extern void PackagePartName_Equals_m31DFA0D017AD54FB1BA71E3BF81595EA020AC1DB (void);
// 0x000000DC System.Int32 NPOI.OpenXml4Net.OPC.PackagePartName::GetHashCode()
extern void PackagePartName_GetHashCode_m7054467DFBE26824A96E20774FE30CD71207A252 (void);
// 0x000000DD System.String NPOI.OpenXml4Net.OPC.PackagePartName::ToString()
extern void PackagePartName_ToString_mF0F435463B1D1DEA93DF2308F10E7560FF63C768 (void);
// 0x000000DE System.Uri NPOI.OpenXml4Net.OPC.PackagePartName::get_URI()
extern void PackagePartName_get_URI_m34453BA50987E5AC56C8D940CEA0E30FBB1F002A (void);
// 0x000000DF System.Void NPOI.OpenXml4Net.OPC.PackagePartName::.cctor()
extern void PackagePartName__cctor_m8740C1CB412029409AD8F14044AD1498C0FA2F73 (void);
// 0x000000E0 System.Void NPOI.OpenXml4Net.OPC.PackageRelationship::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage,NPOI.OpenXml4Net.OPC.PackagePart,System.Uri,NPOI.OpenXml4Net.OPC.TargetMode,System.String,System.String)
extern void PackageRelationship__ctor_m7E360D6008E3638921B9DA46F19C36165246D143 (void);
// 0x000000E1 System.Boolean NPOI.OpenXml4Net.OPC.PackageRelationship::Equals(System.Object)
extern void PackageRelationship_Equals_m85DE06F2EC3095A534FF067A3573BFC2A4232BD3 (void);
// 0x000000E2 System.Int32 NPOI.OpenXml4Net.OPC.PackageRelationship::GetHashCode()
extern void PackageRelationship_GetHashCode_mCB0D57CC65B3094C0FE3B8E9399141554D2F97D9 (void);
// 0x000000E3 System.String NPOI.OpenXml4Net.OPC.PackageRelationship::get_Id()
extern void PackageRelationship_get_Id_m47A35214238CCC25962903573C4776B8CFDE29FD (void);
// 0x000000E4 System.String NPOI.OpenXml4Net.OPC.PackageRelationship::get_RelationshipType()
extern void PackageRelationship_get_RelationshipType_m402C88CD26BF19F4DADF7A9DD9BDAE947CBFBCD8 (void);
// 0x000000E5 System.Uri NPOI.OpenXml4Net.OPC.PackageRelationship::get_SourceUri()
extern void PackageRelationship_get_SourceUri_mFEFDBA067B0748ADD6094B1BAEF1F35D6259C95C (void);
// 0x000000E6 System.Nullable`1<NPOI.OpenXml4Net.OPC.TargetMode> NPOI.OpenXml4Net.OPC.PackageRelationship::get_TargetMode()
extern void PackageRelationship_get_TargetMode_mE161D2CF0E5EB2131F107AD78E476CEE9926CC7F (void);
// 0x000000E7 System.Uri NPOI.OpenXml4Net.OPC.PackageRelationship::get_TargetUri()
extern void PackageRelationship_get_TargetUri_m62F5CEE7050B22603BF87625936FFDFA2967CC82 (void);
// 0x000000E8 System.String NPOI.OpenXml4Net.OPC.PackageRelationship::ToString()
extern void PackageRelationship_ToString_m3859F5ABF16F84D195FDCD975B3B60B03E975A61 (void);
// 0x000000E9 System.Void NPOI.OpenXml4Net.OPC.PackageRelationship::.cctor()
extern void PackageRelationship__cctor_m41484A9819972C6E0DC8111B887BC9BCB65CE259 (void);
// 0x000000EA System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::.ctor()
extern void PackageRelationshipCollection__ctor_m399D293B7954F1502165AAF9817C06F307BF533C (void);
// 0x000000EB System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::.ctor(NPOI.OpenXml4Net.OPC.PackageRelationshipCollection,System.String)
extern void PackageRelationshipCollection__ctor_m207F1CDBC46BF37B18D498C5BECCEB83F8F9399E (void);
// 0x000000EC System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage)
extern void PackageRelationshipCollection__ctor_mAA910013F3CC05E6D6A1369025C6A7E308270EDD (void);
// 0x000000ED System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::.ctor(NPOI.OpenXml4Net.OPC.PackagePart)
extern void PackageRelationshipCollection__ctor_m056BD1940673BD3CE4AB58E42749E65A4028FC65 (void);
// 0x000000EE System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage,NPOI.OpenXml4Net.OPC.PackagePart)
extern void PackageRelationshipCollection__ctor_m3B6B28BF759850E459D67AEB6B82405F2E515A92 (void);
// 0x000000EF NPOI.OpenXml4Net.OPC.PackagePartName NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::GetRelationshipPartName(NPOI.OpenXml4Net.OPC.PackagePart)
extern void PackageRelationshipCollection_GetRelationshipPartName_mA9E6E1B0E1AB6ED13156D3F088556E7349E919E8 (void);
// 0x000000F0 System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::AddRelationship(NPOI.OpenXml4Net.OPC.PackageRelationship)
extern void PackageRelationshipCollection_AddRelationship_mA02BBF2A509391CEF010C107E8E287242B693923 (void);
// 0x000000F1 NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::AddRelationship(System.Uri,NPOI.OpenXml4Net.OPC.TargetMode,System.String,System.String)
extern void PackageRelationshipCollection_AddRelationship_m2BE51E4382BF59084214A851091638D8B40B9997 (void);
// 0x000000F2 System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::RemoveRelationship(System.String)
extern void PackageRelationshipCollection_RemoveRelationship_m6DD10C598986D36222235DC0518ECB46D9C298B3 (void);
// 0x000000F3 NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::GetRelationship(System.Int32)
extern void PackageRelationshipCollection_GetRelationship_mA7E97DB33D57C99044254D096040CC450641D225 (void);
// 0x000000F4 NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::GetRelationshipByID(System.String)
extern void PackageRelationshipCollection_GetRelationshipByID_mED2D93B9F935F47DD965CEC2BBA83A3592EEDD48 (void);
// 0x000000F5 System.Int32 NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::get_Size()
extern void PackageRelationshipCollection_get_Size_m07B31165EA5078ACBB9F34F705F55C3079AD1906 (void);
// 0x000000F6 System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::ParseRelationshipsPart(NPOI.OpenXml4Net.OPC.PackagePart)
extern void PackageRelationshipCollection_ParseRelationshipsPart_m0D4EFE1EA3B439DDF13463B5CD77EEFC52287090 (void);
// 0x000000F7 NPOI.OpenXml4Net.OPC.PackageRelationshipCollection NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::GetRelationships(System.String)
extern void PackageRelationshipCollection_GetRelationships_m9845EEEBFEAF60B0ADA0D886582A2693944D444E (void);
// 0x000000F8 System.Collections.Generic.IEnumerator`1<NPOI.OpenXml4Net.OPC.PackageRelationship> NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::GetEnumerator()
extern void PackageRelationshipCollection_GetEnumerator_m53BF9AE33BD837BB6D7A43E43C0F246F38018A40 (void);
// 0x000000F9 System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::Clear()
extern void PackageRelationshipCollection_Clear_mFBE771D8C25B0C6EF92FED84ED02871125108A85 (void);
// 0x000000FA System.String NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::ToString()
extern void PackageRelationshipCollection_ToString_m93C42933C33B71A209F10A964889DDB998DC5099 (void);
// 0x000000FB NPOI.OpenXml4Net.OPC.PackageRelationship NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::System.Collections.Generic.IEnumerator<NPOI.OpenXml4Net.OPC.PackageRelationship>.get_Current()
extern void PackageRelationshipCollection_System_Collections_Generic_IEnumeratorU3CNPOI_OpenXml4Net_OPC_PackageRelationshipU3E_get_Current_mFDFB6FA09641ED4060359192A122CE7761F280A7 (void);
// 0x000000FC System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::System.IDisposable.Dispose()
extern void PackageRelationshipCollection_System_IDisposable_Dispose_mEABACEC2ADFF7D2744B003A647676DC706406F2D (void);
// 0x000000FD System.Object NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::System.Collections.IEnumerator.get_Current()
extern void PackageRelationshipCollection_System_Collections_IEnumerator_get_Current_mA012AAC0325334979BD33957D83364B820C83F94 (void);
// 0x000000FE System.Boolean NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::System.Collections.IEnumerator.MoveNext()
extern void PackageRelationshipCollection_System_Collections_IEnumerator_MoveNext_m6A31E1C9F4DC775D2E67A738DD5147C1A15FF434 (void);
// 0x000000FF System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::System.Collections.IEnumerator.Reset()
extern void PackageRelationshipCollection_System_Collections_IEnumerator_Reset_m106BBD499E44C2C302AEE92F3AFE126735E2E484 (void);
// 0x00000100 System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection::.cctor()
extern void PackageRelationshipCollection__cctor_m3A90DCD3F86027D3878DFB4DF71B943D625AA875 (void);
// 0x00000101 System.Int32 NPOI.OpenXml4Net.OPC.PackageRelationshipCollection_DuplicateComparer::Compare(System.String,System.String)
extern void DuplicateComparer_Compare_m30C49CA90E47B6E211644B9CA10F7A86DECF7FC7 (void);
// 0x00000102 System.Void NPOI.OpenXml4Net.OPC.PackageRelationshipCollection_DuplicateComparer::.ctor()
extern void DuplicateComparer__ctor_m902589D343AB270FD209F7510009C0CA26D4979B (void);
// 0x00000103 System.Void NPOI.OpenXml4Net.OPC.PackagingUriHelper::.cctor()
extern void PackagingUriHelper__cctor_m598F10184D5F3EB1ACFEC5F04CB4D0DC07580C9C (void);
// 0x00000104 System.Uri NPOI.OpenXml4Net.OPC.PackagingUriHelper::ParseUri(System.String,System.UriKind)
extern void PackagingUriHelper_ParseUri_m53B2631A7B867FA7744F052504B6B8C0DE6C7214 (void);
// 0x00000105 System.Boolean NPOI.OpenXml4Net.OPC.PackagingUriHelper::IsRelationshipPartURI(System.Uri)
extern void PackagingUriHelper_IsRelationshipPartURI_mD20E0A8B671BC51C8B0CDC9DBD906C96D7D9BBE9 (void);
// 0x00000106 System.String NPOI.OpenXml4Net.OPC.PackagingUriHelper::GetFilename(System.Uri)
extern void PackagingUriHelper_GetFilename_mBC04F32AFDD855BBA224A3CA98F5F8CEF07FD3FD (void);
// 0x00000107 System.String NPOI.OpenXml4Net.OPC.PackagingUriHelper::GetFilenameWithoutExtension(System.Uri)
extern void PackagingUriHelper_GetFilenameWithoutExtension_m3993070825BE54416B33D545CF7C593A9C4C072F (void);
// 0x00000108 System.String NPOI.OpenXml4Net.OPC.PackagingUriHelper::Combine(System.String,System.String)
extern void PackagingUriHelper_Combine_m7B59E30AD8A57E71FDDF29C65FEDAF0E2B7F8B75 (void);
// 0x00000109 System.Uri NPOI.OpenXml4Net.OPC.PackagingUriHelper::RelativizeUri(System.Uri,System.Uri,System.Boolean)
extern void PackagingUriHelper_RelativizeUri_mC230B29439DD8CAF6376674620A40CA8F60744A5 (void);
// 0x0000010A System.Uri NPOI.OpenXml4Net.OPC.PackagingUriHelper::ResolvePartUri(System.Uri,System.Uri)
extern void PackagingUriHelper_ResolvePartUri_mAD6B46BE909E24D5C361A71B80869C0CA8255499 (void);
// 0x0000010B System.Uri NPOI.OpenXml4Net.OPC.PackagingUriHelper::GetURIFromPath(System.String)
extern void PackagingUriHelper_GetURIFromPath_m9C0B164DFADAF272DC195CC051BE8B0F8DBDEE20 (void);
// 0x0000010C System.Uri NPOI.OpenXml4Net.OPC.PackagingUriHelper::GetSourcePartUriFromRelationshipPartUri(System.Uri)
extern void PackagingUriHelper_GetSourcePartUriFromRelationshipPartUri_mBBBFEE645C1359E8C677C6EF775F975E0EFCB3FA (void);
// 0x0000010D NPOI.OpenXml4Net.OPC.PackagePartName NPOI.OpenXml4Net.OPC.PackagingUriHelper::CreatePartName(System.Uri)
extern void PackagingUriHelper_CreatePartName_m1AAB63929F4598559F58E044DC1AB3AB71D22AF9 (void);
// 0x0000010E NPOI.OpenXml4Net.OPC.PackagePartName NPOI.OpenXml4Net.OPC.PackagingUriHelper::CreatePartName(System.String)
extern void PackagingUriHelper_CreatePartName_mEA17599A2BEAB71EBCE22F3249D8457A4C61CD7A (void);
// 0x0000010F System.Uri NPOI.OpenXml4Net.OPC.PackagingUriHelper::ToUri(System.String)
extern void PackagingUriHelper_ToUri_m483DB899A7F95CEEBF457318EB411443612CEBDE (void);
// 0x00000110 System.String NPOI.OpenXml4Net.OPC.PackagingUriHelper::Encode(System.String)
extern void PackagingUriHelper_Encode_mDB6877181EA6609DDF0375DF263002DDCAA460D7 (void);
// 0x00000111 System.Boolean NPOI.OpenXml4Net.OPC.PackagingUriHelper::IsUnsafe(System.Int32)
extern void PackagingUriHelper_IsUnsafe_mEB6AC440C9CF7031C1450BB8EDDC2AEE10962177 (void);
// 0x00000112 NPOI.OpenXml4Net.OPC.PackagePartName NPOI.OpenXml4Net.OPC.PackagingUriHelper::GetRelationshipPartName(NPOI.OpenXml4Net.OPC.PackagePartName)
extern void PackagingUriHelper_GetRelationshipPartName_m5AB1D0D71E38B1B78BD344C945BA1913884A5865 (void);
// 0x00000113 System.Void NPOI.OpenXml4Net.OPC.StreamHelper::SaveXmlInStream(System.Xml.XmlDocument,System.IO.Stream)
extern void StreamHelper_SaveXmlInStream_m463CE67E8D183075D84DB21FE5F2AD13DF41135A (void);
// 0x00000114 System.Void NPOI.OpenXml4Net.OPC.StreamHelper::CopyStream(System.IO.Stream,System.IO.Stream)
extern void StreamHelper_CopyStream_mB94A2BDC2D0B9D905C2CFC4A71A669FA638EE899 (void);
// 0x00000115 System.Void NPOI.OpenXml4Net.OPC.ZipPackage::.ctor(System.IO.Stream,NPOI.OpenXml4Net.OPC.PackageAccess)
extern void ZipPackage__ctor_mA0BE696D39265A43C040F826E0007021D6AE67D9 (void);
// 0x00000116 NPOI.OpenXml4Net.OPC.PackagePart[] NPOI.OpenXml4Net.OPC.ZipPackage::GetPartsImpl()
extern void ZipPackage_GetPartsImpl_m284DB52F3BFDFB334C228F4E0BC698F07EF2B92A (void);
// 0x00000117 NPOI.OpenXml4Net.OPC.PackagePartName NPOI.OpenXml4Net.OPC.ZipPackage::BuildPartName(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern void ZipPackage_BuildPartName_mD0A347092ED9341CA819B01A65DAD1A8559C02DC (void);
// 0x00000118 NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.ZipPackage::CreatePartImpl(NPOI.OpenXml4Net.OPC.PackagePartName,System.String,System.Boolean)
extern void ZipPackage_CreatePartImpl_m324444D4082CFD20ACCBCEE7C75F072EE912AE77 (void);
// 0x00000119 System.Void NPOI.OpenXml4Net.OPC.ZipPackage::RemovePartImpl(NPOI.OpenXml4Net.OPC.PackagePartName)
extern void ZipPackage_RemovePartImpl_m6464D769F2353ADB5C29761E38C521C7F4ECC565 (void);
// 0x0000011A NPOI.OpenXml4Net.OPC.PackagePart NPOI.OpenXml4Net.OPC.ZipPackage::GetPartImpl(NPOI.OpenXml4Net.OPC.PackagePartName)
extern void ZipPackage_GetPartImpl_mCF6297F818376471F0F1CE3C7DC6C570B0C0183A (void);
// 0x0000011B System.Void NPOI.OpenXml4Net.OPC.ZipPackage::SaveImpl(System.IO.Stream)
extern void ZipPackage_SaveImpl_mBBC269FE0EDD414A457BA4CEE6A09D318E64F9E0 (void);
// 0x0000011C NPOI.OpenXml4Net.Util.ZipEntrySource NPOI.OpenXml4Net.OPC.ZipPackage::get_ZipArchive()
extern void ZipPackage_get_ZipArchive_m10D15C36BB5748B965A90335CAA31A8510BF738F (void);
// 0x0000011D System.Void NPOI.OpenXml4Net.OPC.ZipPackage::.cctor()
extern void ZipPackage__cctor_mAD004B5F22ECD259EE7EF32B43F2316C9E055060 (void);
// 0x0000011E System.Void NPOI.OpenXml4Net.OPC.ZipPackagePart::.ctor(NPOI.OpenXml4Net.OPC.OPCPackage,ICSharpCode.SharpZipLib.Zip.ZipEntry,NPOI.OpenXml4Net.OPC.PackagePartName,System.String)
extern void ZipPackagePart__ctor_mCF2927135F5383CE87ED4C9D16722B7746ABA6EE (void);
// 0x0000011F System.IO.Stream NPOI.OpenXml4Net.OPC.ZipPackagePart::GetInputStreamImpl()
extern void ZipPackagePart_GetInputStreamImpl_mEB7D7F5177101586D32EBEFD82EF0E2DDD1E051C (void);
// 0x00000120 System.IO.Stream NPOI.OpenXml4Net.OPC.ZipPackagePart::GetOutputStreamImpl()
extern void ZipPackagePart_GetOutputStreamImpl_mE36C5007D0E238A6071BF38EBFE8A02AF8140532 (void);
// 0x00000121 System.Boolean NPOI.OpenXml4Net.OPC.ZipPackagePart::Save(System.IO.Stream)
extern void ZipPackagePart_Save_m3A1825F7BE718F2D1FB8BD26875A3704CF00E579 (void);
// 0x00000122 System.Int32 NPOI.OpenXml4Net.Util.XmlHelper::ReadInt(System.Xml.XmlAttribute)
extern void XmlHelper_ReadInt_m7955A6B33C55D0833259CD6B0D9E161D15EE841F (void);
// 0x00000123 System.String NPOI.OpenXml4Net.Util.XmlHelper::ReadString(System.Xml.XmlAttribute)
extern void XmlHelper_ReadString_m2C362A9D2D902C34D5E2DC3235A815A575B959A0 (void);
// 0x00000124 System.UInt32 NPOI.OpenXml4Net.Util.XmlHelper::ReadUInt(System.Xml.XmlAttribute)
extern void XmlHelper_ReadUInt_mFFBCC8A25F064FAC392F13E4CA9C5E76A30DD3C4 (void);
// 0x00000125 System.Boolean NPOI.OpenXml4Net.Util.XmlHelper::ReadBool(System.Xml.XmlAttribute)
extern void XmlHelper_ReadBool_m3D5A026A46892B9E4F197C166CA4B8A0502FA570 (void);
// 0x00000126 System.Double NPOI.OpenXml4Net.Util.XmlHelper::ReadDouble(System.Xml.XmlAttribute)
extern void XmlHelper_ReadDouble_m6B6BFF1C927BB3D8B8B9DE21E662E3DFDF3D6BC0 (void);
// 0x00000127 System.Boolean NPOI.OpenXml4Net.Util.XmlHelper::ReadBool(System.Xml.XmlAttribute,System.Boolean)
extern void XmlHelper_ReadBool_mF4AA57F64F62E86F0CB86611E2435A9C648BE81F (void);
// 0x00000128 System.String NPOI.OpenXml4Net.Util.XmlHelper::ExcelEncodeString(System.String)
extern void XmlHelper_ExcelEncodeString_mB7E531EB1A714F3D7BD5F5ED7DD2FE5CA81C5386 (void);
// 0x00000129 System.String NPOI.OpenXml4Net.Util.XmlHelper::EncodeXml(System.String)
extern void XmlHelper_EncodeXml_m1B89F00830C4C08B195E4DB519740068767E52D0 (void);
// 0x0000012A System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.Boolean)
extern void XmlHelper_WriteAttribute_m5638F21D81D50B847348FB1DE2402057015B0B14 (void);
// 0x0000012B System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.Boolean,System.Boolean)
extern void XmlHelper_WriteAttribute_m2855EDA0DC2AAF185BE60632C0B31182FD1CC6F3 (void);
// 0x0000012C System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.Double)
extern void XmlHelper_WriteAttribute_m8E1F05D6C6D031224087C6EB39E2243BA6865A2B (void);
// 0x0000012D System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.Double,System.Boolean)
extern void XmlHelper_WriteAttribute_mA4FCA24128591613E864DF418BAEDECE4614052D (void);
// 0x0000012E System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.Int32,System.Boolean)
extern void XmlHelper_WriteAttribute_mF1E1FC2FFC49B76B28CDA2160A7C5B32A99CD7C0 (void);
// 0x0000012F System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.Int32)
extern void XmlHelper_WriteAttribute_m577928DCB30B924424D1CAB5A0C9182CB9FA47F5 (void);
// 0x00000130 System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.String)
extern void XmlHelper_WriteAttribute_mFEE74764D9045F1B3DE0D43A2515B583D2894FD0 (void);
// 0x00000131 System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.String,System.Boolean)
extern void XmlHelper_WriteAttribute_mA225E92AC868351AB5496238ED55F9BC297884FE (void);
// 0x00000132 System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.Byte[])
extern void XmlHelper_WriteAttribute_mB6041910EE9F7304F419505533F4C3DD1289ACB6 (void);
// 0x00000133 System.Void NPOI.OpenXml4Net.Util.XmlHelper::WriteAttribute(System.IO.StreamWriter,System.String,System.UInt32)
extern void XmlHelper_WriteAttribute_m3C29E6BA7B239B9DD8CBA6D7F58F3B934DAC78BC (void);
// 0x00000134 System.Byte[] NPOI.OpenXml4Net.Util.XmlHelper::ReadBytes(System.Xml.XmlAttribute)
extern void XmlHelper_ReadBytes_m33614A50F8D9DA6A1D24E449C86EA051B3D64E23 (void);
// 0x00000135 System.Byte NPOI.OpenXml4Net.Util.XmlHelper::ReadByte(System.Xml.XmlAttribute)
extern void XmlHelper_ReadByte_mDFCEE5D2D4103371B0D815C13B3DDC76CB9C3F23 (void);
// 0x00000136 System.Collections.IEnumerator NPOI.OpenXml4Net.Util.ZipEntrySource::get_Entries()
// 0x00000137 System.IO.Stream NPOI.OpenXml4Net.Util.ZipEntrySource::GetInputStream(ICSharpCode.SharpZipLib.Zip.ZipEntry)
// 0x00000138 System.Void NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource::.ctor(ICSharpCode.SharpZipLib.Zip.ZipInputStream)
extern void ZipInputStreamZipEntrySource__ctor_mFCCB0B8492259A3683188C721A3F9A07E0A043B8 (void);
// 0x00000139 System.Collections.IEnumerator NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource::get_Entries()
extern void ZipInputStreamZipEntrySource_get_Entries_m1FD0F8AE76CC69F5BCE4278603660230324A4809 (void);
// 0x0000013A System.IO.Stream NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource::GetInputStream(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern void ZipInputStreamZipEntrySource_GetInputStream_mC2021D15884767B523E1ABB302DA4E3EAC9F92FD (void);
// 0x0000013B System.Void NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource_EntryEnumerator::.ctor(System.Collections.Generic.List`1<NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource_FakeZipEntry>)
extern void EntryEnumerator__ctor_mFD2EDD3B50698DD5FAC6748F78CD84F35DC88AE3 (void);
// 0x0000013C System.Boolean NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource_EntryEnumerator::MoveNext()
extern void EntryEnumerator_MoveNext_m4A5B59475FD4B85B7E3F81326C785DC12BE4B1E0 (void);
// 0x0000013D System.Object NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource_EntryEnumerator::get_Current()
extern void EntryEnumerator_get_Current_m44A4931D38D87D2E14C877226004995116B3CC2C (void);
// 0x0000013E System.Void NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource_EntryEnumerator::Reset()
extern void EntryEnumerator_Reset_mB951FD8921557A7594D3B646C4B64C92B29D62E3 (void);
// 0x0000013F System.Void NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource_FakeZipEntry::.ctor(ICSharpCode.SharpZipLib.Zip.ZipEntry,ICSharpCode.SharpZipLib.Zip.ZipInputStream)
extern void FakeZipEntry__ctor_mC969931CBAB0F7FEA57952937D9955AD7F663703 (void);
// 0x00000140 System.IO.Stream NPOI.OpenXml4Net.Util.ZipInputStreamZipEntrySource_FakeZipEntry::GetInputStream()
extern void FakeZipEntry_GetInputStream_m21D3022D6E41C4E29B71411D591A7850BA3D7001 (void);
static Il2CppMethodPointer s_methodPointers[320] = 
{
	OpenXml4NetException__ctor_m6C899D9023150431D2881FD559BEE8770C5761F4,
	InvalidFormatException__ctor_m654349C24AB2808E1A1D31769F2333596EC159AF,
	PartAlreadyExistsException__ctor_m794FAF4D0AA217655F74A32333A52A894061F82D,
	ContentTypes__cctor_m2DE4DD48B78C24483D95027D88E24A0F92D48186,
	ContentType__cctor_m8CE4A47002AE3257555E3B571EEB8B28F98A5EE7,
	ContentType__ctor_m81E9FCEBBB63F9A2C06392C968F13132BFC860A5,
	ContentType_ToString_m2F1FAFE69B7F21544A76D668FF8D679FE8170061,
	ContentType_Equals_m1A4022F4B9F7013D2EC04116660D5E92AED2B3AF,
	ContentType_GetHashCode_m43F5ECB78FD289798A7BE4667C756A4C6D716099,
	ContentType_get_SubType_m346E2F16EAFF49D7AACB3A54B6EF44CE13346A5B,
	ContentType_get_Type_mD00A61D092ECF8E78718528FF24E37EB659882DF,
	ContentType_CompareTo_mDEE691295D2ECFEB79A6A064986372D3753735DC,
	ContentTypeManager__ctor_m412AA6F5F8E653A18C815D73877BE5A60740F9A0,
	ContentTypeManager_AddContentType_m61414649EFC84ED426EB9F0E4222703C5D70E97A,
	ContentTypeManager_AddOverrideContentType_m0C6CFCF1622D636FD6E50258DE669B29E38FA643,
	ContentTypeManager_AddDefaultContentType_m07F7CCB24975636A79596C7D13ED0D9C752B271A,
	ContentTypeManager_RemoveContentType_m094ED4CFE665EE29E24011DD9BD1B84879184FFA,
	ContentTypeManager_IsContentTypeRegister_m3C14AFC238F889497A81E745E8A99361DBDB0C2F,
	ContentTypeManager_GetContentType_mF4741AA0BAF4243040763AE616162C78A93D95E2,
	ContentTypeManager_ParseContentTypesFile_m789A4EB4BB6051BB0ADE652F2DB22EB4A2194B36,
	ContentTypeManager_Save_mD4EDE5986AC537FFE5992EEE68DE379AC8ACA681,
	ContentTypeManager_AppendSpecificTypes_mB4AABD3BA58854502D965BF0227C43FBD6B5929D,
	ContentTypeManager_AppendDefaultType_m236D8787324737F16C52997186F8D0ACB82A8098,
	NULL,
	NULL,
	DefaultMarshaller_Marshall_m8B91F14049D989FAF70748F3F3FEAD6DF41A8C6B,
	DefaultMarshaller__ctor_mEDDC1CA4C92F78A9CF3AE2FFD49D8D05756665D2,
	PackagePropertiesMarshaller_Marshall_m1EB154DE9DB730757B95E9B74189127DC23DA0DF,
	PackagePropertiesMarshaller_AddCategory_m5D12A2744D06BBF9EFB2FA76F38788CA2CA057ED,
	PackagePropertiesMarshaller_AddContentStatus_m75766AB0E1D08429BA1927AFAF36CF23EB5BBCDA,
	PackagePropertiesMarshaller_AddContentType_mD80A18668EB22DAB4885EFEC0B0BEAA166DB274E,
	PackagePropertiesMarshaller_AddCreated_mB33DD5B473730FC6A99E4BE157758CB12EC36299,
	PackagePropertiesMarshaller_AddCreator_mFF2CCFDD24681B6F6F46E9142DF30B6F66C7888A,
	PackagePropertiesMarshaller_AddDescription_m10E47BAA0AD40D47378411D6839310E51F074F6D,
	PackagePropertiesMarshaller_AddIdentifier_mF7B10D9977A94958C2630FEC5185588C49532E46,
	PackagePropertiesMarshaller_AddKeywords_mB3ACB5AF02F519FF9E3914106C3382ABA406B150,
	PackagePropertiesMarshaller_AddLanguage_m57CDD79E7AFCFA898A0129FEEC4F8AAC36DA4CDD,
	PackagePropertiesMarshaller_AddLastModifiedBy_mF1856428D018FA27A1DCDE23224C85BB83A34553,
	PackagePropertiesMarshaller_AddLastPrinted_m8F52345A0A2D89A4F2F99624A18770A765A3CCE1,
	PackagePropertiesMarshaller_AddModified_m72BF5F7014118DFE80665A0E14C68ED40407C812,
	PackagePropertiesMarshaller_AddRevision_m00C9C61821524FF3344001B59ECBC594CCB27603,
	PackagePropertiesMarshaller_AddSubject_m7A69413019E5716D20F5D143573B1832D6A567A6,
	PackagePropertiesMarshaller_AddTitle_mB3B1D7DFEAE0D144B8982FE7EC21985EAF10B047,
	PackagePropertiesMarshaller_AddVersion_m814F5FF9401E81626FC4B5ACF559A26E09F218C7,
	PackagePropertiesMarshaller__ctor_mADA9904E2A66D962D196DB9EA5DEF87B762CDED6,
	PackagePropertiesMarshaller__cctor_mC6765660EDA5C1AC5F7E8BFC2396CB3360EA6E56,
	ZipPackagePropertiesMarshaller_Marshall_m4D48C4AF34F58AA777AAA334D9B9899F629C6C6B,
	ZipPackagePropertiesMarshaller__ctor_mCE6289D72D8D5599A430127823DAD234DFBF192A,
	ZipPartMarshaller_Marshall_m279CEDE31A0028531B0695CB0CCC414BC9C52333,
	ZipPartMarshaller_MarshallRelationshipPart_m48FB9D43F6B91E9CEF9B9D43BA444C41B7D43B4B,
	ZipPartMarshaller__ctor_m096761F199BE21DD282FDCCE4601BD510ACA7B89,
	ZipPartMarshaller__cctor_m070B480BBCD03494872847AC0C937AA1B0196E3E,
	PackagePart__ctor_m5218968E8ECEF93C295A07FDA89271D8409B8239,
	PackagePart__ctor_mE778EF606EEFBF4EA5BB5A5521A146B91EE26207,
	PackagePart__ctor_m82429EE439C2781F6A10566337AC5FE0F3F48A64,
	PackagePart_AddExternalRelationship_m4C7E49865DC3EC92675C62623BB9668B928AB706,
	PackagePart_AddExternalRelationship_m7380111AEE8B3B2818F0FCAD2640565869E376E3,
	PackagePart_AddRelationship_m6F2B99FE4B7E6C7604DA352B8C8607D752F67964,
	PackagePart_AddRelationship_m1622749938B4FD2B62F1E2B415C2D49498EB183F,
	PackagePart_ClearRelationships_mD4EC0737A5326C3E1049FA23AB93B804640C0AD1,
	PackagePart_RemoveRelationship_m8FF47E081EC4B32A748DE1D17D0C32F6D7C909E9,
	PackagePart_get_Relationships_mEBC5A2BF0A6113A2AF6BFB8FC9396F8E8BCD735E,
	PackagePart_GetRelationshipsByType_mAE97FDB1F2CD84E0C8F517A01520CCEBC4715E3D,
	PackagePart_GetRelationshipsCore_m4FA8884FEC2D1BDA682D2353EA9371EA637485AB,
	PackagePart_get_HasRelationships_m9F543E34C433B95439A998C8F747A5108FC45765,
	PackagePart_GetInputStream_m26C4864490C6F4D667315E1AA0F6673AB454CEBC,
	PackagePart_GetOutputStream_m0924062667C4A9BDD6DED8F9754AC507AA71ABE8,
	PackagePart_ThrowExceptionIfRelationship_mC532189876CDE7F499724B7E7FFD09822E048C28,
	PackagePart_LoadRelationships_m694278EFCAE55C4513CCFC637973BF3634D7175B,
	PackagePart_get_PartName_m4F8D952D7F3DD08A420E8734F8D79B75BF9DA459,
	PackagePart_get_ContentType_mE756395E779DD8794A554A0A0F9A4A7249579C44,
	PackagePart_get_Package_mE084072340DC174A9FCC0184AD6487F35A0CB9A7,
	PackagePart_get_IsRelationshipPart_m590A85BFEE831C3AE57FA4FEB77DACC8D0EFD05C,
	PackagePart_get_IsDeleted_mDA7356EBCADFC462C3823A11471B7C2D3464ED3C,
	PackagePart_set_IsDeleted_mC520BF23F1A487BE22C6750C021CFBE9C92DE25C,
	PackagePart_ToString_m9C528A049CCCC5B4E20DDF018789F5C4EEF9B665,
	NULL,
	NULL,
	NULL,
	MemoryPackagePart__ctor_m778B26D3333A396BA4E764A40009602B7DCE5C1F,
	MemoryPackagePart_GetInputStreamImpl_mA496A76884E636398B5EA0A7F744D7C3D11076EE,
	MemoryPackagePart_GetOutputStreamImpl_mF3E14180318C9A5EB6AF59CB4B1C71AFE201055A,
	MemoryPackagePart_Save_m987E627CFBAE395266795ED48D5C5DCF17F5B002,
	MemoryPackagePartOutputStream__ctor_m46135E8B32D50A86517E547302A33C16368D5BFD,
	MemoryPackagePartOutputStream_Read_mBCABADA6139EA08A1D70A9C6D993A9ED19F88F05,
	MemoryPackagePartOutputStream_get_CanRead_m2126B6DDC97F946348BBF5E4962ED2A19686BD72,
	MemoryPackagePartOutputStream_get_CanWrite_m7485A45AEB08745325CB594069219785EC887C69,
	MemoryPackagePartOutputStream_get_CanSeek_m472A70C09626D12748D4307A44C46A76E4013589,
	MemoryPackagePartOutputStream_get_Length_mA6F906DD8725CCE0F99B6F62A7C2A29952D9C976,
	MemoryPackagePartOutputStream_Seek_m303D6335F2AB3FC27759A3ADA4AAC01B066895AE,
	MemoryPackagePartOutputStream_get_Position_mC7FE8A398820C34873F74F4AD67A45BBBE17EBC8,
	MemoryPackagePartOutputStream_set_Position_m73CAD3C23917CEC62DF59A7DBEF2FB03EB6E1DE2,
	MemoryPackagePartOutputStream_Close_m286ACA4E727EBE6E836CFE5DB0FAF8F63A541AF6,
	MemoryPackagePartOutputStream_Flush_m97B7ABAD34C9CAFD78C80A22F14B4546DD048DE1,
	MemoryPackagePartOutputStream_Write_mB69F110A3383BD2DE3C18CAC7FB65A5A239333AE,
	PackagePropertiesPart__ctor_mD66E4FBD5F267FA4AC8370CF34B6EDFD19CEF37A,
	PackagePropertiesPart_GetCategoryProperty_m007EDE04DB6D18839D452C923ACDB5F51A837256,
	PackagePropertiesPart_GetContentStatusProperty_m9CB340943A373E5AA9853BB019B58D8DACD4000A,
	PackagePropertiesPart_GetContentTypeProperty_m1A2C27496631B6D8588B8833F3C14D017A0D26B2,
	PackagePropertiesPart_GetCreatedProperty_mEC332351FDFD1EFE52B976E879AF79EE3A27C2BF,
	PackagePropertiesPart_GetCreatedPropertyString_m1AEAE5722715BCE56CFFE0FF7FCACCA268056C9B,
	PackagePropertiesPart_GetCreatorProperty_mB04D65232109085CB7A322D4B6DDAA54C740E009,
	PackagePropertiesPart_GetDescriptionProperty_mC2827B7B173CE596B00F4CD136648462B81A565E,
	PackagePropertiesPart_GetIdentifierProperty_m01725BB1843A5A61CC196DF39BA0AF6655B53A73,
	PackagePropertiesPart_GetKeywordsProperty_m6E195A6949FFF51FCAC639F8843365EFB39E8BCF,
	PackagePropertiesPart_GetLanguageProperty_m4284E00871650991018489461472D2F1574B7215,
	PackagePropertiesPart_GetLastModifiedByProperty_m6F0B3BB7071089F79FF172A14C264AC76E5884C8,
	PackagePropertiesPart_GetLastPrintedProperty_m56C51482A309EB80625E6F3E62274F799F9820CB,
	PackagePropertiesPart_GetLastPrintedPropertyString_m74D2634C9F537A00B661EE58BE6253F5820D2F67,
	PackagePropertiesPart_GetModifiedProperty_m0C951D272B41707FC9B4945BA1AF8BC67578FC76,
	PackagePropertiesPart_GetModifiedPropertyString_m453CE116FDA2C5920A087C91C1F3E36EE7CB7FD0,
	PackagePropertiesPart_GetRevisionProperty_mB4DC9D04BAC12940554F19B9A3EBAC9A22DB1A78,
	PackagePropertiesPart_GetSubjectProperty_mD716DA6E05B4BC7289A4D98FD6256D08021FC6BA,
	PackagePropertiesPart_GetTitleProperty_m3A02B95EC1CF4D7E7066F28672BA92062CF52CE8,
	PackagePropertiesPart_GetVersionProperty_m4343B98931AA392F3E939C56C7E45B2F3FEDD720,
	PackagePropertiesPart_SetCategoryProperty_mACE06F9E3F9E83CB06031DAE1D270FA881BBBCD3,
	PackagePropertiesPart_SetContentStatusProperty_m6540C7D0E6EDB090AE7C7D0843E89A7B8E56E1EC,
	PackagePropertiesPart_SetContentTypeProperty_m2CD8E66F178E1D0D120496E8AE8B0FE344DC3A10,
	PackagePropertiesPart_SetCreatedProperty_m2ABDB0629FFC457CD3BC4FE8212B5F5CC1B48746,
	PackagePropertiesPart_SetCreatorProperty_mF40D51F705F67F7AEC112B9057075B1D112A112A,
	PackagePropertiesPart_SetDescriptionProperty_m630728B656641D5428CEE2BCF0DBE793410436EB,
	PackagePropertiesPart_SetIdentifierProperty_mEC7DC1C968506577B01B0A30A1691164D3361A86,
	PackagePropertiesPart_SetKeywordsProperty_m2B95DC42FF2EFB05A193EE26C9FEB1FDA22A4495,
	PackagePropertiesPart_SetLanguageProperty_m4307317FF641BA3FD474517A6B731ADD3C2F8CD7,
	PackagePropertiesPart_SetLastModifiedByProperty_mD029455B8C477EAFD2B3E5C9F42F45963D5C5D8F,
	PackagePropertiesPart_SetLastPrintedProperty_m96095BD028ED7DF393E2C3D998F84EC8FA1E23C8,
	PackagePropertiesPart_SetModifiedProperty_m6413E1BC8FED9394CBC60BACBE169FFAAB0F5849,
	PackagePropertiesPart_SetRevisionProperty_m876C0D4060F392042ECE5BD6D52A6A3E030205B7,
	PackagePropertiesPart_SetSubjectProperty_m1FD1D042971431A04AE1FD036E9C3D0AB52040F4,
	PackagePropertiesPart_SetTitleProperty_m8656F3BFCAB86192E2F7E0DBBFF834E51F61CD5E,
	PackagePropertiesPart_SetVersionProperty_m948B3F1E9D6BEC08985404731882E2520EF33665,
	PackagePropertiesPart_SetStringValue_mA5B0F3907637E7F5ACA85DBF4E63003ACEF54518,
	PackagePropertiesPart_SetDateValue_m267BBB5AB065DE9F4818E2A5C00F706EE36255B3,
	PackagePropertiesPart_GetDateValue_m1BEA9153AB2FAE0B8B57CEEE3F247DC4481FBCDB,
	PackagePropertiesPart_GetInputStreamImpl_m8102D5D02F664DC669389B6A2C711A498890B4ED,
	PackagePropertiesPart_GetOutputStreamImpl_mCB83EDDDA785888C7131990CB5740C9568B26A2B,
	PackagePropertiesPart_Save_mD3CB20CFC446E183DF95B2BF79249B43B370B196,
	PackagePropertiesPart__cctor_m17163805CA54B0873FCA32F12B928E8EE2816436,
	NULL,
	PackagePropertiesUnmarshaller_Unmarshall_m1DFDA25357C04DEF523C48DAE5D64F0258744156,
	PackagePropertiesUnmarshaller_LoadCategory_m3ABCE3BD4C09693CAF8547B72845B4945A1EFDF0,
	PackagePropertiesUnmarshaller_LoadContentStatus_mCFFEBE6309ED455C57946488834CDB6EA7648B23,
	PackagePropertiesUnmarshaller_LoadContentType_mD59CDE2552C431954C173E39A27C32186662B2A6,
	PackagePropertiesUnmarshaller_LoadCreated_m1A93357B7BD6E73BD81809E3CCF7CF01320D4428,
	PackagePropertiesUnmarshaller_LoadCreator_m4043BA63D5772984F48D0677B6EE4972999E9716,
	PackagePropertiesUnmarshaller_LoadDescription_m0C81884D047EC38EEDAF830C08BEA17E9398DA4C,
	PackagePropertiesUnmarshaller_LoadIdentifier_m0AB3C9135381356161F84E2031DE1A35FFCCFD1C,
	PackagePropertiesUnmarshaller_LoadKeywords_m9CCBB0A08A92EE88D888ED444180F4C105584E54,
	PackagePropertiesUnmarshaller_LoadLanguage_m507955AF960349DE49049F8A1788CD033FAEAC95,
	PackagePropertiesUnmarshaller_LoadLastModifiedBy_mC7863306CFB79F3FB8693F55CC39D99949ECD72A,
	PackagePropertiesUnmarshaller_LoadLastPrinted_m4E423C1FDAD1B7E7C7049AE6737778CE0AD8AA5A,
	PackagePropertiesUnmarshaller_LoadModified_m48F7BCEFA556FC3CD28DA064F42EC155317A913C,
	PackagePropertiesUnmarshaller_LoadRevision_m15CF985F898BD9E1F8BC0FA7F2A94668191052B9,
	PackagePropertiesUnmarshaller_LoadSubject_mFEC1558263071EC75FD70E75FCC0ED3527CB4290,
	PackagePropertiesUnmarshaller_LoadTitle_mC58EC077333498FC1B135F06BE6ED9C717AE89A4,
	PackagePropertiesUnmarshaller_LoadVersion_mCBD152C8877F8841BCE7D12CEA57F5315AA49B04,
	PackagePropertiesUnmarshaller_CheckElementForOPCCompliance_m731D43C5BF93F162540DEB9E21555D9287E1EEC5,
	PackagePropertiesUnmarshaller__ctor_m62841C493D0C20789096E0D77A463FD0D7C9C972,
	PackagePropertiesUnmarshaller__cctor_m8EDADDECD26009524B3FB6C6885A2EA41BE7CD46,
	UnmarshallContext__ctor_mEF8351B9DA07D35469D5F30186ED4655120784DF,
	UnmarshallContext_get_Package_m67BA8C1348A8B394C4356907FA3D72F1F076F416,
	UnmarshallContext_get_PartName_m3C57262BA9CA175010AE969100E95AB412269952,
	UnmarshallContext_get_ZipEntry_m035909CE6FF1F68F5DAFA1B62C81EC19B549CB38,
	ZipContentTypeManager__ctor_m12FD825BFAE187E2F3EC71B2CEFB25C7B76F4BC0,
	ZipContentTypeManager_SaveImpl_mA57F907DEC096CCC3139673E83255B5CA35D2BEA,
	ZipContentTypeManager__cctor_mDF4DE5E83C66F612849D4EE5D54D28AFCBA28351,
	ZipHelper_GetCorePropertiesZipEntry_mD4CECD530CD71DA44F8C641150BDA953A98C9EAB,
	ZipHelper_GetOPCNameFromZipItemName_m676D77AAB2D5A6393C80B654D21C194B979BFF54,
	ZipHelper_GetZipItemNameFromOPCName_mB569A3EAB11667FCAED4DF2224370B59C17E89A3,
	ZipHelper_GetZipURIFromOPCName_mF1D8F3F72FED0A76347393706CC2709ACD22FC24,
	ZipHelper__cctor_m1AEA7DD2871F60625B2E24E5571914D41038FF52,
	OPCPackage__ctor_mD95C34675F2B43BA89DCDF87A9A0ACB09E089CD6,
	OPCPackage_Init_m2623BFE7DC1F9EC5E47CB27EC7E58BA24AEE98D9,
	OPCPackage_Open_m1D91ACF774E300E90D2F87F74681746532A68FB7,
	OPCPackage_ThrowExceptionIfReadOnly_m69736C3183972AD66D1990FFB5F488CDB8EDB0CF,
	OPCPackage_ThrowExceptionIfWriteOnly_mC5623AFC38D0798E2DDBF42955CFCE7C4E363C77,
	OPCPackage_GetPackageProperties_m76EC283FB5FB3FB627E641BDA31E463D942A371B,
	OPCPackage_GetPart_m7550E32E3DA094F0E92429D6C4C883D789A8320A,
	OPCPackage_GetPartsByRelationshipType_mFCDD18276119EACB40B08370BC0472605AAB5A0A,
	OPCPackage_GetPart_m6769F7CDCB4929026379454AAE2A3983C18E2425,
	OPCPackage_GetParts_m80C7F7DB5CC9E8140F91A4596511C1DA84D53B2A,
	OPCPackage_CreatePart_m372587D3911A6B69CC378AE51E959F44E9B53079,
	OPCPackage_CreatePart_m769C4CACD14CC721C722CCAFC94FF68706D54B05,
	OPCPackage_RemovePart_m22C03A14F5A209CCAAEB93950063F6DAE5BB51DA,
	OPCPackage_RemovePart_m2F876C973B893D8E773B47C7406FBF0652907748,
	OPCPackage_ContainPart_mFB84E7B3AAC5AAFCE61D67459A71CFF0DFE947B7,
	OPCPackage_AddRelationship_m17A9EA8BEB7D6C1DD6C894EF8034751E0ED5045B,
	OPCPackage_AddRelationship_mD288268E3052E5520B18C4F583CF084599213171,
	OPCPackage_get_Relationships_mA7BC97D9CD73E8EF0CC9CD9FCDA4BAB26FC62B88,
	OPCPackage_GetRelationshipsByType_m6A00A08F5C2FEA23607B3636BE2B2E7249F02A58,
	OPCPackage_GetRelationshipsHelper_m8EA4FDD69F9FB24866B8A02E2713781FD64408A8,
	OPCPackage_ClearRelationships_m16D4756A6AF5BD14091B2DF1780A2B3DD6861FA9,
	OPCPackage_EnsureRelationships_mEAB54F3C19D8305EAE7FACA0ABD642EF3C628FEC,
	OPCPackage_GetPackageAccess_m85000CB1009DF6DFD18897540B2B4346937456EC,
	OPCPackage_Save_m21B8E4B5C27DCEED5341CC58FB44C8E18A8D1DA5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	OPCPackage__cctor_m46373163C8513CB95E42854493204535324FCEA0,
	PackagePartCollection_Remove_m88DDDA1C9624F03A0643FDE3CE154153C09BD041,
	PackagePartCollection__ctor_mEC2F5212E19E9E32BDF3B80305DC7AE04BCD9DE1,
	PackagePartCollection__cctor_m29203A4D4174E6A7EC053C7115E3C896B1AA0B7D,
	PackagePartName__ctor_mE55FCFAEAF3FFE9474FFEA5429B951D6221D7145,
	PackagePartName_IsRelationshipPartURI_m91F380CEF8F36336352A2A30068154570040BFEA,
	PackagePartName_IsRelationshipPartURI_m9BCA0BD95D515525462541BC9B10B5FCF1E857F2,
	PackagePartName_ThrowExceptionIfInvalidPartUri_mA0569C268055D55036A412CC2B6EEFF5CF14632E,
	PackagePartName_ThrowExceptionIfEmptyURI_m32371A677A60BF58B12537F06FB4AA4F9C6A6CC3,
	PackagePartName_ThrowExceptionIfPartNameHaveInvalidSegments_mE2172BEF98A93E10A13C6441AA282840172CFE4F,
	PackagePartName_CheckPCharCompliance_mB8B3A8305FDE57A9ADA8983706F8BCA7623957A3,
	PackagePartName_ThrowExceptionIfPartNameNotStartsWithForwardSlashChar_m08156A103ABFD09C35B9C1F3FB7E70FF725C0303,
	PackagePartName_ThrowExceptionIfPartNameEndsWithForwardSlashChar_mD94723400426D683B28A4B15233CB0D617F79BE6,
	PackagePartName_ThrowExceptionIfAbsoluteUri_m1D31877C803CD103EC6F1EF77049385373E0BDB6,
	PackagePartName_Compare_mF7BED3A8A7CD14391FC719042F400EEC09C71CB6,
	PackagePartName_CompareTo_m19A4817BE18FB68C90048E52251A0D051A7D874C,
	PackagePartName_get_Extension_m94A111451C91BA04A1BE4E21D0C2955588FC17D1,
	PackagePartName_get_Name_m42D4E23FD298C2ABABEF741F36D6D86FD15F4141,
	PackagePartName_Equals_m31DFA0D017AD54FB1BA71E3BF81595EA020AC1DB,
	PackagePartName_GetHashCode_m7054467DFBE26824A96E20774FE30CD71207A252,
	PackagePartName_ToString_mF0F435463B1D1DEA93DF2308F10E7560FF63C768,
	PackagePartName_get_URI_m34453BA50987E5AC56C8D940CEA0E30FBB1F002A,
	PackagePartName__cctor_m8740C1CB412029409AD8F14044AD1498C0FA2F73,
	PackageRelationship__ctor_m7E360D6008E3638921B9DA46F19C36165246D143,
	PackageRelationship_Equals_m85DE06F2EC3095A534FF067A3573BFC2A4232BD3,
	PackageRelationship_GetHashCode_mCB0D57CC65B3094C0FE3B8E9399141554D2F97D9,
	PackageRelationship_get_Id_m47A35214238CCC25962903573C4776B8CFDE29FD,
	PackageRelationship_get_RelationshipType_m402C88CD26BF19F4DADF7A9DD9BDAE947CBFBCD8,
	PackageRelationship_get_SourceUri_mFEFDBA067B0748ADD6094B1BAEF1F35D6259C95C,
	PackageRelationship_get_TargetMode_mE161D2CF0E5EB2131F107AD78E476CEE9926CC7F,
	PackageRelationship_get_TargetUri_m62F5CEE7050B22603BF87625936FFDFA2967CC82,
	PackageRelationship_ToString_m3859F5ABF16F84D195FDCD975B3B60B03E975A61,
	PackageRelationship__cctor_m41484A9819972C6E0DC8111B887BC9BCB65CE259,
	PackageRelationshipCollection__ctor_m399D293B7954F1502165AAF9817C06F307BF533C,
	PackageRelationshipCollection__ctor_m207F1CDBC46BF37B18D498C5BECCEB83F8F9399E,
	PackageRelationshipCollection__ctor_mAA910013F3CC05E6D6A1369025C6A7E308270EDD,
	PackageRelationshipCollection__ctor_m056BD1940673BD3CE4AB58E42749E65A4028FC65,
	PackageRelationshipCollection__ctor_m3B6B28BF759850E459D67AEB6B82405F2E515A92,
	PackageRelationshipCollection_GetRelationshipPartName_mA9E6E1B0E1AB6ED13156D3F088556E7349E919E8,
	PackageRelationshipCollection_AddRelationship_mA02BBF2A509391CEF010C107E8E287242B693923,
	PackageRelationshipCollection_AddRelationship_m2BE51E4382BF59084214A851091638D8B40B9997,
	PackageRelationshipCollection_RemoveRelationship_m6DD10C598986D36222235DC0518ECB46D9C298B3,
	PackageRelationshipCollection_GetRelationship_mA7E97DB33D57C99044254D096040CC450641D225,
	PackageRelationshipCollection_GetRelationshipByID_mED2D93B9F935F47DD965CEC2BBA83A3592EEDD48,
	PackageRelationshipCollection_get_Size_m07B31165EA5078ACBB9F34F705F55C3079AD1906,
	PackageRelationshipCollection_ParseRelationshipsPart_m0D4EFE1EA3B439DDF13463B5CD77EEFC52287090,
	PackageRelationshipCollection_GetRelationships_m9845EEEBFEAF60B0ADA0D886582A2693944D444E,
	PackageRelationshipCollection_GetEnumerator_m53BF9AE33BD837BB6D7A43E43C0F246F38018A40,
	PackageRelationshipCollection_Clear_mFBE771D8C25B0C6EF92FED84ED02871125108A85,
	PackageRelationshipCollection_ToString_m93C42933C33B71A209F10A964889DDB998DC5099,
	PackageRelationshipCollection_System_Collections_Generic_IEnumeratorU3CNPOI_OpenXml4Net_OPC_PackageRelationshipU3E_get_Current_mFDFB6FA09641ED4060359192A122CE7761F280A7,
	PackageRelationshipCollection_System_IDisposable_Dispose_mEABACEC2ADFF7D2744B003A647676DC706406F2D,
	PackageRelationshipCollection_System_Collections_IEnumerator_get_Current_mA012AAC0325334979BD33957D83364B820C83F94,
	PackageRelationshipCollection_System_Collections_IEnumerator_MoveNext_m6A31E1C9F4DC775D2E67A738DD5147C1A15FF434,
	PackageRelationshipCollection_System_Collections_IEnumerator_Reset_m106BBD499E44C2C302AEE92F3AFE126735E2E484,
	PackageRelationshipCollection__cctor_m3A90DCD3F86027D3878DFB4DF71B943D625AA875,
	DuplicateComparer_Compare_m30C49CA90E47B6E211644B9CA10F7A86DECF7FC7,
	DuplicateComparer__ctor_m902589D343AB270FD209F7510009C0CA26D4979B,
	PackagingUriHelper__cctor_m598F10184D5F3EB1ACFEC5F04CB4D0DC07580C9C,
	PackagingUriHelper_ParseUri_m53B2631A7B867FA7744F052504B6B8C0DE6C7214,
	PackagingUriHelper_IsRelationshipPartURI_mD20E0A8B671BC51C8B0CDC9DBD906C96D7D9BBE9,
	PackagingUriHelper_GetFilename_mBC04F32AFDD855BBA224A3CA98F5F8CEF07FD3FD,
	PackagingUriHelper_GetFilenameWithoutExtension_m3993070825BE54416B33D545CF7C593A9C4C072F,
	PackagingUriHelper_Combine_m7B59E30AD8A57E71FDDF29C65FEDAF0E2B7F8B75,
	PackagingUriHelper_RelativizeUri_mC230B29439DD8CAF6376674620A40CA8F60744A5,
	PackagingUriHelper_ResolvePartUri_mAD6B46BE909E24D5C361A71B80869C0CA8255499,
	PackagingUriHelper_GetURIFromPath_m9C0B164DFADAF272DC195CC051BE8B0F8DBDEE20,
	PackagingUriHelper_GetSourcePartUriFromRelationshipPartUri_mBBBFEE645C1359E8C677C6EF775F975E0EFCB3FA,
	PackagingUriHelper_CreatePartName_m1AAB63929F4598559F58E044DC1AB3AB71D22AF9,
	PackagingUriHelper_CreatePartName_mEA17599A2BEAB71EBCE22F3249D8457A4C61CD7A,
	PackagingUriHelper_ToUri_m483DB899A7F95CEEBF457318EB411443612CEBDE,
	PackagingUriHelper_Encode_mDB6877181EA6609DDF0375DF263002DDCAA460D7,
	PackagingUriHelper_IsUnsafe_mEB6AC440C9CF7031C1450BB8EDDC2AEE10962177,
	PackagingUriHelper_GetRelationshipPartName_m5AB1D0D71E38B1B78BD344C945BA1913884A5865,
	StreamHelper_SaveXmlInStream_m463CE67E8D183075D84DB21FE5F2AD13DF41135A,
	StreamHelper_CopyStream_mB94A2BDC2D0B9D905C2CFC4A71A669FA638EE899,
	ZipPackage__ctor_mA0BE696D39265A43C040F826E0007021D6AE67D9,
	ZipPackage_GetPartsImpl_m284DB52F3BFDFB334C228F4E0BC698F07EF2B92A,
	ZipPackage_BuildPartName_mD0A347092ED9341CA819B01A65DAD1A8559C02DC,
	ZipPackage_CreatePartImpl_m324444D4082CFD20ACCBCEE7C75F072EE912AE77,
	ZipPackage_RemovePartImpl_m6464D769F2353ADB5C29761E38C521C7F4ECC565,
	ZipPackage_GetPartImpl_mCF6297F818376471F0F1CE3C7DC6C570B0C0183A,
	ZipPackage_SaveImpl_mBBC269FE0EDD414A457BA4CEE6A09D318E64F9E0,
	ZipPackage_get_ZipArchive_m10D15C36BB5748B965A90335CAA31A8510BF738F,
	ZipPackage__cctor_mAD004B5F22ECD259EE7EF32B43F2316C9E055060,
	ZipPackagePart__ctor_mCF2927135F5383CE87ED4C9D16722B7746ABA6EE,
	ZipPackagePart_GetInputStreamImpl_mEB7D7F5177101586D32EBEFD82EF0E2DDD1E051C,
	ZipPackagePart_GetOutputStreamImpl_mE36C5007D0E238A6071BF38EBFE8A02AF8140532,
	ZipPackagePart_Save_m3A1825F7BE718F2D1FB8BD26875A3704CF00E579,
	XmlHelper_ReadInt_m7955A6B33C55D0833259CD6B0D9E161D15EE841F,
	XmlHelper_ReadString_m2C362A9D2D902C34D5E2DC3235A815A575B959A0,
	XmlHelper_ReadUInt_mFFBCC8A25F064FAC392F13E4CA9C5E76A30DD3C4,
	XmlHelper_ReadBool_m3D5A026A46892B9E4F197C166CA4B8A0502FA570,
	XmlHelper_ReadDouble_m6B6BFF1C927BB3D8B8B9DE21E662E3DFDF3D6BC0,
	XmlHelper_ReadBool_mF4AA57F64F62E86F0CB86611E2435A9C648BE81F,
	XmlHelper_ExcelEncodeString_mB7E531EB1A714F3D7BD5F5ED7DD2FE5CA81C5386,
	XmlHelper_EncodeXml_m1B89F00830C4C08B195E4DB519740068767E52D0,
	XmlHelper_WriteAttribute_m5638F21D81D50B847348FB1DE2402057015B0B14,
	XmlHelper_WriteAttribute_m2855EDA0DC2AAF185BE60632C0B31182FD1CC6F3,
	XmlHelper_WriteAttribute_m8E1F05D6C6D031224087C6EB39E2243BA6865A2B,
	XmlHelper_WriteAttribute_mA4FCA24128591613E864DF418BAEDECE4614052D,
	XmlHelper_WriteAttribute_mF1E1FC2FFC49B76B28CDA2160A7C5B32A99CD7C0,
	XmlHelper_WriteAttribute_m577928DCB30B924424D1CAB5A0C9182CB9FA47F5,
	XmlHelper_WriteAttribute_mFEE74764D9045F1B3DE0D43A2515B583D2894FD0,
	XmlHelper_WriteAttribute_mA225E92AC868351AB5496238ED55F9BC297884FE,
	XmlHelper_WriteAttribute_mB6041910EE9F7304F419505533F4C3DD1289ACB6,
	XmlHelper_WriteAttribute_m3C29E6BA7B239B9DD8CBA6D7F58F3B934DAC78BC,
	XmlHelper_ReadBytes_m33614A50F8D9DA6A1D24E449C86EA051B3D64E23,
	XmlHelper_ReadByte_mDFCEE5D2D4103371B0D815C13B3DDC76CB9C3F23,
	NULL,
	NULL,
	ZipInputStreamZipEntrySource__ctor_mFCCB0B8492259A3683188C721A3F9A07E0A043B8,
	ZipInputStreamZipEntrySource_get_Entries_m1FD0F8AE76CC69F5BCE4278603660230324A4809,
	ZipInputStreamZipEntrySource_GetInputStream_mC2021D15884767B523E1ABB302DA4E3EAC9F92FD,
	EntryEnumerator__ctor_mFD2EDD3B50698DD5FAC6748F78CD84F35DC88AE3,
	EntryEnumerator_MoveNext_m4A5B59475FD4B85B7E3F81326C785DC12BE4B1E0,
	EntryEnumerator_get_Current_m44A4931D38D87D2E14C877226004995116B3CC2C,
	EntryEnumerator_Reset_mB951FD8921557A7594D3B646C4B64C92B29D62E3,
	FakeZipEntry__ctor_mC969931CBAB0F7FEA57952937D9955AD7F663703,
	FakeZipEntry_GetInputStream_m21D3022D6E41C4E29B71411D591A7850BA3D7001,
};
static const int32_t s_InvokerIndices[320] = 
{
	26,
	26,
	26,
	3,
	3,
	26,
	14,
	9,
	10,
	14,
	14,
	112,
	27,
	27,
	27,
	27,
	26,
	9,
	28,
	26,
	9,
	2128,
	2129,
	90,
	90,
	90,
	23,
	90,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	90,
	23,
	90,
	363,
	23,
	3,
	209,
	967,
	209,
	105,
	215,
	490,
	142,
	23,
	26,
	14,
	28,
	28,
	89,
	14,
	14,
	23,
	23,
	14,
	14,
	14,
	89,
	89,
	31,
	14,
	14,
	14,
	9,
	967,
	14,
	14,
	9,
	26,
	512,
	89,
	89,
	89,
	184,
	742,
	184,
	210,
	23,
	23,
	35,
	27,
	14,
	14,
	14,
	1116,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	1116,
	14,
	1116,
	14,
	14,
	14,
	14,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	28,
	2130,
	2131,
	14,
	14,
	9,
	3,
	105,
	105,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	26,
	23,
	3,
	27,
	14,
	14,
	14,
	27,
	90,
	3,
	0,
	0,
	0,
	0,
	3,
	32,
	23,
	0,
	23,
	23,
	14,
	28,
	28,
	28,
	14,
	105,
	152,
	26,
	26,
	9,
	142,
	490,
	14,
	28,
	28,
	23,
	23,
	10,
	26,
	152,
	26,
	26,
	28,
	14,
	3,
	26,
	23,
	3,
	459,
	9,
	89,
	163,
	163,
	163,
	163,
	163,
	163,
	163,
	41,
	112,
	14,
	14,
	9,
	10,
	14,
	14,
	3,
	1181,
	9,
	10,
	14,
	14,
	14,
	2132,
	14,
	14,
	3,
	23,
	27,
	26,
	26,
	27,
	0,
	26,
	142,
	26,
	34,
	28,
	10,
	26,
	28,
	14,
	23,
	14,
	14,
	23,
	14,
	89,
	23,
	3,
	41,
	23,
	3,
	119,
	114,
	0,
	0,
	1,
	216,
	1,
	0,
	0,
	0,
	0,
	0,
	0,
	46,
	0,
	137,
	137,
	130,
	14,
	28,
	152,
	26,
	28,
	26,
	14,
	3,
	446,
	14,
	14,
	9,
	94,
	0,
	94,
	114,
	282,
	629,
	0,
	0,
	109,
	1006,
	2133,
	2134,
	735,
	207,
	198,
	844,
	198,
	207,
	0,
	114,
	14,
	28,
	26,
	14,
	28,
	26,
	89,
	14,
	23,
	27,
	14,
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationNPOI_OpenXml4Net;
extern const Il2CppCodeGenModule g_NPOI_OpenXml4NetCodeGenModule;
const Il2CppCodeGenModule g_NPOI_OpenXml4NetCodeGenModule = 
{
	"NPOI.OpenXml4Net.dll",
	320,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	&g_DebuggerMetadataRegistrationNPOI_OpenXml4Net,
};
