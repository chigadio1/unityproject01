﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#else
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#else
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[16] = 
{
	{ 0, 0 } /* 0x06000001 System.Void UnityEngine.AssetBundle::.ctor() */,
	{ 0, 0 } /* 0x06000002 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync_Internal(System.String,System.UInt32,System.UInt64) */,
	{ 0, 0 } /* 0x06000003 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String) */,
	{ 0, 0 } /* 0x06000004 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String,System.UInt32) */,
	{ 0, 0 } /* 0x06000005 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type) */,
	{ 0, 0 } /* 0x06000006 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync(System.String,System.Type) */,
	{ 0, 0 } /* 0x06000007 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync() */,
	{ 0, 0 } /* 0x06000008 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync(System.Type) */,
	{ 0, 0 } /* 0x06000009 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type) */,
	{ 0, 0 } /* 0x0600000A System.Void UnityEngine.AssetBundle::Unload(System.Boolean) */,
	{ 0, 0 } /* 0x0600000B UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type) */,
	{ 0, 0 } /* 0x0600000C UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle() */,
	{ 0, 0 } /* 0x0600000D System.Void UnityEngine.AssetBundleCreateRequest::.ctor() */,
	{ 0, 0 } /* 0x0600000E UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset() */,
	{ 0, 0 } /* 0x0600000F UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets() */,
	{ 0, 0 } /* 0x06000010 System.Void UnityEngine.AssetBundleRequest::.ctor() */,
};
#else
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[1] = { { 0, 0} };
#endif
#if IL2CPP_MONO_DEBUGGER
extern Il2CppSequencePoint g_sequencePointsUnityEngine_AssetBundleModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_AssetBundleModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#else
extern Il2CppSequencePoint g_sequencePointsUnityEngine_AssetBundleModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_AssetBundleModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#else
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#else
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#else
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#else
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[16] = 
{
	{ 0, 0, 0 } /* System.Void UnityEngine.AssetBundle::.ctor() */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync_Internal(System.String,System.UInt32,System.UInt64) */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String) */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String,System.UInt32) */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type) */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync(System.String,System.Type) */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync() */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync(System.Type) */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AssetBundle::Unload(System.Boolean) */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type) */,
	{ 0, 0, 0 } /* UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AssetBundleCreateRequest::.ctor() */,
	{ 0, 0, 0 } /* UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset() */,
	{ 0, 0, 0 } /* UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AssetBundleRequest::.ctor() */,
};
#else
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[1] = { { 0, 0, 0 } };
#endif
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_AssetBundleModule;
const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_AssetBundleModule = 
{
	(Il2CppMethodExecutionContextInfo*)g_methodExecutionContextInfos,
	(Il2CppMethodExecutionContextInfoIndex*)g_methodExecutionContextInfoIndexes,
	(Il2CppMethodScope*)g_methodScopes,
	(Il2CppMethodHeaderInfo*)g_methodHeaderInfos,
	(Il2CppSequencePointSourceFile*)g_sequencePointSourceFiles,
	0,
	(Il2CppSequencePoint*)g_sequencePointsUnityEngine_AssetBundleModule,
	0,
	(Il2CppCatchPoint*)g_catchPoints,
	0,
	(Il2CppTypeSourceFilePair*)g_typeSourceFiles,
	(const char**)g_methodExecutionContextInfoStrings,
};
