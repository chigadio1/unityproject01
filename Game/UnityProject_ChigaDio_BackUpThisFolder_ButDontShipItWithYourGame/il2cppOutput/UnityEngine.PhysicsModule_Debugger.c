﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#else
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#else
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[124] = 
{
	{ 0, 0 } /* 0x06000001 UnityEngine.Collider UnityEngine.RaycastHit::get_collider() */,
	{ 0, 0 } /* 0x06000002 UnityEngine.Vector3 UnityEngine.RaycastHit::get_point() */,
	{ 0, 0 } /* 0x06000003 UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal() */,
	{ 0, 0 } /* 0x06000004 System.Single UnityEngine.RaycastHit::get_distance() */,
	{ 0, 0 } /* 0x06000005 System.Void UnityEngine.RaycastHit::set_distance(System.Single) */,
	{ 0, 0 } /* 0x06000006 UnityEngine.Transform UnityEngine.RaycastHit::get_transform() */,
	{ 0, 0 } /* 0x06000007 UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody() */,
	{ 0, 0 } /* 0x06000008 System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean) */,
	{ 0, 0 } /* 0x06000009 System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints) */,
	{ 0, 0 } /* 0x0600000A System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode) */,
	{ 0, 0 } /* 0x0600000B System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode) */,
	{ 0, 0 } /* 0x0600000C System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x0600000D System.Void UnityEngine.Rigidbody::.ctor() */,
	{ 0, 0 } /* 0x0600000E System.Void UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode) */,
	{ 0, 0 } /* 0x0600000F System.Void UnityEngine.Rigidbody::AddTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode) */,
	{ 0, 0 } /* 0x06000010 System.Boolean UnityEngine.Collider::get_enabled() */,
	{ 0, 0 } /* 0x06000011 UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody() */,
	{ 0, 0 } /* 0x06000012 UnityEngine.Vector3 UnityEngine.Collider::ClosestPoint(UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x06000013 System.Void UnityEngine.Collider::.ctor() */,
	{ 0, 0 } /* 0x06000014 System.Void UnityEngine.Collider::ClosestPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&) */,
	{ 0, 0 } /* 0x06000015 UnityEngine.Mesh UnityEngine.MeshCollider::get_sharedMesh() */,
	{ 0, 0 } /* 0x06000016 UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center() */,
	{ 0, 0 } /* 0x06000017 System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x06000018 System.Single UnityEngine.CapsuleCollider::get_radius() */,
	{ 0, 0 } /* 0x06000019 System.Void UnityEngine.CapsuleCollider::set_radius(System.Single) */,
	{ 0, 0 } /* 0x0600001A System.Single UnityEngine.CapsuleCollider::get_height() */,
	{ 0, 0 } /* 0x0600001B System.Void UnityEngine.CapsuleCollider::set_height(System.Single) */,
	{ 0, 0 } /* 0x0600001C System.Int32 UnityEngine.CapsuleCollider::get_direction() */,
	{ 0, 0 } /* 0x0600001D System.Void UnityEngine.CapsuleCollider::set_direction(System.Int32) */,
	{ 0, 0 } /* 0x0600001E UnityEngine.Vector2 UnityEngine.CapsuleCollider::GetGlobalExtents() */,
	{ 0, 0 } /* 0x0600001F UnityEngine.Matrix4x4 UnityEngine.CapsuleCollider::CalculateTransform() */,
	{ 0, 0 } /* 0x06000020 System.Void UnityEngine.CapsuleCollider::.ctor() */,
	{ 0, 0 } /* 0x06000021 System.Void UnityEngine.CapsuleCollider::get_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0 } /* 0x06000022 System.Void UnityEngine.CapsuleCollider::set_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0 } /* 0x06000023 System.Void UnityEngine.CapsuleCollider::GetGlobalExtents_Injected(UnityEngine.Vector2&) */,
	{ 0, 0 } /* 0x06000024 System.Void UnityEngine.CapsuleCollider::CalculateTransform_Injected(UnityEngine.Matrix4x4&) */,
	{ 0, 0 } /* 0x06000025 UnityEngine.Vector3 UnityEngine.BoxCollider::get_center() */,
	{ 0, 0 } /* 0x06000026 UnityEngine.Vector3 UnityEngine.BoxCollider::get_size() */,
	{ 0, 0 } /* 0x06000027 System.Void UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0 } /* 0x06000028 System.Void UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&) */,
	{ 0, 0 } /* 0x06000029 UnityEngine.Vector3 UnityEngine.SphereCollider::get_center() */,
	{ 0, 0 } /* 0x0600002A System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x0600002B System.Single UnityEngine.SphereCollider::get_radius() */,
	{ 0, 0 } /* 0x0600002C System.Void UnityEngine.SphereCollider::set_radius(System.Single) */,
	{ 0, 0 } /* 0x0600002D System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0 } /* 0x0600002E System.Void UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0 } /* 0x0600002F System.String UnityEngine.PhysicsScene::ToString() */,
	{ 0, 0 } /* 0x06000030 System.Int32 UnityEngine.PhysicsScene::GetHashCode() */,
	{ 0, 0 } /* 0x06000031 System.Boolean UnityEngine.PhysicsScene::Equals(System.Object) */,
	{ 0, 0 } /* 0x06000032 System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene) */,
	{ 0, 0 } /* 0x06000033 System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000034 System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000035 System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000036 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000037 System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000038 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000039 System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600003A System.Boolean UnityEngine.PhysicsScene::Internal_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600003B System.Boolean UnityEngine.PhysicsScene::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600003C System.Boolean UnityEngine.PhysicsScene::Query_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600003D System.Boolean UnityEngine.PhysicsScene::Internal_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600003E System.Boolean UnityEngine.PhysicsScene::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600003F System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000040 System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000041 System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000042 System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000043 System.Boolean UnityEngine.PhysicsScene::Query_SphereCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000044 UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene() */,
	{ 0, 0 } /* 0x06000045 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000046 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32) */,
	{ 0, 0 } /* 0x06000047 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single) */,
	{ 0, 0 } /* 0x06000048 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x06000049 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600004A System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32) */,
	{ 0, 0 } /* 0x0600004B System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single) */,
	{ 0, 0 } /* 0x0600004C System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&) */,
	{ 0, 0 } /* 0x0600004D System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600004E System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32) */,
	{ 0, 0 } /* 0x0600004F System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single) */,
	{ 0, 0 } /* 0x06000050 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray) */,
	{ 0, 0 } /* 0x06000051 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000052 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32) */,
	{ 0, 0 } /* 0x06000053 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single) */,
	{ 0, 0 } /* 0x06000054 System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&) */,
	{ 0, 0 } /* 0x06000055 System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000056 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000057 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000058 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32) */,
	{ 0, 0 } /* 0x06000059 System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600005A System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single) */,
	{ 0, 0 } /* 0x0600005B UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600005C UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600005D UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32) */,
	{ 0, 0 } /* 0x0600005E UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single) */,
	{ 0, 0 } /* 0x0600005F UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3) */,
	{ 0, 0 } /* 0x06000060 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000061 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32) */,
	{ 0, 0 } /* 0x06000062 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single) */,
	{ 0, 0 } /* 0x06000063 UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray) */,
	{ 0, 0 } /* 0x06000064 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000065 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32) */,
	{ 0, 0 } /* 0x06000066 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single) */,
	{ 0, 0 } /* 0x06000067 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[]) */,
	{ 0, 0 } /* 0x06000068 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000069 System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32) */,
	{ 0, 0 } /* 0x0600006A System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single) */,
	{ 0, 0 } /* 0x0600006B System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[]) */,
	{ 0, 0 } /* 0x0600006C UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600006D UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600006E UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single) */,
	{ 0, 0 } /* 0x0600006F UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000070 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000071 UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single) */,
	{ 0, 0 } /* 0x06000072 System.Boolean UnityEngine.Physics::Query_ComputePenetration(UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&) */,
	{ 0, 0 } /* 0x06000073 System.Boolean UnityEngine.Physics::ComputePenetration(UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&) */,
	{ 0, 0 } /* 0x06000074 UnityEngine.Collider[] UnityEngine.Physics::OverlapBox_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000075 UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000076 UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion) */,
	{ 0, 0 } /* 0x06000077 System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&) */,
	{ 0, 0 } /* 0x06000078 UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x06000079 UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600007A UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0 } /* 0x0600007B System.Boolean UnityEngine.Physics::Query_ComputePenetration_Injected(UnityEngine.Collider,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Collider,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&) */,
	{ 0, 0 } /* 0x0600007C UnityEngine.Collider[] UnityEngine.Physics::OverlapBox_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
};
#else
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[1] = { { 0, 0} };
#endif
#if IL2CPP_MONO_DEBUGGER
extern Il2CppSequencePoint g_sequencePointsUnityEngine_PhysicsModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_PhysicsModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#else
extern Il2CppSequencePoint g_sequencePointsUnityEngine_PhysicsModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_PhysicsModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#else
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#else
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#else
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#else
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[124] = 
{
	{ 0, 0, 0 } /* UnityEngine.Collider UnityEngine.RaycastHit::get_collider() */,
	{ 0, 0, 0 } /* UnityEngine.Vector3 UnityEngine.RaycastHit::get_point() */,
	{ 0, 0, 0 } /* UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal() */,
	{ 0, 0, 0 } /* System.Single UnityEngine.RaycastHit::get_distance() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.RaycastHit::set_distance(System.Single) */,
	{ 0, 0, 0 } /* UnityEngine.Transform UnityEngine.RaycastHit::get_transform() */,
	{ 0, 0, 0 } /* UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Rigidbody::set_constraints(UnityEngine.RigidbodyConstraints) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Rigidbody::.ctor() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Rigidbody::AddForce_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Rigidbody::AddTorque_Injected(UnityEngine.Vector3&,UnityEngine.ForceMode) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Collider::get_enabled() */,
	{ 0, 0, 0 } /* UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody() */,
	{ 0, 0, 0 } /* UnityEngine.Vector3 UnityEngine.Collider::ClosestPoint(UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Collider::.ctor() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Collider::ClosestPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&) */,
	{ 0, 0, 0 } /* UnityEngine.Mesh UnityEngine.MeshCollider::get_sharedMesh() */,
	{ 0, 0, 0 } /* UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* System.Single UnityEngine.CapsuleCollider::get_radius() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.CapsuleCollider::set_radius(System.Single) */,
	{ 0, 0, 0 } /* System.Single UnityEngine.CapsuleCollider::get_height() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.CapsuleCollider::set_height(System.Single) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.CapsuleCollider::get_direction() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.CapsuleCollider::set_direction(System.Int32) */,
	{ 0, 0, 0 } /* UnityEngine.Vector2 UnityEngine.CapsuleCollider::GetGlobalExtents() */,
	{ 0, 0, 0 } /* UnityEngine.Matrix4x4 UnityEngine.CapsuleCollider::CalculateTransform() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.CapsuleCollider::.ctor() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.CapsuleCollider::get_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.CapsuleCollider::set_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.CapsuleCollider::GetGlobalExtents_Injected(UnityEngine.Vector2&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.CapsuleCollider::CalculateTransform_Injected(UnityEngine.Matrix4x4&) */,
	{ 0, 0, 0 } /* UnityEngine.Vector3 UnityEngine.BoxCollider::get_center() */,
	{ 0, 0, 0 } /* UnityEngine.Vector3 UnityEngine.BoxCollider::get_size() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.BoxCollider::get_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.BoxCollider::get_size_Injected(UnityEngine.Vector3&) */,
	{ 0, 0, 0 } /* UnityEngine.Vector3 UnityEngine.SphereCollider::get_center() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.SphereCollider::set_center(UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* System.Single UnityEngine.SphereCollider::get_radius() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.SphereCollider::set_radius(System.Single) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.SphereCollider::get_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.SphereCollider::set_center_Injected(UnityEngine.Vector3&) */,
	{ 0, 0, 0 } /* System.String UnityEngine.PhysicsScene::ToString() */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.PhysicsScene::GetHashCode() */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Equals(System.Object) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Equals(UnityEngine.PhysicsScene) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Internal_Raycast(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.PhysicsScene::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc(UnityEngine.PhysicsScene,UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Internal_CapsuleCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Query_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Internal_SphereCast(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Internal_RaycastTest_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Internal_Raycast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.PhysicsScene::Internal_RaycastNonAlloc_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Query_CapsuleCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.PhysicsScene::Query_SphereCast_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,System.Single,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.PhysicsScene UnityEngine.Physics::get_defaultPhysicsScene() */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&,System.Single) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll(UnityEngine.PhysicsScene,UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single,System.Int32) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[],System.Single) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit[]) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single,System.Int32) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[],System.Single) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.Physics::RaycastNonAlloc(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit[]) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule(UnityEngine.Vector3,UnityEngine.Vector3,System.Single) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Query_ComputePenetration(UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::ComputePenetration(UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Collider,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapBox_Internal(UnityEngine.PhysicsScene,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapBox(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Physics::get_defaultPhysicsScene_Injected(UnityEngine.PhysicsScene&) */,
	{ 0, 0, 0 } /* UnityEngine.RaycastHit[] UnityEngine.Physics::Internal_RaycastAll_Injected(UnityEngine.PhysicsScene&,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapCapsule_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.Physics::Query_ComputePenetration_Injected(UnityEngine.Collider,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Collider,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&) */,
	{ 0, 0, 0 } /* UnityEngine.Collider[] UnityEngine.Physics::OverlapBox_Internal_Injected(UnityEngine.PhysicsScene&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.QueryTriggerInteraction) */,
};
#else
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[1] = { { 0, 0, 0 } };
#endif
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_PhysicsModule;
const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_PhysicsModule = 
{
	(Il2CppMethodExecutionContextInfo*)g_methodExecutionContextInfos,
	(Il2CppMethodExecutionContextInfoIndex*)g_methodExecutionContextInfoIndexes,
	(Il2CppMethodScope*)g_methodScopes,
	(Il2CppMethodHeaderInfo*)g_methodHeaderInfos,
	(Il2CppSequencePointSourceFile*)g_sequencePointSourceFiles,
	0,
	(Il2CppSequencePoint*)g_sequencePointsUnityEngine_PhysicsModule,
	0,
	(Il2CppCatchPoint*)g_catchPoints,
	0,
	(Il2CppTypeSourceFilePair*)g_typeSourceFiles,
	(const char**)g_methodExecutionContextInfoStrings,
};
