﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#else
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#else
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[16] = 
{
	{ 0, 0 } /* 0x06000001 System.Boolean UnityEngine.XR.XRSettings::get_enabled() */,
	{ 0, 0 } /* 0x06000002 System.Boolean UnityEngine.XR.XRSettings::get_isDeviceActive() */,
	{ 0, 0 } /* 0x06000003 System.Single UnityEngine.XR.XRSettings::get_eyeTextureResolutionScale() */,
	{ 0, 0 } /* 0x06000004 System.Void UnityEngine.XR.XRSettings::set_eyeTextureResolutionScale(System.Single) */,
	{ 0, 0 } /* 0x06000005 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureWidth() */,
	{ 0, 0 } /* 0x06000006 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureHeight() */,
	{ 0, 0 } /* 0x06000007 UnityEngine.RenderTextureDescriptor UnityEngine.XR.XRSettings::get_eyeTextureDesc() */,
	{ 0, 0 } /* 0x06000008 System.Single UnityEngine.XR.XRSettings::get_renderViewportScale() */,
	{ 0, 0 } /* 0x06000009 System.Single UnityEngine.XR.XRSettings::get_renderViewportScaleInternal() */,
	{ 0, 0 } /* 0x0600000A System.String UnityEngine.XR.XRSettings::get_loadedDeviceName() */,
	{ 0, 0 } /* 0x0600000B System.String[] UnityEngine.XR.XRSettings::get_supportedDevices() */,
	{ 0, 0 } /* 0x0600000C UnityEngine.XR.XRSettings/StereoRenderingMode UnityEngine.XR.XRSettings::get_stereoRenderingMode() */,
	{ 0, 0 } /* 0x0600000D System.Void UnityEngine.XR.XRSettings::get_eyeTextureDesc_Injected(UnityEngine.RenderTextureDescriptor&) */,
	{ 0, 0 } /* 0x0600000E System.Void UnityEngine.XR.XRDevice::DisableAutoXRCameraTracking(UnityEngine.Camera,System.Boolean) */,
	{ 0, 0 } /* 0x0600000F System.Void UnityEngine.XR.XRDevice::InvokeDeviceLoaded(System.String) */,
	{ 0, 0 } /* 0x06000010 System.Void UnityEngine.XR.XRDevice::.cctor() */,
};
#else
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[1] = { { 0, 0} };
#endif
#if IL2CPP_MONO_DEBUGGER
extern Il2CppSequencePoint g_sequencePointsUnityEngine_VRModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_VRModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#else
extern Il2CppSequencePoint g_sequencePointsUnityEngine_VRModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_VRModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#else
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#else
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#else
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#else
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[16] = 
{
	{ 0, 0, 0 } /* System.Boolean UnityEngine.XR.XRSettings::get_enabled() */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.XR.XRSettings::get_isDeviceActive() */,
	{ 0, 0, 0 } /* System.Single UnityEngine.XR.XRSettings::get_eyeTextureResolutionScale() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.XR.XRSettings::set_eyeTextureResolutionScale(System.Single) */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureWidth() */,
	{ 0, 0, 0 } /* System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureHeight() */,
	{ 0, 0, 0 } /* UnityEngine.RenderTextureDescriptor UnityEngine.XR.XRSettings::get_eyeTextureDesc() */,
	{ 0, 0, 0 } /* System.Single UnityEngine.XR.XRSettings::get_renderViewportScale() */,
	{ 0, 0, 0 } /* System.Single UnityEngine.XR.XRSettings::get_renderViewportScaleInternal() */,
	{ 0, 0, 0 } /* System.String UnityEngine.XR.XRSettings::get_loadedDeviceName() */,
	{ 0, 0, 0 } /* System.String[] UnityEngine.XR.XRSettings::get_supportedDevices() */,
	{ 0, 0, 0 } /* UnityEngine.XR.XRSettings/StereoRenderingMode UnityEngine.XR.XRSettings::get_stereoRenderingMode() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.XR.XRSettings::get_eyeTextureDesc_Injected(UnityEngine.RenderTextureDescriptor&) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.XR.XRDevice::DisableAutoXRCameraTracking(UnityEngine.Camera,System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.XR.XRDevice::InvokeDeviceLoaded(System.String) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.XR.XRDevice::.cctor() */,
};
#else
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[1] = { { 0, 0, 0 } };
#endif
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_VRModule;
const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_VRModule = 
{
	(Il2CppMethodExecutionContextInfo*)g_methodExecutionContextInfos,
	(Il2CppMethodExecutionContextInfoIndex*)g_methodExecutionContextInfoIndexes,
	(Il2CppMethodScope*)g_methodScopes,
	(Il2CppMethodHeaderInfo*)g_methodHeaderInfos,
	(Il2CppSequencePointSourceFile*)g_sequencePointSourceFiles,
	0,
	(Il2CppSequencePoint*)g_sequencePointsUnityEngine_VRModule,
	0,
	(Il2CppCatchPoint*)g_catchPoints,
	0,
	(Il2CppTypeSourceFilePair*)g_typeSourceFiles,
	(const char**)g_methodExecutionContextInfoStrings,
};
