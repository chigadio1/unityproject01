﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void SharpZipBaseException__ctor_m8C574D72A2DBB387770CEE2BC7793B994C052C31 (void);
// 0x00000002 System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor()
extern void SharpZipBaseException__ctor_m5D31F40276F9D34FA727D5B35D48EBF1EE898504 (void);
// 0x00000003 System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.String)
extern void SharpZipBaseException__ctor_m5F7B81277BF8529B1E21F028931A63E529AA723B (void);
// 0x00000004 System.Int64 ICSharpCode.SharpZipLib.Checksums.Adler32::get_Value()
extern void Adler32_get_Value_m0E563BB11949AA0ABEEF1D6045F54D283D8FD316 (void);
// 0x00000005 System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::.ctor()
extern void Adler32__ctor_m189E519C7C20A7DEB2195594F80F18E9F89DD034 (void);
// 0x00000006 System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Reset()
extern void Adler32_Reset_m8DFF36A606BCE10AC1D7D9E4FD025F1565D94756 (void);
// 0x00000007 System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Update(System.Byte[],System.Int32,System.Int32)
extern void Adler32_Update_m92B4A996CEE421564653C8643D6EBC571211259A (void);
// 0x00000008 System.UInt32 ICSharpCode.SharpZipLib.Checksums.Crc32::ComputeCrc32(System.UInt32,System.Byte)
extern void Crc32_ComputeCrc32_mDB88CD370CD8606FA3112FC1FE5B4E6BFAF47987 (void);
// 0x00000009 System.Int64 ICSharpCode.SharpZipLib.Checksums.Crc32::get_Value()
extern void Crc32_get_Value_m21991A1B6074434262599FEF1AC1B3B39465D5FB (void);
// 0x0000000A System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Reset()
extern void Crc32_Reset_m4F178EF359E8EF2200516B86E1C2892A7D2FBCE6 (void);
// 0x0000000B System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Byte[],System.Int32,System.Int32)
extern void Crc32_Update_m98B8261D6293B3C258210360FAD2A7851EF346CE (void);
// 0x0000000C System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.ctor()
extern void Crc32__ctor_mAA446A1931D198D734D8AD7423C82EFCD23D78B9 (void);
// 0x0000000D System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.cctor()
extern void Crc32__cctor_m4CED53AD61612D3ADE805E75088E9DB3CF7BF2AF (void);
// 0x0000000E System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassic::GenerateKeys(System.Byte[])
extern void PkzipClassic_GenerateKeys_mC87C1A9BC91653FB18B8B9333F188BBA2C0A20C1 (void);
// 0x0000000F System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassic::.ctor()
extern void PkzipClassic__ctor_mF0477324F7E255FC027E3ABF1749F6C3E2B232C0 (void);
// 0x00000010 System.Byte ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::TransformByte()
extern void PkzipClassicCryptoBase_TransformByte_m44EBD23D951C57E5A055E947BE7E7BE7D53FBC8A (void);
// 0x00000011 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::SetKeys(System.Byte[])
extern void PkzipClassicCryptoBase_SetKeys_m13A485E7183C14ECE038E91CD4291F84E14E5462 (void);
// 0x00000012 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::UpdateKeys(System.Byte)
extern void PkzipClassicCryptoBase_UpdateKeys_m97169997AF20F0C99A5C6A563D176276D49FDDA3 (void);
// 0x00000013 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::Reset()
extern void PkzipClassicCryptoBase_Reset_mE120FE4B0E4C21B546F11B7098549ABD5D253D6C (void);
// 0x00000014 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::.ctor()
extern void PkzipClassicCryptoBase__ctor_m90EEF2BA70CFBD40B8EF1CEB16D2A89A1A0DCBE8 (void);
// 0x00000015 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::.ctor(System.Byte[])
extern void PkzipClassicEncryptCryptoTransform__ctor_mDFDADB7C644E07A22A6D0F3641367AD83640109D (void);
// 0x00000016 System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern void PkzipClassicEncryptCryptoTransform_TransformFinalBlock_m8299274F9B3160CCCD7F19E886ED33B831C9E30F (void);
// 0x00000017 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern void PkzipClassicEncryptCryptoTransform_TransformBlock_mA43B7C37A39F4A5C3F574809A12CFD803631E13F (void);
// 0x00000018 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_InputBlockSize()
extern void PkzipClassicEncryptCryptoTransform_get_InputBlockSize_m9E2CA229F5E350520141346D4224538C50FB26F9 (void);
// 0x00000019 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_OutputBlockSize()
extern void PkzipClassicEncryptCryptoTransform_get_OutputBlockSize_mFAC62084FDC9C8B715D32CF4B2A15A2D7F9C0465 (void);
// 0x0000001A System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_CanTransformMultipleBlocks()
extern void PkzipClassicEncryptCryptoTransform_get_CanTransformMultipleBlocks_mDC2CED50742CDE1060CF37D731D10EC9DF0F80D7 (void);
// 0x0000001B System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::Dispose()
extern void PkzipClassicEncryptCryptoTransform_Dispose_mC7445D0E9D7B0E5CB6969D8ADA0C0CEB38BAC084 (void);
// 0x0000001C System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::.ctor(System.Byte[])
extern void PkzipClassicDecryptCryptoTransform__ctor_m93C2A9F72B1A1275DB6B2CB452B2C167C80F8702 (void);
// 0x0000001D System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern void PkzipClassicDecryptCryptoTransform_TransformFinalBlock_m10195ED2BBEB707D6FA1D6F891B15983261A248E (void);
// 0x0000001E System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern void PkzipClassicDecryptCryptoTransform_TransformBlock_mB3EA8DAAD292F5D201FDA78E3EB11E8FA25AE6CB (void);
// 0x0000001F System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_InputBlockSize()
extern void PkzipClassicDecryptCryptoTransform_get_InputBlockSize_mC40E55467EFAECE5E5CCB71761AB681AF9829A8D (void);
// 0x00000020 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_OutputBlockSize()
extern void PkzipClassicDecryptCryptoTransform_get_OutputBlockSize_mE799FA513835A118F9E8081079667B1983AC6279 (void);
// 0x00000021 System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_CanTransformMultipleBlocks()
extern void PkzipClassicDecryptCryptoTransform_get_CanTransformMultipleBlocks_mE6A5A313A210A168BF8AF2F46F4F7FEC1E8121B7 (void);
// 0x00000022 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::Dispose()
extern void PkzipClassicDecryptCryptoTransform_Dispose_mF0A7E62DBEC69D8D1F02E57411A07A0034D9B205 (void);
// 0x00000023 System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_BlockSize()
extern void PkzipClassicManaged_get_BlockSize_m349ADCAE37ECBD24111110DF9F8451F6C5AF0AE2 (void);
// 0x00000024 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::set_BlockSize(System.Int32)
extern void PkzipClassicManaged_set_BlockSize_mBEEC77C17A0F7546E4D2A47D05050E999360DE0E (void);
// 0x00000025 System.Security.Cryptography.KeySizes[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_LegalKeySizes()
extern void PkzipClassicManaged_get_LegalKeySizes_m4D6C76A9236013F8BDBA2F7815916171697DD19F (void);
// 0x00000026 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::GenerateIV()
extern void PkzipClassicManaged_GenerateIV_m07424E8014012219B1C2912B374FC7FE6C291889 (void);
// 0x00000027 System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_Key()
extern void PkzipClassicManaged_get_Key_m25774986983FDEECE78E98ACDDF7AE7D9DF5454D (void);
// 0x00000028 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::set_Key(System.Byte[])
extern void PkzipClassicManaged_set_Key_mF004DEB1FA2514DA3C8BCDAEF3A363501B7474BA (void);
// 0x00000029 System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::GenerateKey()
extern void PkzipClassicManaged_GenerateKey_mE0373F4EF87959399AF8680696100005E6589C8F (void);
// 0x0000002A System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void PkzipClassicManaged_CreateEncryptor_mC4128CAA756FF1A4AB4F48F120F633B623D22FF8 (void);
// 0x0000002B System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void PkzipClassicManaged_CreateDecryptor_m90A2C4DAB94BD9D7AD135A06286829F362AF0E96 (void);
// 0x0000002C System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::.ctor()
extern void PkzipClassicManaged__ctor_mFA2033EAC7C2B8CA246EFDE8084171D1130BA95C (void);
// 0x0000002D System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::.ctor(System.String,System.Byte[],System.Int32,System.Boolean)
extern void ZipAESTransform__ctor_mFC621DE8CC0688E6759F5B82C2F8A0F71ADD9690 (void);
// 0x0000002E System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern void ZipAESTransform_TransformBlock_m85327BA67B33E7B7E38358F929DBC131845D6414 (void);
// 0x0000002F System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_PwdVerifier()
extern void ZipAESTransform_get_PwdVerifier_m189484EF21F4401E86432AC112FE6EFD0F10FB93 (void);
// 0x00000030 System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::GetAuthCode()
extern void ZipAESTransform_GetAuthCode_mC0C1CE38794ACE7C0C74012A23F5D0B1D943A7DE (void);
// 0x00000031 System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern void ZipAESTransform_TransformFinalBlock_mC458BFB8F9905CE47DF46CF52EA21F686A82A54F (void);
// 0x00000032 System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_InputBlockSize()
extern void ZipAESTransform_get_InputBlockSize_m6767965E776543E9DAC1B4A60518A82CAA77C7FC (void);
// 0x00000033 System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_OutputBlockSize()
extern void ZipAESTransform_get_OutputBlockSize_m1DB068C8473DC7C396D4C61B0C9C031BF940FB3D (void);
// 0x00000034 System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_CanTransformMultipleBlocks()
extern void ZipAESTransform_get_CanTransformMultipleBlocks_m24EC990AC8AEA9772192142F09BCBE8CFCC60974 (void);
// 0x00000035 System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::Dispose()
extern void ZipAESTransform_Dispose_m7774633312C81A8BC2A2E60B5A482E801C5A0630 (void);
// 0x00000036 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern void InflaterInputStream__ctor_m2D67F323B922339EEB883B54A9DD7974E9197FE6 (void);
// 0x00000037 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Inflater,System.Int32)
extern void InflaterInputStream__ctor_mC18255CD57BC7BD161291399EA155E1ED6121CB6 (void);
// 0x00000038 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Skip(System.Int64)
extern void InflaterInputStream_Skip_mEFAB40EEF418B5F23280CD6AD94F86A4E0A9149C (void);
// 0x00000039 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::StopDecrypting()
extern void InflaterInputStream_StopDecrypting_mEF73A6A1850A2E31BF65B658058108933BFE9EF7 (void);
// 0x0000003A System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Fill()
extern void InflaterInputStream_Fill_m9DDD63F2A2141B597D6D848F98F0C856C24172B8 (void);
// 0x0000003B System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanRead()
extern void InflaterInputStream_get_CanRead_mF4FEA2080C1C4F195974CD7A5237F8BF4446A859 (void);
// 0x0000003C System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanSeek()
extern void InflaterInputStream_get_CanSeek_m946426BFE7AEB48F3C4C11AAA105895B23734C70 (void);
// 0x0000003D System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanWrite()
extern void InflaterInputStream_get_CanWrite_mED03AA9162B7568E8264A7B6C238E68EE5530F73 (void);
// 0x0000003E System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Length()
extern void InflaterInputStream_get_Length_m307D6AF21DE8F4A451ECFC3E789FA59A9B460A3B (void);
// 0x0000003F System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Position()
extern void InflaterInputStream_get_Position_mF61C4CE8F4E36E9BDD7646F4223515996ACE0AF8 (void);
// 0x00000040 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::set_Position(System.Int64)
extern void InflaterInputStream_set_Position_m5A307857AF0190502BABE598E8D0826CE9ED57C2 (void);
// 0x00000041 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Flush()
extern void InflaterInputStream_Flush_m361411526D8F2B0C269DF456E4878FDEDB2DB4DE (void);
// 0x00000042 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void InflaterInputStream_Seek_m02A1A453FA1DDFD093316656412E87DCA81B7D15 (void);
// 0x00000043 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputStream_Write_m98987A2DBA7884F2657FCC654D20519AB73FB25D (void);
// 0x00000044 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::WriteByte(System.Byte)
extern void InflaterInputStream_WriteByte_mE5F3C1F14CAB75C65E6D23C37A6D1B3A7B86A984 (void);
// 0x00000045 System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void InflaterInputStream_BeginWrite_mA228DD12469D82A81B02C90CFE78430290606A7D (void);
// 0x00000046 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Close()
extern void InflaterInputStream_Close_m93B5B6E8483332B9FEE86FD2B575ACA6D6418247 (void);
// 0x00000047 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputStream_Read_m9E31A0C1E5CBB4C85189206C2227FE73F7358ACF (void);
// 0x00000048 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Deflater)
extern void DeflaterOutputStream__ctor_m2BFCAF362250F4AE0B55FA890B3DFDCA9924C536 (void);
// 0x00000049 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Deflater,System.Int32)
extern void DeflaterOutputStream__ctor_mFD1DDBEB0CF1FC7AF628BF5C183A27D37BC93C08 (void);
// 0x0000004A System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish()
extern void DeflaterOutputStream_Finish_mF3A964B96DBD5804755B330F32C2792B67185E3F (void);
// 0x0000004B System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanPatchEntries()
extern void DeflaterOutputStream_get_CanPatchEntries_mBD7227EB3E9F150D9DE0A98936D020157974BD51 (void);
// 0x0000004C System.String ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Password()
extern void DeflaterOutputStream_get_Password_m0A75E0485FA612874D9077DF597FAEC3550F7231 (void);
// 0x0000004D System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::EncryptBlock(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_EncryptBlock_mB09A83CAF5A6B19283934CE6D82824153DDE95ED (void);
// 0x0000004E System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::InitializePassword(System.String)
extern void DeflaterOutputStream_InitializePassword_mDD3CDFDBA90AC01E0B004F1FBCB015C5EAB6F62A (void);
// 0x0000004F System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::InitializeAESPassword(ICSharpCode.SharpZipLib.Zip.ZipEntry,System.String,System.Byte[]&,System.Byte[]&)
extern void DeflaterOutputStream_InitializeAESPassword_m762CE7A5200ACEA61162244A13A849E7D8EEAB23 (void);
// 0x00000050 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate()
extern void DeflaterOutputStream_Deflate_m491B3347F4BAC0A2B64508AAB4C29EA6BF4D3010 (void);
// 0x00000051 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanRead()
extern void DeflaterOutputStream_get_CanRead_m959A760DED202E496B7AF960DF4B5873D301703E (void);
// 0x00000052 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanSeek()
extern void DeflaterOutputStream_get_CanSeek_m277D558DFCA9812B5A96540706DCEE04AA557B8A (void);
// 0x00000053 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanWrite()
extern void DeflaterOutputStream_get_CanWrite_mFD4C1EBC3D3FF0D0F58A6F2089F7B7C2A7D1313C (void);
// 0x00000054 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Length()
extern void DeflaterOutputStream_get_Length_mC5F45EC8A9ABB26E7D2469B2718F2131DE0DFF0D (void);
// 0x00000055 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Position()
extern void DeflaterOutputStream_get_Position_m664B7AEE9A0600BCC14CF0CA2BFF8DBF0B8682B9 (void);
// 0x00000056 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::set_Position(System.Int64)
extern void DeflaterOutputStream_set_Position_m5C87234D7F2BFC50CEFB30ECAC6535988670E3C2 (void);
// 0x00000057 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void DeflaterOutputStream_Seek_mF94638CD607592609AD0AB90727009F9787BEEF4 (void);
// 0x00000058 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::ReadByte()
extern void DeflaterOutputStream_ReadByte_m23DDA0D0F19D92EA9619F48DEAA1745AD97E2472 (void);
// 0x00000059 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_Read_m9B0ABA7AEC3AB1C611F6D53720BEB24BE785CFBD (void);
// 0x0000005A System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void DeflaterOutputStream_BeginRead_m987D34BDF38102D85EE4E4683633131AC0AD5A28 (void);
// 0x0000005B System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void DeflaterOutputStream_BeginWrite_m071D11051F07068E070FA9524A2B3EA5A356D755 (void);
// 0x0000005C System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Flush()
extern void DeflaterOutputStream_Flush_mAC771D83884862A0223EBA2139DF447F0DDFD524 (void);
// 0x0000005D System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Close()
extern void DeflaterOutputStream_Close_m3EEB8909C3F8FCF0B45B81BBE6D9DAF9DA7B502A (void);
// 0x0000005E System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::GetAuthCodeIfAES()
extern void DeflaterOutputStream_GetAuthCodeIfAES_m3C526DB012AC415CF0E28A9415E60B942D547E01 (void);
// 0x0000005F System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::WriteByte(System.Byte)
extern void DeflaterOutputStream_WriteByte_m00521396857F2CB1394026B1180B1AE3CC63BB40 (void);
// 0x00000060 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_Write_m2FA1251076AF0C741BD1481818E9EA0E62A08572 (void);
// 0x00000061 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
extern void InflaterInputBuffer__ctor_m5C40E106FF29D8186F4958C0789410241684F68E (void);
// 0x00000062 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_RawLength()
extern void InflaterInputBuffer_get_RawLength_m4E2375D23E77A424666EE7C946D6023172C84710 (void);
// 0x00000063 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
extern void InflaterInputBuffer_get_Available_mD46AA858207E17E410C307D4A580B5F7DD2926D1 (void);
// 0x00000064 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
extern void InflaterInputBuffer_set_Available_m579478077662D91ED88915C6365FAEBFB871A675 (void);
// 0x00000065 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern void InflaterInputBuffer_SetInflaterInput_mDB5F86150242CC8015A050EF6C60FAA694103532 (void);
// 0x00000066 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
extern void InflaterInputBuffer_Fill_m1C17B8704FC1052E6330CF736841399CA0274A07 (void);
// 0x00000067 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[])
extern void InflaterInputBuffer_ReadRawBuffer_m7ED9C21B8BE12B064F8EB61313935F34060D41F6 (void);
// 0x00000068 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputBuffer_ReadRawBuffer_m107AD9F7482F30C1C8A589A76F222A341488C2AD (void);
// 0x00000069 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputBuffer_ReadClearTextBuffer_mB4E97497E1AF741BBDF5542C9BCCB5FA603538DA (void);
// 0x0000006A System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
extern void InflaterInputBuffer_ReadLeByte_m062AD8D7AEF0D408CD3B686B23E2D5460CC0A499 (void);
// 0x0000006B System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeShort()
extern void InflaterInputBuffer_ReadLeShort_mA12C80BC2F669B60D272063472FE4E09ADF82DF1 (void);
// 0x0000006C System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeInt()
extern void InflaterInputBuffer_ReadLeInt_m268A02D6CBBD994453D7D316612A58CF1F969AC0 (void);
// 0x0000006D System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeLong()
extern void InflaterInputBuffer_ReadLeLong_m36FF2F3F8573DB88BD8E30B2748BF202C25E5E50 (void);
// 0x0000006E System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_CryptoTransform(System.Security.Cryptography.ICryptoTransform)
extern void InflaterInputBuffer_set_CryptoTransform_mCACE1607E0EA8FFA2A32DC404D9DA5F718A1A39E (void);
// 0x0000006F System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
extern void OutputWindow_Write_m1EA5E2CC2935FDA915230BAA4E36198ED4380BDF (void);
// 0x00000070 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
extern void OutputWindow_SlowRepeat_m7F12C1EE0873B617985C8DB6C33BBC762020484E (void);
// 0x00000071 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
extern void OutputWindow_Repeat_m8FA0401694BC172B983A8CB7C285DFC21634A726 (void);
// 0x00000072 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
extern void OutputWindow_CopyStored_mB719F206FABB0103860333E29A7DEB591044B8E0 (void);
// 0x00000073 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
extern void OutputWindow_GetFreeSpace_m5155AC048E9DD283E58A627221A6629B807D2083 (void);
// 0x00000074 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
extern void OutputWindow_GetAvailable_m265560A962D99072E86EBC99BCCDF2174B79E7B6 (void);
// 0x00000075 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
extern void OutputWindow_CopyOutput_m5FF61442E27D6C4AD742FE89D17645689E9E014B (void);
// 0x00000076 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
extern void OutputWindow_Reset_mF2B99AE284F54A7D07B9F1C29452408D40BC6F26 (void);
// 0x00000077 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
extern void OutputWindow__ctor_m0B67D27A7EAFEE54A927E889E6C85DE4BBC5C63D (void);
// 0x00000078 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::.ctor()
extern void StreamManipulator__ctor_mAF3E5C7ADFFF99059D244DD1DA02081E5F5948DD (void);
// 0x00000079 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::PeekBits(System.Int32)
extern void StreamManipulator_PeekBits_mC33B6EEBBBECC21466FC2683CE3AEEA471D7031D (void);
// 0x0000007A System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::DropBits(System.Int32)
extern void StreamManipulator_DropBits_mEDE078F4C1A6E3EABD6ED74F94AFDFCFC2E11468 (void);
// 0x0000007B System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBits()
extern void StreamManipulator_get_AvailableBits_m8500BA0CFAEF99E784BA5C6FB1073CDA39770563 (void);
// 0x0000007C System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBytes()
extern void StreamManipulator_get_AvailableBytes_m2D908CBD7CC371151DC519828D8E21F3A37EC7F9 (void);
// 0x0000007D System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SkipToByteBoundary()
extern void StreamManipulator_SkipToByteBoundary_mF31C8FC9B1E972DFBC9470EC32AA889740BCFB8B (void);
// 0x0000007E System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_IsNeedingInput()
extern void StreamManipulator_get_IsNeedingInput_m58ED090B35C6E27DEA1DB3F75991D4BD79597646 (void);
// 0x0000007F System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::CopyBytes(System.Byte[],System.Int32,System.Int32)
extern void StreamManipulator_CopyBytes_m51F37B1454498549CF310B851AD77563006182CA (void);
// 0x00000080 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::Reset()
extern void StreamManipulator_Reset_m4191FF2F24CBF4F41D2DC12213FDD6019344F937 (void);
// 0x00000081 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SetInput(System.Byte[],System.Int32,System.Int32)
extern void StreamManipulator_SetInput_mB46C735CCF621ABCDED412FA7FB268B0C22668EE (void);
// 0x00000082 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32,System.Boolean)
extern void Deflater__ctor_m57CB8C4A1FBCFF2923DD8522EB8E196700B83D77 (void);
// 0x00000083 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Reset()
extern void Deflater_Reset_m30DA5E7A7FDC2D946D85D97F0D420DC735CEB7D0 (void);
// 0x00000084 System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_TotalOut()
extern void Deflater_get_TotalOut_m239B1C30F62A30507E35FBDF8785AC622B6E04B6 (void);
// 0x00000085 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Flush()
extern void Deflater_Flush_mC0EB1DC4AAE1D05EA2DDCD5B9906F43CA91942BC (void);
// 0x00000086 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Finish()
extern void Deflater_Finish_m382411296C20000FFE2F07E2783710CFA1619EDA (void);
// 0x00000087 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsFinished()
extern void Deflater_get_IsFinished_m39E54240221DFB16E5E59C131B0074406267DEB1 (void);
// 0x00000088 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsNeedingInput()
extern void Deflater_get_IsNeedingInput_mAB7D6D34D94A03A1DEDF832AA86F852EF6E6A5A5 (void);
// 0x00000089 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern void Deflater_SetInput_mE0523323E987118BB736BED5671C874060E14423 (void);
// 0x0000008A System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetLevel(System.Int32)
extern void Deflater_SetLevel_mE4532EDBAD0768FDE51C50884FF4BC746D9852D9 (void);
// 0x0000008B System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetStrategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern void Deflater_SetStrategy_m606E78CC7284058DDEE3C53903185DAACE025CD6 (void);
// 0x0000008C System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[],System.Int32,System.Int32)
extern void Deflater_Deflate_mD0F6011C3D7336386B00F502D9685FE718A8B96C (void);
// 0x0000008D System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::.ctor()
extern void DeflaterConstants__ctor_m13587F1586B73023DD07EB2FF778DABA53E7C574 (void);
// 0x0000008E System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::.cctor()
extern void DeflaterConstants__cctor_m368AF88FF2E825B4148CAE398EC92993034C4AC9 (void);
// 0x0000008F System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending)
extern void DeflaterEngine__ctor_m69EF65BACD1DA939C9FFC2B5C1D1475EC7A0DC83 (void);
// 0x00000090 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Deflate(System.Boolean,System.Boolean)
extern void DeflaterEngine_Deflate_mA9D11BF772015B55537B31A6B29C629D46839A4E (void);
// 0x00000091 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetInput(System.Byte[],System.Int32,System.Int32)
extern void DeflaterEngine_SetInput_m9438FB0A3F36E3C76D02F8578E29261802FC577B (void);
// 0x00000092 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::NeedsInput()
extern void DeflaterEngine_NeedsInput_mF2E07871A55D7F4752D7BD5465C415D8C5C3FAB3 (void);
// 0x00000093 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Reset()
extern void DeflaterEngine_Reset_m196587E7E4DC938F1B500BFFAADA62466A1E016C (void);
// 0x00000094 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::ResetAdler()
extern void DeflaterEngine_ResetAdler_mA249980BBC22D9D47D065B7690B37C86A5E94C68 (void);
// 0x00000095 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::get_Adler()
extern void DeflaterEngine_get_Adler_mFB7A59C55E20B8B0AA1EDDBA9F24586F5AE79C52 (void);
// 0x00000096 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::set_Strategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern void DeflaterEngine_set_Strategy_m79C64440BA8C81248CFEA8D33B5E6FF52BDA85A1 (void);
// 0x00000097 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetLevel(System.Int32)
extern void DeflaterEngine_SetLevel_m2A1E629F0843C41B46AC75B4D96C9EEFEB46397B (void);
// 0x00000098 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FillWindow()
extern void DeflaterEngine_FillWindow_mA2B47111117B204C8A4657E7741046546027E0C2 (void);
// 0x00000099 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::UpdateHash()
extern void DeflaterEngine_UpdateHash_m4F3A17A71FF0FCCE29663BCB21370C0ED6F8A5A6 (void);
// 0x0000009A System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::InsertString()
extern void DeflaterEngine_InsertString_mACF16A7C87FE666ED50B372ACC47E06986DCDDDC (void);
// 0x0000009B System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SlideWindow()
extern void DeflaterEngine_SlideWindow_m94FEA3F82B8C517D540B5E175B5BF1C93F884941 (void);
// 0x0000009C System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FindLongestMatch(System.Int32)
extern void DeflaterEngine_FindLongestMatch_mB114F771F305D63FEAF011FC44C555C011CFEDBA (void);
// 0x0000009D System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateStored(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateStored_m3D121267BB6F64782CAB3EFA51BA04D38E309A06 (void);
// 0x0000009E System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateFast(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateFast_m7A173501C97F9B7CAE906972C18DFB5727E0C479 (void);
// 0x0000009F System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateSlow(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateSlow_mC6F9DFEE6F8B6DBFE75E8CD20A546F80B75DD42E (void);
// 0x000000A0 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::.cctor()
extern void DeflaterHuffman__cctor_mD72DFCC98B9C9DF8B42EB7603141D5CF98BD60E8 (void);
// 0x000000A1 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending)
extern void DeflaterHuffman__ctor_m0E5611060CFCBBEB4B08F3C33AC1A8BB76FB1777 (void);
// 0x000000A2 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Reset()
extern void DeflaterHuffman_Reset_m8E61516F51334A9B629F8F40ADF3B8B584F8BB86 (void);
// 0x000000A3 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::SendAllTrees(System.Int32)
extern void DeflaterHuffman_SendAllTrees_m9BB6B306AA29CC46D3E3BA81F9BD64AB13A3BB48 (void);
// 0x000000A4 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::CompressBlock()
extern void DeflaterHuffman_CompressBlock_m89038ADA557D80FEC1972B4E7D199BAA3C5A9A52 (void);
// 0x000000A5 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushStoredBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void DeflaterHuffman_FlushStoredBlock_mAAB7BE6CAC5ABB6738A66A1C19BFBA063C22A5BF (void);
// 0x000000A6 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void DeflaterHuffman_FlushBlock_mB67D6DB018DBBA5AE108235F523F420E9A466474 (void);
// 0x000000A7 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::IsFull()
extern void DeflaterHuffman_IsFull_m72587953732A715A7231FCDBF377C4D9C8DC3DA9 (void);
// 0x000000A8 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyLit(System.Int32)
extern void DeflaterHuffman_TallyLit_mC47AB48E45F67F7CEC4F2A9D9237C44D4778B68D (void);
// 0x000000A9 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyDist(System.Int32,System.Int32)
extern void DeflaterHuffman_TallyDist_mB0C593D47758BEC3193E2BB4642018BE669C635D (void);
// 0x000000AA System.Int16 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::BitReverse(System.Int32)
extern void DeflaterHuffman_BitReverse_m180F41F7EBFEE48A0D37253024C62186C35FF580 (void);
// 0x000000AB System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Lcode(System.Int32)
extern void DeflaterHuffman_Lcode_m861BAFFA67C4DE9717AF87CE465669774C97299A (void);
// 0x000000AC System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Dcode(System.Int32)
extern void DeflaterHuffman_Dcode_mF865E06FCB31FF4D0AC2A13CBE99E7BC63AB3D59 (void);
// 0x000000AD System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman,System.Int32,System.Int32,System.Int32)
extern void Tree__ctor_mA614B1C230AE89F28C5B1E72DD69C897182239A8 (void);
// 0x000000AE System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::Reset()
extern void Tree_Reset_mDDD9D3366F78EF20B9A0B4D1E511FB1EAFEDB0EE (void);
// 0x000000AF System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::WriteSymbol(System.Int32)
extern void Tree_WriteSymbol_m635AE2D405DAE9FF9167DBEBB94F60CCCD59B3DA (void);
// 0x000000B0 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::SetStaticCodes(System.Int16[],System.Byte[])
extern void Tree_SetStaticCodes_mEC15A35B2B2CFF8F17477D0177432BBB56852971 (void);
// 0x000000B1 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::BuildCodes()
extern void Tree_BuildCodes_m81EF550BD19A0105B4E7F1C4DD9E8DD793579B39 (void);
// 0x000000B2 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::BuildTree()
extern void Tree_BuildTree_mE2D4A8C2E84B3769B982878EC6481DAD8CDE432B (void);
// 0x000000B3 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::GetEncodedLength()
extern void Tree_GetEncodedLength_m7A0700F98E3BCD64AD5AAEE42CC3742CCFBDC8A9 (void);
// 0x000000B4 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::CalcBLFreq(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree)
extern void Tree_CalcBLFreq_m7A86ED8496A97A4D4881F4E3347E6EB153441072 (void);
// 0x000000B5 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::WriteTree(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree)
extern void Tree_WriteTree_m122FC81F15E2D670B1B097B458C7DF5E06022683 (void);
// 0x000000B6 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman_Tree::BuildLength(System.Int32[])
extern void Tree_BuildLength_m0F6E015DD5FF1D1C7E1D9D6DB1895D678D8D9428 (void);
// 0x000000B7 System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::.ctor(System.Int32)
extern void PendingBuffer__ctor_m2807D9EE3C2C24AEAEEBB20D2610BB2E158ED1A6 (void);
// 0x000000B8 System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::Reset()
extern void PendingBuffer_Reset_m99B076AACDEA9EF12EEAF6619EF784AEF0785A7A (void);
// 0x000000B9 System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteShort(System.Int32)
extern void PendingBuffer_WriteShort_m1D7325AD314B385178974FD5DA91F3583488AB0D (void);
// 0x000000BA System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteBlock(System.Byte[],System.Int32,System.Int32)
extern void PendingBuffer_WriteBlock_m972E0492F2C4451376175E4D94CC44D02BCB9FAF (void);
// 0x000000BB System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::get_BitCount()
extern void PendingBuffer_get_BitCount_mDEBD5EF56A155066CF45F097AFE625D96C5AD905 (void);
// 0x000000BC System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::AlignToByte()
extern void PendingBuffer_AlignToByte_m3E370D075FF049034AB69141A815EE147886402D (void);
// 0x000000BD System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteBits(System.Int32,System.Int32)
extern void PendingBuffer_WriteBits_mD1F29B539D8541020DE7517ECC6BD334EA8CB279 (void);
// 0x000000BE System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteShortMSB(System.Int32)
extern void PendingBuffer_WriteShortMSB_m4E52464ABDD32F254442E0832EDE35FEC279F0CB (void);
// 0x000000BF System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::get_IsFlushed()
extern void PendingBuffer_get_IsFlushed_m5ECF252760839CF44C49302BB9E7E937EF0EE015 (void);
// 0x000000C0 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::Flush(System.Byte[],System.Int32,System.Int32)
extern void PendingBuffer_Flush_m480024E2E6D8E73043C6325D04E8CC0F2ED7A34A (void);
// 0x000000C1 System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending::.ctor()
extern void DeflaterPending__ctor_mD92E0B1626CEFAC57F70D807742063E62EFB7902 (void);
// 0x000000C2 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
extern void Inflater__ctor_m58B684F78C1B58D26CC7C6A2E729A0D07402E56F (void);
// 0x000000C3 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Reset()
extern void Inflater_Reset_m8451B365C0D2E7070FB3105ED9DEC7AC8711B239 (void);
// 0x000000C4 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
extern void Inflater_DecodeHeader_m14EE76D62FC98EC2780CABDC8E7FF58B443A0FE5 (void);
// 0x000000C5 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
extern void Inflater_DecodeDict_mA6570C196A64149B289CFA165682D8FFA0EADE1C (void);
// 0x000000C6 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
extern void Inflater_DecodeHuffman_mEA57C000BE39233D53CED45DEA58265BF37AEA43 (void);
// 0x000000C7 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
extern void Inflater_DecodeChksum_mD51CB6ADF6C3663F612E77085D5EC01BCE08DE6D (void);
// 0x000000C8 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Decode()
extern void Inflater_Decode_m6CA877FD4DF171E36352527E8B8098C7EBE51E13 (void);
// 0x000000C9 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern void Inflater_SetInput_m8D6E5C4C16F58A65A223788263AADA7B63A77D4D (void);
// 0x000000CA System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
extern void Inflater_Inflate_mFBB808BE146122CE8EB46207AB6DA4F3AD958A60 (void);
// 0x000000CB System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
extern void Inflater_get_IsNeedingInput_mD15EAFB849F34802D253C96470D860859ABAADCB (void);
// 0x000000CC System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
extern void Inflater_get_IsNeedingDictionary_mB76ECC03A203D3E02D780A0ECBC028B143301418 (void);
// 0x000000CD System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
extern void Inflater_get_IsFinished_mC81B8579D62FD0F072A25E92D8FB98939853DE29 (void);
// 0x000000CE System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
extern void Inflater_get_TotalOut_mA36DE58122D832F7398842FCC2B051A04D791F28 (void);
// 0x000000CF System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalIn()
extern void Inflater_get_TotalIn_m7FB88153C0C290B650FC1A9367533A1BD1ED9FD4 (void);
// 0x000000D0 System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
extern void Inflater_get_RemainingInput_mA27E6FEEFC734F6CB34B4C86C09D9C58DFA2AC26 (void);
// 0x000000D1 System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.cctor()
extern void Inflater__cctor_m3912F4163CBE87F8044EB15BC71C08614B44161F (void);
// 0x000000D2 System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.ctor()
extern void InflaterDynHeader__ctor_m9417085430C2F2E079578BBD888B2F84126E2E79 (void);
// 0x000000D3 System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::Decode(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern void InflaterDynHeader_Decode_m2EC8822D0676C54EF74A9159D85BAA42815662F4 (void);
// 0x000000D4 ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildLitLenTree()
extern void InflaterDynHeader_BuildLitLenTree_m8E21490D1B5FB54BE3FEB8AE7E4DC6422AB82C21 (void);
// 0x000000D5 ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildDistTree()
extern void InflaterDynHeader_BuildDistTree_m218FF52158FE818581F7EFD89A4280329989158E (void);
// 0x000000D6 System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.cctor()
extern void InflaterDynHeader__cctor_mE125FD860474122FD6E476717D451672EF78BB8F (void);
// 0x000000D7 System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.cctor()
extern void InflaterHuffmanTree__cctor_m2BD112AE4E9C06A5CA5A3191C26B891C1D07F253 (void);
// 0x000000D8 System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.ctor(System.Byte[])
extern void InflaterHuffmanTree__ctor_m94855E1E5CC7CC9CE3035A525E4BB10FA0111A7A (void);
// 0x000000D9 System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::BuildTree(System.Byte[])
extern void InflaterHuffmanTree_BuildTree_m7AB8EA6CD97AD54855E08B8825E5958991E7D382 (void);
// 0x000000DA System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::GetSymbol(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern void InflaterHuffmanTree_GetSymbol_m5D37F51DC607A632A1A182F5C0673B6ABD7D918C (void);
// 0x000000DB System.Int32 ICSharpCode.SharpZipLib.Zip.ZipConstants::get_DefaultCodePage()
extern void ZipConstants_get_DefaultCodePage_mB06F1B4565607B1C38345A8A5CE9D63B0F5C1B0C (void);
// 0x000000DC System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToString(System.Byte[],System.Int32)
extern void ZipConstants_ConvertToString_mB2022333FBF462A12086D7FF962A64A836889E85 (void);
// 0x000000DD System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToStringExt(System.Int32,System.Byte[])
extern void ZipConstants_ConvertToStringExt_m24B37DA0A06D42C54C13A4FD53AE60B49F7F03E4 (void);
// 0x000000DE System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToArray(System.String)
extern void ZipConstants_ConvertToArray_mD029ED36EAA500F69FEFDFC108E2EC339E578733 (void);
// 0x000000DF System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToArray(System.Int32,System.String)
extern void ZipConstants_ConvertToArray_mA70267A31A3C782412341244A8C3C1F0E7E9DA55 (void);
// 0x000000E0 System.Void ICSharpCode.SharpZipLib.Zip.ZipConstants::.cctor()
extern void ZipConstants__cctor_mF46C75FBEFF7667D5E0FEFC5AD7EFF996E98582A (void);
// 0x000000E1 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String)
extern void ZipEntry__ctor_mE75AE4978F145D399A272CA1695A7E6840EF2E78 (void);
// 0x000000E2 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String,System.Int32)
extern void ZipEntry__ctor_mD0CC6DE52D77799A812856C64D0BC57F3D7834D1 (void);
// 0x000000E3 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String,System.Int32,System.Int32,ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern void ZipEntry__ctor_mDA6564263E314CF570F57BA5EC8403535D302EED (void);
// 0x000000E4 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_HasCrc()
extern void ZipEntry_get_HasCrc_mB50F6087F1BB279F4D3A49C9DA41749CE4C807C0 (void);
// 0x000000E5 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsCrypted()
extern void ZipEntry_get_IsCrypted_m67AC3A78E4EA97DF33289AE38C05591EA15A9EC6 (void);
// 0x000000E6 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_IsCrypted(System.Boolean)
extern void ZipEntry_set_IsCrypted_m4BE89AE22B3C5F2009A2C2702A5490D69361974A (void);
// 0x000000E7 System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CryptoCheckValue()
extern void ZipEntry_get_CryptoCheckValue_m40A5DD7932EF09DB3C2F1FD1C60E0DD4A7818E66 (void);
// 0x000000E8 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CryptoCheckValue(System.Byte)
extern void ZipEntry_set_CryptoCheckValue_m5EE9D24AA04321C54A98B8AF50D6842BF1D802D3 (void);
// 0x000000E9 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Flags()
extern void ZipEntry_get_Flags_m9875608730955C1ED8ECFD422F65C853A49418C2 (void);
// 0x000000EA System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Flags(System.Int32)
extern void ZipEntry_set_Flags_m8D422FED00F0C9F3D1E0132889779F02032308D5 (void);
// 0x000000EB System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Offset()
extern void ZipEntry_get_Offset_m6BD5F5C70367EFC1AA07B7AF12F4F5B6A324B8BA (void);
// 0x000000EC System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Offset(System.Int64)
extern void ZipEntry_set_Offset_m37E437585AB681C720FAAF86696665C534505209 (void);
// 0x000000ED System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_ExternalFileAttributes()
extern void ZipEntry_get_ExternalFileAttributes_m369EB1A3B8E4A6399710048318A7330DBF8B12FD (void);
// 0x000000EE System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::HasDosAttributes(System.Int32)
extern void ZipEntry_HasDosAttributes_mB81C9009D0C60D67F4E1B33B70C6C70D073F92D0 (void);
// 0x000000EF System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_HostSystem()
extern void ZipEntry_get_HostSystem_mE2E80A099A6F9E0196CDCB58F039F8EB06BD20AB (void);
// 0x000000F0 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Version()
extern void ZipEntry_get_Version_m186D72ECEC69C87CAD85EF7D110D7D864C42A2DE (void);
// 0x000000F1 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CanDecompress()
extern void ZipEntry_get_CanDecompress_m8ABCED141E8057C5E08A1EC9B65A79DF3ECFCCFD (void);
// 0x000000F2 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ForceZip64()
extern void ZipEntry_ForceZip64_m03801A793D168D8A0B67A1A0FBE46B9A554A2AFA (void);
// 0x000000F3 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsZip64Forced()
extern void ZipEntry_IsZip64Forced_mFF7B0A2B129A4CCDA19E966F54A368B136E24AB5 (void);
// 0x000000F4 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_LocalHeaderRequiresZip64()
extern void ZipEntry_get_LocalHeaderRequiresZip64_m484ECEF0804BA1E0A93B482DF163A9B29065B50F (void);
// 0x000000F5 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CentralHeaderRequiresZip64()
extern void ZipEntry_get_CentralHeaderRequiresZip64_mC9DF5350EEDCBBEEB3E55943D6320BD2A37B0273 (void);
// 0x000000F6 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_DosTime()
extern void ZipEntry_get_DosTime_m075FC7A087884B7A896184E18B8B5FFFB69E27E6 (void);
// 0x000000F7 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_DosTime(System.Int64)
extern void ZipEntry_set_DosTime_mC3F6EAB77DDFBEEB1793FE88FF5006CD73B17165 (void);
// 0x000000F8 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_DateTime(System.DateTime)
extern void ZipEntry_set_DateTime_m06237CCB8F2A23BB4D25F0CF2C9B3FFB51132F5C (void);
// 0x000000F9 System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Name()
extern void ZipEntry_get_Name_m29BD4E2597CF1C73560D3266D22DB907556A10BE (void);
// 0x000000FA System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Size()
extern void ZipEntry_get_Size_m1FAB6C21B24342AC715A008BDE608FDF6EEF65D5 (void);
// 0x000000FB System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Size(System.Int64)
extern void ZipEntry_set_Size_mDFE3D6D8EBA7A0D11CBAC65768A0672120569F73 (void);
// 0x000000FC System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressedSize()
extern void ZipEntry_get_CompressedSize_mBB45D815684D26B45E24711212F0B4E1F0C74E89 (void);
// 0x000000FD System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CompressedSize(System.Int64)
extern void ZipEntry_set_CompressedSize_mF1AD1055D95022048219D75ADC30B598A232A357 (void);
// 0x000000FE System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Crc()
extern void ZipEntry_get_Crc_mA4D7319A6B039A26056CCF7B832119ACECCDDD5B (void);
// 0x000000FF System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Crc(System.Int64)
extern void ZipEntry_set_Crc_m26E9B25A3A02DE98E385FEE9A4336590A5C872CD (void);
// 0x00000100 ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressionMethod()
extern void ZipEntry_get_CompressionMethod_mA498B8DC016C4893C0D16ABD85BB857C4E916895 (void);
// 0x00000101 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CompressionMethod(ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern void ZipEntry_set_CompressionMethod_mA38F4CB0E027658922D29A6061AD5229FD211941 (void);
// 0x00000102 ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressionMethodForHeader()
extern void ZipEntry_get_CompressionMethodForHeader_m5101B3F24AD863F218A6B8DDA0EDC0D1F0F9DFBD (void);
// 0x00000103 System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipEntry::get_ExtraData()
extern void ZipEntry_get_ExtraData_mD0160EC8AD0A3EFB1128DC1B59DB8B361952599F (void);
// 0x00000104 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_ExtraData(System.Byte[])
extern void ZipEntry_set_ExtraData_mA38232939D5667482C6004A2B1C666085E65BF5F (void);
// 0x00000105 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESKeySize()
extern void ZipEntry_get_AESKeySize_mBEE765C4AF081DE3D2BFBEAFF44EDD5BB51F5829 (void);
// 0x00000106 System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESEncryptionStrength()
extern void ZipEntry_get_AESEncryptionStrength_m56B07314C8CE286E97E8AD336D3A0F2AAC489C0A (void);
// 0x00000107 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESSaltLen()
extern void ZipEntry_get_AESSaltLen_mE03E8E5F09E7B669C084EF98871C709EDE394681 (void);
// 0x00000108 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESOverheadSize()
extern void ZipEntry_get_AESOverheadSize_m7E4874E31E6523E9C50AE517385E301376E9FDFA (void);
// 0x00000109 System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ProcessExtraData(System.Boolean)
extern void ZipEntry_ProcessExtraData_mC601598FCF73135556DA39EF1A0ADB5CF4B5670C (void);
// 0x0000010A System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ProcessAESExtraData(ICSharpCode.SharpZipLib.Zip.ZipExtraData)
extern void ZipEntry_ProcessAESExtraData_m774EDF5EB687BD9B1C20816558D270F7822DF5EF (void);
// 0x0000010B System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Comment()
extern void ZipEntry_get_Comment_m4A8F1B84DB7DE3168370A9D83D4882755BB385AE (void);
// 0x0000010C System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsDirectory()
extern void ZipEntry_get_IsDirectory_m37F50B2FA96030F68DFFC7255D9F10ECF26C0DDD (void);
// 0x0000010D System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsCompressionMethodSupported()
extern void ZipEntry_IsCompressionMethodSupported_mCA65FD1F20017893DF3054A02D2CCD1254F825AE (void);
// 0x0000010E System.Object ICSharpCode.SharpZipLib.Zip.ZipEntry::Clone()
extern void ZipEntry_Clone_m088F3ACABBE83A0153966D8250DF39E4AD2072AE (void);
// 0x0000010F System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::ToString()
extern void ZipEntry_ToString_m59F693E4060C3BC9870910CB959202EC4C782E3C (void);
// 0x00000110 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsCompressionMethodSupported(ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern void ZipEntry_IsCompressionMethodSupported_m6DD6C3F45BB642E34E558E641F9EB3DC78CD2B9A (void);
// 0x00000111 System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ZipException__ctor_mEA31871219DACB3B1D876369CF918068BC976E21 (void);
// 0x00000112 System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor()
extern void ZipException__ctor_mA35D0790599840E2231872EECF9B3B40B3B8CEDF (void);
// 0x00000113 System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor(System.String)
extern void ZipException__ctor_m82958DBCA7D000F61715DEBF1C9F65311F2A3109 (void);
// 0x00000114 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::.ctor(System.Byte[])
extern void ZipExtraData__ctor_m9ADBD056FDD05FF38B8BB04724EAA1B47671B47C (void);
// 0x00000115 System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipExtraData::GetEntryData()
extern void ZipExtraData_GetEntryData_m07BD3E631D8C157346C01AD133D5CA2824EF1F0F (void);
// 0x00000116 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_Length()
extern void ZipExtraData_get_Length_mAAB1D0B75A8CEA239EDDE82947E9ADC2EF77934E (void);
// 0x00000117 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_ValueLength()
extern void ZipExtraData_get_ValueLength_m2DF15993544BB0630402C80173360BAB4395BA12 (void);
// 0x00000118 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_CurrentReadIndex()
extern void ZipExtraData_get_CurrentReadIndex_mDB44C1CB9C938EB221860FA54ADB4FC28AF823D6 (void);
// 0x00000119 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_UnreadCount()
extern void ZipExtraData_get_UnreadCount_mCAF8DA3C0FD0138604238B7571830C160D724EAE (void);
// 0x0000011A System.Boolean ICSharpCode.SharpZipLib.Zip.ZipExtraData::Find(System.Int32)
extern void ZipExtraData_Find_mA0D23ADA4738A8BF7EA1C258E39AF7FB018D602E (void);
// 0x0000011B System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddEntry(System.Int32,System.Byte[])
extern void ZipExtraData_AddEntry_mAEA2E2A9A52E2FA158DED4857C1164D752D87870 (void);
// 0x0000011C System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::StartNewEntry()
extern void ZipExtraData_StartNewEntry_m70FD6B95429F080F80ECFB4FF55319D8E475F6C3 (void);
// 0x0000011D System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddNewEntry(System.Int32)
extern void ZipExtraData_AddNewEntry_mD730C3BA770600752A284C04FE5FFB24AA66881F (void);
// 0x0000011E System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddData(System.Byte)
extern void ZipExtraData_AddData_m5902477D0D0F4A44877596B584073EE78C5CC653 (void);
// 0x0000011F System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddLeShort(System.Int32)
extern void ZipExtraData_AddLeShort_m785DEBD288A11C2E867D2DAA5B1FE6703B86FF52 (void);
// 0x00000120 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddLeInt(System.Int32)
extern void ZipExtraData_AddLeInt_m659D2CB49C61FDF5083F039C1F68B1AFF8184DDA (void);
// 0x00000121 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::AddLeLong(System.Int64)
extern void ZipExtraData_AddLeLong_mB1394AB0487B4E5B1ADDC8C8952C3125C017F2B9 (void);
// 0x00000122 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipExtraData::Delete(System.Int32)
extern void ZipExtraData_Delete_m9B1AC771EE50FB327E373D0DFE35E0A62DB36FFA (void);
// 0x00000123 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadLong()
extern void ZipExtraData_ReadLong_m23337B6C32C586EBAB86F70970E1AC73245915AD (void);
// 0x00000124 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadInt()
extern void ZipExtraData_ReadInt_mB3FF89E08BC05335BA073189F8BE1FC3A104AEAE (void);
// 0x00000125 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadShort()
extern void ZipExtraData_ReadShort_mDD80B097D4D3986F8977FFC4B72D69B085D9BC39 (void);
// 0x00000126 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadByte()
extern void ZipExtraData_ReadByte_m196A33F4701DAB055BA59EAE11166DECEDE7CD1B (void);
// 0x00000127 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::Skip(System.Int32)
extern void ZipExtraData_Skip_m0E3AFFE37C487B025913B571470926AECD8E009A (void);
// 0x00000128 System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadCheck(System.Int32)
extern void ZipExtraData_ReadCheck_m552ED12A1C092593C722BBEDF1F82B1CCC50EA14 (void);
// 0x00000129 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadShortInternal()
extern void ZipExtraData_ReadShortInternal_m5BF71B645A7499EB2613D76709C3C8FA9C54EA54 (void);
// 0x0000012A System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::SetShort(System.Int32&,System.Int32)
extern void ZipExtraData_SetShort_m39E1FC2020B3E51DA52AD716A9907F53CFD73656 (void);
// 0x0000012B System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::Dispose()
extern void ZipExtraData_Dispose_mA4669C59E52C9304F8539E7A23D797780A7E4183 (void);
// 0x0000012C System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::.ctor(System.IO.Stream)
extern void ZipHelperStream__ctor_m84FB7A0C7A65C48684E782BC12C0E75D1E1202CC (void);
// 0x0000012D System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanRead()
extern void ZipHelperStream_get_CanRead_mD14AD502F6130E802296115E2F8B44CAE17A5372 (void);
// 0x0000012E System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanSeek()
extern void ZipHelperStream_get_CanSeek_mDB8C9C4B4F9B666817C029F3874174E25C288231 (void);
// 0x0000012F System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_Length()
extern void ZipHelperStream_get_Length_m157DB0B54B7E470870C3383D39F7F238527D38D0 (void);
// 0x00000130 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_Position()
extern void ZipHelperStream_get_Position_m7386EB504DCD827CD85D35B6AB9360CA02E88482 (void);
// 0x00000131 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::set_Position(System.Int64)
extern void ZipHelperStream_set_Position_mC6F3BDCC2CC8939953CFF8437E25B605182D4EF4 (void);
// 0x00000132 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanWrite()
extern void ZipHelperStream_get_CanWrite_mC7C73FA037F7AC002C187C42BB901CD187CB0A8E (void);
// 0x00000133 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Flush()
extern void ZipHelperStream_Flush_m627493F2903D0D767800F779956A1B5D158109D2 (void);
// 0x00000134 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void ZipHelperStream_Seek_m57F0879FC7BA9EF97C1A75F360EFAEB92873EC13 (void);
// 0x00000135 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ZipHelperStream_Read_m60DEFE46550FFC416E9638847B4E8A7FABB30EF9 (void);
// 0x00000136 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Write(System.Byte[],System.Int32,System.Int32)
extern void ZipHelperStream_Write_m16C28AE4AB3226C201E6542A84EEB332FE892DE4 (void);
// 0x00000137 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Close()
extern void ZipHelperStream_Close_m60EF560F88DF3D51009F31D3FC130C7EF144BBCB (void);
// 0x00000138 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteZip64EndOfCentralDirectory(System.Int64,System.Int64,System.Int64)
extern void ZipHelperStream_WriteZip64EndOfCentralDirectory_mE2E45FCCD8DABF985957FE6D2D3679ECE744E12A (void);
// 0x00000139 System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteEndOfCentralDirectory(System.Int64,System.Int64,System.Int64,System.Byte[])
extern void ZipHelperStream_WriteEndOfCentralDirectory_m56854BB92BB65EF1D86090ECBA047B3319F54968 (void);
// 0x0000013A System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEShort(System.Int32)
extern void ZipHelperStream_WriteLEShort_m6B78C9254E5B86186ED4C07C35BFB15E76D57A86 (void);
// 0x0000013B System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEUshort(System.UInt16)
extern void ZipHelperStream_WriteLEUshort_mFA40F52C59527AD8935CCB2DC5328F718CA7DE99 (void);
// 0x0000013C System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEInt(System.Int32)
extern void ZipHelperStream_WriteLEInt_m7AFADB5385B180B12A4D8ADA5960067D346980F5 (void);
// 0x0000013D System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLEUint(System.UInt32)
extern void ZipHelperStream_WriteLEUint_m8BA34F76F5E66A1C6B5533B7C50ADD44DEEB8943 (void);
// 0x0000013E System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::WriteLELong(System.Int64)
extern void ZipHelperStream_WriteLELong_m65EE7120C46CA1F5423FADA2EB5EAEAFE0FD4126 (void);
// 0x0000013F System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::.ctor(System.IO.Stream)
extern void ZipInputStream__ctor_m09C60A45C886F5672BF32CA1F1CD19C4E37803B8 (void);
// 0x00000140 System.Boolean ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_CanDecompressEntry()
extern void ZipInputStream_get_CanDecompressEntry_mDC475359069C9C92B7508784333BA115493764CF (void);
// 0x00000141 ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipInputStream::GetNextEntry()
extern void ZipInputStream_GetNextEntry_m6214163CCA6DB35CADD44D8A11CAC68EF73EFF9C (void);
// 0x00000142 System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadDataDescriptor()
extern void ZipInputStream_ReadDataDescriptor_mF2DB8D32975EA418C1E6A0E798AD6A43E5E2783A (void);
// 0x00000143 System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CompleteCloseEntry(System.Boolean)
extern void ZipInputStream_CompleteCloseEntry_mE668EC03951FA303DA78A0F66EF0FC8CF542CDE5 (void);
// 0x00000144 System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CloseEntry()
extern void ZipInputStream_CloseEntry_m7EDC971B19FE393F7120A4099C2284FE34340BDC (void);
// 0x00000145 System.Int64 ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_Length()
extern void ZipInputStream_get_Length_mF42567C765DB95FF420D2735CC41349FA97CF25E (void);
// 0x00000146 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadByte()
extern void ZipInputStream_ReadByte_m60999A08446128FA349D5C5F1C58954D7BD1A26D (void);
// 0x00000147 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotAvailable(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_ReadingNotAvailable_m0DAC56AD9CD250CA27D782FC7910CA3A783EAC87 (void);
// 0x00000148 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotSupported(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_ReadingNotSupported_m2FFD5B7B29EFB6B00486E6156D35C077750850FB (void);
// 0x00000149 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::InitialRead(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_InitialRead_m7BEE10499DBD168BF379287C8F5713A6FA973FA4 (void);
// 0x0000014A System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_Read_m50DE2633F7FC6F114DBB6487CE38C6A49BEDC1A3 (void);
// 0x0000014B System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::BodyRead(System.Byte[],System.Int32,System.Int32)
extern void ZipInputStream_BodyRead_mC7DEF0CB04964D77DA7D16E051B7D05CD8A3D925 (void);
// 0x0000014C System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::Close()
extern void ZipInputStream_Close_m6D197F07A615A05F1FC4ED5F65F8342164B1C463 (void);
// 0x0000014D System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream_ReadDataHandler::.ctor(System.Object,System.IntPtr)
extern void ReadDataHandler__ctor_m53C6B73CBDD079F1A0D393D41D37CE34B79F6023 (void);
// 0x0000014E System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream_ReadDataHandler::Invoke(System.Byte[],System.Int32,System.Int32)
extern void ReadDataHandler_Invoke_m17A11D2E156315570FD94EA50B02862EB85BC830 (void);
// 0x0000014F System.IAsyncResult ICSharpCode.SharpZipLib.Zip.ZipInputStream_ReadDataHandler::BeginInvoke(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void ReadDataHandler_BeginInvoke_m39B4C1E3B3437BEED23262246F77219B4A5F8E1A (void);
// 0x00000150 System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream_ReadDataHandler::EndInvoke(System.IAsyncResult)
extern void ReadDataHandler_EndInvoke_mAA61E4DB07F6E969A10F1593052BED8EEF76D716 (void);
// 0x00000151 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::.ctor(System.IO.Stream)
extern void ZipOutputStream__ctor_m1DAFD8B681063B5BF77E1DFB3828A61A805DA3D8 (void);
// 0x00000152 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::set_UseZip64(ICSharpCode.SharpZipLib.Zip.UseZip64)
extern void ZipOutputStream_set_UseZip64_m71E20223540606C95BBA88742723393CB7DF95B4 (void);
// 0x00000153 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteLeShort(System.Int32)
extern void ZipOutputStream_WriteLeShort_mD394C50E59224E17E8584D51EF2DAD16DF0DC169 (void);
// 0x00000154 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteLeInt(System.Int32)
extern void ZipOutputStream_WriteLeInt_m300B6C706AAF510D1149287F667F4EE8858879A9 (void);
// 0x00000155 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteLeLong(System.Int64)
extern void ZipOutputStream_WriteLeLong_m5885BA54D517B067AEE3E25D1DE420F1431FFB98 (void);
// 0x00000156 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::PutNextEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern void ZipOutputStream_PutNextEntry_mAF22470468086F088C0B724F7D3130373E1EC14C (void);
// 0x00000157 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::CloseEntry()
extern void ZipOutputStream_CloseEntry_m97D40223E9E4C391590D1F8A72409D21286A19CB (void);
// 0x00000158 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteEncryptionHeader(System.Int64)
extern void ZipOutputStream_WriteEncryptionHeader_mCF60CA6E06E72401835E9DAB99437DA435C38E94 (void);
// 0x00000159 System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::AddExtraDataAES(ICSharpCode.SharpZipLib.Zip.ZipEntry,ICSharpCode.SharpZipLib.Zip.ZipExtraData)
extern void ZipOutputStream_AddExtraDataAES_m742C7F3488BA8364FF70C7CDE5827E29102703F4 (void);
// 0x0000015A System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::WriteAESHeader(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern void ZipOutputStream_WriteAESHeader_mB9A0152F2A4DFB001221C065AA62CD5D25152250 (void);
// 0x0000015B System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void ZipOutputStream_Write_m757B89BB7E2911B361ADA5E947FD783077B70781 (void);
// 0x0000015C System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::CopyAndEncrypt(System.Byte[],System.Int32,System.Int32)
extern void ZipOutputStream_CopyAndEncrypt_m60E16266F30C7B999736C9D79D58ED536F2B1575 (void);
// 0x0000015D System.Void ICSharpCode.SharpZipLib.Zip.ZipOutputStream::Finish()
extern void ZipOutputStream_Finish_mE54C47A7003C7DCA7FBBDE684AAE9EBAAD08AA7A (void);
static Il2CppMethodPointer s_methodPointers[349] = 
{
	SharpZipBaseException__ctor_m8C574D72A2DBB387770CEE2BC7793B994C052C31,
	SharpZipBaseException__ctor_m5D31F40276F9D34FA727D5B35D48EBF1EE898504,
	SharpZipBaseException__ctor_m5F7B81277BF8529B1E21F028931A63E529AA723B,
	Adler32_get_Value_m0E563BB11949AA0ABEEF1D6045F54D283D8FD316,
	Adler32__ctor_m189E519C7C20A7DEB2195594F80F18E9F89DD034,
	Adler32_Reset_m8DFF36A606BCE10AC1D7D9E4FD025F1565D94756,
	Adler32_Update_m92B4A996CEE421564653C8643D6EBC571211259A,
	Crc32_ComputeCrc32_mDB88CD370CD8606FA3112FC1FE5B4E6BFAF47987,
	Crc32_get_Value_m21991A1B6074434262599FEF1AC1B3B39465D5FB,
	Crc32_Reset_m4F178EF359E8EF2200516B86E1C2892A7D2FBCE6,
	Crc32_Update_m98B8261D6293B3C258210360FAD2A7851EF346CE,
	Crc32__ctor_mAA446A1931D198D734D8AD7423C82EFCD23D78B9,
	Crc32__cctor_m4CED53AD61612D3ADE805E75088E9DB3CF7BF2AF,
	PkzipClassic_GenerateKeys_mC87C1A9BC91653FB18B8B9333F188BBA2C0A20C1,
	PkzipClassic__ctor_mF0477324F7E255FC027E3ABF1749F6C3E2B232C0,
	PkzipClassicCryptoBase_TransformByte_m44EBD23D951C57E5A055E947BE7E7BE7D53FBC8A,
	PkzipClassicCryptoBase_SetKeys_m13A485E7183C14ECE038E91CD4291F84E14E5462,
	PkzipClassicCryptoBase_UpdateKeys_m97169997AF20F0C99A5C6A563D176276D49FDDA3,
	PkzipClassicCryptoBase_Reset_mE120FE4B0E4C21B546F11B7098549ABD5D253D6C,
	PkzipClassicCryptoBase__ctor_m90EEF2BA70CFBD40B8EF1CEB16D2A89A1A0DCBE8,
	PkzipClassicEncryptCryptoTransform__ctor_mDFDADB7C644E07A22A6D0F3641367AD83640109D,
	PkzipClassicEncryptCryptoTransform_TransformFinalBlock_m8299274F9B3160CCCD7F19E886ED33B831C9E30F,
	PkzipClassicEncryptCryptoTransform_TransformBlock_mA43B7C37A39F4A5C3F574809A12CFD803631E13F,
	PkzipClassicEncryptCryptoTransform_get_InputBlockSize_m9E2CA229F5E350520141346D4224538C50FB26F9,
	PkzipClassicEncryptCryptoTransform_get_OutputBlockSize_mFAC62084FDC9C8B715D32CF4B2A15A2D7F9C0465,
	PkzipClassicEncryptCryptoTransform_get_CanTransformMultipleBlocks_mDC2CED50742CDE1060CF37D731D10EC9DF0F80D7,
	PkzipClassicEncryptCryptoTransform_Dispose_mC7445D0E9D7B0E5CB6969D8ADA0C0CEB38BAC084,
	PkzipClassicDecryptCryptoTransform__ctor_m93C2A9F72B1A1275DB6B2CB452B2C167C80F8702,
	PkzipClassicDecryptCryptoTransform_TransformFinalBlock_m10195ED2BBEB707D6FA1D6F891B15983261A248E,
	PkzipClassicDecryptCryptoTransform_TransformBlock_mB3EA8DAAD292F5D201FDA78E3EB11E8FA25AE6CB,
	PkzipClassicDecryptCryptoTransform_get_InputBlockSize_mC40E55467EFAECE5E5CCB71761AB681AF9829A8D,
	PkzipClassicDecryptCryptoTransform_get_OutputBlockSize_mE799FA513835A118F9E8081079667B1983AC6279,
	PkzipClassicDecryptCryptoTransform_get_CanTransformMultipleBlocks_mE6A5A313A210A168BF8AF2F46F4F7FEC1E8121B7,
	PkzipClassicDecryptCryptoTransform_Dispose_mF0A7E62DBEC69D8D1F02E57411A07A0034D9B205,
	PkzipClassicManaged_get_BlockSize_m349ADCAE37ECBD24111110DF9F8451F6C5AF0AE2,
	PkzipClassicManaged_set_BlockSize_mBEEC77C17A0F7546E4D2A47D05050E999360DE0E,
	PkzipClassicManaged_get_LegalKeySizes_m4D6C76A9236013F8BDBA2F7815916171697DD19F,
	PkzipClassicManaged_GenerateIV_m07424E8014012219B1C2912B374FC7FE6C291889,
	PkzipClassicManaged_get_Key_m25774986983FDEECE78E98ACDDF7AE7D9DF5454D,
	PkzipClassicManaged_set_Key_mF004DEB1FA2514DA3C8BCDAEF3A363501B7474BA,
	PkzipClassicManaged_GenerateKey_mE0373F4EF87959399AF8680696100005E6589C8F,
	PkzipClassicManaged_CreateEncryptor_mC4128CAA756FF1A4AB4F48F120F633B623D22FF8,
	PkzipClassicManaged_CreateDecryptor_m90A2C4DAB94BD9D7AD135A06286829F362AF0E96,
	PkzipClassicManaged__ctor_mFA2033EAC7C2B8CA246EFDE8084171D1130BA95C,
	ZipAESTransform__ctor_mFC621DE8CC0688E6759F5B82C2F8A0F71ADD9690,
	ZipAESTransform_TransformBlock_m85327BA67B33E7B7E38358F929DBC131845D6414,
	ZipAESTransform_get_PwdVerifier_m189484EF21F4401E86432AC112FE6EFD0F10FB93,
	ZipAESTransform_GetAuthCode_mC0C1CE38794ACE7C0C74012A23F5D0B1D943A7DE,
	ZipAESTransform_TransformFinalBlock_mC458BFB8F9905CE47DF46CF52EA21F686A82A54F,
	ZipAESTransform_get_InputBlockSize_m6767965E776543E9DAC1B4A60518A82CAA77C7FC,
	ZipAESTransform_get_OutputBlockSize_m1DB068C8473DC7C396D4C61B0C9C031BF940FB3D,
	ZipAESTransform_get_CanTransformMultipleBlocks_m24EC990AC8AEA9772192142F09BCBE8CFCC60974,
	ZipAESTransform_Dispose_m7774633312C81A8BC2A2E60B5A482E801C5A0630,
	InflaterInputStream__ctor_m2D67F323B922339EEB883B54A9DD7974E9197FE6,
	InflaterInputStream__ctor_mC18255CD57BC7BD161291399EA155E1ED6121CB6,
	InflaterInputStream_Skip_mEFAB40EEF418B5F23280CD6AD94F86A4E0A9149C,
	InflaterInputStream_StopDecrypting_mEF73A6A1850A2E31BF65B658058108933BFE9EF7,
	InflaterInputStream_Fill_m9DDD63F2A2141B597D6D848F98F0C856C24172B8,
	InflaterInputStream_get_CanRead_mF4FEA2080C1C4F195974CD7A5237F8BF4446A859,
	InflaterInputStream_get_CanSeek_m946426BFE7AEB48F3C4C11AAA105895B23734C70,
	InflaterInputStream_get_CanWrite_mED03AA9162B7568E8264A7B6C238E68EE5530F73,
	InflaterInputStream_get_Length_m307D6AF21DE8F4A451ECFC3E789FA59A9B460A3B,
	InflaterInputStream_get_Position_mF61C4CE8F4E36E9BDD7646F4223515996ACE0AF8,
	InflaterInputStream_set_Position_m5A307857AF0190502BABE598E8D0826CE9ED57C2,
	InflaterInputStream_Flush_m361411526D8F2B0C269DF456E4878FDEDB2DB4DE,
	InflaterInputStream_Seek_m02A1A453FA1DDFD093316656412E87DCA81B7D15,
	InflaterInputStream_Write_m98987A2DBA7884F2657FCC654D20519AB73FB25D,
	InflaterInputStream_WriteByte_mE5F3C1F14CAB75C65E6D23C37A6D1B3A7B86A984,
	InflaterInputStream_BeginWrite_mA228DD12469D82A81B02C90CFE78430290606A7D,
	InflaterInputStream_Close_m93B5B6E8483332B9FEE86FD2B575ACA6D6418247,
	InflaterInputStream_Read_m9E31A0C1E5CBB4C85189206C2227FE73F7358ACF,
	DeflaterOutputStream__ctor_m2BFCAF362250F4AE0B55FA890B3DFDCA9924C536,
	DeflaterOutputStream__ctor_mFD1DDBEB0CF1FC7AF628BF5C183A27D37BC93C08,
	DeflaterOutputStream_Finish_mF3A964B96DBD5804755B330F32C2792B67185E3F,
	DeflaterOutputStream_get_CanPatchEntries_mBD7227EB3E9F150D9DE0A98936D020157974BD51,
	DeflaterOutputStream_get_Password_m0A75E0485FA612874D9077DF597FAEC3550F7231,
	DeflaterOutputStream_EncryptBlock_mB09A83CAF5A6B19283934CE6D82824153DDE95ED,
	DeflaterOutputStream_InitializePassword_mDD3CDFDBA90AC01E0B004F1FBCB015C5EAB6F62A,
	DeflaterOutputStream_InitializeAESPassword_m762CE7A5200ACEA61162244A13A849E7D8EEAB23,
	DeflaterOutputStream_Deflate_m491B3347F4BAC0A2B64508AAB4C29EA6BF4D3010,
	DeflaterOutputStream_get_CanRead_m959A760DED202E496B7AF960DF4B5873D301703E,
	DeflaterOutputStream_get_CanSeek_m277D558DFCA9812B5A96540706DCEE04AA557B8A,
	DeflaterOutputStream_get_CanWrite_mFD4C1EBC3D3FF0D0F58A6F2089F7B7C2A7D1313C,
	DeflaterOutputStream_get_Length_mC5F45EC8A9ABB26E7D2469B2718F2131DE0DFF0D,
	DeflaterOutputStream_get_Position_m664B7AEE9A0600BCC14CF0CA2BFF8DBF0B8682B9,
	DeflaterOutputStream_set_Position_m5C87234D7F2BFC50CEFB30ECAC6535988670E3C2,
	DeflaterOutputStream_Seek_mF94638CD607592609AD0AB90727009F9787BEEF4,
	DeflaterOutputStream_ReadByte_m23DDA0D0F19D92EA9619F48DEAA1745AD97E2472,
	DeflaterOutputStream_Read_m9B0ABA7AEC3AB1C611F6D53720BEB24BE785CFBD,
	DeflaterOutputStream_BeginRead_m987D34BDF38102D85EE4E4683633131AC0AD5A28,
	DeflaterOutputStream_BeginWrite_m071D11051F07068E070FA9524A2B3EA5A356D755,
	DeflaterOutputStream_Flush_mAC771D83884862A0223EBA2139DF447F0DDFD524,
	DeflaterOutputStream_Close_m3EEB8909C3F8FCF0B45B81BBE6D9DAF9DA7B502A,
	DeflaterOutputStream_GetAuthCodeIfAES_m3C526DB012AC415CF0E28A9415E60B942D547E01,
	DeflaterOutputStream_WriteByte_m00521396857F2CB1394026B1180B1AE3CC63BB40,
	DeflaterOutputStream_Write_m2FA1251076AF0C741BD1481818E9EA0E62A08572,
	InflaterInputBuffer__ctor_m5C40E106FF29D8186F4958C0789410241684F68E,
	InflaterInputBuffer_get_RawLength_m4E2375D23E77A424666EE7C946D6023172C84710,
	InflaterInputBuffer_get_Available_mD46AA858207E17E410C307D4A580B5F7DD2926D1,
	InflaterInputBuffer_set_Available_m579478077662D91ED88915C6365FAEBFB871A675,
	InflaterInputBuffer_SetInflaterInput_mDB5F86150242CC8015A050EF6C60FAA694103532,
	InflaterInputBuffer_Fill_m1C17B8704FC1052E6330CF736841399CA0274A07,
	InflaterInputBuffer_ReadRawBuffer_m7ED9C21B8BE12B064F8EB61313935F34060D41F6,
	InflaterInputBuffer_ReadRawBuffer_m107AD9F7482F30C1C8A589A76F222A341488C2AD,
	InflaterInputBuffer_ReadClearTextBuffer_mB4E97497E1AF741BBDF5542C9BCCB5FA603538DA,
	InflaterInputBuffer_ReadLeByte_m062AD8D7AEF0D408CD3B686B23E2D5460CC0A499,
	InflaterInputBuffer_ReadLeShort_mA12C80BC2F669B60D272063472FE4E09ADF82DF1,
	InflaterInputBuffer_ReadLeInt_m268A02D6CBBD994453D7D316612A58CF1F969AC0,
	InflaterInputBuffer_ReadLeLong_m36FF2F3F8573DB88BD8E30B2748BF202C25E5E50,
	InflaterInputBuffer_set_CryptoTransform_mCACE1607E0EA8FFA2A32DC404D9DA5F718A1A39E,
	OutputWindow_Write_m1EA5E2CC2935FDA915230BAA4E36198ED4380BDF,
	OutputWindow_SlowRepeat_m7F12C1EE0873B617985C8DB6C33BBC762020484E,
	OutputWindow_Repeat_m8FA0401694BC172B983A8CB7C285DFC21634A726,
	OutputWindow_CopyStored_mB719F206FABB0103860333E29A7DEB591044B8E0,
	OutputWindow_GetFreeSpace_m5155AC048E9DD283E58A627221A6629B807D2083,
	OutputWindow_GetAvailable_m265560A962D99072E86EBC99BCCDF2174B79E7B6,
	OutputWindow_CopyOutput_m5FF61442E27D6C4AD742FE89D17645689E9E014B,
	OutputWindow_Reset_mF2B99AE284F54A7D07B9F1C29452408D40BC6F26,
	OutputWindow__ctor_m0B67D27A7EAFEE54A927E889E6C85DE4BBC5C63D,
	StreamManipulator__ctor_mAF3E5C7ADFFF99059D244DD1DA02081E5F5948DD,
	StreamManipulator_PeekBits_mC33B6EEBBBECC21466FC2683CE3AEEA471D7031D,
	StreamManipulator_DropBits_mEDE078F4C1A6E3EABD6ED74F94AFDFCFC2E11468,
	StreamManipulator_get_AvailableBits_m8500BA0CFAEF99E784BA5C6FB1073CDA39770563,
	StreamManipulator_get_AvailableBytes_m2D908CBD7CC371151DC519828D8E21F3A37EC7F9,
	StreamManipulator_SkipToByteBoundary_mF31C8FC9B1E972DFBC9470EC32AA889740BCFB8B,
	StreamManipulator_get_IsNeedingInput_m58ED090B35C6E27DEA1DB3F75991D4BD79597646,
	StreamManipulator_CopyBytes_m51F37B1454498549CF310B851AD77563006182CA,
	StreamManipulator_Reset_m4191FF2F24CBF4F41D2DC12213FDD6019344F937,
	StreamManipulator_SetInput_mB46C735CCF621ABCDED412FA7FB268B0C22668EE,
	Deflater__ctor_m57CB8C4A1FBCFF2923DD8522EB8E196700B83D77,
	Deflater_Reset_m30DA5E7A7FDC2D946D85D97F0D420DC735CEB7D0,
	Deflater_get_TotalOut_m239B1C30F62A30507E35FBDF8785AC622B6E04B6,
	Deflater_Flush_mC0EB1DC4AAE1D05EA2DDCD5B9906F43CA91942BC,
	Deflater_Finish_m382411296C20000FFE2F07E2783710CFA1619EDA,
	Deflater_get_IsFinished_m39E54240221DFB16E5E59C131B0074406267DEB1,
	Deflater_get_IsNeedingInput_mAB7D6D34D94A03A1DEDF832AA86F852EF6E6A5A5,
	Deflater_SetInput_mE0523323E987118BB736BED5671C874060E14423,
	Deflater_SetLevel_mE4532EDBAD0768FDE51C50884FF4BC746D9852D9,
	Deflater_SetStrategy_m606E78CC7284058DDEE3C53903185DAACE025CD6,
	Deflater_Deflate_mD0F6011C3D7336386B00F502D9685FE718A8B96C,
	DeflaterConstants__ctor_m13587F1586B73023DD07EB2FF778DABA53E7C574,
	DeflaterConstants__cctor_m368AF88FF2E825B4148CAE398EC92993034C4AC9,
	DeflaterEngine__ctor_m69EF65BACD1DA939C9FFC2B5C1D1475EC7A0DC83,
	DeflaterEngine_Deflate_mA9D11BF772015B55537B31A6B29C629D46839A4E,
	DeflaterEngine_SetInput_m9438FB0A3F36E3C76D02F8578E29261802FC577B,
	DeflaterEngine_NeedsInput_mF2E07871A55D7F4752D7BD5465C415D8C5C3FAB3,
	DeflaterEngine_Reset_m196587E7E4DC938F1B500BFFAADA62466A1E016C,
	DeflaterEngine_ResetAdler_mA249980BBC22D9D47D065B7690B37C86A5E94C68,
	DeflaterEngine_get_Adler_mFB7A59C55E20B8B0AA1EDDBA9F24586F5AE79C52,
	DeflaterEngine_set_Strategy_m79C64440BA8C81248CFEA8D33B5E6FF52BDA85A1,
	DeflaterEngine_SetLevel_m2A1E629F0843C41B46AC75B4D96C9EEFEB46397B,
	DeflaterEngine_FillWindow_mA2B47111117B204C8A4657E7741046546027E0C2,
	DeflaterEngine_UpdateHash_m4F3A17A71FF0FCCE29663BCB21370C0ED6F8A5A6,
	DeflaterEngine_InsertString_mACF16A7C87FE666ED50B372ACC47E06986DCDDDC,
	DeflaterEngine_SlideWindow_m94FEA3F82B8C517D540B5E175B5BF1C93F884941,
	DeflaterEngine_FindLongestMatch_mB114F771F305D63FEAF011FC44C555C011CFEDBA,
	DeflaterEngine_DeflateStored_m3D121267BB6F64782CAB3EFA51BA04D38E309A06,
	DeflaterEngine_DeflateFast_m7A173501C97F9B7CAE906972C18DFB5727E0C479,
	DeflaterEngine_DeflateSlow_mC6F9DFEE6F8B6DBFE75E8CD20A546F80B75DD42E,
	DeflaterHuffman__cctor_mD72DFCC98B9C9DF8B42EB7603141D5CF98BD60E8,
	DeflaterHuffman__ctor_m0E5611060CFCBBEB4B08F3C33AC1A8BB76FB1777,
	DeflaterHuffman_Reset_m8E61516F51334A9B629F8F40ADF3B8B584F8BB86,
	DeflaterHuffman_SendAllTrees_m9BB6B306AA29CC46D3E3BA81F9BD64AB13A3BB48,
	DeflaterHuffman_CompressBlock_m89038ADA557D80FEC1972B4E7D199BAA3C5A9A52,
	DeflaterHuffman_FlushStoredBlock_mAAB7BE6CAC5ABB6738A66A1C19BFBA063C22A5BF,
	DeflaterHuffman_FlushBlock_mB67D6DB018DBBA5AE108235F523F420E9A466474,
	DeflaterHuffman_IsFull_m72587953732A715A7231FCDBF377C4D9C8DC3DA9,
	DeflaterHuffman_TallyLit_mC47AB48E45F67F7CEC4F2A9D9237C44D4778B68D,
	DeflaterHuffman_TallyDist_mB0C593D47758BEC3193E2BB4642018BE669C635D,
	DeflaterHuffman_BitReverse_m180F41F7EBFEE48A0D37253024C62186C35FF580,
	DeflaterHuffman_Lcode_m861BAFFA67C4DE9717AF87CE465669774C97299A,
	DeflaterHuffman_Dcode_mF865E06FCB31FF4D0AC2A13CBE99E7BC63AB3D59,
	Tree__ctor_mA614B1C230AE89F28C5B1E72DD69C897182239A8,
	Tree_Reset_mDDD9D3366F78EF20B9A0B4D1E511FB1EAFEDB0EE,
	Tree_WriteSymbol_m635AE2D405DAE9FF9167DBEBB94F60CCCD59B3DA,
	Tree_SetStaticCodes_mEC15A35B2B2CFF8F17477D0177432BBB56852971,
	Tree_BuildCodes_m81EF550BD19A0105B4E7F1C4DD9E8DD793579B39,
	Tree_BuildTree_mE2D4A8C2E84B3769B982878EC6481DAD8CDE432B,
	Tree_GetEncodedLength_m7A0700F98E3BCD64AD5AAEE42CC3742CCFBDC8A9,
	Tree_CalcBLFreq_m7A86ED8496A97A4D4881F4E3347E6EB153441072,
	Tree_WriteTree_m122FC81F15E2D670B1B097B458C7DF5E06022683,
	Tree_BuildLength_m0F6E015DD5FF1D1C7E1D9D6DB1895D678D8D9428,
	PendingBuffer__ctor_m2807D9EE3C2C24AEAEEBB20D2610BB2E158ED1A6,
	PendingBuffer_Reset_m99B076AACDEA9EF12EEAF6619EF784AEF0785A7A,
	PendingBuffer_WriteShort_m1D7325AD314B385178974FD5DA91F3583488AB0D,
	PendingBuffer_WriteBlock_m972E0492F2C4451376175E4D94CC44D02BCB9FAF,
	PendingBuffer_get_BitCount_mDEBD5EF56A155066CF45F097AFE625D96C5AD905,
	PendingBuffer_AlignToByte_m3E370D075FF049034AB69141A815EE147886402D,
	PendingBuffer_WriteBits_mD1F29B539D8541020DE7517ECC6BD334EA8CB279,
	PendingBuffer_WriteShortMSB_m4E52464ABDD32F254442E0832EDE35FEC279F0CB,
	PendingBuffer_get_IsFlushed_m5ECF252760839CF44C49302BB9E7E937EF0EE015,
	PendingBuffer_Flush_m480024E2E6D8E73043C6325D04E8CC0F2ED7A34A,
	DeflaterPending__ctor_mD92E0B1626CEFAC57F70D807742063E62EFB7902,
	Inflater__ctor_m58B684F78C1B58D26CC7C6A2E729A0D07402E56F,
	Inflater_Reset_m8451B365C0D2E7070FB3105ED9DEC7AC8711B239,
	Inflater_DecodeHeader_m14EE76D62FC98EC2780CABDC8E7FF58B443A0FE5,
	Inflater_DecodeDict_mA6570C196A64149B289CFA165682D8FFA0EADE1C,
	Inflater_DecodeHuffman_mEA57C000BE39233D53CED45DEA58265BF37AEA43,
	Inflater_DecodeChksum_mD51CB6ADF6C3663F612E77085D5EC01BCE08DE6D,
	Inflater_Decode_m6CA877FD4DF171E36352527E8B8098C7EBE51E13,
	Inflater_SetInput_m8D6E5C4C16F58A65A223788263AADA7B63A77D4D,
	Inflater_Inflate_mFBB808BE146122CE8EB46207AB6DA4F3AD958A60,
	Inflater_get_IsNeedingInput_mD15EAFB849F34802D253C96470D860859ABAADCB,
	Inflater_get_IsNeedingDictionary_mB76ECC03A203D3E02D780A0ECBC028B143301418,
	Inflater_get_IsFinished_mC81B8579D62FD0F072A25E92D8FB98939853DE29,
	Inflater_get_TotalOut_mA36DE58122D832F7398842FCC2B051A04D791F28,
	Inflater_get_TotalIn_m7FB88153C0C290B650FC1A9367533A1BD1ED9FD4,
	Inflater_get_RemainingInput_mA27E6FEEFC734F6CB34B4C86C09D9C58DFA2AC26,
	Inflater__cctor_m3912F4163CBE87F8044EB15BC71C08614B44161F,
	InflaterDynHeader__ctor_m9417085430C2F2E079578BBD888B2F84126E2E79,
	InflaterDynHeader_Decode_m2EC8822D0676C54EF74A9159D85BAA42815662F4,
	InflaterDynHeader_BuildLitLenTree_m8E21490D1B5FB54BE3FEB8AE7E4DC6422AB82C21,
	InflaterDynHeader_BuildDistTree_m218FF52158FE818581F7EFD89A4280329989158E,
	InflaterDynHeader__cctor_mE125FD860474122FD6E476717D451672EF78BB8F,
	InflaterHuffmanTree__cctor_m2BD112AE4E9C06A5CA5A3191C26B891C1D07F253,
	InflaterHuffmanTree__ctor_m94855E1E5CC7CC9CE3035A525E4BB10FA0111A7A,
	InflaterHuffmanTree_BuildTree_m7AB8EA6CD97AD54855E08B8825E5958991E7D382,
	InflaterHuffmanTree_GetSymbol_m5D37F51DC607A632A1A182F5C0673B6ABD7D918C,
	ZipConstants_get_DefaultCodePage_mB06F1B4565607B1C38345A8A5CE9D63B0F5C1B0C,
	ZipConstants_ConvertToString_mB2022333FBF462A12086D7FF962A64A836889E85,
	ZipConstants_ConvertToStringExt_m24B37DA0A06D42C54C13A4FD53AE60B49F7F03E4,
	ZipConstants_ConvertToArray_mD029ED36EAA500F69FEFDFC108E2EC339E578733,
	ZipConstants_ConvertToArray_mA70267A31A3C782412341244A8C3C1F0E7E9DA55,
	ZipConstants__cctor_mF46C75FBEFF7667D5E0FEFC5AD7EFF996E98582A,
	ZipEntry__ctor_mE75AE4978F145D399A272CA1695A7E6840EF2E78,
	ZipEntry__ctor_mD0CC6DE52D77799A812856C64D0BC57F3D7834D1,
	ZipEntry__ctor_mDA6564263E314CF570F57BA5EC8403535D302EED,
	ZipEntry_get_HasCrc_mB50F6087F1BB279F4D3A49C9DA41749CE4C807C0,
	ZipEntry_get_IsCrypted_m67AC3A78E4EA97DF33289AE38C05591EA15A9EC6,
	ZipEntry_set_IsCrypted_m4BE89AE22B3C5F2009A2C2702A5490D69361974A,
	ZipEntry_get_CryptoCheckValue_m40A5DD7932EF09DB3C2F1FD1C60E0DD4A7818E66,
	ZipEntry_set_CryptoCheckValue_m5EE9D24AA04321C54A98B8AF50D6842BF1D802D3,
	ZipEntry_get_Flags_m9875608730955C1ED8ECFD422F65C853A49418C2,
	ZipEntry_set_Flags_m8D422FED00F0C9F3D1E0132889779F02032308D5,
	ZipEntry_get_Offset_m6BD5F5C70367EFC1AA07B7AF12F4F5B6A324B8BA,
	ZipEntry_set_Offset_m37E437585AB681C720FAAF86696665C534505209,
	ZipEntry_get_ExternalFileAttributes_m369EB1A3B8E4A6399710048318A7330DBF8B12FD,
	ZipEntry_HasDosAttributes_mB81C9009D0C60D67F4E1B33B70C6C70D073F92D0,
	ZipEntry_get_HostSystem_mE2E80A099A6F9E0196CDCB58F039F8EB06BD20AB,
	ZipEntry_get_Version_m186D72ECEC69C87CAD85EF7D110D7D864C42A2DE,
	ZipEntry_get_CanDecompress_m8ABCED141E8057C5E08A1EC9B65A79DF3ECFCCFD,
	ZipEntry_ForceZip64_m03801A793D168D8A0B67A1A0FBE46B9A554A2AFA,
	ZipEntry_IsZip64Forced_mFF7B0A2B129A4CCDA19E966F54A368B136E24AB5,
	ZipEntry_get_LocalHeaderRequiresZip64_m484ECEF0804BA1E0A93B482DF163A9B29065B50F,
	ZipEntry_get_CentralHeaderRequiresZip64_mC9DF5350EEDCBBEEB3E55943D6320BD2A37B0273,
	ZipEntry_get_DosTime_m075FC7A087884B7A896184E18B8B5FFFB69E27E6,
	ZipEntry_set_DosTime_mC3F6EAB77DDFBEEB1793FE88FF5006CD73B17165,
	ZipEntry_set_DateTime_m06237CCB8F2A23BB4D25F0CF2C9B3FFB51132F5C,
	ZipEntry_get_Name_m29BD4E2597CF1C73560D3266D22DB907556A10BE,
	ZipEntry_get_Size_m1FAB6C21B24342AC715A008BDE608FDF6EEF65D5,
	ZipEntry_set_Size_mDFE3D6D8EBA7A0D11CBAC65768A0672120569F73,
	ZipEntry_get_CompressedSize_mBB45D815684D26B45E24711212F0B4E1F0C74E89,
	ZipEntry_set_CompressedSize_mF1AD1055D95022048219D75ADC30B598A232A357,
	ZipEntry_get_Crc_mA4D7319A6B039A26056CCF7B832119ACECCDDD5B,
	ZipEntry_set_Crc_m26E9B25A3A02DE98E385FEE9A4336590A5C872CD,
	ZipEntry_get_CompressionMethod_mA498B8DC016C4893C0D16ABD85BB857C4E916895,
	ZipEntry_set_CompressionMethod_mA38F4CB0E027658922D29A6061AD5229FD211941,
	ZipEntry_get_CompressionMethodForHeader_m5101B3F24AD863F218A6B8DDA0EDC0D1F0F9DFBD,
	ZipEntry_get_ExtraData_mD0160EC8AD0A3EFB1128DC1B59DB8B361952599F,
	ZipEntry_set_ExtraData_mA38232939D5667482C6004A2B1C666085E65BF5F,
	ZipEntry_get_AESKeySize_mBEE765C4AF081DE3D2BFBEAFF44EDD5BB51F5829,
	ZipEntry_get_AESEncryptionStrength_m56B07314C8CE286E97E8AD336D3A0F2AAC489C0A,
	ZipEntry_get_AESSaltLen_mE03E8E5F09E7B669C084EF98871C709EDE394681,
	ZipEntry_get_AESOverheadSize_m7E4874E31E6523E9C50AE517385E301376E9FDFA,
	ZipEntry_ProcessExtraData_mC601598FCF73135556DA39EF1A0ADB5CF4B5670C,
	ZipEntry_ProcessAESExtraData_m774EDF5EB687BD9B1C20816558D270F7822DF5EF,
	ZipEntry_get_Comment_m4A8F1B84DB7DE3168370A9D83D4882755BB385AE,
	ZipEntry_get_IsDirectory_m37F50B2FA96030F68DFFC7255D9F10ECF26C0DDD,
	ZipEntry_IsCompressionMethodSupported_mCA65FD1F20017893DF3054A02D2CCD1254F825AE,
	ZipEntry_Clone_m088F3ACABBE83A0153966D8250DF39E4AD2072AE,
	ZipEntry_ToString_m59F693E4060C3BC9870910CB959202EC4C782E3C,
	ZipEntry_IsCompressionMethodSupported_m6DD6C3F45BB642E34E558E641F9EB3DC78CD2B9A,
	ZipException__ctor_mEA31871219DACB3B1D876369CF918068BC976E21,
	ZipException__ctor_mA35D0790599840E2231872EECF9B3B40B3B8CEDF,
	ZipException__ctor_m82958DBCA7D000F61715DEBF1C9F65311F2A3109,
	ZipExtraData__ctor_m9ADBD056FDD05FF38B8BB04724EAA1B47671B47C,
	ZipExtraData_GetEntryData_m07BD3E631D8C157346C01AD133D5CA2824EF1F0F,
	ZipExtraData_get_Length_mAAB1D0B75A8CEA239EDDE82947E9ADC2EF77934E,
	ZipExtraData_get_ValueLength_m2DF15993544BB0630402C80173360BAB4395BA12,
	ZipExtraData_get_CurrentReadIndex_mDB44C1CB9C938EB221860FA54ADB4FC28AF823D6,
	ZipExtraData_get_UnreadCount_mCAF8DA3C0FD0138604238B7571830C160D724EAE,
	ZipExtraData_Find_mA0D23ADA4738A8BF7EA1C258E39AF7FB018D602E,
	ZipExtraData_AddEntry_mAEA2E2A9A52E2FA158DED4857C1164D752D87870,
	ZipExtraData_StartNewEntry_m70FD6B95429F080F80ECFB4FF55319D8E475F6C3,
	ZipExtraData_AddNewEntry_mD730C3BA770600752A284C04FE5FFB24AA66881F,
	ZipExtraData_AddData_m5902477D0D0F4A44877596B584073EE78C5CC653,
	ZipExtraData_AddLeShort_m785DEBD288A11C2E867D2DAA5B1FE6703B86FF52,
	ZipExtraData_AddLeInt_m659D2CB49C61FDF5083F039C1F68B1AFF8184DDA,
	ZipExtraData_AddLeLong_mB1394AB0487B4E5B1ADDC8C8952C3125C017F2B9,
	ZipExtraData_Delete_m9B1AC771EE50FB327E373D0DFE35E0A62DB36FFA,
	ZipExtraData_ReadLong_m23337B6C32C586EBAB86F70970E1AC73245915AD,
	ZipExtraData_ReadInt_mB3FF89E08BC05335BA073189F8BE1FC3A104AEAE,
	ZipExtraData_ReadShort_mDD80B097D4D3986F8977FFC4B72D69B085D9BC39,
	ZipExtraData_ReadByte_m196A33F4701DAB055BA59EAE11166DECEDE7CD1B,
	ZipExtraData_Skip_m0E3AFFE37C487B025913B571470926AECD8E009A,
	ZipExtraData_ReadCheck_m552ED12A1C092593C722BBEDF1F82B1CCC50EA14,
	ZipExtraData_ReadShortInternal_m5BF71B645A7499EB2613D76709C3C8FA9C54EA54,
	ZipExtraData_SetShort_m39E1FC2020B3E51DA52AD716A9907F53CFD73656,
	ZipExtraData_Dispose_mA4669C59E52C9304F8539E7A23D797780A7E4183,
	ZipHelperStream__ctor_m84FB7A0C7A65C48684E782BC12C0E75D1E1202CC,
	ZipHelperStream_get_CanRead_mD14AD502F6130E802296115E2F8B44CAE17A5372,
	ZipHelperStream_get_CanSeek_mDB8C9C4B4F9B666817C029F3874174E25C288231,
	ZipHelperStream_get_Length_m157DB0B54B7E470870C3383D39F7F238527D38D0,
	ZipHelperStream_get_Position_m7386EB504DCD827CD85D35B6AB9360CA02E88482,
	ZipHelperStream_set_Position_mC6F3BDCC2CC8939953CFF8437E25B605182D4EF4,
	ZipHelperStream_get_CanWrite_mC7C73FA037F7AC002C187C42BB901CD187CB0A8E,
	ZipHelperStream_Flush_m627493F2903D0D767800F779956A1B5D158109D2,
	ZipHelperStream_Seek_m57F0879FC7BA9EF97C1A75F360EFAEB92873EC13,
	ZipHelperStream_Read_m60DEFE46550FFC416E9638847B4E8A7FABB30EF9,
	ZipHelperStream_Write_m16C28AE4AB3226C201E6542A84EEB332FE892DE4,
	ZipHelperStream_Close_m60EF560F88DF3D51009F31D3FC130C7EF144BBCB,
	ZipHelperStream_WriteZip64EndOfCentralDirectory_mE2E45FCCD8DABF985957FE6D2D3679ECE744E12A,
	ZipHelperStream_WriteEndOfCentralDirectory_m56854BB92BB65EF1D86090ECBA047B3319F54968,
	ZipHelperStream_WriteLEShort_m6B78C9254E5B86186ED4C07C35BFB15E76D57A86,
	ZipHelperStream_WriteLEUshort_mFA40F52C59527AD8935CCB2DC5328F718CA7DE99,
	ZipHelperStream_WriteLEInt_m7AFADB5385B180B12A4D8ADA5960067D346980F5,
	ZipHelperStream_WriteLEUint_m8BA34F76F5E66A1C6B5533B7C50ADD44DEEB8943,
	ZipHelperStream_WriteLELong_m65EE7120C46CA1F5423FADA2EB5EAEAFE0FD4126,
	ZipInputStream__ctor_m09C60A45C886F5672BF32CA1F1CD19C4E37803B8,
	ZipInputStream_get_CanDecompressEntry_mDC475359069C9C92B7508784333BA115493764CF,
	ZipInputStream_GetNextEntry_m6214163CCA6DB35CADD44D8A11CAC68EF73EFF9C,
	ZipInputStream_ReadDataDescriptor_mF2DB8D32975EA418C1E6A0E798AD6A43E5E2783A,
	ZipInputStream_CompleteCloseEntry_mE668EC03951FA303DA78A0F66EF0FC8CF542CDE5,
	ZipInputStream_CloseEntry_m7EDC971B19FE393F7120A4099C2284FE34340BDC,
	ZipInputStream_get_Length_mF42567C765DB95FF420D2735CC41349FA97CF25E,
	ZipInputStream_ReadByte_m60999A08446128FA349D5C5F1C58954D7BD1A26D,
	ZipInputStream_ReadingNotAvailable_m0DAC56AD9CD250CA27D782FC7910CA3A783EAC87,
	ZipInputStream_ReadingNotSupported_m2FFD5B7B29EFB6B00486E6156D35C077750850FB,
	ZipInputStream_InitialRead_m7BEE10499DBD168BF379287C8F5713A6FA973FA4,
	ZipInputStream_Read_m50DE2633F7FC6F114DBB6487CE38C6A49BEDC1A3,
	ZipInputStream_BodyRead_mC7DEF0CB04964D77DA7D16E051B7D05CD8A3D925,
	ZipInputStream_Close_m6D197F07A615A05F1FC4ED5F65F8342164B1C463,
	ReadDataHandler__ctor_m53C6B73CBDD079F1A0D393D41D37CE34B79F6023,
	ReadDataHandler_Invoke_m17A11D2E156315570FD94EA50B02862EB85BC830,
	ReadDataHandler_BeginInvoke_m39B4C1E3B3437BEED23262246F77219B4A5F8E1A,
	ReadDataHandler_EndInvoke_mAA61E4DB07F6E969A10F1593052BED8EEF76D716,
	ZipOutputStream__ctor_m1DAFD8B681063B5BF77E1DFB3828A61A805DA3D8,
	ZipOutputStream_set_UseZip64_m71E20223540606C95BBA88742723393CB7DF95B4,
	ZipOutputStream_WriteLeShort_mD394C50E59224E17E8584D51EF2DAD16DF0DC169,
	ZipOutputStream_WriteLeInt_m300B6C706AAF510D1149287F667F4EE8858879A9,
	ZipOutputStream_WriteLeLong_m5885BA54D517B067AEE3E25D1DE420F1431FFB98,
	ZipOutputStream_PutNextEntry_mAF22470468086F088C0B724F7D3130373E1EC14C,
	ZipOutputStream_CloseEntry_m97D40223E9E4C391590D1F8A72409D21286A19CB,
	ZipOutputStream_WriteEncryptionHeader_mCF60CA6E06E72401835E9DAB99437DA435C38E94,
	ZipOutputStream_AddExtraDataAES_m742C7F3488BA8364FF70C7CDE5827E29102703F4,
	ZipOutputStream_WriteAESHeader_mB9A0152F2A4DFB001221C065AA62CD5D25152250,
	ZipOutputStream_Write_m757B89BB7E2911B361ADA5E947FD783077B70781,
	ZipOutputStream_CopyAndEncrypt_m60E16266F30C7B999736C9D79D58ED536F2B1575,
	ZipOutputStream_Finish_mE54C47A7003C7DCA7FBBDE684AAE9EBAAD08AA7A,
};
static const int32_t s_InvokerIndices[349] = 
{
	111,
	23,
	26,
	184,
	23,
	23,
	35,
	306,
	184,
	23,
	35,
	23,
	3,
	0,
	23,
	89,
	26,
	31,
	23,
	23,
	26,
	54,
	113,
	10,
	10,
	89,
	23,
	26,
	54,
	113,
	10,
	10,
	89,
	23,
	10,
	32,
	14,
	23,
	14,
	26,
	23,
	105,
	105,
	23,
	750,
	113,
	14,
	14,
	54,
	10,
	10,
	89,
	23,
	27,
	118,
	984,
	23,
	23,
	89,
	89,
	89,
	184,
	184,
	210,
	23,
	742,
	35,
	31,
	743,
	23,
	512,
	27,
	118,
	23,
	89,
	14,
	35,
	26,
	589,
	23,
	89,
	89,
	89,
	184,
	184,
	210,
	742,
	10,
	512,
	743,
	743,
	23,
	23,
	23,
	31,
	35,
	130,
	10,
	10,
	32,
	26,
	23,
	112,
	512,
	512,
	10,
	10,
	10,
	184,
	26,
	32,
	38,
	129,
	511,
	10,
	10,
	512,
	23,
	23,
	23,
	37,
	32,
	10,
	10,
	23,
	89,
	512,
	23,
	35,
	133,
	23,
	184,
	23,
	23,
	89,
	89,
	35,
	32,
	32,
	512,
	23,
	3,
	26,
	669,
	35,
	89,
	23,
	23,
	10,
	32,
	32,
	23,
	23,
	10,
	23,
	30,
	669,
	669,
	669,
	3,
	26,
	23,
	32,
	23,
	1038,
	1038,
	89,
	30,
	52,
	222,
	21,
	21,
	204,
	23,
	32,
	27,
	23,
	23,
	10,
	26,
	26,
	26,
	32,
	23,
	32,
	35,
	10,
	23,
	129,
	32,
	89,
	512,
	23,
	31,
	23,
	89,
	89,
	89,
	89,
	89,
	35,
	512,
	89,
	89,
	89,
	184,
	184,
	10,
	3,
	23,
	9,
	14,
	14,
	3,
	3,
	26,
	26,
	112,
	106,
	119,
	132,
	0,
	132,
	3,
	26,
	130,
	204,
	89,
	89,
	31,
	89,
	31,
	10,
	32,
	184,
	210,
	10,
	30,
	10,
	10,
	89,
	23,
	89,
	89,
	89,
	184,
	210,
	334,
	14,
	184,
	210,
	184,
	210,
	184,
	210,
	10,
	32,
	10,
	14,
	26,
	10,
	89,
	10,
	10,
	31,
	26,
	14,
	89,
	89,
	14,
	14,
	46,
	111,
	23,
	26,
	26,
	14,
	10,
	10,
	10,
	10,
	30,
	62,
	23,
	32,
	31,
	32,
	32,
	210,
	30,
	184,
	10,
	10,
	10,
	32,
	32,
	10,
	64,
	23,
	26,
	89,
	89,
	184,
	184,
	210,
	89,
	23,
	742,
	512,
	35,
	23,
	1476,
	1507,
	32,
	614,
	32,
	32,
	210,
	26,
	89,
	14,
	23,
	31,
	23,
	184,
	10,
	512,
	512,
	512,
	512,
	512,
	23,
	124,
	512,
	743,
	112,
	26,
	32,
	32,
	32,
	210,
	26,
	23,
	210,
	137,
	26,
	35,
	35,
	23,
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationICSharpCode_SharpZipLib;
extern const Il2CppCodeGenModule g_ICSharpCode_SharpZipLibCodeGenModule;
const Il2CppCodeGenModule g_ICSharpCode_SharpZipLibCodeGenModule = 
{
	"ICSharpCode.SharpZipLib.dll",
	349,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	&g_DebuggerMetadataRegistrationICSharpCode_SharpZipLib,
};
