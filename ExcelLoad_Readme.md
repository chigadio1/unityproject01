# Excel_LoadData
* [Readme](https://gitlab.com/chigadio1/unityproject01/-/blob/main/README.md)
## 使用したもの
* NPOI(C#でExcelのデータを読み込むため)

## 概要
ExcelでのデータをUnityのScriptbleObjectでスムーズに読み込みが可能<br>
また、Jsonやバイナリでの書き込みに対応し、様々なところで活用するよう設計

## Script
* DataFrame,DataFrameControl,DataFrameGroup -> Excelでの読み込みで一時保存で使うデータクラス
* BaseExcelScriptableObject,BaseComposition -> ScriptablePbjectで使用する基礎クラス
* ListDataObject -> BaseComposition(派生クラスも)のデータをリストにまとめるクラス
* BaseExcelData  -> Excelで読み込んだのを、ScriptableObjectに変換するためのクラス
* ExcelSystem -> Excelの読み込みやコンバートなどを行うクラス（コア部分）
* ExcelBinary,ExcelJson -> バイナリ、Jsonに対応するクラス

## Tutorial

### Step1 Excelでデータを作ろう!
<img src=/uploads/4a9883d9fe9d3c3ae84d783a269ce3eb/ExcelData.png width = "450px" height = "210px">
<br>
こんな感じで、一番上の行に必ずデータ群の識別名を書き、その下からデータを入力してください！

### Step2 データ用のScriptableObjectのスクリプトを作ろう!

下記コードのように、スクリプトを作成してください。<br>
ManualSetUpで、Excelで書いたデータの名称に合わせてください。
```c#:TokimemoData.cs
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sample
{
    /// <summary>
    /// サンプルデータ
    /// </summary>
    [System.Serializable]
    public class TokimemoData : BaseComposition
    {
        /// <summary>
        /// 名前
        /// </summary>
        [SerializeField]
        private string girl_name_ = "";
        public string GetGirlName() { return girl_name_; }

        /// <summary>
        /// 誕生月
        /// </summary>
        [SerializeField]
        private int birth_month_ = 0;
        public int GetBirthMonth() { return birth_month_; }

        /// <summary>
        /// 誕生日
        /// </summary>
        [SerializeField]
        private int birth_day_ = 0;
        public int GetBirthDay() { return birth_day_; }

        /// <summary>
        /// 身長
        /// </summary>
        [SerializeField]
        private float height_ = 0;
        public float GetHeight() { return height_; }

        /// <summary>
        /// 体重
        /// </summary>
        [SerializeField]
        private float weight_ = 0;
        public float GetWeight() { return weight_; }

        /// <summary>
        /// 胸
        /// </summary>
        [SerializeField]
        private float breast_ = 0;
        public float GetBreast() { return breast_; }

        /// <summary>
        /// 腰
        /// </summary>
        [SerializeField]
        private float　waist_ = 0;
        public float GetWaist() { return waist_; }

        /// <summary>
        /// 尻
        /// </summary>
        [SerializeField]
        private float hip_ = 0;
        public float GetHip() { return hip_; }


        public override void ManualSetUp(ref DataFrameGroup data_grop, ref ProjectSystem.ExcelSystem.DataGroup excel_data_group)
        {
            data_grop.AddData("名前", nameof(girl_name_), girl_name_);
            data_grop.AddData("誕生月", nameof(birth_month_), birth_month_);
            data_grop.AddData("誕生日", nameof(birth_day_), birth_day_);
            data_grop.AddData("身長", nameof(height_), height_);
            data_grop.AddData("体重", nameof(weight_), weight_);
            data_grop.AddData("B", nameof(breast_), breast_);
            data_grop.AddData("W", nameof(waist_), waist_);
            data_grop.AddData("H", nameof(hip_), hip_);
        }
    }
}
```
<br>
<br>
<br>
BaseExcelScriptableObjectを継承したクラスを作成し、ScriptableObjectが作れるようにしておきます。<br>
また、それようのエディタを作り、EditorExcelScriptableObjectInspector.OnGUIを使えば簡単に設定できるようになります。

```c#:TokimemoScriptableData.cs
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(Sample.TokimemoScriptableData))]
public class EditorTokimekiScriptableData : Editor
{
    public override void OnInspectorGUI()
    {
        EditorExcelScriptableObjectInspector.OnGUI<Sample.TokimemoData>(target as BaseExcelScriptableObject<Sample.TokimemoData>);
        base.OnInspectorGUI();
    }
}


#endif

namespace Sample
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Sample/CreateTokimekiData")]
    public class TokimemoScriptableData : BaseExcelScriptableObject<TokimemoData>
    {
    }
}

```

### Step3 読み込みたいExcelを選択し、ロードしよう！

### サンプルファイル位置

基本的に、ExcelとScriptableは同じ場所においてください。
[ExcelSampleProjectFolder](/uploads/a6bdd86f2ef85c460030a7df6e520340/ExcelSampleProjectFolder.png)

#### 使い方

Inspectorを確認すれば、様々な情報がのっておりますが、一番上のIDに注目してください。<br>
ここは、先ほど画像でのせましたファイル名番号を書きます。<br>
その後、Excelと折りたためられてるところをクリックしパス指定を指定してください。<br><br>
シート名も指定し、Createボタンを押せばこのように情報が書き足されます。
![ExcelCreateScriptable](/uploads/44fa2734eba0198d384b54fbee3f82c5/ExcelCreateScriptable.gif)

### Step4 Json,Binaryに変換しよう！
JsonとBinary変換も簡単にし、下記画像のようにPath先を指定しファイル名を入力し、ボタンを押せば作成されまた読み込みできるようになります<br>
![Excel_JsonBinary](/uploads/d383f7fafec6c6dba0f77356fe759277/Excel_JsonBinary.png)
 
